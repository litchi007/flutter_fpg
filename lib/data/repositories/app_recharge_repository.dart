import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/wd/CapitalDetailModel.dart';
import 'package:fpg_flutter/data/models/wd/DepositRecordModel.dart';
import 'package:fpg_flutter/data/models/wd/WithdrawalRecordModel.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';

class AppRechargeRepository {
  late AppHttpClient appHttpClient;

  final storage = GetStorage();
  AppRechargeRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<CapitalDetailListModel?> getCapitalDetailData(
      {int? page, String? startDate, String? endDate, String? group}) async {
    try {
      var response = await appHttpClient.getCapitalDetailData(
          token: AppDefine.userToken?.apiSid,
          page: page.toString(),
          startDate: startDate,
          endDate: endDate,
          group: group,
          rows: '20');
      return response.data;
    } catch (e) {
      AppLogger.e('getCapitalDetailData error', error: e);
    }
    return null;
  }

  Future<DepositRecordListModel?> getDepositRecordData(
      {int? page, String? startDate, String? endDate}) async {
    try {
      var response = await appHttpClient.getDepositRecordData(
          token: AppDefine.userToken?.apiSid,
          page: page.toString(),
          startDate: startDate,
          endDate: endDate,
          rows: '20');
      return response.data;
    } catch (e) {
      AppLogger.e('getDepositRecordData error', error: e);
    }
    return null;
  }

  Future<WithdrawalRecordListModel?> getWithdrawalRecordData(
      {int? page, String? startDate, String? endDate}) async {
    try {
      var response = await appHttpClient.getWithdrawalRecordData(
          token: AppDefine.userToken?.apiSid,
          page: page.toString(),
          startDate: startDate,
          endDate: endDate,
          rows: '100');
      return response.data;
    } catch (e) {
      AppLogger.e('getWithdrawalRecordData error', error: e);
    }
    return null;
  }

  Future<PayAisleData?> cashier() async {
    try {
      var response =
          await appHttpClient.cashier(token: AppDefine.userToken?.apiSid);
      return response.data;
    } catch (e) {
      AppLogger.e('cashier error', error: e);
    }
    return null;
  }

  Future<dynamic> payLoginCoin({String? payId}) async {
    try {
      var response = await appHttpClient.payLoginCoin(
          token: AppDefine.userToken?.apiSid, payId: payId);
      // if (response.data == null) AppToast.showDuration(msg: response.msg);
      return response.data;
    } catch (e) {
      AppLogger.e('payLoginCoin error', error: e);
    }
    return null;
  }

  Future<dynamic> withdrawApply(
      {String? id,
      int? money,
      String? pwd,
      int? virtual_amount,
      int? isC2c}) async {
    try {
      var response = await appHttpClient.withdrawApply(
          token: AppDefine.userToken?.apiSid,
          id: id,
          money: money,
          pwd: pwd,
          virtual_amount: virtual_amount,
          isC2c: isC2c);
      AppToast.showDuration(msg: response.msg);

      return response.data;
    } catch (e) {
      AppLogger.e('withdrawApply error', error: e);
    }
    return null;
  }

  Future<dynamic> transfer(
      {String? amount,
      String? payee,
      String? channel,
      String? remark,
      String? payer,
      String? depositTime,
      String? upiAccount}) async {
    try {
      var response = await appHttpClient.transferForPay(
          token: AppDefine.userToken?.apiSid,
          amount: amount,
          channel: channel,
          remark: remark,
          payer: payer,
          payee: payee,
          depositTime: depositTime,
          upiAccount: upiAccount);

      return response;
    } catch (e) {
      AppLogger.e('transferForPay error', error: e);
    }
    return null;
  }

  Future<dynamic> onlinePay({
    String? money,
    String? payId,
    String? gateway,
  }) async {
    try {
      var response = await appHttpClient.onlinePay(
        token: AppDefine.userToken?.apiSid,
        money: money,
        payId: payId,
        gateway: gateway,
      );

      return response;
    } catch (e) {
      AppLogger.e('onlinePay error', error: e);
    }
    return '';
  }
}
