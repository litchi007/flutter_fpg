import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/data/models/redEnvelop/RedEnvelopModel.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/data/models/redEnvelop/RedPackedLogModel.dart';
import 'package:fpg_flutter/services/global_service.dart';

class ApiRedbagCenterRepository {
  late AppHttpClient appHttpClient;

  final storage = GetStorage();
  ApiRedbagCenterRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<RedEnvelopListModel?> getRedPacketList({String? action}) async {
    try {
      var response = await appHttpClient.getRedPacketList(
          token: AppDefine.userToken?.apiSid, action: action);
      return response.data;
    } catch (e) {
      AppLogger.e('getGraborderRecords error', error: e);
    }
    return null;
  }

  Future<String?> getRedPacket({String? rpid}) async {
    try {
      var response = await appHttpClient.getRedPacket(
          token: GlobalService.to.token.value, rpid: rpid);
      AppLogger.d("rpid  : $rpid");
      AppLogger.d("response.code  : ${response.code}");
      AppLogger.d("response.data  : ${response.data}");
      AppLogger.d("response.msg  :  ${response.msg}");

      if (response.code == 0 && response.data != null) {
        AppToast.showDuration(msg: '恭喜您获得了${response.data}元红包');
      } else {
        AppToast.showDuration(msg: response.msg);
        // AppToast.showDuration(msg: '红包已被抢完');
      }

      return response.data;
    } catch (e) {
      AppLogger.e('getGraborderRecords error', error: e);
    }
    return null;
  }

  Future<List<RedpackedLogmodel>?> getRedPacketLogList() async {
    try {
      var response = await appHttpClient.getRedPacketLogList(
        token: AppDefine.userToken?.apiSid,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('getGraborderRecords error', error: e);
    }
    return null;
  }
}
