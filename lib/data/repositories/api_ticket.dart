import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';

class ApiTicket {
  late AppHttpClient appHttpClient;

  ApiTicket()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<UGBetsRecordListModel?> lotteryStatistics(
      String token, String startDate, String endDate) async {
    try {
      var response = await appHttpClient.lotteryStatistics(
          token: token, startDate: startDate, endDate: endDate);
      return response.data;
    } catch (e) {
      AppLogger.e('lotteryStatistics', error: e);
    }
    return null;
  }

  Future<UGBetsRecordDataLongModel?> gethistory(
      String token, String startDate, String endDate) async {
    try {
      var response = await appHttpClient.gethistory(
          token: token, startDate: startDate, endDate: endDate);
      return response.data;
    } catch (e) {
      AppLogger.e('gethistory', error: e);
    }
    return null;
  }

  Future<UGBetsRecordDataLongModel?> history(
      String token,
      String category,
      int status,
      String startDate,
      String endDate,
      String gameId,
      int page,
      int rows,
      String timeZone) async {
    try {
      var response = await appHttpClient.history(
          token: token,
          category: category,
          startDate: startDate,
          endDate: endDate,
          gameId: gameId,
          page: page,
          rows: rows,
          timeZone: timeZone);
      return response.data;
    } catch (e) {
      AppLogger.e('history', error: e);
    }
    return null;
  }

  Future<dynamic> getLotteryData() async {
    try {
      var response = await appHttpClient.getLotteryData();
      return response.data['unbalancedMoney'];
    } catch (e) {
      AppLogger.e('getLotteryData error', error: e);
    }
    return null;
  }
}
