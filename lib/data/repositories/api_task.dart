import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/data/models/MissionModel.dart';
import 'package:fpg_flutter/data/models/MissionData.dart';
import 'package:fpg_flutter/data/models/UGSignInModel.dart';

class ApiTask {
  late AppHttpClient appHttpClient;

  ApiTask()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<List<SalaryModel>?> getMissionBonusList() async {
    try {
      var response = await appHttpClient.getMissionBonusList();
      return response.data;
    } catch (e) {
      AppLogger.e('getMissionBonusList', error: e);
    }
    return null;
  }

  Future<List<ScoreLevelData>?> getLevels() async {
    try {
      var response = await appHttpClient.getLevels();
      return response.data;
    } catch (e) {
      AppLogger.e('getLevels', error: e);
    }
    return null;
  }

  Future<NewSalaryModel?> getMissionBonusListNew({String? token}) async {
    try {
      var response = await appHttpClient.getMissionBonusListNew(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getMissionBonusListNew', error: e);
    }
    return null;
  }

  Future<Null> sendMissionBonus({String? bonsId}) async {
    try {
      var response = await appHttpClient.sendMissionBonus(bonsId: bonsId);
      if (response.code == 0) AppToast.showDuration(msg: '领取成功');
      return response.data;
    } catch (e) {
      AppLogger.e('sendMissionBonus', error: e);
    }
    return null;
  }

  Future<CreditsLogModel?> getCreditsLog(
      String token, String time, int page, int rows) async {
    try {
      var response = await appHttpClient.getCreditsLog(
          token: token, time: time, page: page, rows: rows);
      return response.data;
    } catch (e) {
      AppLogger.e('getCreditsLog', error: e);
    }
    return null;
  }

  Future<int?> creditsExchange(String? token, double money) async {
    try {
      var response =
          await appHttpClient.creditsExchange(token: token, money: money);

      AppToast.showDuration(msg: response.msg);

      return response.code;
    } catch (e) {
      AppLogger.e('creditsExchange', error: e);
    }
    return null;
  }

  Future<UGSignInModel?> checkinList(String token) async {
    try {
      var response = await appHttpClient.checkinList(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('checkinList', error: e);
    }
    return null;
  }

  Future<List<UGSignInHistoryModel>?> checkinHistory(String token) async {
    try {
      var response = await appHttpClient.checkinHistory(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('checkinHistory', error: e);
    }
    return null;
  }

  Future<String?> checkinBonus(String token, String type) async {
    try {
      var response = await appHttpClient.checkinBonus(token: token, type: type);
      return response.data;
    } catch (e) {
      AppLogger.e('checkinBonus', error: e);
    }
    return null;
  }

  // 用户签到
  Future<String?> checkin(String token, String type, String date) async {
    try {
      var response =
          await appHttpClient.checkin(token: token, type: type, date: date);
      return response.data;
    } catch (e) {
      AppLogger.e('checkin', error: e);
    }
    return null;
  }

  // 任务大厅
  Future<dynamic> center(
      String token, String category, int page, int rows) async {
    try {
      var response = await appHttpClient.center(
          token: token, category: category, page: page, rows: rows);
      return response.data;
    } catch (e) {
      AppLogger.e('center', error: e);
    }
    return null;
  }

  // 任务大厅
  Future<List<MissionCategory>?> categories({String? token}) async {
    try {
      var response = await appHttpClient.categories(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('center', error: e);
    }
    return null;
  }
}
