import 'dart:io';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:fpg_flutter/data/models/UGRegisterModel.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:fpg_flutter/http/tools/rsa_encrypt.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/data/models/UserMsgList.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/data/models/userModel/userTransData.dart';
import 'package:fpg_flutter/data/models/changLongModel/LotteryResultModel.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/utils/log_util.dart';

class AppUserRepository {
  late AppHttpClient appHttpClient;

  AppUserRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<UGUserModel?> getUserInfo({String? token}) async {
    try {
      var response = await appHttpClient.getUserInfoAPI(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getUserInfo error', error: e);
    }
    return null;
  }

  Future<dynamic> getRecord(
      {String? token,
      int? type,
      String? date,
      int? isReply,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.getRecord(
          token: token,
          type: type,
          date: date,
          isReply: isReply,
          page: page,
          rows: rows);
      return response.data;
    } catch (e) {
      AppLogger.e('getRecord error', error: e);
    }
  }

  Future<dynamic> addFeedback(
      {String? token,
      String? type,
      String? pid,
      String? content,
      List<String>? imgPaths}) async {
    try {
      var response = await appHttpClient.addFeedback(
          token: token,
          type: type,
          pid: pid,
          content: content,
          imgPaths: imgPaths);
      AppToast.showDuration(msg: response.msg, duration: 2);
      return response.data;
    } catch (e) {
      AppLogger.e('getUserInfo error', error: e);
    }
  }

  Future<NetBaseEntity<UGRegisterModel>> register(
      String userName, String password,
      {String? fullName,
      String? inviter,
      String? inviteCode,
      String? fundPwd,
      String? qq,
      String? wx,
      String? email,
      String? imgCode,
      String? smsCode,
      String? phone,
      String? fb,
      String? line}) async {
    var res = await appHttpClient.register(
        usr: userName,
        pwd: md5(password),
        device: Platform.isAndroid ? '2' : '3',
        fullName: fullName,
        inviter: inviter,
        inviteCode: inviteCode,
        fundPwd: fundPwd,
        qq: qq,
        wx: wx,
        email: email,
        imgCode: imgCode,
        accessToken: AppDefine.accessToken,
        smsCode: smsCode,
        phone: phone,
        fb: fb,
        line: line);
    if (res.data == null) {
      // return '注册失败: ${res.msg}';
    }
    return res;
  }

  Future<NetBaseEntity<UGLoginModel>?> login(String userName, String password,
      {String? token, String? fullName, String? imgCode}) async {
    var res = await appHttpClient.login(
        token: token,
        usr: userName,
        pwd: md5(password),
        accessToken: AppDefine.accessToken,
        device: Platform.isAndroid ? '2' : '3',
        fullName: fullName,
        imgCode: imgCode);
    LogUtil.w("===================");
    LogUtil.w(res);
    return res;
  }

  Future<UGLoginModel?> guestLogin() async {
    var res = await appHttpClient.guestLogin(
      usr: "46da83e1773338540e1e1c973f6c8a68", // md5(userName),
      pwd: "46da83e1773338540e1e1c973f6c8a68", // md5(password),
    );

    return res.data;
  }

  Future<dynamic> logout({String? token}) async {
    var res = await appHttpClient.logout(
      token: token,
      device: '2',
    );

    return res;
  }

  // 修改登录密码
  Future<NetBaseEntity?> changeLoginPwd(String oldPwd, String newPwd,
      {String? token}) async {
    try {
      var response = await appHttpClient.changeLoginPwd(
        token: token,
        oldPwd: md5(oldPwd),
        newPwd: md5(newPwd),
      );
      return response;
    } catch (e) {
      // AppLogger.e('Change Login Password error', error: e);
    }
    return null;
  }

  // 修改取款密码
  Future<NetBaseEntity?> changeFundPwd(String oldPwd, String newPwd,
      {String? token}) async {
    try {
      var response = await appHttpClient.changeFundPwd(
        token: token,
        oldPwd: md5(oldPwd),
        newPwd: md5(newPwd),
      );
      return response;
    } catch (e) {
      // AppLogger.e('Change Fund Password error', error: e);
    }
    return null;
  }

  // 修改取款密码
  Future<NetBaseEntity?> addFundPwd(String loginPwd, String fundPwd,
      {String? token}) async {
    try {
      var response = await appHttpClient.addFundPwd(
        token: token,
        loginPwd: md5(loginPwd),
        fundPwd: md5(fundPwd),
      );
      return response;
    } catch (e) {
      // AppLogger.e('Add Fund Password error', error: e);
    }
    return null;
  }

  // 更新常用登录地点（待完善）
  Future<NetBaseEntity?> changeAddress(List<Address> address,
      {String? token}) async {
    try {
      var response = await appHttpClient.changeAddress(
        token: token,
        address: address,
      );
      return response;
    } catch (e) {
      // AppLogger.e('Change User Address error', error: e);
    }
    return null;
  }

  // 删除常用登录地点
  Future<NetBaseEntity?> delAddress({String? id, String? token}) async {
    try {
      var response = await appHttpClient.delAddress(
        token: token,
        id: id,
      );
      return response;
    } catch (e) {
      // AppLogger.e('Delete User Address error', error: e);
    }
    return null;
  }

  // 常用登录地点列表
  Future<List<Address>?> getAddress({String? token}) async {
    try {
      var response = await appHttpClient.getAddress(
        token: token,
      );
      return response.data;
    } catch (e) {
      // AppLogger.e('Get Addresses error', error: e);
    }
    return null;
  }

  Future<UserMsgList?> msgList(int page, int row) async {
    try {
      var response = await appHttpClient.msgList(
          token: AppDefine.userToken?.apiSid, page: page, row: row);
      return response.data;
    } catch (e) {
      AppLogger.e('msgList', error: e);
    }
    return null;
  }

  Future<dynamic> transfer(
      {String? token,
      double? money,
      String? fromType,
      String? toType,
      String? pwd,
      double? rate}) async {
    try {
      var response = await appHttpClient.transfer(
          token: token,
          money: money,
          fromType: fromType,
          toType: toType,
          pwd: md5(pwd ?? ''),
          rate: rate);
      AppToast.showDuration(msg: response.msg);
      return response.data;
    } catch (e) {
      AppLogger.e('transfer error', error: e);
    }
    return null;
  }

  Future<dynamic> updateUserAvatar(
      {String? token, String? publicAvatarId}) async {
    try {
      var response = await appHttpClient.updateUserAvatar(
          token: token, publicAvatarId: publicAvatarId);
      return response;
    } catch (e) {
      AppLogger.e('updateUserAvatar', error: e);
    }
    return null;
  }

  // 我的提款账户列表
  Future<ManageBankCardData?> bankCard({String? token}) async {
    try {
      var response = await appHttpClient.bankCard(token: token);

      return response.data;
    } catch (e) {
      AppLogger.e('bankCard error', error: e);
    }
    return null;
  }

  // 设置真实姓名
  Future<NetBaseEntity?> profileName({String? token, String? fullName}) async {
    try {
      var response =
          await appHttpClient.profileName(token: token, fullName: fullName);
      return response;
    } catch (e) {
      AppLogger.e('profileName', error: e);
    }
    return null;
  }

  // 绑定提款账户
  Future<NetBaseEntity?> bindBank(String type, String bankId, String bankAddr,
      String bankCard, String qrcode, String ownerName, String pwd,
      {String? token}) async {
    try {
      var response = await appHttpClient.bindBank(
          token: token,
          type: type,
          bankId: bankId,
          bankAddr: bankAddr,
          bankCard: bankCard,
          qrcode: qrcode,
          ownerName: ownerName,
          pwd: md5(pwd));
      return response;
    } catch (e) {
      AppLogger.e('bindBank', error: e);
    }
    return null;
  }

  // 编辑提款账户
  Future<NetBaseEntity?> editBank(String id, String bankId, String bankAddr,
      String bankCard, String qrcode, String ownerName, String pwd,
      {String? token}) async {
    try {
      var response = await appHttpClient.editBank(
          token: token,
          id: id,
          bankId: bankId,
          bankAddr: bankAddr,
          bankCard: bankCard,
          qrcode: qrcode,
          ownerName: ownerName,
          pwd: md5(pwd));
      return response;
    } catch (e) {
      AppLogger.e('editBank', error: e);
    }
    return null;
  }

  Future<UserTransData?> transferLogs({int? type}) async {
    try {
      var response = await appHttpClient.userTransferLogs(
          token: AppDefine.userToken?.apiSid, type: type);
      return response.data;
    } catch (e) {
      AppLogger.e('get_stat_for_yuebao error', error: e);
    }
    return null;
  }

  Future<dynamic> changeEmail(String token, String email) async {
    try {
      var response =
          await appHttpClient.changeEmail(token: token, email: email);
      return response.data;
    } catch (e) {
      AppLogger.e('changeEmail', error: e);
    }
  }

  Future<dynamic> applyCoinPwd(
      {String? token,
      String? bankNo,
      String? coinpwd,
      String? mobile,
      String? smsCode,
      String? identityPathDot}) async {
    try {
      var response = await appHttpClient.applyCoinPwd(
          token: token,
          bankNo: bankNo,
          coinpwd: md5(coinpwd ?? ''),
          mobile: mobile,
          smsCode: smsCode,
          identityPathDot: identityPathDot);
      AppToast.showDuration(msg: response.msg);
      return response.data;
    } catch (e) {
      AppLogger.e('applyCoinPwd', error: e);
    }
  }

  Future<LotteryResultData?> userGameBetWithParams({
    String? token,
    List<Map<String, String>>? betBean,
    String? gameId,
    String? endTime,
    String? mobile,
    String? totalNum,
    String? tag,
    String? betIssue,
    String? totalMoney,
    String? activeReturnCoinRatio,
    String? isInstant,
  }) async {
    var response;
    if (isInstant == '1') {
      try {
        response = await appHttpClient.instantBet(
          token: token,
          betBean: betBean,
          gameId: gameId,
          endTime: endTime,
          totalNum: totalNum,
          tag: tag,
          betIssue: betIssue,
          totalMoney: totalMoney,
          activeReturnCoinRatio: activeReturnCoinRatio, //拉条的退水
          // isInstant:  selectedGame. isInstant,
          isInstant: isInstant,
        );
      } catch (e) {
        AppLogger.e('instantBet', error: e);
      }
    } else {
      if (GlobalService.to.userInfo.value?.isTest == true) {
        try {
          response = await appHttpClient.guestBet(
            token: token,
            betBean: betBean,
            gameId: gameId,
            endTime: endTime,
            totalNum: totalNum,
            tag: tag,
            betIssue: betIssue,
            totalMoney: totalMoney,
            activeReturnCoinRatio: activeReturnCoinRatio, //拉条的退水
            // isInstant:  selectedGame. isInstant,
            isInstant: isInstant,
          );
        } catch (e) {
          AppLogger.e('guestBet', error: e);
        }
      } else {
        try {
          response = await appHttpClient.bet(
            token: token,
            betBean: betBean,
            gameId: gameId,
            endTime: endTime,
            totalNum: totalNum,
            tag: tag,
            betIssue: betIssue,
            totalMoney: totalMoney,
            activeReturnCoinRatio: activeReturnCoinRatio, //拉条的退水
            // isInstant:  selectedGame. isInstant,
            isInstant: isInstant,
          );
        } catch (e) {
          AppLogger.e('bet', error: e);
        }
      }

      return response.data;
    }
    return null;
  }

  Future<dynamic> readMsg(String id) async {
    try {
      var res = appHttpClient.readMsg(
        id: id,
        token: AppDefine.userToken?.apiSid ?? '',
      );
      return res;
    } catch (e) {
      AppLogger.e('readMsg error', error: e);
    }
    return null;
  }

  Future<dynamic> getWebAccessToken() async {
    try {
      var res = appHttpClient.grantWebAccessToken(
        token: GlobalService.to.token.value,
      );
      return res;
    } catch (e) {
      AppLogger.e('getWebAccessToken error', error: e);
    }
    return null;
  }
}
