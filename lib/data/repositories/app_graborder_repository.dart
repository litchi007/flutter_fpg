import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/deal.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/http/tools/rsa_encrypt.dart';

class ApiGraborderRepository {
  late AppHttpClient appHttpClient;

  final storage = GetStorage();
  ApiGraborderRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<RecordsData?> getGraborderRecords() async {
    try {
      var response = await appHttpClient.getGraborderRecords(
          token: AppDefine.userToken?.apiSid);
      return response.data;
    } catch (e) {
      AppLogger.e('getGraborderRecords error', error: e);
    }
    return null;
  }

  Future<Deal?> detailForGrabOrder(String dealId) async {
    try {
      var response = await appHttpClient.detailForGrabOrder(
          token: AppDefine.userToken?.apiSid, dealId: dealId);
      return response.data;
    } catch (e) {
      AppLogger.e('getGraborderRecords error', error: e);
    }
    return null;
  }

  Future<DealListData?> getList(
      {ActionType? type,
      List<int>? payChannel,
      int? amountMin,
      int? amountMax}) async {
    try {
      var response = await appHttpClient.getList(
          token: AppDefine.userToken?.apiSid,
          type: type,
          payChannel: payChannel,
          amountMin: amountMin,
          amountMax: amountMax);
      return response.data;
    } catch (e) {
      AppLogger.e('getList error', error: e);
    }
    return null;
  }

  Future<dynamic> createDeposit(
      {String? amount, List<int>? allowBankType}) async {
    try {
      var response = await appHttpClient.createDeposit(
          token: AppDefine.userToken?.apiSid,
          amount: amount,
          allowBankType: allowBankType);
      return response;
    } catch (e) {
      AppLogger.e('createDeposit error', error: e);
    }
    return null;
  }

  Future<dynamic> createWithdraw(
      {String? amount, String? coinpwd, List<int>? payChannels}) async {
    try {
      var response = await appHttpClient.createWithdraw(
          token: AppDefine.userToken?.apiSid,
          amount: amount,
          coinpwd: md5(coinpwd ?? ''),
          payChannels: payChannels);
      return response;
    } catch (e) {
      AppLogger.e('createWithdraw error', error: e);
    }
    return null;
  }
}
