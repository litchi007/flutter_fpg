import 'package:fpg_flutter/app/component/home_menu/model/HomeMenuModel.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/AvatarModel.dart';
import 'package:fpg_flutter/data/models/BannersData.dart';
import 'package:fpg_flutter/data/models/HomeAdsModel.dart';
import 'package:fpg_flutter/data/models/ImgCaptchaModel.dart';
import 'package:fpg_flutter/data/models/NoticeMode.dart';
import 'package:fpg_flutter/data/models/RankUserModel.dart';
import 'package:fpg_flutter/data/models/UGPromoteListModel.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/models/BankListDetailModel.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/models/system_config_model/system_config_model.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/data/models/YuebaoData.dart';

class AppSystemRepository {
  late AppHttpClient appHttpClient;
  static String configKey = '@systemConfig';

  AppSystemRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);
  final storage = GetStorage();

  Future<void> fetchConfig() async {
    var data;
    // var data = await _fetchLocalConfig();
    // if (data != null) {
    // } else {
    // for (int i = 0; i < 5; i++) {
    data = await _fetchRemoteConfig();
    // if (data != null) break;
    // }
    // }

    // storage.write(configKey, data);
    AppDefine.systemConfig = data;
    AppDefine.staticServers = data?.staticServers ?? [];
    GlobalService.to.systemConfig.value = data;
    // if (data != null) {
    //   setConfig(data);
    // }

    LogUtil.w('AppDefine.systemConfig');
    LogUtil.w(AppDefine.systemConfig?.loginVCode);
    LogUtil.w(AppDefine.systemConfig?.loginVCodeType);
  }

  // Future<UGSysConfModel?> _fetchLocalConfig() async {
  //   try {
  //     return storage.read(configKey);
  //   } catch (_) {
  //     return null;
  //   }
  // }

  Future<UGSysConfModel?> _fetchRemoteConfig() async {
    try {
      final response = await appHttpClient.getSystemConfig();
      return response.data;
    } catch (e) {
      LogUtil.e(tag: "_fetchRemoteConfig   error", e);
    }
    return null;
  }

  Future<List<HomeAdsModel>?> homeAds() async {
    try {
      var response = await appHttpClient.getHomeAds();
      return response.data;
    } catch (e) {
      AppLogger.e('homeAds error', error: e);
    }
    return null;
  }

  // picture
  Future<BannersData?> banners() async {
    try {
      var response = await appHttpClient.getBanners();
      return response.data;
    } catch (e) {
      AppLogger.e('banners error', error: e);
    }
    return null;
  }

  Future<HomeNoticeData?> notices() async {
    try {
      var response = await appHttpClient.getLatestNotices();
      return response.data;
    } catch (e) {
      AppLogger.e('notices error', error: e);
    }
    return null;
  }

  Future<List<RankUserModel>?> getRankList() async {
    try {
      var response = await appHttpClient.getRanklist();
      return response.data;
    } catch (e) {
      AppLogger.e('getRankList error', error: e);
    }
    return null;
  }

  Future<UGPromoteListModel?> getPromotions() async {
    try {
      var response = await appHttpClient.getPromotions();
      return response.data;
    } catch (e) {
      AppLogger.e('getPromotion error', error: e);
    }
    return null;
  }

  Future<AvatarSettingModel?> getAvatarSetting({String? token}) async {
    try {
      var response = await appHttpClient.getAvatarSetting(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getPromotion error', error: e);
    }
    return null;
  }

  // 提款渠道列表
  Future<List<BankDetailListData>?> BankList(
      {String? type = '1', String? status = '0'}) async {
    // 0全部, 1银行卡, 2支付宝, 3微信, 4虚拟币
    try {
      var response = await appHttpClient.BankList(type: type, status: status);
      return response.data;
    } catch (e) {
      AppLogger.e('getPromotion error', error: e);
    }
    return null;
  }

  Future<ConverRateModel?> currencyRate(
      {String? token,
      String? from,
      String? to,
      String? amount,
      String? float}) async {
    try {
      var response = await appHttpClient.currencyRate(
          token: token, from: from, to: to, amount: amount, float: float);
      return response.data;
    } catch (e) {
      AppLogger.e('getPromotion error', error: e);
    }
    return null;
  }

  Future<List<AvatarModel>?> getAvatarList() async {
    try {
      var response = await appHttpClient.getAvatarList();
      return response.data;
    } catch (e) {
      AppLogger.e('getAvatarList error', error: e);
    }
    return [];
  }

  Future<ImgCaptchaModel?> getImgCaptcha() async {
    try {
      var response =
          await appHttpClient.getImgCaptcha(accessToken: AppDefine.accessToken);
      return response.data;
    } catch (e) {
      AppLogger.e('getImgCaptcha error', error: e);
    }
    return null;
  }

  Future<List<HomeMenuModel>?> getMobileRightMenu() async {
    try {
      var response = await appHttpClient.getMobileRightMenu();
      return response.data;
    } catch (e) {
      AppLogger.e('getMobileRightMenu error', error: e);
    }
    return null;
  }

  Future<dynamic> floatAds() async {
    try {
      var response = await appHttpClient.floatAds();
      return response.data;
    } catch (e) {
      AppLogger.e('floatAds error', error: e);
    }
    return null;
  }
}
