import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/GameCategory.dart';
import 'package:fpg_flutter/data/models/HallGameModel.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/data/models/LotteryHistoryModel.dart';
import 'package:fpg_flutter/data/models/RealGameTypeModel.dart';
import 'package:fpg_flutter/data/models/RealGamesModel.dart';
import 'package:fpg_flutter/data/models/changLongModel/ChangLongModel.dart';
import 'package:fpg_flutter/data/models/changLongModel/LotteryRecordModel';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class AppGameRepository {
  late AppHttpClient appHttpClient;

  AppGameRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<List<HallGameData>?> lotteryGames() async {
    try {
      var response = await appHttpClient.getLotteryGames();
      return response.data;
    } catch (e) {
      AppLogger.e('lotteryGames error', error: e);
    }
    return null;
  }

  Future<HomeGamesData?> homeGames() async {
    try {
      var response = await appHttpClient.getHomeGames();
      return response.data;
    } catch (e) {
      AppLogger.e('homeGames error', error: e);
    }
    return null;
  }

  Future<List<GameCategory>?> homeRecommend() async {
    try {
      var response = await appHttpClient.getHomeRecommend();
      return response.data;
    } catch (e) {
      AppLogger.e('GameCategory error', error: e);
    }
    return null;
  }

  //获取真人游戏列表
  Future<List<RealGameData>?> realGames() async {
    try {
      var response = await appHttpClient.getRealGames();
      return response.data;
    } catch (e) {
      AppLogger.e('RealGames error', error: e);
    }

    return null;
  }

  //获取真人游戏列表
  Future<List<GroupGameData>?> lotteryGroupGames(
      {int showIssueType = 0}) async {
    try {
      var response = await appHttpClient.getLotteryGroupGames(
          showIssueType: showIssueType);
      return response.data;
    } catch (e) {
      AppLogger.e('GroupGameData error', error: e);
    }
    return null;
  }

  Future<List<RealGameTypeModel>?> getRealGameTypes(
      {String? id, String? search}) async {
    try {
      var response =
          await appHttpClient.getRealGameTypes(id: id, search: search);
      return response.data;
    } catch (e) {
      AppLogger.e('RealGameTypes error', error: e);
    }
    return null;
  }

  Future<LotteryHistoryModel?> getLotteryHistory(
      {String? id, String? date, int? rows}) async {
    try {
      var response =
          await appHttpClient.getLotteryHistory(id: id, date: date, rows: rows);
      return response.data;
    } catch (e) {
      AppLogger.e('getLotteryHistory error', error: e);
    }
    return null;
  }

  Future<List<ChangLongModel>?> changlong({String? id}) async {
    try {
      var response = await appHttpClient.getChanglong(id: id);
      return response.data;
    } catch (e) {
      AppLogger.e('getLotteryHistory error', error: e);
    }
    return null;
  }

  Future<List<LotteryRecordModel>?> getUserRecentBet() async {
    try {
      var response = await appHttpClient.getUserRecentBet(
          // token: AppDefine.userToken?.apiSid, tag: 1);
          token: '7xBBw177Bi1EB77bBSRW8N7x',
          tag: 1);
      return response.data;
    } catch (e) {
      AppLogger.e('getLotteryHistory error', error: e);
    }
    return null;
  }
}
