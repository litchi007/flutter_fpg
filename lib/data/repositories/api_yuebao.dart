import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class AppYuebaoRepository {
  late AppHttpClient appHttpClient;

  AppYuebaoRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<dynamic> stat() async {
    try {
      var response =
          await appHttpClient.stat(token: AppDefine.userToken?.apiSid);
      return response.data;
    } catch (e) {
      AppLogger.e('get_stat_for_yuebao error', error: e);
    }
  }

  Future<dynamic> profitReport() async {
    try {
      var response =
          await appHttpClient.profitReport(token: AppDefine.userToken?.apiSid);
      return response.data;
    } catch (e) {
      AppLogger.e('get_stat_for_yuebao error', error: e);
    }
  }
}
