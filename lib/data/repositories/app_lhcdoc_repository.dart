import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/ColdHotModel.dart';
import 'package:fpg_flutter/data/models/CommentModel.dart';
import 'package:fpg_flutter/data/models/FollowModel.dart';
import 'package:fpg_flutter/data/models/LhcForum2Model.dart';
import 'package:fpg_flutter/data/models/LhcNoListModel.dart';
import 'package:fpg_flutter/data/models/LhcdocCategoryModel.dart';
import 'package:fpg_flutter/data/models/LhcdocContent.dart';
import 'package:fpg_flutter/data/models/LhlDataModel.dart';
import 'package:fpg_flutter/data/models/PostDetailModel.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/data/models/LhcdocModel.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/data/models/QueryAssisModel.dart';
import 'package:fpg_flutter/data/models/TkListModel.dart';
import 'package:fpg_flutter/data/models/UserProfileModel.dart';
import 'package:fpg_flutter/data/models/userModel/lhc_fans_list_model.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class AppLHCDocRepository {
  late AppHttpClient appHttpClient;

  AppLHCDocRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  // 获取首页六合栏目列表
  Future<List<LhcdocCategoryModel>?> getCategoryList() async {
    try {
      var response = await appHttpClient.getCategoryList();
      return response.data;
    } catch (e) {
      AppLogger.e('categoryList error', error: e);
    }
    return null;
  }

  // 六合论坛2信息
  Future<LhcForum2Model?> lhcForum2() async {
    try {
      var response = await appHttpClient.getLhcForum2();
      return response.data;
    } catch (e) {
      AppLogger.e('lhcForum2 error', error: e);
    }
    return null;
  }

  Future<LoginRedPageInfoModel?> loginRegPageInfo() async {
    try {
      var response = await appHttpClient.loginRegPageInfo();
      return response.data;
    } catch (e) {
      AppLogger.e('loginRegPageInfo error', error: e);
    }
    return null;
  }

  // 当前开奖信息
  Future<LotteryNumberData?> lotteryNumber(
      {String? gameName, int? gameId}) async {
    try {
      var response = await appHttpClient.getLotteryNumber(
          gameId: gameId, gameName: gameName);
      return response.data;
    } catch (e) {
      AppLogger.e('lotteryNumber error', error: e);
    }
    return null;
  }

  Future<LhcdocContent?> getLhcContentList({
    String? key,
    String? alias,
    int? page,
    String? model,
    // String sort = "hot",
    String? uid,
    int rows = 25,
  }) async {
    try {
      var response = await appHttpClient.getLhcContentList(
        alias: alias,
        page: page,
        model: model,
        uid: uid,
        // sort: sort,
        rows: rows,
        token: AppDefine.userToken?.apiSid,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('getLhcContentList error', error: e);
    }
    return null;
  }

  Future<LhcdocContent?> getLhcHistoryContent(
      {int page = 1, String? uid, int rows = 1000, String? token}) async {
    try {
      var response = await appHttpClient.getLhcHistoryContent(
          page: page, uid: uid, rows: rows, token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getLhcHistoryContent error', error: e);
    }
    return null;
  }

  Future<List<LhcdocContentList>?> getFavContentList(
      {int page = 0, String? uid, int rows = 1000, String? token}) async {
    try {
      var response = await appHttpClient.getFavContentList(
          page: page, uid: uid, rows: rows, token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getFavContentList error', error: e);
    }
    return null;
  }

  Future<FollowModel?> getFollowList(
      {int page = 0, String? uid, int rows = 1000, String? token}) async {
    try {
      var response = await appHttpClient.getFollowList(
          page: page, uid: uid, rows: rows, token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getFollowList error', error: e);
    }
    return null;
  }

  Future<PostDetailModel?> getLhcContentDetail(String id) async {
    try {
      var response = await appHttpClient.getLhcContentDetail(id: id);
      return response.data;
    } catch (e) {
      AppLogger.e('lhcdocContentDetail error', error: e);
    }
    return null;
  }

  Future<dynamic> doFavorites({String? id, int? favFlag, String? token}) async {
    try {
      var response = await appHttpClient.doFavorites(
          id: id, favFlag: favFlag, token: token, type: 2);
      return response;
    } catch (e) {
      AppLogger.e('doFavorites error', error: e);
    }
    return null;
  }

  Future<dynamic> likePost({
    String? rid,
    int? likeFlag,
    String? token,
    int? type = 1,
  }) async {
    try {
      var response = await appHttpClient.likePost(
          rid: rid, likeFlag: likeFlag, token: token, type: type);
      return response;
    } catch (e) {
      AppLogger.e('likePost error', error: e);
    }
    return null;
  }

  Future<dynamic> followPoster(
      {String? posterUid, int? followFlag, String? token}) async {
    try {
      var response = await appHttpClient.followPoster(
          posterUid: posterUid, followFlag: followFlag, token: token);
      return response;
    } catch (e) {
      AppLogger.e('followPoster error', error: e);
    }
    return null;
  }

  Future<dynamic> postContentReply(
      {String? cid,
      String? rid,
      String? toUid,
      String? content,
      String? token}) async {
    try {
      var response = await appHttpClient.postContentReply(
          cid: cid, rid: rid, toUid: toUid, content: content, token: token);
      return response;
    } catch (e) {
      AppLogger.e('postContentReply error', error: e);
    }
    return null;
  }

  Future<dynamic> vote({String? cid, int? animalId, String? token}) async {
    try {
      var response =
          await appHttpClient.vote(cid: cid, animalId: animalId, token: token);
      return response;
    } catch (e) {
      AppLogger.e('vote error', error: e);
    }
    return null;
  }

  Future<FansRanking?> getLhcFansRankData({String? token, int? limit}) async {
    try {
      var response =
          await appHttpClient.getLhcFansRankData(token: token, limit: limit);
      return response.data;
    } catch (e) {
      AppLogger.e('getLhcFansRankData', error: e);
    }
    return null;
  }

  Future<TipsRanking?> getLhcTipsRankData({String? token, int? limit}) async {
    try {
      var response =
          await appHttpClient.getLhcTipsRankData(token: token, limit: limit);
      return response.data;
    } catch (e) {
      AppLogger.e('getLhcTipsRankData', error: e);
    }
    return null;
  }

  Future<TkListModel?> getTkList(
      {String? token,
      String? alias,
      String? sort,
      String? colorType,
      String? showFav,
      int? page = 1,
      int? rows = 1000}) async {
    try {
      var response = await appHttpClient.getTkList(
          token: token,
          alias: alias,
          sort: sort,
          colorType: colorType,
          showFav: showFav,
          page: page,
          rows: rows);
      return response.data;
    } catch (e) {
      AppLogger.e('getTkList', error: e);
    }
    return null;
  }

  Future<LhcNoListModel?> getLhcNoList(
      {String? token, String? type, String? type2}) async {
    try {
      var response = await appHttpClient.getLhcNoList(
          token: token, type: type, type2: type2);
      return response.data;
    } catch (e) {
      AppLogger.e('getLhcNoList', error: e);
    }
    return null;
  }

  Future<List<ColdHotModel>?> getColdHot() async {
    try {
      var response = await appHttpClient.getColdHot(limit: 50);
      return response.data;
    } catch (e) {
      AppLogger.e('colHot error', error: e);
    }
    return null;
  }

  Future<dynamic?> getlhlItem() async {
    try {
      var response = await appHttpClient.getlhlItem();
      return response.data;
    } catch (e) {
      AppLogger.e('getlhlItem error', error: e);
    }
    return null;
  }

  Future<QueryAssisModel?> getQueryAssis() async {
    try {
      var response = await appHttpClient.getQueryAssis();
      return response.data;
    } catch (e) {
      AppLogger.e('queryAssis error', error: e);
    }
    return null;
  }

  Future<UserProfileModel?> getUserProfile({String? token, String? uid}) async {
    try {
      var response = await appHttpClient.getUserInfo(token: token, uid: uid);
      return response.data;
    } catch (e) {
      AppLogger.e('getUserProfile', error: e);
    }
    return null;
  }

  Future<dynamic> tipContent(
      {String? cid, String? amount, String? token}) async {
    try {
      var response = await appHttpClient.tipContent(
          cid: cid, amount: amount, token: token);
      return response;
    } catch (e) {
      AppLogger.e('tipContent error', error: e);
    }
    return null;
  }

  Future<LhcFansListModel?> contentFansList(
      {String? uid, String? rows, String? page}) async {
    try {
      var response = await appHttpClient.contentFansList(
          token: AppDefine.userToken?.apiSid, uid: uid, rows: rows, page: page);
      return response.data;
    } catch (e) {
      AppLogger.e('contentFansList error', error: e);
    }
    return null;
  }

  Future<LhcFansListModel?> fansList(
      {String? uid, String? rows, String? page}) async {
    try {
      var response = await appHttpClient.fansList(
          token: AppDefine.userToken?.apiSid, uid: uid, rows: rows, page: page);
      return response.data;
    } catch (e) {
      AppLogger.e('fansList error', error: e);
    }
    return null;
  }

  Future<List<CommentModel>?> contentReplyList({
    String? postId,
    String? replyPId,
    int? rows = 100,
    int? page = 1,
  }) async {
    try {
      var response = await appHttpClient.contentReplyList(
        token: AppDefine.userToken?.apiSid,
        contentId: postId,
        replyPId: replyPId,
        rows: rows,
        page: page,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('fansList error', error: e);
    }
    return null;
  }

  Future<dynamic> setNickName({
    String? nickName,
  }) async {
    try {
      var response = await appHttpClient.setNickname(
        token: AppDefine.userToken?.apiSid,
        nickname: nickName,
      );
      return response;
    } catch (e) {
      AppLogger.e('setNickName error', error: e);
    }
    return null;
  }
}
