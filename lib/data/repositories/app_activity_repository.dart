import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/RedBagDetail.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/models/golden_egg_list_model/golden_egg_list_model.dart';
import 'package:fpg_flutter/models/turntable_list_model/turntable_list_model.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/data/models/ApplyWinLog.dart';
import 'package:fpg_flutter/data/models/WinApplyList.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/data/models/ApplyWinLogDetail.dart';
import 'package:fpg_flutter/data/models/activitys/SettingsModel.dart';
import 'package:fpg_flutter/data/models/activitys/ScratchListModel.dart';
import 'package:fpg_flutter/data/models/activitys/PrizeData.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';

class AppActivityRepository {
  late AppHttpClient appHttpClient;

  AppActivityRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<RedBagDetail?> redBagDetail() async {
    try {
      var response = await appHttpClient.redBagDetail();
      return response.data;
    } catch (e) {
      AppLogger.e('redBagDetail error', error: e);
    }
    return null;
  }

  Future<WinApplyList?> fetchWinApplyList() async {
    try {
      var response = await appHttpClient.fetchWinApplyList(
        token: AppDefine.userToken?.apiSid,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('fetchWinApplyList', error: e);
    }
    return null;
  }

  Future<ApplyWinLog?> fetchApplyWinLog() async {
    try {
      var response = await appHttpClient.fetchApplyWinLog(
        token: AppDefine.userToken?.apiSid,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('fetchApplyWinLog', error: e);
    }
    return null;
  }

  Future<ApplyWinLogDetail?> fetchApplyWinLogDetail(String? id) async {
    try {
      var response = await appHttpClient.applyWinLogDetail(
        token: AppDefine.userToken?.apiSid,
        id: id,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('fetchApplyWinLog', error: e);
    }
    return null;
  }

  Future<dynamic> applyWin({
    String? id,
    String? userComment,
    String? amount,
    String? imgCode,
    List<String>? files,
  }) async {
    try {
      var response = await appHttpClient.applyWin(
        token: AppDefine.userToken?.apiToken,
        id: id,
        userComment: userComment,
        amount: amount,
        imgCode: imgCode,
        files: files,
      );
      AppToast.showDuration(msg: response.msg, duration: 2);
      return response.data;
    } catch (e) {
      AppLogger.e('applyWin error', error: e);
    }
  }

  // 获取红包、转盘、刮刮乐、砸金蛋图片
  Future<ActivitySettingsModel?> settings() async {
    try {
      var response = await appHttpClient.settings();
      return response.data;
    } catch (e) {
      AppLogger.e('settings error', error: e);
    }
    return null;
  }

  Future<List<TurntableListModel>?> turntableList() async {
    try {
      var response = await appHttpClient.turntableList();
      return response.data;
    } catch (e) {
      AppLogger.e('redBagDetail', error: e);
    }
    return null;
  }

  Future<RedBagDetail?> redBagRainDetail() async {
    try {
      var response = await appHttpClient.redBagRainDetail(
        token: AppDefine.userToken?.apiSid,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('redBagRainDetail error', error: e);
    }
    return null;
  }

  Future<List<GoldenEggListModel>?> goldenEggList() async {
    try {
      var response = await appHttpClient.goldenEggList();
      return response.data;
    } catch (e) {
      AppLogger.e('redBagDetail', error: e);
    }
    return null;
  }

  Future<ScratchListModel?> scratchList() async {
    try {
      var response = await appHttpClient.scratchList(
        token: AppDefine.userToken?.apiSid,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('scratchList', error: e);
    }
    return null;
  }

  Future<NetBaseEntity<dynamic>?> scratchWin(String? scratchId) async {
    try {
      var response = await appHttpClient.scratchWin(
          token: AppDefine.userToken?.apiSid, scratchId: scratchId);
      return response;
    } catch (e) {
      AppLogger.e('scratchWin', error: e);
    }
    return null;
  }

  Future<PrizeData?> scratchLog(String? id) async {
    try {
      var response = await appHttpClient.scratchLog(
          token: AppDefine.userToken?.apiSid, activityId: id);
      return response.data;
    } catch (e) {
      AppLogger.e('scratchLog', error: e);
    }
    return null;
  }

  Future<dynamic> getRedBag_activity({String? id}) async {
    try {
      var response = await appHttpClient.getRedBag_activity(
        token: AppDefine.userToken?.apiToken,
        id: id,
      );
      return response;
    } catch (e) {
      AppLogger.e('getRedBag_activity', error: e);
    }
    return null;
  }
}
