import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatMessageModel.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatTokenModel.dart';
import 'package:fpg_flutter/data/models/NextIssueData.dart';
import 'package:fpg_flutter/data/models/RedBagGrabModel.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatUserInfoResModel.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:uuid/v4.dart';

class AppBetChatRepository {
  late AppHttpClient appHttpClient;

  AppBetChatRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<ChatTokenModel?> getChatToken({String? token, String? roomId}) async {
    DateTime now = DateTime.now();

    try {
      var response = await appHttpClient.getChatToken(
          token: token,
          t: now.millisecondsSinceEpoch.toString(),
          roomId: roomId);
      return response.data;
    } catch (e) {
      AppLogger.e('getChatToken error', error: e);
    }
    return null;
  }

  Future<dynamic> updateNickname(
      {String? token, String? text, String? roomId}) async {
    try {
      var response = await appHttpClient.updateNickname(
          token: token, text: text, roomId: roomId);
      return response;
    } catch (e) {
      AppLogger.e('updateNickname error', error: e);
    }
    return null;
  }

  Future<NextIssueData?> getNextIssueData({String? id}) async {
    try {
      var response = await appHttpClient.getNextIssue(id: id);
      return response.data;
    } catch (e) {
      AppLogger.e('getNextIssue error', error: e);
    }
    return null;
  }

  Future<ChatUserInfoResModel?> getUserInfoById(
      {String? uid, String? token}) async {
    try {
      var response =
          await appHttpClient.getUserInfoById(uid: uid, token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getUserInfoById Chat error', error: e);
    }
    return null;
  }

  Future<ChatUserInfoResModel?> banned(
      {String? uid, String? token, int? operate}) async {
    try {
      var response =
          await appHttpClient.banned(uid: uid, token: token, operate: operate);
      return response.data;
    } catch (e) {
      AppLogger.e('banned error', error: e);
    }
    return null;
  }

  Future<ChatUserInfoResModel?> operateNickname(
      {String? uid, String? token, String? roomId}) async {
    try {
      var response = await appHttpClient.operateNickname(
          uid: uid, token: token, roomId: roomId);
      return response.data;
    } catch (e) {
      AppLogger.e('banned error', error: e);
    }
    return null;
  }

  Future<ChatUserInfoResModel?> bannedIp(
      {String? uid, String? token, int? operate, String? ip}) async {
    try {
      var response = await appHttpClient.bannedIp(
          uid: uid, token: token, operate: operate, ip: ip);
      return response.data;
    } catch (e) {
      AppLogger.e('bannedIp error', error: e);
    }
    return null;
  }

  Future<ChatUserInfoResModel?> getMemberInfoById(
      {String? uid, String? token}) async {
    try {
      var response =
          await appHttpClient.getMemberInfoById(uid: uid, token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getNextIssue error', error: e);
    }
    return null;
  }

  Future<List<ChatMessageModel>?> getChatMessageRecord(
      {String? token,
      String? sendUid,
      int? chatType,
      String? messageCode,
      String? roomId,
      int? num}) async {
    try {
      var response = await appHttpClient.getMessageRecord(
          token: token,
          sendUid: sendUid,
          chatType: chatType,
          messageCode: messageCode,
          roomId: roomId,
          num: num);
      return response.data;
    } catch (e) {
      AppLogger.e('getMessageRecord error', error: e);
    }
    return null;
  }

  Future<dynamic> getBetBills({String? token}) async {
    try {
      var response = await appHttpClient.getBetBills(token: token);
      return response;
    } catch (e) {
      AppLogger.e('getBetBills error', error: e);
    }
    return null;
  }

  Future<dynamic> createRedBag(
      {String? token,
      int? genre,
      String? amount,
      String? roomId,
      String? title,
      int? quantity,
      String? mineNumber,
      String? coinPwd}) async {
    try {
      var response = await appHttpClient.createRedBag(
          token: token,
          genre: genre,
          amount: amount,
          roomId: roomId,
          title: title,
          quantity: quantity,
          mineNumber: mineNumber,
          coinPwd: coinPwd);
      return response;
    } catch (e) {
      AppLogger.e('createRedBag error', error: e);
    }
    return null;
  }

  Future<RedBagGrabModel?> getRedBag({String? token, String? redBagId}) async {
    try {
      var response =
          await appHttpClient.getRedBag(token: token, redBagId: redBagId);
      return response.data;
    } catch (e) {
      AppLogger.e('createRedBag error', error: e);
    }
    return null;
  }
}
