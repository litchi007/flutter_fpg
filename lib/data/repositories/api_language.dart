import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';

class ApiLanguage {
  late AppHttpClient appHttpClient;

  ApiLanguage()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<UGinviteInfoModel?> inviteInfo(String token) async {
    try {
      var response = await appHttpClient.inviteInfo(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('inviteInfo', error: e);
    }
    return null;
  }

  Future<LanguageData?> getconfigs(String token) async {
    try {
      var response = await appHttpClient.getconfigs(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('getconfigs', error: e);
    }
    return null;
  }
}
