import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/http/tools/rsa_encrypt.dart';

class ApiTeam {
  late AppHttpClient appHttpClient;

  ApiTeam()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  Future<UGinviteInfoModel?> inviteInfo(String token) async {
    try {
      var response = await appHttpClient.inviteInfo(token: token);
      return response.data;
    } catch (e) {
      AppLogger.e('inviteInfo', error: e);
    }
    return null;
  }

  Future<M_RealBetStat?> sportsBetStat(
      {String? token,
      int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.sportsBetStat(
        token: token,
        level: level,
        startDate: startDate,
        endDate: endDate,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('sportsBetStat', error: e);
    }
    return null;
  }

  Future<InviteCodeList?> inviteCodeList(
      String token, int page, int rows) async {
    try {
      var response = await appHttpClient.inviteCodeList(
          token: token, page: page, rows: rows);
      return response.data;
    } catch (e) {
      AppLogger.e('inviteCodelist', error: e);
    }
    return null;
  }

  Future<Null> updateInviteCodeStatus(String token, int id, int status) async {
    try {
      var response = await appHttpClient.updateInviteCodeStatus(
          token: token, id: id, status: status);
      return response.data;
    } catch (e) {
      AppLogger.e('updateInviteCodeStatus', error: e);
    }
    return null;
  }

  Future<String?> createInviteCode(String token, int length, int count,
      int user_type, int randomCheck) async {
    try {
      var response = await appHttpClient.createInviteCode(
          token: token,
          length: length,
          count: count,
          user_type: user_type,
          randomCheck: randomCheck);
      return response;
    } catch (e) {
      AppLogger.e('createInviteCode', error: e);
    }
    return null;
  }

  Future<InviteListModel?> inviteList(
      {String? token, int? level, int? page, int? rows}) async {
    try {
      var response = await appHttpClient.inviteList(
        token: token,
        level: level,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('inviteList', error: e);
    }
    return null;
  }

  Future<M_BetStat?> betStat(
      {int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.betStat(
        token: AppDefine.userToken?.apiSid,
        level: level,
        startDate: startDate,
        endDate: endDate,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('betStat', error: e);
    }
    return null;
  }

  Future<M_RealBetList?> realBetList(
      {int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.realBetList(
        token: AppDefine.userToken?.apiSid,
        level: level,
        startDate: startDate,
        endDate: endDate,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('realBetList', error: e);
    }
    return null;
  }

  Future<M_BetList?> betList(
      {String? token,
      int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.betList(
        token: token,
        level: level,
        startDate: startDate,
        endDate: endDate,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('betList', error: e);
    }
    return null;
  }

  Future<M_DepositStatData?> depositStat(
      {String? token,
      int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.depositStat(
        token: token,
        level: level,
        startDate: startDate,
        endDate: endDate,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('depositStat', error: e);
    }
    return null;
  }

  Future<M_WithdrawStatData?> withdrawStat(
      {String? token,
      int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.withdrawStat(
        token: token,
        level: level,
        startDate: startDate,
        endDate: endDate,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('withdrawStat', error: e);
    }
    return null;
  }

  Future<M_RealBetStat?> realBetStat(
      {String? token,
      int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.realBetStat(
        token: token,
        level: level,
        startDate: startDate,
        endDate: endDate,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('realBetStat', error: e);
    }
    return null;
  }

  Future<M_InviteDomainList?> inviteDomain(
      {String? token, int? page, int? rows}) async {
    try {
      var response = await appHttpClient.inviteDomain(
        token: token,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('inviteDomain', error: e);
    }
    return null;
  }

  Future<M_DepositListtData?> depositList(
      {String? token,
      int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.depositList(
        token: token,
        level: level,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('depositList', error: e);
    }
    return null;
  }

  Future<M_WithdrawListData?> withdrawList(
      {String? token,
      int? level,
      String? startDate,
      String? endDate,
      int? page,
      int? rows}) async {
    try {
      var response = await appHttpClient.withdrawList(
        token: token,
        level: level,
        page: page,
        rows: rows,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('withdrawList', error: e);
    }
    return null;
  }

  Future<String?> addMember(String token, String userName, String pwd,
      String? fullName, String? phone, int type) async {
    try {
      var response = await appHttpClient.addMember(
        token: token,
        pwd: md5(pwd),
        fullName: fullName,
        phone: phone,
        type: type,
      );
      return response.data;
    } catch (e) {
      AppLogger.e('addMember', error: e);
    }
    return null;
  }
}
