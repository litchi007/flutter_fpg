import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/RealGamesModel.dart';
import 'package:fpg_flutter/http/api_interface/app_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class AppRealRepository {
  late AppHttpClient appHttpClient;

  AppRealRepository()
      : appHttpClient = AppHttpClient(AppHttpCore(baseUrl: AppDefine.host).dio);

  // 真人余额查询
  Future<ResponseBalance?> checkBalance(int? id, String? token) async {
    try {
      var response =
          await appHttpClient.checkBalance(id: id.toString(), token: token);
      // int? id, String? token
      return response.data;
    } catch (e) {
      // AppLogger.e('checkBalance error', error: e);
    }
    return null;
  }

  // 手动额度转换
  Future<NetBaseEntity<String?>?> apiRealManualTransfer(String? token,
      double? money, String? transOutId, String? transInId) async {
    try {
      var response = await appHttpClient.manualTransfer(
          token: token,
          money: money.toString(),
          fromId: transOutId,
          toId: transInId);
      // int? id, String? token
      return response;
    } catch (e) {
      // AppLogger.e('apiRealManualTransfer error', error: e);
    }
    return null;
  }

  // 额度一键转出，第一步：获取需要转出的真人ID
  Future<ResponseOneKeyTransferOutData?> oneKeyTransferOut(
      String? token) async {
    try {
      var response = await appHttpClient.oneKeyTransferOut(
        token: token,
      );
      // String? token
      return response.data;
    } catch (e) {
      // AppLogger.e('oneKeyTransferOut error', error: e);
    }
    return null;
  }

  // 额度一键转出，第二步：根据真人ID并发请求单游戏快速转出
  Future<int?> quickTransferOut(String? token, String? id) async {
    try {
      var response = await appHttpClient.quickTransferOut(
        token: token,
        id: id,
      );
      // String? id, String? token
      return response.code;
    } catch (e) {
      // AppLogger.e('quickTransferOut error', error: e);
    }
    return null;
  }

  // 手动额度转换记录
  Future<ResponseTransferLogs?> transferLogs(
      String? token, String? startTime, String? endTime,
      {int page = 1, int rows = 20}) async {
    try {
      var response = await appHttpClient.transferLogs(
        token: token,
        startTime: startTime,
        endTime: endTime,
        page: page.toString(),
        rows: rows.toString(),
      );
      // String? id, String? token
      return response.data;
    } catch (e) {
      // AppLogger.e('quickTransferOut error', error: e);
    }
    return null;
  }

  Future<NetBaseEntity<String?>?> gotoGame(
      {String? id, String? game, String? token}) async {
    try {
      var response =
          await appHttpClient.gotoGame(id: id, game: game, token: token);
      return response;
    } catch (e) {
      AppLogger.e('followPoster error', error: e);
    }
    return null;
  }
}
