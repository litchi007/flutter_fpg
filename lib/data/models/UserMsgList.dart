import 'package:fpg_flutter/utils/helper.dart';

class UserMsg {
  int? isRead;
  String? content;
  String? summary;
  String? updateTime;
  String? id;
  bool? isPopup;
  String? title;
  int? isPushNotification;
  String? readTime;
  String? extra;

  UserMsg({
    this.isRead,
    this.content,
    this.summary,
    this.updateTime,
    this.id,
    this.isPopup,
    this.title,
    this.isPushNotification,
    this.readTime,
    this.extra,
  });

  factory UserMsg.fromJson(Map<String, dynamic> json) {
    return UserMsg(
      isRead: json['isRead'],
      content: json['content'],
      summary: json['summary'],
      updateTime: json['updateTime'],
      id: json['id'],
      isPopup: json['isPopup'],
      title: json['title'],
      isPushNotification: json['isPushNotification'],
      readTime: json['readTime'],
      extra: json['extra'],
    );
  }
}

class RealTime {
  int? id;

  RealTime({required this.id});

  factory RealTime.fromJson(Map<String, dynamic> json) {
    return RealTime(
      id: convertToInt(json['id']),
    );
  }
}

class UserMsgList {
  RealTime? realTime;
  List<UserMsg>? list;
  int? readTotal;
  int? total;

  UserMsgList({
    this.realTime,
    this.list,
    this.readTotal,
    this.total,
  });

  factory UserMsgList.fromJson(Map<String, dynamic> json) {
    // Deserialize list of List objects
    List<UserMsg>? list =
        (json['list'] as List?)?.map((item) => UserMsg.fromJson(item)).toList();

    return UserMsgList(
      realTime:
          json['realTime'] == null ? null : RealTime.fromJson(json['realTime']),
      list: list,
      readTotal: json['readTotal'],
      total: json['total'],
    );
  }
}
