class MixBallModel {
  final dynamic group;
  final String nums;

  MixBallModel({
    required this.group,
    required this.nums,
  });

  factory MixBallModel.fromJson(Map<String, dynamic> json) {
    return MixBallModel(
      group: json['group'],
      nums: json['nums'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'group': group,
      'nums': nums,
    };
  }
}

class QueryAssisModel {
  final Map<String, MixBallModel> mixBall;
  final List<String> sxlmb;

  QueryAssisModel({
    required this.mixBall,
    required this.sxlmb,
  });

  factory QueryAssisModel.fromJson(Map<String, dynamic> json) {
    return QueryAssisModel(
      mixBall: (json['mixBall'] as Map<String, dynamic>).map(
        (key, value) => MapEntry(key, MixBallModel.fromJson(value)),
      ),
      sxlmb: List<String>.from(json['sxlmb']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'mixBall': mixBall.map((key, value) => MapEntry(key, value.toJson())),
      'sxlmb': sxlmb,
    };
  }
}
