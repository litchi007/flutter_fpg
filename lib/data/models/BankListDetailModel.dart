/// 银行卡信息数据

class BankDetailListModel {
  final int? code;
  final String? msg;
  final List<BankDetailListData>? data;

  BankDetailListModel({
    this.code,
    this.msg,
    this.data,
  });

  // Factory method to create an instance from a JSON object
  factory BankDetailListModel.fromJson(Map<String, dynamic> json) {
    return BankDetailListModel(
      code: json['code'] as int?,
      msg: json['msg'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => BankDetailListData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }

  // Convert instance to JSON object
  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'msg': msg,
      'data': data?.map((e) => e.toJson()).toList(),
    };
  }
}

// BankDetailListData represents the detailed information of a bank
class BankDetailListData {
  final String? id;
  final String? code;
  final String? name;
  final String? logo;
  final String? home;
  final String? rate;
  final String? isDelete;
  final String? currencyRate;

  BankDetailListData({
    this.id, // 1
    this.code,
    this.name, //工商银行
    this.logo,
    this.home, //http://www.icbc.com.cn
    this.rate, //0.00
    this.isDelete,
    this.currencyRate,
  });

  // Factory method to create an instance from a JSON object
  factory BankDetailListData.fromJson(Map<String, dynamic> json) {
    return BankDetailListData(
      id: json['id'],
      code: json['code'],
      name: json['name'],
      logo: json['logo'],
      home: json['home'],
      rate: json['rate'],
      isDelete: json['isDelete'],
      currencyRate: json['currencyRate'].toString(),
    );
  }

  // Convert instance to JSON object
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'code': code,
      'name': name,
      'logo': logo,
      'home': home,
      'rate': rate,
      'isDelete': isDelete,
      'currencyRate': currencyRate,
    };
  }
}
