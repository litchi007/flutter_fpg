class UserTransData {
  int total;
  List<UserTransList> list;

  UserTransData({
    required this.total,
    required this.list,
  });

  factory UserTransData.fromJson(Map<String, dynamic> json) {
    return UserTransData(
      total: json['total'],
      list: (json['list'] as List<dynamic>)
          .map((item) => UserTransList.fromJson(item))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      'total': total,
      'list': list.map((item) => item.toJson()).toList(),
    };
    return data;
  }
}

class UserTransList {
  String typeName; // 转入DOY
  String coin; // 2.00
  String settleCoin; // 795.00
  String addTime; // 2021-09-26 14:28:01

  UserTransList({
    required this.typeName,
    required this.coin,
    required this.settleCoin,
    required this.addTime,
  });

  factory UserTransList.fromJson(Map<String, dynamic> json) {
    return UserTransList(
      typeName: json['typeName'],
      coin: json['coin'],
      settleCoin: json['settleCoin'],
      addTime: json['addTime'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      'typeName': typeName,
      'coin': coin,
      'settleCoin': settleCoin,
      'addTime': addTime,
    };
    return data;
  }
}
