class LhcFansModel {
  final String? uid;
  final String? nickname;
  final String? headImg;

  LhcFansModel({this.uid, this.nickname, this.headImg});

  // Factory method to create an instance from JSON
  factory LhcFansModel.fromJson(Map<String, dynamic> json) {
    return LhcFansModel(
      uid: json['uid'] as String?,
      nickname: json['nickname'] as String?,
      headImg: json['headImg'] as String?,
    );
  }

  // Method to convert the instance to JSON
  Map<String, dynamic> toJson() {
    return {
      'uid': uid,
      'nickname': nickname,
      'headImg': headImg,
    };
  }
}

class LhcFansListModel {
  final List<LhcFansModel>? list;
  final int? total;

  LhcFansListModel({
    this.list,
    this.total,
  });

  // Factory method to create an instance from JSON
  factory LhcFansListModel.fromJson(Map<String, dynamic> json) {
    return LhcFansListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((item) => LhcFansModel.fromJson(item as Map<String, dynamic>))
          .toList(),
      total: json['total'] as int?,
    );
  }

  // Method to convert the instance to JSON
  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((user) => user.toJson()).toList(),
      'total': total,
    };
  }
}
