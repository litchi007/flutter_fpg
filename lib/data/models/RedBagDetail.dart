class RedBagDetail {
  final String name;
  final String start;
  final String end;
  final String showTime;
  final String redBagLogo;
  final bool isHideAmount;
  final bool isHideCount;
  final String leftAmount;
  final String leftCount;
  final String id;
  final String intro;
  final String username;
  final bool hasLogin;
  final int attendedTimes;
  final int attendTimesLimit;
  final int canGet;

  RedBagDetail({
    required this.name,
    required this.start,
    required this.end,
    required this.showTime,
    required this.redBagLogo,
    required this.isHideAmount,
    required this.isHideCount,
    required this.leftAmount,
    required this.leftCount,
    required this.id,
    required this.intro,
    required this.username,
    required this.hasLogin,
    required this.attendedTimes,
    required this.attendTimesLimit,
    required this.canGet,
  });

  factory RedBagDetail.fromJson(Map<String, dynamic> json) {
    return RedBagDetail(
      name: json['name'],
      start: json['start'],
      end: json['end'],
      showTime: json['show_time'],
      redBagLogo: json['redBagLogo'],
      isHideAmount: json['isHideAmount'],
      isHideCount: json['isHideCount'],
      leftAmount: json['leftAmount'],
      leftCount: json['leftCount'],
      id: json['id'],
      intro: json['intro'],
      username: json['username'],
      hasLogin: json['hasLogin'],
      attendedTimes: json['attendedTimes'],
      attendTimesLimit: json['attendTimesLimit'],
      canGet: json['canGet'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'start': start,
      'end': end,
      'show_time': showTime,
      'redBagLogo': redBagLogo,
      'isHideAmount': isHideAmount,
      'isHideCount': isHideCount,
      'leftAmount': leftAmount,
      'leftCount': leftCount,
      'id': id,
      'intro': intro,
      'username': username,
      'hasLogin': hasLogin,
      'attendedTimes': attendedTimes,
      'attendTimesLimit': attendTimesLimit,
      'canGet': canGet,
    };
  }
}
