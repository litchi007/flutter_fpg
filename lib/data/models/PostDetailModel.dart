import 'package:dartx/dartx.dart';
import 'package:intl/intl.dart';

class PostDetailModel {
  String? id;
  String? uid;
  String? nickname;
  String? headImg;
  String? title;
  String? content;
  List<dynamic>? contentPic;
  String? periods;
  String? fullLhcno;
  String? isHot;
  String? createTime;
  dynamic replyCount;
  int? isLike;
  int? isFav;
  int? isFollow;
  String? likeNum;
  String? viewNum;
  int? favNum;
  String? price;
  int? isLhcdocVip;
  int? isAdmin;
  String? updateTime;
  int? hasPay;
  dynamic topAdPc;
  dynamic bottomAdPc;
  dynamic topAdWap;
  dynamic bottomAdWap;
  String? alias;
  String? baomaType;
  String? baomaId;
  bool? lanmuBaoma;
  String? issue;
  List<Vote>? vote;
  String? lhcdocWords;
  List<dynamic>? recommend;
  dynamic recommendTotal;
  List<dynamic>? plugins;

  PostDetailModel({
    this.id,
    this.uid,
    this.nickname,
    this.headImg,
    this.title,
    this.content,
    this.contentPic,
    this.periods,
    this.fullLhcno,
    this.isHot,
    this.createTime,
    this.replyCount,
    this.isLike,
    this.isFav,
    this.isFollow,
    this.likeNum,
    this.viewNum,
    this.favNum,
    this.price,
    this.isLhcdocVip,
    this.isAdmin,
    this.updateTime,
    this.hasPay,
    this.topAdPc,
    this.bottomAdPc,
    this.topAdWap,
    this.bottomAdWap,
    this.alias,
    this.baomaType,
    this.baomaId,
    this.lanmuBaoma,
    this.issue,
    this.vote,
    this.lhcdocWords,
    this.recommend,
    this.recommendTotal,
    this.plugins,
  });

  factory PostDetailModel.fromJson(Map<String, dynamic> json) {
    String? time = json['updateTime'] as String?;
    DateTime dateTime =
        DateTime.fromMillisecondsSinceEpoch(time!.toInt() * 1000);
    return PostDetailModel(
      id: json['id'] as String?,
      uid: json['uid'] as String?,
      nickname: json['nickname'] as String?,
      headImg: json['headImg'] as String?,
      title: json['title'] as String?,
      content: json['content'] as String?,
      contentPic: json['contentPic'] as List<dynamic>?,
      periods: json['periods'] as String?,
      fullLhcno: json['full_lhcno'] as String?,
      isHot: json['isHot'] as String?,
      createTime: json['createTime'] as String?,
      replyCount: json['replyCount'],
      isLike: json['isLike'] as int?,
      isFav: json['isFav'] as int?,
      isFollow: json['isFollow'] as int?,
      likeNum: json['likeNum'] as String?,
      viewNum: json['viewNum'] as String?,
      favNum: json['favNum'] as int?,
      price: json['price'] as String?,
      isLhcdocVip: json['isLhcdocVip'] as int?,
      isAdmin: json['isAdmin'] as int?,
      updateTime: DateFormat('yyyy-MM-dd HH:mm:ss').format(dateTime).toString(),
      hasPay: json['hasPay'] as int?,
      topAdPc: json['topAdPc'],
      bottomAdPc: json['bottomAdPc'],
      topAdWap: json['topAdWap'],
      bottomAdWap: json['bottomAdWap'],
      alias: json['alias'] as String?,
      baomaType: json['baomaType'] as String?,
      baomaId: json['baomaId'] as String?,
      lanmuBaoma: json['lanmuBaoma'] as bool?,
      issue: json['issue'] as String?,
      vote: (json['vote'] as List<dynamic>?)
          ?.map((e) => Vote.fromJson(e as Map<String, dynamic>))
          .toList(),
      lhcdocWords: json['lhcdocWords'] as String?,
      recommend: json['Recommend'] as List<dynamic>?,
      recommendTotal: json['RecommendTotal'] as dynamic,
      plugins: json['plugins'] as List<dynamic>?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'uid': uid,
      'nickname': nickname,
      'headImg': headImg,
      'title': title,
      'content': content,
      'contentPic': contentPic,
      'periods': periods,
      'full_lhcno': fullLhcno,
      'isHot': isHot,
      'createTime': createTime,
      'replyCount': replyCount,
      'isLike': isLike,
      'isFav': isFav,
      'isFollow': isFollow,
      'likeNum': likeNum,
      'viewNum': viewNum,
      'favNum': favNum,
      'price': price,
      'isLhcdocVip': isLhcdocVip,
      'isAdmin': isAdmin,
      'updateTime': updateTime,
      'hasPay': hasPay,
      'topAdPc': topAdPc,
      'bottomAdPc': bottomAdPc,
      'topAdWap': topAdWap,
      'bottomAdWap': bottomAdWap,
      'alias': alias,
      'baomaType': baomaType,
      'baomaId': baomaId,
      'lanmuBaoma': lanmuBaoma,
      'issue': issue,
      'vote': vote?.map((e) => e.toJson()).toList(),
      'lhcdocWords': lhcdocWords,
      'Recommend': recommend,
      'RecommendTotal': recommendTotal,
      'plugins': plugins,
    };
  }
}

class Vote {
  int? animalFlag;
  String? animal;
  dynamic num;
  dynamic percent;

  Vote({
    this.animalFlag,
    this.animal,
    this.num,
    this.percent,
  });

  factory Vote.fromJson(Map<String, dynamic> json) {
    return Vote(
      animalFlag: json['animalFlag'] as int?,
      animal: json['animal'] as String?,
      num: json['num'] as dynamic,
      percent: json['percent'] as dynamic,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'animalFlag': animalFlag,
      'animal': animal,
      'num': num,
      'percent': percent,
    };
  }
}
