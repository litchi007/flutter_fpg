import 'package:fpg_flutter/data/models/LotteryHistoryItemModel.dart';

class LotteryHistoryModel {
  List<LotteryHistoryItemModel> list;
  List<String> redBalls;
  List<String> greenBalls;
  List<String> blueBalls;

  LotteryHistoryModel({
    required this.list,
    required this.redBalls,
    required this.greenBalls,
    required this.blueBalls,
  });

  factory LotteryHistoryModel.fromJson(Map<String, dynamic> json) {
    return LotteryHistoryModel(
      list: (json['list'] as List)
          .map((item) => LotteryHistoryItemModel.fromJson(item))
          .toList(),
      redBalls: List<String>.from(json['redBalls']),
      greenBalls: List<String>.from(json['greenBalls']),
      blueBalls: List<String>.from(json['blueBalls']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list.map((item) => item.toJson()).toList(),
      'redBalls': redBalls,
      'greenBalls': greenBalls,
      'blueBalls': blueBalls,
    };
  }
}
