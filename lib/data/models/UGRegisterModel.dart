class UGRegisterModel {
  // sessid
  String? apiSid;
  // token
  String? apiToken;
  String? uid;
  String? usr;

  UGRegisterModel({this.apiSid, this.apiToken, this.uid, this.usr});

  factory UGRegisterModel.fromJson(Map<String, dynamic> json) {
    return UGRegisterModel(
      apiSid: json['API-SID'],
      apiToken: json['API-TOKEN'],
      uid: json['uid'],
      usr: json['usr'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'API-SID': apiSid,
      'API-TOKEN': apiToken,
      'uid': uid,
      'usr': usr,
    };
  }
}
