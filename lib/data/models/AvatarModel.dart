class AvatarModel {
  final String filename;
  final String url;

  AvatarModel({
    required this.filename,
    required this.url,
  });

  factory AvatarModel.fromJson(Map<String, dynamic> json) {
    return AvatarModel(
      filename: json['filename'],
      url: json['url'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'filename': filename,
      'url': url,
    };
  }
}
