class Mission {
  final String? id;
  final String? sortId;
  final String? missionName;
  final String? missionDesc;
  final String? integral;
  final String? missionType;
  final String? sorts;
  final String? status;
  final String? jumpTypeId;
  final DateTime overTime;
  final String? type;
  final String? missionLevelId;
  final String? rechargeAmount;
  final String? isAutoApply;
  final String? isAutoSettle;
  final String? chatRoomIds;
  final String? redbagType;
  final String? ipLimit;
  final String? sortName2;
  final String? dml;
  final String? isAdminRecharge;
  final String? sortName;
  final String? typeSort;
  final bool? name2Index;
  final AlgorithmData algorithmData;

  Mission({
    required this.id,
    required this.sortId,
    required this.missionName,
    required this.missionDesc,
    required this.integral,
    required this.missionType,
    required this.sorts,
    required this.status,
    required this.jumpTypeId,
    required this.overTime,
    required this.type,
    required this.missionLevelId,
    required this.rechargeAmount,
    required this.isAutoApply,
    required this.isAutoSettle,
    required this.chatRoomIds,
    required this.redbagType,
    required this.ipLimit,
    required this.sortName2,
    required this.dml,
    required this.isAdminRecharge,
    required this.sortName,
    required this.typeSort,
    this.name2Index,
    required this.algorithmData,
  });

  factory Mission.fromJson(Map<String, dynamic> json) {
    return Mission(
      id: json['id']?.toString(),
      sortId: json['sortId']?.toString(),
      missionName: json['missionName']?.toString(),
      missionDesc: json['missionDesc']?.toString(),
      integral: json['integral']?.toString(),
      missionType: json['missionType']?.toString(),
      sorts: json['sorts']?.toString(),
      status: json['status']?.toString(),
      jumpTypeId: json['jump_type_id']?.toString(),
      overTime: DateTime.parse(json['overTime']),
      type: json['type']?.toString(),
      missionLevelId: json['mission_level_id']?.toString(),
      rechargeAmount: json['rechargeAmount']?.toString(),
      isAutoApply: json['is_auto_apply']?.toString(),
      isAutoSettle: json['is_auto_settle']?.toString(),
      chatRoomIds: json['chat_room_ids']?.toString(),
      redbagType: json['redbagType']?.toString(),
      ipLimit: json['ip_limit']?.toString(),
      sortName2: json['sortName2']?.toString(),
      dml: json['dml']?.toString(),
      isAdminRecharge: json['is_admin_recharge']?.toString(),
      sortName: json['sortName']?.toString(),
      typeSort: json['typeSort']?.toString(),
      name2Index: json['name2Index'] == null
          ? null
          : json['name2Index'] is bool
              ? json['name2Index'] as bool
              : json['name2Index'] == 0
                  ? false
                  : true,
      algorithmData: AlgorithmData.fromJson(json['algorithmData']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'sortId': sortId,
      'missionName': missionName,
      'missionDesc': missionDesc,
      'integral': integral,
      'missionType': missionType,
      'sorts': sorts,
      'status': status,
      'jump_type_id': jumpTypeId,
      'overTime': overTime.toIso8601String(),
      'type': type,
      'mission_level_id': missionLevelId,
      'rechargeAmount': rechargeAmount,
      'is_auto_apply': isAutoApply,
      'is_auto_settle': isAutoSettle,
      'chat_room_ids': chatRoomIds,
      'redbagType': redbagType,
      'ip_limit': ipLimit,
      'sortName2': sortName2,
      'dml': dml,
      'is_admin_recharge': isAdminRecharge,
      'sortName': sortName,
      'typeSort': typeSort,
      'name2Index': name2Index,
      'algorithmData': algorithmData.toJson(),
    };
  }
}

class AlgorithmData {
  final String? now;
  final String? percent;

  AlgorithmData({
    required this.now,
    required this.percent,
  });

  factory AlgorithmData.fromJson(Map<String, dynamic> json) {
    return AlgorithmData(
      now: json['now']?.toString(),
      percent: json['percent']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'now': now,
      'percent': percent,
    };
  }
}

class MissionModel {
  final List<Mission> missions;
  final String? total;

  MissionModel({
    required this.missions,
    required this.total,
  });

  factory MissionModel.fromJson(Map<String, dynamic> json) {
    return MissionModel(
      missions: (json['list'] as List<dynamic>)
          .map((item) => Mission.fromJson(item as Map<String, dynamic>))
          .toList(),
      total: json['total']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': missions.map((mission) => mission.toJson()).toList(),
      'total': total,
    };
  }
}

class MissionCategory {
  final String? id;
  final String? sortName;

  MissionCategory({this.id, this.sortName});

  // Factory constructor to create a Task instance from JSON
  factory MissionCategory.fromJson(Map<String, dynamic> json) {
    return MissionCategory(
      id: json['id']?.toString(),
      sortName: json['sortName']?.toString(),
    );
  }

  // Convert Task instance to JSON
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'sortName': sortName,
    };
  }
}
