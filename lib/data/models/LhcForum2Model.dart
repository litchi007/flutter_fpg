import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/data/models/LhcdocCategoryModel.dart';
import 'package:fpg_flutter/data/models/ShowHomeListModel.dart';
import 'package:fpg_flutter/data/models/shequModel.dart';

class TopItem {
  String? cid;
  String? type;
  String? type2;
  String? title;
  String? sort;
  String? price;
  String? full_lhcno;
  String? alias;
  String? short_lhcno;
  String? baomaType;

  TopItem({
    this.alias,
    this.cid,
    this.full_lhcno,
    this.price,
    this.short_lhcno,
    this.sort,
    this.title,
    this.type,
    this.type2,
    this.baomaType,
  });

  factory TopItem.fromJson(Map<String, dynamic> json) {
    return TopItem(
      alias: json['alias'],
      cid: json['cid'],
      full_lhcno: json['full_lhcno'],
      price: json['price'],
      short_lhcno: json['short_lhcno'],
      sort: json['sort'],
      title: json['title'],
      type: json['type'],
      type2: json['type2'],
      baomaType: json['baoma_type'],
    );
  }
}

class LhcForum2Model {
  HomeGamesData? homeGames;
  ShequModel? shequ;
  List<TopItem>? topList;
  List<LhcdocCategoryModel>? categoryList;
  Map<String, List<ShowHomeListModel>>? contentShowHomeList;

  LhcForum2Model({
    this.categoryList,
    this.contentShowHomeList,
    this.homeGames,
    this.shequ,
    this.topList,
  });

  factory LhcForum2Model.fromJson(Map<String, dynamic> json) {
    return LhcForum2Model(
        categoryList: (json['categoryList'] as List?)
            ?.map((x) => LhcdocCategoryModel.fromJson(x))
            .toList(),
        contentShowHomeList:
            (json['contentShowHomeList'] as Map<String, dynamic>?)?.map(
          (key, value) => MapEntry(
            key,
            (value as List<dynamic>)
                .map((e) =>
                    ShowHomeListModel.fromJson(e as Map<String, dynamic>))
                .toList(),
          ),
        ),
        homeGames: HomeGamesData.fromJson(json['homeGames']),
        shequ: ShequModel.fromJson(json['shequ']),
        topList: (json['topList'] as List?)
            ?.map((x) => TopItem.fromJson(x))
            .toList());
  }

  Map<String, dynamic> toJson() {
    return {
      'homeGames': homeGames,
      'shequ': shequ,
      'topList': topList,
      'categoryList': categoryList,
      'contentShowHomeList': contentShowHomeList?.map(
        (key, value) => MapEntry(key, value.map((e) => e.toJson()).toList()),
      ),
    };
  }
}
