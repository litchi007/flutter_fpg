class RedEnvelopModel {
  String? id;
  String? enable;
  String? type;
  String? title;
  String? start;
  String? end;
  String? amount;
  String? minAmount;
  String? maxAmount;
  String? usedAmount;
  String? dmlEnable;
  String? dml;
  String? rechargeMinAmount;
  String? rechargeMethodIds;
  String? rechargeStart;
  String? rechargeEnd;
  String? lotteryDmlMinAmount;
  String? lotteryDmlGameIds;
  String? lotteryDmlStart;
  String? lotteryDmlEnd;
  String? realDmlMinAmount;
  String? realDmlGameIds;
  String? realDmlStart;
  String? realDmlEnd;
  String? regStart;
  String? regEnd;
  String? levelIds;
  String? info;
  String? isDelete;
  String? sort;
  String? createTime;
  String? updateTime;

  RedEnvelopModel({
    this.id,
    this.enable,
    this.type,
    this.title,
    this.start,
    this.end,
    this.amount,
    this.minAmount,
    this.maxAmount,
    this.usedAmount,
    this.dmlEnable,
    this.dml,
    this.rechargeMinAmount,
    this.rechargeMethodIds,
    this.rechargeStart,
    this.rechargeEnd,
    this.lotteryDmlMinAmount,
    this.lotteryDmlGameIds,
    this.lotteryDmlStart,
    this.lotteryDmlEnd,
    this.realDmlMinAmount,
    this.realDmlGameIds,
    this.realDmlStart,
    this.realDmlEnd,
    this.regStart,
    this.regEnd,
    this.levelIds,
    this.info,
    this.isDelete,
    this.sort,
    this.createTime,
    this.updateTime,
  });

  factory RedEnvelopModel.fromJson(Map<String, dynamic> json) {
    return RedEnvelopModel(
      id: json['id']?.toString(),
      enable: json['enable']?.toString(),
      type: json['type']?.toString(),
      title: json['title']?.toString(),
      start: json['start']?.toString(),
      end: json['end']?.toString(),
      amount: json['amount']?.toString(),
      minAmount: json['min_amount']?.toString(),
      maxAmount: json['max_amount']?.toString(),
      usedAmount: json['used_amount']?.toString(),
      dmlEnable: json['dml_enable']?.toString(),
      dml: json['dml']?.toString(),
      rechargeMinAmount: json['recharge_min_amount']?.toString(),
      rechargeMethodIds: json['recharge_method_ids']?.toString(),
      rechargeStart: json['recharge_start']?.toString(),
      rechargeEnd: json['recharge_end']?.toString(),
      lotteryDmlMinAmount: json['lottery_dml_min_amount']?.toString(),
      lotteryDmlGameIds: json['lottery_dml_game_ids']?.toString(),
      lotteryDmlStart: json['lottery_dml_start']?.toString(),
      lotteryDmlEnd: json['lottery_dml_end']?.toString(),
      realDmlMinAmount: json['real_dml_min_amount']?.toString(),
      realDmlGameIds: json['real_dml_game_ids']?.toString(),
      realDmlStart: json['real_dml_start']?.toString(),
      realDmlEnd: json['real_dml_end']?.toString(),
      regStart: json['reg_start']?.toString(),
      regEnd: json['reg_end']?.toString(),
      levelIds: json['level_ids']?.toString(),
      info: json['info']?.toString(),
      isDelete: json['isDelete']?.toString(),
      sort: json['sort']?.toString(),
      createTime: json['create_time']?.toString(),
      updateTime: json['update_time']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'enable': enable,
      'type': type,
      'title': title,
      'start': start,
      'end': end,
      'amount': amount,
      'min_amount': minAmount,
      'max_amount': maxAmount,
      'used_amount': usedAmount,
      'dml_enable': dmlEnable,
      'dml': dml,
      'recharge_min_amount': rechargeMinAmount,
      'recharge_method_ids': rechargeMethodIds,
      'recharge_start': rechargeStart,
      'recharge_end': rechargeEnd,
      'lottery_dml_min_amount': lotteryDmlMinAmount,
      'lottery_dml_game_ids': lotteryDmlGameIds,
      'lottery_dml_start': lotteryDmlStart,
      'lottery_dml_end': lotteryDmlEnd,
      'real_dml_min_amount': realDmlMinAmount,
      'real_dml_game_ids': realDmlGameIds,
      'real_dml_start': realDmlStart,
      'real_dml_end': realDmlEnd,
      'reg_start': regStart,
      'reg_end': regEnd,
      'level_ids': levelIds,
      'info': info,
      'isDelete': isDelete,
      'sort': sort,
      'create_time': createTime,
      'update_time': updateTime,
    };
  }
}

class RedEnvelopListModel {
  List<RedEnvelopModel>? list;
  String? total;

  RedEnvelopListModel({
    this.list,
    this.total,
  });

  factory RedEnvelopListModel.fromJson(Map<String, dynamic> json) {
    return RedEnvelopListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map(
              (item) => RedEnvelopModel.fromJson(item as Map<String, dynamic>))
          .toList(),
      total: json['total']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((item) => item.toJson()).toList(),
      'total': total,
    };
  }
}
