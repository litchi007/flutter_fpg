class RedpackedLogmodel {
  final String? id;
  final String? redPacketId;
  final String? uid;
  final String? username;
  final String? levelId;
  final String? redPacketType;
  final String? amount;
  final String? createTime;
  final String? title;

  RedpackedLogmodel({
    this.id,
    this.redPacketId,
    this.uid,
    this.username,
    this.levelId,
    this.redPacketType,
    this.amount,
    this.createTime,
    this.title,
  });

  factory RedpackedLogmodel.fromJson(Map<String, dynamic> json) {
    return RedpackedLogmodel(
      id: json['id'] as String?,
      redPacketId: json['red_packet_id'] as String?,
      uid: json['uid'] as String?,
      username: json['username'] as String?,
      levelId: json['level_id'] as String?,
      redPacketType: json['red_packet_type'] as String?,
      amount: json['amount'] as String?,
      createTime: json['createTime'] as String?,
      title: json['title'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'red_packet_id': redPacketId,
      'uid': uid,
      'username': username,
      'level_id': levelId,
      'red_packet_type': redPacketType,
      'amount': amount,
      'createTime': createTime,
      'title': title,
    };
  }
}
