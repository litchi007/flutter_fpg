class UGinviteInfoModel {
  String? username;
  String? rid;
  String? uid;
  String? linkI;
  String? linkR;
  String? fandianIntro;
  String? realFandianIntro;
  String? gameFandianIntro;
  String? fishFandianIntro;
  String? cardFandianIntro;
  String? esportFandianIntro;
  String? sportFandianIntro;
  String? fandian;
  String? monthEarn;
  String? monthRealEarn;
  String? allEarn;
  String? monthMember;
  String? totalMember;
  String? myrecoImg;
  String? agentProfitInfo;

  String? title;
  String? content;
  bool? isPress = false;

  UGinviteInfoModel({
    this.username,
    this.rid,
    this.uid,
    this.linkI,
    this.linkR,
    this.fandianIntro,
    this.realFandianIntro,
    this.gameFandianIntro,
    this.fishFandianIntro,
    this.cardFandianIntro,
    this.esportFandianIntro,
    this.sportFandianIntro,
    this.fandian,
    this.monthEarn,
    this.monthRealEarn,
    this.allEarn,
    this.monthMember,
    this.totalMember,
    this.myrecoImg,
    this.agentProfitInfo,
    this.title,
    this.content,
    this.isPress,
  });

  factory UGinviteInfoModel.fromJson(Map<String, dynamic> json) {
    return UGinviteInfoModel(
      username: json['username']?.toString(),
      rid: json['rid']?.toString(),
      uid: json['uid']?.toString(),
      linkI: json['link_i']?.toString(),
      linkR: json['link_r']?.toString(),
      fandianIntro: json['fandian_intro']?.toString(),
      realFandianIntro: json['real_fandian_intro']?.toString(),
      gameFandianIntro: json['game_fandian_intro']?.toString(),
      fishFandianIntro: json['fish_fandian_intro']?.toString(),
      cardFandianIntro: json['card_fandian_intro']?.toString(),
      esportFandianIntro: json['esport_fandian_intro']?.toString(),
      sportFandianIntro: json['sport_fandian_intro']?.toString(),
      fandian: json['fandian']?.toString(),
      monthEarn: json['month_earn']?.toString(),
      monthRealEarn: json['month_real_earn']?.toString(),
      allEarn: json['all_earn']?.toString(),
      monthMember: json['month_member']?.toString(),
      totalMember: json['total_member']?.toString(),
      myrecoImg: json['myreco_img']?.toString(),
      agentProfitInfo: json['agentProfitInfo']?.toString(),
      isPress: false,
      title: '',
      content: '',
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'rid': rid,
      'uid': uid,
      'link_i': linkI,
      'link_r': linkR,
      'fandian_intro': fandianIntro,
      'real_fandian_intro': realFandianIntro,
      'game_fandian_intro': gameFandianIntro,
      'fish_fandian_intro': fishFandianIntro,
      'card_fandian_intro': cardFandianIntro,
      'esport_fandian_intro': esportFandianIntro,
      'sport_fandian_intro': sportFandianIntro,
      'fandian': fandian,
      'month_earn': monthEarn,
      'month_real_earn': monthRealEarn,
      'all_earn': allEarn,
      'month_member': monthMember,
      'total_member': totalMember,
      'myreco_img': myrecoImg,
      'agentProfitInfo': agentProfitInfo,
      'isPress': isPress,
      'title': title,
      'content': content,
    };
  }
}

// Dart equivalent for List class
class ListModel {
  String? status;
  String? uid;
  String? userTypeTxt;
  String? id;
  int? usedNum;
  String? userType;
  String? inviteCode;
  String? createdTime;
  String? url;

  ListModel({
    this.status,
    this.uid,
    this.userTypeTxt,
    this.id,
    this.usedNum,
    this.userType,
    this.inviteCode,
    this.createdTime,
    this.url,
  });

  // Factory method to create a ListModel object from JSON
  factory ListModel.fromJson(Map<String, dynamic> json) {
    return ListModel(
      status: json['status'],
      uid: json['uid'],
      userTypeTxt: json['user_type_txt'],
      id: json['id'],
      usedNum: json['used_num'],
      userType: json['user_type'],
      inviteCode: json['invite_code'],
      createdTime: json['created_time'],
      url: json['url'],
    );
  }
}

// Dart equivalent for Data class
class InviteCodeList {
  int? total;
  List<ListModel>? list;

  InviteCodeList({
    this.total,
    this.list,
  });

  // Factory method to create a Data object from JSON
  factory InviteCodeList.fromJson(Map<String, dynamic> json) {
    var list = json['list'] as List;
    List<ListModel> dataList =
        list.map((item) => ListModel.fromJson(item)).toList();

    return InviteCodeList(
      total: json['total'],
      list: dataList,
    );
  }
}

class Games {
  String? isSeal;
  String? isClose;
  String? isInstant;
  String? id;
  String? isHot;
  String? title;
  String? gameTypeName;
  String? pic;
  String? gameType;
  String? name;
  int? supportTrial;

  Games({
    this.isSeal,
    this.isClose,
    this.isInstant,
    this.id,
    this.isHot,
    this.title,
    this.gameTypeName,
    this.pic,
    this.gameType,
    this.name,
    this.supportTrial,
  });

  // Factory method to convert JSON into Games object
  factory Games.fromJson(Map<String, dynamic> json) => Games(
        isSeal: json['isSeal'],
        isClose: json['isClose'],
        isInstant: json['isInstant'],
        id: json['id'],
        isHot: json['isHot'],
        title: json['title'],
        gameTypeName: json['gameTypeName'],
        pic: json['pic'],
        gameType: json['gameType'],
        name: json['name'],
        supportTrial: json['supportTrial'],
      );
}

// Dart equivalent for Data class
class Data_ {
  List<Games>? games;
  String? category;
  String? categoryName;

  Data_({
    this.games,
    this.category,
    this.categoryName,
  });

  // Factory method to convert JSON into Data object
  factory Data_.fromJson(Map<String, dynamic> json) => Data_(
        games: List<Games>.from(json['games'].map((x) => Games.fromJson(x))),
        category: json['category'],
        categoryName: json['categoryName'],
      );
}

class BetStat_Data {
  String? zjSum;
  int? betMember;
  String? winAmount;
  int? level;
  String? date;
  String? betSum;
  int? betCount;
  int? type;
  String? fandianSum;
  String? sunyi;

  BetStat_Data(
      {this.zjSum,
      this.betMember,
      this.winAmount,
      this.level,
      this.date,
      this.betSum,
      this.betCount,
      this.type,
      this.fandianSum,
      this.sunyi});

  // Factory method to create ListItem object from JSON
  factory BetStat_Data.fromJson(Map<String, dynamic> json) {
    return BetStat_Data(
      zjSum: json['zj_sum'],
      betMember: json['bet_member'],
      winAmount: json['win_amount'],
      level: json['level'],
      date: json['date'],
      betSum: json['bet_sum'],
      betCount: json['bet_count'],
      type: json['type'],
      fandianSum: json['fandian_sum']?.toString(),
      sunyi: json['sunyi']?.toString(),
    );
  }
}

// Dart model class for Data
class M_BetStat {
  int? total;
  String? total_bet_sum;
  String? total_fadian_sum;
  String? total_sunyi;

  List<BetStat_Data>? list;

  M_BetStat({
    this.total,
    this.list,
    this.total_bet_sum,
    this.total_fadian_sum,
    this.total_sunyi,
  });

  // Factory method to create Data object from JSON
  factory M_BetStat.fromJson(Map<String, dynamic> json) {
    return M_BetStat(
      total: json['total'],
      total_bet_sum: json['total_bet_sum']?.toString(),
      total_fadian_sum: json['total_fadian_sum']?.toString(),
      total_sunyi: json['total_sunyi']?.toString(),
      list: (json['list'] as List<dynamic>)
          .map((item) => BetStat_Data.fromJson(item))
          .toList(),
    );
  }
}

class M_RealBetData_list {
  String? date;
  String? username;
  String? validBetAmount;
  String? betAmount;
  String? netAmount;
  int? level;
  String? comNetAmount;
  String? platform;
  String? betSum;
  String? winAmount;

  M_RealBetData_list({
    this.date,
    this.username,
    this.validBetAmount,
    this.netAmount,
    this.level,
    this.comNetAmount,
    this.platform,
    this.betSum,
    this.winAmount,
  });

  // Factory method to convert JSON into a List object
  factory M_RealBetData_list.fromJson(Map<String, dynamic> json) {
    return M_RealBetData_list(
      date: json['date']?.toString(),
      username: json['username']?.toString(),
      validBetAmount:
          json['validBetAmount']?.toString(), // Adjusted to match JSON key
      netAmount: json['netAmount']?.toString(),
      level: json['level'] as int,
      comNetAmount: json['comNetAmount']?.toString(),
      platform: json['platform']?.toString(),
      betSum: json['bet_sum']?.toString(),
      winAmount: json['win_amount']?.toString(),
    );
  }
  // Method to convert the instance into JSON map
}

class M_InviteDomainList {
  int? total;
  List<M_Domain_list>? list;

  M_InviteDomainList({
    this.total,
    this.list,
  });

  // Factory method to convert JSON into a Data object
  factory M_InviteDomainList.fromJson(Map<String, dynamic> json) {
    return M_InviteDomainList(
      total: json['total'] as int,
      list: (json['list'] as List)
          .map((item) => M_Domain_list.fromJson(item))
          .toList(),
    );
  }
}

class M_Domain_list {
  List<String>? domain;

  M_Domain_list({
    this.domain,
  });

  // Factory method to convert JSON into a Data object
  factory M_Domain_list.fromJson(Map<String, dynamic> json) {
    return M_Domain_list(
      domain: (json['domain'] as List?)?.map((e) => e.toString()).toList(),
    );
  }
}

class M_RealBetList {
  int? total;
  List<M_RealBetData_list>? list;

  M_RealBetList({
    this.total,
    this.list,
  });

  // Factory method to convert JSON into a Data object
  factory M_RealBetList.fromJson(Map<String, dynamic> json) {
    return M_RealBetList(
      total: json['total'] as int,
      list: (json['list'] as List)
          .map((item) => M_RealBetData_list.fromJson(item))
          .toList(),
    );
  }
}

class M_BetData_list {
  String? lotteryNo;
  String? money;
  String? groupName;
  String? odds;
  String? lotteryName;
  int? level;
  String? actionData;
  String? date;
  String? actionNo;
  String? type;
  String? username;
  String? bonus;

  M_BetData_list({
    this.lotteryNo,
    this.money,
    this.groupName,
    this.odds,
    this.lotteryName,
    this.level,
    this.actionData,
    this.date,
    this.actionNo,
    this.type,
    this.username,
    this.bonus,
  });

  // Factory method to convert JSON into a List object
  factory M_BetData_list.fromJson(Map<String, dynamic> json) {
    return M_BetData_list(
      lotteryNo: json['lotteryNo']?.toString(),
      money: json['money']?.toString(),
      groupName: json['Groupname']?.toString(), // Adjusted to match JSON key
      odds: json['odds']?.toString(),
      lotteryName: json['lottery_name']?.toString(),
      level: json['level'] as int,
      actionData: json['actionData']?.toString(),
      date: json['date']?.toString(),
      actionNo: json['actionNo']?.toString(),
      type: json['type']?.toString(),
      username: json['username']?.toString(),
      bonus: json['bonus']?.toString(),
    );
  }
  // Method to convert the instance into JSON map
  Map<String, dynamic> toJson() {
    return {
      'lotteryNo': lotteryNo,
      'money': money,
      'Groupname': groupName, // Adjusted to match JSON key
      'odds': odds,
      'lottery_name': lotteryName,
      'level': level,
      'actionData': actionData,
      'date': date,
      'actionNo': actionNo,
      'type': type,
      'username': username,
      'bonus': bonus,
    };
  }
}

class M_BetList {
  int? total;
  List<M_BetData_list>? list;

  M_BetList({
    this.total,
    this.list,
  });

  // Factory method to convert JSON into a Data object
  factory M_BetList.fromJson(Map<String, dynamic> json) {
    return M_BetList(
      total: json['total'] as int,
      list: (json['list'] as List)
          .map((item) => M_BetData_list.fromJson(item))
          .toList(),
    );
  }
}

class RealBetStat_Data {
  String? date;
  int? level;
  int? betCount;
  String? validBetAmount;
  String? betSum;
  String? netAmount;
  String? fandianSum;
  String? betMember;

  RealBetStat_Data(
      {this.date,
      this.level,
      this.betCount,
      this.validBetAmount,
      this.betSum,
      this.netAmount,
      this.fandianSum,
      this.betMember});
  factory RealBetStat_Data.fromJson(Map<String, dynamic> json) {
    return RealBetStat_Data(
      date: json['date'],
      level: json['level'],
      betCount: json['bet_count'],
      validBetAmount: json['validBetAmount']?.toString(),
      betSum: json['bet_sum']?.toString(),
      netAmount: json['netAmount']?.toString(),
      fandianSum: json['fandian_sum']?.toString(),
      betMember: json['bet_member']?.toString(),
    );
  }
}

class RealBetStat_Data2 {
  String? zjSum;
  int? betMember;
  String? winAmount;
  int? level;
  String? date;
  String? betSum;
  int? betCount;
  int? type;
  String? fandianSum;

  RealBetStat_Data2({
    this.zjSum,
    this.betMember,
    this.winAmount,
    this.level,
    this.date,
    this.betSum,
    this.betCount,
    this.type,
    this.fandianSum,
  });

  // Factory method to create ListItem object from JSON
  factory RealBetStat_Data2.fromJson(Map<String, dynamic> json) {
    return RealBetStat_Data2(
      zjSum: json['zj_sum'],
      betMember: json['bet_member'],
      winAmount: json['win_amount'],
      level: json['level'],
      date: json['date'],
      betSum: json['bet_sum'],
      betCount: json['bet_count'],
      type: json['type'],
      fandianSum: json['fandian_sum'],
    );
  }
}

// Dart model class for Data
class M_RealBetStat {
  int? total;
  String? total_bet_sum;
  String? total_fadian_sum;
  String? total_sunyi;

  List<RealBetStat_Data>? list;

  M_RealBetStat({
    this.total,
    this.list,
    this.total_bet_sum,
    this.total_fadian_sum,
    this.total_sunyi,
  });

  // Factory method to create Data object from JSON
  factory M_RealBetStat.fromJson(Map<String, dynamic> json) {
    return M_RealBetStat(
      total: json['total'],
      total_bet_sum: json['total_bet_sum'],
      total_fadian_sum: json['total_fadian_sum'],
      total_sunyi: json['total_sunyi']?.toString(),
      list: (json['list'] as List<dynamic>)
          .map((item) => RealBetStat_Data.fromJson(item))
          .toList(),
    );
  }
}

class M_DepositStat_List {
  String? amount;
  int? member;
  int? level;
  String? date;

  M_DepositStat_List({
    this.amount,
    this.member,
    this.level,
    this.date,
  });

  // Factory method to convert JSON into a List object
  factory M_DepositStat_List.fromJson(Map<String, dynamic> json) {
    return M_DepositStat_List(
      amount: json['amount'],
      member: json['member'] as int,
      level: json['level'] as int,
      date: json['date'],
    );
  }
}

class M_DepositStatData {
  int? total;
  List<M_DepositStat_List>? list;

  M_DepositStatData({
    this.total,
    this.list,
  });

  // Factory method to convert JSON into a Data object
  factory M_DepositStatData.fromJson(Map<String, dynamic> json) {
    return M_DepositStatData(
      total: json['total'] as int,
      list: (json['list'] as List)
          .map((item) => M_DepositStat_List.fromJson(item))
          .toList(),
    );
  }
}

class M_DepositListItem {
  String? amount;
  String? username;
  int? level;
  String? date;

  M_DepositListItem({
    this.amount,
    this.username,
    this.level,
    this.date,
  });

  // Factory method to convert JSON into a List object
  factory M_DepositListItem.fromJson(Map<String, dynamic> json) {
    return M_DepositListItem(
      amount: json['amount'],
      username: json['username'],
      level: json['level'] as int,
      date: json['date'],
    );
  }
}

class M_DepositListtData {
  String? total;
  List<M_DepositListItem>? list;

  M_DepositListtData({
    this.total,
    this.list,
  });

  // Factory method to convert JSON into a Data object
  factory M_DepositListtData.fromJson(Map<String, dynamic> json) {
    return M_DepositListtData(
      total: json['total'],
      list: (json['list'] as List)
          .map((item) => M_DepositListItem.fromJson(item))
          .toList(),
    );
  }
}

class M_WithdrawListItem {
  String? amount;
  String? username;
  int? level;
  String? date;

  M_WithdrawListItem({
    this.amount,
    this.username,
    this.level,
    this.date,
  });

  // Factory method to convert JSON into a List object
  factory M_WithdrawListItem.fromJson(Map<String, dynamic> json) {
    return M_WithdrawListItem(
      amount: json['amount'],
      username: json['username'],
      level: json['level'] as int,
      date: json['date'],
    );
  }
}

class M_WithdrawListData {
  int? total;
  List<M_WithdrawListItem>? list;

  M_WithdrawListData({
    this.total,
    this.list,
  });

  // Factory method to convert JSON into a Data object
  factory M_WithdrawListData.fromJson(Map<String, dynamic> json) {
    return M_WithdrawListData(
      total: json['total'] as int,
      list: (json['list'] as List)
          .map((item) => M_WithdrawListItem.fromJson(item))
          .toList(),
    );
  }
}

class M_WithdrawStat_List {
  String? amount;
  int? member;
  int? level;
  String? date;

  M_WithdrawStat_List({
    this.amount,
    this.member,
    this.level,
    this.date,
  });

  // Factory method to convert JSON into a List object
  factory M_WithdrawStat_List.fromJson(Map<String, dynamic> json) {
    return M_WithdrawStat_List(
      amount: json['amount'],
      member: json['member'] as int,
      level: json['level'] as int,
      date: json['date'],
    );
  }
}

class M_WithdrawStatData {
  int? total;
  List<M_WithdrawStat_List>? list;

  M_WithdrawStatData({
    this.total,
    this.list,
  });

  // Factory method to convert JSON into a Data object
  factory M_WithdrawStatData.fromJson(Map<String, dynamic> json) {
    return M_WithdrawStatData(
      total: json['total'] as int,
      list: (json['list'] as List)
          .map((item) => M_WithdrawStat_List.fromJson(item))
          .toList(),
    );
  }
}

class Config {
  bool? enable;
  String? defaultLanguage;
  List<String>? supportLanguages;
  String? defaultLanguageAppend;
  bool? allowMemberModifyLanguage;

  Config({
    this.enable,
    this.defaultLanguage,
    this.supportLanguages,
    this.defaultLanguageAppend,
    this.allowMemberModifyLanguage,
  });

  factory Config.fromJson(Map<String, dynamic> json) {
    return Config(
      enable: json['enable'] as bool,
      defaultLanguage: json['defaultLanguage']?.toString(),
      supportLanguages: (json['supportLanguages'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      defaultLanguageAppend: json['defaultLanguageAppend']?.toString(),
      allowMemberModifyLanguage: json['allowMemberModifyLanguage'] as bool,
    );
  }
}

class SupportLanguageAppend {
  String? languageCode;
  List<String>? appendCodes;

  SupportLanguageAppend({
    this.languageCode,
    this.appendCodes,
  });

  factory SupportLanguageAppend.fromJson(Map<String, dynamic> json) {
    return SupportLanguageAppend(
      languageCode: json['languageCode']?.toString(),
      appendCodes: (json['appendCodes'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );
  }
}

class SupportLanguagesMap {
  String? code;
  String? name;

  SupportLanguagesMap({
    this.code,
    this.name,
  });

  factory SupportLanguagesMap.fromJson(Map<String, dynamic> json) {
    return SupportLanguagesMap(
      code: json['code']?.toString(),
      name: json['name']?.toString(),
    );
  }
}

class LanguageData {
  Config? config;
  int? packageLastVersion;
  String? currentLanguageCode;
  String? currentLanguageCodeAppend;
  List<SupportLanguageAppend>? supportLanguageAppends;
  List<SupportLanguagesMap>? supportLanguagesMap;

  LanguageData({
    this.config,
    this.packageLastVersion,
    this.currentLanguageCode,
    this.currentLanguageCodeAppend,
    this.supportLanguageAppends,
    this.supportLanguagesMap,
  });

  factory LanguageData.fromJson(Map<String, dynamic> json) {
    return LanguageData(
      config: Config.fromJson(json['config']),
      packageLastVersion: json['packageLastVersion'] as int,
      currentLanguageCode: json['currentLanguageCode']?.toString(),
      currentLanguageCodeAppend: json['currentLanguageCodeAppend']?.toString(),
      supportLanguageAppends: (json['supportLanguageAppends'] as List<dynamic>)
          .map((e) => SupportLanguageAppend.fromJson(e))
          .toList(),
      supportLanguagesMap: (json['supportLanguagesMap'] as List<dynamic>)
          .map((e) => SupportLanguagesMap.fromJson(e))
          .toList(),
    );
  }
}

// Define the Dart model for the list item with nullable fields
class InviteListItem {
  final String? uid;
  final int? level;
  final String? username;
  final int? is_online;
  final String? name;
  final String? coin;
  final String? enable;
  final String? accessTime;
  final String? regtime;
  final String? sunyi;
  final String? is_setting;

  InviteListItem({
    this.uid,
    this.level,
    this.username,
    this.is_online,
    this.name,
    this.coin,
    this.enable,
    this.accessTime,
    this.regtime,
    this.sunyi,
    this.is_setting,
  });

  factory InviteListItem.fromJson(Map<String, dynamic> json) {
    return InviteListItem(
      uid: json['uid']?.toString(),
      level: json['level'] as int?,
      username: json['username']?.toString(),
      is_online: json['is_online'] as int?,
      name: json['name']?.toString(),
      coin: json['coin']?.toString(),
      enable: json['enable']?.toString(),
      accessTime: json['accessTime']?.toString(),
      regtime: json['regtime']?.toString(),
      sunyi: json['sunyi']?.toString(),
      is_setting: json['is_setting']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uid': uid,
      'level': level,
      'username': username,
      'is_online': is_online,
      'name': name,
      'coin': coin,
      'enable': enable,
      'accessTime': accessTime,
      'regtime': regtime,
      'sunyi': sunyi,
      'is_setting': is_setting,
    };
  }
}

// Define the Dart model for the entire JSON structure with nullable fields
class InviteListModel {
  final List<InviteListItem>? list;
  final String? total;
  final String? total_people_sunyi;

  InviteListModel({
    this.list,
    this.total,
    this.total_people_sunyi,
  });

  factory InviteListModel.fromJson(Map<String, dynamic> json) {
    return InviteListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((item) => InviteListItem.fromJson(item as Map<String, dynamic>))
          .toList(),
      total: json['total']?.toString(),
      total_people_sunyi: json['total_people_sunyi']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((user) => user.toJson()).toList(),
      'total': total,
      'total_people_sunyi': total_people_sunyi,
    };
  }
}
