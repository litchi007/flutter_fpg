class PlayDataResult {
  final String? numColor; // 'red,red,blue,red,blue,red,green'
  final String? numWx; // '火,土,金,金,木,土,金'
  final String? zodiacs; // '马,马,牛,虎,兔,鸡,猴'

  PlayDataResult({
    this.numColor,
    this.numWx,
    this.zodiacs,
  });

  // Factory constructor to create a PlayDataResult from a map
  factory PlayDataResult.fromJson(Map<String, dynamic> json) {
    return PlayDataResult(
      numColor: json['numColor'] as String?,
      numWx: json['numWx'] as String?,
      zodiacs: json['zodiacs'] as String?,
    );
  }

  // Method to convert PlayDataResult to a map
  Map<String, dynamic> toJson() {
    return {
      'numColor': numColor,
      'numWx': numWx,
      'zodiacs': zodiacs,
    };
  }
}

class PlayData {
  final String? issue; // '2021003'
  final String? displayNumber; // '2021003'
  final String? openTime; // "2021-01-08 21:30:00"
  final String? num; // "33,29,34,32,19,05,43"
  final String? gameType; // 'lhc'
  final PlayDataResult? result;

  PlayData({
    this.issue,
    this.displayNumber,
    this.openTime,
    this.num,
    this.gameType,
    this.result,
  });

  // Factory constructor to create a PlayData from a map
  factory PlayData.fromJson(Map<String, dynamic> json) {
    return PlayData(
      issue: json['issue'] as String?,
      displayNumber: json['displayNumber'] as String?,
      openTime: json['openTime'] as String?,
      num: json['num'] as String?,
      gameType: json['gameType'] as String?,
      result: json['result'] != null
          ? PlayDataResult.fromJson(json['result'] as Map<String, dynamic>)
          : null,
    );
  }

  // Method to convert PlayData to a map
  Map<String, dynamic> toJson() {
    return {
      'issue': issue,
      'displayNumber': displayNumber,
      'openTime': openTime,
      'num': num,
      'gameType': gameType,
      'result': result?.toJson(),
    };
  }
}

class LotteryHistoryData {
  final List<PlayData>? list;
  final List<int>? redBalls;
  final List<int>? greenBalls;
  final List<int>? blueBalls;

  LotteryHistoryData({
    this.list,
    this.redBalls,
    this.greenBalls,
    this.blueBalls,
  });

  // Factory constructor to create a LotteryHistoryData from a map
  factory LotteryHistoryData.fromJson(Map<String, dynamic> json) {
    return LotteryHistoryData(
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => PlayData.fromJson(e as Map<String, dynamic>))
          .toList(),
      redBalls:
          (json['redBalls'] as List<dynamic>?)?.map((e) => e as int).toList(),
      greenBalls:
          (json['greenBalls'] as List<dynamic>?)?.map((e) => e as int).toList(),
      blueBalls:
          (json['blueBalls'] as List<dynamic>?)?.map((e) => e as int).toList(),
    );
  }

  // Method to convert LotteryHistoryData to a map
  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((e) => e.toJson()).toList(),
      'redBalls': redBalls,
      'greenBalls': greenBalls,
      'blueBalls': blueBalls,
    };
  }
}
