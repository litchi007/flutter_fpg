enum LotteryAdvType {
  InterestTreasure, // 利息宝
  TaskCenter, // 任务中心
  Lottery, // 彩票
}

class NextIssueData {
  String? id; // lh
  String? title; // 六合彩系列
  String? logo; // 彩种图标
  String? gameType; // 六合彩系列
  String? isSeal; // 是否封盘:1=是，0=否
  String? lowFreq; // 是否 低频彩，1 是
  String? serverTime;
  String? curIssue; // 当前第几期
  String? displayNumber; // 显示的期号
  String? curOpenTime;
  String? curCloseTime;
  String? preIsOpen;
  String? preIssue; // 2101030051
  String? preDisplayNumber; // 显示的期号 2101030051
  String? preOpenTime; // 2021-01-03 16:40:00
  String? preNum; // 29,27,02,08,12,18,35
  String? preResult; // 猴,狗,猪,蛇,牛,羊,虎
  String? adEnable;
  String? adPic;
  LotteryAdvType? adLink; // LotteryAdvType enum
  String? adGameType;
  String? adLinkType;
  String? isClose;
  String? isInstant; // 是否是即开彩：1=是，0=否
  String? serverTimestamp;
  List<ZodiacNumsData>? zodiacNums;
  List<ZodiacNumsData>? fiveElements;
  List<String>? winningPlayers;
  String? betTemplate; // BetTemplate 0经典   1 水果 2飞机 3球类
  String? hash;
  String? verifUrl;

  String? d0; // 越南彩开奖记录
  String? d1; // 越南彩开奖记录
  String? d2; // 越南彩开奖记录
  String? d3; // 越南彩开奖记录
  String? d4; // 越南彩开奖记录
  String? d5; // 越南彩开奖记录
  String? d6; // 越南彩开奖记录
  String? d7; // 越南彩开奖记录
  String? d8; // 越南彩开奖记录

  String? t0; // 越南彩开奖记录
  String? t1; // 越南彩开奖记录
  String? t2; // 越南彩开奖记录
  String? t3; // 越南彩开奖记录
  String? t4; // 越南彩开奖记录
  String? t5; // 越南彩开奖记录
  String? t6; // 越南彩开奖记录
  String? t7; // 越南彩开奖记录
  String? t8; // 越南彩开奖记录
  String? t9; // 越南彩开奖记录

  // Constructor
  NextIssueData({
    this.id,
    this.title,
    this.logo,
    this.gameType,
    this.isSeal,
    this.lowFreq,
    this.serverTime,
    this.curIssue,
    this.displayNumber,
    this.curOpenTime,
    this.curCloseTime,
    this.preIsOpen,
    this.preIssue,
    this.preDisplayNumber,
    this.preOpenTime,
    this.preNum,
    this.preResult,
    this.adEnable,
    this.adPic,
    this.adLink,
    this.adGameType,
    this.adLinkType,
    this.isClose,
    this.isInstant,
    this.serverTimestamp,
    this.zodiacNums,
    this.fiveElements,
    this.winningPlayers,
    this.betTemplate,
    this.hash,
    this.verifUrl,
    // Vietnamese color records
    this.d0,
    this.d1,
    this.d2,
    this.d3,
    this.d4,
    this.d5,
    this.d6,
    this.d7,
    this.d8,
    this.t0,
    this.t1,
    this.t2,
    this.t3,
    this.t4,
    this.t5,
    this.t6,
    this.t7,
    this.t8,
    this.t9,
  });

  // Factory method to create an instance from JSON
  factory NextIssueData.fromJson(Map<String, dynamic> json) {
    return NextIssueData(
      id: json['id'] as String?,
      title: json['title'] as String?,
      logo: json['logo'] as String?,
      gameType: json['gameType'] as String?,
      isSeal: json['isSeal'] as String?,
      lowFreq: json['lowFreq'] as String?,
      serverTime: json['serverTime'] as String?,
      curIssue: json['curIssue'] as String?,
      displayNumber: json['displayNumber'] as String?,
      curOpenTime: json['curOpenTime'] as String?,
      curCloseTime: json['curCloseTime'] as String?,
      preIsOpen: json['preIsOpen'] as String?,
      preIssue: json['preIssue'] as String?,
      preDisplayNumber: json['preDisplayNumber'] as String?,
      preOpenTime: json['preOpenTime'] as String?,
      preNum: json['preNum'] as String?,
      preResult: json['preResult'] as String?,
      adEnable: json['adEnable'] as String?,
      adPic: json['adPic'] as String?,
      adLink: json['adLink'] != null
          ? LotteryAdvType.values[json['adLink'] as int]
          : null,
      adGameType: json['adGameType'] as String?,
      adLinkType: json['adLinkType'] as String?,
      isClose: json['isClose'] as String?,
      isInstant: json['isInstant'] as String?,
      serverTimestamp: json['serverTimestamp'] as String?,
      zodiacNums: (json['zodiacNums'] as List<dynamic>?)
          ?.map((e) => ZodiacNumsData.fromJson(e as Map<String, dynamic>))
          .toList(),
      fiveElements: (json['fiveElements'] as List<dynamic>?)
          ?.map((e) => ZodiacNumsData.fromJson(e as Map<String, dynamic>))
          .toList(),
      winningPlayers: (json['winningPlayers'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      betTemplate: json['betTemplate'] as String?,
      hash: json['hash'] as String?,
      verifUrl: json['verifUrl'] as String?,
      d0: json['d0'] as String?,
      d1: json['d1'] as String?,
      d2: json['d2'] as String?,
      d3: json['d3'] as String?,
      d4: json['d4'] as String?,
      d5: json['d5'] as String?,
      d6: json['d6'] as String?,
      d7: json['d7'] as String?,
      d8: json['d8'] as String?,
      t0: json['t0'] as String?,
      t1: json['t1'] as String?,
      t2: json['t2'] as String?,
      t3: json['t3'] as String?,
      t4: json['t4'] as String?,
      t5: json['t5'] as String?,
      t6: json['t6'] as String?,
      t7: json['t7'] as String?,
      t8: json['t8'] as String?,
      t9: json['t9'] as String?,
    );
  }

  // Method to convert an instance to JSON
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'logo': logo,
      'gameType': gameType,
      'isSeal': isSeal,
      'lowFreq': lowFreq,
      'serverTime': serverTime,
      'curIssue': curIssue,
      'displayNumber': displayNumber,
      'curOpenTime': curOpenTime,
      'curCloseTime': curCloseTime,
      'preIsOpen': preIsOpen,
      'preIssue': preIssue,
      'preDisplayNumber': preDisplayNumber,
      'preOpenTime': preOpenTime,
      'preNum': preNum,
      'preResult': preResult,
      'adEnable': adEnable,
      'adPic': adPic,
      'adLink': adLink?.index,
      'adGameType': adGameType,
      'adLinkType': adLinkType,
      'isClose': isClose,
      'isInstant': isInstant,
      'serverTimestamp': serverTimestamp,
      'zodiacNums': zodiacNums?.map((e) => e.toJson()).toList(),
      'fiveElements': fiveElements?.map((e) => e.toJson()).toList(),
    };
  }
}

class ZodiacNumsData {
  String? key; // rat, metal
  String? name; // 鼠, 金
  List<int>? nums; //
  ZodiacNumsData({
    this.key,
    this.name,
    this.nums,
  });
  // Factory method to create an instance from JSON
  factory ZodiacNumsData.fromJson(Map<String, dynamic> json) {
    return ZodiacNumsData(
      key: json['key'] as String?,
      name: json['name'] as String?,
      nums: (json['nums'] as List<dynamic>?)?.map((e) => e as int).toList(),
    );
  }

  // Method to convert an instance to JSON
  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'name': name,
      'nums': nums,
    };
  }
}
