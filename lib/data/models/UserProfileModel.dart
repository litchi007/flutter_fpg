class UserProfileModel {
  int? contentNum;
  String? face;
  int? fansNum;
  int? favContentNum;
  int? followNum;
  int? isFollow;
  int? isLhcdocVip;
  String? levelName;
  int? likeNum;
  String? missionLevel;
  String? nickname;

  UserProfileModel({
    this.contentNum,
    this.face,
    this.fansNum,
    this.favContentNum,
    this.followNum,
    this.isFollow,
    this.isLhcdocVip,
    this.levelName,
    this.likeNum,
    this.missionLevel,
    this.nickname,
  });

  factory UserProfileModel.fromJson(Map<String, dynamic> json) {
    return UserProfileModel(
      contentNum: json['contentNum'],
      face: json['face'],
      fansNum: json['fansNum'],
      favContentNum: json['favContentNum'],
      followNum: json['followNum'],
      isFollow: json['isFollow'],
      isLhcdocVip: json['isLhcdocVip'],
      levelName: json['levelName'],
      likeNum: json['likeNum'],
      missionLevel: json['missionLevel'],
      nickname: json['nickname'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'contentNum': contentNum,
      'face': face,
      'fansNum': fansNum,
      'favContentNum': favContentNum,
      'followNum': followNum,
      'isFollow': isFollow,
      'isLhcdocVip': isLhcdocVip,
      'levelName': levelName,
      'likeNum': likeNum,
      'missionLevel': missionLevel,
      'nickname': nickname,
    };
  }
}
