class ColdHotModel {
  String number;
  int count;

  ColdHotModel({
    required this.number,
    required this.count,
  });

  // Deserialize from JSON
  factory ColdHotModel.fromJson(Map<String, dynamic> json) => ColdHotModel(
        number: json['number'] ?? '',
        count: json['count'] ?? 0,
      );

  // Serialize to JSON
  Map<String, dynamic> toJson() => {
        'number': number,
        'count': count,
      };
}
