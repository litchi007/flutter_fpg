class ChatUserInfoData {
  final String? uid;
  final String? nickname;
  final int? hasNickname;
  final String? avatar;
  final int? hasAvatar;
  final String? describe;
  final int? gender;
  final dynamic ip;
  final List<dynamic>? address;
  final int? chatStatus;
  final int? ipStatus;
  final int? nicknameStatus;
  final int? avatarStatus;
  final dynamic isManager;
  final int? isFriend;
  final int? isAllowMoment;

  ChatUserInfoData({
    this.uid,
    this.nickname,
    this.hasNickname,
    this.avatar,
    this.hasAvatar,
    this.describe,
    this.gender,
    this.ip,
    this.address,
    this.chatStatus,
    this.ipStatus,
    this.nicknameStatus,
    this.avatarStatus,
    this.isManager,
    this.isFriend,
    this.isAllowMoment,
  });

  factory ChatUserInfoData.fromJson(Map<String, dynamic> json) {
    return ChatUserInfoData(
      uid: json['uid'] as String?,
      nickname: json['nickname'] as String?,
      hasNickname: json['has_nickname'] as int?,
      avatar: json['avatar'] as String?,
      hasAvatar: json['has_avatar'] as int?,
      describe: json['describe'] as String?,
      gender: json['gender'] as int?,
      ip: json['ip'] as dynamic,
      address: json['address'] as List<dynamic>?,
      chatStatus: json['chat_status'] as int?,
      ipStatus: json['ip_status'] as int?,
      nicknameStatus: json['nickname_status'] as int?,
      avatarStatus: json['avatar_status'] as int?,
      isManager: json['isManager'] as dynamic,
      isFriend: json['is_friend'] as int?,
      isAllowMoment: json['is_allow_moment'] as int?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uid': uid,
      'nickname': nickname,
      'has_nickname': hasNickname,
      'avatar': avatar,
      'has_avatar': hasAvatar,
      'describe': describe,
      'gender': gender,
      'ip': ip,
      'address': address,
      'chat_status': chatStatus,
      'ip_status': ipStatus,
      'nickname_status': nicknameStatus,
      'avatar_status': avatarStatus,
      'isManager': isManager,
      'is_friend': isFriend,
      'is_allow_moment': isAllowMoment,
    };
  }
}

class ChatUserInfoStatement {
  final int? fansCount;
  final int? followCount;
  final int? shareCount;
  final int? isFollow;

  ChatUserInfoStatement({
    this.fansCount,
    this.followCount,
    this.shareCount,
    this.isFollow,
  });

  factory ChatUserInfoStatement.fromJson(Map<String, dynamic> json) {
    return ChatUserInfoStatement(
      fansCount: json['fans_count'] as int?,
      followCount: json['follow_count'] as int?,
      shareCount: json['share_count'] as int?,
      isFollow: json['is_follow'] as int?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'fans_count': fansCount,
      'follow_count': followCount,
      'share_count': shareCount,
      'is_follow': isFollow,
    };
  }
}

class ChatUserInfoResModel {
  final ChatUserInfoData? info;
  final List<dynamic>? momentList;
  final ChatUserInfoStatement? statement;

  ChatUserInfoResModel({
    this.info,
    this.momentList,
    this.statement,
  });

  factory ChatUserInfoResModel.fromJson(Map<String, dynamic> json) {
    return ChatUserInfoResModel(
      info:
          json['info'] != null ? ChatUserInfoData.fromJson(json['info']) : null,
      momentList: json['momentList'] as List<dynamic>?,
      statement: json['statement'] != null
          ? ChatUserInfoStatement.fromJson(json['statement'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'info': info?.toJson(),
      'momentList': momentList,
      'statement': statement?.toJson(),
    };
  }
}
