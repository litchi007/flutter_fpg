// ChatRedBagSetting Model
import 'package:fpg_flutter/utils/helper.dart';

class ChatRedBagSetting {
  String? isRedBag;
  String? isRedBagPwd;
  String? maxAmount;
  String? minAmount;
  String? maxQuantity;

  ChatRedBagSetting({
    this.isRedBag,
    this.isRedBagPwd,
    this.maxAmount,
    this.minAmount,
    this.maxQuantity,
  });

  factory ChatRedBagSetting.fromJson(Map<String, dynamic> json) {
    return ChatRedBagSetting(
      isRedBag: json['isRedBag']?.toString(),
      isRedBagPwd: json['isRedBagPwd']?.toString(),
      maxAmount: json['maxAmount']?.toString(),
      minAmount: json['minAmount']?.toString(),
      maxQuantity: json['maxQuantity']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isRedBag': isRedBag,
      'isRedBagPwd': isRedBagPwd,
      'maxAmount': maxAmount,
      'minAmount': minAmount,
      'maxQuantity': maxQuantity,
    };
  }
}

// ChatRoomChatAry Model
class ChatRoomChatAry {
  String? roomId;
  String? roomName;
  String? password;
  bool? isChatBan;
  bool? isShareBet;
  bool? isShareBill;
  String? typeId;
  String? sortId;
  List<String>? typeIds;
  String? talkLevels;
  String? talkGrade;
  ChatRedBagSetting? chatRedBagSetting;
  String? isMine;
  String? minAmount;
  String? maxAmount;
  String? oddsRate;
  String? quantity;
  String? isTalk;
  String? placeholder;
  String? cronDataTopType;

  ChatRoomChatAry({
    this.roomId,
    this.roomName,
    this.password,
    this.isChatBan,
    this.isShareBet,
    this.isShareBill,
    this.typeId,
    this.sortId,
    this.typeIds,
    this.talkLevels,
    this.talkGrade,
    this.chatRedBagSetting,
    this.isMine,
    this.minAmount,
    this.maxAmount,
    this.oddsRate,
    this.quantity,
    this.isTalk,
    this.placeholder,
    this.cronDataTopType,
  });

  factory ChatRoomChatAry.fromJson(Map<String, dynamic> json) {
    return ChatRoomChatAry(
      roomId: json['roomId']?.toString(),
      roomName: json['roomName']?.toString(),
      password: json['password']?.toString(),
      isChatBan: json['isChatBan'],
      isShareBet: json['isShareBet'],
      isShareBill: json['isShareBill'],
      typeId: json['typeId']?.toString(),
      sortId: json['sortId']?.toString(),
      typeIds:
          json['typeIds'] != null ? List<String>.from(json['typeIds']) : null,
      talkLevels: json['talkLevels']?.toString(),
      talkGrade: json['talkGrade']?.toString(),
      chatRedBagSetting: json['chatRedBagSetting'] != null
          ? ChatRedBagSetting.fromJson(json['chatRedBagSetting'])
          : null,
      isMine: json['isMine']?.toString(),
      minAmount: json['minAmount']?.toString(),
      maxAmount: json['maxAmount']?.toString(),
      oddsRate: json['oddsRate']?.toString(),
      quantity: json['quantity']?.toString(),
      isTalk: json['isTalk']?.toString(),
      placeholder: json['placeholder']?.toString(),
      cronDataTopType: json['cronDataTopType']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'roomId': roomId,
      'roomName': roomName,
      'password': password,
      'isChatBan': isChatBan,
      'isShareBet': isShareBet,
      'isShareBill': isShareBill,
      'typeId': typeId,
      'sortId': sortId,
      'typeIds': typeIds,
      'talkLevels': talkLevels,
      'talkGrade': talkGrade,
      'chatRedBagSetting': chatRedBagSetting?.toJson(),
      'isMine': isMine,
      'minAmount': minAmount,
      'maxAmount': maxAmount,
      'oddsRate': oddsRate,
      'quantity': quantity,
      'isTalk': isTalk,
      'placeholder': placeholder,
      'cronDataTopType': cronDataTopType,
    };
  }
}

// ChatRoomAnnounce Model
class ChatRoomAnnounce {
  String? title;
  String? content;

  ChatRoomAnnounce({
    this.title,
    this.content,
  });

  factory ChatRoomAnnounce.fromJson(Map<String, dynamic> json) {
    return ChatRoomAnnounce(
      title: json['title']?.toString(),
      content: json['content']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'content': content,
    };
  }
}

// ChatRoomData Model
class ChatRoomData {
  String? username;
  String? nickname;
  String? uid;
  String? testFlag;
  List<ChatRoomChatAry>? chatAry;
  List<ChatRoomAnnounce>? announce;
  String? ip;
  String? token;
  String? tokenImg;
  String? tokenTxt;
  String? level;
  String? isManager;
  bool? isAllBan;
  String? isPicBan;
  String? isShareBill;
  String? roomName;
  String? placeholderFlag;
  String? placeholder;
  String? sendRedBagUserType;
  String? showRedBagDetailConfig;
  String? titleStatus;
  String? redBagMineRule;
  String? redBagTotalAmountAllowFloat;
  String? redBagMineTotalAmountAllowFloat;
  String? chatOnlineMemberStatus;
  String? chatOnlineMemberCount;
  String? cronDataTopStatus;
  String? cronDataTopType;
  String? chatRoomRedirect;

  ChatRoomData({
    this.username,
    this.nickname,
    this.uid,
    this.testFlag,
    this.chatAry,
    this.announce,
    this.ip,
    this.token,
    this.tokenImg,
    this.tokenTxt,
    this.level,
    this.isManager,
    this.isAllBan,
    this.isPicBan,
    this.isShareBill,
    this.roomName,
    this.placeholderFlag,
    this.placeholder,
    this.sendRedBagUserType,
    this.showRedBagDetailConfig,
    this.titleStatus,
    this.redBagMineRule,
    this.redBagTotalAmountAllowFloat,
    this.redBagMineTotalAmountAllowFloat,
    this.chatOnlineMemberStatus,
    this.chatOnlineMemberCount,
    this.cronDataTopStatus,
    this.cronDataTopType,
    this.chatRoomRedirect,
  });

  factory ChatRoomData.fromJson(Map<String, dynamic> json) {
    return ChatRoomData(
      username: json['username']?.toString(),
      nickname: json['nickname']?.toString(),
      uid: json['uid']?.toString(),
      testFlag: json['testFlag']?.toString(),
      chatAry: json['chatAry'] != null
          ? List<ChatRoomChatAry>.from(
              json['chatAry'].map((item) => ChatRoomChatAry.fromJson(item)))
          : null,
      announce: json['announce'] != null
          ? List<ChatRoomAnnounce>.from(
              json['announce'].map((item) => ChatRoomAnnounce.fromJson(item)))
          : null,
      ip: json['ip']?.toString(),
      token: json['token']?.toString(),
      tokenImg: json['tokenImg']?.toString(),
      tokenTxt: json['tokenTxt']?.toString(),
      level: json['level']?.toString(),
      isManager: json['isManager']?.toString(),
      isAllBan: json['isAllBan'],
      isPicBan: json['isPicBan']?.toString(),
      isShareBill: json['isShareBill']?.toString(),
      roomName: json['roomName']?.toString(),
      placeholderFlag: json['placeholderFlag']?.toString(),
      placeholder: json['placeholder']?.toString(),
      sendRedBagUserType: json['sendRedBagUserType']?.toString(),
      showRedBagDetailConfig: json['showRedBagDetailConfig']?.toString(),
      titleStatus: json['titleStatus']?.toString(),
      redBagMineRule: json['redBagMineRule']?.toString(),
      redBagTotalAmountAllowFloat:
          json['redBagTotalAmountAllowFloat']?.toString(),
      redBagMineTotalAmountAllowFloat:
          json['redBagMineTotalAmountAllowFloat']?.toString(),
      chatOnlineMemberStatus: json['chatOnlineMemberStatus']?.toString(),
      chatOnlineMemberCount: json['chatOnlineMemberCount']?.toString(),
      cronDataTopStatus: json['cronDataTopStatus']?.toString(),
      cronDataTopType: json['cronDataTopType']?.toString(),
      chatRoomRedirect: json['chatRoomRedirect']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'nickname': nickname,
      'uid': uid,
      'testFlag': testFlag,
      'chatAry': chatAry?.map((item) => item.toJson()).toList(),
      'announce': announce?.map((item) => item.toJson()).toList(),
      'ip': ip,
      'token': token,
      'tokenImg': tokenImg,
      'tokenTxt': tokenTxt,
      'level': level,
      'isManager': isManager,
      'isAllBan': isAllBan,
      'isPicBan': isPicBan,
      'isShareBill': isShareBill,
      'roomName': roomName,
      'placeholderFlag': placeholderFlag,
      'placeholder': placeholder,
      'sendRedBagUserType': sendRedBagUserType,
      'showRedBagDetailConfig': showRedBagDetailConfig,
      'titleStatus': titleStatus,
      'redBagMineRule': redBagMineRule,
      'redBagTotalAmountAllowFloat': redBagTotalAmountAllowFloat,
      'redBagMineTotalAmountAllowFloat': redBagMineTotalAmountAllowFloat,
      'chatOnlineMemberStatus': chatOnlineMemberStatus,
      'chatOnlineMemberCount': chatOnlineMemberCount,
      'cronDataTopStatus': cronDataTopStatus,
      'cronDataTopType': cronDataTopType,
      'chatRoomRedirect': chatRoomRedirect,
    };
  }
}
