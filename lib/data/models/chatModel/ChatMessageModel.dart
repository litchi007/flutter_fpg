import 'package:fpg_flutter/data/models/RedBagModel.dart';

class ChatMessageModel {
  String? id;
  String? uid;
  String? avatar;
  String? username;
  String? level;
  dynamic isTrial;
  dynamic isManager;
  dynamic createTime;
  dynamic updateTime;
  String? dataId;
  String? messageCode;
  dynamic chatType;
  String? dataType;
  String? fullname;
  String? usernameBak;
  bool? isChatCron;
  bool? shareBillFlag;
  bool? betFollowFlag;
  String? ip;
  String? msg;
  dynamic callJson; // Allow dynamic type for callJson
  dynamic t;
  String? time;
  dynamic roomId;
  String? isManagerIconTag;
  List<dynamic>? callList;
  String? avator;
  String? avatarStatus;
  RedBagModel? redBag;

  ChatMessageModel({
    this.id,
    this.uid,
    this.avatar,
    this.username,
    this.level,
    this.isTrial,
    this.isManager,
    this.createTime,
    this.updateTime,
    this.dataId,
    this.messageCode,
    this.chatType,
    this.dataType,
    this.fullname,
    this.usernameBak,
    this.isChatCron,
    this.shareBillFlag,
    this.betFollowFlag,
    this.ip,
    this.msg,
    this.callJson,
    this.t,
    this.time,
    this.roomId,
    this.isManagerIconTag,
    this.callList,
    this.avator,
    this.avatarStatus,
    this.redBag,
  });

  // Factory constructor for creating a new instance from a map
  factory ChatMessageModel.fromJson(Map<String, dynamic> json) {
    return ChatMessageModel(
      id: json['id']?.toString(),
      uid: json['uid']?.toString(),
      avatar: json['avatar']?.toString(),
      username: json['username']?.toString(),
      level: json['level']?.toString(),
      isTrial: json['is_trial'],
      isManager: json['isManager'],
      createTime: json['createTime'],
      updateTime: json['updateTime'],
      dataId: json['dataId']?.toString(),
      messageCode: json['messageCode']?.toString(),
      chatType: json['chat_type'],
      dataType: json['data_type']?.toString(),
      fullname: json['fullname']?.toString(),
      usernameBak: json['usernameBak']?.toString(),
      isChatCron: json['isChatCron'],
      shareBillFlag: json['shareBillFlag'],
      betFollowFlag: json['betFollowFlag'],
      ip: json['ip']?.toString(),
      msg: json['msg']?.toString(),
      callJson: json['callJson'],
      t: json['t'],
      time: json['time']?.toString(),
      roomId: json['roomId'],
      isManagerIconTag: json['isManagerIconTag']?.toString(),
      callList:
          json['callList'] == null ? [] : List<dynamic>.from(json['callList']),
      avator: json['avator']?.toString(),
      avatarStatus: json['avatar_status']?.toString(),
      redBag:
          json['redBag'] != null ? RedBagModel.fromJson(json['redBag']) : null,
    );
  }

  // Method to convert an instance to a map
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'uid': uid,
      'avatar': avatar,
      'username': username,
      'level': level,
      'is_trial': isTrial,
      'isManager': isManager,
      'createTime': createTime,
      'updateTime': updateTime,
      'dataId': dataId,
      'messageCode': messageCode,
      'chat_type': chatType,
      'data_type': dataType,
      'fullname': fullname,
      'usernameBak': usernameBak,
      'isChatCron': isChatCron,
      'shareBillFlag': shareBillFlag,
      'betFollowFlag': betFollowFlag,
      'ip': ip,
      'msg': msg,
      'callJson': callJson,
      't': t,
      'time': time,
      'roomId': roomId,
      'isManagerIconTag': isManagerIconTag,
      'callList': callList,
      'avator': avator,
      'avatar_status': avatarStatus,
      'redBag': redBag
    };
  }
}
