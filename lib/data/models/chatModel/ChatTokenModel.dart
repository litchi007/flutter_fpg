import 'package:fpg_flutter/data/models/chatModel/ChatRoomData.dart';

class ChatTokenModel {
  String? chatOnlineMemberCount;
  String? operates;
  String? chatOnlineMemberType;
  String? chatOnlineMemberStatus;
  String? isCallStatus;
  String? uid;
  String? username;
  String? nickname;
  List<ChatRoomChatAry>? chatAry;
  List<ChatRoomAnnounce>? announce;
  String? ip;
  String? level;
  dynamic isManager;
  bool? isAllBan;
  String? isPicBan;
  String? isShareBill;
  String? roomName;
  String? checkCoinPwdStatus;
  String? checkMineCoinPwdStatus;
  String? titleStatus;
  String? levelName;
  String? cronDataTopType;
  String? cronDataTopStatus;
  String? isAddFriend;
  String? isShowChat;
  String? chatBetAreaChange;

  ChatTokenModel({
    this.chatOnlineMemberCount,
    this.operates,
    this.chatOnlineMemberType,
    this.chatOnlineMemberStatus,
    this.isCallStatus,
    this.uid,
    this.username,
    this.nickname,
    this.chatAry,
    this.announce,
    this.ip,
    this.level,
    this.isManager,
    this.isAllBan,
    this.isPicBan,
    this.isShareBill,
    this.roomName,
    this.checkCoinPwdStatus,
    this.checkMineCoinPwdStatus,
    this.titleStatus,
    this.levelName,
    this.cronDataTopType,
    this.cronDataTopStatus,
    this.isAddFriend,
    this.isShowChat,
    this.chatBetAreaChange,
  });

  factory ChatTokenModel.fromJson(Map<String, dynamic> json) {
    return ChatTokenModel(
      chatOnlineMemberCount: json['chatOnlineMemberCount']?.toString(),
      operates: json['operates']?.toString(),
      chatOnlineMemberType: json['chatOnlineMemberType']?.toString(),
      chatOnlineMemberStatus: json['chatOnlineMemberStatus']?.toString(),
      isCallStatus: json['isCallStatus']?.toString(),
      uid: json['uid']?.toString(),
      username: json['username']?.toString(),
      nickname: json['nickname']?.toString(),
      chatAry: (json['chatAry'] as List?)
          ?.map((e) => ChatRoomChatAry.fromJson(e))
          .toList(),
      announce: (json['announce'] as List?)
          ?.map((e) => ChatRoomAnnounce.fromJson(e))
          .toList(),
      ip: json['ip']?.toString(),
      level: json['level']?.toString(),
      isManager: json['isManager'],
      isAllBan: json['isAllBan'],
      isPicBan: json['isPicBan']?.toString(),
      isShareBill: json['isShareBill']?.toString(),
      roomName: json['roomName']?.toString(),
      checkCoinPwdStatus: json['checkCoinPwdStatus']?.toString(),
      checkMineCoinPwdStatus: json['checkMineCoinPwdStatus']?.toString(),
      titleStatus: json['titleStatus']?.toString(),
      levelName: json['levelName']?.toString(),
      cronDataTopType: json['cronDataTopType']?.toString(),
      cronDataTopStatus: json['cronDataTopStatus']?.toString(),
      isAddFriend: json['isAddFriend']?.toString(),
      isShowChat: json['isShowChat']?.toString(),
      chatBetAreaChange: json['chatBetAreaChange']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'chatOnlineMemberCount': chatOnlineMemberCount,
      'operates': operates,
      'chatOnlineMemberType': chatOnlineMemberType,
      'chatOnlineMemberStatus': chatOnlineMemberStatus,
      'isCallStatus': isCallStatus,
      'uid': uid,
      'username': username,
      'nickname': nickname,
      'chatAry': chatAry?.map((e) => e.toJson()).toList(),
      'announce': announce?.map((e) => e.toJson()).toList(),
      'ip': ip,
      'level': level,
      'isManager': isManager,
      'isAllBan': isAllBan,
      'isPicBan': isPicBan,
      'isShareBill': isShareBill,
      'roomName': roomName,
      'checkCoinPwdStatus': checkCoinPwdStatus,
      'checkMineCoinPwdStatus': checkMineCoinPwdStatus,
      'titleStatus': titleStatus,
      'levelName': levelName,
      'cronDataTopType': cronDataTopType,
      'cronDataTopStatus': cronDataTopStatus,
      'isAddFriend': isAddFriend,
      'isShowChat': isShowChat,
      'chatBetAreaChange': chatBetAreaChange,
    };
  }
}
