class UGPromoteModel {
  String? clsName;
  String? promoteId;
  String? title;
  String? content;
  String? pic;
  String? linkUrl;
  String? category;
  int? linkCategory;
  int? linkPosition;
  double? picHeight;
  double? webViewHeight;

  UGPromoteModel({
    this.clsName = 'UGPromoteModel',
    this.promoteId,
    this.title,
    this.content,
    this.pic,
    this.linkUrl,
    this.category,
    this.linkCategory,
    this.linkPosition,
    this.picHeight,
    this.webViewHeight,
  });

  factory UGPromoteModel.fromJson(Map<String, dynamic> json) {
    return UGPromoteModel(
      promoteId: json['promoteId'],
      title: json['title'],
      content: json['content'],
      pic: json['pic'],
      linkUrl: json['linkUrl'],
      category: json['category'],
      linkCategory: json['linkCategory'],
      linkPosition: json['linkPosition'],
      picHeight:
          (json['picHeight'] != null) ? json['picHeight'].toDouble() : null,
      webViewHeight: (json['webViewHeight'] != null)
          ? json['webViewHeight'].toDouble()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['clsName'] = clsName;
    data['promoteId'] = promoteId;
    data['title'] = title;
    data['content'] = content;
    data['pic'] = pic;
    data['linkUrl'] = linkUrl;
    data['category'] = category;
    data['linkCategory'] = linkCategory;
    data['linkPosition'] = linkPosition;
    if (picHeight != null) data['picHeight'] = picHeight;
    if (webViewHeight != null) data['webViewHeight'] = webViewHeight;
    return data;
  }
}

class UGPromoteListModel {
  Map<String, String>? categories;
  List<UGPromoteModel>? list;
  bool? showCategory;
  String? style;
  List<Category>? newCategories;

  UGPromoteListModel({
    this.categories,
    this.list,
    this.showCategory,
    this.style,
    this.newCategories,
  });

  factory UGPromoteListModel.fromJson(Map<String, dynamic> json) {
    return UGPromoteListModel(
      categories: Map<String, String>.from(json['categories']),
      list: (json['list'] as List<dynamic>?)
          ?.map((item) => UGPromoteModel.fromJson(item))
          .toList(),
      showCategory: json['showCategory'],
      style: json['style'],
      newCategories: (json['newCategories'] as List<dynamic>?)
          ?.map((item) => Category.fromJson(item))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'categories': categories,
      'list': list?.map((item) => item.toJson()).toList(),
      'showCategory': showCategory,
      'style': style,
      'newCategories': newCategories?.map((item) => item.toJson()).toList(),
    };
  }
}

class Category {
  String? id;
  String? name;
  int? sort;

  Category({
    this.id,
    this.name,
    this.sort,
  });

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json['id'],
      name: json['name'],
      sort: json['sort'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'sort': sort,
    };
  }
}
