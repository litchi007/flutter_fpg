import 'package:fpg_flutter/data/models/CallModel.dart';

class SendMsgTextModel {
  List<CallModel>? callList = [];
  String? code;
  String? dataType; // 消息类型 text [普通消息]，image(图片)
  int? chatType; // 聊天类型 0-公聊，1-私聊
  String? roomId; // number or string
  String? chatUid; // string or number
  String? chatName; // 目标人昵称
  String? username; // 用户名
  String? level; // 用户的等级
  String? ip; // 用户ip
  String? t; // 时间戳
  String? token; // 用户的token
  bool? shareBillFlag = false; // 是否为注单分享 false,true 默认false
  bool? betFollowFlag = false; // 是否是投注计划 false,true 默认false
  String? betUrl; // 注单地址 默认填空字符串
  String? msg; // 内容 假如是图片base64 的数据流
  int? channel; //聊天app固定2
  String? messageCode;
  SendMsgTextModel({
    this.code,
    this.dataType,
    this.chatType,
    this.callList = const [],
    this.roomId,
    this.chatUid = "",
    this.chatName = "",
    this.username,
    this.level,
    this.ip,
    this.t,
    this.token,
    this.shareBillFlag = false,
    this.betFollowFlag = false,
    this.betUrl = "",
    this.msg,
    this.channel = 2,
    this.messageCode,
  });

  factory SendMsgTextModel.fromJson(Map<String, dynamic> json) {
    return SendMsgTextModel(
      callList: (json['callList'] as List<dynamic>?)
          ?.map((e) => CallModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      code: json['code'] as String,
      dataType: json['data_type'] as String?,
      chatType: json['chat_type'] as int?,
      roomId: json['roomId'] as String,
      chatUid: json['chatUid'] as String,
      chatName: json['chatName'] as String,
      username: json['username'] as String,
      level: json['level'] as String,
      ip: json['ip'] as String,
      t: json['t'] as String,
      token: json['token'] as String,
      shareBillFlag: json['shareBillFlag'] as bool? ?? false,
      betFollowFlag: json['betFollowFlag'] as bool? ?? false,
      betUrl: json['betUrl'] as String,
      msg: json['msg'] as String,
      messageCode: json['messageCode'] as String,
      channel: json['channel'] as int? ?? 2,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'callList': callList?.map((e) => e.toJson()).toList(),
      'code': code,
      'data_type': dataType,
      'chat_type': chatType,
      'roomId': roomId,
      'chatUid': chatUid,
      'chatName': chatName,
      'username': username,
      'level': level,
      'ip': ip,
      't': t,
      'token': token,
      'shareBillFlag': shareBillFlag,
      'betFollowFlag': betFollowFlag,
      'betUrl': betUrl,
      'msg': msg,
      'channel': channel,
      'messageCode': messageCode,
    };
  }
}

// Example placeholders for CallModel, SendDataType, and SendChatType classes/enums

enum SendDataType {
  text,
  image,
}

enum SendChatType {
  public,
  private,
}
