class BannersData {
  String? interval;
  List<SliderBanner>? list;

  BannersData({
    this.interval,
    this.list,
  });

  factory BannersData.fromJson(Map<String, dynamic> json) {
    return BannersData(
      interval: json['interval'],
      list: (json['list'] as List<dynamic>?)
          ?.map((item) => SliderBanner.fromJson(item))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'interval': interval,
      'list': list?.map((item) => item.toJson()).toList(),
    };
  }
}

class SliderBanner {
  String? lotteryGameType;
  String? id;
  String? pic;
  int? realIsPopup;
  String? linkPosition;
  int? realSupportTrial;
  String? linkCategory;
  String? url;
  String? linkAction;

  SliderBanner({
    this.lotteryGameType,
    this.id,
    this.pic,
    this.realIsPopup,
    this.linkPosition,
    this.realSupportTrial,
    this.linkCategory,
    this.url,
    this.linkAction,
  });

  factory SliderBanner.fromJson(Map<String, dynamic> json) {
    return SliderBanner(
      lotteryGameType: json['lotteryGameType'],
      id: json['id'],
      pic: json['pic'],
      realIsPopup: json['realIsPopup'],
      linkPosition: json['linkPosition'],
      realSupportTrial: json['realSupportTrial'],
      linkCategory: json['linkCategory'],
      url: json['url'],
      linkAction: json['linkAction'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'lotteryGameType': lotteryGameType,
      'id': id,
      'pic': pic,
      'realIsPopup': realIsPopup,
      'linkPosition': linkPosition,
      'realSupportTrial': realSupportTrial,
      'linkCategory': linkCategory,
      'url': url,
      'linkAction': linkAction,
    };
  }
}
