class TkListItemModel {
  String? id;
  String? name;
  String? isHot;
  String? colorType;
  String? pinyin;

  TkListItemModel({
    this.id,
    this.name,
    this.isHot,
    this.colorType,
    this.pinyin,
  });

  factory TkListItemModel.fromJson(Map<String, dynamic> json) {
    return TkListItemModel(
      id: json['id'],
      name: json['name'],
      isHot: json['isHot'],
      colorType: json['colorType'],
      pinyin: json['pinyin'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'isHot': isHot,
      'colorType': colorType,
      'pinyin': pinyin,
    };
  }
}
