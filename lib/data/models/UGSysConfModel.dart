import 'package:fpg_flutter/data/models/sysConfModel/InviteCode.dart';
import 'package:fpg_flutter/data/models/sysConfModel/LHCPriceItem.dart';
import 'package:fpg_flutter/data/models/sysConfModel/MobileMenu.dart';
import 'package:fpg_flutter/data/models/sysConfModel/OAuth.dart';
import 'package:fpg_flutter/data/models/sysConfModel/QdSupChannel.dart';
import 'package:fpg_flutter/data/models/sysConfModel/UserCenterCategory.dart';
import 'package:fpg_flutter/data/models/sysConfModel/UserCenterItem.dart';

class LoginNotice {
  final String? text; // 登录成功文本
  final String? switch_; // 否显示登录成功文本

  LoginNotice({this.text, this.switch_});

  // Factory constructor to create an instance from a map
  factory LoginNotice.fromJson(Map<String, dynamic> json) {
    return LoginNotice(
      text: json['text'] as String?,
      switch_: json['switch'] as String?,
    );
  }

  // Method to convert an instance to a map
  Map<String, dynamic> toJson() {
    return {
      'text': text,
      'switch': switch_,
    };
  }
}

class UGSysConfModel {
  String? host;
  String? port;
  String? apiHost;
  int? siteId;
  String? merId;
  String? webName;
  String? zxkfUrl;
  String? allowreg;
  String? closeregreason;
  String? agentMApply;
  String? pageTitle;
  String? loginTo;
  String? memberAgent;
  String? mPromotePos;
  String? agentRegbutton;
  String? passLengthMin;
  String? passLengthMax;
  String? passLimit;
  String? myrecoImg;
  String? missionName;
  String? hideReco;
  String? regName;
  String? regFundpwd;
  String? regQq;
  String? regWx;
  String? regPhone;
  String? regFb;
  String? regZalo;
  String? smsVerify;
  String? regVcode;
  String? regEmail;
  String? betBankCard;
  String? regLine;
  bool? googleVerifier;
  String? znxNotify;
  String? regAndLogin;
  String? smsFindPassword;
  String? userLengthLimitSwitch;
  String? userLengthMin;
  String? userLengthMax;
  String? oftenLoginArea;
  String? missionSwitch;
  String? isIntToMoney;
  String? missionBili;
  String? checkinSwitch;
  String? mkCheckinSwitch;
  String? chatappLogo;
  String? chatappStartLogo;
  String? mobileLogo;
  String? mobileIcon;
  int? missionPopUpSwitch;
  int? guestSwitch;
  String? missionLogo;
  String? minWithdrawMoney;
  String? maxWithdrawMoney;
  String? announceFirst;
  String? adSliderTimer;
  List<String>? popupTab;
  String? popupAnnounce;
  String? popupHour;
  dynamic popupType;
  String? switchDOYUSDTTrans;
  String? yuebaoName;
  bool? allowMemberCancelBet;
  dynamic balanceDecimal;
  String? chaseNumber;
  String? selectNumber;
  String? switchBindVerify;
  String? switchShowFriendReferral;
  String? showNavigationBar;
  String? rankingListSwitch;
  int? grabRedbagRankingListSwitch;
  bool? chatRoomSwitch;
  String? chatRoomName;
  bool? chatFollowSwitch;
  String? chatShareBetMinAmount;
  String? mobileGameHall;
  String? betMin;
  String? betMax;
  String? customiseBetMax;
  String? switchBalanceChannel;
  String? balanceChannelStartTime;
  String? balanceChannelEndTime;
  String? balanceChannelPrompt;
  String? switchYuebaoChannel;
  String? yuebaoChannelStartTime;
  String? yuebaoChannelEndTime;
  String? yuebaoChannelPrompt;
  bool? mobileHomeGameTypeSwitch;
  String? picTypeshow;
  List<String>? apiHosts;
  String? curLangCode;
  String? allowMemberModifyLanguage;
  String? mobileTemplateCategory;
  String? mobileTemplateBackground;
  String? mobileTemplateStyle;
  String? mobileViewModel;
  String? mobileTemplateLhcStyle;
  String? mobileTemplateGpkStyle;
  String? mobileTemplateHBStyle;
  String? mobileTemplateXbjStyle;
  List<MobileMenu>? mobileMenu;
  dynamic kjwTemplateSp;
  List<String>? kjwLotteries;
  String? serviceQQ1;
  String? serviceQQ2;
  String? facekfUrl;
  String? whatskfUrl;
  String? appPopupQqNum;
  String? appPopupQqImg;
  String? appPopupWechatNum;
  String? appPopupWechatImg;
  String? withdrawChannelAd;
  String? zxkfUrl2;
  String? zalokfUrl;
  String? telegramkfUrl;
  String? shortCut;
  String? shortCutIcon;
  bool? iosRelease;
  bool? yuebaoSwitch;
  String? easyRememberDomain;
  bool? loginVCode;
  String? loginVCodeType;
  int? loginChatAppVcode;
  int? loginChatAppVcodeType;
  String? chatRoomServer;
  List<UserCenterItem>? userCenters;
  bool? lhcdocSwitchMobileHome;
  List<UserCenterCategory>? userCenterCategoryList;
  bool? lhcdocMiCard;
  List<LHCPriceItem>? lhcPriceList;
  bool? lhcdocIsShowNav;
  bool? lhcdocUserContCheck;
  bool? lhcdocUserContRepCheck;
  int? qdSwitchDe;
  int? qdSwitchWi;
  int? qdSwitch;
  List<QdSupChannel>? qdSupChannels;
  String? appDownloadUrl;
  String? appdownloadbar;
  int? domainBindAgentId;
  String? proxyDomainDefault;
  String? chatLink;
  bool? switchAgentRecharge;
  int? nameAgentRecharge;
  bool? switchShowActivityCategory;
  bool? betAmountIsDecimal;
  bool? activeReturnCoinStatus;
  int? activeReturnCoinRatio;
  int? autoTransferLimit;
  bool? switchAutoTransfer;
  String? currency;
  String? switchToK;
  String? appSelectType;
  String? regLoginBgSw;
  String? regLoginBg;
  dynamic oauth;
  LoginNotice? loginNotice;
  InviteCode? inviteCode;
  String? inviteWord;
  String? inviteCodeSwitch;
  String? regBindCardSwitch;
  String? frontendAgentAddMember;
  String? regNameSwitch;
  String? switchAgentRechargeDML;
  String? isAgentRechargeNeededPwd;
  String? switchCoinPwd;
  int? switchCoinPwdSms;
  List<String>? coinPwdAuditOptionAry;
  int? mBonsSwitch;
  String? addEditBankSw;
  String? addEditAlipaySw;
  String? addEditWechatSw;
  String? addEditVcoinSw;
  int? addEditKdouSw;
  String? rechargeImgSw;
  List<String>? staticServers;
  String? loginsessid;

  UGSysConfModel({
    this.host,
    this.port,
    this.apiHost,
    this.siteId,
    this.merId,
    this.webName,
    this.zxkfUrl,
    this.allowreg,
    this.closeregreason,
    this.agentMApply,
    this.pageTitle,
    this.loginTo,
    this.memberAgent,
    this.mPromotePos,
    this.agentRegbutton,
    this.passLengthMin,
    this.passLengthMax,
    this.passLimit,
    this.myrecoImg,
    this.missionName,
    this.hideReco,
    this.regName,
    this.regFundpwd,
    this.regQq,
    this.regWx,
    this.regPhone,
    this.regFb,
    this.regZalo,
    this.smsVerify,
    this.regVcode,
    this.regEmail,
    this.betBankCard,
    this.regLine,
    this.googleVerifier,
    this.znxNotify,
    this.regAndLogin,
    this.smsFindPassword,
    this.userLengthLimitSwitch,
    this.userLengthMin,
    this.userLengthMax,
    this.oftenLoginArea,
    this.missionSwitch,
    this.isIntToMoney,
    this.missionBili,
    this.checkinSwitch,
    this.mkCheckinSwitch,
    this.chatappLogo,
    this.chatappStartLogo,
    this.mobileLogo,
    this.mobileIcon,
    this.missionPopUpSwitch,
    this.guestSwitch,
    this.missionLogo,
    this.minWithdrawMoney,
    this.maxWithdrawMoney,
    this.announceFirst,
    this.adSliderTimer,
    this.popupTab,
    this.popupAnnounce,
    this.popupHour,
    this.popupType,
    this.switchDOYUSDTTrans,
    this.yuebaoName,
    this.allowMemberCancelBet,
    this.balanceDecimal,
    this.chaseNumber,
    this.selectNumber,
    this.switchBindVerify,
    this.switchShowFriendReferral,
    this.showNavigationBar,
    this.rankingListSwitch,
    this.grabRedbagRankingListSwitch,
    this.chatRoomSwitch,
    this.chatRoomName,
    this.chatFollowSwitch,
    this.chatShareBetMinAmount,
    this.mobileGameHall,
    this.betMin,
    this.betMax,
    this.customiseBetMax,
    this.switchBalanceChannel,
    this.balanceChannelStartTime,
    this.balanceChannelEndTime,
    this.balanceChannelPrompt,
    this.switchYuebaoChannel,
    this.yuebaoChannelStartTime,
    this.yuebaoChannelEndTime,
    this.yuebaoChannelPrompt,
    this.mobileHomeGameTypeSwitch,
    this.activeReturnCoinRatio,
    this.activeReturnCoinStatus,
    this.addEditAlipaySw,
    this.addEditBankSw,
    this.addEditKdouSw,
    this.addEditVcoinSw,
    this.addEditWechatSw,
    this.allowMemberModifyLanguage,
    this.apiHosts,
    this.appDownloadUrl,
    this.appPopupQqImg,
    this.appPopupQqNum,
    this.appPopupWechatImg,
    this.appPopupWechatNum,
    this.appSelectType,
    this.appdownloadbar,
    this.autoTransferLimit,
    this.betAmountIsDecimal,
    this.chatLink,
    this.chatRoomServer,
    this.coinPwdAuditOptionAry,
    this.curLangCode,
    this.currency,
    this.domainBindAgentId,
    this.easyRememberDomain,
    this.facekfUrl,
    this.frontendAgentAddMember,
    this.inviteCode,
    this.inviteCodeSwitch,
    this.inviteWord,
    this.iosRelease,
    this.isAgentRechargeNeededPwd,
    this.kjwLotteries,
    this.kjwTemplateSp,
    this.lhcPriceList,
    this.lhcdocIsShowNav,
    this.lhcdocMiCard,
    this.lhcdocSwitchMobileHome,
    this.lhcdocUserContCheck,
    this.lhcdocUserContRepCheck,
    this.loginChatAppVcode,
    this.loginChatAppVcodeType,
    this.loginNotice,
    this.loginVCode,
    this.loginVCodeType,
    this.loginsessid,
    this.mBonsSwitch,
    this.mobileMenu,
    this.mobileTemplateBackground,
    this.mobileTemplateCategory,
    this.mobileTemplateGpkStyle,
    this.mobileTemplateHBStyle,
    this.mobileTemplateLhcStyle,
    this.mobileTemplateStyle,
    this.mobileTemplateXbjStyle,
    this.mobileViewModel,
    this.nameAgentRecharge,
    this.oauth,
    this.picTypeshow,
    this.proxyDomainDefault,
    this.qdSupChannels,
    this.qdSwitch,
    this.qdSwitchDe,
    this.qdSwitchWi,
    this.rechargeImgSw,
    this.regBindCardSwitch,
    this.regLoginBg,
    this.regLoginBgSw,
    this.regNameSwitch,
    this.serviceQQ1,
    this.serviceQQ2,
    this.shortCut,
    this.shortCutIcon,
    this.staticServers,
    this.switchAgentRecharge,
    this.switchAgentRechargeDML,
    this.switchAutoTransfer,
    this.switchCoinPwd,
    this.switchCoinPwdSms,
    this.switchShowActivityCategory,
    this.switchToK,
    this.telegramkfUrl,
    this.userCenterCategoryList,
    this.userCenters,
    this.whatskfUrl,
    this.withdrawChannelAd,
    this.yuebaoSwitch,
    this.zalokfUrl,
    this.zxkfUrl2,
  });

  factory UGSysConfModel.fromJson(Map<String, dynamic> json) {
    return UGSysConfModel(
      host: json['host'],
      port: json['port'],
      apiHost: json['apiHost'],
      siteId: json['siteId'],
      merId: json['merId'],
      webName: json['webName'],
      zxkfUrl: json['zxkfUrl'],
      allowreg: json['allowreg'],
      closeregreason: json['closeregreason'],
      agentMApply: json['agent_m_apply'],
      pageTitle: json['page_title'],
      loginTo: json['login_to'],
      memberAgent: json['member_agent'],
      mPromotePos: json['m_promote_pos'],
      agentRegbutton: json['agentRegbutton'],
      passLengthMin: json['pass_length_min'],
      passLengthMax: json['pass_length_max'],
      passLimit: json['pass_limit'],
      myrecoImg: json['myreco_img'],
      missionName: json['missionName'],
      hideReco: json['hide_reco'],
      regName: json['reg_name'],
      regFundpwd: json['reg_fundpwd'],
      regQq: json['reg_qq'],
      regWx: json['reg_wx'],
      regPhone: json['reg_phone'],
      regFb: json['reg_fb'],
      regZalo: json['reg_zalo'],
      smsVerify: json['smsVerify'],
      regVcode: json['reg_vcode'],
      regEmail: json['reg_email'],
      betBankCard: json['bet_bank_card'],
      regLine: json['reg_line'],
      googleVerifier: json['googleVerifier'],
      znxNotify: json['znxNotify'],
      regAndLogin: json['regAndLogin'],
      smsFindPassword: json['smsFindPassword'],
      userLengthLimitSwitch: json['user_length_limit_switch'],
      userLengthMin: json['user_length_min'],
      userLengthMax: json['user_length_max'],
      oftenLoginArea: json['oftenLoginArea'],
      missionSwitch: json['missionSwitch'],
      isIntToMoney: json['isIntToMoney'],
      missionBili: json['missionBili'],
      checkinSwitch: json['checkinSwitch'],
      mkCheckinSwitch: json['mkCheckinSwitch'],
      chatappLogo: json['chatapp_logo'],
      chatappStartLogo: json['chatapp_start_logo'],
      mobileLogo: json['mobile_logo'],
      mobileIcon: json['mobile_icon'],
      missionPopUpSwitch: json['missionPopUpSwitch'],
      guestSwitch: json['guestSwitch'],
      missionLogo: json['mission_logo'],
      minWithdrawMoney: json['minWithdrawMoney'],
      maxWithdrawMoney: json['maxWithdrawMoney'],
      announceFirst: json['announce_first'],
      adSliderTimer: json['adSliderTimer'],
      popupTab: List<String>.from(json['popup_tab']),
      popupAnnounce: json['popup_announce'],
      popupHour: json['popup_hour'],
      popupType: json['popup_type'],
      switchDOYUSDTTrans: json['switchDOYUSDTTrans'],
      yuebaoName: json['yuebaoName'],
      allowMemberCancelBet: json['allowMemberCancelBet'],
      balanceDecimal: json['balanceDecimal'],
      chaseNumber: json['chaseNumber'],
      selectNumber: json['selectNumber'],
      switchBindVerify: json['switchBindVerify'],
      switchShowFriendReferral: json['switchShowFriendReferral'],
      showNavigationBar: json['showNavigationBar'],
      rankingListSwitch: json['rankingListSwitch'],
      grabRedbagRankingListSwitch: json['grabRedbagRankingListSwitch'],
      chatRoomSwitch: json['chatRoomSwitch'],
      chatRoomName: json['chatRoomName'],
      chatFollowSwitch: json['chatFollowSwitch'],
      chatShareBetMinAmount: json['chatShareBetMinAmount'],
      mobileGameHall: json['mobileGameHall'],
      betMin: json['betMin'],
      betMax: json['betMax'],
      customiseBetMax: json['customiseBetMax'],
      switchBalanceChannel: json['switchBalanceChannel'],
      balanceChannelStartTime: json['balanceChannelStartTime'],
      balanceChannelEndTime: json['balanceChannelEndTime'],
      balanceChannelPrompt: json['balanceChannelPrompt'],
      switchYuebaoChannel: json['switchYuebaoChannel'],
      yuebaoChannelStartTime: json['yuebaoChannelStartTime'],
      yuebaoChannelEndTime: json['yuebaoChannelEndTime'],
      yuebaoChannelPrompt: json['yuebaoChannelPrompt'],
      mobileHomeGameTypeSwitch: json['mobileHomeGameTypeSwitch'],
      picTypeshow: json['picTypeshow'],
      apiHosts:
          (json['apiHosts'] as List?)?.map((item) => item.toString()).toList(),
      curLangCode: json['curLangCode'],
      allowMemberModifyLanguage: json['allowMemberModifyLanguage'],
      mobileTemplateCategory: json['mobileTemplateCategory'],
      mobileTemplateBackground: json['mobileTemplateBackground'],
      mobileTemplateStyle: json['mobileTemplateStyle'],
      mobileViewModel: json['mobileViewModel'],
      mobileTemplateLhcStyle: json['mobileTemplateLhcStyle'].toString(),
      mobileTemplateGpkStyle: json['mobileTemplateGpkStyle'].toString(),
      mobileTemplateHBStyle: json['mobileTemplateHBStyle'].toString(),
      mobileTemplateXbjStyle: json['mobileTemplateXbjStyle'].toString(),
      mobileMenu: json['mobileMenu'] != null
          ? List<MobileMenu>.from(
              json['mobileMenu']?.map((x) => MobileMenu.fromJson(x)))
          : null,
      kjwTemplateSp: json['kjwTemplateSp'],
      kjwLotteries: List<String>.from(json['kjwLotteries']),
      serviceQQ1: json['serviceQQ1'],
      serviceQQ2: json['serviceQQ2'],
      facekfUrl: json['facekfUrl'],
      whatskfUrl: json['whatskfUrl'],
      appPopupQqNum: json['appPopupQqNum'],
      appPopupQqImg: json['appPopupQqImg'],
      appPopupWechatNum: json['appPopupWechatNum'],
      appPopupWechatImg: json['appPopupWechatImg'],
      withdrawChannelAd: json['withdrawChannelAd'],
      zxkfUrl2: json['zxkfUrl2'],
      zalokfUrl: json['zalokfUrl'],
      telegramkfUrl: json['telegramkfUrl'],
      shortCut: json['shortCut'],
      shortCutIcon: json['shortCutIcon'],
      iosRelease: json['iosRelease'],
      yuebaoSwitch: json['yuebaoSwitch'],
      easyRememberDomain: json['easyRememberDomain'],
      loginVCode: json['loginVCode'],
      loginVCodeType: json['loginVCodeType'],
      loginChatAppVcode: json['login_chatApp_vcode'],
      loginChatAppVcodeType: json['login_chatApp_vcode_type'],
      chatRoomServer: json['chatRoomServer'],
      userCenters: json['userCenter'] != null
          ? List<UserCenterItem>.from(
              json['userCenter'].map((x) => UserCenterItem.fromJson(x)))
          : null,
      lhcdocSwitchMobileHome: json['lhcdocSwitchMobileHome'],
      userCenterCategoryList: json['userCenterCategoryList'] != null
          ? List<UserCenterCategory>.from(json['userCenterCategoryList']
              .map((x) => UserCenterCategory.fromJson(x)))
          : null,
      lhcdocMiCard: json['lhcdocMiCard'],
      lhcPriceList: json['lhcPriceList'] != null
          ? List<LHCPriceItem>.from(
              json['lhcPriceList'].map((x) => LHCPriceItem.fromJson(x)))
          : null,
      lhcdocIsShowNav: json['lhcdocIsShowNav'],
      lhcdocUserContCheck: json['lhcdocUserContCheck'],
      lhcdocUserContRepCheck: json['lhcdocUserContRepCheck'],
      qdSwitchDe: json['qdSwitchDe'],
      qdSwitchWi: json['qdSwitchWi'],
      qdSwitch: json['qdSwitch'],
      qdSupChannels: json['qd_supChannels'] != null
          ? List<QdSupChannel>.from(
              json['qd_supChannels'].map((x) => QdSupChannel.fromJson(x)))
          : null,
      appDownloadUrl: json['appDownloadUrl'],
      appdownloadbar: json['appdownloadbar'],
      domainBindAgentId: json['domainBindAgentId'],
      proxyDomainDefault: json['proxyDomainDefault'],
      chatLink: json['chatLink'],
      switchAgentRecharge: json['switchAgentRecharge'],
      nameAgentRecharge: json['nameAgentRecharge'],
      switchShowActivityCategory: json['switchShowActivityCategory'],
      betAmountIsDecimal: json['betAmountIsDecimal'],
      activeReturnCoinStatus: json['activeReturnCoinStatus'],
      activeReturnCoinRatio: json['activeReturnCoinRatio'],
      autoTransferLimit: json['autoTransferLimit'],
      switchAutoTransfer: json['switchAutoTransfer'],
      currency: json['currency'],
      switchToK: json['switchToK'],
      appSelectType: json['appSelectType'],
      regLoginBgSw: json['reg_login_bg_sw'],
      regLoginBg: json['reg_login_bg'],
      oauth: json['oauth'],
      loginNotice: LoginNotice.fromJson(json['loginNotice']),
      inviteCode: InviteCode.fromJson(json['inviteCode']),
      inviteWord: json['inviteWord'],
      inviteCodeSwitch: json['inviteCodeSwitch'],
      regBindCardSwitch: json['regBindCardSwitch'],
      frontendAgentAddMember: json['frontend_agent_add_member'],
      regNameSwitch: json['regNameSwitch'],
      switchAgentRechargeDML: json['switchAgentRechargeDML'],
      isAgentRechargeNeededPwd: json['isAgentRechargeNeededPwd'],
      switchCoinPwd: json['switchCoinPwd'],
      switchCoinPwdSms: json['switchCoinPwdSms'],
      coinPwdAuditOptionAry: List<String>.from(json['coinPwdAuditOptionAry']),
      mBonsSwitch: json['mBonsSwitch'],
      addEditBankSw: json['add_edit_bank_sw'],
      addEditAlipaySw: json['add_edit_alipay_sw'],
      addEditWechatSw: json['add_edit_wechat_sw'],
      addEditVcoinSw: json['add_edit_vcoin_sw'],
      addEditKdouSw: json['add_edit_kdou_sw'],
      rechargeImgSw: json['recharge_img_sw'],
      staticServers: List<String>.from(json['staticServers']),
      loginsessid: json['loginsessid'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'host': host,
      'port': port,
      'apiHost': apiHost,
      'siteId': siteId,
      'merId': merId,
      'webName': webName,
      'zxkfUrl': zxkfUrl,
      'allowreg': allowreg,
      'closeregreason': closeregreason,
      'agent_m_apply': agentMApply,
      'page_title': pageTitle,
      'login_to': loginTo,
      'member_agent': memberAgent,
      'm_promote_pos': mPromotePos,
      'agentRegbutton': agentRegbutton,
      'pass_length_min': passLengthMin,
      'pass_length_max': passLengthMax,
      'pass_limit': passLimit,
      'myreco_img': myrecoImg,
      'missionName': missionName,
      'hide_reco': hideReco,
      'reg_name': regName,
      'reg_fundpwd': regFundpwd,
      'reg_qq': regQq,
      'reg_wx': regWx,
      'reg_phone': regPhone,
      'reg_fb': regFb,
      'reg_zalo': regZalo,
      'smsVerify': smsVerify,
      'reg_vcode': regVcode,
      'reg_email': regEmail,
      'bet_bank_card': betBankCard,
      'reg_line': regLine,
      'googleVerifier': googleVerifier,
      'znxNotify': znxNotify,
      'regAndLogin': regAndLogin,
      'smsFindPassword': smsFindPassword,
      'user_length_limit_switch': userLengthLimitSwitch,
      'user_length_min': userLengthMin,
      'user_length_max': userLengthMax,
      'oftenLoginArea': oftenLoginArea,
      'missionSwitch': missionSwitch,
      'isIntToMoney': isIntToMoney,
      'missionBili': missionBili,
      'checkinSwitch': checkinSwitch,
      'mkCheckinSwitch': mkCheckinSwitch,
      'chatapp_logo': chatappLogo,
      'chatapp_start_logo': chatappStartLogo,
      'mobile_logo': mobileLogo,
      'mobile_icon': mobileIcon,
      'missionPopUpSwitch': missionPopUpSwitch,
      'guestSwitch': guestSwitch,
      'mission_logo': missionLogo,
      'minWithdrawMoney': minWithdrawMoney,
      'maxWithdrawMoney': maxWithdrawMoney,
      'announce_first': announceFirst,
      'adSliderTimer': adSliderTimer,
      'popup_tab': popupTab,
      'popup_announce': popupAnnounce,
      'popup_hour': popupHour,
      'popup_type': popupType,
      'switchDOYUSDTTrans': switchDOYUSDTTrans,
      'yuebaoName': yuebaoName,
      'allowMemberCancelBet': allowMemberCancelBet,
      'balanceDecimal': balanceDecimal,
      'chaseNumber': chaseNumber,
      'selectNumber': selectNumber,
      'switchBindVerify': switchBindVerify,
      'switchShowFriendReferral': switchShowFriendReferral,
      'showNavigationBar': showNavigationBar,
      'rankingListSwitch': rankingListSwitch,
      'grabRedbagRankingListSwitch': grabRedbagRankingListSwitch,
      'chatRoomSwitch': chatRoomSwitch,
      'chatRoomName': chatRoomName,
      'chatFollowSwitch': chatFollowSwitch,
      'chatShareBetMinAmount': chatShareBetMinAmount,
      'mobileGameHall': mobileGameHall,
      'betMin': betMin,
      'betMax': betMax,
      'customiseBetMax': customiseBetMax,
      'switchBalanceChannel': switchBalanceChannel,
      'balanceChannelStartTime': balanceChannelStartTime,
      'balanceChannelEndTime': balanceChannelEndTime,
      'balanceChannelPrompt': balanceChannelPrompt,
      'switchYuebaoChannel': switchYuebaoChannel,
      'yuebaoChannelStartTime': yuebaoChannelStartTime,
      'yuebaoChannelEndTime': yuebaoChannelEndTime,
      'yuebaoChannelPrompt': yuebaoChannelPrompt,
      'mobileHomeGameTypeSwitch': mobileHomeGameTypeSwitch,
      'picTypeshow': picTypeshow,
      'apiHosts': apiHosts,
      'curLangCode': curLangCode,
      'allowMemberModifyLanguage': allowMemberModifyLanguage,
      'mobileTemplateCategory': mobileTemplateCategory,
      'mobileTemplateBackground': mobileTemplateBackground,
      'mobileTemplateStyle': mobileTemplateStyle,
      'mobileViewModel': mobileViewModel,
      'mobileTemplateLhcStyle': mobileTemplateLhcStyle,
      'mobileTemplateGpkStyle': mobileTemplateGpkStyle,
      'mobileTemplateHBStyle': mobileTemplateHBStyle,
      'mobileTemplateXbjStyle': mobileTemplateXbjStyle,
      'mobileMenu': mobileMenu?.map((menu) => menu.toJson()).toList(),
      'kjwTemplateSp': kjwTemplateSp,
      'kjwLotteries': kjwLotteries,
      'serviceQQ1': serviceQQ1,
      'serviceQQ2': serviceQQ2,
      'facekfUrl': facekfUrl,
      'whatskfUrl': whatskfUrl,
      'appPopupQqNum': appPopupQqNum,
      'appPopupQqImg': appPopupQqImg,
      'appPopupWechatNum': appPopupWechatNum,
      'appPopupWechatImg': appPopupWechatImg,
      'withdrawChannelAd': withdrawChannelAd,
      'zxkfUrl2': zxkfUrl2,
      'zalokfUrl': zalokfUrl,
      'telegramkfUrl': telegramkfUrl,
      'shortCut': shortCut,
      'shortCutIcon': shortCutIcon,
      'iosRelease': iosRelease,
      'yuebaoSwitch': yuebaoSwitch,
      'easyRememberDomain': easyRememberDomain,
      'loginVCode': loginVCode,
      'loginVCodeType': loginVCodeType,
      'login_chatApp_vcode': loginChatAppVcode,
      'login_chatApp_vcode_type': loginChatAppVcodeType,
      'chatRoomServer': chatRoomServer,
      'userCenter': userCenters?.map((center) => center.toJson()).toList(),
      'lhcdocSwitchMobileHome': lhcdocSwitchMobileHome,
      'userCenterCategoryList':
          userCenterCategoryList?.map((category) => category.toJson()).toList(),
      'lhcdocMiCard': lhcdocMiCard,
      'lhcPriceList': lhcPriceList?.map((price) => price.toJson()).toList(),
      'lhcdocIsShowNav': lhcdocIsShowNav,
      'lhcdocUserContCheck': lhcdocUserContCheck,
      'lhcdocUserContRepCheck': lhcdocUserContRepCheck,
      'qdSwitchDe': qdSwitchDe,
      'qdSwitchWi': qdSwitchWi,
      'qdSwitch': qdSwitch,
      'qd_supChannels':
          qdSupChannels?.map((channel) => channel.toJson()).toList(),
      'appDownloadUrl': appDownloadUrl,
      'appdownloadbar': appdownloadbar,
      'domainBindAgentId': domainBindAgentId,
      'proxyDomainDefault': proxyDomainDefault,
      'chatLink': chatLink,
      'switchAgentRecharge': switchAgentRecharge,
      'nameAgentRecharge': nameAgentRecharge,
      'switchShowActivityCategory': switchShowActivityCategory,
      'betAmountIsDecimal': betAmountIsDecimal,
      'activeReturnCoinStatus': activeReturnCoinStatus,
      'activeReturnCoinRatio': activeReturnCoinRatio,
      'autoTransferLimit': autoTransferLimit,
      'switchAutoTransfer': switchAutoTransfer,
      'currency': currency,
      'switchToK': switchToK,
      'appSelectType': appSelectType,
      'reg_login_bg_sw': regLoginBgSw,
      'reg_login_bg': regLoginBg,
      'oauth': oauth?.toJson(),
      'loginNotice': loginNotice?.toJson(),
      'inviteCode': inviteCode?.toJson(),
      'inviteWord': inviteWord,
      'inviteCodeSwitch': inviteCodeSwitch,
      'regBindCardSwitch': regBindCardSwitch,
      'frontend_agent_add_member': frontendAgentAddMember,
      'regNameSwitch': regNameSwitch,
      'switchAgentRechargeDML': switchAgentRechargeDML,
      'isAgentRechargeNeededPwd': isAgentRechargeNeededPwd,
      'switchCoinPwd': switchCoinPwd,
      'switchCoinPwdSms': switchCoinPwdSms,
      'coinPwdAuditOptionAry': coinPwdAuditOptionAry,
      'mBonsSwitch': mBonsSwitch,
      'add_edit_bank_sw': addEditBankSw,
      'add_edit_alipay_sw': addEditAlipaySw,
      'add_edit_wechat_sw': addEditWechatSw,
      'add_edit_vcoin_sw': addEditVcoinSw,
      'add_edit_kdou_sw': addEditKdouSw,
      'recharge_img_sw': rechargeImgSw,
      'staticServers': staticServers,
      'loginsessid': loginsessid,
    };
  }
}

class AvatarImageData {
  String? id;
  String? url;

  AvatarImageData({
    this.id,
    this.url,
  });

  factory AvatarImageData.fromJson(Map<String, dynamic> json) {
    return AvatarImageData(
      id: json['id'],
      url: json['url'],
    );
  }
}

class AvatarSettingModel {
  int? isAcceptUpload;
  int? isReview;
  List<AvatarImageData>? avatarList;

  AvatarSettingModel({
    this.isAcceptUpload,
    this.isReview,
    this.avatarList,
  });

  factory AvatarSettingModel.fromJson(Map<String, dynamic> json) {
    var publicAvatarList = json['publicAvatarList'] as List;
    List<AvatarImageData> avatarlist =
        publicAvatarList.map((i) => AvatarImageData.fromJson(i)).toList();

    return AvatarSettingModel(
      isAcceptUpload: json['isAcceptUpload'],
      isReview: json['isReview'],
      avatarList: avatarlist,
    );
  }
}
