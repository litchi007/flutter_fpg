class UGSignInHistoryModel {
  final String idKey;
  final String name;
  final String imgUrl;
  final String remark;

  UGSignInHistoryModel({
    required this.idKey,
    required this.name,
    required this.imgUrl,
    required this.remark,
  });
}
