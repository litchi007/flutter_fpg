class GameCategory {
  String? category;
  String? categoryName;
  List<Game>? games;

  GameCategory({
    this.category,
    this.categoryName,
    this.games,
  });

  factory GameCategory.fromJson(Map<String, dynamic> json) {
    return GameCategory(
      category: json['category'],
      categoryName: json['categoryName'],
      games: (json['games'] as List?)?.map((i) => Game.fromJson(i)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'category': category,
      'categoryName': categoryName,
      'games': games?.map((e) => e.toJson()).toList(),
    };
  }
}

class Game {
  String? id;
  String? isSeal;
  String? isClose;
  String? isHot;
  String? name;
  String? title;
  String? fromType;
  String? isInstant;
  String? gameType;
  String? gameTypeName;
  String? pic;

  String? sort;
  String? mobile;
  String? pc;
  String? category;
  String? shortName;
  String? gameCat;
  String? enable;
  int? isPopup;
  String? gameSymbol;
  String? currency;
  int? surrpotTrial;

  Game({
    this.id,
    this.isSeal,
    this.isClose,
    this.isHot,
    this.name,
    this.title,
    this.fromType,
    this.isInstant,
    this.gameType,
    this.gameTypeName,
    this.pic,
    this.sort,
    this.mobile,
    this.pc,
    this.category,
    this.shortName,
    this.gameCat,
    this.enable,
    this.isPopup,
    this.gameSymbol,
    this.currency,
    this.surrpotTrial,
  });

  factory Game.fromJson(Map<String, dynamic> json) {
    return Game(
      id: json['id'],
      isSeal: json['isSeal'],
      isClose: json['isClose'],
      isHot: json['isHot'],
      name: json['name'],
      title: json['title'],
      fromType: json['from_type'],
      isInstant: json['isInstant'],
      gameType: json['gameType'],
      gameTypeName: json['gameTypeName'],
      pic: json['pic'],
      sort: json['sort'],
      mobile: json['mobile'],
      pc: json['pc'],
      category: json['category'],
      shortName: json['short_name'],
      gameCat: json['gameCat'],
      enable: json['enable'],
      isPopup: json['isPopup'],
      gameSymbol: json['gameSymbol'],
      currency: json['currency'],
      surrpotTrial: json['surrpotTrial'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'isSeal': isSeal,
      'isClose': isClose,
      'isHot': isHot,
      'name': name,
      'title': title,
      'from_type': fromType,
      'isInstant': isInstant,
      'gameType': gameType,
      'gameTypeName': gameTypeName,
      'pic': pic,
      'sort': sort,
      'mobile': mobile,
      'pc': pc,
      'category': category,
      'short_name': shortName,
      'gameCat': gameCat,
      'enable': enable,
      'isPopup': isPopup,
      'gameSymbol': gameSymbol,
      'currency': currency,
      'surrpotTrial': surrpotTrial,
    };
  }
}
