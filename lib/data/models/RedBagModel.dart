class RedBagModel {
  String? roomId;
  String? id;
  String? uid;
  dynamic createTime;
  dynamic expireTime;
  String? title;
  String? genre;
  String? amount;
  dynamic status;
  dynamic quantity;
  String? isGrab;
  String? grabAmount;
  List<dynamic>? grabList;

  RedBagModel({
    this.roomId,
    this.id,
    this.uid,
    this.createTime,
    this.title,
    this.genre,
    this.amount,
    this.status,
    this.quantity,
    this.isGrab,
    this.grabAmount,
    this.grabList,
    this.expireTime,
  });

  factory RedBagModel.fromJson(Map<String, dynamic> json) {
    return RedBagModel(
      roomId: json['roomId']?.toString(),
      id: json['id']?.toString(),
      uid: json['uid']?.toString(),
      createTime: json['createTime'],
      expireTime: json['expireTime'],
      title: json['title']?.toString(),
      genre: json['genre']?.toString(),
      amount: json['amount']?.toString(),
      status: json['status'],
      quantity: json['quantity'],
      isGrab: json['is_grab']?.toString(),
      grabAmount: json['grab_amount']?.toString(),
      grabList: List<dynamic>.from(json['grabList']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'roomId': roomId,
      'id': id,
      'uid': uid,
      'createTime': createTime,
      'expireTime': expireTime,
      'title': title,
      'genre': genre,
      'amount': amount,
      'status': status,
      'quantity': quantity,
      'is_grab': isGrab,
      'grab_amount': grabAmount.toString(),
      'grabList': grabList,
    };
  }
}
