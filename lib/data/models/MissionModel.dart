// 俸禄列表
class SalaryModel {
  String? MonthBons; //月俸禄
  String? bonsId;
  String? levelName;
  String? weekBons; //周俸禄
  SalaryModel({
    this.MonthBons,
    this.bonsId,
    this.weekBons,
    this.levelName,
  });
  factory SalaryModel.fromJson(Map<String, dynamic> json) {
    return SalaryModel(
      MonthBons: json['MonthBons']?.toString(),
      bonsId: json['bonsId']?.toString(),
      levelName: json['levelName']?.toString(),
      weekBons: json['weekBons']?.toString(),
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'MonthBons': MonthBons,
      'bonsId': bonsId,
      'levelName': levelName,
      'weekBons': weekBons,
    };
  }
}

class NewSalaryModel {
  final Bons? bons;
  final String? monthSetStatus;
  final String? weekSetStatus;
  final Need? need;

  NewSalaryModel({
    this.bons,
    this.monthSetStatus,
    this.weekSetStatus,
    this.need,
  });

  factory NewSalaryModel.fromJson(Map<String, dynamic> json) {
    return NewSalaryModel(
      bons: json['bons'] != null ? Bons.fromJson(json['bons']) : null,
      monthSetStatus: json['monthSetStatus']?.toString(),
      weekSetStatus: json['weekSetStatus']?.toString(),
      need: json['need'] != null ? Need.fromJson(json['need']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'bons': bons?.toJson(),
      'monthSetStatus': monthSetStatus,
      'weekSetStatus': weekSetStatus,
      'need': need?.toJson(),
    };
  }
}

class Bons {
  final WeekBons? week;
  final MonthBons? month;

  Bons({
    this.week,
    this.month,
  });

  factory Bons.fromJson(Map<String, dynamic> json) {
    return Bons(
      week: json['week'] != null ? WeekBons.fromJson(json['week']) : null,
      month: json['month'] != null ? MonthBons.fromJson(json['month']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'week': week?.toJson(),
      'month': month?.toJson(),
    };
  }
}

class WeekBons {
  final String? bonsId;
  final String? weekBonsAmount;
  final String? weekBonsStatus;

  WeekBons({
    this.bonsId,
    this.weekBonsAmount,
    this.weekBonsStatus,
  });

  factory WeekBons.fromJson(Map<String, dynamic> json) {
    return WeekBons(
      bonsId: json['bonsId']?.toString(),
      weekBonsAmount: json['weekBonsAmount']?.toString(),
      weekBonsStatus: json['weekBonsStatus']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'bonsId': bonsId,
      'weekBonsAmount': weekBonsAmount,
      'weekBonsStatus': weekBonsStatus,
    };
  }
}

class MonthBons {
  final String? bonsId;
  final String? monthBonsAmount;
  final String? monthBonsStatus;

  MonthBons({
    this.bonsId,
    this.monthBonsAmount,
    this.monthBonsStatus,
  });

  factory MonthBons.fromJson(Map<String, dynamic> json) {
    return MonthBons(
      bonsId: json['bonsId']?.toString(),
      monthBonsAmount: json['monthBonsAmount']?.toString(),
      monthBonsStatus: json['monthBonsStatus']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'bonsId': bonsId,
      'monthBonsAmount': monthBonsAmount,
      'monthBonsStatus': monthBonsStatus,
    };
  }
}

class Need {
  final WeekNeed? week;
  final MonthNeed? month;

  Need({
    this.week,
    this.month,
  });

  factory Need.fromJson(Map<String, dynamic> json) {
    return Need(
      week: json['week'] != null ? WeekNeed.fromJson(json['week']) : null,
      month: json['month'] != null ? MonthNeed.fromJson(json['month']) : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'week': week?.toJson(),
      'month': month?.toJson(),
    };
  }
}

class WeekNeed {
  final String? weekBonsNeed;
  final String? weekIntegral;

  WeekNeed({
    this.weekBonsNeed,
    this.weekIntegral,
  });

  factory WeekNeed.fromJson(Map<String, dynamic> json) {
    return WeekNeed(
      weekBonsNeed: json['weekBonsNeed']?.toString(),
      weekIntegral: json['weekIntegral']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'weekBonsNeed': weekBonsNeed,
      'weekIntegral': weekIntegral,
    };
  }
}

class MonthNeed {
  final String? monthBonsNeed;
  final String? monthIntegral;

  MonthNeed({
    this.monthBonsNeed,
    this.monthIntegral,
  });

  factory MonthNeed.fromJson(Map<String, dynamic> json) {
    return MonthNeed(
      monthBonsNeed: json['monthBonsNeed']?.toString(),
      monthIntegral: json['monthIntegral']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'monthBonsNeed': monthBonsNeed,
      'monthIntegral': monthIntegral,
    };
  }
}

class ScoreLevelData {
  String? params;
  String? id;
  String? checkinCards;
  String? levelTitle;
  String? integral;
  String? levelName;
  String? levelDesc;

  // Constructor
  ScoreLevelData({
    this.params,
    this.id,
    this.checkinCards,
    this.levelTitle,
    this.integral,
    this.levelName,
    this.levelDesc,
  });
  factory ScoreLevelData.fromJson(Map<String, dynamic> json) {
    return ScoreLevelData(
      params: json['params'],
      id: json['id'],
      checkinCards: json['checkinCards'],
      levelTitle: json['levelTitle'],
      integral: json['integral'],
      levelName: json['levelName'],
      levelDesc: json['levelDesc'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'params': params,
      'id': id,
      'checkinCards': checkinCards,
      'levelTitle': levelTitle,
      'integral': integral,
      'levelName': levelName,
      'levelDesc': levelDesc,
    };
  }
}

class CreditsUList {
  String? id;
  String? mid;
  String? type;
  String? integral;
  String? oldInt;
  String? newInt;
  String? addTime;

  CreditsUList({
    this.id,
    this.mid,
    this.type,
    this.integral,
    this.oldInt,
    this.newInt,
    this.addTime,
  });

  factory CreditsUList.fromJson(Map<String, dynamic> json) {
    return CreditsUList(
      id: json['id'],
      mid: json['mid'],
      type: json['type'],
      integral: json['integral'],
      oldInt: json['oldInt'],
      newInt: json['newInt'],
      addTime: json['addTime'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'mid': mid,
      'type': type,
      'integral': integral,
      'oldInt': oldInt,
      'newInt': newInt,
      'addTime': addTime,
    };
  }
}

class CreditsData {
  int? total;
  List<CreditsUList>? list;
  CreditsData({
    this.total,
    this.list,
  });
  factory CreditsData.fromJson(Map<String, dynamic> json) {
    List<dynamic>? listData = json['list'];
    List<CreditsUList>? list;
    if (listData != null) {
      list = listData.map((item) => CreditsUList.fromJson(item)).toList();
    }
    return CreditsData(total: json['total'] as int, list: list);
  }

  Map<String, dynamic> toJson() {
    return {
      'total': total,
      'list': list?.map((item) => item.toJson()).toList(),
    };
  }
}

class CreditsLogModel {
  String? code;
  String? msg;
  CreditsData? data;
  CreditsLogModel({
    this.code,
    this.msg,
    this.data,
  });
  factory CreditsLogModel.fromJson(Map<String, dynamic> json) {
    return CreditsLogModel(
      code: json['code'],
      msg: json['msg'],
      data: CreditsData.fromJson(json),
    );
  }
}
