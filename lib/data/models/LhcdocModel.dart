class TipsRankingData {
  int? id;
  String? username;
  double? totalAmount; // Changed to double if amount can have decimal points

  // Constructor
  TipsRankingData({
    this.id,
    this.username,
    this.totalAmount,
  });

  factory TipsRankingData.fromJson(Map<String, dynamic> json) {
    return TipsRankingData(
      id: json['id'],
      username: json['username'],
      totalAmount: (json['totalAmount'] as num).toDouble(), // Convert to double
    );
  }
}

class TipsRanking {
  int? enable;
  List<TipsRankingData>? data;

  // Constructor
  TipsRanking({
    this.enable,
    this.data,
  });

  factory TipsRanking.fromJson(Map<String, dynamic> json) {
    return TipsRanking(
      enable: json['enable'],
      data: (json['data'] as List<dynamic>)
          .map((item) => TipsRankingData.fromJson(item as Map<String, dynamic>))
          .toList(),
    );
  }
}

class FansRankingData {
  int? id;
  String? username;
  int? fansNum;

  FansRankingData({
    this.id,
    this.username,
    this.fansNum,
  });

  factory FansRankingData.fromJson(Map<String, dynamic> json) {
    return FansRankingData(
      id: json['id'] as int,
      username: json['username'],
      fansNum: json['fansNum'] as int,
    );
  }
}

class FansRanking {
  String? enable;
  List<FansRankingData>? data;

  FansRanking({
    this.enable,
    this.data,
  });

  factory FansRanking.fromJson(Map<String, dynamic> json) {
    List<dynamic>? dataList = json['data'];
    List<FansRankingData>? data;
    if (dataList != null) {
      data = dataList.map((item) => FansRankingData.fromJson(item)).toList();
    }
    return FansRanking(
      enable: json['enable'].toString(),
      data: data,
    );
  }
}

class LoginRedPageInfoModel {
  String? logo;
  String? intro;

  LoginRedPageInfoModel({this.logo, this.intro});

  factory LoginRedPageInfoModel.fromJson(Map<String, dynamic> json) {
    return LoginRedPageInfoModel(
        logo: json['logo']?.toString(), intro: json['intro']?.toString());
  }

  Map<String, dynamic> toJson() {
    return {'logo': logo, 'intro': intro};
  }
}
