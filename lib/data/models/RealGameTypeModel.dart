class RealGameTypeModel {
  String? code;
  String? name;
  String? pic;

  RealGameTypeModel({
    this.code,
    this.name,
    this.pic,
  });

  // Factory constructor for creating a new instance from a map
  factory RealGameTypeModel.fromJson(Map<String, dynamic> json) {
    return RealGameTypeModel(
      code: json['code'].toString(),
      name: json['name'],
      pic: json['pic'],
    );
  }

  // Method to convert an instance to a map
  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'name': name,
      'pic': pic,
    };
  }
}
