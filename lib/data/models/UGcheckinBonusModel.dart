class UGcheckinBonusModel {
  String? clsName = 'UGcheckinBonusModel';
  String? int;

  /// <   20趣味豆
  String? switchs;

  /// <   签到开关，1显示0隐藏
  bool? isComplete;

  /// <   是否已经领取  true   是，   false  否
  bool? isCheckin; /**<   是否可以领取  true   是，   false  否 */
}

class UGCheckinListModel {
  String clsName = 'UGCheckinListModel';
  String? week;

  /// <   星期一
  double? whichDay;

  /// <   日期 //2019-09-02 //???? String to double
  double? integral;

  /// <   10 金币
  bool? isCheckin;

  /// <   是否已经签到  true 已经签到   false 还没有签到
  bool? isMakeup;

  /// <   是否可以补签   true 可以   false 不可以
  String? serverTime;

  /// <   服务器时间==>本地
  String? mkCheckinSwitch; /**<   补签总开关  true  开，   false  关 */
}

class UGSignInModel {
  String? clsName = 'UGSignInModel';
  double? serverTime;

  /// <   服务器时间  //???? String to double
  double? checkinTimes;

  /// <   连续签到多少天
  String? checkinMoney;

  /// <   积分
  bool? checkinSwitch;

  /// <   签到总开关  true   开，   false  关
  bool? mkCheckinSwitch;

  /// <   补签总开关  true  开，   false  关
  List<UGCheckinListModel>? checkinList;
  List<UGcheckinBonusModel>? checkinBonus;
}
