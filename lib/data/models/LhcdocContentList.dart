class LhcdocContentList {
  String? cid;
  String? enable;
  String? alias;
  String? uid;
  String? nickname;
  String? headImg;
  String? title;
  String? hyperlink;
  String? content;
  String? fullContent;
  List<dynamic>? contentPic;
  String? periods;
  String? isShowNo;
  String? openWay;
  String? fullLhcno;
  String? isHot;
  String? likeNum;
  String? viewNum;
  dynamic favNum;
  dynamic isLike;
  String? isFollow;
  String? createTime;
  dynamic replyCount;
  String? isTop;
  String? price;
  dynamic isLhcdocVip;
  dynamic isAdmin;
  String? inListBackgroundColor;
  dynamic inListBackgroundColorTransparent;
  String? type2;
  String? type;
  String? model;
  dynamic hasPay;
  String? isFav;

  LhcdocContentList({
    this.cid,
    this.enable,
    this.alias,
    this.uid,
    this.nickname,
    this.headImg,
    this.title,
    this.hyperlink,
    this.content,
    this.fullContent,
    this.contentPic,
    this.periods,
    this.isShowNo,
    this.openWay,
    this.fullLhcno,
    this.isHot,
    this.likeNum,
    this.viewNum,
    this.favNum,
    this.isLike,
    this.isFollow,
    this.createTime,
    this.replyCount,
    this.isTop,
    this.price,
    this.isLhcdocVip,
    this.isAdmin,
    this.inListBackgroundColor,
    this.inListBackgroundColorTransparent,
    this.type2,
    this.type,
    this.model,
    this.hasPay,
    this.isFav,
  });

  factory LhcdocContentList.fromJson(Map<String, dynamic> json) {
    return LhcdocContentList(
      cid: json['cid'] as String?,
      enable: json['enable'] as String?,
      alias: json['alias'] as String?,
      uid: json['uid'] as String?,
      nickname: json['nickname'] as String?,
      headImg: json['headImg'] as String?,
      title: json['title'] as String?,
      hyperlink: json['hyperlink'] as String?,
      content: json['content'] as String?,
      fullContent: json['fullContent'] as String?,
      contentPic: json['contentPic'] as List<dynamic>?,
      periods: json['periods'] as String?,
      isShowNo: json['is_showNo'] as String?,
      openWay: json['openWay'] as String?,
      fullLhcno: json['full_lhcno'] as String?,
      isHot: json['isHot'] as String?,
      likeNum: json['likeNum'] as String?,
      viewNum: json['viewNum'] as String?,
      favNum: json['favNum'] as dynamic,
      isLike: json['isLike'] as dynamic,
      isFollow: json['isFollow'] as String?,
      createTime: json['createTime'] as String?,
      replyCount: json['replyCount'] as dynamic,
      isTop: json['isTop'] as String?,
      price: json['price'] as String?,
      isLhcdocVip: json['isLhcdocVip'] as dynamic,
      isAdmin: json['isAdmin'] as dynamic,
      inListBackgroundColor: json['inListBackgroundColor'] as String?,
      inListBackgroundColorTransparent:
          json['inListBackgroundColorTransparent'] as dynamic,
      type2: json['type2'] as String?,
      type: json['type'] as String?,
      model: json['model'] as String?,
      hasPay: json['hasPay'] as dynamic,
      isFav: json['isFav'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'cid': cid,
      'enable': enable,
      'alias': alias,
      'uid': uid,
      'nickname': nickname,
      'headImg': headImg,
      'title': title,
      'hyperlink': hyperlink,
      'content': content,
      'fullContent': fullContent,
      'contentPic': contentPic,
      'periods': periods,
      'is_showNo': isShowNo,
      'openWay': openWay,
      'full_lhcno': fullLhcno,
      'isHot': isHot,
      'likeNum': likeNum,
      'viewNum': viewNum,
      'favNum': favNum,
      'isLike': isLike,
      'isFollow': isFollow,
      'createTime': createTime,
      'replyCount': replyCount,
      'isTop': isTop,
      'price': price,
      'isLhcdocVip': isLhcdocVip,
      'isAdmin': isAdmin,
      'inListBackgroundColor': inListBackgroundColor,
      'inListBackgroundColorTransparent': inListBackgroundColorTransparent,
      'type2': type2,
      'type': type,
      'model': model,
      'hasPay': hasPay,
      'isFav': isFav,
    };
  }
}
