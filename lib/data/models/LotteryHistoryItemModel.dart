class LotteryHistoryItemModel {
  String? issue;
  String? displayNumber;
  String? openTime;
  String? num;
  String? gameType;
  String? hash;
  String? verifUrl;
  String? result;

  LotteryHistoryItemModel({
    this.issue,
    this.displayNumber,
    this.openTime,
    this.num,
    this.gameType,
    this.hash,
    this.verifUrl,
    this.result,
  });

  factory LotteryHistoryItemModel.fromJson(Map<String, dynamic> json) {
    return LotteryHistoryItemModel(
      issue: json['issue'],
      displayNumber: json['displayNumber'],
      openTime: json['openTime'],
      num: json['num'],
      gameType: json['gameType'],
      hash: json['hash'],
      verifUrl: json['verifUrl'],
      result: json['result'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'issue': issue,
      'displayNumber': displayNumber,
      'openTime': openTime,
      'num': num,
      'gameType': gameType,
      'hash': hash,
      'verifUrl': verifUrl,
      'result': result,
    };
  }
}
