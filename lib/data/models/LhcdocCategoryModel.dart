class LhcdocCategoryModel {
  String? id;
  String? name;
  String? alias;
  String? desc;
  String? icon;
  String? isHot;
  String? link;
  String? readPri;
  String? readPriLevels;
  String? siteTagsId;
  String? openWay;
  String? model;
  String? iconIsShowHome;
  String? appLink;
  String? appLinkCode;
  String? contentId;
  String? contentType;
  String? threadType;
  String? baomaType;
  String? tagTypePid;
  String? tagType;
  String? sort;
  String? lanmuList;
  dynamic tag;

  LhcdocCategoryModel({
    this.id,
    this.name,
    this.alias,
    this.desc,
    this.icon,
    this.isHot,
    this.link,
    this.readPri,
    this.readPriLevels,
    this.siteTagsId,
    this.openWay,
    this.model,
    this.iconIsShowHome,
    this.appLink,
    this.appLinkCode,
    this.contentId,
    this.contentType,
    this.threadType,
    this.baomaType,
    this.tagTypePid,
    this.tagType,
    this.sort,
    this.lanmuList,
    this.tag,
  });

  factory LhcdocCategoryModel.fromJson(Map<String, dynamic> json) {
    return LhcdocCategoryModel(
      id: json['id'],
      name: json['name'],
      alias: json['alias'],
      desc: json['desc'],
      icon: json['icon'],
      isHot: json['isHot'],
      link: json['link'],
      readPri: json['read_pri'],
      readPriLevels: json['read_pri_levels'],
      siteTagsId: json['site_tags_id'],
      openWay: json['openWay'],
      model: json['model'],
      iconIsShowHome: json['iconIsShowHome'],
      appLink: json['appLink'],
      appLinkCode: json['appLinkCode'],
      contentId: json['contentId'],
      contentType: json['contentType'],
      threadType: json['thread_type'],
      baomaType: json['baoma_type'],
      tagTypePid: json['tag_type_pid'],
      tagType: json['tag_type'],
      sort: json['sort'],
      lanmuList: json['lanmu_list'],
      tag: json['tag'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'alias': alias,
      'desc': desc,
      'icon': icon,
      'isHot': isHot,
      'link': link,
      'read_pri': readPri,
      'read_pri_levels': readPriLevels,
      'site_tags_id': siteTagsId,
      'openWay': openWay,
      'model': model,
      'iconIsShowHome': iconIsShowHome,
      'appLink': appLink,
      'appLinkCode': appLinkCode,
      'contentId': contentId,
      'contentType': contentType,
      'thread_type': threadType,
      'baoma_type': baomaType,
      'tag_type_pid': tagTypePid,
      'tag_type': tagType,
      'sort': sort,
      'lanmu_list': lanmuList,
      'tag': tag
    };
  }
}
