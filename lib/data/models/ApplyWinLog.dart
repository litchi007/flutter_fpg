class ListDataForWinLog {
  String? amount; // 申请金额
  String? winName; // 活动名称
  String? state; // 审核结果
  String? id; // ID
  String? updateTime; // 更新时间

  ListDataForWinLog({
    this.amount,
    this.winName,
    this.state,
    this.id,
    this.updateTime,
  });

  factory ListDataForWinLog.fromJson(Map<String, dynamic> json) {
    return ListDataForWinLog(
      amount: json['amount'],
      winName: json['winName'],
      state: json['state'],
      id: json['id'],
      updateTime: json['updateTime'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'amount': amount,
      'winName': winName,
      'state': state,
      'id': id,
      'updateTime': updateTime,
    };
  }
}

class ApplyWinLog {
  int? total;
  List<ListDataForWinLog>? list;

  ApplyWinLog({
    this.total,
    this.list,
  });

  factory ApplyWinLog.fromJson(Map<String, dynamic> json) {
    var lists = json['list'] as List?;
    List<ListDataForWinLog> parsedList =
        lists?.map((e) => ListDataForWinLog.fromJson(e)).toList() ?? [];

    return ApplyWinLog(
      total: json['total'],
      list: parsedList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'total': total,
      'list': list?.map((e) => e.toJson()).toList(),
    };
  }
}
