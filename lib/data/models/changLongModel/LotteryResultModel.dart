class LotteryResultData {
  // Nullable properties
  final String? openNum; // e.g., "09,49,02,40,05,42,07"
  final String? bonus; // e.g., "0.0000"
  final String? gameType; // e.g., "lhc"
  final String? color; // e.g., "blue,green,red,red,green,blue,red"
  final String? sx; // e.g., "龙,鼠,猪,鸡,猴,羊,马"
  final String? result; // e.g., "龙,鼠,猪,鸡,猴,羊,马"

  // Nullable custom object properties
  final IBetLotteryParams? betParams;
  final BetShareModel? betShareModel;
  final BetShareModel? orgShareModel;

  LotteryResultData({
    this.openNum,
    this.bonus,
    this.gameType,
    this.color,
    this.sx,
    this.result,
    this.betParams,
    this.betShareModel,
    this.orgShareModel,
  });

  // Convert a JSON map to a LotteryResultData instance
  factory LotteryResultData.fromJson(Map<String, dynamic> json) {
    return LotteryResultData(
      openNum: json['openNum'] as String?,
      bonus: json['bonus'] as String?,
      gameType: json['gameType'] as String?,
      color: json['color'] as String?,
      sx: json['sx'] as String?,
      result: json['result'] as String?,
      betParams: json['betParams'] != null
          ? IBetLotteryParams.fromJson(json['betParams'])
          : null,
      betShareModel: json['betShareModel'] != null
          ? BetShareModel.fromJson(json['betShareModel'])
          : null,
      orgShareModel: json['orgShareModel'] != null
          ? BetShareModel.fromJson(json['orgShareModel'])
          : null,
    );
  }

  // Convert a LotteryResultData instance to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'openNum': openNum,
      'bonus': bonus,
      'gameType': gameType,
      'color': color,
      'sx': sx,
      'result': result,
      'betParams': betParams?.toJson(),
      'betShareModel': betShareModel?.toJson(),
      'orgShareModel': orgShareModel?.toJson(),
    };
  }
}

// Example of a custom class IBetLotteryParams
class IBetLotteryParams {
  final String? someParam;

  IBetLotteryParams({this.someParam});

  factory IBetLotteryParams.fromJson(Map<String, dynamic> json) {
    return IBetLotteryParams(
      someParam: json['someParam'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'someParam': someParam,
    };
  }
}

// Example of a custom class BetShareModel
class BetShareModel {
  final String? shareId;

  BetShareModel({this.shareId});

  factory BetShareModel.fromJson(Map<String, dynamic> json) {
    return BetShareModel(
      shareId: json['shareId'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'shareId': shareId,
    };
  }
}
