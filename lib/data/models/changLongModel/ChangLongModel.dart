class ChangLongModel {
  final String? title;
  final List<BetItemModel>? betList;
  final String? playName;
  final String? playCateName;
  final String? gameId;
  final String? playCateId;
  final String? count;
  final int? sort;
  final String? issue;
  final String? closeTime;
  final String? openTime;
  final String? displayNumber;
  final bool? preIsOpen;
  final String? isSeal;
  final String? serverTime;
  final int? lotteryCountdown;
  final int? closeCountdown;
  final String? logo;

  ChangLongModel({
    this.title,
    this.betList,
    this.playName,
    this.playCateName,
    this.gameId,
    this.playCateId,
    this.count,
    this.sort,
    this.issue,
    this.closeTime,
    this.openTime,
    this.displayNumber,
    this.preIsOpen,
    this.isSeal,
    this.serverTime,
    this.lotteryCountdown,
    this.closeCountdown,
    this.logo,
  });

  factory ChangLongModel.fromJson(Map<String, dynamic> json) {
    return ChangLongModel(
      title: json['title'] as String?,
      betList: (json['betList'] as List<dynamic>?)
          ?.map((e) => BetItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      playName: json['playName'] as String?,
      playCateName: json['playCateName'] as String?,
      gameId: json['gameId'] as String?,
      playCateId: json['playCateId'] as String?,
      count: json['count'] as String?,
      sort: json['sort'] as int?,
      issue: json['issue'] as String?,
      closeTime: json['closeTime'] as String?,
      openTime: json['openTime'] as String?,
      displayNumber: json['displayNumber'] as String?,
      preIsOpen: json['preIsOpen'] as bool?,
      isSeal: json['isSeal'] as String?,
      serverTime: json['serverTime'] as String?,
      lotteryCountdown: json['lotteryCountdown'] as int?,
      closeCountdown: json['closeCountdown'] as int?,
      logo: json['logo'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'betList': betList?.map((e) => e.toJson()).toList(),
      'playName': playName,
      'playCateName': playCateName,
      'gameId': gameId,
      'playCateId': playCateId,
      'count': count,
      'sort': sort,
      'issue': issue,
      'closeTime': closeTime,
      'openTime': openTime,
      'displayNumber': displayNumber,
      'preIsOpen': preIsOpen,
      'isSeal': isSeal,
      'serverTime': serverTime,
      'lotteryCountdown': lotteryCountdown,
      'closeCountdown': closeCountdown,
      'logo': logo,
    };
  }
}

class BetItemModel {
  final String? playName;
  final String? playId;
  final String? odds;

  BetItemModel({
    this.playName,
    this.playId,
    this.odds,
  });

  factory BetItemModel.fromJson(Map<String, dynamic> json) {
    return BetItemModel(
      playName: json['playName'] as String?,
      playId: json['playId'] as String?,
      odds: json['odds'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'playName': playName,
      'playId': playId,
      'odds': odds,
    };
  }
}
