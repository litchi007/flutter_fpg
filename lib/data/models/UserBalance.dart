class UserBalance {
  String? balance;
  String? doyBalance;
  String? usdtBalance;

  UserBalance({
    this.balance,
    this.doyBalance,
    this.usdtBalance,
  });

  factory UserBalance.fromJson(Map<String, dynamic> json) {
    return UserBalance(
      balance: json['balance'],
      doyBalance: json['doy_balance'],
      usdtBalance: json['usdt_balance'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['balance'] = balance;
    data['doy_balance'] = doyBalance;
    data['usdt_balance'] = usdtBalance;
    return data;
  }
}
