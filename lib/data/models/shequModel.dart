import 'package:fpg_flutter/data/models/BannersData.dart';

class ShequModel {
  BannersData? banner;
  String? lhcdocShequName;
  String? lhcdocShequIntro;
  ShequModel({
    this.banner,
    this.lhcdocShequIntro,
    this.lhcdocShequName,
  });
  factory ShequModel.fromJson(Map<String, dynamic> json) {
    return ShequModel(
        banner: BannersData.fromJson(json['banner']),
        lhcdocShequIntro: json['lhcdocShequIntro'],
        lhcdocShequName: json['lhcdocShequName']);
  }

  toJson() {}
}
