// import 'package:fpg_flutter/data/models/UGUserCenterItem.dart';
// import 'package:fpg_flutter/utils/helper.dart';

// class LoginNotice {
//   final String? text; // 登录成功文本
//   final String? switch_; // 否显示登录成功文本

//   LoginNotice({this.text, this.switch_});

//   // Factory constructor to create an instance from a map
//   factory LoginNotice.fromJson(Map<String, dynamic> json) {
//     return LoginNotice(
//       text: json['text'] as String?,
//       switch_: json['switch'] as String?,
//     );
//   }

//   // Method to convert an instance to a map
//   Map<String, dynamic> toJson() {
//     return {
//       'text': text,
//       'switch': switch_,
//     };
//   }
// }

// class Platform {
//   final bool? facebook;
//   final bool? sms;
//   final bool? zalo;

//   Platform({this.facebook, this.sms, this.zalo});

//   // Factory constructor to create an instance from a map
//   factory Platform.fromJson(Map<String, dynamic> json) {
//     return Platform(
//       facebook: json['facebook'] as bool?,
//       sms: json['sms'] as bool?,
//       zalo: json['zalo'] as bool?,
//     );
//   }

//   // Method to convert an instance to a map
//   Map<String, dynamic> toJson() {
//     return {
//       'facebook': facebook,
//       'sms': sms,
//       'zalo': zalo,
//     };
//   }
// }

// class OAuth {
//   final List<Platform>? platform;
//   final bool? switch_;

//   OAuth({this.platform, this.switch_});

//   // Factory constructor to create an instance from a map
//   factory OAuth.fromJson(Map<String, dynamic> json) {
//     return OAuth(
//       platform: List<Platform>.from(
//           json['platform']?.map((x) => Platform.fromJson(x))),
//       switch_: json['switch'] as bool?,
//     );
//   }

//   // Method to convert an instance to a map
//   Map<String, dynamic> toJson() {
//     return {
//       'platform': platform,
//       'switch': switch_,
//     };
//   }
// }

// class UserCenterItem {
//   final String bmImgName;
//   final int code;
//   final String defaultImgName;
//   final bool isDefaultLogo;
//   final bool isLoading;
//   final String lhImgName;
//   final String logo;
//   final String name;

//   UserCenterItem({
//     required this.bmImgName,
//     required this.code,
//     required this.defaultImgName,
//     required this.isDefaultLogo,
//     required this.isLoading,
//     required this.lhImgName,
//     required this.logo,
//     required this.name,
//   });

//   // Factory constructor to create an instance from a map
//   factory UserCenterItem.fromJson(Map<String, dynamic> json) {
//     return UserCenterItem(
//       bmImgName: json['bmImgName'] as String,
//       code: json['code'] as int,
//       defaultImgName: json['defaultImgName'] as String,
//       isDefaultLogo: json['isDefaultLogo'] as bool,
//       isLoading: json['isLoading'] as bool,
//       lhImgName: json['lhImgName'] as String,
//       logo: json['logo'] as String,
//       name: json['name'] as String,
//     );
//   }

//   // Method to convert an instance to a map
//   Map<String, dynamic> toJson() {
//     return {
//       'bmImgName': bmImgName,
//       'code': code,
//       'defaultImgName': defaultImgName,
//       'isDefaultLogo': isDefaultLogo,
//       'isLoading': isLoading,
//       'lhImgName': lhImgName,
//       'logo': logo,
//       'name': name,
//     };
//   }
// }

// class InviteCodeConfigModel {
//   final String? codeSwitch;
//   final String? displayWord;
//   final String? canGenNum;
//   final String? canUseNum;
//   final String? randomSwitch;
//   final String? randomLength;
//   final String? noticeSwitch;
//   final String? noticeText;

//   InviteCodeConfigModel({
//     this.codeSwitch,
//     this.displayWord,
//     this.canGenNum,
//     this.canUseNum,
//     this.randomSwitch,
//     this.randomLength,
//     this.noticeSwitch,
//     this.noticeText,
//   });

//   factory InviteCodeConfigModel.fromJson(Map<String, dynamic> json) {
//     return InviteCodeConfigModel(
//       codeSwitch: json['codeSwitch'] as String?,
//       displayWord: json['displayWord'] as String?,
//       canGenNum: json['canGenNum'] as String?,
//       canUseNum: json['canUseNum'] as String?,
//       randomSwitch: json['randomSwitch'] as String?,
//       randomLength: json['randomLength'] as String?,
//       noticeSwitch: json['noticeSwitch'] as String?,
//       noticeText: json['noticeText'] as String?,
//     );
//   }

//   Map<String, dynamic> toJson() {
//     return {
//       'codeSwitch': codeSwitch,
//       'displayWord': displayWord,
//       'canGenNum': canGenNum,
//       'canUseNum': canUseNum,
//       'randomSwitch': randomSwitch,
//       'randomLength': randomLength,
//       'noticeSwitch': noticeSwitch,
//       'noticeText': noticeText,
//     };
//   }
// }

// class UGAgentApplyInfo {
//   final String username;
//   final String qq;
//   final String whatsapp;
//   final String zalo;
//   final String mobile;
//   final String applyReason;
//   final String reviewResult;
//   final int reviewStatus;
//   final bool isAgent;

//   UGAgentApplyInfo({
//     required this.username,
//     required this.qq,
//     required this.whatsapp,
//     required this.zalo,
//     required this.mobile,
//     required this.applyReason,
//     required this.reviewResult,
//     required this.reviewStatus,
//     required this.isAgent,
//   });

//   factory UGAgentApplyInfo.fromJson(Map<String, dynamic> json) {
//     return UGAgentApplyInfo(
//       username: json['username'] as String,
//       qq: json['qq'] as String,
//       whatsapp: json['whatsapp'] as String,
//       zalo: json['zalo'] as String,
//       mobile: json['mobile'] as String,
//       applyReason: json['applyReason'] as String,
//       reviewResult: json['reviewResult'] as String,
//       reviewStatus: json['reviewStatus'] as int,
//       isAgent: json['isAgent'] as bool,
//     );
//   }

//   Map<String, dynamic> toJson() {
//     return {
//       'username': username,
//       'qq': qq,
//       'whatsapp': whatsapp,
//       'zalo': zalo,
//       'mobile': mobile,
//       'applyReason': applyReason,
//       'reviewResult': reviewResult,
//       'reviewStatus': reviewStatus,
//       'isAgent': isAgent,
//     };
//   }
// }

// class UGTabbarItem {
//   final String? path; // 界面
//   final String? icon; // 图标名字
//   final String? name; // 标题
//   final int? sort; // 排序
//   final int? status; // 1=显示建设中页面；0=正常显示
//   final String? isHot;
//   final String? iconLogo; // 图标url
//   final String? iconHot; // 热门图片路径
//   final String? roles; // 权限
//   final bool? isCustom; // 特规
//   final dynamic url;

//   UGTabbarItem({
//     this.path,
//     this.icon,
//     this.name,
//     this.sort,
//     this.status,
//     this.isHot,
//     this.iconLogo,
//     this.iconHot,
//     this.roles,
//     this.isCustom,
//     this.url,
//   });

//   factory UGTabbarItem.fromJson(Map<String, dynamic> json) {
//     return UGTabbarItem(
//       path: json['path'],
//       icon: json['icon'],
//       name: json['name'],
//       sort: ConvertToInt(json['sort']),
//       status: json['status'],
//       isHot: json['isHot'],
//       iconLogo: json['icon_logo'],
//       iconHot: json['icon_hot'],
//       roles: json['roles'],
//       isCustom: json['isCustom'],
//       url: json['url'],
//     );
//   }

//   Map<String, dynamic> toJson() {
//     return {
//       'path': path,
//       'icon': icon,
//       'name': name,
//       'sort': sort,
//       'status': status,
//       'isHot': isHot,
//       'icon_logo': iconLogo,
//       'icon_hot': iconHot,
//       'roles': roles,
//       'isCustom': isCustom,
//       'url': url,
//     };
//   }
// }

// class QdChannel {
//   // card, alipay, wechat, xnb
//   String? enName;
//   String? index;
//   String? name;

//   QdChannel({this.enName, this.index, this.name});

//   factory QdChannel.fromJson(json) {
//     return QdChannel(
//         enName: json['enName'], index: json['index'], name: json['name']);
//   }
// }

// class UserCenterCategory {
//   // card, alipay, wechat, xnb
//   int? id;
//   String? name;

//   UserCenterCategory({this.id, this.name});

//   factory UserCenterCategory.fromJson(json) {
//     return UserCenterCategory(id: json['id'], name: json['name']);
//   }
// }

// class LHPriceModel {
//   LHPriceModel();
//   factory LHPriceModel.fromJson(json) {
//     return LHPriceModel();
//   }
// }

// class UGSysConfModel {
//   // static var switchAutoTransfer;

//   // static List<UGUserCenterItem> getUserCenterItems(
//   //     Map<String, dynamic> sysConf, Map<String, dynamic> userInfo) {
//   //   final userCenter = sysConf['userCenter'] ?? [];
//   //   final hasActLottery = userInfo['hasActLottery'] ?? false;
//   //   final yuebaoSwitch = userInfo['yuebaoSwitch'] ?? false;

//   //   final temp = userCenter.map((uci) {
//   //     if (!hasActLottery && uci['code'] == UGUserCenterType['活动彩金']) {
//   //       return null;
//   //     }
//   //     if (!yuebaoSwitch && uci['code'] == UGUserCenterType['利息宝']) {
//   //       return null;
//   //     }
//   //     return UGUserCenterItem.fromJson(uci);
//   //   }).where((item) => item != null).toList();

//   //   return temp;
//   // }

//   // static const int CallingInterval = 10 * 60 * 1000; // 10 minutes
//   // static int systemConfigCalled = 0;

//   // static Future<void> updateFromNetwork(Function? completed) async {
//   //   final now = DateTime.now().millisecondsSinceEpoch;

//   //   // 只有超过10分钟才会更新
//   //   if (now - systemConfigCalled < CallingInterval) {
//   //     if (completed != null) {
//   //       completed();
//   //     }
//   //     return;
//   //   }
//   //   systemConfigCalled = now;

//   //   final response = await apiSystemConfig(); // Implement API call
//   //   final data = response['data'];

//   //   UGStore.dispatch({
//   //     'type': 'merge',
//   //     'sysConf': data,
//   //   });
//   //   if (completed != null) {
//   //     completed();
//   //   }
//   // }

//   String? loginNoticeText;
//   String? loginNoticeSwitch;
//   OAuth? oauth;
//   String? addEditBankSw;
//   String? addEditAlipaySw;
//   String? addEditWechatSw;
//   String? addEditVcoinSw;
//   String? timezone;
//   bool? balanceDecimal;
//   bool? betAmountIsDecimal;
//   String? currency;
//   String? zxkfUrl2;
//   String? zxkfUrl;
//   String? zalokfUrl;
//   String? telegramkfUrl;
//   String? minWithdrawMoney;
//   String? maxWithdrawMoney;
//   String? closeregreason;
//   String? missionName;
//   String? missionBili;
//   String? isIntToMoney;
//   String? missionSwitch;
//   String? myrecoImg;
//   String? checkinSwitch;
//   String? mkCheckinSwitch;
//   String? agentMApply;
//   String? mobileLogo;
//   String? agentRegbutton;
//   String? oftenLoginArea;
//   String? mobileTemplateBackground;
//   String? mobileTemplateCategory;
//   String? mobileTemplateLhcStyle;
//   String? mobileTemplateXbjStyle;
//   String? mobileTemplateVB68Style;
//   String? mobileTemplateStyle;
//   String? mobileGameHall;
//   String? picTypeshow;
//   String? webName;
//   String? serviceQQ1;
//   String? serviceQQ2;
//   String? appPopupWechatNum;
//   String? appPopupWechatImg;
//   String? appPopupQqNum;
//   String? appPopupQqImg;
//   int? domainBindAgentId;
//   String? switchDOYUSDTTrans;
//   String? homeTypeSelect;
//   String? chatRoomName;
//   String? chatMinFollowAmount;
//   String? chaseNumber;
//   String? selectNumber;
//   String? easyRememberDomain;
//   String? chatLink;
//   int? mBonsSwitch;
//   int? missionPopUpSwitch;
//   String? announceFirst;
//   String? smsFindPassword;
//   int? switchCoinPwdSms;
//   String? switchCoinPwd;
//   List<String>? coinPwdAuditOptionAry;
//   String? chatShareBetMinAmount;
//   InviteCodeConfigModel? inviteCode;
//   String? hideReco;
//   String? inviteCodeSwitch;
//   String? inviteWord;
//   String? regName;
//   String? regFundpwd;
//   String? regQq;
//   String? regWx;
//   String? regLine;
//   String? regBindCardSwitch;
//   String? regFb;
//   String? regPhone;
//   String? regEmail;
//   String? regVcode;
//   String? passLimit;
//   String? passLengthMin;
//   String? passLengthMax;
//   String? smsVerify;
//   int? guestSwitch;
//   String? userLengthLimitSwitch;
//   String? userLengthMin;
//   String? userLengthMax;
//   String? switchSports;
//   String? regZalo;
//   String? rankingListSwitch;
//   bool? googleVerifier;
//   bool? recharge;
//   String? allowreg;
//   bool? allowMemberCancelBet;
//   String? mPromotePos;
//   bool? yuebaoSwitch;
//   String? yuebaoName;
//   bool? chatFollowSwitch;
//   int? switchBindVerify;
//   bool? switchAgentRecharge;
//   int? nameAgentRecharge;
//   String? isAgentRechargeNeededPwd;
//   bool? lhcdocMiCard;
//   String? lhcdocLotteryStr;
//   List<LHPriceModel>? lhcPriceList;
//   String? lhcdocVipUserContRepCheck;
//   List<UGTabbarItem>? mobileMenu;
//   List<UGUserCenterItem>? userCenter;
//   bool? chatRoomSwitch;
//   String? withdrawChannelAd;
//   String? switchBalanceChannel;
//   String? balanceChannelStartTime;
//   String? balanceChannelEndTime;
//   String? balanceChannelPrompt;
//   String? switchYuebaoChannel;
//   String? yuebaoChannelStartTime;
//   String? yuebaoChannelEndTime;
//   String? yuebaoChannelPrompt;
//   int? regLoginBgSw;
//   String? regLoginBg;
//   bool? activeReturnCoinStatus;
//   double? activeReturnCoinRatio;
//   String? betMax;
//   String? customiseBetMax;
//   String? customise;
//   String? switchShowFriendReferral;
//   String? showNavigationBar;
//   String? popupType;
//   String? popupAnnounce;
//   String? switchToK;
//   bool? loginVCode;
//   String? loginVCodeType;
//   int? loginChatAppVcode;
//   int? loginChatAppVcodeType;
//   String? loginTo;
//   dynamic adSliderTimer;
//   String? appDownloadUrl;
//   List<UserCenterItem>? userCenterItems;
//   List<UserCenterCategory>? userCenterCategoryList;
//   String? frontendAgentAddMember;
//   String? allowMemberModifyLanguage;
//   String? appVersion;
//   bool? switchShowActivityCategory;
//   bool? lhcdocIsShowNav;
//   String? appSelectType;
//   String? mobileViewModel;
//   List<QdChannel>? qdSupChannels;
//   String? chatRoomServer;
//   String? rechargeImgSw;
//   String? znxNotify;
//   String? chatappStartLogo;
//   String? chatappLogo;
//   String? facekfUrl;
//   String? whatskfUrl;
//   String? merId;
//   String? missionLogo;
//   String? pageTitle;
//   bool? mobileHomeGameTypeSwitch;
//   List<String>? kjwLotteries;
//   String? regAndLogin;
//   bool? switchAutoTransfer;

//   UGSysConfModel({
//     this.loginNoticeText,
//     this.loginNoticeSwitch,
//     this.oauth,
//     this.addEditBankSw,
//     this.addEditAlipaySw,
//     this.addEditWechatSw,
//     this.addEditVcoinSw,
//     this.timezone,
//     this.balanceDecimal,
//     this.betAmountIsDecimal,
//     this.currency,
//     this.zxkfUrl2,
//     this.zxkfUrl,
//     this.zalokfUrl,
//     this.telegramkfUrl,
//     this.minWithdrawMoney,
//     this.maxWithdrawMoney,
//     this.closeregreason,
//     this.missionName,
//     this.missionBili,
//     this.isIntToMoney,
//     this.missionSwitch,
//     this.myrecoImg,
//     this.checkinSwitch,
//     this.mkCheckinSwitch,
//     this.agentMApply,
//     this.mobileLogo,
//     this.agentRegbutton,
//     this.oftenLoginArea,
//     this.mobileTemplateBackground,
//     this.mobileTemplateCategory,
//     this.mobileTemplateLhcStyle,
//     this.mobileTemplateXbjStyle,
//     this.mobileTemplateVB68Style,
//     this.mobileTemplateStyle,
//     this.mobileGameHall,
//     this.picTypeshow,
//     this.webName,
//     this.serviceQQ1,
//     this.serviceQQ2,
//     this.appPopupWechatNum,
//     this.appPopupWechatImg,
//     this.appPopupQqNum,
//     this.appPopupQqImg,
//     this.domainBindAgentId,
//     this.switchDOYUSDTTrans,
//     this.homeTypeSelect,
//     this.chatRoomName,
//     this.chatMinFollowAmount,
//     this.chaseNumber,
//     this.selectNumber,
//     this.easyRememberDomain,
//     this.chatLink,
//     this.mBonsSwitch,
//     this.missionPopUpSwitch,
//     this.announceFirst,
//     this.smsFindPassword,
//     this.switchCoinPwdSms,
//     this.switchCoinPwd,
//     this.coinPwdAuditOptionAry,
//     this.chatShareBetMinAmount,
//     this.inviteCode,
//     this.hideReco,
//     this.inviteCodeSwitch,
//     this.inviteWord,
//     this.regName,
//     this.regFundpwd,
//     this.regQq,
//     this.regWx,
//     this.regLine,
//     this.regBindCardSwitch,
//     this.regFb,
//     this.regPhone,
//     this.regEmail,
//     this.regVcode,
//     this.passLimit,
//     this.passLengthMin,
//     this.passLengthMax,
//     this.smsVerify,
//     this.guestSwitch,
//     this.userLengthLimitSwitch,
//     this.userLengthMin,
//     this.userLengthMax,
//     this.switchSports,
//     this.regZalo,
//     this.rankingListSwitch,
//     this.googleVerifier,
//     this.recharge,
//     this.allowreg,
//     this.allowMemberCancelBet,
//     this.mPromotePos,
//     this.yuebaoSwitch,
//     this.yuebaoName,
//     this.chatFollowSwitch,
//     this.switchBindVerify,
//     this.switchAgentRecharge,
//     this.nameAgentRecharge,
//     this.isAgentRechargeNeededPwd,
//     this.lhcdocMiCard,
//     this.lhcdocLotteryStr,
//     this.lhcPriceList,
//     this.lhcdocVipUserContRepCheck,
//     this.mobileMenu,
//     this.userCenter,
//     this.chatRoomSwitch,
//     this.withdrawChannelAd,
//     this.switchBalanceChannel,
//     this.balanceChannelStartTime,
//     this.balanceChannelEndTime,
//     this.balanceChannelPrompt,
//     this.switchYuebaoChannel,
//     this.yuebaoChannelStartTime,
//     this.yuebaoChannelEndTime,
//     this.yuebaoChannelPrompt,
//     this.regLoginBgSw,
//     this.regLoginBg,
//     this.activeReturnCoinStatus,
//     this.activeReturnCoinRatio,
//     this.betMax,
//     this.customiseBetMax,
//     this.customise,
//     this.switchShowFriendReferral,
//     this.showNavigationBar,
//     this.popupType,
//     this.popupAnnounce,
//     this.switchToK,
//     this.loginVCode,
//     this.loginVCodeType,
//     this.loginChatAppVcode,
//     this.loginChatAppVcodeType,
//     this.loginTo,
//     this.adSliderTimer,
//     this.appDownloadUrl,
//     this.userCenterItems,
//     this.userCenterCategoryList,
//     this.frontendAgentAddMember,
//     this.allowMemberModifyLanguage,
//     this.appVersion,
//     this.switchShowActivityCategory,
//     this.lhcdocIsShowNav,
//     this.appSelectType,
//     this.mobileViewModel,
//     this.qdSupChannels,
//     this.chatRoomServer,
//     this.rechargeImgSw,
//     this.znxNotify,
//     this.chatappStartLogo,
//     this.chatappLogo,
//     this.facekfUrl,
//     this.whatskfUrl,
//     this.merId,
//     this.missionLogo,
//     this.pageTitle,
//     this.mobileHomeGameTypeSwitch,
//     this.kjwLotteries,
//     this.regAndLogin,
//     this.switchAutoTransfer,
//   });

//   factory UGSysConfModel.fromJson(Map<String, dynamic> json) {
//     return UGSysConfModel(
//       loginNoticeText: json['loginNoticeText'],
//       loginNoticeSwitch: json['loginNoticeSwitch'],
//       oauth: json['oauth'] != null ? OAuth.fromJson(json['oauth']) : null,
//       addEditBankSw: json['add_edit_bank_sw'],
//       addEditAlipaySw: json['add_edit_alipay_sw'],
//       addEditWechatSw: json['add_edit_wechat_sw'],
//       addEditVcoinSw: json['add_edit_vcoin_sw'],
//       timezone: json['timezone'],
//       balanceDecimal: json['balanceDecimal'],
//       betAmountIsDecimal: json['betAmountIsDecimal'],
//       currency: json['currency'],
//       zxkfUrl2: json['zxkfUrl2'],
//       zxkfUrl: json['zxkfUrl'],
//       zalokfUrl: json['zalokfUrl'],
//       telegramkfUrl: json['telegramkfUrl'],
//       minWithdrawMoney: json['minWithdrawMoney'],
//       maxWithdrawMoney: json['maxWithdrawMoney'],
//       closeregreason: json['closeregreason'],
//       missionName: json['missionName'],
//       missionBili: json['missionBili'],
//       isIntToMoney: json['isIntToMoney'],
//       missionSwitch: json['missionSwitch'],
//       myrecoImg: json['myreco_img'],
//       checkinSwitch: json['checkinSwitch'],
//       mkCheckinSwitch: json['mkCheckinSwitch'],
//       agentMApply: json['agent_m_apply'],
//       mobileLogo: json['mobile_logo'],
//       agentRegbutton: json['agentRegbutton'],
//       oftenLoginArea: json['oftenLoginArea'],
//       mobileTemplateBackground: json['mobileTemplateBackground'],
//       mobileTemplateCategory: json['mobileTemplateCategory'],
//       mobileTemplateLhcStyle: json['mobileTemplateLhcStyle'],
//       mobileTemplateXbjStyle: json['mobileTemplateXbjStyle'],
//       mobileTemplateVB68Style: json['mobileTemplateVB68Style'],
//       mobileTemplateStyle: json['mobileTemplateStyle'],
//       mobileGameHall: json['mobileGameHall'],
//       picTypeshow: json['picTypeshow'],
//       webName: json['webName'],
//       serviceQQ1: json['serviceQQ1'],
//       serviceQQ2: json['serviceQQ2'],
//       appPopupWechatNum: json['appPopupWechatNum'],
//       appPopupWechatImg: json['appPopupWechatImg'],
//       appPopupQqNum: json['appPopupQqNum'],
//       appPopupQqImg: json['appPopupQqImg'],
//       domainBindAgentId: json['domainBindAgentId'],
//       switchDOYUSDTTrans: json['switchDOYUSDTTrans'],
//       homeTypeSelect: json['homeTypeSelect'],
//       chatRoomName: json['chatRoomName'],
//       chatMinFollowAmount: json['chatMinFollowAmount'],
//       chaseNumber: json['chaseNumber'],
//       selectNumber: json['selectNumber'],
//       easyRememberDomain: json['easyRememberDomain'],
//       chatLink: json['chatLink'],
//       mBonsSwitch: json['mBonsSwitch'],
//       missionPopUpSwitch: json['missionPopUpSwitch'],
//       announceFirst: json['announce_first'],
//       smsFindPassword: json['smsFindPassword'],
//       switchCoinPwdSms: json['switchCoinPwdSms'],
//       switchCoinPwd: json['switchCoinPwd'],
//       coinPwdAuditOptionAry: json['coinPwdAuditOptionAry'] != null
//           ? List<String>.from(json['coinPwdAuditOptionAry'])
//           : null,
//       chatShareBetMinAmount: json['chatShareBetMinAmount'],
//       inviteCode: json['inviteCode'] != null
//           ? InviteCodeConfigModel.fromJson(json['inviteCode'])
//           : null,
//       hideReco: json['hide_reco'],
//       inviteCodeSwitch: json['inviteCodeSwitch'],
//       inviteWord: json['inviteWord'],
//       regName: json['reg_name'],
//       regFundpwd: json['reg_fundpwd'],
//       regQq: json['reg_qq'],
//       regWx: json['reg_wx'],
//       regLine: json['reg_line'],
//       regBindCardSwitch: json['regBindCardSwitch'],
//       regFb: json['reg_fb'],
//       regPhone: json['reg_phone'],
//       regEmail: json['reg_email'],
//       regVcode: json['regVcode'],
//       passLimit: json['pass_limit'],
//       passLengthMin: json['pass_length_min'],
//       passLengthMax: json['pass_length_max'],
//       smsVerify: json['smsVerify'],
//       guestSwitch: json['guestSwitch'],
//       userLengthLimitSwitch: json['user_length_limit_switch'],
//       userLengthMin: json['user_length_min'],
//       userLengthMax: json['user_length_max'],
//       switchSports: json['switchSports'],
//       regZalo: json['reg_zalo'],
//       rankingListSwitch: json['rankingListSwitch'],
//       googleVerifier: json['googleVerifier'],
//       recharge: json['recharge'],
//       allowreg: json['allowreg'],
//       allowMemberCancelBet: json['allowMemberCancelBet'],
//       mPromotePos: json['m_promote_pos'],
//       yuebaoSwitch: json['yuebaoSwitch'],
//       yuebaoName: json['yuebaoName'],
//       chatFollowSwitch: json['chatFollowSwitch'],
//       switchBindVerify: ConvertToInt(json['switchBindVerify'].toString()),
//       switchAgentRecharge: json['switchAgentRecharge'],
//       nameAgentRecharge: ConvertToInt(json['nameAgentRecharge'].toString()),
//       isAgentRechargeNeededPwd: json['isAgentRechargeNeededPwd'],
//       lhcdocMiCard: json['lhcdocMiCard'],
//       lhcdocLotteryStr: json['lhcdocLotteryStr'],
//       lhcPriceList: json['lhcPriceList'] != null
//           ? List<LHPriceModel>.from(
//               json['lhcPriceList'].map((x) => LHPriceModel.fromJson(x)))
//           : null,
//       lhcdocVipUserContRepCheck: json['lhcdocVipUserContRepCheck'],
//       mobileMenu: json['mobileMenu'] != null
//           ? List<UGTabbarItem>.from(
//               json['mobileMenu'].map((x) => UGTabbarItem.fromJson(x)))
//           : null,
//       userCenter: json['userCenter'] != null
//           ? List<UGUserCenterItem>.from(
//               json['userCenter'].map((x) => UGUserCenterItem.fromJson(x)))
//           : null,
//       chatRoomSwitch: json['chatRoomSwitch'],
//       withdrawChannelAd: json['withdrawChannelAd'],
//       switchBalanceChannel: json['switchBalanceChannel'],
//       balanceChannelStartTime: json['balanceChannelStartTime'],
//       balanceChannelEndTime: json['balanceChannelEndTime'],
//       balanceChannelPrompt: json['balanceChannelPrompt'],
//       switchYuebaoChannel: json['switchYuebaoChannel'],
//       yuebaoChannelStartTime: json['yuebaoChannelStartTime'],
//       yuebaoChannelEndTime: json['yuebaoChannelEndTime'],
//       yuebaoChannelPrompt: json['yuebaoChannelPrompt'],
//       regLoginBgSw: ConvertToInt(json['reg_login_bg_sw']),
//       regLoginBg: json['reg_login_bg'],
//       activeReturnCoinStatus: json['activeReturnCoinStatus'],
//       activeReturnCoinRatio: convertToDouble(json['activeReturnCoinRatio']),
//       betMax: json['betMax'],
//       customiseBetMax: json['customiseBetMax'],
//       customise: json['customise'],
//       switchShowFriendReferral: json['switchShowFriendReferral'],
//       showNavigationBar: json['showNavigationBar'],
//       popupType: json['popup_type'],
//       popupAnnounce: json['popup_announce'],
//       switchToK: json['switchToK'],
//       loginVCode: json['loginVCode'],
//       loginVCodeType: json['loginVCodeType'],
//       loginChatAppVcode: json['loginChatAppVcode'],
//       loginChatAppVcodeType: json['loginChatAppVcodeType'],
//       loginTo: json['login_to'],
//       adSliderTimer: json['adSliderTimer'],
//       appDownloadUrl: json['appDownloadUrl'],
//       userCenterItems: json['userCenterItems'] != null
//           ? List<UserCenterItem>.from(
//               json['userCenterItems']?.map((x) => UserCenterItem.fromJson(x)))
//           : null,
//       userCenterCategoryList: json['userCenterCategoryList'] != null
//           ? List<UserCenterCategory>.from(json['userCenterCategoryList']
//               ?.map((x) => UserCenterCategory.fromJson(x)))
//           : null,
//       frontendAgentAddMember: json['frontend_agent_add_member'],
//       allowMemberModifyLanguage: json['allowMemberModifyLanguage'],
//       appVersion: json['appVersion'],
//       switchShowActivityCategory: json['switchShowActivityCategory'],
//       lhcdocIsShowNav: json['lhcdocIsShowNav'],
//       appSelectType: json['appSelectType'],
//       mobileViewModel: json['mobileViewModel'],
//       qdSupChannels: json['qdSupChannels'] != null
//           ? List<QdChannel>.from(
//               json['qdSupChannels'].map((x) => QdChannel.fromJson(x)))
//           : null,
//       chatRoomServer: json['chatRoomServer'],
//       rechargeImgSw: json['recharge_img_sw'],
//       znxNotify: json['znxNotify'],
//       chatappStartLogo: json['chatapp_start_logo'],
//       chatappLogo: json['chatapp_logo'],
//       facekfUrl: json['facekfUrl'],
//       whatskfUrl: json['whatskfUrl'],
//       merId: json['merId'],
//       missionLogo: json['mission_logo'],
//       pageTitle: json['page_title'],
//       mobileHomeGameTypeSwitch: json['mobileHomeGameTypeSwitch'],
//       kjwLotteries: json['kjwLotteries'] != null
//           ? List<String>.from(json['kjwLotteries'])
//           : null,
//       regAndLogin: json['regAndLogin'],
//       switchAutoTransfer: json['switchAutoTransfer'],
//     );
//   }

//   Map<String, dynamic> toJson() => {
//         'loginNoticeText': loginNoticeText,
//         'loginNoticeSwitch': loginNoticeSwitch,
//         'oauth': oauth?.toJson(),
//         'addEditBankSw': addEditBankSw,
//         'addEditAlipaySw': addEditAlipaySw,
//         'addEditWechatSw': addEditWechatSw,
//         'addEditVcoinSw': addEditVcoinSw,
//         'timezone': timezone,
//         'balanceDecimal': balanceDecimal,
//         'betAmountIsDecimal': betAmountIsDecimal,
//         'currency': currency,
//         'zxkfUrl2': zxkfUrl2,
//         'zxkfUrl': zxkfUrl,
//         'zalokfUrl': zalokfUrl,
//         'telegramkfUrl': telegramkfUrl,
//         'minWithdrawMoney': minWithdrawMoney,
//         'maxWithdrawMoney': maxWithdrawMoney,
//         'closeregreason': closeregreason,
//         'missionName': missionName,
//         'missionBili': missionBili,
//         'isIntToMoney': isIntToMoney,
//         'missionSwitch': missionSwitch,
//         'myrecoImg': myrecoImg,
//         'checkinSwitch': checkinSwitch,
//         'mkCheckinSwitch': mkCheckinSwitch,
//         'agentMApply': agentMApply,
//         'mobileLogo': mobileLogo,
//         'agentRegbutton': agentRegbutton,
//         'oftenLoginArea': oftenLoginArea,
//         'mobileTemplateBackground': mobileTemplateBackground,
//         'mobileTemplateCategory': mobileTemplateCategory,
//         'mobileTemplateLhcStyle': mobileTemplateLhcStyle,
//         'mobileTemplateXbjStyle': mobileTemplateXbjStyle,
//         'mobileTemplateVB68Style': mobileTemplateVB68Style,
//         'mobileTemplateStyle': mobileTemplateStyle,
//         'mobileGameHall': mobileGameHall,
//         'picTypeshow': picTypeshow,
//         'webName': webName,
//         'serviceQQ1': serviceQQ1,
//         'serviceQQ2': serviceQQ2,
//         'appPopupWechatNum': appPopupWechatNum,
//         'appPopupWechatImg': appPopupWechatImg,
//         'appPopupQqNum': appPopupQqNum,
//         'appPopupQqImg': appPopupQqImg,
//         'domainBindAgentId': domainBindAgentId,
//         'switchDOYUSDTTrans': switchDOYUSDTTrans,
//         'homeTypeSelect': homeTypeSelect,
//         'chatRoomName': chatRoomName,
//         'chatMinFollowAmount': chatMinFollowAmount,
//         'chaseNumber': chaseNumber,
//         'selectNumber': selectNumber,
//         'easyRememberDomain': easyRememberDomain,
//         'chatLink': chatLink,
//         'mBonsSwitch': mBonsSwitch,
//         'missionPopUpSwitch': missionPopUpSwitch,
//         'announceFirst': announceFirst,
//         'smsFindPassword': smsFindPassword,
//         'switchCoinPwdSms': switchCoinPwdSms,
//         'switchCoinPwd': switchCoinPwd,
//         'coinPwdAuditOptionAry': coinPwdAuditOptionAry,
//         'chatShareBetMinAmount': chatShareBetMinAmount,
//         'inviteCode': inviteCode?.toJson(),
//         'hideReco': hideReco,
//         'inviteCodeSwitch': inviteCodeSwitch,
//         'inviteWord': inviteWord,
//         'regName': regName,
//         'regFundpwd': regFundpwd,
//         'regQq': regQq,
//         'regWx': regWx,
//         'regLine': regLine,
//         'regBindCardSwitch': regBindCardSwitch,
//         'regFb': regFb,
//         'regPhone': regPhone,
//         'regEmail': regEmail,
//         'regVcode': regVcode,
//         'passLimit': passLimit,
//         'passLengthMin': passLengthMin,
//         'passLengthMax': passLengthMax,
//         'smsVerify': smsVerify,
//         'guestSwitch': guestSwitch,
//         'userLengthLimitSwitch': userLengthLimitSwitch,
//         'userLengthMin': userLengthMin,
//         'userLengthMax': userLengthMax,
//         'switchSports': switchSports,
//         'regZalo': regZalo,
//         'rankingListSwitch': rankingListSwitch,
//         'googleVerifier': googleVerifier,
//         'recharge': recharge,
//         'allowreg': allowreg,
//         'allowMemberCancelBet': allowMemberCancelBet,
//         'mPromotePos': mPromotePos,
//         'yuebaoSwitch': yuebaoSwitch,
//         'yuebaoName': yuebaoName,
//         'chatFollowSwitch': chatFollowSwitch,
//         'switchBindVerify': switchBindVerify,
//         'switchAgentRecharge': switchAgentRecharge,
//         'nameAgentRecharge': nameAgentRecharge,
//         'isAgentRechargeNeededPwd': isAgentRechargeNeededPwd,
//         'lhcdocMiCard': lhcdocMiCard,
//         'lhcdocLotteryStr': lhcdocLotteryStr,
//         // 'lhcPriceList': lhcPriceList != null
//         //     ? List<dynamic>.from(lhcPriceList!.map((x) => x.toJson()))
//         //     : null,
//         'lhcdocVipUserContRepCheck': lhcdocVipUserContRepCheck,
//         'mobileMenu': mobileMenu != null
//             ? List<dynamic>.from(mobileMenu!.map((x) => x.toJson()))
//             : null,
//         'userCenter': userCenter != null
//             ? List<dynamic>.from(userCenter!.map((x) => x.toJson()))
//             : null,
//         'chatRoomSwitch': chatRoomSwitch,
//         'withdrawChannelAd': withdrawChannelAd,
//         'switchBalanceChannel': switchBalanceChannel,
//         'balanceChannelStartTime': balanceChannelStartTime,
//         'balanceChannelEndTime': balanceChannelEndTime,
//         'balanceChannelPrompt': balanceChannelPrompt,
//         'switchYuebaoChannel': switchYuebaoChannel,
//         'yuebaoChannelStartTime': yuebaoChannelStartTime,
//         'yuebaoChannelEndTime': yuebaoChannelEndTime,
//         'yuebaoChannelPrompt': yuebaoChannelPrompt,
//         'regLoginBgSw': regLoginBgSw,
//         'regLoginBg': regLoginBg,
//         'activeReturnCoinStatus': activeReturnCoinStatus,
//         'activeReturnCoinRatio': activeReturnCoinRatio,
//         'betMax': betMax,
//         'customiseBetMax': customiseBetMax,
//         'customise': customise,
//         'switchShowFriendReferral': switchShowFriendReferral,
//         'showNavigationBar': showNavigationBar,
//         'popupType': popupType,
//         'popupAnnounce': popupAnnounce,
//         'switchToK': switchToK,
//         'loginVCode': loginVCode,
//         'loginVCodeType': loginVCodeType,
//         'loginChatAppVcode': loginChatAppVcode,
//         'loginChatAppVcodeType': loginChatAppVcodeType,
//         'loginTo': loginTo,
//         'adSliderTimer': adSliderTimer,
//         'appDownloadUrl': appDownloadUrl,
//         'userCenterItems': userCenterItems != null
//             ? List<dynamic>.from(userCenterItems!.map((x) => x.toJson()))
//             : null,
//         // 'userCenterCategoryList': userCenterCategoryList != null
//         //     ? List<dynamic>.from(userCenterCategoryList!)
//         //     : null,
//         'frontendAgentAddMember': frontendAgentAddMember,
//         'allowMemberModifyLanguage': allowMemberModifyLanguage,
//         'appVersion': appVersion,
//         'switchShowActivityCategory': switchShowActivityCategory,
//         'lhcdocIsShowNav': lhcdocIsShowNav,
//         'appSelectType': appSelectType,
//         'mobileViewModel': mobileViewModel,
//         // 'qdSupChannels': qdSupChannels != null
//         //     ? List<dynamic>.from(qdSupChannels!.map((x) => x.toJson()))
//         //     : null,
//         'chatRoomServer': chatRoomServer,
//         'rechargeImgSw': rechargeImgSw,
//         'znxNotify': znxNotify,
//         'chatappStartLogo': chatappStartLogo,
//         'chatappLogo': chatappLogo,
//         'facekfUrl': facekfUrl,
//         'whatskfUrl': whatskfUrl,
//         'merId': merId,
//         'missionLogo': missionLogo,
//         'pageTitle': pageTitle,
//         'mobileHomeGameTypeSwitch': mobileHomeGameTypeSwitch,
//         'kjwLotteries':
//             kjwLotteries != null ? List<dynamic>.from(kjwLotteries!) : null,
//         'regAndLogin': regAndLogin,
//         'switchAutoTransfer': switchAutoTransfer,
//       };
// }
