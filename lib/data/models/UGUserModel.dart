import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:fpg_flutter/data/models/interfaceModel.dart';
import 'package:fpg_flutter/utils/helper.dart';

class RememberPassModel {
  String? username;
  String? password;
  bool? remember;

  RememberPassModel({
    this.username,
    this.password,
    this.remember,
  });

  factory RememberPassModel.fromJson(Map<String, dynamic> json) {
    return RememberPassModel(
      username: json['username'],
      password: json['password'],
      remember: json['remember'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'password': password,
      'remember': remember,
    };
  }
}

class UGUserModel extends UGLoginModel {
  // 抢单开关开关， 1=开,0=关
  int? qdSwitch;
  // 抢单存开关，1=开,0=关
  int? qdSwitchDe;
  // 抢单取开关，1=开,0=关
  int? qdSwitchWi;
  // 用户名
  String? username;
  // 用户ID
  String? uid;
  // 头像
  String? avatar;
  // 头像
  String? blockCoin;
  // 余额
  String? balance;
  // 余额
  String? doyBalance;
  // 余额
  String? usdtBalance;
  // 余额
  String? trxBalance;
  // 昵称
  String? usr;
  // 全名
  String? fullName;
  // 聊天室昵称
  String? chatRoomNickname;
  // email
  String? email;
  // 手机号
  String? phone;
  String? telegram;
  // QQ号
  String? qq;
  // 第三方facebook
  dynamic oauth;
  // 微信号
  String? wx;
  // 用户当前的ip地址
  String? clientIp;
  // 是否是六合文档的VIP "1"
  dynamic isLhcdocVip;
  // 当级经验值 "100000"
  dynamic curLevelInt;
  // 等级 "VIP4"
  String? curLevelGrade;
  // 头衔 "黄金"
  String? curLevelTitle;
  // 下级经验值 "100000"
  dynamic nextLevelInt;
  // 等级 "VIP5"
  String? nextLevelGrade;
  // 头衔 "铂金"
  String? nextLevelTitle;
  // 成长值 "19073.0000"
  dynamic taskReward;
  // 总成长值 "20498.5000"
  dynamic taskRewardTotal;
  // 成长标题 "阿西"
  String? taskRewardTitle;
  // 今日输赢金额 "0.00"
  dynamic todayWinAmount;
  // 未结金额 "10.00"
  dynamic unsettleAmount;
  // 玩过的真人游戏
  List<String>? playedRealGames;
  // 是否允许会员撤单
  bool? allowMemberCancelBet;
  // 是否是开启聊天室
  bool? chatRoomSwitch;
  // 是否显示活动彩金
  bool? hasActLottery;
  // 是否已绑定银行卡
  bool? hasBankCard;
  // 是否已设置取款密码
  bool? hasFundPwd;
  // 是否是代理
  bool? isAgent;
  // 是否开启google验证
  bool? googleVerifier;
  // 判断会员是否绑定谷歌验证码
  bool? isBindGoogleVerifier;
  // 是否试玩账号
  bool? isTest;
  // 是否是开启利息宝
  bool? yuebaoSwitch;
  int? unreadFaq;
  // 站内信未读消息数量
  int? unreadMsg;
  // 站内信未读消息列表
  List<PopupMessage>? unreadMsgList;
  // 是否允许注单分享
  int? chatShareBet;
  // 我的动态未读数
  int? unReadReply;
  // 加入第_天
  int? regUntilNow;
  // 是否带玩
  String? isTrial;
  dynamic dmlNeeded;
  dynamic dmlReal;
  // 这里是意见反馈那里是否被拉黑
  bool? hideSuggest;
  // 累计彩金
  dynamic amountSum;
  // 挂单提款显示判断
  bool? hasWithdrawEnableC2c;
  // 挂单提款url
  String? c2cUrl;
  String? type;

  UGUserModel({
    super.apiSid,
    super.apiToken,
    super.needFullName,
    super.needVcode,
    super.ggCheck,
    super.sessid,
    super.token,
    super.pwd,
    super.clsName,
    this.qdSwitch,
    this.qdSwitchDe,
    this.qdSwitchWi,
    this.username,
    this.uid,
    this.avatar,
    this.blockCoin,
    this.balance,
    this.doyBalance,
    this.usdtBalance,
    this.trxBalance,
    this.usr,
    this.fullName,
    this.chatRoomNickname,
    this.email,
    this.phone,
    this.telegram,
    this.qq,
    this.oauth,
    this.wx,
    this.clientIp,
    this.isLhcdocVip,
    this.curLevelInt,
    this.curLevelGrade,
    this.curLevelTitle,
    this.nextLevelInt,
    this.nextLevelGrade,
    this.nextLevelTitle,
    this.taskReward,
    this.taskRewardTotal,
    this.taskRewardTitle,
    this.todayWinAmount,
    this.unsettleAmount,
    this.playedRealGames,
    this.allowMemberCancelBet,
    this.chatRoomSwitch,
    this.hasActLottery,
    this.hasBankCard,
    this.hasFundPwd,
    this.isAgent,
    this.googleVerifier,
    this.isBindGoogleVerifier,
    this.isTest,
    this.yuebaoSwitch,
    this.unreadFaq,
    this.unreadMsg,
    this.unreadMsgList,
    this.chatShareBet,
    this.unReadReply,
    this.regUntilNow,
    this.isTrial,
    this.dmlNeeded,
    this.dmlReal,
    this.hideSuggest,
    this.amountSum,
    this.hasWithdrawEnableC2c,
    this.c2cUrl,
    this.type,
  });

  factory UGUserModel.fromJson(Map<String, dynamic> json) {
    return UGUserModel(
      apiSid: json['API-SID'],
      apiToken: json['API-TOKEN'],
      needFullName: json['needFullName'],
      needVcode: json['needVcode'],
      ggCheck: json['ggCheck'],
      sessid: json['sessid'],
      token: json['token'],
      pwd: json['pwd'],
      clsName: json['clsName'],
      qdSwitch: json['qdSwitch'],
      qdSwitchDe: json['qdSwitchDe'],
      qdSwitchWi: json['qdSwitchWi'],
      username: json['username'],
      uid: json['uid'],
      avatar: json['avatar'],
      blockCoin: json['blockCoin'],
      balance: json['balance'],
      doyBalance: json['doy_balance'],
      usdtBalance: json['usdt_balance'],
      trxBalance: json['trx_balance'],
      usr: json['usr'],
      fullName: json['fullName'],
      chatRoomNickname: json['chatRoomNickname'],
      email: json['email'],
      phone: json['phone'],
      telegram: json['telegram'],
      qq: json['qq'],
      oauth: json['oauth'],
      wx: json['wx'],
      clientIp: json['clientIp'],
      isLhcdocVip: json['isLhcdocVip'],
      curLevelInt: convertToInt(json['curLevelInt']),
      curLevelGrade: json['curLevelGrade'],
      curLevelTitle: json['curLevelTitle'],
      nextLevelInt: convertToInt(json['nextLevelInt']),
      nextLevelGrade: json['nextLevelGrade'],
      nextLevelTitle: json['nextLevelTitle'],
      taskReward: json['taskReward']?.toString(),
      taskRewardTotal: json['taskRewardTotal']?.toString(),
      taskRewardTitle: json['taskRewardTitle'],
      todayWinAmount: json['todayWinAmount']?.toString(),
      unsettleAmount: json['unsettleAmount']?.toString(),
      playedRealGames: List<String>.from(json['playedRealGames']),
      allowMemberCancelBet: json['allowMemberCancelBet'],
      chatRoomSwitch: json['chatRoomSwitch'],
      hasActLottery: json['hasActLottery'],
      hasBankCard: json['hasBankCard'],
      hasFundPwd: json['hasFundPwd'],
      isAgent: json['isAgent'],
      googleVerifier: json['googleVerifier'],
      isBindGoogleVerifier: json['isBindGoogleVerifier'],
      isTest: json['isTest'],
      yuebaoSwitch: json['yuebaoSwitch'],
      unreadFaq: json['unreadFaq'],
      unreadMsg: json['unreadMsg'],
      unreadMsgList: (json['unreadMsgList'] as List)
          .map((i) => PopupMessage.fromJson(i))
          .toList(),
      chatShareBet: json['chatShareBet'],
      unReadReply: json['unReadReply'],
      regUntilNow: json['regUntilNow'],
      isTrial: json['is_trial']?.toString(),
      dmlNeeded: json['dml_needed']?.toString(),
      dmlReal: json['dml_real']?.toString(),
      hideSuggest: json['hideSuggest'],
      amountSum: json['amountSum']?.toString(),
      hasWithdrawEnableC2c: json['hasWithdrawEnableC2c'],
      c2cUrl: json['c2cUrl'],
      type: json['type'].toString(),
    );
  }

  @override
  Map<String, dynamic> toJson() {
    final json = super.toJson();
    json.addAll({
      'qdSwitch': qdSwitch,
      'qdSwitchDe': qdSwitchDe,
      'qdSwitchWi': qdSwitchWi,
      'username': username,
      'uid': uid,
      'avatar': avatar,
      'blockCoin': blockCoin,
      'balance': balance,
      'doy_balance': doyBalance,
      'usdt_balance': usdtBalance,
      'trx_balance': trxBalance,
      'usr': usr,
      'fullName': fullName,
      'chatRoomNickname': chatRoomNickname,
      'email': email,
      'phone': phone,
      'telegram': telegram,
      'qq': qq,
      'oauth': oauth,
      'wx': wx,
      'clientIp': clientIp,
      'isLhcdocVip': isLhcdocVip,
      'curLevelInt': curLevelInt,
      'curLevelGrade': curLevelGrade,
      'curLevelTitle': curLevelTitle,
      'nextLevelInt': nextLevelInt,
      'nextLevelGrade': nextLevelGrade,
      'nextLevelTitle': nextLevelTitle,
      'taskReward': taskReward,
      'taskRewardTotal': taskRewardTotal,
      'taskRewardTitle': taskRewardTitle,
      'todayWinAmount': todayWinAmount,
      'unsettleAmount': unsettleAmount,
      'playedRealGames': playedRealGames,
      'allowMemberCancelBet': allowMemberCancelBet,
      'chatRoomSwitch': chatRoomSwitch,
      'hasActLottery': hasActLottery,
      'hasBankCard': hasBankCard,
      'hasFundPwd': hasFundPwd,
      'isAgent': isAgent,
      'googleVerifier': googleVerifier,
      'isBindGoogleVerifier': isBindGoogleVerifier,
      'isTest': isTest,
      'yuebaoSwitch': yuebaoSwitch,
      'unreadFaq': unreadFaq,
      'unreadMsg': unreadMsg,
      'unreadMsgList': unreadMsgList?.map((i) => i.toJson()).toList(),
      'chatShareBet': chatShareBet,
      'unReadReply': unReadReply,
      'regUntilNow': regUntilNow,
      'is_trial': isTrial,
      'dml_needed': dmlNeeded,
      'dml_real': dmlReal,
      'hideSuggest': hideSuggest,
      'amountSum': amountSum,
      'hasWithdrawEnableC2c': hasWithdrawEnableC2c,
      'c2cUrl': c2cUrl,
      'type': type,
    });
    return json;
  }
}
