class UGBetsRecordModel {
  String? date;
  String? dayOfWeek;
  String? betCount;
  String? betAmount;
  String? winCount;
  String? winAmount;
  String? winLoseAmount;
  UGBetsRecordModel({
    this.date,
    this.dayOfWeek,
    this.betCount,
    this.betAmount,
    this.winCount,
    this.winAmount,
    this.winLoseAmount,
  });
  factory UGBetsRecordModel.fromJson(Map<String, dynamic> json) {
    return UGBetsRecordModel(
      date: json['date'].toString(),
      dayOfWeek: json['dayOfWeek'].toString(),
      betCount: json['betCount'].toString(),
      betAmount: json['betAmount'].toString(),
      winCount: json['winCount'].toString(),
      winAmount: json['winAmount'].toString(),
      winLoseAmount: json['winLoseAmount'].toString(),
    );
  }
}

class UGBetsRecordListModel {
  String? code;
  String? msg;
  String? totalBetCount;
  String? totalBetAmount;
  String? totalWinAmount;
  List<UGBetsRecordModel>? tickets;

  UGBetsRecordListModel({
    this.code,
    this.msg,
    this.totalBetCount,
    this.totalBetAmount,
    this.totalWinAmount,
    this.tickets,
  });
  factory UGBetsRecordListModel.fromJson(Map<String, dynamic> json) {
    List<dynamic>? listData = json['tickets'];
    List<UGBetsRecordModel>? list;
    if (listData != null) {
      list = listData.map((item) => UGBetsRecordModel.fromJson(item)).toList();
    }
    return UGBetsRecordListModel(
      code: json['code'],
      msg: json['msg'],
      totalBetAmount: json['totalBetAmount'],
      totalBetCount: json['totalBetCount'],
      totalWinAmount: json['totalWinAmount'],
      tickets: list,
    );
    // tickets=list;
  }
}

class UGBetsRecordDataLongModel {
  String? code;
  String? msg;
  UGBetsRecordListLongModel? data;
  UGBetsRecordDataLongModel({
    this.code,
    this.msg,
    this.data,
  });
  factory UGBetsRecordDataLongModel.fromJson(Map<String, dynamic> json) {
    return UGBetsRecordDataLongModel(
      code: json['code'],
      msg: json['msg'],
      data: UGBetsRecordListLongModel.fromJson(json),
    );
  }
}

class UGBetsRecordListLongModel {
  int? total; // 数据总数
  String? totalBetAmount; // 总下注金额
  String? totalWinAmount; // 输赢总金额
  String? token;
  List<UGBetsRecordLongModel>? list;

  UGBetsRecordListLongModel({
    this.total,
    this.totalBetAmount,
    this.totalWinAmount,
    this.list,
    this.token,
  });

  factory UGBetsRecordListLongModel.fromJson(Map<String, dynamic> json) {
    return UGBetsRecordListLongModel(
      total: json['total'],
      totalBetAmount: json['totalBetAmount'],
      totalWinAmount: json['totalWinAmount'],
      token: json['token'],
      list: json['list'] != null
          ? (json['list'] as List)
              .map((e) => UGBetsRecordLongModel.fromJson(e))
              .toList()
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['totalBetAmount'] = this.totalBetAmount;
    data['totalWinAmount'] = this.totalWinAmount;
    data['token'] = this.token;
    if (this.list != null) {
      data['list'] = this.list!.map((e) => e.toJson()).toList();
    }
    return data;
  }
}

class UGBetsRecordLongModel {
  String? id; // 注单ID/
  String? gameName; // 游戏名称（彩种）/
  String? issue; // 彩票期号（彩票）/
  String? displayNumber; // 开奖期数  自营优先使用/
  String? playGroupName; // 游戏分类名称（彩票）/
  String? playName; // 游戏玩法（彩票）/
  String? lotteryNo; // 开奖号（彩票）/
  String? betAmount; // 下注金额/
  String? betCount; // 笔数/
  String? rebateRate; //
  String? rebateAmount; // 退水金额//
  String? expectAmount; // 可赢金额//

  String? winCount; // 中奖笔数
  String? winAmount; // 输赢金额//
  String? settleAmount; // 结算金额 （彩票）//
  String? odds; // 赔率（彩票）//

  String? betInfo; // 下注号码//
  String? status; // 注单状态 1=待开奖，2=已中奖，3=未中奖，4=已撤单//
  String? betTime; //
  bool? isAllowCancel; // 是否允许撤单//
  String? gameType;
  String? gameId; // 笔数

  UGBetsRecordLongModel({
    this.id,
    this.gameType,
    this.gameName,
    this.playGroupName,
    this.playName,
    this.betAmount,
    this.rebateAmount,
    this.betTime,
    this.winAmount,
    this.settleAmount,
    this.lotteryNo,
    this.status,
    this.odds,
    this.betInfo,
    this.expectAmount,
    this.isAllowCancel,
    this.issue,
    this.displayNumber,
    this.winCount,
    this.betCount,
    this.gameId,
    this.rebateRate,
  });

  factory UGBetsRecordLongModel.fromJson(Map<String, dynamic> json) {
    return UGBetsRecordLongModel(
      id: json['id'].toString(),
      gameType: json['gameType'].toString(),
      gameName: json['gameName'].toString(),
      playGroupName: json['playGroupName'].toString(),
      playName: json['playName'].toString(),
      betAmount: json['betAmount'].toString(),
      rebateAmount: json['rebateAmount'].toString(),
      betTime: json['betTime'].toString(),
      winAmount: json['winAmount'].toString(),
      settleAmount: json['settleAmount'].toString(),
      lotteryNo: json['lotteryNo'].toString(),
      status: json['status'].toString(),
      odds: json['odds'].toString(),
      betInfo: json['betInfo'].toString(),
      expectAmount: json['expectAmount'].toString(),
      isAllowCancel: json['isAllowCancel'],
      issue: json['issue'].toString(),
      displayNumber: json['displayNumber'].toString(),
      winCount: json['winCount'].toString(),
      betCount: json['betCount'].toString(),
      gameId: json['gameId'].toString(),
      rebateRate: json['rebateRate'].toString(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['gameType'] = this.gameType;
    data['gameName'] = this.gameName;
    data['playGroupName'] = this.playGroupName;
    data['playName'] = this.playName;
    data['betAmount'] = this.betAmount;
    data['rebateAmount'] = this.rebateAmount;
    data['betTime'] = this.betTime;
    data['winAmount'] = this.winAmount;
    data['settleAmount'] = this.settleAmount;
    data['lotteryNo'] = this.lotteryNo;
    data['status'] = this.status;
    data['odds'] = this.odds;
    data['betInfo'] = this.betInfo;
    data['expectAmount'] = this.expectAmount;
    data['isAllowCancel'] = this.isAllowCancel;
    data['issue'] = this.issue;
    data['displayNumber'] = this.displayNumber;
    data['winCount'] = this.winCount;
    data['betCount'] = this.betCount;
    data['gameId'] = this.gameId;
    data['rebateRate'] = this.rebateRate;
    return data;
  }
}
