class RealGameModel {
  int? code;
  String? msg;
  List<RealGameData>? data;

  RealGameModel({
    this.code,
    this.msg,
    this.data,
  });

  factory RealGameModel.fromJson(Map<String, dynamic> json) {
    List<dynamic>? dataList = json['data'];
    List<RealGameData>? data;
    if (dataList != null) {
      data = dataList.map((item) => RealGameData.fromJson(item)).toList();
    }
    return RealGameModel(
      code: json['code'] as int?,
      msg: json['msg'],
      data: data,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'msg': msg,
      'data': data?.map((item) => item.toJson()).toList(),
    };
  }
}

class RealGameData {
  String? id;
  String? category;
  String? title;
  String? gameType;
  String? gameCat;
  dynamic isPopup;
  dynamic isHot;
  String? gameSymbol;
  String? currency;
  dynamic supportTrial;
  String? pic;

  RealGameData({
    this.id,
    this.category,
    this.title,
    this.gameType,
    this.gameCat,
    this.isPopup,
    this.isHot,
    this.gameSymbol,
    this.currency,
    this.supportTrial,
    this.pic,
  });

  factory RealGameData.fromJson(Map<String, dynamic> json) {
    // List<dynamic>? listData = json['list'];
    // List<RealGameModel1>? list;
    // if (listData != null) {
    //   list = listData.map((item) => RealGameModel1.fromJson(item)).toList();
    // }
    return RealGameData(
      id: json['id'],
      category: json['category'],
      title: json['title'],
      gameType: json['gameType'],
      gameCat: json['gameCat'],
      isPopup: json['isPopup'],
      isHot: json['isHot'],
      gameSymbol: json['gameSymbol'],
      currency: json['currency'],
      supportTrial: json['supportTrial'],
      pic: json['pic'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'category': category,
      'title': title,
      'gameType': gameType,
      'gameCat': gameCat,
      'isPopup': isPopup,
      'isHot': isHot,
      'gameSymbol': gameSymbol,
      'currency': currency,
      'supportTrial': supportTrial,
      'pic': pic,
    };
  }
}

class WalletData {
  final String title;
  final String id;
  final String pic;
  final String balance;

  WalletData({
    required this.title,
    required this.id,
    required this.pic,
    required this.balance,
  });
}

// 返回结果类型
class TransferStatusData {
  final String cookieStr;
  final String message;
  final String code;
  final String msg;
  final String data;
  final String info;
  final ResponseExtra extra;

  TransferStatusData({
    required this.cookieStr,
    required this.message,
    required this.code,
    required this.msg,
    required this.data,
    required this.info,
    // response 额外信息
    required this.extra,
  });
}

// response 额外信息
class ResponseExtra {
  // 0 表示重复登录
  final int status;

  ResponseExtra({
    required this.status,
  });
}

// response Balance
class ResponseBalance {
  final String balance;

  ResponseBalance({
    required this.balance,
  });
  factory ResponseBalance.fromJson(Map<String, dynamic> json) {
    // List<dynamic>? listData = json['list'];
    // List<RealGameModel1>? list;
    // if (listData != null) {
    //   list = listData.map((item) => RealGameModel1.fromJson(item)).toList();
    // }
    return ResponseBalance(
      balance: json['balance'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'balance': balance,
    };
  }
}

class ResponseGameData {
  int? id;
  String? name;

  ResponseGameData({this.id, this.name});

  // Factory method to create an instance from a JSON map
  factory ResponseGameData.fromJson(Map<String, dynamic> json) {
    return ResponseGameData(
      id: json['id'],
      name: json['name'],
    );
  }

  // Method to convert an instance to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
    };
  }
}

class ResponseOneKeyTransferOutData {
  List<ResponseGameData>? games;
  ResponseOneKeyTransferOutData({this.games});

  // Factory method to create an instance from a JSON map
  factory ResponseOneKeyTransferOutData.fromJson(Map<String, dynamic> json) {
    List<dynamic>? gameList = json['games'];
    List<ResponseGameData>? data;
    if (gameList != null) {
      data = gameList.map((item) => ResponseGameData.fromJson(item)).toList();
    }

    return ResponseOneKeyTransferOutData(games: data);
  }

  // Method to convert an instance to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'games': games?.map((item) => item.toJson()).toList(),
    };
  }
}

class TransferLogs {
  String? id;
  String? username;
  String? gameName;
  String? amount;
  String? actionTime;
  String? status;
  String? isAuto;
  String? billno;
  String? gameCat;
  String? gameUsername;

  TransferLogs({
    this.id,
    this.username,
    this.gameName,
    this.amount,
    this.actionTime,
    this.status,
    this.isAuto,
    this.billno,
    this.gameCat,
    this.gameUsername,
  });

  // Method to convert an instance of TransferLogs to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'username': username,
      'gameName': gameName,
      'amount': amount,
      'actionTime': actionTime,
      'status': status,
      'isAuto': isAuto,
      'billno': billno,
      'game_cat': gameCat,
      'game_username': gameUsername,
    };
  }

  // Factory constructor to create an instance of TransferLogs from a JSON map
  factory TransferLogs.fromJson(Map<String, dynamic> json) {
    return TransferLogs(
      id: json['id'],
      username: json['username'],
      gameName: json['gameName'],
      amount: json['amount'],
      actionTime: json['actionTime'],
      status: json['status'],
      isAuto: json['isAuto'],
      billno: json['billno'],
      gameCat: json['game_cat'],
      gameUsername: json['game_username'],
    );
  }
}

class ResponseTransferLogs {
  int? total;
  List<TransferLogs>? list;

  ResponseTransferLogs({
    this.list,
    this.total,
  });

  factory ResponseTransferLogs.fromJson(Map<String, dynamic> json) {
    List<dynamic>? dataList = json['list'];
    List<TransferLogs>? list;
    if (dataList != null) {
      list = dataList.map((item) => TransferLogs.fromJson(item)).toList();
    }
    return ResponseTransferLogs(
      total: json['code'] as int?,
      list: list,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((item) => item.toJson()).toList(),
      'total': total,
    };
  }
}
