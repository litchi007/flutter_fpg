class UGcheckinBonusModel {
  String? int_;
  int? switch_;
  bool? isComplete;
  bool? isCheckin;

  UGcheckinBonusModel({
    this.int_,
    this.switch_,
    this.isComplete,
    this.isCheckin,
  });

  factory UGcheckinBonusModel.fromJson(Map<String, dynamic> json) {
    return UGcheckinBonusModel(
      int_: json['int'],
      switch_: json['switch'],
      isComplete: json['isComplete'],
      isCheckin: json['isCheckin'],
    );
  }
}

class UGlevelModel {
  String? levelName;
  String? checkinCards;
  UGlevelModel({
    this.levelName,
    this.checkinCards,
  });
  factory UGlevelModel.fromJson(Map<String, dynamic> json) {
    return UGlevelModel(
      levelName: json['levelName'],
      checkinCards: json['checkinCards'],
    );
  }
}

class UGCheckinListModel {
  String? week;
  String? whichDay;
  String? integral;
  bool? isCheckin;
  bool? isMakeup;
  String? updateTime;

  UGCheckinListModel({
    this.week,
    this.whichDay,
    this.integral,
    this.isCheckin,
    this.isMakeup,
    this.updateTime,
  });

  factory UGCheckinListModel.fromJson(Map<String, dynamic> json) {
    return UGCheckinListModel(
      week: json['week']?.toString(),
      whichDay: json['whichDay']?.toString(),
      updateTime: json['updateTime']?.toString(),
      integral: json['integral'].toString(),
      isCheckin: json['isCheckin'],
      isMakeup: json['isMakeup'],
    );
  }
}

class UGSignInModel {
  String? serverTime; //
  int? checkinTimes; //
  String? checkinMoney; //
  bool? checkinSwitch; //
  bool? mkCheckinSwitch; //
  String? lastTime;
  List<UGCheckinListModel>? checkinList;
  List<UGcheckinBonusModel>? checkinBonus;
  List<UGlevelModel>? level;

  UGSignInModel({
    this.serverTime,
    this.checkinTimes,
    this.checkinMoney,
    this.checkinSwitch,
    this.mkCheckinSwitch,
    this.checkinList,
    this.checkinBonus,
    this.lastTime,
    this.level,
  });

  factory UGSignInModel.fromJson(Map<String, dynamic> json) {
    return UGSignInModel(
      checkinSwitch: json['checkinSwitch'],
      mkCheckinSwitch: json['mkCheckinSwitch'],
      serverTime: json['serverTime']?.toString(),
      checkinMoney: json['checkinMoney']?.toString(),
      lastTime: json['lastTime']?.toString(),
      checkinTimes: json['checkinTimes'],
      checkinList: json['checkinList'] != null
          ? (json['checkinList'] as List)
              .map((e) => UGCheckinListModel.fromJson(e))
              .toList()
          : null,
      level: json['level'] != null
          ? (json['level'] as List)
              .map((e) => UGlevelModel.fromJson(e))
              .toList()
          : null,
      checkinBonus: json['checkinBonus'] != null
          ? (json['checkinBonus'] as List)
              .map((e) => UGcheckinBonusModel.fromJson(e))
              .toList()
          : null,
    );
  }
}

class UGSignInHistoryModel {
  String? checkinDate; // 签到日期    // 2019-09-04
  String? integral; // 积分 // "10.00"
  String? remark; // 备注 // "签到送积分"

  UGSignInHistoryModel({this.checkinDate, this.integral, this.remark});

  // Factory method to convert JSON object to UGSignInHistoryModel
  factory UGSignInHistoryModel.fromJson(Map<String, dynamic> json) {
    return UGSignInHistoryModel(
      checkinDate: json['checkinDate'],
      integral: json['integral'],
      remark: json['remark'],
    );
  }
}
