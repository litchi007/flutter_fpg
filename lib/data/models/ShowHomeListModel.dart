class ShowHomeListModel {
  String? id;
  String? actionTime;
  String? type;
  String? type2;
  String? title;
  String? content;

  ShowHomeListModel({
    this.id,
    this.actionTime,
    this.type,
    this.type2,
    this.title,
    this.content,
  });

  factory ShowHomeListModel.fromJson(Map<String, dynamic> json) {
    return ShowHomeListModel(
      id: json['id'] as String?,
      actionTime: json['actionTime'] as String?,
      type: json['type'] as String?,
      type2: json['type2'] as String?,
      title: json['title'] as String?,
      content: json['content'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'actionTime': actionTime,
      'type': type,
      'type2': type2,
      'title': title,
      'content': content,
    };
  }
}
