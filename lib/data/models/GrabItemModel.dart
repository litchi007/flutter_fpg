class GrabItemModel {
  int? redBagId;
  String? miniRedBagAmount;
  bool? isMine;
  bool? isMax;
  bool? isLuck;
  int? luckAmount;
  dynamic time;
  dynamic createTime;
  String? uid;
  String? id;
  String? nickname;
  String? avatar;
  String? date;

  GrabItemModel({
    this.redBagId,
    this.miniRedBagAmount,
    this.isMine,
    this.isMax,
    this.isLuck,
    this.luckAmount,
    this.time,
    this.createTime,
    this.uid,
    this.id,
    this.nickname,
    this.avatar,
    this.date,
  });

  factory GrabItemModel.fromJson(Map<String, dynamic> json) {
    return GrabItemModel(
      redBagId: json['redBagId'],
      miniRedBagAmount: json['miniRedBagAmount'],
      isMine: json['isMine'],
      isMax: json['isMax'],
      isLuck: json['isLuck'],
      luckAmount: json['luckAmount'],
      time: json['time'],
      createTime: json['createTime'],
      uid: json['uid'],
      id: json['id'],
      nickname: json['nickname'],
      avatar: json['avatar'],
      date: json['date'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'redBagId': redBagId,
      'miniRedBagAmount': miniRedBagAmount,
      'isMine': isMine,
      'isMax': isMax,
      'isLuck': isLuck,
      'luckAmount': luckAmount,
      'time': time,
      'createTime': createTime,
      'uid': uid,
      'id': id,
      'nickname': nickname,
      'avatar': avatar,
      'date': date,
    };
  }
}
