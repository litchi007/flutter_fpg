class UGLoginModel {
  // sessid
  String? apiSid;
  // token
  String? apiToken;
  // 登录是否需要验证真实姓名
  bool? needFullName;
  // 时需要滑动验证
  bool? needVcode;
  // 登录是否需要谷歌验证
  bool? ggCheck;
  // 自定义参数
  String? sessid;
  String? token;
  String? pwd;
  String? clsName;

  UGLoginModel({
    this.apiSid,
    this.apiToken,
    this.needFullName,
    this.needVcode,
    this.ggCheck,
    this.sessid,
    this.token,
    this.pwd,
    this.clsName,
  });

  factory UGLoginModel.fromJson(Map<String, dynamic> json) {
    return UGLoginModel(
      apiSid: json['API-SID'],
      apiToken: json['API-TOKEN'],
      needFullName: json['needFullName'],
      needVcode: json['needVcode'],
      ggCheck: json['ggCheck'],
      sessid: json['sessid'],
      token: json['token'],
      pwd: json['pwd'],
      clsName: json['clsName'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'API-SID': apiSid,
      'API-TOKEN': apiToken,
      'needFullName': needFullName,
      'needVcode': needVcode,
      'ggCheck': ggCheck,
      'sessid': sessid,
      'token': token,
      'pwd': pwd,
      'clsName': clsName,
    };
  }
}
