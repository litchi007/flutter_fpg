class PopupMessage {
  // 消息html
  String content;
  String extra;
  // 消息id
  String id;
  // 是否弹窗显示
  bool isPopup;
  // 是否APP通知栏展示
  int isPushNotification;
  // 是否已读
  int isRead;
  // 已读时间 YYYY-MM-DD HH:mm:ss
  String readTime;
  // 内容简略文字
  String summary;
  // 标题
  String title;
  // 更新时间 YYYY-MM-DD HH:mm:ss
  String updateTime;

  PopupMessage({
    required this.content,
    required this.extra,
    required this.id,
    required this.isPopup,
    required this.isPushNotification,
    required this.isRead,
    required this.readTime,
    required this.summary,
    required this.title,
    required this.updateTime,
  });

  factory PopupMessage.fromJson(Map<String, dynamic> json) {
    return PopupMessage(
      content: json['content'],
      extra: json['extra'],
      id: json['id'],
      isPopup: json['isPopup'],
      isPushNotification: json['isPushNotification'],
      isRead: json['isRead'],
      readTime: json['readTime'],
      summary: json['summary'],
      title: json['title'],
      updateTime: json['updateTime'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'content': content,
      'extra': extra,
      'id': id,
      'isPopup': isPopup,
      'isPushNotification': isPushNotification,
      'isRead': isRead,
      'readTime': readTime,
      'summary': summary,
      'title': title,
      'updateTime': updateTime,
    };
  }
}

class UserToken {
  String? apiSid;
  String? apiToken;
  String? sessid;
  String? token;

  UserToken({
    this.apiSid,
    this.apiToken,
    this.sessid,
    this.token,
  });

  factory UserToken.fromJson(Map<String, dynamic> json) {
    return UserToken(
      apiSid: json['API-SID'],
      apiToken: json['API-TOKEN'],
      sessid: json['sessid'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'API-SID': apiSid,
      'API-TOKEN': apiToken,
      'sessid': sessid,
      'token': token,
    };
  }
}

// 首页报码器tab & 论坛帖子分类tab
class TabItem {
  // 彩种名（中），客户可能会自订名称，不能完全依据gameLobby
  String? label;
  // 彩种名（英）
  String? name;
  String? gameId;
  String? categoryId;
  String? alias;
  // 位置
  num? pos;

  TabItem(
      {this.alias,
      this.label,
      this.name,
      this.gameId,
      this.categoryId,
      this.pos});

  factory TabItem.fromJson(Map<String, dynamic> json) {
    return TabItem(
      label: json['label'],
      name: json['name'],
      gameId: json['gameId'],
      categoryId: json['categoryId'],
      alias: json['alias'],
      pos: json['pos'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'label': label,
      'name': name,
      'gameId': gameId,
      'categoryId': categoryId,
      'alias': alias,
      'pos': pos,
    };
  }
}

class Address {
  String? id;
  String? country;
  String? province;
  String? city;

  Address({this.id, this.country, this.province, this.city});

  // Method to create an Address object from a JSON map
  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      id: json['id'],
      country: json['country'],
      province: json['province'],
      city: json['city'],
    );
  }

  // Method to convert an Address object to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'country': country,
      'province': province,
      'city': city,
    };
  }
}
