class CallModel {
  final String uid;
  final String isCall;

  CallModel({
    required this.uid,
    required this.isCall,
  });

  factory CallModel.fromJson(Map<String, dynamic> json) {
    return CallModel(
      uid: json['uid'] as String,
      isCall: json['isCall'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'uid': uid,
      'isCall': isCall,
    };
  }
}
