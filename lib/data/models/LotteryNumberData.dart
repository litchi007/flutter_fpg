import 'dart:convert';

class LotteryNumberData {
  bool? auto;
  String? serverTime;
  String? lotteryStr;
  String? gameId;
  String? issue;
  String? endtime;
  String? lotteryTime;
  String? numbers;
  String? numColor;
  String? numSx;
  int? isFinish;
  String? lhcdocLotteryNo;
  List<String>? fiveElements;

  LotteryNumberData({
    this.auto,
    this.serverTime,
    this.lotteryStr,
    this.gameId,
    this.issue,
    this.endtime,
    this.lotteryTime,
    this.numbers,
    this.numColor,
    this.numSx,
    this.isFinish,
    this.lhcdocLotteryNo,
    this.fiveElements,
  });

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory LotteryNumberData.fromJson(Map<String, dynamic> json) {
    return LotteryNumberData(
      auto: json['auto'],
      serverTime: json['serverTime'],
      lotteryStr: json['lotteryStr'],
      gameId: json['gameId'],
      issue: json['issue'],
      endtime: json['endtime'],
      lotteryTime: json['lotteryTime'],
      numbers: json['numbers'],
      numColor: json['numColor'],
      numSx: json['numSx'],
      isFinish: json['isFinish'],
      lhcdocLotteryNo: json['lhcdocLotteryNo'],
      fiveElements: List<String>.from(json['fiveElements']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'auto': auto,
      'serverTime': serverTime,
      'lotteryStr': lotteryStr,
      'gameId': gameId,
      'issue': issue,
      'endtime': endtime,
      'lotteryTime': lotteryTime,
      'numbers': numbers,
      'numColor': numColor,
      'numSx': numSx,
      'isFinish': isFinish,
      'lhcdocLotteryNo': lhcdocLotteryNo,
      'fiveElements': fiveElements,
    };
  }
}
