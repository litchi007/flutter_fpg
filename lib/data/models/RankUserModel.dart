class RankUserModel {
  String? username;
  String? coin;
  String? type;
  String? logo;
  String? actionTime;
  RankUserModel(
      {this.actionTime, this.coin, this.logo, this.type, this.username});

  factory RankUserModel.fromJson(Map<String, dynamic> json) {
    return RankUserModel(
      username: json['username'],
      coin: json['coin'].toString(),
      type: json['type'],
      logo: json['logo'],
      actionTime: json['actionTime'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'coin': coin,
      'type': type,
      'logo': logo,
      'actionTime': actionTime,
    };
  }
}
