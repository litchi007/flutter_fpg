class NextIssueData {
  final dynamic id;
  final String? title;
  final String? logo;
  final String? gameType;
  final String? isSeal;
  final String? lowFreq;
  final String? serverTime;
  final String? curIssue;
  final String? displayNumber;
  final String? curOpenTime;
  final String? curCloseTime;
  final dynamic preIsOpen;
  final String? preIssue;
  final String? preDisplayNumber;
  final String? preOpenTime;
  final String? preNum;
  final String? preResult;
  final dynamic adEnable;
  final String? adPic;
  final dynamic adLink;
  final String? adGameType;
  final String? adLinkType;
  final String? isClose;
  final String? isInstant;
  final String? serverTimestamp;
  final List<ZodiacNumsData>? zodiacNums;
  final List<ZodiacNumsData>? fiveElements;
  final List<String>? redBalls;
  final List<String>? greenBalls;
  final List<String>? blueBalls;
  final List<String>? winningPlayers;
  final String? betTemplate;
  final String? hash;
  final String? verifUrl;

  final String? d0;
  final String? d1;
  final String? d2;
  final String? d3;
  final String? d4;
  final String? d5;
  final String? d6;
  final String? d7;
  final String? d8;

  final String? t0;
  final String? t1;
  final String? t2;
  final String? t3;
  final String? t4;
  final String? t5;
  final String? t6;
  final String? t7;
  final String? t8;
  final String? t9;

  NextIssueData({
    this.id,
    this.title,
    this.logo,
    this.gameType,
    this.isSeal,
    this.lowFreq,
    this.serverTime,
    this.curIssue,
    this.displayNumber,
    this.curOpenTime,
    this.curCloseTime,
    this.preIsOpen,
    this.preIssue,
    this.preDisplayNumber,
    this.preOpenTime,
    this.preNum,
    this.preResult,
    this.adEnable,
    this.adPic,
    this.adLink,
    this.adGameType,
    this.adLinkType,
    this.isClose,
    this.isInstant,
    this.serverTimestamp,
    this.zodiacNums,
    this.fiveElements,
    this.winningPlayers,
    this.redBalls,
    this.greenBalls,
    this.blueBalls,
    this.betTemplate,
    this.hash,
    this.verifUrl,
    this.d0,
    this.d1,
    this.d2,
    this.d3,
    this.d4,
    this.d5,
    this.d6,
    this.d7,
    this.d8,
    this.t0,
    this.t1,
    this.t2,
    this.t3,
    this.t4,
    this.t5,
    this.t6,
    this.t7,
    this.t8,
    this.t9,
  });

  factory NextIssueData.fromJson(Map<String, dynamic> json) {
    return NextIssueData(
      id: json['id'],
      title: json['title'],
      logo: json['logo'],
      gameType: json['gameType'],
      isSeal: json['isSeal'],
      lowFreq: json['lowFreq'],
      serverTime: json['serverTime'],
      curIssue: json['curIssue'],
      displayNumber: json['displayNumber'],
      curOpenTime: json['curOpenTime'],
      curCloseTime: json['curCloseTime'],
      preIsOpen: json['preIsOpen'],
      preIssue: json['preIssue'],
      preDisplayNumber: json['preDisplayNumber'],
      preOpenTime: json['preOpenTime'],
      preNum: json['preNum'],
      preResult: json['preResult'],
      adEnable: json['adEnable'],
      adPic: json['adPic'],
      adLink: json['adLink'],
      adGameType: json['adGameType'],
      adLinkType: json['adLinkType'],
      isClose: json['isClose'],
      isInstant: json['isInstant'],
      serverTimestamp: json['serverTimestamp'],
      zodiacNums: (json['zodiacNums'] as List?)
          ?.map((e) => ZodiacNumsData.fromJson(e))
          .toList(),
      fiveElements: (json['fiveElements'] as List?)
          ?.map((e) => ZodiacNumsData.fromJson(e))
          .toList(),
      winningPlayers:
          (json['winningPlayers'] as List?)?.map((e) => e.toString()).toList(),
      redBalls: (json['redBalls'] as List?)?.map((e) => e.toString()).toList(),
      greenBalls:
          (json['greenBalls'] as List?)?.map((e) => e.toString()).toList(),
      blueBalls:
          (json['blueBalls'] as List?)?.map((e) => e.toString()).toList(),
      betTemplate: json['betTemplate'],
      hash: json['hash'],
      verifUrl: json['verifUrl'],
      d0: json['d0'],
      d1: json['d1'],
      d2: json['d2'],
      d3: json['d3'],
      d4: json['d4'],
      d5: json['d5'],
      d6: json['d6'],
      d7: json['d7'],
      d8: json['d8'],
      t0: json['t0'],
      t1: json['t1'],
      t2: json['t2'],
      t3: json['t3'],
      t4: json['t4'],
      t5: json['t5'],
      t6: json['t6'],
      t7: json['t7'],
      t8: json['t8'],
      t9: json['t9'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'logo': logo,
      'gameType': gameType,
      'isSeal': isSeal,
      'lowFreq': lowFreq,
      'serverTime': serverTime,
      'curIssue': curIssue,
      'displayNumber': displayNumber,
      'curOpenTime': curOpenTime,
      'curCloseTime': curCloseTime,
      'preIsOpen': preIsOpen,
      'preIssue': preIssue,
      'preDisplayNumber': preDisplayNumber,
      'preOpenTime': preOpenTime,
      'preNum': preNum,
      'preResult': preResult,
      'adEnable': adEnable,
      'adPic': adPic,
      'adLink': adLink,
      'adGameType': adGameType,
      'adLinkType': adLinkType,
      'isClose': isClose,
      'isInstant': isInstant,
      'serverTimestamp': serverTimestamp,
      'zodiacNums': zodiacNums?.map((e) => e.toJson()).toList(),
      'fiveElements': fiveElements?.map((e) => e.toJson()).toList(),
      'winningPlayers': winningPlayers,
      'redBalls': redBalls,
      'greenBalls': greenBalls,
      'blueBalls': blueBalls,
      'betTemplate': betTemplate,
      'hash': hash,
      'verifUrl': verifUrl,
      'd0': d0,
      'd1': d1,
      'd2': d2,
      'd3': d3,
      'd4': d4,
      'd5': d5,
      'd6': d6,
      'd7': d7,
      'd8': d8,
      't0': t0,
      't1': t1,
      't2': t2,
      't3': t3,
      't4': t4,
      't5': t5,
      't6': t6,
      't7': t7,
      't8': t8,
      't9': t9,
    };
  }
}

class ZodiacNumsData {
  final String? key;
  final String? name;
  final List<String>? nums;

  ZodiacNumsData({this.key, this.name, this.nums});

  factory ZodiacNumsData.fromJson(Map<String, dynamic> json) {
    return ZodiacNumsData(
      key: json['key'],
      name: json['name'],
      nums: (json['nums'] as List?)?.map((e) => e as String).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'name': name,
      'nums': nums,
    };
  }
}
