// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_center.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserCenter _$UserCenterFromJson(Map<String, dynamic> json) => UserCenter(
      id: json['id'] as String?,
      parentId: json['parent_id'] as String?,
      category: json['category'] as String?,
      code: json['code'] as String?,
      name: json['name'] as String?,
      status: json['status'] as String?,
      sorts: json['sorts'] as String?,
      siteIds: json['site_ids'] as String?,
      siteId: json['site_id'] as String?,
      showListStyle: json['show_list_style'] as String?,
      linkUrl: json['link_url'] as String?,
      userCenterCategory: json['user_center_category'] as String?,
      sourceId: json['source_id'] as String?,
      imageHot: json['image_hot'] as String?,
      logo: json['logo'] as String?,
    );

Map<String, dynamic> _$UserCenterToJson(UserCenter instance) =>
    <String, dynamic>{
      'id': instance.id,
      'parent_id': instance.parentId,
      'category': instance.category,
      'code': instance.code,
      'name': instance.name,
      'status': instance.status,
      'sorts': instance.sorts,
      'site_ids': instance.siteIds,
      'site_id': instance.siteId,
      'show_list_style': instance.showListStyle,
      'link_url': instance.linkUrl,
      'user_center_category': instance.userCenterCategory,
      'source_id': instance.sourceId,
      'image_hot': instance.imageHot,
      'logo': instance.logo,
    };
