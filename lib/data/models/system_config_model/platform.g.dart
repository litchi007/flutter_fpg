// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'platform.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Platform _$PlatformFromJson(Map<String, dynamic> json) => Platform(
      facebook: json['facebook'] as bool?,
    );

Map<String, dynamic> _$PlatformToJson(Platform instance) => <String, dynamic>{
      'facebook': instance.facebook,
    };
