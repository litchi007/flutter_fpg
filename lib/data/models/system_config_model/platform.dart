import 'package:json_annotation/json_annotation.dart';

part 'platform.g.dart';

@JsonSerializable()
class Platform {
  final bool? facebook;

  const Platform({this.facebook});

  @override
  String toString() => 'Platform(facebook: $facebook)';

  factory Platform.fromJson(Map<String, dynamic> json) {
    return _$PlatformFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PlatformToJson(this);

  Platform copyWith({
    bool? facebook,
  }) {
    return Platform(
      facebook: facebook ?? this.facebook,
    );
  }
}
