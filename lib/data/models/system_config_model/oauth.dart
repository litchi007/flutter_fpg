import 'package:json_annotation/json_annotation.dart';

import 'platform.dart';

part 'oauth.g.dart';

@JsonSerializable()
class Oauth {
  @JsonKey(name: 'switch')
  final bool? switch_;
  final Platform? platform;

  const Oauth({this.switch_, this.platform});

  @override
  String toString() => 'Oauth(switch: $switch_, platform: $platform)';

  factory Oauth.fromJson(Map<String, dynamic> json) => _$OauthFromJson(json);

  Map<String, dynamic> toJson() => _$OauthToJson(this);

  Oauth copyWith({
    bool? switch_,
    Platform? platform,
  }) {
    return Oauth(
      switch_: switch_ ?? this.switch_,
      platform: platform ?? this.platform,
    );
  }
}
