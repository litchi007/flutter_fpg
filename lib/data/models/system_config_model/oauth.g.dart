// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oauth.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Oauth _$OauthFromJson(Map<String, dynamic> json) => Oauth(
      switch_: json['switch'] as bool?,
      platform: json['platform'] == null
          ? null
          : Platform.fromJson(json['platform'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OauthToJson(Oauth instance) => <String, dynamic>{
      'switch': instance.switch_,
      'platform': instance.platform,
    };
