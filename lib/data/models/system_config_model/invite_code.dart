import 'package:json_annotation/json_annotation.dart';

part 'invite_code.g.dart';

@JsonSerializable()
class InviteCode {
  @JsonKey(name: 'switch')
  final String? switch_;
  final String? displayWord;
  final String? canGenNum;
  final String? canUseNum;
  final String? randomSwitch;
  final String? randomLength;
  final String? noticeSwitch;
  final String? noticeText;

  const InviteCode({
    this.switch_,
    this.displayWord,
    this.canGenNum,
    this.canUseNum,
    this.randomSwitch,
    this.randomLength,
    this.noticeSwitch,
    this.noticeText,
  });

  @override
  String toString() {
    return 'InviteCode(switch: $switch_, displayWord: $displayWord, canGenNum: $canGenNum, canUseNum: $canUseNum, randomSwitch: $randomSwitch, randomLength: $randomLength, noticeSwitch: $noticeSwitch, noticeText: $noticeText)';
  }

  factory InviteCode.fromJson(Map<String, dynamic> json) {
    return _$InviteCodeFromJson(json);
  }

  Map<String, dynamic> toJson() => _$InviteCodeToJson(this);

  InviteCode copyWith({
    String? switch_,
    String? displayWord,
    String? canGenNum,
    String? canUseNum,
    String? randomSwitch,
    String? randomLength,
    String? noticeSwitch,
    String? noticeText,
  }) {
    return InviteCode(
      switch_: switch_ ?? this.switch_,
      displayWord: displayWord ?? this.displayWord,
      canGenNum: canGenNum ?? this.canGenNum,
      canUseNum: canUseNum ?? this.canUseNum,
      randomSwitch: randomSwitch ?? this.randomSwitch,
      randomLength: randomLength ?? this.randomLength,
      noticeSwitch: noticeSwitch ?? this.noticeSwitch,
      noticeText: noticeText ?? this.noticeText,
    );
  }
}
