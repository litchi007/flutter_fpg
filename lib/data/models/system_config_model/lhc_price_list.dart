import 'package:json_annotation/json_annotation.dart';

part 'lhc_price_list.g.dart';

@JsonSerializable()
class LhcPriceList {
  final String? id;
  final String? alias;
  final String? priceMin;
  final String? priceMax;

  const LhcPriceList({this.id, this.alias, this.priceMin, this.priceMax});

  @override
  String toString() {
    return 'LhcPriceList(id: $id, alias: $alias, priceMin: $priceMin, priceMax: $priceMax)';
  }

  factory LhcPriceList.fromJson(Map<String, dynamic> json) {
    return _$LhcPriceListFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LhcPriceListToJson(this);

  LhcPriceList copyWith({
    String? id,
    String? alias,
    String? priceMin,
    String? priceMax,
  }) {
    return LhcPriceList(
      id: id ?? this.id,
      alias: alias ?? this.alias,
      priceMin: priceMin ?? this.priceMin,
      priceMax: priceMax ?? this.priceMax,
    );
  }
}
