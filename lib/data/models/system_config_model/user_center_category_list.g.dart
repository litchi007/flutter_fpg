// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_center_category_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserCenterCategoryList _$UserCenterCategoryListFromJson(
        Map<String, dynamic> json) =>
    UserCenterCategoryList(
      id: (json['id'] as num?)?.toInt(),
      name: json['name'] as String?,
    );

Map<String, dynamic> _$UserCenterCategoryListToJson(
        UserCenterCategoryList instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
