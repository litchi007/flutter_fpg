import 'package:json_annotation/json_annotation.dart';

part 'login_notice.g.dart';

@JsonSerializable()
class LoginNotice {
  @JsonKey(name: 'switch')
  final String? switch_;
  final String? text;

  const LoginNotice({this.switch_, this.text});

  @override
  String toString() => 'LoginNotice(switch: $switch_, text: $text)';

  factory LoginNotice.fromJson(Map<String, dynamic> json) {
    return _$LoginNoticeFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LoginNoticeToJson(this);

  LoginNotice copyWith({
    String? switch_,
    String? text,
  }) {
    return LoginNotice(
      switch_: switch_ ?? this.switch_,
      text: text ?? this.text,
    );
  }
}
