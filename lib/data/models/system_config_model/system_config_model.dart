import 'package:json_annotation/json_annotation.dart';

import 'invite_code.dart';
import 'lhc_price_list.dart';
import 'login_notice.dart';
import 'oauth.dart';
import 'user_center.dart';
import 'user_center_category_list.dart';

part 'system_config_model.g.dart';

@JsonSerializable()
class SystemConfigModel {
  final String? host;
  final String? port;
  final String? apiHost;
  final int? siteId;
  final String? merId;
  final String? webName;
  final String? zxkfUrl;
  final String? allowreg;
  final String? closeregreason;
  @JsonKey(name: 'agent_m_apply')
  final String? agentMApply;
  @JsonKey(name: 'page_title')
  final String? pageTitle;
  @JsonKey(name: 'login_to')
  final String? loginTo;
  @JsonKey(name: 'member_agent')
  final String? memberAgent;
  @JsonKey(name: 'm_promote_pos')
  final String? mPromotePos;
  final String? agentRegbutton;
  @JsonKey(name: 'pass_length_min')
  final String? passLengthMin;
  @JsonKey(name: 'pass_length_max')
  final String? passLengthMax;
  @JsonKey(name: 'pass_limit')
  final String? passLimit;
  @JsonKey(name: 'myreco_img')
  final String? myrecoImg;
  final String? missionName;
  @JsonKey(name: 'hide_reco')
  final String? hideReco;
  @JsonKey(name: 'reg_name')
  final String? regName;
  @JsonKey(name: 'reg_fundpwd')
  final String? regFundpwd;
  @JsonKey(name: 'reg_qq')
  final String? regQq;
  @JsonKey(name: 'reg_wx')
  final String? regWx;
  @JsonKey(name: 'reg_phone')
  final String? regPhone;
  @JsonKey(name: 'reg_fb')
  final String? regFb;
  @JsonKey(name: 'reg_zalo')
  final String? regZalo;
  final String? smsVerify;
  @JsonKey(name: 'reg_vcode')
  final String? regVcode;
  @JsonKey(name: 'reg_email')
  final String? regEmail;
  @JsonKey(name: 'bet_bank_card')
  final String? betBankCard;
  @JsonKey(name: 'reg_line')
  final String? regLine;
  final bool? googleVerifier;
  final String? znxNotify;
  final String? regAndLogin;
  final String? smsFindPassword;
  @JsonKey(name: 'user_length_limit_switch')
  final String? userLengthLimitSwitch;
  @JsonKey(name: 'user_length_min')
  final String? userLengthMin;
  @JsonKey(name: 'user_length_max')
  final String? userLengthMax;
  final String? oftenLoginArea;
  final String? missionSwitch;
  final String? isIntToMoney;
  final String? missionBili;
  final String? checkinSwitch;
  final String? mkCheckinSwitch;
  @JsonKey(name: 'chatapp_logo')
  final String? chatappLogo;
  @JsonKey(name: 'chatapp_start_logo')
  final String? chatappStartLogo;
  @JsonKey(name: 'mobile_logo')
  final String? mobileLogo;
  @JsonKey(name: 'mobile_icon')
  final String? mobileIcon;
  final int? missionPopUpSwitch;
  final int? guestSwitch;
  @JsonKey(name: 'mission_logo')
  final String? missionLogo;
  final String? minWithdrawMoney;
  final String? maxWithdrawMoney;
  @JsonKey(name: 'announce_first')
  final String? announceFirst;
  final String? adSliderTimer;
  @JsonKey(name: 'popup_tab')
  final List<String>? popupTab;
  @JsonKey(name: 'popup_announce')
  final String? popupAnnounce;
  @JsonKey(name: 'popup_hour')
  final String? popupHour;
  @JsonKey(name: 'popup_type')
  final String? popupType;
  @JsonKey(name: 'switchDOYUSDTTrans')
  final String? switchDoyusdtTrans;
  final String? yuebaoName;
  final bool? allowMemberCancelBet;
  final int? balanceDecimal;
  final String? chaseNumber;
  final String? selectNumber;
  final String? switchBindVerify;
  final String? switchShowFriendReferral;
  final String? showNavigationBar;
  final String? rankingListSwitch;
  final int? grabRedbagRankingListSwitch;
  final bool? chatRoomSwitch;
  final String? chatRoomName;
  final bool? chatFollowSwitch;
  final String? chatShareBetMinAmount;
  final String? mobileGameHall;
  final String? betMin;
  final String? betMax;
  final String? customiseBetMax;
  final String? switchBalanceChannel;
  final String? balanceChannelStartTime;
  final String? balanceChannelEndTime;
  final String? balanceChannelPrompt;
  final String? switchYuebaoChannel;
  final String? yuebaoChannelStartTime;
  final String? yuebaoChannelEndTime;
  final String? yuebaoChannelPrompt;
  final bool? mobileHomeGameTypeSwitch;
  final String? picTypeshow;
  final List<String>? apiHosts;
  final String? curLangCode;
  final String? allowMemberModifyLanguage;
  final String? mobileTemplateCategory;
  final String? mobileTemplateBackground;
  final String? mobileTemplateStyle;
  final String? mobileViewModel;
  final String? mobileBetStyle;
  final String? mobileTemplateLhcStyle;
  final String? mobileTemplateGpkStyle;
  @JsonKey(name: 'mobileTemplateHBStyle')
  final String? mobileTemplateHbStyle;
  final String? mobileTemplateXbjStyle;
  final List<dynamic>? mobileMenu;
  final String? kjwTemplateSp;
  final List<dynamic>? kjwLotteries;
  @JsonKey(name: 'serviceQQ1')
  final String? serviceQq1;
  @JsonKey(name: 'serviceQQ2')
  final String? serviceQq2;
  final String? facekfUrl;
  final String? whatskfUrl;
  final String? appPopupQqNum;
  final String? appPopupQqImg;
  final String? appPopupWechatNum;
  final String? appPopupWechatImg;
  final String? withdrawChannelAd;
  final String? zxkfUrl2;
  final dynamic zalokfUrl;
  final dynamic telegramkfUrl;
  final String? shortCut;
  final String? shortCutIcon;
  final bool? iosRelease;
  final bool? yuebaoSwitch;
  final String? easyRememberDomain;
  final bool? loginVCode;
  final String? loginVCodeType;
  @JsonKey(name: 'login_chatApp_vcode')
  final int? loginChatAppVcode;
  @JsonKey(name: 'login_chatApp_vcode_type')
  final int? loginChatAppVcodeType;
  final String? chatRoomServer;
  final List<UserCenter>? userCenter;
  final bool? lhcdocSwitchMobileHome;
  final List<UserCenterCategoryList>? userCenterCategoryList;
  final bool? lhcdocMiCard;
  final List<LhcPriceList>? lhcPriceList;
  final bool? lhcdocIsShowNav;
  final bool? lhcdocUserContCheck;
  final bool? lhcdocUserContRepCheck;
  final int? qdSwitchDe;
  final int? qdSwitchWi;
  final int? qdSwitch;
  final String? appDownloadUrl;
  final String? appdownloadbar;
  final int? domainBindAgentId;
  final String? proxyDomainDefault;
  final String? chatLink;
  final bool? switchAgentRecharge;
  final int? nameAgentRecharge;
  final bool? switchShowActivityCategory;
  final bool? betAmountIsDecimal;
  final bool? activeReturnCoinStatus;
  final int? activeReturnCoinRatio;
  final int? autoTransferLimit;
  final bool? switchAutoTransfer;
  final String? currency;
  final String? switchToK;
  final String? appSelectType;
  @JsonKey(name: 'reg_login_bg_sw')
  final String? regLoginBgSw;
  @JsonKey(name: 'reg_login_bg')
  final String? regLoginBg;
  final Oauth? oauth;
  final LoginNotice? loginNotice;
  final InviteCode? inviteCode;
  final String? inviteWord;
  final String? inviteCodeSwitch;
  final String? regBindCardSwitch;
  @JsonKey(name: 'frontend_agent_add_member')
  final String? frontendAgentAddMember;
  final String? regNameSwitch;
  @JsonKey(name: 'switchAgentRechargeDML')
  final String? switchAgentRechargeDml;
  final String? isAgentRechargeNeededPwd;
  final String? switchCoinPwd;
  final int? switchCoinPwdSms;
  final List<String>? coinPwdAuditOptionAry;
  final int? mBonsSwitch;
  @JsonKey(name: 'add_edit_bank_sw')
  final String? addEditBankSw;
  @JsonKey(name: 'add_edit_alipay_sw')
  final String? addEditAlipaySw;
  @JsonKey(name: 'add_edit_wechat_sw')
  final String? addEditWechatSw;
  @JsonKey(name: 'add_edit_vcoin_sw')
  final String? addEditVcoinSw;
  @JsonKey(name: 'add_edit_kdou_sw')
  final int? addEditKdouSw;
  @JsonKey(name: 'recharge_img_sw')
  final String? rechargeImgSw;
  final List<String>? staticServers;
  final String? loginsessid;

  const SystemConfigModel({
    this.host,
    this.port,
    this.apiHost,
    this.siteId,
    this.merId,
    this.webName,
    this.zxkfUrl,
    this.allowreg,
    this.closeregreason,
    this.agentMApply,
    this.pageTitle,
    this.loginTo,
    this.memberAgent,
    this.mPromotePos,
    this.agentRegbutton,
    this.passLengthMin,
    this.passLengthMax,
    this.passLimit,
    this.myrecoImg,
    this.missionName,
    this.hideReco,
    this.regName,
    this.regFundpwd,
    this.regQq,
    this.regWx,
    this.regPhone,
    this.regFb,
    this.regZalo,
    this.smsVerify,
    this.regVcode,
    this.regEmail,
    this.betBankCard,
    this.regLine,
    this.googleVerifier,
    this.znxNotify,
    this.regAndLogin,
    this.smsFindPassword,
    this.userLengthLimitSwitch,
    this.userLengthMin,
    this.userLengthMax,
    this.oftenLoginArea,
    this.missionSwitch,
    this.isIntToMoney,
    this.missionBili,
    this.checkinSwitch,
    this.mkCheckinSwitch,
    this.chatappLogo,
    this.chatappStartLogo,
    this.mobileLogo,
    this.mobileIcon,
    this.missionPopUpSwitch,
    this.guestSwitch,
    this.missionLogo,
    this.minWithdrawMoney,
    this.maxWithdrawMoney,
    this.announceFirst,
    this.adSliderTimer,
    this.popupTab,
    this.popupAnnounce,
    this.popupHour,
    this.popupType,
    this.switchDoyusdtTrans,
    this.yuebaoName,
    this.allowMemberCancelBet,
    this.balanceDecimal,
    this.chaseNumber,
    this.selectNumber,
    this.switchBindVerify,
    this.switchShowFriendReferral,
    this.showNavigationBar,
    this.rankingListSwitch,
    this.grabRedbagRankingListSwitch,
    this.chatRoomSwitch,
    this.chatRoomName,
    this.chatFollowSwitch,
    this.chatShareBetMinAmount,
    this.mobileGameHall,
    this.betMin,
    this.betMax,
    this.customiseBetMax,
    this.switchBalanceChannel,
    this.balanceChannelStartTime,
    this.balanceChannelEndTime,
    this.balanceChannelPrompt,
    this.switchYuebaoChannel,
    this.yuebaoChannelStartTime,
    this.yuebaoChannelEndTime,
    this.yuebaoChannelPrompt,
    this.mobileHomeGameTypeSwitch,
    this.picTypeshow,
    this.apiHosts,
    this.curLangCode,
    this.allowMemberModifyLanguage,
    this.mobileTemplateCategory,
    this.mobileTemplateBackground,
    this.mobileTemplateStyle,
    this.mobileViewModel,
    this.mobileBetStyle,
    this.mobileTemplateLhcStyle,
    this.mobileTemplateGpkStyle,
    this.mobileTemplateHbStyle,
    this.mobileTemplateXbjStyle,
    this.mobileMenu,
    this.kjwTemplateSp,
    this.kjwLotteries,
    this.serviceQq1,
    this.serviceQq2,
    this.facekfUrl,
    this.whatskfUrl,
    this.appPopupQqNum,
    this.appPopupQqImg,
    this.appPopupWechatNum,
    this.appPopupWechatImg,
    this.withdrawChannelAd,
    this.zxkfUrl2,
    this.zalokfUrl,
    this.telegramkfUrl,
    this.shortCut,
    this.shortCutIcon,
    this.iosRelease,
    this.yuebaoSwitch,
    this.easyRememberDomain,
    this.loginVCode,
    this.loginVCodeType,
    this.loginChatAppVcode,
    this.loginChatAppVcodeType,
    this.chatRoomServer,
    this.userCenter,
    this.lhcdocSwitchMobileHome,
    this.userCenterCategoryList,
    this.lhcdocMiCard,
    this.lhcPriceList,
    this.lhcdocIsShowNav,
    this.lhcdocUserContCheck,
    this.lhcdocUserContRepCheck,
    this.qdSwitchDe,
    this.qdSwitchWi,
    this.qdSwitch,
    this.appDownloadUrl,
    this.appdownloadbar,
    this.domainBindAgentId,
    this.proxyDomainDefault,
    this.chatLink,
    this.switchAgentRecharge,
    this.nameAgentRecharge,
    this.switchShowActivityCategory,
    this.betAmountIsDecimal,
    this.activeReturnCoinStatus,
    this.activeReturnCoinRatio,
    this.autoTransferLimit,
    this.switchAutoTransfer,
    this.currency,
    this.switchToK,
    this.appSelectType,
    this.regLoginBgSw,
    this.regLoginBg,
    this.oauth,
    this.loginNotice,
    this.inviteCode,
    this.inviteWord,
    this.inviteCodeSwitch,
    this.regBindCardSwitch,
    this.frontendAgentAddMember,
    this.regNameSwitch,
    this.switchAgentRechargeDml,
    this.isAgentRechargeNeededPwd,
    this.switchCoinPwd,
    this.switchCoinPwdSms,
    this.coinPwdAuditOptionAry,
    this.mBonsSwitch,
    this.addEditBankSw,
    this.addEditAlipaySw,
    this.addEditWechatSw,
    this.addEditVcoinSw,
    this.addEditKdouSw,
    this.rechargeImgSw,
    this.staticServers,
    this.loginsessid,
  });

  @override
  String toString() {
    return 'SystemConfigModel(host: $host, port: $port, apiHost: $apiHost, siteId: $siteId, merId: $merId, webName: $webName, zxkfUrl: $zxkfUrl, allowreg: $allowreg, closeregreason: $closeregreason, agentMApply: $agentMApply, pageTitle: $pageTitle, loginTo: $loginTo, memberAgent: $memberAgent, mPromotePos: $mPromotePos, agentRegbutton: $agentRegbutton, passLengthMin: $passLengthMin, passLengthMax: $passLengthMax, passLimit: $passLimit, myrecoImg: $myrecoImg, missionName: $missionName, hideReco: $hideReco, regName: $regName, regFundpwd: $regFundpwd, regQq: $regQq, regWx: $regWx, regPhone: $regPhone, regFb: $regFb, regZalo: $regZalo, smsVerify: $smsVerify, regVcode: $regVcode, regEmail: $regEmail, betBankCard: $betBankCard, regLine: $regLine, googleVerifier: $googleVerifier, znxNotify: $znxNotify, regAndLogin: $regAndLogin, smsFindPassword: $smsFindPassword, userLengthLimitSwitch: $userLengthLimitSwitch, userLengthMin: $userLengthMin, userLengthMax: $userLengthMax, oftenLoginArea: $oftenLoginArea, missionSwitch: $missionSwitch, isIntToMoney: $isIntToMoney, missionBili: $missionBili, checkinSwitch: $checkinSwitch, mkCheckinSwitch: $mkCheckinSwitch, chatappLogo: $chatappLogo, chatappStartLogo: $chatappStartLogo, mobileLogo: $mobileLogo, mobileIcon: $mobileIcon, missionPopUpSwitch: $missionPopUpSwitch, guestSwitch: $guestSwitch, missionLogo: $missionLogo, minWithdrawMoney: $minWithdrawMoney, maxWithdrawMoney: $maxWithdrawMoney, announceFirst: $announceFirst, adSliderTimer: $adSliderTimer, popupTab: $popupTab, popupAnnounce: $popupAnnounce, popupHour: $popupHour, popupType: $popupType, switchDoyusdtTrans: $switchDoyusdtTrans, yuebaoName: $yuebaoName, allowMemberCancelBet: $allowMemberCancelBet, balanceDecimal: $balanceDecimal, chaseNumber: $chaseNumber, selectNumber: $selectNumber, switchBindVerify: $switchBindVerify, switchShowFriendReferral: $switchShowFriendReferral, showNavigationBar: $showNavigationBar, rankingListSwitch: $rankingListSwitch, grabRedbagRankingListSwitch: $grabRedbagRankingListSwitch, chatRoomSwitch: $chatRoomSwitch, chatRoomName: $chatRoomName, chatFollowSwitch: $chatFollowSwitch, chatShareBetMinAmount: $chatShareBetMinAmount, mobileGameHall: $mobileGameHall, betMin: $betMin, betMax: $betMax, customiseBetMax: $customiseBetMax, switchBalanceChannel: $switchBalanceChannel, balanceChannelStartTime: $balanceChannelStartTime, balanceChannelEndTime: $balanceChannelEndTime, balanceChannelPrompt: $balanceChannelPrompt, switchYuebaoChannel: $switchYuebaoChannel, yuebaoChannelStartTime: $yuebaoChannelStartTime, yuebaoChannelEndTime: $yuebaoChannelEndTime, yuebaoChannelPrompt: $yuebaoChannelPrompt, mobileHomeGameTypeSwitch: $mobileHomeGameTypeSwitch, picTypeshow: $picTypeshow, apiHosts: $apiHosts, curLangCode: $curLangCode, allowMemberModifyLanguage: $allowMemberModifyLanguage, mobileTemplateCategory: $mobileTemplateCategory, mobileTemplateBackground: $mobileTemplateBackground, mobileTemplateStyle: $mobileTemplateStyle, mobileViewModel: $mobileViewModel, mobileBetStyle: $mobileBetStyle, mobileTemplateLhcStyle: $mobileTemplateLhcStyle, mobileTemplateGpkStyle: $mobileTemplateGpkStyle, mobileTemplateHbStyle: $mobileTemplateHbStyle, mobileTemplateXbjStyle: $mobileTemplateXbjStyle, mobileMenu: $mobileMenu, kjwTemplateSp: $kjwTemplateSp, kjwLotteries: $kjwLotteries, serviceQq1: $serviceQq1, serviceQq2: $serviceQq2, facekfUrl: $facekfUrl, whatskfUrl: $whatskfUrl, appPopupQqNum: $appPopupQqNum, appPopupQqImg: $appPopupQqImg, appPopupWechatNum: $appPopupWechatNum, appPopupWechatImg: $appPopupWechatImg, withdrawChannelAd: $withdrawChannelAd, zxkfUrl2: $zxkfUrl2, zalokfUrl: $zalokfUrl, telegramkfUrl: $telegramkfUrl, shortCut: $shortCut, shortCutIcon: $shortCutIcon, iosRelease: $iosRelease, yuebaoSwitch: $yuebaoSwitch, easyRememberDomain: $easyRememberDomain, loginVCode: $loginVCode, loginVCodeType: $loginVCodeType, loginChatAppVcode: $loginChatAppVcode, loginChatAppVcodeType: $loginChatAppVcodeType, chatRoomServer: $chatRoomServer, userCenter: $userCenter, lhcdocSwitchMobileHome: $lhcdocSwitchMobileHome, userCenterCategoryList: $userCenterCategoryList, lhcdocMiCard: $lhcdocMiCard, lhcPriceList: $lhcPriceList, lhcdocIsShowNav: $lhcdocIsShowNav, lhcdocUserContCheck: $lhcdocUserContCheck, lhcdocUserContRepCheck: $lhcdocUserContRepCheck, qdSwitchDe: $qdSwitchDe, qdSwitchWi: $qdSwitchWi, qdSwitch: $qdSwitch, appDownloadUrl: $appDownloadUrl, appdownloadbar: $appdownloadbar, domainBindAgentId: $domainBindAgentId, proxyDomainDefault: $proxyDomainDefault, chatLink: $chatLink, switchAgentRecharge: $switchAgentRecharge, nameAgentRecharge: $nameAgentRecharge, switchShowActivityCategory: $switchShowActivityCategory, betAmountIsDecimal: $betAmountIsDecimal, activeReturnCoinStatus: $activeReturnCoinStatus, activeReturnCoinRatio: $activeReturnCoinRatio, autoTransferLimit: $autoTransferLimit, switchAutoTransfer: $switchAutoTransfer, currency: $currency, switchToK: $switchToK, appSelectType: $appSelectType, regLoginBgSw: $regLoginBgSw, regLoginBg: $regLoginBg, oauth: $oauth, loginNotice: $loginNotice, inviteCode: $inviteCode, inviteWord: $inviteWord, inviteCodeSwitch: $inviteCodeSwitch, regBindCardSwitch: $regBindCardSwitch, frontendAgentAddMember: $frontendAgentAddMember, regNameSwitch: $regNameSwitch, switchAgentRechargeDml: $switchAgentRechargeDml, isAgentRechargeNeededPwd: $isAgentRechargeNeededPwd, switchCoinPwd: $switchCoinPwd, switchCoinPwdSms: $switchCoinPwdSms, coinPwdAuditOptionAry: $coinPwdAuditOptionAry, mBonsSwitch: $mBonsSwitch, addEditBankSw: $addEditBankSw, addEditAlipaySw: $addEditAlipaySw, addEditWechatSw: $addEditWechatSw, addEditVcoinSw: $addEditVcoinSw, addEditKdouSw: $addEditKdouSw, rechargeImgSw: $rechargeImgSw, staticServers: $staticServers, loginsessid: $loginsessid)';
  }

  factory SystemConfigModel.fromJson(Map<String, dynamic> json) {
    return _$SystemConfigModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SystemConfigModelToJson(this);

  SystemConfigModel copyWith({
    String? host,
    String? port,
    String? apiHost,
    int? siteId,
    String? merId,
    String? webName,
    String? zxkfUrl,
    String? allowreg,
    String? closeregreason,
    String? agentMApply,
    String? pageTitle,
    String? loginTo,
    String? memberAgent,
    String? mPromotePos,
    String? agentRegbutton,
    String? passLengthMin,
    String? passLengthMax,
    String? passLimit,
    String? myrecoImg,
    String? missionName,
    String? hideReco,
    String? regName,
    String? regFundpwd,
    String? regQq,
    String? regWx,
    String? regPhone,
    String? regFb,
    String? regZalo,
    String? smsVerify,
    String? regVcode,
    String? regEmail,
    String? betBankCard,
    String? regLine,
    bool? googleVerifier,
    String? znxNotify,
    String? regAndLogin,
    String? smsFindPassword,
    String? userLengthLimitSwitch,
    String? userLengthMin,
    String? userLengthMax,
    String? oftenLoginArea,
    String? missionSwitch,
    String? isIntToMoney,
    String? missionBili,
    String? checkinSwitch,
    String? mkCheckinSwitch,
    String? chatappLogo,
    String? chatappStartLogo,
    String? mobileLogo,
    String? mobileIcon,
    int? missionPopUpSwitch,
    int? guestSwitch,
    String? missionLogo,
    String? minWithdrawMoney,
    String? maxWithdrawMoney,
    String? announceFirst,
    String? adSliderTimer,
    List<String>? popupTab,
    String? popupAnnounce,
    String? popupHour,
    String? popupType,
    String? switchDoyusdtTrans,
    String? yuebaoName,
    bool? allowMemberCancelBet,
    int? balanceDecimal,
    String? chaseNumber,
    String? selectNumber,
    String? switchBindVerify,
    String? switchShowFriendReferral,
    String? showNavigationBar,
    String? rankingListSwitch,
    int? grabRedbagRankingListSwitch,
    bool? chatRoomSwitch,
    String? chatRoomName,
    bool? chatFollowSwitch,
    String? chatShareBetMinAmount,
    String? mobileGameHall,
    String? betMin,
    String? betMax,
    String? customiseBetMax,
    String? switchBalanceChannel,
    String? balanceChannelStartTime,
    String? balanceChannelEndTime,
    String? balanceChannelPrompt,
    String? switchYuebaoChannel,
    String? yuebaoChannelStartTime,
    String? yuebaoChannelEndTime,
    String? yuebaoChannelPrompt,
    bool? mobileHomeGameTypeSwitch,
    String? picTypeshow,
    List<String>? apiHosts,
    String? curLangCode,
    String? allowMemberModifyLanguage,
    String? mobileTemplateCategory,
    String? mobileTemplateBackground,
    String? mobileTemplateStyle,
    String? mobileViewModel,
    String? mobileBetStyle,
    String? mobileTemplateLhcStyle,
    String? mobileTemplateGpkStyle,
    String? mobileTemplateHbStyle,
    String? mobileTemplateXbjStyle,
    List<dynamic>? mobileMenu,
    String? kjwTemplateSp,
    List<dynamic>? kjwLotteries,
    String? serviceQq1,
    String? serviceQq2,
    String? facekfUrl,
    String? whatskfUrl,
    String? appPopupQqNum,
    String? appPopupQqImg,
    String? appPopupWechatNum,
    String? appPopupWechatImg,
    String? withdrawChannelAd,
    String? zxkfUrl2,
    dynamic zalokfUrl,
    dynamic telegramkfUrl,
    String? shortCut,
    String? shortCutIcon,
    bool? iosRelease,
    bool? yuebaoSwitch,
    String? easyRememberDomain,
    bool? loginVCode,
    String? loginVCodeType,
    int? loginChatAppVcode,
    int? loginChatAppVcodeType,
    String? chatRoomServer,
    List<UserCenter>? userCenter,
    bool? lhcdocSwitchMobileHome,
    List<UserCenterCategoryList>? userCenterCategoryList,
    bool? lhcdocMiCard,
    List<LhcPriceList>? lhcPriceList,
    bool? lhcdocIsShowNav,
    bool? lhcdocUserContCheck,
    bool? lhcdocUserContRepCheck,
    int? qdSwitchDe,
    int? qdSwitchWi,
    int? qdSwitch,
    String? appDownloadUrl,
    String? appdownloadbar,
    int? domainBindAgentId,
    String? proxyDomainDefault,
    String? chatLink,
    bool? switchAgentRecharge,
    int? nameAgentRecharge,
    bool? switchShowActivityCategory,
    bool? betAmountIsDecimal,
    bool? activeReturnCoinStatus,
    int? activeReturnCoinRatio,
    int? autoTransferLimit,
    bool? switchAutoTransfer,
    String? currency,
    String? switchToK,
    String? appSelectType,
    String? regLoginBgSw,
    String? regLoginBg,
    Oauth? oauth,
    LoginNotice? loginNotice,
    InviteCode? inviteCode,
    String? inviteWord,
    String? inviteCodeSwitch,
    String? regBindCardSwitch,
    String? frontendAgentAddMember,
    String? regNameSwitch,
    String? switchAgentRechargeDml,
    String? isAgentRechargeNeededPwd,
    String? switchCoinPwd,
    int? switchCoinPwdSms,
    List<String>? coinPwdAuditOptionAry,
    int? mBonsSwitch,
    String? addEditBankSw,
    String? addEditAlipaySw,
    String? addEditWechatSw,
    String? addEditVcoinSw,
    int? addEditKdouSw,
    String? rechargeImgSw,
    List<String>? staticServers,
    String? loginsessid,
  }) {
    return SystemConfigModel(
      host: host ?? this.host,
      port: port ?? this.port,
      apiHost: apiHost ?? this.apiHost,
      siteId: siteId ?? this.siteId,
      merId: merId ?? this.merId,
      webName: webName ?? this.webName,
      zxkfUrl: zxkfUrl ?? this.zxkfUrl,
      allowreg: allowreg ?? this.allowreg,
      closeregreason: closeregreason ?? this.closeregreason,
      agentMApply: agentMApply ?? this.agentMApply,
      pageTitle: pageTitle ?? this.pageTitle,
      loginTo: loginTo ?? this.loginTo,
      memberAgent: memberAgent ?? this.memberAgent,
      mPromotePos: mPromotePos ?? this.mPromotePos,
      agentRegbutton: agentRegbutton ?? this.agentRegbutton,
      passLengthMin: passLengthMin ?? this.passLengthMin,
      passLengthMax: passLengthMax ?? this.passLengthMax,
      passLimit: passLimit ?? this.passLimit,
      myrecoImg: myrecoImg ?? this.myrecoImg,
      missionName: missionName ?? this.missionName,
      hideReco: hideReco ?? this.hideReco,
      regName: regName ?? this.regName,
      regFundpwd: regFundpwd ?? this.regFundpwd,
      regQq: regQq ?? this.regQq,
      regWx: regWx ?? this.regWx,
      regPhone: regPhone ?? this.regPhone,
      regFb: regFb ?? this.regFb,
      regZalo: regZalo ?? this.regZalo,
      smsVerify: smsVerify ?? this.smsVerify,
      regVcode: regVcode ?? this.regVcode,
      regEmail: regEmail ?? this.regEmail,
      betBankCard: betBankCard ?? this.betBankCard,
      regLine: regLine ?? this.regLine,
      googleVerifier: googleVerifier ?? this.googleVerifier,
      znxNotify: znxNotify ?? this.znxNotify,
      regAndLogin: regAndLogin ?? this.regAndLogin,
      smsFindPassword: smsFindPassword ?? this.smsFindPassword,
      userLengthLimitSwitch:
          userLengthLimitSwitch ?? this.userLengthLimitSwitch,
      userLengthMin: userLengthMin ?? this.userLengthMin,
      userLengthMax: userLengthMax ?? this.userLengthMax,
      oftenLoginArea: oftenLoginArea ?? this.oftenLoginArea,
      missionSwitch: missionSwitch ?? this.missionSwitch,
      isIntToMoney: isIntToMoney ?? this.isIntToMoney,
      missionBili: missionBili ?? this.missionBili,
      checkinSwitch: checkinSwitch ?? this.checkinSwitch,
      mkCheckinSwitch: mkCheckinSwitch ?? this.mkCheckinSwitch,
      chatappLogo: chatappLogo ?? this.chatappLogo,
      chatappStartLogo: chatappStartLogo ?? this.chatappStartLogo,
      mobileLogo: mobileLogo ?? this.mobileLogo,
      mobileIcon: mobileIcon ?? this.mobileIcon,
      missionPopUpSwitch: missionPopUpSwitch ?? this.missionPopUpSwitch,
      guestSwitch: guestSwitch ?? this.guestSwitch,
      missionLogo: missionLogo ?? this.missionLogo,
      minWithdrawMoney: minWithdrawMoney ?? this.minWithdrawMoney,
      maxWithdrawMoney: maxWithdrawMoney ?? this.maxWithdrawMoney,
      announceFirst: announceFirst ?? this.announceFirst,
      adSliderTimer: adSliderTimer ?? this.adSliderTimer,
      popupTab: popupTab ?? this.popupTab,
      popupAnnounce: popupAnnounce ?? this.popupAnnounce,
      popupHour: popupHour ?? this.popupHour,
      popupType: popupType ?? this.popupType,
      switchDoyusdtTrans: switchDoyusdtTrans ?? this.switchDoyusdtTrans,
      yuebaoName: yuebaoName ?? this.yuebaoName,
      allowMemberCancelBet: allowMemberCancelBet ?? this.allowMemberCancelBet,
      balanceDecimal: balanceDecimal ?? this.balanceDecimal,
      chaseNumber: chaseNumber ?? this.chaseNumber,
      selectNumber: selectNumber ?? this.selectNumber,
      switchBindVerify: switchBindVerify ?? this.switchBindVerify,
      switchShowFriendReferral:
          switchShowFriendReferral ?? this.switchShowFriendReferral,
      showNavigationBar: showNavigationBar ?? this.showNavigationBar,
      rankingListSwitch: rankingListSwitch ?? this.rankingListSwitch,
      grabRedbagRankingListSwitch:
          grabRedbagRankingListSwitch ?? this.grabRedbagRankingListSwitch,
      chatRoomSwitch: chatRoomSwitch ?? this.chatRoomSwitch,
      chatRoomName: chatRoomName ?? this.chatRoomName,
      chatFollowSwitch: chatFollowSwitch ?? this.chatFollowSwitch,
      chatShareBetMinAmount:
          chatShareBetMinAmount ?? this.chatShareBetMinAmount,
      mobileGameHall: mobileGameHall ?? this.mobileGameHall,
      betMin: betMin ?? this.betMin,
      betMax: betMax ?? this.betMax,
      customiseBetMax: customiseBetMax ?? this.customiseBetMax,
      switchBalanceChannel: switchBalanceChannel ?? this.switchBalanceChannel,
      balanceChannelStartTime:
          balanceChannelStartTime ?? this.balanceChannelStartTime,
      balanceChannelEndTime:
          balanceChannelEndTime ?? this.balanceChannelEndTime,
      balanceChannelPrompt: balanceChannelPrompt ?? this.balanceChannelPrompt,
      switchYuebaoChannel: switchYuebaoChannel ?? this.switchYuebaoChannel,
      yuebaoChannelStartTime:
          yuebaoChannelStartTime ?? this.yuebaoChannelStartTime,
      yuebaoChannelEndTime: yuebaoChannelEndTime ?? this.yuebaoChannelEndTime,
      yuebaoChannelPrompt: yuebaoChannelPrompt ?? this.yuebaoChannelPrompt,
      mobileHomeGameTypeSwitch:
          mobileHomeGameTypeSwitch ?? this.mobileHomeGameTypeSwitch,
      picTypeshow: picTypeshow ?? this.picTypeshow,
      apiHosts: apiHosts ?? this.apiHosts,
      curLangCode: curLangCode ?? this.curLangCode,
      allowMemberModifyLanguage:
          allowMemberModifyLanguage ?? this.allowMemberModifyLanguage,
      mobileTemplateCategory:
          mobileTemplateCategory ?? this.mobileTemplateCategory,
      mobileTemplateBackground:
          mobileTemplateBackground ?? this.mobileTemplateBackground,
      mobileTemplateStyle: mobileTemplateStyle ?? this.mobileTemplateStyle,
      mobileViewModel: mobileViewModel ?? this.mobileViewModel,
      mobileBetStyle: mobileBetStyle ?? this.mobileBetStyle,
      mobileTemplateLhcStyle:
          mobileTemplateLhcStyle ?? this.mobileTemplateLhcStyle,
      mobileTemplateGpkStyle:
          mobileTemplateGpkStyle ?? this.mobileTemplateGpkStyle,
      mobileTemplateHbStyle:
          mobileTemplateHbStyle ?? this.mobileTemplateHbStyle,
      mobileTemplateXbjStyle:
          mobileTemplateXbjStyle ?? this.mobileTemplateXbjStyle,
      mobileMenu: mobileMenu ?? this.mobileMenu,
      kjwTemplateSp: kjwTemplateSp ?? this.kjwTemplateSp,
      kjwLotteries: kjwLotteries ?? this.kjwLotteries,
      serviceQq1: serviceQq1 ?? this.serviceQq1,
      serviceQq2: serviceQq2 ?? this.serviceQq2,
      facekfUrl: facekfUrl ?? this.facekfUrl,
      whatskfUrl: whatskfUrl ?? this.whatskfUrl,
      appPopupQqNum: appPopupQqNum ?? this.appPopupQqNum,
      appPopupQqImg: appPopupQqImg ?? this.appPopupQqImg,
      appPopupWechatNum: appPopupWechatNum ?? this.appPopupWechatNum,
      appPopupWechatImg: appPopupWechatImg ?? this.appPopupWechatImg,
      withdrawChannelAd: withdrawChannelAd ?? this.withdrawChannelAd,
      zxkfUrl2: zxkfUrl2 ?? this.zxkfUrl2,
      zalokfUrl: zalokfUrl ?? this.zalokfUrl,
      telegramkfUrl: telegramkfUrl ?? this.telegramkfUrl,
      shortCut: shortCut ?? this.shortCut,
      shortCutIcon: shortCutIcon ?? this.shortCutIcon,
      iosRelease: iosRelease ?? this.iosRelease,
      yuebaoSwitch: yuebaoSwitch ?? this.yuebaoSwitch,
      easyRememberDomain: easyRememberDomain ?? this.easyRememberDomain,
      loginVCode: loginVCode ?? this.loginVCode,
      loginVCodeType: loginVCodeType ?? this.loginVCodeType,
      loginChatAppVcode: loginChatAppVcode ?? this.loginChatAppVcode,
      loginChatAppVcodeType:
          loginChatAppVcodeType ?? this.loginChatAppVcodeType,
      chatRoomServer: chatRoomServer ?? this.chatRoomServer,
      userCenter: userCenter ?? this.userCenter,
      lhcdocSwitchMobileHome:
          lhcdocSwitchMobileHome ?? this.lhcdocSwitchMobileHome,
      userCenterCategoryList:
          userCenterCategoryList ?? this.userCenterCategoryList,
      lhcdocMiCard: lhcdocMiCard ?? this.lhcdocMiCard,
      lhcPriceList: lhcPriceList ?? this.lhcPriceList,
      lhcdocIsShowNav: lhcdocIsShowNav ?? this.lhcdocIsShowNav,
      lhcdocUserContCheck: lhcdocUserContCheck ?? this.lhcdocUserContCheck,
      lhcdocUserContRepCheck:
          lhcdocUserContRepCheck ?? this.lhcdocUserContRepCheck,
      qdSwitchDe: qdSwitchDe ?? this.qdSwitchDe,
      qdSwitchWi: qdSwitchWi ?? this.qdSwitchWi,
      qdSwitch: qdSwitch ?? this.qdSwitch,
      appDownloadUrl: appDownloadUrl ?? this.appDownloadUrl,
      appdownloadbar: appdownloadbar ?? this.appdownloadbar,
      domainBindAgentId: domainBindAgentId ?? this.domainBindAgentId,
      proxyDomainDefault: proxyDomainDefault ?? this.proxyDomainDefault,
      chatLink: chatLink ?? this.chatLink,
      switchAgentRecharge: switchAgentRecharge ?? this.switchAgentRecharge,
      nameAgentRecharge: nameAgentRecharge ?? this.nameAgentRecharge,
      switchShowActivityCategory:
          switchShowActivityCategory ?? this.switchShowActivityCategory,
      betAmountIsDecimal: betAmountIsDecimal ?? this.betAmountIsDecimal,
      activeReturnCoinStatus:
          activeReturnCoinStatus ?? this.activeReturnCoinStatus,
      activeReturnCoinRatio:
          activeReturnCoinRatio ?? this.activeReturnCoinRatio,
      autoTransferLimit: autoTransferLimit ?? this.autoTransferLimit,
      switchAutoTransfer: switchAutoTransfer ?? this.switchAutoTransfer,
      currency: currency ?? this.currency,
      switchToK: switchToK ?? this.switchToK,
      appSelectType: appSelectType ?? this.appSelectType,
      regLoginBgSw: regLoginBgSw ?? this.regLoginBgSw,
      regLoginBg: regLoginBg ?? this.regLoginBg,
      oauth: oauth ?? this.oauth,
      loginNotice: loginNotice ?? this.loginNotice,
      inviteCode: inviteCode ?? this.inviteCode,
      inviteWord: inviteWord ?? this.inviteWord,
      inviteCodeSwitch: inviteCodeSwitch ?? this.inviteCodeSwitch,
      regBindCardSwitch: regBindCardSwitch ?? this.regBindCardSwitch,
      frontendAgentAddMember:
          frontendAgentAddMember ?? this.frontendAgentAddMember,
      regNameSwitch: regNameSwitch ?? this.regNameSwitch,
      switchAgentRechargeDml:
          switchAgentRechargeDml ?? this.switchAgentRechargeDml,
      isAgentRechargeNeededPwd:
          isAgentRechargeNeededPwd ?? this.isAgentRechargeNeededPwd,
      switchCoinPwd: switchCoinPwd ?? this.switchCoinPwd,
      switchCoinPwdSms: switchCoinPwdSms ?? this.switchCoinPwdSms,
      coinPwdAuditOptionAry:
          coinPwdAuditOptionAry ?? this.coinPwdAuditOptionAry,
      mBonsSwitch: mBonsSwitch ?? this.mBonsSwitch,
      addEditBankSw: addEditBankSw ?? this.addEditBankSw,
      addEditAlipaySw: addEditAlipaySw ?? this.addEditAlipaySw,
      addEditWechatSw: addEditWechatSw ?? this.addEditWechatSw,
      addEditVcoinSw: addEditVcoinSw ?? this.addEditVcoinSw,
      addEditKdouSw: addEditKdouSw ?? this.addEditKdouSw,
      rechargeImgSw: rechargeImgSw ?? this.rechargeImgSw,
      staticServers: staticServers ?? this.staticServers,
      loginsessid: loginsessid ?? this.loginsessid,
    );
  }
}
