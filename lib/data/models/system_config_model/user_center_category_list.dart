import 'package:json_annotation/json_annotation.dart';

part 'user_center_category_list.g.dart';

@JsonSerializable()
class UserCenterCategoryList {
  final int? id;
  final String? name;

  const UserCenterCategoryList({this.id, this.name});

  @override
  String toString() => 'UserCenterCategoryList(id: $id, name: $name)';

  factory UserCenterCategoryList.fromJson(Map<String, dynamic> json) {
    return _$UserCenterCategoryListFromJson(json);
  }

  Map<String, dynamic> toJson() => _$UserCenterCategoryListToJson(this);

  UserCenterCategoryList copyWith({
    int? id,
    String? name,
  }) {
    return UserCenterCategoryList(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }
}
