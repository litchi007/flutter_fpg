import 'package:json_annotation/json_annotation.dart';

part 'user_center.g.dart';

@JsonSerializable()
class UserCenter {
  final String? id;
  @JsonKey(name: 'parent_id')
  final String? parentId;
  final String? category;
  final String? code;
  final String? name;
  final String? status;
  final String? sorts;
  @JsonKey(name: 'site_ids')
  final String? siteIds;
  @JsonKey(name: 'site_id')
  final String? siteId;
  @JsonKey(name: 'show_list_style')
  final String? showListStyle;
  @JsonKey(name: 'link_url')
  final String? linkUrl;
  @JsonKey(name: 'user_center_category')
  final String? userCenterCategory;
  @JsonKey(name: 'source_id')
  final String? sourceId;
  @JsonKey(name: 'image_hot')
  final String? imageHot;
  final String? logo;

  const UserCenter({
    this.id,
    this.parentId,
    this.category,
    this.code,
    this.name,
    this.status,
    this.sorts,
    this.siteIds,
    this.siteId,
    this.showListStyle,
    this.linkUrl,
    this.userCenterCategory,
    this.sourceId,
    this.imageHot,
    this.logo,
  });

  @override
  String toString() {
    return 'UserCenter(id: $id, parentId: $parentId, category: $category, code: $code, name: $name, status: $status, sorts: $sorts, siteIds: $siteIds, siteId: $siteId, showListStyle: $showListStyle, linkUrl: $linkUrl, userCenterCategory: $userCenterCategory, sourceId: $sourceId, imageHot: $imageHot, logo: $logo)';
  }

  factory UserCenter.fromJson(Map<String, dynamic> json) {
    return _$UserCenterFromJson(json);
  }

  Map<String, dynamic> toJson() => _$UserCenterToJson(this);

  UserCenter copyWith({
    String? id,
    String? parentId,
    String? category,
    String? code,
    String? name,
    String? status,
    String? sorts,
    String? siteIds,
    String? siteId,
    String? showListStyle,
    String? linkUrl,
    String? userCenterCategory,
    String? sourceId,
    String? imageHot,
    String? logo,
  }) {
    return UserCenter(
      id: id ?? this.id,
      parentId: parentId ?? this.parentId,
      category: category ?? this.category,
      code: code ?? this.code,
      name: name ?? this.name,
      status: status ?? this.status,
      sorts: sorts ?? this.sorts,
      siteIds: siteIds ?? this.siteIds,
      siteId: siteId ?? this.siteId,
      showListStyle: showListStyle ?? this.showListStyle,
      linkUrl: linkUrl ?? this.linkUrl,
      userCenterCategory: userCenterCategory ?? this.userCenterCategory,
      sourceId: sourceId ?? this.sourceId,
      imageHot: imageHot ?? this.imageHot,
      logo: logo ?? this.logo,
    );
  }
}
