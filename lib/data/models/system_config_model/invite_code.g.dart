// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invite_code.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InviteCode _$InviteCodeFromJson(Map<String, dynamic> json) => InviteCode(
      switch_: json['switch'] as String?,
      displayWord: json['displayWord'] as String?,
      canGenNum: json['canGenNum'] as String?,
      canUseNum: json['canUseNum'] as String?,
      randomSwitch: json['randomSwitch'] as String?,
      randomLength: json['randomLength'] as String?,
      noticeSwitch: json['noticeSwitch'] as String?,
      noticeText: json['noticeText'] as String?,
    );

Map<String, dynamic> _$InviteCodeToJson(InviteCode instance) =>
    <String, dynamic>{
      'switch': instance.switch_,
      'displayWord': instance.displayWord,
      'canGenNum': instance.canGenNum,
      'canUseNum': instance.canUseNum,
      'randomSwitch': instance.randomSwitch,
      'randomLength': instance.randomLength,
      'noticeSwitch': instance.noticeSwitch,
      'noticeText': instance.noticeText,
    };
