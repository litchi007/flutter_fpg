// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lhc_price_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LhcPriceList _$LhcPriceListFromJson(Map<String, dynamic> json) => LhcPriceList(
      id: json['id'] as String?,
      alias: json['alias'] as String?,
      priceMin: json['priceMin'] as String?,
      priceMax: json['priceMax'] as String?,
    );

Map<String, dynamic> _$LhcPriceListToJson(LhcPriceList instance) =>
    <String, dynamic>{
      'id': instance.id,
      'alias': instance.alias,
      'priceMin': instance.priceMin,
      'priceMax': instance.priceMax,
    };
