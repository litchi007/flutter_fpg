// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_notice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginNotice _$LoginNoticeFromJson(Map<String, dynamic> json) => LoginNotice(
      switch_: json['switch'] as String?,
      text: json['text'] as String?,
    );

Map<String, dynamic> _$LoginNoticeToJson(LoginNotice instance) =>
    <String, dynamic>{
      'switch': instance.switch_,
      'text': instance.text,
    };
