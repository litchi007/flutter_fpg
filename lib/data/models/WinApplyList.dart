class WinApplyList {
  List<ListDataForApplyList>? list;

  WinApplyList({
    this.list,
  });

  factory WinApplyList.fromJson(Map<String, dynamic> json) {
    var lists = json['list'] as List?;
    List<ListDataForApplyList> parsedList =
        lists?.map((i) => ListDataForApplyList.fromJson(i)).toList() ?? [];

    return WinApplyList(
      list: parsedList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((e) => e.toJson()).toList(),
    };
  }
}

class Param {
  String? joinCount;
  String? joinType;
  String? getDay;
  String? isApplyReason;
  String? isSMS;
  String? isReview;
  String? ipLimitU;
  String? winApplyContent;
  String? timeInterval;
  String? rechargeAmount;
  String? dmlAmount;
  String? minWinAmount;
  String? maxWinAmount;
  String? winSort;
  int? showWinAmount;
  String? quickAmount1;
  String? quickAmount2;
  String? quickAmount3;
  String? quickAmount4;
  String? quickAmount5;
  String? quickAmount6;
  String? quickAmount7;
  String? quickAmount8;
  String? quickAmount9;
  String? quickAmount10;
  String? quickAmount11;
  String? quickAmount12;
  String? quickAmount13;
  String? quickAmount14;
  String? quickAmount15;
  String? keyPankou2;
  String? minBetDay;
  String? isEach;
  String? checkInLotteryGames;
  String? dailyBetAmount;
  String? singleBonus;
  String? checkInUserLevels;
  String? isAutoCheck;
  String? wacType;
  String? lotteryType;
  String? realType;
  String? playedGroup;
  String? allItem;
  String? wacCondition1;
  String? wacGive1;
  String? receiveNum1;
  String? wacCondition2;
  String? wacGive2;
  String? receiveNum2;
  String? winApplyImage;

  Param({
    this.joinCount,
    this.joinType,
    this.getDay,
    this.isApplyReason,
    this.isSMS,
    this.isReview,
    this.ipLimitU,
    this.winApplyContent,
    this.timeInterval,
    this.rechargeAmount,
    this.dmlAmount,
    this.minWinAmount,
    this.maxWinAmount,
    this.winSort,
    this.showWinAmount,
    this.quickAmount1,
    this.quickAmount2,
    this.quickAmount3,
    this.quickAmount4,
    this.quickAmount5,
    this.quickAmount6,
    this.quickAmount7,
    this.quickAmount8,
    this.quickAmount9,
    this.quickAmount10,
    this.quickAmount11,
    this.quickAmount12,
    this.quickAmount13,
    this.quickAmount14,
    this.quickAmount15,
    this.keyPankou2,
    this.minBetDay,
    this.isEach,
    this.checkInLotteryGames,
    this.dailyBetAmount,
    this.singleBonus,
    this.checkInUserLevels,
    this.isAutoCheck,
    this.wacType,
    this.lotteryType,
    this.realType,
    this.playedGroup,
    this.allItem,
    this.wacCondition1,
    this.wacGive1,
    this.receiveNum1,
    this.wacCondition2,
    this.wacGive2,
    this.receiveNum2,
    this.winApplyImage,
  });

  factory Param.fromJson(Map<String, dynamic> json) {
    return Param(
      joinCount: json['joinCount']?.toString(),
      joinType: json['joinType']?.toString(),
      getDay: json['getDay']?.toString(),
      isApplyReason: json['isApplyReason']?.toString(),
      isSMS: json['isSMS']?.toString(),
      isReview: json['isReview']?.toString(),
      ipLimitU: json['ip_limit_u']?.toString(),
      winApplyContent: json['win_apply_content']?.toString(),
      timeInterval: json['timeInterval']?.toString(),
      rechargeAmount: json['rechargeAmount']?.toString(),
      dmlAmount: json['dmlAmount']?.toString(),
      minWinAmount: json['minWinAmount']?.toString(),
      maxWinAmount: json['maxWinAmount']?.toString(),
      winSort: json['winSort']?.toString(),
      showWinAmount: json['showWinAmount'] as int?,
      quickAmount1: json['quickAmount1']?.toString(),
      quickAmount2: json['quickAmount2']?.toString(),
      quickAmount3: json['quickAmount3']?.toString(),
      quickAmount4: json['quickAmount4']?.toString(),
      quickAmount5: json['quickAmount5']?.toString(),
      quickAmount6: json['quickAmount6']?.toString(),
      quickAmount7: json['quickAmount7']?.toString(),
      quickAmount8: json['quickAmount8']?.toString(),
      quickAmount9: json['quickAmount9']?.toString(),
      quickAmount10: json['quickAmount10']?.toString(),
      quickAmount11: json['quickAmount11']?.toString(),
      quickAmount12: json['quickAmount12']?.toString(),
      quickAmount13: json['quickAmount13']?.toString(),
      quickAmount14: json['quickAmount14']?.toString(),
      quickAmount15: json['quickAmount15']?.toString(),
      keyPankou2: json['keyPankou2']?.toString(),
      minBetDay: json['min_bet_day']?.toString(),
      isEach: json['isEach']?.toString(),
      checkInLotteryGames: json['check_in_lottery_games']?.toString(),
      dailyBetAmount: json['daily_bet_amount']?.toString(),
      singleBonus: json['single_bonus']?.toString(),
      checkInUserLevels: json['check_in_user_levels']?.toString(),
      isAutoCheck: json['is_auto_check']?.toString(),
      wacType: json['wacType']?.toString(),
      lotteryType: json['lotteryType']?.toString(),
      realType: json['realType']?.toString(),
      playedGroup: json['playedGroup'].toString(),
      allItem: json['allItem'].toString(),
      wacCondition1: json['wacCondition1']?.toString(),
      wacGive1: json['wacGive1']?.toString(),
      receiveNum1: json['receiveNum1']?.toString(),
      wacCondition2: json['wacCondition2']?.toString(),
      wacGive2: json['wacGive2']?.toString(),
      receiveNum2: json['receiveNum2']?.toString(),
      winApplyImage: json['win_apply_image']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'joinCount': joinCount,
      'joinType': joinType,
      'getDay': getDay,
      'isApplyReason': isApplyReason,
      'isSMS': isSMS,
      'isReview': isReview,
      'ip_limit_u': ipLimitU,
      'win_apply_content': winApplyContent,
      'timeInterval': timeInterval,
      'rechargeAmount': rechargeAmount,
      'dmlAmount': dmlAmount,
      'minWinAmount': minWinAmount,
      'maxWinAmount': maxWinAmount,
      'winSort': winSort,
      'showWinAmount': showWinAmount,
      'quickAmount1': quickAmount1,
      'quickAmount2': quickAmount2,
      'quickAmount3': quickAmount3,
      'quickAmount4': quickAmount4,
      'quickAmount5': quickAmount5,
      'quickAmount6': quickAmount6,
      'quickAmount7': quickAmount7,
      'quickAmount8': quickAmount8,
      'quickAmount9': quickAmount9,
      'quickAmount10': quickAmount10,
      'quickAmount11': quickAmount11,
      'quickAmount12': quickAmount12,
      'quickAmount13': quickAmount13,
      'quickAmount14': quickAmount14,
      'quickAmount15': quickAmount15,
      'keyPankou2': keyPankou2,
      'min_bet_day': minBetDay,
      'isEach': isEach,
      'check_in_lottery_games': checkInLotteryGames,
      'daily_bet_amount': dailyBetAmount,
      'single_bonus': singleBonus,
      'check_in_user_levels': checkInUserLevels,
      'is_auto_check': isAutoCheck,
      'wacType': wacType,
      'lotteryType': lotteryType,
      'realType': realType,
      'playedGroup': playedGroup,
      'allItem': allItem,
      'wacCondition1': wacCondition1,
      'wacGive1': wacGive1,
      'receiveNum1': receiveNum1,
      'wacCondition2': wacCondition2,
      'wacGive2': wacGive2,
      'receiveNum2': receiveNum2,
      'win_apply_image': winApplyImage,
    };
  }
}

class ListDataForApplyList {
  String? id;
  String? enable;
  String? type;
  String? name;
  String? start;
  String? end;
  String? showTime;
  Param? param;
  String? category;
  String? turntableSkin;
  String? winSort;
  String? categoryName;

  ListDataForApplyList({
    this.id,
    this.enable,
    this.type,
    this.name,
    this.start,
    this.end,
    this.showTime,
    this.param,
    this.category,
    this.turntableSkin,
    this.winSort,
    this.categoryName,
  });

  factory ListDataForApplyList.fromJson(Map<String, dynamic> json) {
    return ListDataForApplyList(
      id: json['id']?.toString(),
      enable: json['enable']?.toString(),
      type: json['type']?.toString(),
      name: json['name']?.toString(),
      start: json['start']?.toString(),
      end: json['end']?.toString(),
      showTime: json['show_time']?.toString(),
      param: json['param'] != null
          ? Param.fromJson(json['param'] as Map<String, dynamic>)
          : null,
      category: json['category']?.toString(),
      turntableSkin: json['turntable_skin']?.toString(),
      winSort: json['winSort']?.toString(),
      categoryName: json['categoryName']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'enable': enable,
      'type': type,
      'name': name,
      'start': start,
      'end': end,
      'show_time': showTime,
      'param': param?.toJson(),
      'category': category,
      'turntable_skin': turntableSkin,
      'winSort': winSort,
      'categoryName': categoryName,
    };
  }
}
