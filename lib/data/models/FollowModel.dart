import 'package:fpg_flutter/data/models/FollowListModel.dart';

class FollowModel {
  int? total;
  List<FollowListModel>? list;

  FollowModel({
    this.total,
    this.list,
  });

  factory FollowModel.fromJson(Map<String, dynamic> json) {
    return FollowModel(
      total: json['total'],
      list: (json['list'] as List?)
          ?.map((i) => FollowListModel.fromJson(i))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'total': total,
      'list': list?.map((e) => e.toJson()).toList(),
    };
  }
}
