class CapitalDetailModel {
  String? time;
  String? changeMoney;
  String? balance;
  String? category;

  CapitalDetailModel({
    this.time,
    this.changeMoney,
    this.balance,
    this.category,
  });

  factory CapitalDetailModel.fromJson(Map<String, dynamic> json) {
    return CapitalDetailModel(
      time: json['time'],
      changeMoney: json['changeMoney'],
      balance: json['balance'],
      category: json['category'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['time'] = time;
    data['changeMoney'] = changeMoney;
    data['balance'] = balance;
    data['category'] = category;
    return data;
  }
}

class CapitalGroupData {
  int? id;
  String? name;
  CapitalGroupData({this.id, this.name});
  factory CapitalGroupData.fromJson(Map<String, dynamic> json) {
    return CapitalGroupData(id: json['id'], name: json['name']);
  }
  Map<String, dynamic> toJson() {
    return {'id': id, 'name': name};
  }
}

class CapitalDetailListModel {
  List<CapitalDetailModel>? list;
  int? total;
  List<CapitalGroupData>? groups;
  String? sumCoin;
  CapitalDetailListModel({
    this.list,
    this.sumCoin,
    this.total,
    this.groups,
  });

  factory CapitalDetailListModel.fromJson(Map<String, dynamic> json) {
    return CapitalDetailListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((item) => CapitalDetailModel.fromJson(item))
          .toList(),
      total: json['total'],
      sumCoin: json['sumCoin'],
      groups: (json['groups'] as List<dynamic>?)
          ?.map((item) => CapitalGroupData.fromJson(item))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((item) => item.toJson()).toList(),
      'total': total,
      'sumCoin': sumCoin,
      'groups': groups?.map((item) => item.toJson()).toList(),
    };
  }
}
