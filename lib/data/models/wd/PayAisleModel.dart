class BankList {
  String name;
  String code;

  BankList({required this.name, required this.code});

  factory BankList.fromJson(Map<String, dynamic>? json) {
    if (json == null) return BankList(name: '', code: '');
    return BankList(
      name: json['name'] ?? '',
      code: json['code'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'code': code,
    };
  }
}

class Para {
  String? fixedAmount;
  List<BankList>? bankList;

  Para({this.fixedAmount, this.bankList});

  factory Para.fromJson(Map<String, dynamic>? json) {
    if (json == null) return Para();
    return Para(
      fixedAmount: json['fixedAmount'],
      bankList: (json['bankList'] as List<dynamic>?)
          ?.map((e) => BankList.fromJson(e))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'fixedAmount': fixedAmount,
      'bankList': bankList?.map((e) => e.toJson()).toList(),
    };
  }
}

class PayChannelBean {
  String? branchAddress;
  String? limitAmount;
  String? fcomment;
  String? account;
  String? thirdPay;
  String? phoneDevice;
  String? maximum;
  Para? para;
  String? endTime;
  String? onlineType;
  String? name;
  String? rechType;
  String? domain;
  String? id;
  String? mininum;
  String? payeeName;
  String? currencyRate;
  String? pid;
  String? startTime;
  String? comment;
  String? address;
  String? limitNum;

  PayChannelBean({
    this.branchAddress,
    this.limitAmount,
    this.fcomment,
    this.account,
    this.thirdPay,
    this.phoneDevice,
    this.maximum,
    this.para,
    this.endTime,
    this.onlineType,
    this.name,
    this.rechType,
    this.domain,
    this.id,
    this.mininum,
    this.payeeName,
    this.currencyRate,
    this.pid,
    this.startTime,
    this.comment,
    this.address,
    this.limitNum,
  });

  factory PayChannelBean.fromJson(Map<String, dynamic>? json) {
    if (json == null) return PayChannelBean();
    return PayChannelBean(
      branchAddress: json['branchAddress'],
      limitAmount: json['limitAmount'],
      fcomment: json['fcomment'],
      account: json['account'],
      thirdPay: json['thirdPay'],
      phoneDevice: json['phoneDevice'],
      maximum: json['maximum'],
      para: Para.fromJson(json['para']),
      endTime: json['endTime'],
      onlineType: json['onlineType'],
      name: json['name'],
      rechType: json['rechType'],
      domain: json['domain'],
      id: json['id'],
      mininum: json['mininum'],
      payeeName: json['payeeName'],
      currencyRate: json['currencyRate']?.toString(),
      pid: json['pid'],
      startTime: json['startTime'],
      comment: json['comment'],
      address: json['address'],
      limitNum: json['limitNum'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'branchAddress': branchAddress,
      'limitAmount': limitAmount,
      'fcomment': fcomment,
      'account': account,
      'thirdPay': thirdPay,
      'phoneDevice': phoneDevice,
      'maximum': maximum,
      'para': para?.toJson(),
      'endTime': endTime,
      'onlineType': onlineType,
      'name': name,
      'rechType': rechType,
      'domain': domain,
      'id': id,
      'mininum': mininum,
      'payeeName': payeeName,
      'currencyRate': currencyRate,
      'pid': pid,
      'startTime': startTime,
      'comment': comment,
      'address': address,
      'limitNum': limitNum,
    };
  }
}

class PayAisleData {
  List<PayAisleListData>? payment;
  List<String>? quickAmount;
  String? depositPrompt;
  String? transferPrompt;
  String? rechargePopUpAlarmSwitch;
  String? rechargePopUpAlarmMsg;

  PayAisleData({
    this.payment,
    this.quickAmount,
    this.depositPrompt,
    this.transferPrompt,
    this.rechargePopUpAlarmSwitch,
    this.rechargePopUpAlarmMsg,
  });

  factory PayAisleData.fromJson(Map<String, dynamic>? json) {
    if (json == null) return PayAisleData();
    return PayAisleData(
      payment: (json['payment'] as List<dynamic>?)
          ?.map((e) => PayAisleListData.fromJson(e))
          .toList(),
      quickAmount: (json['quickAmount'] as List<dynamic>?)
          ?.map((e) => e.toString())
          .toList(),
      depositPrompt: json['depositPrompt'],
      transferPrompt: json['transferPrompt'],
      rechargePopUpAlarmSwitch: json['rechargePopUpAlarmSwitch'],
      rechargePopUpAlarmMsg: json['rechargePopUpAlarmMsg'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'payment': payment?.map((e) => e.toJson()).toList(),
      'quickAmount': quickAmount,
      'depositPrompt': depositPrompt,
      'transferPrompt': transferPrompt,
      'rechargePopUpAlarmSwitch': rechargePopUpAlarmSwitch,
      'rechargePopUpAlarmMsg': rechargePopUpAlarmMsg,
    };
  }
}

class PayAisleListData {
  PayItemType? id;
  String? idString;
  String? code;
  String? name;
  String? sort;
  String? prompt;
  String? tip;
  String? promptPc;
  String? copyPrompt;
  String? bankSortIcon;
  String? type;
  String? rechargeAlarm;
  String? bankPayPrompt;
  List<PayChannelBean>? channel;
  List<PayChannelBean>? channel2;
  String? quickAmount;
  List<RechargeTutoBean>? rechargeTutorials;

  PayAisleListData({
    this.id,
    this.idString,
    this.code,
    this.name,
    this.sort,
    this.prompt,
    this.tip,
    this.promptPc,
    this.copyPrompt,
    this.bankSortIcon,
    this.type,
    this.rechargeAlarm,
    this.bankPayPrompt,
    this.channel,
    this.channel2,
    this.quickAmount,
    this.rechargeTutorials,
  });

  factory PayAisleListData.fromJson(Map<String, dynamic>? json) {
    if (json == null) return PayAisleListData();
    String? idString = json['id'];

    PayItemType? itemType = payItemTypeMap[idString];
    return PayAisleListData(
      id: itemType,
      idString: idString,
      code: json['code'],
      name: json['name'],
      sort: json['sort'],
      prompt: json['prompt'],
      tip: json['tip'],
      promptPc: json['prompt_pc'],
      copyPrompt: json['copy_prompt'],
      bankSortIcon: json['bank_sort_icon'],
      type: json['type'],
      rechargeAlarm: json['recharge_alarm'],
      bankPayPrompt: json['bankPayPrompt'],
      channel: (json['channel'] as List<dynamic>?)
          ?.map((e) => PayChannelBean.fromJson(e))
          .toList(),
      channel2: (json['channel2'] as List<dynamic>?)
          ?.map((e) => PayChannelBean.fromJson(e))
          .toList(),
      quickAmount: json['quickAmount'],
      rechargeTutorials: (json['recharge_tutorials'] as List<dynamic>?)
          ?.map((e) => RechargeTutoBean.fromJson(e))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id != null ? _getStringValueForItemType(id!) : null,
      'code': code,
      'name': name,
      'sort': sort,
      'prompt': prompt,
      'tip': tip,
      'prompt_pc': promptPc,
      'copy_prompt': copyPrompt,
      'bank_sort_icon': bankSortIcon,
      'type': type,
      'recharge_alarm': rechargeAlarm,
      'bankPayPrompt': bankPayPrompt,
      'channel': channel?.map((e) => e.toJson()).toList(),
      'channel2': channel2?.map((e) => e.toJson()).toList(),
      'quickAmount': quickAmount,
      'recharge_tutorials': rechargeTutorials?.map((e) => e.toJson()).toList(),
    };
  }

  String _getStringValueForItemType(PayItemType itemType) {
    // Reverse mapping to get string value from enum
    return payItemTypeMap.entries
        .firstWhere(
          (entry) => entry.value == itemType,
          orElse: () => MapEntry('', itemType), // Default case
        )
        .key;
  }
}

class RechargeTutoBean {
  String? id;
  String? code;
  String? name;
  String? linkUrl;
  String? videoUrl;
  String? sort; //存款提示语是否打开
  String? enable; //存款提示语

  RechargeTutoBean({
    this.id,
    this.code,
    this.name,
    this.linkUrl,
    this.videoUrl,
    this.sort,
    this.enable,
  });

  factory RechargeTutoBean.fromJson(Map<String, dynamic>? json) {
    if (json == null) return RechargeTutoBean();
    return RechargeTutoBean(
      id: json['id'],
      code: json['code'],
      name: json['name'],
      linkUrl: json['linkUrl'],
      videoUrl: json['videoUrl'],
      sort: json['sort'],
      enable: json['enable'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'code': code,
      'name': name,
      'linkUrl': linkUrl,
      'videoUrl': videoUrl,
      'sort': sort,
      'enable': enable,
    };
  }
}

enum PayItemType {
  ez_recharge,
  alipay_online,
  wechat_online,
  tenpay_online,
  aliyin_transfer,
  bank_online,
  yinlian_online,
  quick_online,
  huobi_online,
  xnb_online,
  jingdong_online,
  alipayenvelope_transfer,
  bank_transfer,
  upi_transfer,
  alipay_transfer,
  yxsm_transfer,
  momo_transfer,
  yunshanfu_transfer,
  wxzsm_transfer,
  wxsm_transfer,
  ysf_transfer,
  tenpay_transfer,
  wechat_transfer,
  qqpay_transfer,
  wechat_alipay_transfer,
  alihb_online,
  jdzz_transfer,
  ddhb_transfer,
  dshb_transfer,
  xlsm_transfer,
  zfbzyhk_transfer,
  zfbzk_transfer,
  bankalipay_transfer,
  wxzfbsm_transfer,
  liaobei_transfer,
  xnb_transfer,
  gopay_online,
  okp_online,
  upay_online,
  qd_online,
  viettelpay_transfer,
  siyu_transfer,
  momopay_online,
  zalo_online,
  topay_online,
  qq_online,
  viettelpay_online,
  abpay_online,
  jiujiubapay_online,
  kdou_online,
  douyin_online,
  ecny_online,
  pending_order,
  eden,
  vippay_online,
  tgpay_online,
  agpay_online,
  cbrpay_online,
  obpay_online,
  bobipay_online,
  mpay_online,
  edan_online,
  guantianshouyintai_online,
  embedwallet_online,
  miaofupay_online,
  cgpay_online,
  ebpay_online,
  fpay_online,
  didishoyintai_online,
  goubao_online,
  wanbipay_online,
  kbao_online,
  jdpay_online,
  balingba_online,
  qiannengpay_online,
  truemoney_transfer
}

Map<String, PayItemType> payItemTypeMap = {
  'ez_recharge': PayItemType.ez_recharge,
  'alipay_online': PayItemType.alipay_online,
  'wechat_online': PayItemType.wechat_online,
  'tenpay_online': PayItemType.tenpay_online,
  'aliyin_transfer': PayItemType.aliyin_transfer,
  'bank_online': PayItemType.bank_online,
  'yinlian_online': PayItemType.yinlian_online,
  'quick_online': PayItemType.quick_online,
  'huobi_online': PayItemType.huobi_online,
  'xnb_online': PayItemType.xnb_online,
  'jingdong_online': PayItemType.jingdong_online,
  'alipayenvelope_transfer': PayItemType.alipayenvelope_transfer,
  'bank_transfer': PayItemType.bank_transfer,
  'upi_transfer': PayItemType.upi_transfer,
  'alipay_transfer': PayItemType.alipay_transfer,
  'yxsm_transfer': PayItemType.yxsm_transfer,
  'momo_transfer': PayItemType.momo_transfer,
  'yunshanfu_transfer': PayItemType.yunshanfu_transfer,
  'wxzsm_transfer': PayItemType.wxzsm_transfer,
  'wxsm_transfer': PayItemType.wxsm_transfer,
  'ysf_transfer': PayItemType.ysf_transfer,
  'tenpay_transfer': PayItemType.tenpay_transfer,
  'wechat_transfer': PayItemType.wechat_transfer,
  'qqpay_transfer': PayItemType.qqpay_transfer,
  'wechat_alipay_transfer': PayItemType.wechat_alipay_transfer,
  'alihb_online': PayItemType.alihb_online,
  'jdzz_transfer': PayItemType.jdzz_transfer,
  'ddhb_transfer': PayItemType.ddhb_transfer,
  'dshb_transfer': PayItemType.dshb_transfer,
  'xlsm_transfer': PayItemType.xlsm_transfer,
  'zfbzyhk_transfer': PayItemType.zfbzyhk_transfer,
  'zfbzk_transfer': PayItemType.zfbzk_transfer,
  'bankalipay_transfer': PayItemType.bankalipay_transfer,
  'wxzfbsm_transfer': PayItemType.wxzfbsm_transfer,
  'liaobei_transfer': PayItemType.liaobei_transfer,
  'xnb_transfer': PayItemType.xnb_transfer,
  'gopay_online': PayItemType.gopay_online,
  'okp_online': PayItemType.okp_online,
  'upay_online': PayItemType.upay_online,
  'qd_online': PayItemType.qd_online,
  'viettelpay_transfer': PayItemType.viettelpay_transfer,
  'siyu_transfer': PayItemType.siyu_transfer,
  'momopay_online': PayItemType.momopay_online,
  'zalo_online': PayItemType.zalo_online,
  'topay_online': PayItemType.topay_online,
  'qq_online': PayItemType.qq_online,
  'viettelpay_online': PayItemType.viettelpay_online,
  'abpay_online': PayItemType.abpay_online,
  'jiujiubapay_online': PayItemType.jiujiubapay_online,
  'kdou_online': PayItemType.kdou_online,
  'douyin_online': PayItemType.douyin_online,
  'ecny_online': PayItemType.ecny_online,
  'pending_order': PayItemType.pending_order,
  'eden': PayItemType.eden,
  'vippay_online': PayItemType.vippay_online,
  'tgpay_online': PayItemType.tgpay_online,
  'agpay_online': PayItemType.agpay_online,
  'cbrpay_online': PayItemType.cbrpay_online,
  'obpay_online': PayItemType.obpay_online,
  'bobipay_online': PayItemType.bobipay_online,
  'mpay_online': PayItemType.mpay_online,
  'edan_online': PayItemType.edan_online,
  'guantianshouyintai_online': PayItemType.guantianshouyintai_online,
  'embedwallet_online': PayItemType.embedwallet_online,
  'miaofupay_online': PayItemType.miaofupay_online,
  'cgpay_online': PayItemType.cgpay_online,
  'ebpay_online': PayItemType.ebpay_online,
  'fpay_online': PayItemType.fpay_online,
  'didishoyintai_online': PayItemType.didishoyintai_online,
  'goubao_online': PayItemType.goubao_online,
  'wanbipay_online': PayItemType.wanbipay_online,
  'kbao_online': PayItemType.kbao_online,
  'jdpay_online': PayItemType.jdpay_online,
  'balingba_online': PayItemType.balingba_online,
  'qiannengpay_online': PayItemType.qiannengpay_online,
  'truemoney_transfer': PayItemType.truemoney_transfer,
};
