class DepositRecordModel {
  String? orderNo;
  String? amount;
  String? applyTime;
  String? arriveTime;
  String? status;
  String? category;
  String? remark;
  String? username;
  String? fee;
  List<String>? images;

  DepositRecordModel({
    this.orderNo,
    this.amount,
    this.applyTime,
    this.arriveTime,
    this.status,
    this.category,
    this.remark,
    this.username,
    this.fee,
    this.images,
  });

  factory DepositRecordModel.fromJson(Map<String, dynamic> json) {
    return DepositRecordModel(
      orderNo: json['orderNo'],
      amount: json['amount'],
      applyTime: json['applyTime'],
      arriveTime: json['arriveTime'],
      status: json['status'],
      category: json['category'],
      remark: json['remark'],
      username: json['username'],
      fee: json['fee'],
      images: List<String>.from(json['images']),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['orderNo'] = orderNo;
    data['amount'] = amount;
    data['applyTime'] = applyTime;
    data['arriveTime'] = arriveTime;
    data['status'] = status;
    data['category'] = category;
    data['remark'] = remark;
    data['username'] = username;
    data['fee'] = fee;
    data['images'] = images;
    return data;
  }
}

class DepositRecordListModel {
  List<DepositRecordModel>? list;
  int? total;

  DepositRecordListModel({
    this.list,
    this.total,
  });

  factory DepositRecordListModel.fromJson(Map<String, dynamic> json) {
    return DepositRecordListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((item) => DepositRecordModel.fromJson(item))
          .toList(),
      total: json['total'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((item) => item.toJson()).toList(),
      'total': total,
    };
  }
}
