class WithdrawalRecordModel {
  String? status;
  String? amount;
  String? bankName;
  String? bankCard;
  String? remark;
  String? applyTime;
  String? arriveTime;
  String? orderNo;
  String? fee;
  String? type;
  String? bankAccount;

  WithdrawalRecordModel({
    this.type,
    this.orderNo,
    this.bankName,
    this.bankCard,
    this.bankAccount,
    this.amount,
    this.fee,
    this.status,
    this.applyTime,
    this.remark,
    this.arriveTime,
  });

  factory WithdrawalRecordModel.fromJson(Map<String, dynamic> json) {
    return WithdrawalRecordModel(
      type: json['type'],
      orderNo: json['orderNo'],
      bankName: json['bankName'],
      bankCard: json['bankCard'],
      bankAccount: json['bankAccount'],
      amount: json['amount'],
      fee: json['fee'],
      status: json['status'],
      applyTime: json['applyTime'],
      remark: json['remark'],
      arriveTime: json['arriveTime'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['type'] = type;
    data['orderNo'] = orderNo;
    data['bankName'] = bankName;
    data['bankCard'] = bankCard;
    data['bankAccount'] = bankAccount;
    data['amount'] = amount;
    data['fee'] = fee;
    data['status'] = status;
    data['applyTime'] = applyTime;
    data['remark'] = remark;
    data['arriveTime'] = arriveTime;
    return data;
  }
}

class WithdrawalRecordListModel {
  List<WithdrawalRecordModel>? list;
  int? total;

  WithdrawalRecordListModel({
    this.list,
    this.total,
  });

  factory WithdrawalRecordListModel.fromJson(Map<String, dynamic> json) {
    return WithdrawalRecordListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((item) => WithdrawalRecordModel.fromJson(item))
          .toList(),
      total: json['total'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((item) => item.toJson()).toList(),
      'total': total,
    };
  }
}
