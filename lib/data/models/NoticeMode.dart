class Scroll {
  String? id;
  String? nodeId;
  String? type;
  String? content;
  List<dynamic>? customiseContent; // Assuming customise_content is dynamic
  List<dynamic>? withdrawContent; // Assuming withdraw_content is dynamic
  String? title;
  String? addTime;

  Scroll({
    this.id,
    this.nodeId,
    this.type,
    this.content,
    this.customiseContent,
    this.withdrawContent,
    this.title,
    this.addTime,
  });

  factory Scroll.fromJson(Map<String, dynamic> json) {
    return Scroll(
      id: json['id'].toString(),
      nodeId: json['nodeId'].toString(),
      type: json['type'].toString(),
      content: json['content'].toString(),
      customiseContent: json['customise_content'] ?? [],
      withdrawContent: json['withdraw_content'] ?? [],
      title: json['title'].toString(),
      addTime: json['addTime'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nodeId': nodeId,
      'type': type,
      'content': content,
      'customise_content': customiseContent,
      'withdraw_content': withdrawContent,
      'title': title,
      'addTime': addTime,
    };
  }
}

class HomeNoticeData {
  List<Scroll>? scroll;
  List<Scroll>? popup;
  int? popupSwitch; // Can be '0', '1', or '2'
  double? popupInterval;

  HomeNoticeData({
    this.scroll,
    this.popup,
    this.popupSwitch,
    this.popupInterval,
  });

  factory HomeNoticeData.fromJson(Map<String, dynamic> json) {
    return HomeNoticeData(
      scroll: (json['scroll'] as List<dynamic>?)
          ?.map((item) => Scroll.fromJson(item))
          .toList(),
      popup: (json['popup'] as List<dynamic>?)
          ?.map((item) => Scroll.fromJson(item))
          .toList(),
      popupSwitch: json['popupSwitch'],
      popupInterval: json['popupInterval'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'scroll': scroll?.map((item) => item.toJson()).toList(),
      'popup': popup?.map((item) => item.toJson()).toList(),
      'popupSwitch': popupSwitch,
      'popupInterval': popupInterval,
    };
  }
}

class NoticeModel {
  int? code = 0;
  String? msg = "";
  HomeNoticeData? data;

  NoticeModel({
    this.code,
    this.msg,
    this.data,
  });

  factory NoticeModel.fromJson(Map<String, dynamic> json) {
    return NoticeModel(
      code: json['code'],
      msg: json['msg'],
      data: HomeNoticeData.fromJson(json['data']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'msg': msg,
      'data': data?.toJson(),
    };
  }
}
