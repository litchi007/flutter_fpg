class FeedbackDataList {
  String status;
  String isRead;
  String content;
  String id;
  String userName;
  String imgPaths;
  String type;
  String createTime;
  String pid;

  FeedbackDataList({
    required this.status,
    required this.isRead,
    required this.content,
    required this.id,
    required this.userName,
    required this.imgPaths,
    required this.type,
    required this.createTime,
    required this.pid,
  });

  // Deserialize from JSON
  factory FeedbackDataList.fromJson(Map<String, dynamic> json) =>
      FeedbackDataList(
        status: json['status'] ?? '',
        isRead: json['isRead'] ?? '',
        content: json['content'] ?? '',
        id: json['id'] ?? '',
        userName: json['userName'] ?? '',
        imgPaths: json['imgPaths'] ?? '',
        type: json['type'] ?? '',
        createTime: json['createTime'] ?? '',
        pid: json['pid'] ?? '',
      );

  // Serialize to JSON
  Map<String, dynamic> toJson() => {
        'status': status,
        'isRead': isRead,
        'content': content,
        'id': id,
        'userName': userName,
        'imgPaths': imgPaths,
        'type': type,
        'createTime': createTime,
        'pid': pid,
      };
}

class FeedbackData {
  int total;
  List<FeedbackDataList> list;

  FeedbackData({
    required this.total,
    required this.list,
  });

  // Deserialize from JSON
  factory FeedbackData.fromJson(Map<String, dynamic> json) => FeedbackData(
        total: json['total'] ?? 0,
        list: json['list'] != null
            ? List<FeedbackDataList>.from(
                json['list'].map((x) => FeedbackDataList.fromJson(x)))
            : [],
      );

  // Serialize to JSON
  Map<String, dynamic> toJson() => {
        'total': total,
        'list': List<dynamic>.from(list.map((x) => x.toJson())),
      };
}
