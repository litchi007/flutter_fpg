class BankInfoParam {
  String? parentTypeName; // 本地使用，父类名字，比如 银行卡

  String? id;
  String? uid;
  String? type;
  String? admin;
  String? enable;
  String? bankId;
  String? editEnable;
  String? xgtime;
  String? bdtime;
  String? ownerName;
  String? bankCard;
  String? bankCard2;
  String? bankAddr;
  String? bankName;
  String? bankCode;
  String? sort;
  String? bankBackgroundImage;
  bool? notBind; //本地使用，未绑定数据
  String? qrcode;

  BankInfoParam({
    this.parentTypeName,
    this.id,
    this.uid,
    this.type,
    this.admin,
    this.enable,
    this.bankId,
    this.editEnable,
    this.xgtime,
    this.bdtime,
    this.ownerName,
    this.bankCard,
    this.bankCard2,
    this.bankAddr,
    this.bankName,
    this.bankCode,
    this.sort,
    this.bankBackgroundImage,
    this.notBind,
    this.qrcode,
  });

  factory BankInfoParam.fromJson(Map<String, dynamic> json) {
    return BankInfoParam(
      parentTypeName: json['parentTypeName'],
      id: json['id'],
      uid: json['uid'],
      type: json['type'],
      admin: json['admin'].toString(),
      enable: json['enable'].toString(),
      bankId: json['bankId'].toString(),
      editEnable: json['editEnable'].toString(),
      xgtime: json['xgtime'].toString(),
      bdtime: json['bdtime'].toString(),
      ownerName: json['ownerName'],
      bankCard: json['bankCard'],
      bankCard2: json['bankCard2'],
      bankAddr: json['bankAddr'],
      bankName: json['bankName'],
      bankCode: json['bankCode'],
      sort: json['sort'].toString(),
      bankBackgroundImage: json['bankBackgroundImage'],
      notBind: json['notBind'],
      qrcode: json['qrcode'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'parentTypeName': parentTypeName,
      'id': id,
      'uid': uid,
      'type': type,
      'admin': admin,
      'enable': enable,
      'bankId': bankId,
      'editEnable': editEnable,
      'xgtime': xgtime,
      'bdtime': bdtime,
      'ownerName': ownerName,
      'bankCard': bankCard,
      'bankCard2': bankCard2,
      'bankAddr': bankAddr,
      'bankName': bankName,
      'bankCode': bankCode,
      'sort': sort,
      'bankBackgroundImage': bankBackgroundImage,
      'notBind': notBind,
      'qrcode': qrcode,
    };
  }
}

class AllAccountListData {
  String? type;
  String? name;
  bool? isshow;
  bool? ismore;
  String? number;
  String? minWithdrawMoney;
  String? maxWithdrawMoney;
  List<BankInfoParam>? data;

  AllAccountListData({
    this.type,
    this.name,
    this.isshow,
    this.ismore,
    this.number,
    this.minWithdrawMoney,
    this.maxWithdrawMoney,
    this.data,
  });

  factory AllAccountListData.fromJson(Map<String, dynamic> json) {
    List<dynamic>? dataList = json['data'];
    List<BankInfoParam>? data;
    if (dataList != null) {
      data = dataList.map((item) => BankInfoParam.fromJson(item)).toList();
    }
    return AllAccountListData(
      type: json['type'].toString(),
      name: json['name'],
      isshow: json['isshow'],
      ismore: json['ismore'],
      number: json['number'].toString(),
      minWithdrawMoney: json['minWithdrawMoney'],
      maxWithdrawMoney: json['maxWithdrawMoney'],
      data: data,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'type': type,
      'name': name,
      'isshow': isshow,
      'ismore': ismore,
      'number': number,
      'minWithdrawMoney': minWithdrawMoney,
      'maxWithdrawMoney': maxWithdrawMoney,
      'data': data?.map((e) => e.toJson()).toList(),
    };
  }
}

class ManageBankCardData {
  final List<AllAccountListData>? allAccountList;
  final String? id;
  final String? uid;
  final String? type;
  final String? admin;
  final String? enable;
  final String? bankId;
  final String? editEnable;
  final String? xgtime;
  final String? bdtime;
  final String? ownerName;
  final String? bankCard;
  final String? bankAddr;
  final String? bankName;
  final String? bankCode;
  final String? bankBackgroundImage;
  final String? cashFee;
  final int? isMtzcClose;
  final String? mtzcName;

  ManageBankCardData({
    this.allAccountList,
    this.id,
    this.uid,
    this.type,
    this.admin,
    this.enable,
    this.bankId,
    this.editEnable,
    this.xgtime,
    this.bdtime,
    this.ownerName,
    this.bankCard,
    this.bankAddr,
    this.bankName,
    this.bankCode,
    this.bankBackgroundImage,
    this.cashFee,
    this.isMtzcClose,
    this.mtzcName,
  });

  factory ManageBankCardData.fromJson(Map<String, dynamic> json) {
    List<dynamic>? dataList = json['allAccountList'];
    List<AllAccountListData>? data;
    if (dataList != null) {
      data = dataList.map((item) => AllAccountListData.fromJson(item)).toList();
    }
    return ManageBankCardData(
      allAccountList: data,
      id: json['id'],
      uid: json['uid'],
      type: json['type'].toString(),
      admin: json['admin'],
      enable: json['enable'],
      bankId: json['bankId'],
      editEnable: json['editEnable'],
      xgtime: json['xgtime'],
      bdtime: json['bdtime'],
      ownerName: json['ownerName'],
      bankCard: json['bankCard'],
      bankAddr: json['bankAddr'],
      bankName: json['bankName'],
      bankCode: json['bankCode'],
      bankBackgroundImage: json['bankBackgroundImage'],
      cashFee: json['cashFee'].toString(),
      isMtzcClose: json['isMtzcClose'],
      mtzcName: json['mtzcName'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'allAccountList': allAccountList?.map((e) => e.toJson()).toList(),
      'id': id,
      'uid': uid,
      'type': type,
      'admin': admin,
      'enable': enable,
      'bankId': bankId,
      'editEnable': editEnable,
      'xgtime': xgtime,
      'bdtime': bdtime,
      'ownerName': ownerName,
      'bankCard': bankCard,
      'bankAddr': bankAddr,
      'bankName': bankName,
      'bankCode': bankCode,
      'bankBackgroundImage': bankBackgroundImage,
      'cashFee': cashFee,
      'isMtzcClose': isMtzcClose,
      'mtzcName': mtzcName,
    };
  }
}
