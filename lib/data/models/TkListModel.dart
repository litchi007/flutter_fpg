import 'package:fpg_flutter/data/models/TkListItemModel.dart';

class TkListModel {
  String? title;
  List<TkListItemModel>? list;
  List<dynamic>? hotList;

  TkListModel({
    this.title,
    this.list,
    this.hotList,
  });

  factory TkListModel.fromJson(Map<String, dynamic> json) {
    return TkListModel(
      title: json['title'],
      list: (json['list'] as List<dynamic>)
          .map((item) => TkListItemModel.fromJson(item))
          .toList(),
      hotList: List<dynamic>.from(json['hot_list']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'list': list?.map((item) => item.toJson()).toList(),
      'hot_list': hotList,
    };
  }
}
