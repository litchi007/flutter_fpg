// ignore: file_names
class LhlDataModel {
  int? id;
  String? date;
  LhlInfoModel? info;

  LhlDataModel({
    this.id,
    this.date,
    this.info,
  });

  factory LhlDataModel.fromJson(Map<String, dynamic> json) {
    return LhlDataModel(
      id: json['id'], // 修改为 'id'
      date: json['date'], // 修改为 'date'
      info: LhlInfoModel.fromJson(json['info']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'date': date,
      'info': info?.toJson(),
    };
  }
}

class LhlInfoModel {
  String? baiJi;
  String? caiShen;
  String? chongSha;
  String? dayCN;
  String? dayEN;
  String? fuShen;
  String? ganZhi;
  String? ji;
  String? jiShenYiQu;
  String? jiShi;
  String? luckyNumber;
  String? monthCN;
  String? monthEN;
  String? riWuXing;
  String? riqi;
  String? weekCN;
  String? weekEN;
  String? xiShen;
  String? xiongShaYiJi;
  String? yearCN;
  String? yearEN;
  String? yi;

  // "baiJi": "丁不剃头 亥不结婚",
  // "caiShen": "西南",
  // "chongSha": "冲蛇(辛已)煞西",
  // "dayCN": "十八",
  // "dayEN": "20",
  // "fuShen": "东南",
  // "ganZhi": "丁亥",
  // "ji": "作灶 行丧 理发 乘船 嫁娶 安葬",
  // "jiShenYiQu": "相日 驿马 天后 天巫",
  // "jiShi": "凶 吉 凶 凶 吉 凶 吉 吉 凶 凶 吉 吉",
  // "luckyNumber": "48,32,36,45,03,38,26,13,02",
  // "monthCN": "八月",
  // "monthEN": "9",
  // "riWuXing": "屋上土",
  // "riqi": "20",
  // "weekCN": "星期五",
  // "weekEN": "Friday",
  // "xiShen": "正南",
  // "xiongShaYiJi": "五虚 八风 重日 大煞",
  // "yearCN": "甲辰",
  // "yearEN": 2024,
  // "yi": "开市 交易 立券 挂匾 开光 出行 入宅 移徙 安床 出火 上梁"

  // Constructor
  LhlInfoModel({
    this.baiJi,
    this.caiShen,
    this.chongSha,
    this.dayCN,
    this.dayEN,
    this.fuShen,
    this.ganZhi,
    this.ji,
    this.jiShenYiQu,
    this.jiShi,
    this.luckyNumber,
    this.monthCN,
    this.monthEN,
    this.riWuXing,
    this.riqi,
    this.weekCN,
    this.weekEN,
    this.xiShen,
    this.xiongShaYiJi,
    this.yearCN,
    this.yearEN,
    this.yi,
  });

  factory LhlInfoModel.fromJson(Map<String, dynamic> json) {
    return LhlInfoModel(
      baiJi: json['baiJi'],
      caiShen: json['caiShen'],
      chongSha: json['chongSha'],
      dayCN: json['dayCN'],
      dayEN: json['dayEN'],
      fuShen: json['fuShen'],
      ganZhi: json['ganZhi'],
      ji: json['ji'],
      jiShenYiQu: json['jiShenYiQu'],
      jiShi: json['jiShi'],
      luckyNumber: json['luckyNumber'],
      monthCN: json['monthCN'],
      monthEN: json['monthEN'],
      riWuXing: json['riWuXing'],
      riqi: json['riqi'],
      weekCN: json['weekCN'],
      weekEN: json['weekEN'],
      xiShen: json['xiShen'],
      xiongShaYiJi: json['xiongShaYiJi'],
      yearCN: json['yearCN'],
      yearEN: json['yearEN'],
      yi: json['yi'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'baiJi': baiJi,
      'caiShen': caiShen,
      'chongSha': chongSha,
      'dayCN': dayCN,
      'dayEN': dayEN,
      'fuShen': fuShen,
      'ganZhi': ganZhi,
      'ji': ji,
      'jiShenYiQu': jiShenYiQu,
      'jiShi': jiShi,
      'luckyNumber': luckyNumber,
      'monthCN': monthCN,
      'monthEN': monthEN,
      'riWuXing': riWuXing,
      'riqi': riqi,
      'weekCN': weekCN,
      'weekEN': weekEN,
      'xiShen': xiShen,
      'xiongShaYiJi': xiongShaYiJi,
      'yearCN': yearCN,
      'yearEN': yearEN,
      'yi': yi,
    };
  }
}
