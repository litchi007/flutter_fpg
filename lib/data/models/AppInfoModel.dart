class AppInfo {
  int code = 0;
  String sign = '';
  AppInfoData? data;
  AppInfo({
    this.code = 0,
    this.sign = '',
    this.data,
  });

  factory AppInfo.fromJson(Map<String, dynamic> json) {
    return AppInfo(
      data: AppInfoData.fromJson(json['data']),
      code: json['app_name'] ?? '',
      sign: json['sign'] ?? '',
    );
  }
}

class AppInfoData {
  String id = '';
  String appName = '';
  List<String> packageDomains = [];
  String siteNumber = '';
  String siteUrl = '';
  String alertLogLevel = '';

  AppInfoData({
    this.id = '',
    this.appName = '',
    this.packageDomains = const [],
    this.siteNumber = '',
    this.siteUrl = '',
    this.alertLogLevel = '',
  });

  factory AppInfoData.fromJson(Map<String, dynamic> json) {
    return AppInfoData(
      id: json['id'] ?? '',
      appName: json['app_name'] ?? '',
      packageDomains: List<String>.from(json['package_domains'] ?? []),
      siteNumber: json['site_number'] ?? '',
      siteUrl: json['site_url'] ?? '',
      alertLogLevel: json['alert_log_level'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'app_name': appName,
      'package_domains': packageDomains,
      'site_number': siteNumber,
      'site_url': siteUrl,
      'alert_log_level': alertLogLevel,
    };
  }
}
