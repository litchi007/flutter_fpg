class ApplyWinLogDetail {
  String? id;
  String? uid;
  String? username;
  String? winId;
  String? winName;
  String? amount;
  String? factor;
  String? userComment;
  String? adminComment;
  String? operator;
  String? state;
  String? enableLock;
  String? lockAdminUid;
  String? updateTime;
  String? checkTime; // Assuming checkTime is an integer timestamp
  String? activityType;
  String? applyId;
  String? dmlTimes;
  String? dml;
  String? applyImages;
  String? liqType;

  ApplyWinLogDetail({
    this.id,
    this.uid,
    this.username,
    this.winId,
    this.winName,
    this.amount,
    this.factor,
    this.userComment,
    this.adminComment,
    this.operator,
    this.state,
    this.enableLock,
    this.lockAdminUid,
    this.updateTime,
    this.checkTime,
    this.activityType,
    this.applyId,
    this.dmlTimes,
    this.dml,
    this.applyImages,
    this.liqType,
  });

  factory ApplyWinLogDetail.fromJson(Map<String, dynamic> json) {
    return ApplyWinLogDetail(
      id: json['id'] as String?,
      uid: json['uid'] as String?,
      username: json['username'] as String?,
      winId: json['winId'] as String?,
      winName: json['winName'] as String?,
      amount: json['amount'] as String?,
      factor: json['factor'] as String?,
      userComment: json['userComment'] as String?,
      adminComment: json['adminComment'] as String?,
      operator: json['operator'] as String?,
      state: json['state'] as String?,
      enableLock: json['enableLock'] as String?,
      lockAdminUid: json['lock_admin_uid'] as String?,
      updateTime: json['updateTime'] as String?,
      checkTime:
          json['checkTime'] as String?, // Adjust based on actual data type
      activityType: json['activity_type'] as String?,
      applyId: json['apply_id'] as String?,
      dmlTimes: json['dml_times'] as String?,
      dml: json['dml'] as String?,
      applyImages: json['apply_images'] as String?,
      liqType: json['liqType'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'uid': uid,
      'username': username,
      'winId': winId,
      'winName': winName,
      'amount': amount,
      'factor': factor,
      'userComment': userComment,
      'adminComment': adminComment,
      'operator': operator,
      'state': state,
      'enableLock': enableLock,
      'lock_admin_uid': lockAdminUid,
      'updateTime': updateTime,
      'checkTime': checkTime,
      'activity_type': activityType,
      'apply_id': applyId,
      'dml_times': dmlTimes,
      'dml': dml,
      'apply_images': applyImages,
      'liqType': liqType,
    };
  }
}
