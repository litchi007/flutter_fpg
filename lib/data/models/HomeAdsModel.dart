class HomeAdsModel {
  String? lotteryGameType;
  int? realIsPopup;
  String? linkPosition;
  String? image;
  int? realSupportTrial;
  String? linkCustom;
  String? linkCategory;
  String? linkAction;

  HomeAdsModel(
      {this.lotteryGameType,
      this.realIsPopup,
      this.linkPosition,
      this.image,
      this.realSupportTrial,
      this.linkCustom,
      this.linkCategory,
      this.linkAction});

  factory HomeAdsModel.fromJson(Map<String, dynamic> json) {
    return HomeAdsModel(
      lotteryGameType: json['lotteryGameType'],
      realIsPopup: json['realIsPopup'],
      linkPosition: json['linkPosition'],
      image: json['image'],
      realSupportTrial: json['realSupportTrial'],
      linkCustom: json['linkCustom'],
      linkCategory: json['linkCategory'],
      linkAction: json['linkAction'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['lotteryGameType'] = lotteryGameType;
    data['realIsPopup'] = realIsPopup;
    data['linkPosition'] = linkPosition;
    data['image'] = image;
    data['realSupportTrial'] = realSupportTrial;
    data['linkCustom'] = linkCustom;
    data['linkCategory'] = linkCategory;
    data['linkAction'] = linkAction;
    return data;
  }
}
