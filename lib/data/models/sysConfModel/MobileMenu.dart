import 'package:fpg_flutter/utils/helper.dart';

class MobileMenu {
  String? path;
  String? name;
  String? sort;
  String? isBuilding;
  String? iconLogo;
  String? selectedLogo;
  String? iconHot;
  int? status;
  String? isHot;
  String? icon;
  String? roles;
  String? url;
  dynamic setting;

  MobileMenu({
    this.path,
    this.name,
    this.sort,
    this.isBuilding,
    this.iconLogo,
    this.selectedLogo,
    this.iconHot,
    this.status,
    this.isHot,
    this.icon,
    this.roles,
    this.url,
    this.setting,
  });

  factory MobileMenu.fromJson(Map<String, dynamic> json) {
    return MobileMenu(
        path: json['path'],
        name: json['name'],
        sort: json['sort'],
        isBuilding: json['isBuilding'],
        iconLogo: json['icon_logo'],
        selectedLogo: json['selected_logo'],
        iconHot: json['icon_hot'],
        status: convertToInt(json['status']),
        isHot: json['isHot'],
        icon: json['icon'],
        roles: json['roles'],
        url: json['url'],
        setting: json['setting']);
  }

  Map<String, dynamic> toJson() {
    return {
      'path': path,
      'name': name,
      'sort': sort,
      'isBuilding': isBuilding,
      'icon_logo': iconLogo,
      'selected_logo': selectedLogo,
      'icon_hot': iconHot,
      'status': status,
      'isHot': isHot,
      'icon': icon,
      'roles': roles,
      'url': url,
      'setting': setting,
    };
  }
}

class MobileMenuSetting {
  String? title;
  List<SettingItem>? items;
  String? title2;
  List<SettingItem>? items2;
  String? title3;
  List<SettingItem>? items3;
  String? title4;
  List<SettingItem>? items4;

  MobileMenuSetting({
    this.title,
    this.items,
    this.title2,
    this.items2,
    this.title3,
    this.items3,
    this.title4,
    this.items4,
  });

  factory MobileMenuSetting.fromJson(Map<String, dynamic> json) {
    return MobileMenuSetting(
      title: json['title'],
      items: (json['items'] as List?)
          ?.map((i) => SettingItem.fromJson(i))
          .toList(),
      title2: json['title2'],
      items2: (json['items2'] as List?)
          ?.map((i) => SettingItem.fromJson(i))
          .toList(),
      title3: json['title3'],
      items3: (json['items3'] as List?)
          ?.map((i) => SettingItem.fromJson(i))
          .toList(),
      title4: json['title4'],
      items4: (json['items4'] as List?)
          ?.map((i) => SettingItem.fromJson(i))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'items': items?.map((item) => item.toJson()).toList(),
      'title2': title2,
      'items2': items2?.map((item) => item.toJson()).toList(),
      'title3': title3,
      'items3': items3?.map((item) => item.toJson()).toList(),
      'title4': title4,
      'items4': items4?.map((item) => item.toJson()).toList(),
    };
  }
}

class SettingItem {
  String? sort;
  String? name;
  String? link;
  String? blank;
  String? url;

  SettingItem({
    this.sort,
    this.name,
    this.link,
    this.blank,
    this.url,
  });

  factory SettingItem.fromJson(Map<String, dynamic> json) {
    return SettingItem(
      sort: json['sort'],
      name: json['name'],
      link: json['link'],
      blank: json['blank'],
      url: json['url'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'sort': sort,
      'name': name,
      'link': link,
      'blank': blank,
      'url': url,
    };
  }
}
