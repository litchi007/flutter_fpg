class Platform {
  final bool? facebook;
  final bool? sms;
  final bool? zalo;

  Platform({this.facebook, this.sms, this.zalo});

  // Factory constructor to create an instance from a map
  factory Platform.fromJson(Map<String, dynamic> json) {
    return Platform(
      facebook: json['facebook'] as bool?,
      sms: json['sms'] as bool?,
      zalo: json['zalo'] as bool?,
    );
  }

  // Method to convert an instance to a map
  Map<String, dynamic> toJson() {
    return {
      'facebook': facebook,
      'sms': sms,
      'zalo': zalo,
    };
  }
}

class OAuth {
  final Platform? platform;
  final bool? switch_;

  OAuth({this.platform, this.switch_});

  // Factory constructor to create an instance from a map
  factory OAuth.fromJson(Map<String, dynamic> json) {
    return OAuth(
      platform: Platform.fromJson(json['platform']),
      switch_: json['switch'] as bool?,
    );
  }

  // Method to convert an instance to a map
  Map<String, dynamic> toJson() {
    return {
      'platform': platform,
      'switch': switch_,
    };
  }
}
