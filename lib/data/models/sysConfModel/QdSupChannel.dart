class QdSupChannel {
  String? index;
  String? enName;
  String? name;

  QdSupChannel({this.index, this.name, this.enName});

  factory QdSupChannel.fromJson(Map<String, dynamic> json) {
    return QdSupChannel(
        index: json['index'], name: json['name'], enName: json['enName']);
  }

  Map<String, dynamic> toJson() {
    return {
      'index': index,
      'enName': enName,
      'name': name,
    };
  }
}
