class UserCenterItem {
  String? id;
  String? parentId;
  String? category;
  String? code;
  String? name;
  String? status;
  String? sorts;
  String? siteIds;
  String? siteId;
  String? showListStyle;
  String? linkUrl;
  String? userCenterCategory;
  String? sourceId;
  String? imageHot;
  String? logo;

  UserCenterItem({
    this.id,
    this.parentId,
    this.category,
    this.code,
    this.name,
    this.status,
    this.sorts,
    this.siteIds,
    this.siteId,
    this.showListStyle,
    this.linkUrl,
    this.userCenterCategory,
    this.sourceId,
    this.imageHot,
    this.logo,
  });

  factory UserCenterItem.fromJson(Map<String, dynamic> json) {
    return UserCenterItem(
      id: json['id'],
      parentId: json['parent_id'],
      category: json['category'],
      code: json['code'],
      name: json['name'],
      status: json['status'],
      sorts: json['sorts'],
      siteIds: json['site_ids'],
      siteId: json['site_id'],
      showListStyle: json['show_list_style'],
      linkUrl: json['link_url'],
      userCenterCategory: json['user_center_category'],
      sourceId: json['source_id'],
      imageHot: json['image_hot'],
      logo: json['logo'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'parent_id': parentId,
      'category': category,
      'code': code,
      'name': name,
      'status': status,
      'sorts': sorts,
      'site_ids': siteIds,
      'site_id': siteId,
      'show_list_style': showListStyle,
      'link_url': linkUrl,
      'user_center_category': userCenterCategory,
      'source_id': sourceId,
      'image_hot': imageHot,
      'logo': logo,
    };
  }
}
