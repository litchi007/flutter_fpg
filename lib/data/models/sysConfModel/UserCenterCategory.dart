class UserCenterCategory {
  // card, alipay, wechat, xnb
  int? id;
  String? name;

  UserCenterCategory({this.id, this.name});

  factory UserCenterCategory.fromJson(json) {
    return UserCenterCategory(id: json['id'], name: json['name']);
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
    };
  }
}
