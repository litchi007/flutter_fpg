class InviteCode {
  final String? codeSwitch;
  final String? displayWord;
  final String? canGenNum;
  final String? canUseNum;
  final String? randomSwitch;
  final String? randomLength;
  final String? noticeSwitch;
  final String? noticeText;

  InviteCode({
    this.codeSwitch,
    this.displayWord,
    this.canGenNum,
    this.canUseNum,
    this.randomSwitch,
    this.randomLength,
    this.noticeSwitch,
    this.noticeText,
  });

  factory InviteCode.fromJson(Map<String, dynamic> json) {
    return InviteCode(
      codeSwitch: json['switch'],
      displayWord: json['displayWord'],
      canGenNum: json['canGenNum'],
      canUseNum: json['canUseNum'],
      randomSwitch: json['randomSwitch'],
      randomLength: json['randomLength'],
      noticeSwitch: json['noticeSwitch'],
      noticeText: json['noticeText'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'switch': codeSwitch,
      'displayWord': displayWord,
      'canGenNum': canGenNum,
      'canUseNum': canUseNum,
      'randomSwitch': randomSwitch,
      'randomLength': randomLength,
      'noticeSwitch': noticeSwitch,
      'noticeText': noticeText,
    };
  }
}
