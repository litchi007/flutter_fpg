import 'package:fpg_flutter/utils/helper.dart';

class LHCPriceItem {
  String? id;
  String? alias;
  double? priceMax;
  double? priceMin;

  LHCPriceItem({this.id, this.alias, this.priceMax, this.priceMin});

  factory LHCPriceItem.fromJson(Map<String, dynamic> json) {
    return LHCPriceItem(
      id: json['id'],
      alias: json['alias'],
      priceMax: convertToDouble(json['priceMax']),
      priceMin: convertToDouble(json['priceMin']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'alias': alias,
      'priceMax': priceMax,
      'priceMin': priceMin,
    };
  }
}
