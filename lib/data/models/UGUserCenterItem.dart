import 'package:fpg_flutter/configs/pushhelper.dart';

class UGUserCenterItem {
  UGUserCenterType? code;
  String? logo;
  String? fallbackIcon;
  String? name;
  int? sorts;
  bool? isDefaultLogo;
  String? userCenterCategory;
  String? parentId;
  bool? show;

  // Static default logos
  static final Map<int, String> defaultLogos = {
    1: 'assets/chongzhi@2x.png', // 存款
    2: 'assets/tixian@2x.png', // 取款
    3: 'assets/yinhangqia@2x.png', // 银行卡管理
    4: 'assets/lixibao.png', // 利息宝
    5: 'assets/shouyisel.png', // 推荐收益
    6: 'assets/zdgl@2x.png', // 彩票注单记录
    7: 'assets/zdgl@2x.png', // 其他注单记录
    8: 'assets/change@2x.png', // 额度转换
    9: 'assets/zhanneixin@2x.png', // 站内信
    10: 'assets/ziyuan@2x.png', // 安全中心
    11: 'assets/renwuzhongxin.png', // 任务中心
    12: 'assets/gerenzhongxinxuanzhong.png', // 个人信息
    13: 'assets/yijian.png', // 建议反馈
    14: 'assets/zaixiankefu@2x.png', // 在线客服
    15: 'assets/zdgl@2x.png', // 活动彩金
    16: 'assets/changlong@2x.png', // 长龙助手
    17: 'assets/menu-activity.png', // 全民竞猜
    18: 'assets/kj_trend.png', // 开奖走势
    19: 'assets/usrCenter_qq.png', // QQ客服
    20: 'assets/center_kaijiang@2x.png', // UCI_开奖网
    21: 'assets/zdgl@2x.png', // UCI_未结注单
    22: 'assets/zdgl@2x.png', // UCI_电子注单
    23: 'assets/zdgl@2x.png', // UCI_真人注单
    24: 'assets/zdgl@2x.png', // UCI_棋牌注单
    25: 'assets/zdgl@2x.png', // UCI_捕鱼注单
    26: 'assets/zdgl@2x.png', // UCI_电竞注单
    27: 'assets/zdgl@2x.png', // UCI_体育注单
    28: 'assets/zdgl@2x.png', // UCI_UG注单
    29: 'assets/zdgl@2x.png', // UCI_已结注单
    30: 'assets/chongzhi@2x.png', // UCI_充值纪录
    31: 'assets/chongzhi@2x.png', // UCI_提现纪录
    32: 'assets/chongzhi@2x.png', // UCI_资金明细
    33: 'assets/zdgl@2x.png', // UCI_活动大厅
    34: 'assets/weChat_icon.png', // 聊天室
    35: 'assets/invi@2x.png', // UCI_我的关注
    36: 'assets/friend.png', // UCI_我的动态
    37: 'assets/fans.png', // UCI_我的粉丝
    40: 'assets/AUTOBET/autobet_bet.png', //挂机投注
  };

  UGUserCenterItem({
    this.code,
    this.logo,
    this.fallbackIcon,
    this.name,
    this.sorts,
    this.isDefaultLogo = false,
    this.userCenterCategory,
    this.parentId,
    this.show = true,
  }) {
    // Set default logo if necessary
    if (logo == null || !logo!.contains('http')) {
      logo = defaultLogos[
          (code?.index ?? 0) + 1]; // Adjust index for default logos
      isDefaultLogo = true;
    }
  }
}
