import 'package:fpg_flutter/data/models/LhcdocContentList.dart';

class LhcdocContent {
  List<LhcdocContentList>? list;
  String? title;
  String? baomaType;
  String? baomaId;
  int? total;
  dynamic isHomeListTopShow;
  dynamic homeListTopWords;

  LhcdocContent({
    this.list,
    this.title,
    this.baomaType,
    this.baomaId,
    this.total,
    this.isHomeListTopShow,
    this.homeListTopWords,
  });

  factory LhcdocContent.fromJson(Map<String, dynamic> json) {
    return LhcdocContent(
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => LhcdocContentList.fromJson(e as Map<String, dynamic>))
          .toList(),
      title: json['title'] as String?,
      baomaType: json['baomaType'] as String?,
      baomaId: json['baomaId'] as String?,
      total: json['total'] as int?,
      isHomeListTopShow: json['isHomeListTopShow'],
      homeListTopWords: json['homeListTopWords'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((e) => e.toJson()).toList(),
      'title': title,
      'baomaType': baomaType,
      'baomaId': baomaId,
      'total': total,
      'isHomeListTopShow': isHomeListTopShow,
      'homeListTopWords': homeListTopWords,
    };
  }
}
