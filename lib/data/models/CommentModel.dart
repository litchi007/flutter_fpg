import 'package:fpg_flutter/utils/helper.dart';

class CommentModel {
  String? id;
  String? uid;
  String? content;
  String? actionTime;
  String? replyCount;
  String? likeNum;
  int? isLike;
  int? isAdmin;
  int? isAuthor;
  String? nickname;
  String? headImg;
  List<SecReplyItem>? secReplyList;

  CommentModel({
    this.actionTime,
    this.content,
    this.headImg,
    this.id,
    this.isAdmin,
    this.isAuthor,
    this.isLike,
    this.likeNum,
    this.nickname,
    this.replyCount,
    this.uid,
    this.secReplyList,
  });

  factory CommentModel.fromJson(Map<String, dynamic> json) {
    return CommentModel(
      id: json['id'],
      uid: json['uid'],
      content: json['content'],
      actionTime: json['actionTime'],
      replyCount: json['replyCount'],
      likeNum: json['likeNum'],
      isLike: convertToInt(json['isLike']),
      isAdmin: convertToInt(json['isAdmin']),
      isAuthor: convertToInt(json['isAuthor']),
      nickname: json['nickname'],
      headImg: json['headImg'],
      secReplyList: (json['secReplyList'] as List?)
          ?.map((e) => SecReplyItem.fromJson(e))
          .toList(),
    );
  }
  Map<String, dynamic> toJson() {
    return {
      "actionTime": actionTime,
      "content": content,
      "headImg": headImg,
      "id": id,
      "isAdmin": isAdmin,
      "isAuthor": isAuthor,
      "isLike": isLike,
      "likeNum": likeNum,
      "nickname": nickname,
      "replyCount": replyCount,
      "uid": uid,
      "secReplyList": secReplyList,
    };
  }
}

class SecReplyItem {
  String? id;
  String? uid;
  String? content;
  String? actionTime;
  String? replyPId;
  int? isAdmin;
  int? isAuthor;
  String? nickname;
  String? headImg;
  SecReplyItem({
    this.id,
    this.uid,
    this.content,
    this.actionTime,
    this.replyPId,
    this.headImg,
    this.isAdmin,
    this.isAuthor,
    this.nickname,
  });
  factory SecReplyItem.fromJson(Map<String, dynamic> json) {
    return SecReplyItem(
      id: json['id'],
      uid: json['uid'],
      content: json['content'],
      actionTime: json['actionTime'],
      replyPId: json['replyPId'],
      isAdmin: json['isAdmin'],
      isAuthor: json['isAuthor'],
      nickname: json['nickname'],
      headImg: json['headImg'],
    );
  }
}
