enum ActionType {
  DEPOSIT,
  WITHDRAW,
}

class Filter {
  ActionType type;
  List<int>? payChannel;
  int? amountMin;
  int? amountMax;

  Filter({
    required this.type,
    this.payChannel,
    this.amountMin,
    this.amountMax,
  });

  Map<String, dynamic> toJson() {
    return {
      'type': type.index + 1, // Mapping to 1-based index for JSON compatibility
      'payChannel': payChannel,
      'amountMin': amountMin,
      'amountMax': amountMax,
    };
  }

  factory Filter.fromJson(Map<String, dynamic> json) {
    return Filter(
      type:
          ActionType.values[json['type'] - 1], // Convert 1-based index to enum
      payChannel: json['payChannel'] != null
          ? List<int>.from(json['payChannel'])
          : null,
      amountMin: json['amountMin'],
      amountMax: json['amountMax'],
    );
  }
}

class BankType {
  String index;
  String enName;
  String name;

  BankType({
    required this.index,
    required this.enName,
    required this.name,
  });

  Map<String, dynamic> toJson() {
    return {
      'index': index,
      'enName': enName,
      'name': name,
    };
  }

  factory BankType.fromJson(Map<String, dynamic> json) {
    return BankType(
      index: json['index'],
      enName: json['enName'],
      name: json['name'],
    );
  }
}

class NewDeal {
  String? id;
  ActionType? type;
  String? ownerUsername;
  String? withdrawUsername;
  String? amount;
  dynamic position;
  List<BankType>? allowBankType;

  NewDeal({
    this.id,
    this.type,
    this.ownerUsername,
    this.withdrawUsername,
    this.amount,
    this.position,
    this.allowBankType,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'type': type != null
          ? type!.index + 1
          : 1, // Mapping to 1-based index for JSON compatibility
      'owner_username': ownerUsername,
      'withdraw_username': withdrawUsername,
      'amount': amount,
      'position': position,
      'allow_bank_type':
          allowBankType?.map((bankType) => bankType.toJson()).toList(),
    };
  }

  factory NewDeal.fromJson(Map<String, dynamic> json) {
    return NewDeal(
      id: json['id'],
      type:
          ActionType.values[json['type'] - 1], // Convert 1-based index to enum
      ownerUsername: json['owner_username'],
      withdrawUsername: json['withdraw_username'],
      amount: json['amount'],
      position: json['position'],
      allowBankType: json['allow_bank_type'] != null
          ? List<BankType>.from(json['allow_bank_type']
              .map((bankJson) => BankType.fromJson(bankJson)))
          : null,
    );
  }
}

class DealListData {
  List<NewDeal> data;
  int total;

  DealListData({
    required this.data,
    required this.total,
  });

  Map<String, dynamic> toJson() {
    return {
      'data': data.map((deal) => deal.toJson()).toList(),
      'total': total,
    };
  }

  factory DealListData.fromJson(Map<String, dynamic> json) {
    return DealListData(
      data: List<NewDeal>.from(
          json['data'].map((dealJson) => NewDeal.fromJson(dealJson))),
      total: json['total'],
    );
  }
}

class DialogBoxProps {
  bool isVisible;
  String title;
  String? content;
  bool? hideCancel;
  String? cancelText;
  String? confirmText;
  // Assuming ViewStyle is a custom class that needs to be serialized separately

  DialogBoxProps({
    required this.isVisible,
    this.title = '提示',
    this.content,
    this.hideCancel,
    this.cancelText,
    this.confirmText,
  });

  factory DialogBoxProps.fromJson(Map<String, dynamic> json) {
    return DialogBoxProps(
      isVisible: json['isVisible'] ?? false,
      title: json['title'] ?? '提示',
      content: json['content'],
      hideCancel: json['hideCancel'],
      cancelText: json['cancelText'],
      confirmText: json['confirmText'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      'isVisible': isVisible,
      'title': title,
      'content': content,
      'hideCancel': hideCancel,
      'cancelText': cancelText,
      'confirmText': confirmText,
    };
    return data;
  }
}
