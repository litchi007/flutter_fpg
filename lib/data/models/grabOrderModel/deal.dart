class Deal {
  String? id;
  String? type;
  String? amount;
  String? add_time;
  String? pickTime;
  String? orderno;
  String? remark;
  String? position;
  String? bankAccount;
  String? bankRealname;
  String? bankAddress;
  String? bankType;
  String? allowBankType;
  String? bankTitle;
  String? failReason;
  int? leftTime;
  String? trade_username;
  String? status_str;
  String? status_desc;
  int? status_num;
  Map<String, dynamic>? bankTypeInfo;
  String? bankAccount2;
  String? sendType;
  String? award;
  String? warmNotice;
  String? payedImg;

  Deal({
    this.id,
    this.type,
    this.amount,
    this.add_time,
    this.pickTime,
    this.orderno,
    this.remark,
    this.position,
    this.bankAccount,
    this.bankRealname,
    this.bankAddress,
    this.bankType,
    this.allowBankType,
    this.bankTitle,
    this.failReason,
    this.leftTime,
    this.trade_username,
    this.status_str,
    this.status_desc,
    this.status_num,
    this.bankTypeInfo,
    this.bankAccount2,
    this.sendType,
    this.award,
    this.warmNotice,
    this.payedImg,
  });

  factory Deal.fromJson(Map<String, dynamic> json) => Deal(
        id: json['id'],
        type: json['type'],
        amount: json['amount'],
        add_time: json['add_time'],
        pickTime: json['pick_time'],
        orderno: json['orderno'],
        remark: json['remark'],
        position: json['position'],
        bankAccount: json['bank_account'],
        bankRealname: json['bank_realname'],
        bankAddress: json['bank_address'],
        bankType: json['bank_type'],
        allowBankType: json['allow_bank_type'],
        bankTitle: json['bank_title'],
        failReason: json['fail_reason'],
        leftTime: json['left_time'],
        trade_username: json['trade_username'],
        status_str: json['status_str'],
        status_desc: json['status_desc'],
        status_num: json['status_num'],
        bankTypeInfo: json['bank_type_info'],
        bankAccount2: json['bank_account2'],
        sendType: json['send_type'],
        award: json['award'],
        warmNotice: json['warmNotice'],
        payedImg: json['payed_img'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'type': type,
        'amount': amount,
        'add_time': add_time,
        'pick_time': pickTime,
        'orderno': orderno,
        'remark': remark,
        'position': position,
        'bank_account': bankAccount,
        'bank_realname': bankRealname,
        'bank_address': bankAddress,
        'bank_type': bankType,
        'allow_bank_type': allowBankType,
        'bank_title': bankTitle,
        'fail_reason': failReason,
        'left_time': leftTime,
        'trade_username': trade_username,
        'status_str': status_str,
        'status_desc': status_desc,
        'status_num': status_num,
        'bank_type_info': bankTypeInfo,
        'bank_account2': bankAccount2,
        'send_type': sendType,
        'award': award,
        'warmNotice': warmNotice,
        'payed_img': payedImg,
      };
}

class Record {
  String id;
  int type; // 1-存款， 2-取款
  String status;
  String add_time;
  String orderno;
  String amount;
  String trade_username;
  String status_str;
  String status_desc;
  int status_num;

  Record({
    required this.id,
    required this.type,
    required this.status,
    required this.add_time,
    required this.orderno,
    required this.amount,
    required this.trade_username,
    required this.status_str,
    required this.status_desc,
    required this.status_num,
  });

  factory Record.fromJson(Map<String, dynamic> json) => Record(
        id: json['id'],
        type: json['type'],
        status: json['status'],
        add_time: json['add_time'],
        orderno: json['orderno'],
        amount: json['amount'],
        trade_username: json['trade_username'],
        status_str: json['status_str'],
        status_desc: json['status_desc'],
        status_num: json['status_num'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'type': type,
        'status': status,
        'add_time': add_time,
        'orderno': orderno,
        'amount': amount,
        'trade_username': trade_username,
        'status_str': status_str,
        'status_desc': status_desc,
        'status_num': status_num,
      };
}

class RecordsData {
  String total;
  List<Record> data;

  RecordsData({
    required this.total,
    required this.data,
  });

  factory RecordsData.fromJson(Map<String, dynamic> json) => RecordsData(
        total: json['total'],
        data: List<Record>.from(
            json['data'].map((recordJson) => Record.fromJson(recordJson))),
      );

  Map<String, dynamic> toJson() => {
        'total': total,
        'data': List<dynamic>.from(data.map((record) => record.toJson())),
      };
}
