import 'package:fpg_flutter/data/models/GrabItemModel.dart';

class RedBagGrabModel {
  String? roomId;
  String? uid;
  String? title;
  String? genre;
  String? amount;
  int? quantity;
  String? createDmlMultiple;
  String? grabDmlMultiple;
  String? grabLevels;
  int? oddsRate;
  int? mineNumber;
  dynamic createTime;
  dynamic expireTime;
  dynamic updateTime;
  int? systemPosition;
  int? status;
  int? isSend;
  String? id;
  bool? isLuck;
  int? luckAmount;
  bool? isMine;
  int? isGrab;
  String? grabAmount;
  List<GrabItemModel>? grabList;
  String? getNum;
  int? remainingNum;

  RedBagGrabModel({
    this.roomId,
    this.uid,
    this.title,
    this.genre,
    this.amount,
    this.quantity,
    this.createDmlMultiple,
    this.grabDmlMultiple,
    this.grabLevels,
    this.oddsRate,
    this.mineNumber,
    this.createTime,
    this.expireTime,
    this.updateTime,
    this.systemPosition,
    this.status,
    this.isSend,
    this.id,
    this.isLuck,
    this.luckAmount,
    this.isMine,
    this.isGrab,
    this.grabAmount,
    this.grabList,
    this.getNum,
    this.remainingNum,
  });

  factory RedBagGrabModel.fromJson(Map<String?, dynamic> json) {
    return RedBagGrabModel(
      roomId: json['roomId'],
      uid: json['uid'],
      title: json['title'],
      genre: json['genre'],
      amount: json['amount'],
      quantity: json['quantity'],
      createDmlMultiple: json['createDmlMultiple'],
      grabDmlMultiple: json['grabDmlMultiple'],
      grabLevels: json['grabLevels'],
      oddsRate: json['oddsRate'],
      mineNumber: json['mineNumber'],
      createTime: json['createTime'],
      expireTime: json['expireTime'],
      updateTime: json['updateTime'],
      systemPosition: json['systemPosition'],
      status: json['status'],
      isSend: json['isSend'],
      id: json['id'],
      isLuck: json['isLuck'],
      luckAmount: json['luckAmount'],
      isMine: json['isMine'],
      isGrab: json['is_grab'],
      grabAmount: json['grab_amount'],
      grabList: (json['grabList'] as List<dynamic>)
          .map((item) => GrabItemModel.fromJson(item))
          .toList(),
      getNum: json['getNum'],
      remainingNum: json['remainingNum'],
    );
  }

  Map<String?, dynamic> toJson() {
    return {
      'roomId': roomId,
      'uid': uid,
      'title': title,
      'genre': genre,
      'amount': amount,
      'quantity': quantity,
      'createDmlMultiple': createDmlMultiple,
      'grabDmlMultiple': grabDmlMultiple,
      'grabLevels': grabLevels,
      'oddsRate': oddsRate,
      'mineNumber': mineNumber,
      'createTime': createTime,
      'expireTime': expireTime,
      'updateTime': updateTime,
      'systemPosition': systemPosition,
      'status': status,
      'isSend': isSend,
      'id': id,
      'isLuck': isLuck,
      'luckAmount': luckAmount,
      'isMine': isMine,
      'is_grab': isGrab,
      'grab_amount': grabAmount,
      'grabList': grabList?.map((item) => item.toJson()).toList(),
      'getNum': getNum,
      'remainingNum': remainingNum,
    };
  }
}
