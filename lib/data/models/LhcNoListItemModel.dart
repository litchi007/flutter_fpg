class LhcNoListItemModel {
  String? lhcNo;
  String? fullLhcNo;
  String? id;
  String? contentPic;

  LhcNoListItemModel({
    this.lhcNo,
    this.fullLhcNo,
    this.id,
    this.contentPic,
  });

  factory LhcNoListItemModel.fromJson(Map<String, dynamic> json) {
    return LhcNoListItemModel(
      lhcNo: json['lhcNo'],
      fullLhcNo: json['full_lhcno'],
      id: json['id'],
      contentPic: json['contentPic'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'lhcNo': lhcNo,
      'full_lhcno': fullLhcNo,
      'id': id,
      'contentPic': contentPic,
    };
  }
}
