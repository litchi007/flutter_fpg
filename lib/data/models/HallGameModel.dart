class HallGameModel {
  int? code;
  String? msg;
  List<HallGameData>? data;

  HallGameModel({
    this.code,
    this.msg,
    this.data,
  });

  factory HallGameModel.fromJson(Map<String, dynamic> json) {
    List<dynamic>? dataList = json['data'];
    List<HallGameData>? data;
    if (dataList != null) {
      data = dataList.map((item) => HallGameData.fromJson(item)).toList();
    }
    return HallGameModel(
      code: json['code'] as int?,
      msg: json['msg'],
      data: data,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'msg': msg,
      'data': data?.map((item) => item.toJson()).toList(),
    };
  }
}

class HallGameData {
  String? gameType;
  String? gameTypeName;
  String? logo;
  List<HallGameModel1>? list;

  HallGameData({
    this.gameType,
    this.gameTypeName,
    this.logo,
    this.list,
  });

  factory HallGameData.fromJson(Map<String, dynamic> json) {
    List<dynamic>? listData = json['list'];
    List<HallGameModel1>? list;
    if (listData != null) {
      list = listData.map((item) => HallGameModel1.fromJson(item)).toList();
    }
    return HallGameData(
      gameType: json['gameType'],
      gameTypeName: json['gameTypeName'],
      logo: json['logo'],
      list: list,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'gameType': gameType,
      'gameTypeName': gameTypeName,
      'logo': logo,
      'list': list?.map((item) => item.toJson()).toList(),
    };
  }
}

class HallGameModel1 {
  String? id;
  String? isSeal;
  String? isClose;
  String? enable;
  String? name;
  String? title;
  String? customise;
  String? isInstant;
  String? lowFreq;
  String? gameType;
  String? logo;
  String? icon;
  String? pic;
  String? openCycle;
  String? curIssue;
  String? curOpenTime;
  String? curCloseTime;
  String? displayNumber;
  String? preIssue;
  String? preOpenTime;
  String? preNum;
  String? preDisplayNumber;
  String? preResult;
  String? sort;
  String? serverTime;
  String? serverTimestamp;
  String? parentGameType;

  HallGameModel1({
    this.id,
    this.isSeal,
    this.isClose,
    this.enable,
    this.name,
    this.title,
    this.customise,
    this.isInstant,
    this.lowFreq,
    this.gameType,
    this.logo,
    this.icon,
    this.pic,
    this.openCycle,
    this.curIssue,
    this.curOpenTime,
    this.curCloseTime,
    this.displayNumber,
    this.preIssue,
    this.preOpenTime,
    this.preNum,
    this.preDisplayNumber,
    this.preResult,
    this.sort,
    this.serverTime,
    this.serverTimestamp,
    this.parentGameType,
  });

  factory HallGameModel1.fromJson(Map<String, dynamic> json) {
    return HallGameModel1(
      id: json['id'],
      isSeal: json['isSeal'],
      isClose: json['isClose'],
      enable: json['enable'],
      name: json['name'],
      title: json['title'],
      customise: json['customise'],
      isInstant: json['isInstant'],
      lowFreq: json['lowFreq'],
      gameType: json['gameType'],
      logo: json['logo'],
      icon: json['icon'],
      pic: json['pic'],
      openCycle: json['openCycle'],
      curIssue: json['curIssue'].toString(),
      curOpenTime: json['curOpenTime'],
      curCloseTime: json['curCloseTime'],
      displayNumber: json['displayNumber'].toString(),
      preIssue: json['preIssue'].toString(),
      preOpenTime: json['preOpenTime'],
      preNum: json['preNum'],
      preDisplayNumber: json['preDisplayNumber'].toString(),
      preResult: json['preResult'],
      sort: json['sort'].toString(),
      serverTime: json['serverTime'],
      serverTimestamp: json['serverTimestamp'],
      parentGameType: json['parentGameType'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'isSeal': isSeal,
      'isClose': isClose,
      'enable': enable,
      'name': name,
      'title': title,
      'customise': customise,
      'isInstant': isInstant,
      'lowFreq': lowFreq,
      'gameType': gameType,
      'logo': logo,
      'icon': icon,
      'pic': pic,
      'openCycle': openCycle,
      'curIssue': curIssue,
      'curOpenTime': curOpenTime,
      'curCloseTime': curCloseTime,
      'displayNumber': displayNumber,
      'preIssue': preIssue,
      'preOpenTime': preOpenTime,
      'preNum': preNum,
      'preDisplayNumber': preDisplayNumber,
      'preResult': preResult,
      'sort': sort,
      'serverTime': serverTime,
      'serverTimestamp': serverTimestamp,
      'parentGameType': parentGameType,
    };
  }
}

class GroupGameData {
  String? id;
  String? name;
  String? logo;
  List<HallGameModel2>? lotteries;

  GroupGameData({
    this.id,
    this.name,
    this.logo,
    this.lotteries,
  });

  factory GroupGameData.fromJson(Map<String, dynamic> json) {
    return GroupGameData(
      id: json['id'].toString(),
      name: json['name'],
      logo: json['logo'],
      lotteries: (json['lotteries'] as List<dynamic>?)
          ?.map((item) => HallGameModel2.fromJson(item))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'logo': logo,
      'lotteries': lotteries?.map((item) => item.toJson()).toList(),
    };
  }
}

class HallGameModel2 {
  String? id;
  String? isSeal;
  String? isClose;
  String? name;
  String? title;
  String? customise;
  String? isInstant;
  String? lowFreq;
  String? enable;
  String? shortName;
  String? onGetNoed;
  String? data_ftime;
  String? gameType;
  String? logo;
  String? serverTime;
  int? serverTimestamp;
  String? preNum;
  String? preIssue;
  String? preLotteryTime;

  HallGameModel2({
    this.customise,
    this.data_ftime,
    this.enable,
    this.gameType,
    this.id,
    this.isClose,
    this.isInstant,
    this.isSeal,
    this.logo,
    this.lowFreq,
    this.name,
    this.onGetNoed,
    this.preIssue,
    this.preLotteryTime,
    this.preNum,
    this.serverTime,
    this.serverTimestamp,
    this.shortName,
    this.title,
  });

  factory HallGameModel2.fromJson(Map<String, dynamic> json) {
    return HallGameModel2(
      customise: json['customise'],
      data_ftime: json['data_ftime'],
      enable: json['enable'],
      gameType: json['gameType'],
      id: json['id'],
      isClose: json['isClose'],
      isInstant: json['isInstant'],
      isSeal: json['isSeal'],
      logo: json['logo'],
      lowFreq: json['lowFreq'],
      name: json['name'],
      onGetNoed: json['onGetNoed'],
      preIssue: json['preIssue'],
      preLotteryTime: json['preLotteryTime'],
      preNum: json['preNum'],
      serverTime: json['serverTime'],
      serverTimestamp: json['serverTimestamp'],
      shortName: json['shortName'],
      title: json['title'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isSeal': isSeal,
      'id': id,
      'customise': customise,
      'lowFreq': lowFreq,
      'serverTimestamp': serverTimestamp,
      'title': title,
      'logo': logo,
      'isInstant': isInstant,
      'gameType': gameType,
      'serverTime': serverTime,
      'isClose': isClose,
      'name': name,
      'enable': enable,
      'shortName': shortName,
      'gameId': id,
      'preLotteryTime': onGetNoed,
      'data_ftime': data_ftime,
    };
  }
}
// class HallGameModel2 {
//   String? isSeal;
//   String? id;
//   String? customise;
//   String? lowFreq;
//   int? serverTimestamp;
//   String? title;
//   String? logo;
//   String? isInstant;
//   String? gameType;
//   String? serverTime;
//   String? isClose;
//   String? name;
//   String? enable;
//   String? shortName;
//   String? onGetNoed;
//   String? data_ftime;
//   String? gameId;

//   String? icon;
//   String? pic;
//   String? openCycle; //"20分钟一期"
//   String? curIssue; //"2012200041"
//   String? curOpenTime; //"2020-12-20 13:20:00"
//   String? curCloseTime; // "2020-12-20 13:19:50"
//   String? displayNumber; // 2012200041
//   String? preIssue; //"2012200041"
//   String? preOpenTime; //"2020-12-20 13:20:00"
//   String? preNum; // 40,22,25,33,31,20,27
//   String? preDisplayNumber; // 2012200041
//   String? preResult; // "鸡,兔,鼠,龙,马,蛇,狗"
//   String? sort; //
//   // 自定义参数
//   String? parentGameType; //父类游戏类型
//   String? dataNum;
//   String? endTime;
//   String? interval;
//   String? issue;
//   String? lotteryTime;
//   String? nums;
//   bool? preIsOpen;
//   String? preLotteryTime;
//   String? preNumColor;
//   String? totalNum;

//   HallGameModel2({
//     this.isSeal,
//     this.id,
//     this.customise,
//     this.lowFreq,
//     this.serverTimestamp,
//     this.title,
//     this.logo,
//     this.isInstant,
//     this.gameType,
//     this.serverTime,
//     this.isClose,
//     this.name,
//     this.enable,
//     this.shortName,
//     this.onGetNoed,
//     this.data_ftime,
//     this.curCloseTime,
//     this.curIssue,
//     this.curOpenTime,
//     this.displayNumber,
//     this.icon,
//     this.openCycle,
//     this.parentGameType,
//     this.pic,
//     this.preDisplayNumber,
//     this.preIssue,
//     this.preNum,
//     this.preOpenTime,
//     this.preResult,
//     this.sort,
//     this.gameId,
//     this.dataNum,
//     this.endTime,
//     this.interval,
//     this.issue,
//     this.lotteryTime,
//     this.nums,
//     this.preIsOpen,
//     this.preLotteryTime,
//     this.preNumColor,
//     this.totalNum,
//   });

//   factory HallGameModel2.fromJson(Map<String, dynamic> json) {
//     return HallGameModel2(
//       isSeal: json['isSeal'] ? json['isSeal'].toString() : null,
//       id: json['id'] ? json['id'].toString() : null,
//       customise: json['customise'] ? json['customise'].toString() : null,
//       lowFreq: json['lowFreq'] ? json['lowFreq'].toString() : null,
//       serverTimestamp: json['serverTimestamp'] ? json['serverTimestamp'] : null,
//       title: json['title'] ? json['title'].toString() : null,
//       logo: json['logo'] ? json['logo'].toString() : null,
//       isInstant: json['isInstant'] ? json['isInstant'].toString() : null,
//       gameType: json['gameType'] ? json['gameType'].toString() : null,
//       serverTime: json['serverTime'] ? json['serverTime'].toString() : null,
//       isClose: json['isClose'] ? json['isClose'].toString() : null,
//       name: json['name'] ? json['name'].toString() : null,
//       enable: json['enable'] ? json['enable'].toString() : null,
//       shortName: json['shortName'] ? json['shortName'].toString() : null,
//       onGetNoed: json['onGetNoed'] ? json['onGetNoed'].toString() : null,
//       data_ftime: json['data_ftime'] ? json['data_ftime'].toString() : null,
//       curCloseTime:
//           json["curCloseTime"] ? json["curCloseTime"].toString() : null,
//       curIssue: json["curIssue"] ? json["curIssue"].toString() : null,
//       curOpenTime: json["curOpenTime"] ? json["curOpenTime"].toString() : null,
//       displayNumber:
//           json["displayNumber"] ? json["displayNumber"].toString() : null,
//       icon: json["icon"] ? json["icon"].toString() : null,
//       openCycle: json["openCycle"] ? json["openCycle"].toString() : null,
//       parentGameType:
//           json["parentGameType"] ? json["parentGameType"].toString() : null,
//       pic: json["pic"] ? json["pic"].toString() : null,
//       preDisplayNumber:
//           json["preDisplayNumber"] ? json["preDisplayNumber"].toString() : null,
//       preIssue: json["preIssue"] ? json["preIssue"].toString() : null,
//       preNum: json["preNum"] ? json["preNum"].toString() : null,
//       preOpenTime: json["preOpenTime"] ? json["preOpenTime"].toString() : null,
//       preResult: json["preResult"] ? json["preResult"].toString() : null,
//       sort: json["sort"] ? json["sort"].toString() : null,
//       gameId: json["gameId"] ? json["gameId"].toString() : null,
//       dataNum: json["dataNum"] ? json["dataNum"].toString() : null,
//       endTime: json["endTime"] ? json["endTime"].toString() : null,
//       interval: json["interval"] ? json["interval"].toString() : null,
//       issue: json["issue"] ? json["issue"].toString() : null,
//       lotteryTime: json["lotteryTime"] ? json["lotteryTime"].toString() : null,
//       nums: json["nums"] ? json["nums"].toString() : null,
//       preIsOpen: json["preIsOpen"] ? json["preIsOpen"] : null,
//       preLotteryTime:
//           json["preLotteryTime"] ? json["preLotteryTime"].toString() : null,
//       preNumColor: json["preNumColor"] ? json["preNumColor"].toString() : null,
//       totalNum: json["totalNum"] ? json["totalNum"].toString() : null,
//     );
//   }

//   Map<String, dynamic> toJson() {
//     return {
//       'isSeal': isSeal,
//       'id': id,
//       'customise': customise,
//       'lowFreq': lowFreq,
//       'serverTimestamp': serverTimestamp,
//       'title': title,
//       'logo': logo,
//       'isInstant': isInstant,
//       'gameType': gameType,
//       'serverTime': serverTime,
//       'isClose': isClose,
//       'name': name,
//       'enable': enable,
//       'shortName': shortName,
//       'gameId': gameId,
//       'preLotteryTime': onGetNoed,
//       'data_ftime': data_ftime,
//     };
//   }
// }
