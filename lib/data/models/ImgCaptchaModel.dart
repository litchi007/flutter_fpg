class ImgCaptchaModel {
  String? base64Img;

  ImgCaptchaModel({
    this.base64Img,
  });

  factory ImgCaptchaModel.fromJson(Map<String, dynamic> json) {
    return ImgCaptchaModel(base64Img: json['base64Img']);
  }

  Map<String, dynamic> toJson() {
    return {
      'base64Img': base64Img,
    };
  }
}
