class FollowListModel {
  String? posterUid;
  String? nickname;
  String? headImg;

  FollowListModel({
    this.posterUid,
    this.nickname,
    this.headImg,
  });

  factory FollowListModel.fromJson(Map<String, dynamic> json) {
    return FollowListModel(
      posterUid: json['posterUid'] as String,
      nickname: json['nickname'] as String,
      headImg: json['headImg'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'posterUid': posterUid,
      'nickname': nickname,
      'headImg': headImg,
    };
  }
}
