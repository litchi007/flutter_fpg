class YuebaoModel {
  String? balance; // 账户余额
  String? giftBalance; // 体验金
  String? lastSettleTime; // 上次结算时间
  String? annualizedRate; // 年化率
  String? yuebaoName; // 余额宝名称
  String? todayProfit; // 今日收益
  String? weekProfit; // 本周收益
  String? monthProfit; // 本月收益
  String? totalProfit; // 总收益
  String? minTransferInMoney; // 最低转入金额
  String? maxTransferOutMoney; // 最高转出金额
  String? intro; // 说明

  YuebaoModel({
    this.balance,
    this.giftBalance,
    this.lastSettleTime,
    this.annualizedRate,
    this.yuebaoName,
    this.todayProfit,
    this.weekProfit,
    this.monthProfit,
    this.totalProfit,
    this.minTransferInMoney,
    this.maxTransferOutMoney,
    this.intro,
  });

  // Factory method to create YuebaoModel object from JSON
  factory YuebaoModel.fromJson(Map<String, dynamic> json) {
    return YuebaoModel(
      balance: json['balance'] as String?,
      giftBalance: json['giftBalance'] as String?,
      lastSettleTime: json['lastSettleTime'] as String?,
      annualizedRate: json['annualizedRate'] as String?,
      yuebaoName: json['yuebaoName'] as String?,
      todayProfit: json['todayProfit'] as String?,
      weekProfit: json['weekProfit'] as String?,
      monthProfit: json['monthProfit'] as String?,
      totalProfit: json['totalProfit'] as String?,
      minTransferInMoney: json['minTransferInMoney'] as String?,
      maxTransferOutMoney: json['maxTransferOutMoney'] as String?,
      intro: json['intro'] as String?,
    );
  }

  // Convert YuebaoModel object to JSON
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (balance != null) data['balance'] = balance;
    if (giftBalance != null) data['giftBalance'] = giftBalance;
    if (lastSettleTime != null) data['lastSettleTime'] = lastSettleTime;
    if (annualizedRate != null) data['annualizedRate'] = annualizedRate;
    if (yuebaoName != null) data['yuebaoName'] = yuebaoName;
    if (todayProfit != null) data['todayProfit'] = todayProfit;
    if (weekProfit != null) data['weekProfit'] = weekProfit;
    if (monthProfit != null) data['monthProfit'] = monthProfit;
    if (totalProfit != null) data['totalProfit'] = totalProfit;
    if (minTransferInMoney != null)
      data['minTransferInMoney'] = minTransferInMoney;
    if (maxTransferOutMoney != null)
      data['maxTransferOutMoney'] = maxTransferOutMoney;
    if (intro != null) data['intro'] = intro;
    return data;
  }
}

// Dart (Flutter)
class ListForProfitReport {
  String? settleTime; // 结清时间
  String? settleBalance; // 结清余额
  String? profitAmount; // 收益金额
  String? balance; // 总金额

  ListForProfitReport({
    this.settleTime,
    this.settleBalance,
    this.profitAmount,
    this.balance,
  });

  // Named constructor for JSON deserialization
  factory ListForProfitReport.fromJson(Map<String, dynamic> json) {
    return ListForProfitReport(
      settleTime: json['settleTime'],
      settleBalance: json['settleBalance'],
      profitAmount: json['profitAmount'],
      balance: json['balance'],
    );
  }

  // Convert ListData to JSON
  Map<String, dynamic> toJson() => {
        'settleTime': settleTime,
        'settleBalance': settleBalance,
        'profitAmount': profitAmount,
        'balance': balance,
      };
}

class ProfitReportData {
  int total;
  List<ListForProfitReport>? list;

  ProfitReportData({
    required this.total,
    this.list,
  });

  // Named constructor for JSON deserialization
  factory ProfitReportData.fromJson(Map<String, dynamic> json) {
    return ProfitReportData(
      total: json['total'],
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => ListForProfitReport.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }

  // Convert Data to JSON
  Map<String, dynamic> toJson() => {
        'total': total,
        'list': list?.map((item) => item.toJson()).toList(),
      };
}

class YueBaoTransRecordList {
  String amount; // 操作金额
  String newBalance; // 新金额
  String oldBalance; // 旧金额
  String changeTime; // 操作时间
  String category; // 类型

  YueBaoTransRecordList({
    required this.amount,
    required this.newBalance,
    required this.oldBalance,
    required this.changeTime,
    required this.category,
  });

  factory YueBaoTransRecordList.fromJson(Map<String, dynamic> json) =>
      YueBaoTransRecordList(
        amount: json['amount'],
        newBalance: json['newBalance'],
        oldBalance: json['oldBalance'],
        changeTime: json['changeTime'],
        category: json['category'],
      );

  Map<String, dynamic> toJson() => {
        'amount': amount,
        'newBalance': newBalance,
        'oldBalance': oldBalance,
        'changeTime': changeTime,
        'category': category,
      };
}

class YueBaoTransData {
  int total;
  List<YueBaoTransRecordList> list;
  Map<String, String>
      type; // {"charge":"余额转出","withdraw":"转入余额","settle":"收益结算","doyout":"DOY转出","indoy":"转入DOY","usdtout":"USDT转出","inusdt":"转入USDT"}

  YueBaoTransData({
    required this.total,
    required this.list,
    required this.type,
  });

  factory YueBaoTransData.fromJson(Map<String, dynamic> json) {
    // Convert type map from JSON
    Map<String, String> typeMap = Map<String, String>.from(json['type']);

    // Convert list of YueBaoTransRecordList from JSON
    List<YueBaoTransRecordList> transList = (json['list'] as List)
        .map((item) => YueBaoTransRecordList.fromJson(item))
        .toList();

    return YueBaoTransData(
      total: json['total'],
      list: transList,
      type: typeMap,
    );
  }

  Map<String, dynamic> toJson() => {
        'total': total,
        'list': list.map((trans) => trans.toJson()).toList(),
        'type': type,
      };
}

class ConverRateModel {
  String? from;
  String? to;
  String? rate;

  ConverRateModel({
    this.from,
    this.to,
    this.rate,
  });

  factory ConverRateModel.fromJson(Map<String, dynamic> json) {
    return ConverRateModel(
      from: json['from'],
      to: json['to'],
      rate: json['rate'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (from != null) data['from'] = from;
    if (to != null) data['to'] = to;
    if (rate != null) data['rate'] = rate;
    return data;
  }
}

class IMiddleMenuItem {
  final String title;
  final String id;
  final String type;

  IMiddleMenuItem({required this.title, required this.id, required this.type});
}
