class ScratchLog {
  String? id;
  String? aid;
  String? amount;
  String? status;
  String? addTime;
  String? openTime;
  String? uid;
  String? prizeName;
  String? dml;
  String? updateDate;
  String? pid;
  String? username;

  ScratchLog({
    this.id,
    this.aid,
    this.amount,
    this.status,
    this.addTime,
    this.openTime,
    this.uid,
    this.prizeName,
    this.dml,
    this.updateDate,
    this.pid,
    this.username,
  });

  factory ScratchLog.fromJson(Map<String, dynamic> json) {
    return ScratchLog(
      id: json['id']?.toString(),
      aid: json['aid']?.toString(),
      amount: json['amount']?.toString(),
      status: json['status']?.toString(),
      addTime: json['add_time']?.toString(),
      openTime: json['open_time']?.toString(),
      uid: json['uid']?.toString(),
      prizeName: json['prize_name']?.toString(),
      dml: json['dml']?.toString(),
      updateDate: json['update_date']?.toString(),
      pid: json['pid']?.toString(),
      username: json['username']?.toString(),
    );
  }
}

class PrizeData {
  String? prizeShowType;
  List<ScratchLog>? scratchLog;

  PrizeData({
    this.prizeShowType,
    this.scratchLog,
  });

  factory PrizeData.fromJson(Map<String, dynamic> json) {
    var list = json['scratchLog'] as List?;
    List<ScratchLog>? scratchLogList =
        list?.map((i) => ScratchLog.fromJson(i)).toList();

    return PrizeData(
      prizeShowType: json['prizeShowType'],
      scratchLog: scratchLogList,
    );
  }
}
