class ActivitySettingsModel {
  String? goldenEggLogo; // 砸金蛋图标
  String? redBagLogo; // 红包图标
  String? redBagSkin; // 红包皮肤
  String? redBagIntro; // 红包信息
  String? scratchOffLogo; // 刮刮乐图标
  String? turntableLogo; // 大转盘图标

  ActivitySettingsModel({
    this.goldenEggLogo,
    this.redBagIntro,
    this.redBagLogo,
    this.redBagSkin,
    this.scratchOffLogo,
    this.turntableLogo,
  });

  factory ActivitySettingsModel.fromJson(Map<String, dynamic> json) {
    return ActivitySettingsModel(
      goldenEggLogo: json['goldenEggLogo'],
      redBagLogo: json['redBagLogo'],
      redBagSkin: json['redBagSkin'],
      redBagIntro: json['redBagIntro'],
      scratchOffLogo: json['scratchOffLogo'],
      turntableLogo: json['turntableLogo'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'redBagLogo': redBagLogo,
      'redBagSkin': redBagSkin,
      'redBagIntro': redBagIntro,
      'turntableLogo': turntableLogo,
      'scratchOffLogo': scratchOffLogo,
      'goldenEggLogo': goldenEggLogo,
    };
  }
}
