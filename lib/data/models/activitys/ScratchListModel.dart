class ScratchPrize {
  final String? scratchPrizeId;
  final String? sendTimes;
  final String? scratchPrizeName;
  final String? scratchCondition;
  final String? scratchTimes;
  final String? scratchGive1;
  final String? scratchGive2;
  final String? scratchDmlWithRech;
  final String? rechargeCoinLevels;
  final String? scratchGiveType;

  ScratchPrize({
    this.scratchPrizeId,
    this.sendTimes,
    this.scratchPrizeName,
    this.scratchCondition,
    this.scratchTimes,
    this.scratchGive1,
    this.scratchGive2,
    this.scratchDmlWithRech,
    this.rechargeCoinLevels,
    this.scratchGiveType,
  });

  factory ScratchPrize.fromJson(Map<String, dynamic> json) {
    return ScratchPrize(
      scratchPrizeId: json['scratchPrizeId']?.toString(),
      sendTimes: json['sendTimes'],
      scratchPrizeName: json['scratchPrizeName'],
      scratchCondition: json['scratchCondition'],
      scratchTimes: json['scratchTimes'],
      scratchGive1: json['scratchGive1'],
      scratchGive2: json['scratchGive2'],
      scratchDmlWithRech: json['scratchDmlWithRech'],
      rechargeCoinLevels: json['recharge_coin_levels'],
      scratchGiveType: json['scratchGiveType']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'scratchPrizeId': scratchPrizeId,
      'sendTimes': sendTimes,
      'scratchPrizeName': scratchPrizeName,
      'scratchCondition': scratchCondition,
      'scratchTimes': scratchTimes,
      'scratchGive1': scratchGive1,
      'scratchGive2': scratchGive2,
      'scratchDmlWithRech': scratchDmlWithRech,
      'recharge_coin_levels': rechargeCoinLevels,
      'scratchGiveType': scratchGiveType,
    };
  }
}

class ScratchList {
  final String? id;
  final ScratchParam? param;
  final String? type;
  final String? start;
  final String? end;
  final String? showType;

  ScratchList({
    this.id,
    this.param,
    this.type,
    this.start,
    this.end,
    this.showType,
  });

  factory ScratchList.fromJson(Map<String, dynamic> json) {
    return ScratchList(
      id: json['id']?.toString(),
      param:
          json['param'] != null ? ScratchParam.fromJson(json['param']) : null,
      type: json['type']?.toString(),
      start: json['start']?.toString(),
      end: json['end']?.toString(),
      showType: json['showType']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'param': param?.toJson(),
      'type': type,
      'start': start,
      'end': end,
      'showType': showType,
    };
  }
}

class ScratchParam {
  final String? scrathModel;
  final String? scrathSendType;
  final String? checkInUserLevels;
  final String? activityShow;
  final String? visitorShow;
  final String? prizeShow;
  final String? prizeShowReal;
  final String? prizeShowVirtua;
  final String? prizeAmountRealShow;
  final String? prizeAmountVirtuaShow1;
  final String? prizeAmountVirtuaShow2;
  final String? rechargeBanks;
  final String? rechargeCoinLevels;
  final String? contentScratch;
  final String? timeBetweenStart;
  final String? timeBetweenEnd;
  final String? adminRecharge;
  final List<ScratchPrize>? scratchPrizeArr;
  final List<String>? contentTurntable;

  ScratchParam({
    this.scrathModel,
    this.scrathSendType,
    this.checkInUserLevels,
    this.activityShow,
    this.visitorShow,
    this.prizeShow,
    this.prizeShowReal,
    this.prizeShowVirtua,
    this.prizeAmountRealShow,
    this.prizeAmountVirtuaShow1,
    this.prizeAmountVirtuaShow2,
    this.rechargeBanks,
    this.rechargeCoinLevels,
    this.contentScratch,
    this.timeBetweenStart,
    this.timeBetweenEnd,
    this.adminRecharge,
    this.scratchPrizeArr,
    this.contentTurntable,
  });

  factory ScratchParam.fromJson(Map<String, dynamic> json) {
    var prizeArrJson = json['scratchPrizeArr'] as List? ?? [];
    var prizeArr = prizeArrJson.map((i) => ScratchPrize.fromJson(i)).toList();

    return ScratchParam(
      scrathModel: json['scrath_model'],
      scrathSendType: json['scrath_send_type'],
      checkInUserLevels: json['check_in_user_levels'],
      activityShow: json['activity_show']?.toString(),
      visitorShow: json['visitor_show'],
      prizeShow: json['prizeShow'],
      prizeShowReal: json['prizeShowReal'],
      prizeShowVirtua: json['prizeShowVirtua'],
      prizeAmountRealShow: json['prizeAmountRealShow'],
      prizeAmountVirtuaShow1: json['prizeAmountVirtuaShow1'],
      prizeAmountVirtuaShow2: json['prizeAmountVirtuaShow2'],
      rechargeBanks: json['recharge_banks'],
      rechargeCoinLevels: json['recharge_coin_levels'],
      contentScratch: json['content_scratch'],
      timeBetweenStart: json['time_between_start'],
      timeBetweenEnd: json['time_between_end'],
      adminRecharge: json['admin_recharge']?.toString(),
      scratchPrizeArr: prizeArr,
      contentTurntable: (json['content_turntable'] as List?)
          ?.map((i) => i as String)
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'scrath_model': scrathModel,
      'scrath_send_type': scrathSendType,
      'check_in_user_levels': checkInUserLevels,
      'activity_show': activityShow,
      'visitor_show': visitorShow,
      'prizeShow': prizeShow,
      'prizeShowReal': prizeShowReal,
      'prizeShowVirtua': prizeShowVirtua,
      'prizeAmountRealShow': prizeAmountRealShow,
      'prizeAmountVirtuaShow1': prizeAmountVirtuaShow1,
      'prizeAmountVirtuaShow2': prizeAmountVirtuaShow2,
      'recharge_banks': rechargeBanks,
      'recharge_coin_levels': rechargeCoinLevels,
      'content_scratch': contentScratch,
      'time_between_start': timeBetweenStart,
      'time_between_end': timeBetweenEnd,
      'admin_recharge': adminRecharge,
      'scratchPrizeArr': scratchPrizeArr?.map((i) => i.toJson()).toList(),
      'content_turntable': contentTurntable,
    };
  }
}

class ScratchListModel {
  final List<ScratchWinList>? scratchWinList;
  final List<ScratchList>? scratchList;

  ScratchListModel({
    this.scratchWinList,
    this.scratchList,
  });

  factory ScratchListModel.fromJson(Map<String, dynamic> json) {
    return ScratchListModel(
      scratchWinList: (json['scratchWinList'] as List?)
          ?.map((e) => ScratchWinList.fromJson(e))
          .toList(),
      scratchList: (json['scratchList'] as List?)
          ?.map((e) => ScratchList.fromJson(e))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'scratchWinList': scratchWinList?.map((e) => e.toJson()).toList(),
      'scratchList': scratchList?.map((e) => e.toJson()).toList(),
    };
  }
}

class ScratchWinList {
  String? amount;
  String? id;
  String? addTime;
  String? prizeName;
  String? aid;
  ScratchWinList({
    this.addTime,
    this.aid,
    this.amount,
    this.prizeName,
    this.id,
  });
  factory ScratchWinList.fromJson(Map<String, dynamic> json) {
    return ScratchWinList(
      addTime: json['add_time']?.toString(),
      aid: json['aid']?.toString(),
      amount: json['amount']?.toString(),
      prizeName: json['prize_name']?.toString(),
      id: json['id']?.toString(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'add_time': addTime,
      'aid': aid,
      'amount': amount,
      'prize_name': prizeName,
      'id': id,
    };
  }
}
