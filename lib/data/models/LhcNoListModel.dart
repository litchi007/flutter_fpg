import 'package:fpg_flutter/data/models/LhcNoListItemModel.dart';

class LhcNoListModel {
  String? title;
  List<LhcNoListItemModel>? list;

  LhcNoListModel({
    this.title,
    this.list,
  });

  factory LhcNoListModel.fromJson(Map<String, dynamic> json) {
    return LhcNoListModel(
      title: json['title'],
      list: (json['list'] as List<dynamic>)
          .map((item) => LhcNoListItemModel.fromJson(item))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'list': list?.map((item) => item.toJson()).toList(),
    };
  }
}
