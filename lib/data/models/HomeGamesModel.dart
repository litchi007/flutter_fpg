class NavModel {
  String? id;
  String? icon;
  String? name;
  String? url;
  String? category;
  String? levelType;
  String? sort;
  String? seriesId;
  dynamic subId;
  String? tipFlag;
  String? openWay;
  String? hotIcon;
  String? gameCode;
  String? subtitle;
  String? title;
  dynamic gameId;

  NavModel({
    this.id,
    this.icon,
    this.name,
    this.url,
    this.category,
    this.levelType,
    this.sort,
    this.seriesId,
    this.subId,
    this.tipFlag,
    this.openWay,
    this.hotIcon,
    this.gameCode,
    this.subtitle,
    this.title,
    this.gameId,
  });

  factory NavModel.fromJson(Map<String, dynamic> json) {
    return NavModel(
      id: json['id'],
      icon: json['icon'],
      name: json['name'],
      url: json['url'],
      category: json['category'],
      levelType: json['levelType'],
      sort: json['sort'],
      seriesId: json['seriesId'],
      subId: json['subId'],
      tipFlag: json['tipFlag'],
      openWay: json['openWay'],
      hotIcon: json['hotIcon'],
      gameCode: json['gameCode'],
      subtitle: json['subtitle'],
      title: json['title'],
      gameId: json['gameId'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'icon': icon,
      'name': name,
      'url': url,
      'category': category,
      'levelType': levelType,
      'sort': sort,
      'seriesId': seriesId,
      'subId': subId,
      'tipFlag': tipFlag,
      'openWay': openWay,
      'hotIcon': hotIcon,
      'gameCode': gameCode,
      'subtitle': subtitle,
      'title': title,
      'gameId': gameId,
    };
  }
}

class IconModel {
  final String? id;
  final String? name;
  final String? style;
  final String? logo;
  final List<GameModel>? list;
  final String? imageHot;
  final String? url;

  IconModel({
    this.id,
    this.name,
    this.style,
    this.logo,
    this.list,
    this.imageHot,
    this.url,
  });

  factory IconModel.fromJson(Map<String, dynamic> json) {
    List<dynamic>? listData = json['list'];
    List<GameModel> listModels = [];
    if (listData != null) {
      listModels = listData.map((item) => GameModel.fromJson(item)).toList();
    }
    return IconModel(
      id: json['id'],
      name: json['name'],
      style: json['style'],
      logo: json['logo'],
      list: listModels,
      imageHot: json['image_hot'],
      url: json['url'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'style': style,
      'logo': logo,
      'list': list?.map((item) => item.toJson()).toList(),
      'image_hot': imageHot,
      'url': url,
    };
  }
}

class GameModel {
  String? id;
  String? icon;
  String? name;
  String? url;
  String? category;
  String? levelType;
  String? sort;
  String? seriesId;
  dynamic subId;
  String? tipFlag; //1热门，2活动，3大奖，4中大奖
  String? openWay;
  String? hotIcon;
  String? gameCode;
  String? subtitle;
  List<SubTypeModel>? subType;
  dynamic gameId;
  String? realName;
  String? title;
  String? desc;
  String? type;
  String? adminUid;
  String? enable;
  String? headadd;
  String? footadd;
  String? domain;
  int? docType;
  String? gameType;
  String? logo;
  String? isInstant;
  String? isSeal;
  String? isClose;
  dynamic supportTrial;

  /// 是否支持试玩。1=是；0=否
  int? isPopup;

  /// 是否弹窗。1=是；0=否（1表示有下级菜单）
  int? isHot;

  /// 是否热门。1=是；0=否
  String? threadType;
  dynamic link;
  String? readPri;
  int? random; //自定义参数
  String? isHotStr;
  String? model;
  dynamic alias;
  String? appLink;
  dynamic isMaintained;

  GameModel({
    this.id,
    this.icon,
    this.name,
    this.url,
    this.category,
    this.levelType,
    this.sort,
    this.seriesId,
    this.subId,
    this.tipFlag,
    this.openWay,
    this.hotIcon,
    this.gameCode,
    this.subtitle,
    this.subType,
    this.gameId,
    this.realName,
    this.title,
    this.desc,
    this.type,
    this.adminUid,
    this.enable,
    this.headadd,
    this.footadd,
    this.domain,
    this.docType,
    this.gameType,
    this.logo,
    this.isInstant,
    this.isSeal,
    this.isClose,
    this.supportTrial,
    this.isPopup,
    this.isHot,
    this.threadType,
    this.link,
    this.readPri,
    this.random,
    this.isHotStr,
    this.model,
    this.alias,
    this.appLink,
    this.isMaintained,
  });

  factory GameModel.fromJson(Map<String, dynamic> json) {
    var subTypeList = (json['subType'] as List?)
        ?.map((subTypeJson) => SubTypeModel.fromJson(subTypeJson))
        .toList();

    return GameModel(
      id: json['id'],
      icon: json['icon'],
      name: json['name'],
      url: json['url'],
      category: json['category'],
      levelType: json['levelType'],
      sort: json['sort'],
      seriesId: json['seriesId'],
      subId: json['subId'],
      tipFlag: json['tipFlag'],
      openWay: json['openWay'],
      hotIcon: json['hotIcon'],
      gameCode: json['gameCode'],
      subtitle: json['subtitle'],
      subType: subTypeList,
      gameId: json['gameId'],
      realName: json['realName'],
      title: json['title'],
      desc: json['desc'],
      type: json['type'],
      adminUid: json['admin_uid'],
      enable: json['enable'],
      headadd: json['headadd'],
      footadd: json['footadd'],
      domain: json['domain'],
      docType: json['docType'],
      gameType: json['gameType'],
      logo: json['logo'],
      isInstant: json['isInstant'],
      isSeal: json['isSeal'],
      isClose: json['isClose'],
      supportTrial: json['supportTrial'],
      isPopup: json['isPopup'],
      isHot: json['isHot'],
      threadType: json['thread_type'],
      link: json['link'],
      readPri: json['read_pri'],
      random: json['random'],
      isHotStr: json['is_hot'],
      model: json['model'],
      alias: json['alias'],
      appLink: json['appLink'],
      isMaintained: json['isMaintained'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'icon': icon,
      'name': name,
      'url': url,
      'category': category,
      'levelType': levelType,
      'sort': sort,
      'seriesId': seriesId,
      'subId': subId,
      'tipFlag': tipFlag,
      'openWay': openWay,
      'hotIcon': hotIcon,
      'gameCode': gameCode,
      'subtitle': subtitle,
      'subType': subType?.map((e) => e.toJson()).toList(),
      'gameId': gameId,
      'realName': realName,
      'title': title,
      'desc': desc,
      'type': type,
      'admin_uid': adminUid,
      'enable': enable,
      'headadd': headadd,
      'footadd': footadd,
      'domain': domain,
      'docType': docType,
      'gameType': gameType,
      'logo': logo,
      'isInstant': isInstant,
      'isSeal': isSeal,
      'isClose': isClose,
      'supportTrial': supportTrial,
      'isPopup': isPopup,
      'isHot': isHot,
      'thread_type': threadType,
      'link': link,
      'read_pri': readPri,
      'random': random,
      'is_hot': isHotStr,
      'model': model,
      'alias': alias,
      'appLink': appLink,
      'isMaintained': isMaintained,
    };
  }
}

class SubTypeModel {
  // Define the structure of SubType here based on your requirements
  // For example, assuming SubType has an id and value
  String? id;
  String? value;

  SubTypeModel({
    this.id,
    this.value,
  });

  factory SubTypeModel.fromJson(Map<String, dynamic> json) {
    return SubTypeModel(
      id: json['id'],
      value: json['value'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'value': value,
    };
  }
}

class HomeGamesData {
  List<IconModel>? icons;
  List<NavModel>? navs;

  HomeGamesData({
    this.icons,
    this.navs,
  });

  factory HomeGamesData.fromJson(Map<String, dynamic> json) {
    List<dynamic>? iconsData = json['icons'];
    List<IconModel> icons = [];
    if (iconsData != null) {
      icons = iconsData.map((item) => IconModel.fromJson(item)).toList();
    }

    List<dynamic>? navsData = json['navs'];
    List<NavModel> navs = [];
    if (navsData != null) {
      navs = navsData.map((item) => NavModel.fromJson(item)).toList();
    }

    return HomeGamesData(
      icons: icons,
      navs: navs,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'icons': icons?.map((item) => item.toJson()).toList(),
      'navs': navs?.map((item) => item.toJson()).toList(),
    };
  }
}
