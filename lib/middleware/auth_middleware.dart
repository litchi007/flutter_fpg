import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

//// 路由拦截器
/// [AuthMiddleWare]
class AuthMiddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    Get.log(route!);
    if (GlobalService.to.token.isEmpty) {
      return RouteSettings(name: AppRoutes.login);
    }
    return null;
  }
}

class AccountMiddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    Get.log(route!);
    if (GlobalService.to.userInfo.value?.isTest ?? false) {
      // return RouteSettings(name: AppRoutes.login);
      AppToast.show(msg: "不支持试玩请先登录");
    }
    return null;
  }
}
