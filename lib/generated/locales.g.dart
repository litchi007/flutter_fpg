// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'zh_CN': Locales.zh_CN,
    'en_US': Locales.en_US,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const followingSystem = 'followingSystem';
  static const cancel = 'cancel';
  static const dayModal = 'dayModal';
  static const nightModal = 'nightModal';
  static const hoursAgo = 'hoursAgo';
  static const minuteAgo = 'minuteAgo';
  static const dayAgo = 'dayAgo';
  static const yearAgo = 'yearAgo';
  static const monthAgo = 'monthAgo';
}

class Locales {
  static const zh_CN = {
    'followingSystem': '跟随系统',
    'cancel': '取消',
    'dayModal': '日间模式',
    'nightModal': '夜间模式',
    'hoursAgo': '小时前',
    'minuteAgo': '分钟前',
    'dayAgo': '天前',
    'yearAgo': '年前',
    'monthAgo': '月前',
  };
  static const en_US = {
    'followingSystem': 'Following system',
    'cancel': 'Cancel',
    'dayModal': 'Day Mode',
    'nightModal': 'Night Mode',
    'hoursAgo': 'hours ago',
    'minuteAgo': 'minutes ago',
    'dayAgo': 'days ago',
    'yearAgo': 'years ago',
    'monthAgo': 'months ago',
  };
}
