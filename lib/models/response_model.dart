import 'dart:convert';

import 'package:fpg_flutter/api/error_report/error_report.dart';
import 'package:fpg_flutter/constants/data_factories.dart';
import 'package:fpg_flutter/models/error_report/error_report.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

const JsonEncoder globalJsonEncoder = JsonEncoder.withIndent('  ');

typedef Json = Map<String, dynamic>;

class ModelError extends TypeError {}

abstract class DataModel {
  const DataModel();

  Json toJson();

  @override
  String toString() => globalJsonEncoder.convert(toJson());
}

class ModelMakeError<T> extends ModelError {
  ModelMakeError(this.json, this.error);

  final Json json;
  final Object error;
}

class ModelNotRegisteredError<T> extends ModelError {
  ModelNotRegisteredError();

  @override
  String toString() {
    return "You're inflating an unregistered type: $T\n"
        "Please check if it's registered in `dataModelFactories`.";
  }
}

T makeModel<T>(Json json) {
  // LogUtil.w('==== 🔥 Logoutput self: ${dataModelFactories}====');
  if (!dataModelFactories.containsKey(T)) {
    ErrorReportHttp.report(ErrorReport(
        text: "数据错误",
        err: "You're inflating an unregistered type: $T\n"
            "Please check if it's registered in `dataModelFactories`.",
        page: Get.currentRoute));
    LogUtil.e(
      "You're inflating an unregistered type: $T\n"
      "Please check if it's registered in `dataModelFactories`.",
      tag: '🏭 DataModel',
    );

    throw ModelNotRegisteredError<T>();
  }
  try {
    return dataModelFactories[T]!(json) as T;
  } catch (e, s) {
    ErrorReportHttp.report(ErrorReport(
      text: '[类型错误] Error when making model with $T type: $e\n'
          'The raw data which make this error is: '
          '${globalJsonEncoder.convert(json)}',
      err: s,
      page: Get.currentRoute,
    ));

    LogUtil.e(
      'Error when making model with $T type: $e\n'
      'The raw data which make this error is: '
      '${globalJsonEncoder.convert(json)}',
      stackTrace: s,
      tag: '🏭 DataModel',
    );
    throw ModelMakeError<T>(json, e);
  }
}

class ResponseModel<T> {
  const ResponseModel({
    required this.code,
    required this.msg,
    this.timestamp,
    this.rawData,
    this.data,
    this.page,
    this.total,
    this.canLoadMore = false,
    this.models,
    this.cursor,
  });

  const ResponseModel.succeed(
      {this.rawData,
      this.data,
      this.page,
      this.total,
      this.canLoadMore = false,
      this.models,
      this.cursor,
      this.timestamp})
      : code = codeSucceed,
        msg = '';

  const ResponseModel.failed(
      {required this.msg,
      this.rawData,
      this.data,
      this.page,
      this.total,
      this.canLoadMore = false,
      this.models,
      this.cursor,
      this.timestamp})
      : code = codeFailed;

  const ResponseModel.cancelled(
      {required this.msg,
      this.rawData,
      this.data,
      this.page,
      this.total,
      this.canLoadMore = false,
      this.models,
      this.cursor,
      this.timestamp})
      : code = codeCancelled;

  factory ResponseModel.fromJson(dynamic json,
      {bool Function(Json json)? modelFilter, dynamic datas}) {
    if (kProfileMode) {
      print("9999999 $json $datas");
    }
    List<T> makeModels(List<Json> list) {
      if (modelFilter != null) {
        return list.where(modelFilter).map(makeModel<T>).toList();
      }
      return list.map(makeModel<T>).toList();
    }

    final Object? data = json['data'];
    final bool hasData = data != null;
    final bool isModels = data is List;
    LogUtil.w("hasData____ ${data.runtimeType} $isModels ");
    final isBaseType =
        [String, bool, int, double, null].contains(data.runtimeType);
    final res = ResponseModel<T>(
      code: json['errCode'] ?? datas['code'] ?? codeSucceed,
      msg: json['errMsg'] ?? datas['msg'] ?? errorExternalRequest,
      data: hasData && !isModels
          ? isBaseType
              ? (data as T)
              : makeModel<T>(data as Json)
          : null,
      // data: makeModel<T>(res1),
      timestamp: datas['timestamp'] ?? DateTime.now().millisecondsSinceEpoch,
      rawData: data,
      models: hasData && isModels ? makeModels(data.cast<Json>()) : null,
    );
    return res;
  }

  ResponseModel<Y> replaceTypeData<Y extends T>({
    Y? data,
    List<Y>? models,
    int? total,
  }) {
    return ResponseModel<Y>(
      code: code,
      msg: msg,
      data: data,
      rawData: rawData,
      page: page,
      total: total,
      canLoadMore: canLoadMore,
      cursor: cursor,
      timestamp: timestamp,
    );
  }

  static const int codeSucceed = 200;
  static const int codeFailed = 1;
  static const int codeCancelled = -1;
  static const String errorInternalRequest = '_InternalRequestError';
  static const String errorExternalRequest = '_ExternalError';

  final int code;
  final String msg;
  final T? data;

  /// This is the raw data for the model.
  final dynamic rawData;
  final int? timestamp;

  /// Below fields only works when requesting a list of data.
  final int? page;
  final int? total;
  final bool canLoadMore;
  final List<T>? models;

  /// Tracking visit position.
  final String? cursor;

  bool get isSucceed => code == 200;

  bool get isFailed => code == codeFailed;

  bool get isCancelled => code == codeCancelled;

  ResponseModel<T> copyWith(
      {int? code,
      String? msg,
      String? rawData,
      T? data,
      int? page,
      int? total,
      bool? canLoadMore,
      List<T>? models,
      String? cursor,
      int? timestamp}) {
    return ResponseModel<T>(
        code: code ?? this.code,
        msg: msg ?? this.msg,
        data: data ?? this.data,
        rawData: data ?? this.rawData,
        page: page ?? this.page,
        total: total ?? this.total,
        canLoadMore: canLoadMore ?? this.canLoadMore,
        models: models ?? this.models,
        cursor: cursor ?? this.cursor,
        timestamp: timestamp ?? this.timestamp);
  }

  Json toJson() {
    return <String, dynamic>{
      'code': code,
      'msg': msg,
      'data': (data ?? rawData),
      'timestamp': timestamp ?? 0,
      if (page != null) 'cursor': total,
      if (total != null) 'count': total,
      if (canLoadMore) 'has_more': true,
      if (cursor != null) 'cursor': cursor,
    };
  }

  @override
  String toString() => const JsonEncoder.withIndent('  ').convert(toJson());
}
