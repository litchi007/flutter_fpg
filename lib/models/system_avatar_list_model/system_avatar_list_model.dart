import 'package:json_annotation/json_annotation.dart';

part 'system_avatar_list_model.g.dart';

@JsonSerializable()
class SystemAvatarListModel {
  final String? filename;
  final String? url;

  const SystemAvatarListModel({this.filename, this.url});

  @override
  String toString() {
    return 'SystemAvatarListModel(filename: $filename, url: $url)';
  }

  factory SystemAvatarListModel.fromJson(Map<String, dynamic> json) {
    return _$SystemAvatarListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SystemAvatarListModelToJson(this);

  SystemAvatarListModel copyWith({
    String? filename,
    String? url,
  }) {
    return SystemAvatarListModel(
      filename: filename ?? this.filename,
      url: url ?? this.url,
    );
  }
}
