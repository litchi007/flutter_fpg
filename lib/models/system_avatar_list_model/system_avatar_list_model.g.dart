// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'system_avatar_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SystemAvatarListModel _$SystemAvatarListModelFromJson(
        Map<String, dynamic> json) =>
    SystemAvatarListModel(
      filename: json['filename'] as String?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$SystemAvatarListModelToJson(
        SystemAvatarListModel instance) =>
    <String, dynamic>{
      'filename': instance.filename,
      'url': instance.url,
    };
