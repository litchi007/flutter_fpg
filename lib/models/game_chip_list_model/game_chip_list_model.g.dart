// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_chip_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameChipListModel _$GameChipListModelFromJson(Map<String, dynamic> json) =>
    GameChipListModel(
      chipSetting1: json['chipSetting1'] as String?,
      chipSetting2: json['chipSetting2'] as String?,
      chipSetting3: json['chipSetting3'] as String?,
      chipSetting4: json['chipSetting4'] as String?,
      chipSetting5: json['chipSetting5'] as String?,
      chipSetting6: json['chipSetting6'] as String?,
      chipSetting7: json['chipSetting7'] as String?,
    );

Map<String, dynamic> _$GameChipListModelToJson(GameChipListModel instance) =>
    <String, dynamic>{
      'chipSetting1': instance.chipSetting1,
      'chipSetting2': instance.chipSetting2,
      'chipSetting3': instance.chipSetting3,
      'chipSetting4': instance.chipSetting4,
      'chipSetting5': instance.chipSetting5,
      'chipSetting6': instance.chipSetting6,
      'chipSetting7': instance.chipSetting7,
    };
