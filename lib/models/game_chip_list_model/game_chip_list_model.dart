import 'package:json_annotation/json_annotation.dart';

part 'game_chip_list_model.g.dart';

@JsonSerializable()
class GameChipListModel {
  final String? chipSetting1;
  final String? chipSetting2;
  final String? chipSetting3;
  final String? chipSetting4;
  final String? chipSetting5;
  final String? chipSetting6;
  final String? chipSetting7;

  const GameChipListModel({
    this.chipSetting1,
    this.chipSetting2,
    this.chipSetting3,
    this.chipSetting4,
    this.chipSetting5,
    this.chipSetting6,
    this.chipSetting7,
  });

  @override
  String toString() {
    return 'GameChipListModel(chipSetting1: $chipSetting1, chipSetting2: $chipSetting2, chipSetting3: $chipSetting3, chipSetting4: $chipSetting4, chipSetting5: $chipSetting5, chipSetting6: $chipSetting6, chipSetting7: $chipSetting7)';
  }

  factory GameChipListModel.fromJson(Map<String, dynamic> json) {
    return _$GameChipListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GameChipListModelToJson(this);

  GameChipListModel copyWith({
    String? chipSetting1,
    String? chipSetting2,
    String? chipSetting3,
    String? chipSetting4,
    String? chipSetting5,
    String? chipSetting6,
    String? chipSetting7,
  }) {
    return GameChipListModel(
      chipSetting1: chipSetting1 ?? this.chipSetting1,
      chipSetting2: chipSetting2 ?? this.chipSetting2,
      chipSetting3: chipSetting3 ?? this.chipSetting3,
      chipSetting4: chipSetting4 ?? this.chipSetting4,
      chipSetting5: chipSetting5 ?? this.chipSetting5,
      chipSetting6: chipSetting6 ?? this.chipSetting6,
      chipSetting7: chipSetting7 ?? this.chipSetting7,
    );
  }
}
