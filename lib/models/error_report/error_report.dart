import 'package:json_annotation/json_annotation.dart';

part 'error_report.g.dart';

@JsonSerializable()
class ErrorReport {
  final dynamic err;
  final String? text;
  final String? page;

  const ErrorReport({this.err, this.text, this.page});

  @override
  String toString() => 'ErrorReport(err: $err, text: $text, page: $page)';

  factory ErrorReport.fromJson(Map<String, dynamic> json) {
    return _$ErrorReportFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ErrorReportToJson(this);

  ErrorReport copyWith({
    String? err,
    String? text,
    String? page,
  }) {
    return ErrorReport(
      err: err ?? this.err,
      text: text ?? this.text,
      page: page ?? this.page,
    );
  }
}
