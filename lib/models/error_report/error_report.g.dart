// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error_report.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ErrorReport _$ErrorReportFromJson(Map<String, dynamic> json) => ErrorReport(
      err: json['err'],
      text: json['text'] as String?,
      page: json['page'] as String?,
    );

Map<String, dynamic> _$ErrorReportToJson(ErrorReport instance) =>
    <String, dynamic>{
      'err': instance.err,
      'text': instance.text,
      'page': instance.page,
    };
