// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_ticket_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryTicketModel _$LotteryTicketModelFromJson(Map<String, dynamic> json) =>
    LotteryTicketModel(
      date: json['date'] as String?,
      dayOfWeek: json['dayOfWeek'] as String?,
      betCount: json['betCount'],
      betAmount: json['betAmount'],
      winCount: json['winCount'],
      winAmount: json['winAmount'],
      winLoseAmount: json['winLoseAmount'],
    );

Map<String, dynamic> _$LotteryTicketModelToJson(LotteryTicketModel instance) =>
    <String, dynamic>{
      'date': instance.date,
      'dayOfWeek': instance.dayOfWeek,
      'betCount': instance.betCount,
      'betAmount': instance.betAmount,
      'winCount': instance.winCount,
      'winAmount': instance.winAmount,
      'winLoseAmount': instance.winLoseAmount,
    };
