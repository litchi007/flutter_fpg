import 'package:json_annotation/json_annotation.dart';
import 'lottery_ticket_model.dart';
import 'dart:convert';
part 'lottery_tickets_model.g.dart';

@JsonSerializable()
class LotteryTicketsModel {
  final List<LotteryTicketModel>? tickets;

  const LotteryTicketsModel({
    this.tickets,
  });

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory LotteryTicketsModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryTicketsModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryTicketsModelToJson(this);

  LotteryTicketsModel copyWith({
    List<LotteryTicketModel>? list,
  }) {
    return LotteryTicketsModel(
      tickets: list ?? tickets,
    );
  }
}
