import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'lottery_ticket_model.g.dart';

// {
// "date": "2024-07-17",
// "dayOfWeek": "星期三",
// "betCount": "4",
// "betAmount": "4.00",
// "winCount": "2",
// "winAmount": "3.7700",
// "winLoseAmount": "-0.23"
// }

@JsonSerializable()
class LotteryTicketModel {
  final String? date;
  final String? dayOfWeek;
  final dynamic betCount;
  final dynamic betAmount;
  final dynamic winCount;
  final dynamic winAmount;
  final dynamic winLoseAmount;

  const LotteryTicketModel({
    this.date,
    this.dayOfWeek,
    this.betCount,
    this.betAmount,
    this.winCount,
    this.winAmount,
    this.winLoseAmount,
  });

  factory LotteryTicketModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryTicketModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryTicketModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  LotteryTicketModel copyWith(
      {String? date,
      String? dayOfWeek,
      String? betCount,
      String? betAmount,
      String? winCount,
      String? winAmount,
      String? winLoseAmount}) {
    return LotteryTicketModel(
      date: date ?? this.date,
      dayOfWeek: dayOfWeek ?? this.dayOfWeek,
      betCount: betCount ?? this.betCount,
      betAmount: betAmount ?? this.betAmount,
      winCount: winCount ?? this.winCount,
      winAmount: winAmount ?? this.winAmount,
      winLoseAmount: winLoseAmount ?? this.winLoseAmount,
    );
  }
}
