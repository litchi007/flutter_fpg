import 'dart:convert';

// @JsonSerializable()
class LotteryDateModel {
  int? dateId;
  String? dateStr;

  LotteryDateModel({
    this.dateId,
    this.dateStr,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'dateId': dateId,
        'dateStr': dateStr,
      };

  @override
  String toString() {
    return jsonEncode(this);
  }
}
