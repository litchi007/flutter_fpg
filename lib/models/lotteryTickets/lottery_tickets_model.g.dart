// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_tickets_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryTicketsModel _$LotteryTicketsModelFromJson(Map<String, dynamic> json) =>
    LotteryTicketsModel(
      tickets: (json['tickets'] as List<dynamic>?)
          ?.map((e) => LotteryTicketModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LotteryTicketsModelToJson(
        LotteryTicketsModel instance) =>
    <String, dynamic>{
      'tickets': instance.tickets,
    };
