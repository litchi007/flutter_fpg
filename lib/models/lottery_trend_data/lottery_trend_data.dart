import 'dart:convert';

class TrendData {
  // Average omission
  final List<String>? averageOmission;

  // Maximum connection
  final List<String>? maximumConnection;

  // Maximum omission
  final List<String>? maximumOmission;

  // Total occurrences
  final List<String>? totalTimes;

  // Data
  final List<List<dynamic>>? data;

  // Position array
  final List<Map<String, double>>? positionArr;

  // Header
  final List<String>? header;

  TrendData({
    this.averageOmission,
    this.maximumConnection,
    this.maximumOmission,
    this.totalTimes,
    this.data,
    this.positionArr,
    this.header,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = <String, dynamic>{};
    json['averageOmission'] = averageOmission;
    json['maximumConnection'] = maximumConnection;
    json['maximumOmission'] = maximumOmission;
    json['totalTimes'] = totalTimes;
    json['data'] = data;
    json['positionArr'] = positionArr;
    json['header'] = header;
    return json;
  }

  @override
  String toString() {
    return jsonEncode(this);
  }
}
