// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_road_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryRoadDataModel _$LotteryRoadDataModelFromJson(
        Map<String, dynamic> json) =>
    LotteryRoadDataModel(
      numberList1: (json['nums_1'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      sizeList1: (json['size_1'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      singleDoubleList1: (json['firstsd_1'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      numberList2: (json['nums_2'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      sizeList2: (json['size_2'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      singleDoubleList2: (json['firstsd_2'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      numberList3: (json['nums_3'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      sizeList3: (json['size_3'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      singleDoubleList3: (json['firstsd_3'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      numberList4: (json['nums_4'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      sizeList4: (json['size_4'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      singleDoubleList4: (json['firstsd_4'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      numberList5: (json['nums_5'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      sizeList5: (json['size_5'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      singleDoubleList5: (json['firstsd_5'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      dragonTigerList: (json['lhh'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      totalSizeList: (json['total_size'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      totalSingleDoubleList: (json['total_firstsd'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
    );

Map<String, dynamic> _$LotteryRoadDataModelToJson(
        LotteryRoadDataModel instance) =>
    <String, dynamic>{
      'nums_1': instance.numberList1,
      'size_1': instance.sizeList1,
      'firstsd_1': instance.singleDoubleList1,
      'nums_2': instance.numberList2,
      'size_2': instance.sizeList2,
      'firstsd_2': instance.singleDoubleList2,
      'nums_3': instance.numberList3,
      'size_3': instance.sizeList3,
      'firstsd_3': instance.singleDoubleList3,
      'nums_4': instance.numberList4,
      'size_4': instance.sizeList4,
      'firstsd_4': instance.singleDoubleList4,
      'nums_5': instance.numberList5,
      'size_5': instance.sizeList5,
      'firstsd_5': instance.singleDoubleList5,
      'lhh': instance.dragonTigerList,
      'total_size': instance.totalSizeList,
      'total_firstsd': instance.totalSingleDoubleList,
    };
