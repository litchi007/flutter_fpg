import 'package:dartx/dartx.dart';
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'lottery_road_data_model.g.dart';

@JsonSerializable()
class LotteryRoadDataModel {
  @JsonKey(name: 'nums_1')
  final List<List<String>>? numberList1;
  @JsonKey(name: 'size_1')
  final List<List<String>>? sizeList1;
  @JsonKey(name: 'firstsd_1')
  final List<List<String>>? singleDoubleList1;

  @JsonKey(name: 'nums_2')
  final List<List<String>>? numberList2;
  @JsonKey(name: 'size_2')
  final List<List<String>>? sizeList2;
  @JsonKey(name: 'firstsd_2')
  final List<List<String>>? singleDoubleList2;

  @JsonKey(name: 'nums_3')
  final List<List<String>>? numberList3;
  @JsonKey(name: 'size_3')
  final List<List<String>>? sizeList3;
  @JsonKey(name: 'firstsd_3')
  final List<List<String>>? singleDoubleList3;

  @JsonKey(name: 'nums_4')
  final List<List<String>>? numberList4;
  @JsonKey(name: 'size_4')
  final List<List<String>>? sizeList4;
  @JsonKey(name: 'firstsd_4')
  final List<List<String>>? singleDoubleList4;

  @JsonKey(name: 'nums_5')
  final List<List<String>>? numberList5;
  @JsonKey(name: 'size_5')
  final List<List<String>>? sizeList5;
  @JsonKey(name: 'firstsd_5')
  final List<List<String>>? singleDoubleList5;

  @JsonKey(name: 'lhh')
  final List<List<String>>? dragonTigerList;
  @JsonKey(name: 'total_size')
  final List<List<String>>? totalSizeList;
  @JsonKey(name: 'total_firstsd')
  final List<List<String>>? totalSingleDoubleList;

  const LotteryRoadDataModel({
    this.numberList1,
    this.sizeList1,
    this.singleDoubleList1,
    this.numberList2,
    this.sizeList2,
    this.singleDoubleList2,
    this.numberList3,
    this.sizeList3,
    this.singleDoubleList3,
    this.numberList4,
    this.sizeList4,
    this.singleDoubleList4,
    this.numberList5,
    this.sizeList5,
    this.singleDoubleList5,
    this.dragonTigerList,
    this.totalSizeList,
    this.totalSingleDoubleList,
  });

  @override
  String toString() => jsonEncode(this);

  factory LotteryRoadDataModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryRoadDataModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryRoadDataModelToJson(this);

  LotteryRoadDataModel copyWith({
    List<List<String>>? numberList1,
    List<List<String>>? sizeList1,
    List<List<String>>? singleDoubleList1,
    List<List<String>>? numberList2,
    List<List<String>>? sizeList2,
    List<List<String>>? singleDoubleList2,
    List<List<String>>? numberList3,
    List<List<String>>? sizeList3,
    List<List<String>>? singleDoubleList3,
    List<List<String>>? numberList4,
    List<List<String>>? sizeList4,
    List<List<String>>? singleDoubleList4,
    List<List<String>>? numberList5,
    List<List<String>>? sizeList5,
    List<List<String>>? singleDoubleList5,
    List<List<String>>? dragonTigerList,
    List<List<String>>? totalSizeList,
    List<List<String>>? totalSingleDoubleList,
  }) {
    return LotteryRoadDataModel(
      numberList1: numberList1 ?? this.numberList1,
      sizeList1: sizeList1 ?? this.sizeList1,
      singleDoubleList1: singleDoubleList1 ?? this.singleDoubleList1,
      numberList2: numberList2 ?? this.numberList2,
      sizeList2: sizeList2 ?? this.sizeList2,
      singleDoubleList2: singleDoubleList2 ?? this.singleDoubleList2,
      numberList3: numberList3 ?? this.numberList3,
      sizeList3: sizeList3 ?? this.sizeList3,
      singleDoubleList3: singleDoubleList3 ?? this.singleDoubleList3,
      numberList4: numberList4 ?? this.numberList4,
      sizeList4: sizeList4 ?? this.sizeList4,
      singleDoubleList4: singleDoubleList4 ?? this.singleDoubleList4,
      numberList5: numberList5 ?? this.numberList5,
      sizeList5: sizeList5 ?? this.sizeList5,
      singleDoubleList5: singleDoubleList5 ?? this.singleDoubleList5,
      dragonTigerList: dragonTigerList ?? this.dragonTigerList,
      totalSizeList: totalSizeList ?? this.totalSizeList,
      totalSingleDoubleList:
          totalSingleDoubleList ?? this.totalSingleDoubleList,
    );
  }
}
