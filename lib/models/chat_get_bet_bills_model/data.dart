import 'package:json_annotation/json_annotation.dart';

part 'data.g.dart';

@JsonSerializable()
class Data {
  final double? totalResultMoney;
  final int? totalBetMoney;
  final double? totalBonus;
  final int? userCount;
  final int? totalAmount;

  const Data({
    this.totalResultMoney,
    this.totalBetMoney,
    this.totalBonus,
    this.userCount,
    this.totalAmount,
  });

  @override
  String toString() {
    return 'Data(totalResultMoney: $totalResultMoney, totalBetMoney: $totalBetMoney, totalBonus: $totalBonus, userCount: $userCount, totalAmount: $totalAmount)';
  }

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);

  Data copyWith({
    double? totalResultMoney,
    int? totalBetMoney,
    double? totalBonus,
    int? userCount,
    int? totalAmount,
  }) {
    return Data(
      totalResultMoney: totalResultMoney ?? this.totalResultMoney,
      totalBetMoney: totalBetMoney ?? this.totalBetMoney,
      totalBonus: totalBonus ?? this.totalBonus,
      userCount: userCount ?? this.userCount,
      totalAmount: totalAmount ?? this.totalAmount,
    );
  }
}
