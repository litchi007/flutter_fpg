import 'package:json_annotation/json_annotation.dart';

import 'data.dart';
import 'statement.dart';

part 'chat_get_bet_bills_model.g.dart';

@JsonSerializable()
class ChatGetBetBillsModel {
  final Statement? statement;
  final Data? data;

  const ChatGetBetBillsModel({this.statement, this.data});

  @override
  String toString() {
    return 'ChatGetBetBillsModel(statement: $statement, data: $data)';
  }

  factory ChatGetBetBillsModel.fromJson(Map<String, dynamic> json) {
    return _$ChatGetBetBillsModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatGetBetBillsModelToJson(this);

  ChatGetBetBillsModel copyWith({
    Statement? statement,
    Data? data,
  }) {
    return ChatGetBetBillsModel(
      statement: statement ?? this.statement,
      data: data ?? this.data,
    );
  }
}
