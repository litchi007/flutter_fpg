import 'package:json_annotation/json_annotation.dart';

part 'statement.g.dart';

@JsonSerializable()
class Statement {
  final int? followBetStatStatus;

  const Statement({this.followBetStatStatus});

  @override
  String toString() {
    return 'Statement(followBetStatStatus: $followBetStatStatus)';
  }

  factory Statement.fromJson(Map<String, dynamic> json) {
    return _$StatementFromJson(json);
  }

  Map<String, dynamic> toJson() => _$StatementToJson(this);

  Statement copyWith({
    int? followBetStatStatus,
  }) {
    return Statement(
      followBetStatStatus: followBetStatStatus ?? this.followBetStatStatus,
    );
  }
}
