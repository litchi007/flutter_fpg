// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      totalResultMoney: (json['totalResultMoney'] as num?)?.toDouble(),
      totalBetMoney: (json['totalBetMoney'] as num?)?.toInt(),
      totalBonus: (json['totalBonus'] as num?)?.toDouble(),
      userCount: (json['userCount'] as num?)?.toInt(),
      totalAmount: (json['totalAmount'] as num?)?.toInt(),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'totalResultMoney': instance.totalResultMoney,
      'totalBetMoney': instance.totalBetMoney,
      'totalBonus': instance.totalBonus,
      'userCount': instance.userCount,
      'totalAmount': instance.totalAmount,
    };
