// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_get_bet_bills_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatGetBetBillsModel _$ChatGetBetBillsModelFromJson(
        Map<String, dynamic> json) =>
    ChatGetBetBillsModel(
      statement: json['statement'] == null
          ? null
          : Statement.fromJson(json['statement'] as Map<String, dynamic>),
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatGetBetBillsModelToJson(
        ChatGetBetBillsModel instance) =>
    <String, dynamic>{
      'statement': instance.statement,
      'data': instance.data,
    };
