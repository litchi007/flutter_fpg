// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'statement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Statement _$StatementFromJson(Map<String, dynamic> json) => Statement(
      followBetStatStatus: (json['followBetStatStatus'] as num?)?.toInt(),
    );

Map<String, dynamic> _$StatementToJson(Statement instance) => <String, dynamic>{
      'followBetStatStatus': instance.followBetStatStatus,
    };
