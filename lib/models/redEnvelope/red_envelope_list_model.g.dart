// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'red_envelope_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedEnvelopeListModel _$RedEnvelopeListModelFromJson(
        Map<String, dynamic> json) =>
    RedEnvelopeListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => RedEnvelopeModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RedEnvelopeListModelToJson(
        RedEnvelopeListModel instance) =>
    <String, dynamic>{
      'list': instance.list,
    };
