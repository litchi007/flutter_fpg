import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'red_envelope_model.g.dart';

// {
// "id": "6",
// "uid": "3",
// "createTime": "1721222691",
// "redBagId": "3",
// "genre": "1",
// "operate": "3",
// "amount": "10.00",
// "genreText": "普通红包",
// "operateText": "过期退回余额"
// }

@JsonSerializable()
class RedEnvelopeModel {
  final String? id;

  /// 注单ID
  final String? uid;

  /// 游戏名称（彩种）
  final String? createTime;

  /// 彩票期号（彩票)
  final String? redBagId;

  /// 开奖期数  自营优先使用
  final String? operate;

  /// 游戏分类名称（彩票）
  final String? amount;

  /// 游戏玩法（彩票）
  final String? genreText;

  /// 开奖号（彩票）
  final String? operateText;

  /// 下注金额

  const RedEnvelopeModel({
    this.id,
    this.uid,
    this.createTime,
    this.redBagId,
    this.operate,
    this.amount,
    this.genreText,
    this.operateText,
  });

  factory RedEnvelopeModel.fromJson(Map<String, dynamic> json) =>
      _$RedEnvelopeModelFromJson(json);

  Map<String, dynamic> toJson() => _$RedEnvelopeModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  RedEnvelopeModel copyWith(
      {String? id,
      String? uid,
      String? createTime,
      String? redBagId,
      String? operate,
      String? amount,
      String? genreText,
      String? operateText}) {
    return RedEnvelopeModel(
      id: id ?? this.id,
      uid: uid ?? this.uid,
      createTime: createTime ?? this.createTime,
      redBagId: redBagId ?? this.redBagId,
      operate: operate ?? this.operate,
      amount: amount ?? this.amount,
      genreText: genreText ?? this.genreText,
      operateText: operateText ?? this.operateText,
    );
  }
}
