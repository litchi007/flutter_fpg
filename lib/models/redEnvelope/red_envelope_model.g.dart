// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'red_envelope_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedEnvelopeModel _$RedEnvelopeModelFromJson(Map<String, dynamic> json) =>
    RedEnvelopeModel(
      id: json['id'] as String?,
      uid: json['uid'] as String?,
      createTime: json['createTime'] as String?,
      redBagId: json['redBagId'] as String?,
      operate: json['operate'] as String?,
      amount: json['amount'] as String?,
      genreText: json['genreText'] as String?,
      operateText: json['operateText'] as String?,
    );

Map<String, dynamic> _$RedEnvelopeModelToJson(RedEnvelopeModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'createTime': instance.createTime,
      'redBagId': instance.redBagId,
      'operate': instance.operate,
      'amount': instance.amount,
      'genreText': instance.genreText,
      'operateText': instance.operateText,
    };
