import 'package:json_annotation/json_annotation.dart';
import 'red_envelope_model.dart';
import 'dart:convert';
part 'red_envelope_list_model.g.dart';

@JsonSerializable()
class RedEnvelopeListModel {
  final List<RedEnvelopeModel>? list;

  const RedEnvelopeListModel({
    this.list,
  });

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory RedEnvelopeListModel.fromJson(Map<String, dynamic> json) {
    return _$RedEnvelopeListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RedEnvelopeListModelToJson(this);

  RedEnvelopeListModel copyWith({
    List<RedEnvelopeModel>? list,
  }) {
    return RedEnvelopeListModel(
      list: list ?? list,
    );
  }
}
