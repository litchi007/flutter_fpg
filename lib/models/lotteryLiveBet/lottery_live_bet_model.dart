import 'dart:convert';

class LotteryLiveBetModel {
  String gameId;
  String gameName;
  int betCount;
  double betAmount;

  LotteryLiveBetModel({
    required this.gameId,
    required this.gameName,
    required this.betCount,
    required this.betAmount,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'gameId': gameId,
        'gameName': gameName,
        'betCount': betCount,
        'betAmount': betAmount,
      };

  @override
  String toString() {
    return jsonEncode(this);
  }
}
