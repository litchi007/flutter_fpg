import 'package:json_annotation/json_annotation.dart';

import 'lottery_list.dart';

part 'game_lottery_history_model.g.dart';

@JsonSerializable()
class GameLotteryHistoryModel {
  final List<LotteryList>? list;
  final List<String>? redBalls;
  final List<String>? greenBalls;
  final List<String>? blueBalls;

  const GameLotteryHistoryModel({
    this.list,
    this.redBalls,
    this.greenBalls,
    this.blueBalls,
  });

  @override
  String toString() =>
      'GameLotteryHistoryModel(list: $list, redBalls: $redBalls, greenBalls: $greenBalls, blueBalls: $blueBalls)';

  factory GameLotteryHistoryModel.fromJson(Map<String, dynamic> json) {
    return _$GameLotteryHistoryModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GameLotteryHistoryModelToJson(this);

  GameLotteryHistoryModel copyWith({
    List<LotteryList>? list,
  }) {
    return GameLotteryHistoryModel(
      list: list ?? this.list,
      redBalls: redBalls ?? this.redBalls,
      greenBalls: greenBalls ?? this.greenBalls,
      blueBalls: blueBalls ?? this.blueBalls,
    );
  }
}
