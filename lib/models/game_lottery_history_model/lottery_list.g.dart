// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryList _$LotteryListFromJson(Map<String, dynamic> json) => LotteryList(
      issue: json['issue'] as String?,
      displayNumber: json['displayNumber'] as String?,
      openTime: json['openTime'] as String?,
      num: json['num'] as String?,
      gameType: json['gameType'] as String?,
      hash: json['hash'] as String?,
      verifUrl: json['verifUrl'] as String?,
      result: json['result'] as String?,
      d0: json['d0'] as String?,
      d1: json['d1'] as String?,
      d2: json['d2'] as String?,
      d3: json['d3'] as String?,
      d4: json['d4'] as String?,
      d5: json['d5'] as String?,
      d6: json['d6'] as String?,
      d7: json['d7'] as String?,
      d8: json['d8'] as String?,
      t0: json['t0'] as String?,
      t1: json['t1'] as String?,
      t2: json['t2'] as String?,
      t3: json['t3'] as String?,
      t4: json['t4'] as String?,
      t5: json['t5'] as String?,
      t6: json['t6'] as String?,
      t7: json['t7'] as String?,
      t8: json['t8'] as String?,
      t9: json['t9'] as String?,
    );

Map<String, dynamic> _$LotteryListToJson(LotteryList instance) =>
    <String, dynamic>{
      'issue': instance.issue,
      'displayNumber': instance.displayNumber,
      'openTime': instance.openTime,
      'num': instance.num,
      'gameType': instance.gameType,
      'hash': instance.hash,
      'verifUrl': instance.verifUrl,
      'result': instance.result,
      'd0': instance.d0,
      'd1': instance.d1,
      'd2': instance.d2,
      'd3': instance.d3,
      'd4': instance.d4,
      'd5': instance.d5,
      'd6': instance.d6,
      'd7': instance.d7,
      'd8': instance.d8,
      't0': instance.t0,
      't1': instance.t1,
      't2': instance.t2,
      't3': instance.t3,
      't4': instance.t4,
      't5': instance.t5,
      't6': instance.t6,
      't7': instance.t7,
      't8': instance.t8,
      't9': instance.t9,
    };
