import 'package:json_annotation/json_annotation.dart';

part 'lottery_list.g.dart';

@JsonSerializable()
class LotteryList {
  final String? issue;
  final String? displayNumber;
  final String? openTime;
  final String? num;
  final String? gameType;
  final String? hash;
  final String? verifUrl;
  final String? result;

  /// 一下字段是胡志明游戏使用
  final String? d0;
  final String? d1;
  final String? d2;
  final String? d3;
  final String? d4;
  final String? d5;
  final String? d6;
  final String? d7;
  final String? d8;
  final String? t0;
  final String? t1;
  final String? t2;
  final String? t3;
  final String? t4;
  final String? t5;
  final String? t6;
  final String? t7;
  final String? t8;
  final String? t9;

  const LotteryList({
    this.issue,
    this.displayNumber,
    this.openTime,
    this.num,
    this.gameType,
    this.hash,
    this.verifUrl,
    this.result,
    this.d0,
    this.d1,
    this.d2,
    this.d3,
    this.d4,
    this.d5,
    this.d6,
    this.d7,
    this.d8,
    this.t0,
    this.t1,
    this.t2,
    this.t3,
    this.t4,
    this.t5,
    this.t6,
    this.t7,
    this.t8,
    this.t9,
  });

  @override
  String toString() {
    return '''List(issue: $issue, displayNumber: $displayNumber, openTime: $openTime,
    num: $num, gameType:$gameType, hash: $hash, verifUrl: $verifUrl, result: $result,
    d0: $d0, d1:$d1, d2: $d2, d3: $d3, d4: $d4, d5: $d5, d6:$d6, d7: $d7, d8: $d8
    t0: $t0, t1:$t1, t2: $t2, t3: $t3, t4: $t4, t5: $t5, t6:$t6, t7: $t7, t8: $t8, t9: $t9)
    ''';
  }

  factory LotteryList.fromJson(Map<String, dynamic> json) =>
      _$LotteryListFromJson(json);

  Map<String, dynamic> toJson() => _$LotteryListToJson(this);

  LotteryList copyWith({
    String? issue,
    String? displayNumber,
    String? openTime,
    String? num,
    String? gameType,
    String? hash,
    String? verifUrl,
    String? result,
    String? d0,
    String? d1,
    String? d2,
    String? d3,
    String? d4,
    String? d5,
    String? d6,
    String? d7,
    String? d8,
    String? t0,
    String? t1,
    String? t2,
    String? t3,
    String? t4,
    String? t5,
    String? t6,
    String? t7,
    String? t8,
    String? t9,
  }) {
    return LotteryList(
      issue: issue ?? this.issue,
      displayNumber: displayNumber ?? this.displayNumber,
      openTime: openTime ?? this.openTime,
      num: num ?? this.num,
      gameType: gameType ?? this.gameType,
      hash: hash ?? this.hash,
      verifUrl: verifUrl ?? this.verifUrl,
      result: result ?? this.result,
      d0: d0 ?? this.d0,
      d1: d1 ?? this.d1,
      d2: d2 ?? this.d2,
      d3: d3 ?? this.d3,
      d4: d4 ?? this.d4,
      d5: d5 ?? this.d5,
      d6: d6 ?? this.d6,
      d7: d7 ?? this.d7,
      d8: d8 ?? this.d8,
      t0: t0 ?? this.t0,
      t1: t1 ?? this.t1,
      t2: t2 ?? this.t2,
      t3: t3 ?? this.t3,
      t4: t4 ?? this.t4,
      t5: t5 ?? this.t5,
      t6: t6 ?? this.t6,
      t7: t7 ?? this.t7,
      t8: t8 ?? this.t8,
      t9: t9 ?? this.t9,
    );
  }
}
