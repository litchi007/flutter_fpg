class GameType {
  /// 幸运飞艇
  static const String luckyAirship = 'xyft';

  /// 时时彩
  static const String timeLottery = 'cqssc';

  /// 赛车
  static const String racing = 'pk10';

  /// pc蛋蛋
  static const String pcEggs = 'bjkl8';

  /// 快3
  static const String quickThree = 'jsk3';

  /// 11选5
  static const String elevenSelectedFive = 'gd11x5';

  /// 六合彩
  static const String markSixLottery = 'lhc';

  /// 排列3
  static const String sortThree = 'fc3d';

  /// 澳洲幸运8
  static const String gdkl10 = 'gdkl10';

  /// 胡志明
  static const String huZhiMing = 'ofclvn_hochiminhvip';

  /// 七星彩
  static const String sevenStarLottery = 'qxc';
}
