// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_lottery_history_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameLotteryHistoryModel _$GameLotteryHistoryModelFromJson(
        Map<String, dynamic> json) =>
    GameLotteryHistoryModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => LotteryList.fromJson(e as Map<String, dynamic>))
          .toList(),
      redBalls: (json['redBalls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      greenBalls: (json['greenBalls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      blueBalls: (json['blueBalls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$GameLotteryHistoryModelToJson(
        GameLotteryHistoryModel instance) =>
    <String, dynamic>{
      'list': instance.list,
      'redBalls': instance.redBalls,
      'greenBalls': instance.greenBalls,
      'blueBalls': instance.blueBalls,
    };
