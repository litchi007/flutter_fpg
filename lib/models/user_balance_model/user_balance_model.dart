import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'user_balance_model.g.dart';

@JsonSerializable()
class UserBalanceModel {
  final String? balance;
  @JsonKey(name: 'doy_balance')
  final String? doyBalance;
  @JsonKey(name: 'usdt_balance')
  final String? usdtBalance;

  const UserBalanceModel({this.balance, this.doyBalance, this.usdtBalance});

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory UserBalanceModel.fromJson(Map<String, dynamic> json) {
    return _$UserBalanceModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$UserBalanceModelToJson(this);

  UserBalanceModel copyWith(
      {final String? balance,
      final String? doyBalance,
      final String? usdtBalance}) {
    return UserBalanceModel(
      balance: balance ?? this.balance,
      doyBalance: doyBalance ?? this.doyBalance,
      usdtBalance: doyBalance ?? this.usdtBalance,
    );
  }
}
