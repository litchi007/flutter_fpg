// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_balance_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserBalanceModel _$UserBalanceModelFromJson(Map<String, dynamic> json) =>
    UserBalanceModel(
      balance: json['balance'] as String?,
      doyBalance: json['doy_balance'] as String?,
      usdtBalance: json['usdt_balance'] as String?,
    );

Map<String, dynamic> _$UserBalanceModelToJson(UserBalanceModel instance) =>
    <String, dynamic>{
      'balance': instance.balance,
      'doy_balance': instance.doyBalance,
      'usdt_balance': instance.usdtBalance,
    };
