// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'red_bag.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedBag _$RedBagFromJson(Map<String, dynamic> json) => RedBag(
      roomId: json['roomId'] as String?,
      uid: json['uid'] as String?,
      title: json['title'] as String?,
      genre: json['genre'] as String?,
      amount: json['amount'] as String?,
      quantity: (json['quantity'] as num?)?.toInt(),
      createDmlMultiple: json['createDmlMultiple'] as String?,
      grabDmlMultiple: json['grabDmlMultiple'] as String?,
      grabLevels: json['grabLevels'] as String?,
      oddsRate: (json['oddsRate'] as num?)?.toInt(),
      mineNumber: (json['mineNumber'] as num?)?.toInt(),
      createTime: (json['createTime'] as num?)?.toInt(),
      expireTime: (json['expireTime'] as num?)?.toInt(),
      updateTime: (json['updateTime'] as num?)?.toInt(),
      systemPosition: (json['systemPosition'] as num?)?.toInt(),
      status: (json['status'] as num?)?.toInt(),
      isSend: (json['isSend'] as num?)?.toInt(),
      id: json['id'] as String?,
      isGrab: (json['is_grab'] as num?)?.toInt(),
      grabAmount: (json['grab_amount'] as num?)?.toInt(),
      grabList: json['grabList'] as List<dynamic>?,
    );

Map<String, dynamic> _$RedBagToJson(RedBag instance) => <String, dynamic>{
      'roomId': instance.roomId,
      'uid': instance.uid,
      'title': instance.title,
      'genre': instance.genre,
      'amount': instance.amount,
      'quantity': instance.quantity,
      'createDmlMultiple': instance.createDmlMultiple,
      'grabDmlMultiple': instance.grabDmlMultiple,
      'grabLevels': instance.grabLevels,
      'oddsRate': instance.oddsRate,
      'mineNumber': instance.mineNumber,
      'createTime': instance.createTime,
      'expireTime': instance.expireTime,
      'updateTime': instance.updateTime,
      'systemPosition': instance.systemPosition,
      'status': instance.status,
      'isSend': instance.isSend,
      'id': instance.id,
      'is_grab': instance.isGrab,
      'grab_amount': instance.grabAmount,
      'grabList': instance.grabList,
    };
