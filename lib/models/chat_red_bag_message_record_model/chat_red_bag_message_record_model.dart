import 'package:json_annotation/json_annotation.dart';

import 'red_bag.dart';

part 'chat_red_bag_message_record_model.g.dart';

@JsonSerializable()
class ChatRedBagMessageRecordModel {
  final String? id;
  final String? uid;
  final String? avatar;
  final String? username;
  final String? level;
  @JsonKey(name: 'is_trial')
  final String? isTrial;
  final bool? isManager;
  final String? createTime;
  final String? updateTime;
  final String? dataId;
  final String? messageCode;
  @JsonKey(name: 'chat_type')
  final String? chatType;
  @JsonKey(name: 'data_type')
  final String? dataType;
  final String? fullname;
  final String? usernameBak;
  final bool? isChatCron;
  final bool? shareBillFlag;
  final bool? betFollowFlag;
  final String? ip;
  final String? msg;
  final String? extfield;
  final dynamic callJson;
  final String? t;
  final String? time;
  final String? roomId;
  final int? isManagerIconTag;
  final List<dynamic>? callList;
  final String? avator;
  @JsonKey(name: 'avatar_status')
  final int? avatarStatus;
  final String? levelImg;
  final String? levelIconLeft;
  final String? levelIconRight;
  final RedBag? redBag;

  const ChatRedBagMessageRecordModel({
    this.id,
    this.uid,
    this.avatar,
    this.username,
    this.level,
    this.isTrial,
    this.isManager,
    this.createTime,
    this.updateTime,
    this.dataId,
    this.messageCode,
    this.chatType,
    this.dataType,
    this.fullname,
    this.usernameBak,
    this.isChatCron,
    this.shareBillFlag,
    this.betFollowFlag,
    this.ip,
    this.msg,
    this.extfield,
    this.callJson,
    this.t,
    this.time,
    this.roomId,
    this.isManagerIconTag,
    this.callList,
    this.avator,
    this.avatarStatus,
    this.levelImg,
    this.levelIconLeft,
    this.levelIconRight,
    this.redBag,
  });

  @override
  String toString() {
    return 'ChatRedBagMessageRecordModel(id: $id, uid: $uid, avatar: $avatar, username: $username, level: $level, isTrial: $isTrial, isManager: $isManager, createTime: $createTime, updateTime: $updateTime, dataId: $dataId, messageCode: $messageCode, chatType: $chatType, dataType: $dataType, fullname: $fullname, usernameBak: $usernameBak, isChatCron: $isChatCron, shareBillFlag: $shareBillFlag, betFollowFlag: $betFollowFlag, ip: $ip, msg: $msg, extfield: $extfield, callJson: $callJson, t: $t, time: $time, roomId: $roomId, isManagerIconTag: $isManagerIconTag, callList: $callList, avator: $avator, avatarStatus: $avatarStatus, levelImg: $levelImg, levelIconLeft: $levelIconLeft, levelIconRight: $levelIconRight, redBag: $redBag)';
  }

  factory ChatRedBagMessageRecordModel.fromJson(Map<String, dynamic> json) {
    return _$ChatRedBagMessageRecordModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatRedBagMessageRecordModelToJson(this);

  ChatRedBagMessageRecordModel copyWith({
    String? id,
    String? uid,
    String? avatar,
    String? username,
    String? level,
    String? isTrial,
    bool? isManager,
    String? createTime,
    String? updateTime,
    String? dataId,
    String? messageCode,
    String? chatType,
    String? dataType,
    String? fullname,
    String? usernameBak,
    bool? isChatCron,
    bool? shareBillFlag,
    bool? betFollowFlag,
    String? ip,
    String? msg,
    String? extfield,
    dynamic callJson,
    String? t,
    String? time,
    String? roomId,
    int? isManagerIconTag,
    List<dynamic>? callList,
    String? avator,
    int? avatarStatus,
    String? levelImg,
    String? levelIconLeft,
    String? levelIconRight,
    RedBag? redBag,
  }) {
    return ChatRedBagMessageRecordModel(
      id: id ?? this.id,
      uid: uid ?? this.uid,
      avatar: avatar ?? this.avatar,
      username: username ?? this.username,
      level: level ?? this.level,
      isTrial: isTrial ?? this.isTrial,
      isManager: isManager ?? this.isManager,
      createTime: createTime ?? this.createTime,
      updateTime: updateTime ?? this.updateTime,
      dataId: dataId ?? this.dataId,
      messageCode: messageCode ?? this.messageCode,
      chatType: chatType ?? this.chatType,
      dataType: dataType ?? this.dataType,
      fullname: fullname ?? this.fullname,
      usernameBak: usernameBak ?? this.usernameBak,
      isChatCron: isChatCron ?? this.isChatCron,
      shareBillFlag: shareBillFlag ?? this.shareBillFlag,
      betFollowFlag: betFollowFlag ?? this.betFollowFlag,
      ip: ip ?? this.ip,
      msg: msg ?? this.msg,
      extfield: extfield ?? this.extfield,
      callJson: callJson ?? this.callJson,
      t: t ?? this.t,
      time: time ?? this.time,
      roomId: roomId ?? this.roomId,
      isManagerIconTag: isManagerIconTag ?? this.isManagerIconTag,
      callList: callList ?? this.callList,
      avator: avator ?? this.avator,
      avatarStatus: avatarStatus ?? this.avatarStatus,
      levelImg: levelImg ?? this.levelImg,
      levelIconLeft: levelIconLeft ?? this.levelIconLeft,
      levelIconRight: levelIconRight ?? this.levelIconRight,
      redBag: redBag ?? this.redBag,
    );
  }
}
