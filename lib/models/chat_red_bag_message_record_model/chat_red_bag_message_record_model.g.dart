// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_red_bag_message_record_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatRedBagMessageRecordModel _$ChatRedBagMessageRecordModelFromJson(
        Map<String, dynamic> json) =>
    ChatRedBagMessageRecordModel(
      id: json['id'] as String?,
      uid: json['uid'] as String?,
      avatar: json['avatar'] as String?,
      username: json['username'] as String?,
      level: json['level'] as String?,
      isTrial: json['is_trial'] as String?,
      isManager: json['isManager'] as bool?,
      createTime: json['createTime'] as String?,
      updateTime: json['updateTime'] as String?,
      dataId: json['dataId'] as String?,
      messageCode: json['messageCode'] as String?,
      chatType: json['chat_type'] as String?,
      dataType: json['data_type'] as String?,
      fullname: json['fullname'] as String?,
      usernameBak: json['usernameBak'] as String?,
      isChatCron: json['isChatCron'] as bool?,
      shareBillFlag: json['shareBillFlag'] as bool?,
      betFollowFlag: json['betFollowFlag'] as bool?,
      ip: json['ip'] as String?,
      msg: json['msg'] as String?,
      extfield: json['extfield'] as String?,
      callJson: json['callJson'],
      t: json['t'] as String?,
      time: json['time'] as String?,
      roomId: json['roomId'] as String?,
      isManagerIconTag: (json['isManagerIconTag'] as num?)?.toInt(),
      callList: json['callList'] as List<dynamic>?,
      avator: json['avator'] as String?,
      avatarStatus: (json['avatar_status'] as num?)?.toInt(),
      levelImg: json['levelImg'] as String?,
      levelIconLeft: json['levelIconLeft'] as String?,
      levelIconRight: json['levelIconRight'] as String?,
      redBag: json['redBag'] == null
          ? null
          : RedBag.fromJson(json['redBag'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatRedBagMessageRecordModelToJson(
        ChatRedBagMessageRecordModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'avatar': instance.avatar,
      'username': instance.username,
      'level': instance.level,
      'is_trial': instance.isTrial,
      'isManager': instance.isManager,
      'createTime': instance.createTime,
      'updateTime': instance.updateTime,
      'dataId': instance.dataId,
      'messageCode': instance.messageCode,
      'chat_type': instance.chatType,
      'data_type': instance.dataType,
      'fullname': instance.fullname,
      'usernameBak': instance.usernameBak,
      'isChatCron': instance.isChatCron,
      'shareBillFlag': instance.shareBillFlag,
      'betFollowFlag': instance.betFollowFlag,
      'ip': instance.ip,
      'msg': instance.msg,
      'extfield': instance.extfield,
      'callJson': instance.callJson,
      't': instance.t,
      'time': instance.time,
      'roomId': instance.roomId,
      'isManagerIconTag': instance.isManagerIconTag,
      'callList': instance.callList,
      'avator': instance.avator,
      'avatar_status': instance.avatarStatus,
      'levelImg': instance.levelImg,
      'levelIconLeft': instance.levelIconLeft,
      'levelIconRight': instance.levelIconRight,
      'redBag': instance.redBag,
    };
