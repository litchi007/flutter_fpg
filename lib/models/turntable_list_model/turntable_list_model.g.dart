// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'turntable_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TurntableListModel _$TurntableListModelFromJson(Map<String, dynamic> json) =>
    TurntableListModel(
      id: json['id'] as String?,
      param: json['param'] == null
          ? null
          : Param.fromJson(json['param'] as Map<String, dynamic>),
      type: json['type'] as String?,
      start: json['start'] as String?,
      end: json['end'] as String?,
    );

Map<String, dynamic> _$TurntableListModelToJson(TurntableListModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'param': instance.param,
      'type': instance.type,
      'start': instance.start,
      'end': instance.end,
    };
