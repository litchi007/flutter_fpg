import 'package:json_annotation/json_annotation.dart';

import 'param.dart';

part 'turntable_list_model.g.dart';

@JsonSerializable()
class TurntableListModel {
  final String? id;
  final Param? param;
  final String? type;
  final String? start;
  final String? end;

  const TurntableListModel({
    this.id,
    this.param,
    this.type,
    this.start,
    this.end,
  });

  @override
  String toString() {
    return 'TurntableListModel(id: $id, param: $param, type: $type, start: $start, end: $end)';
  }

  factory TurntableListModel.fromJson(Map<String, dynamic> json) {
    return _$TurntableListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TurntableListModelToJson(this);

  TurntableListModel copyWith({
    String? id,
    Param? param,
    String? type,
    String? start,
    String? end,
  }) {
    return TurntableListModel(
      id: id ?? this.id,
      param: param ?? this.param,
      type: type ?? this.type,
      start: start ?? this.start,
      end: end ?? this.end,
    );
  }
}
