// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prize_arr.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrizeArr _$PrizeArrFromJson(Map<String, dynamic> json) => PrizeArr(
      prizeId: (json['prizeId'] as num?)?.toInt(),
      prizeIcon: json['prizeIcon'] as String?,
      prizeIconName: json['prizeIconName'] as String?,
      prizeName: json['prizeName'] as String?,
      prizeType: json['prizeType'] as String?,
      prizeAmount: json['prizeAmount'] as String?,
      prizeAmount2: json['prizeAmount2'] as String?,
    );

Map<String, dynamic> _$PrizeArrToJson(PrizeArr instance) => <String, dynamic>{
      'prizeId': instance.prizeId,
      'prizeIcon': instance.prizeIcon,
      'prizeIconName': instance.prizeIconName,
      'prizeName': instance.prizeName,
      'prizeType': instance.prizeType,
      'prizeAmount': instance.prizeAmount,
      'prizeAmount2': instance.prizeAmount2,
    };
