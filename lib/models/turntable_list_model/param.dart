import 'package:json_annotation/json_annotation.dart';

import 'arc_arr.dart';
import 'prize_arr.dart';

part 'param.g.dart';

@JsonSerializable()
class Param {
  final String? buy;
  @JsonKey(name: 'buy_amount')
  final int? buyAmount;
  @JsonKey(name: 'check_in_user_levels')
  final String? checkInUserLevels;
  @JsonKey(name: 'content_turntable')
  final List<String>? contentTurntable;
  final String? membergame;
  @JsonKey(name: 'prize_time')
  final int? prizeTime;
  @JsonKey(name: 'scrath_buy_type')
  final String? scrathBuyType;
  @JsonKey(name: 'visitor_show')
  final String? visitorShow;
  @JsonKey(name: 'chassis_img')
  final String? chassisImg;
  @JsonKey(name: 'chassis_img_name')
  final String? chassisImgName;
  final List<PrizeArr>? prizeArr;
  @JsonKey(name: 'activity_buy_type_model')
  final String? activityBuyTypeModel;
  final List<ArcArr>? arcArr;

  const Param({
    this.buy,
    this.buyAmount,
    this.checkInUserLevels,
    this.contentTurntable,
    this.membergame,
    this.prizeTime,
    this.scrathBuyType,
    this.visitorShow,
    this.chassisImg,
    this.chassisImgName,
    this.prizeArr,
    this.activityBuyTypeModel,
    this.arcArr,
  });

  @override
  String toString() {
    return 'Param(buy: $buy, buyAmount: $buyAmount, checkInUserLevels: $checkInUserLevels, contentTurntable: $contentTurntable, membergame: $membergame, prizeTime: $prizeTime, scrathBuyType: $scrathBuyType, visitorShow: $visitorShow, chassisImg: $chassisImg, chassisImgName: $chassisImgName, prizeArr: $prizeArr, activityBuyTypeModel: $activityBuyTypeModel, arcArr: $arcArr)';
  }

  factory Param.fromJson(Map<String, dynamic> json) => _$ParamFromJson(json);

  Map<String, dynamic> toJson() => _$ParamToJson(this);

  Param copyWith({
    String? buy,
    int? buyAmount,
    String? checkInUserLevels,
    List<String>? contentTurntable,
    String? membergame,
    int? prizeTime,
    String? scrathBuyType,
    String? visitorShow,
    String? chassisImg,
    String? chassisImgName,
    List<PrizeArr>? prizeArr,
    String? activityBuyTypeModel,
    List<ArcArr>? arcArr,
  }) {
    return Param(
      buy: buy ?? this.buy,
      buyAmount: buyAmount ?? this.buyAmount,
      checkInUserLevels: checkInUserLevels ?? this.checkInUserLevels,
      contentTurntable: contentTurntable ?? this.contentTurntable,
      membergame: membergame ?? this.membergame,
      prizeTime: prizeTime ?? this.prizeTime,
      scrathBuyType: scrathBuyType ?? this.scrathBuyType,
      visitorShow: visitorShow ?? this.visitorShow,
      chassisImg: chassisImg ?? this.chassisImg,
      chassisImgName: chassisImgName ?? this.chassisImgName,
      prizeArr: prizeArr ?? this.prizeArr,
      activityBuyTypeModel: activityBuyTypeModel ?? this.activityBuyTypeModel,
      arcArr: arcArr ?? this.arcArr,
    );
  }
}
