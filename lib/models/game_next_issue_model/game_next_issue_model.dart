import 'package:json_annotation/json_annotation.dart';

part 'game_next_issue_model.g.dart';

@JsonSerializable()
class GameNextIssueModel {
  final String? id;
  final String? title;
  final String? gameType;
  final String? isSeal;
  final String? isClose;
  final String? isInstant;
  final String? lowFreq;
  final String? serverTime;
  final String? serverTimestamp;
  final String? betTemplate;
  final dynamic curIssue;
  final dynamic displayNumber;
  final String? curOpenTime;
  final String? curCloseTime;
  final List<dynamic>? zodiacNums;
  final List<dynamic>? fiveElements;
  final List<dynamic>? redBalls;
  final List<dynamic>? greenBalls;
  final List<dynamic>? blueBalls;
  final String? logo;
  final dynamic preIsOpen;
  final String? preIssue;
  final String? preOpenTime;
  final String? preNum;
  final String? preDisplayNumber;
  final String? hash;
  final String? verifUrl;
  final String? preResult;
  final String? customise;
  final dynamic adEnable;
  final String? adPic;
  final dynamic adLink;
  final String? adGameType;

  const GameNextIssueModel({
    this.id,
    this.title,
    this.gameType,
    this.isSeal,
    this.isClose,
    this.isInstant,
    this.lowFreq,
    this.serverTime,
    this.serverTimestamp,
    this.betTemplate,
    this.curIssue,
    this.displayNumber,
    this.curOpenTime,
    this.curCloseTime,
    this.zodiacNums,
    this.fiveElements,
    this.redBalls,
    this.greenBalls,
    this.blueBalls,
    this.logo,
    this.preIsOpen,
    this.preIssue,
    this.preOpenTime,
    this.preNum,
    this.preDisplayNumber,
    this.hash,
    this.verifUrl,
    this.preResult,
    this.customise,
    this.adEnable,
    this.adPic,
    this.adLink,
    this.adGameType,
  });

  @override
  String toString() {
    return 'GameNextIssueModel(id: $id, title: $title, gameType: $gameType, isSeal: $isSeal, isClose: $isClose, isInstant: $isInstant, lowFreq: $lowFreq, serverTime: $serverTime, serverTimestamp: $serverTimestamp, betTemplate: $betTemplate, curIssue: $curIssue, displayNumber: $displayNumber, curOpenTime: $curOpenTime, curCloseTime: $curCloseTime, zodiacNums: $zodiacNums, fiveElements: $fiveElements, redBalls: $redBalls, greenBalls: $greenBalls, blueBalls: $blueBalls, logo: $logo, preIsOpen: $preIsOpen, preIssue: $preIssue, preOpenTime: $preOpenTime, preNum: $preNum, preDisplayNumber: $preDisplayNumber, hash: $hash, verifUrl: $verifUrl, preResult: $preResult, customise: $customise, adEnable: $adEnable, adPic: $adPic, adLink: $adLink, adGameType: $adGameType)';
  }

  factory GameNextIssueModel.fromJson(Map<String, dynamic> json) {
    return _$GameNextIssueModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GameNextIssueModelToJson(this);

  GameNextIssueModel copyWith({
    String? id,
    String? title,
    String? gameType,
    String? isSeal,
    String? isClose,
    String? isInstant,
    String? lowFreq,
    String? serverTime,
    String? serverTimestamp,
    String? betTemplate,
    dynamic curIssue,
    dynamic displayNumber,
    String? curOpenTime,
    String? curCloseTime,
    List<dynamic>? zodiacNums,
    List<dynamic>? fiveElements,
    List<dynamic>? redBalls,
    List<dynamic>? greenBalls,
    List<dynamic>? blueBalls,
    String? logo,
    dynamic preIsOpen,
    String? preIssue,
    String? preOpenTime,
    String? preNum,
    String? preDisplayNumber,
    String? hash,
    String? verifUrl,
    String? preResult,
    String? customise,
    dynamic adEnable,
    String? adPic,
    dynamic adLink,
    String? adGameType,
  }) {
    return GameNextIssueModel(
      id: id ?? this.id,
      title: title ?? this.title,
      gameType: gameType ?? this.gameType,
      isSeal: isSeal ?? this.isSeal,
      isClose: isClose ?? this.isClose,
      isInstant: isInstant ?? this.isInstant,
      lowFreq: lowFreq ?? this.lowFreq,
      serverTime: serverTime ?? this.serverTime,
      serverTimestamp: serverTimestamp ?? this.serverTimestamp,
      betTemplate: betTemplate ?? this.betTemplate,
      curIssue: curIssue ?? this.curIssue,
      displayNumber: displayNumber ?? this.displayNumber,
      curOpenTime: curOpenTime ?? this.curOpenTime,
      curCloseTime: curCloseTime ?? this.curCloseTime,
      zodiacNums: zodiacNums ?? this.zodiacNums,
      fiveElements: fiveElements ?? this.fiveElements,
      redBalls: redBalls ?? this.redBalls,
      greenBalls: greenBalls ?? this.greenBalls,
      blueBalls: blueBalls ?? this.blueBalls,
      logo: logo ?? this.logo,
      preIsOpen: preIsOpen ?? this.preIsOpen,
      preIssue: preIssue ?? this.preIssue,
      preOpenTime: preOpenTime ?? this.preOpenTime,
      preNum: preNum ?? this.preNum,
      preDisplayNumber: preDisplayNumber ?? this.preDisplayNumber,
      hash: hash ?? this.hash,
      verifUrl: verifUrl ?? this.verifUrl,
      preResult: preResult ?? this.preResult,
      customise: customise ?? this.customise,
      adEnable: adEnable ?? this.adEnable,
      adPic: adPic ?? this.adPic,
      adLink: adLink ?? this.adLink,
      adGameType: adGameType ?? this.adGameType,
    );
  }
}
