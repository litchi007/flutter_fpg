import 'package:json_annotation/json_annotation.dart';

import 'prize_param.dart';

part 'activity_gold_egg_log_model.g.dart';

@JsonSerializable()
class ActivityGoldEggLogModel {
  final String? id;
  final String? aid;
  final String? uid;
  @JsonKey(name: 'update_date')
  final String? updateDate;
  @JsonKey(name: 'prize_param')
  final List<PrizeParam>? prizeParam;

  const ActivityGoldEggLogModel({
    this.id,
    this.aid,
    this.uid,
    this.updateDate,
    this.prizeParam,
  });

  factory ActivityGoldEggLogModel.fromJson(Map<String, dynamic> json) {
    return _$ActivityGoldEggLogModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ActivityGoldEggLogModelToJson(this);

  ActivityGoldEggLogModel copyWith({
    String? id,
    String? aid,
    String? uid,
    String? updateDate,
    List<PrizeParam>? prizeParam,
  }) {
    return ActivityGoldEggLogModel(
      id: id ?? this.id,
      aid: aid ?? this.aid,
      uid: uid ?? this.uid,
      updateDate: updateDate ?? this.updateDate,
      prizeParam: prizeParam ?? this.prizeParam,
    );
  }
}
