// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_gold_egg_log_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivityGoldEggLogModel _$ActivityGoldEggLogModelFromJson(
        Map<String, dynamic> json) =>
    ActivityGoldEggLogModel(
      id: json['id'] as String?,
      aid: json['aid'] as String?,
      uid: json['uid'] as String?,
      updateDate: json['update_date'] as String?,
      prizeParam: (json['prize_param'] as List<dynamic>?)
          ?.map((e) => PrizeParam.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ActivityGoldEggLogModelToJson(
        ActivityGoldEggLogModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'aid': instance.aid,
      'uid': instance.uid,
      'update_date': instance.updateDate,
      'prize_param': instance.prizeParam,
    };
