import 'package:json_annotation/json_annotation.dart';

part 'prize_param.g.dart';

@JsonSerializable()
class PrizeParam {
  final int? prizeId;
  final String? prizeIcon;
  final String? prizeIconName;
  final String? prizeName;
  final String? prizeType;
  final double? prizeAmount;
  final int? prizeProbability;
  final String? prizeAmount2;
  final String? prizeMsg;
  final int? prizeflag;
  final double? integralOld;
  final double? integral;
  @JsonKey(name: 'update_time')
  final int? updateTime;

  const PrizeParam({
    this.prizeId,
    this.prizeIcon,
    this.prizeIconName,
    this.prizeName,
    this.prizeType,
    this.prizeAmount,
    this.prizeProbability,
    this.prizeAmount2,
    this.prizeMsg,
    this.prizeflag,
    this.integralOld,
    this.integral,
    this.updateTime,
  });

  factory PrizeParam.fromJson(Map<String, dynamic> json) {
    return _$PrizeParamFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PrizeParamToJson(this);

  PrizeParam copyWith({
    int? prizeId,
    String? prizeIcon,
    String? prizeIconName,
    String? prizeName,
    String? prizeType,
    double? prizeAmount,
    int? prizeProbability,
    String? prizeAmount2,
    String? prizeMsg,
    int? prizeflag,
    double? integralOld,
    double? integral,
    int? updateTime,
  }) {
    return PrizeParam(
      prizeId: prizeId ?? this.prizeId,
      prizeIcon: prizeIcon ?? this.prizeIcon,
      prizeIconName: prizeIconName ?? this.prizeIconName,
      prizeName: prizeName ?? this.prizeName,
      prizeType: prizeType ?? this.prizeType,
      prizeAmount: prizeAmount ?? this.prizeAmount,
      prizeProbability: prizeProbability ?? this.prizeProbability,
      prizeAmount2: prizeAmount2 ?? this.prizeAmount2,
      prizeMsg: prizeMsg ?? this.prizeMsg,
      prizeflag: prizeflag ?? this.prizeflag,
      integralOld: integralOld ?? this.integralOld,
      integral: integral ?? this.integral,
      updateTime: updateTime ?? this.updateTime,
    );
  }
}
