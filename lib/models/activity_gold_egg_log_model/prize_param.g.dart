// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prize_param.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrizeParam _$PrizeParamFromJson(Map<String, dynamic> json) => PrizeParam(
      prizeId: (json['prizeId'] as num?)?.toInt(),
      prizeIcon: json['prizeIcon'] as String?,
      prizeIconName: json['prizeIconName'] as String?,
      prizeName: json['prizeName'] as String?,
      prizeType: json['prizeType'] as String?,
      prizeAmount: (json['prizeAmount'] as num?)?.toDouble(),
      prizeProbability: (json['prizeProbability'] as num?)?.toInt(),
      prizeAmount2: json['prizeAmount2'] as String?,
      prizeMsg: json['prizeMsg'] as String?,
      prizeflag: (json['prizeflag'] as num?)?.toInt(),
      integralOld: (json['integralOld'] as num?)?.toDouble(),
      integral: (json['integral'] as num?)?.toDouble(),
      updateTime: (json['update_time'] as num?)?.toInt(),
    );

Map<String, dynamic> _$PrizeParamToJson(PrizeParam instance) =>
    <String, dynamic>{
      'prizeId': instance.prizeId,
      'prizeIcon': instance.prizeIcon,
      'prizeIconName': instance.prizeIconName,
      'prizeName': instance.prizeName,
      'prizeType': instance.prizeType,
      'prizeAmount': instance.prizeAmount,
      'prizeProbability': instance.prizeProbability,
      'prizeAmount2': instance.prizeAmount2,
      'prizeMsg': instance.prizeMsg,
      'prizeflag': instance.prizeflag,
      'integralOld': instance.integralOld,
      'integral': instance.integral,
      'update_time': instance.updateTime,
    };
