import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'lottery_bet_result_data.g.dart';

@JsonSerializable()
class LotteryBetResultData {
  final String? code;
  final String? msg;
  final Map<String, dynamic>? data;

  const LotteryBetResultData({
    this.code,
    this.msg,
    this.data,
  });

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory LotteryBetResultData.fromJson(Map<String, dynamic> json) {
    return _$LotteryBetResultDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryBetResultDataToJson(this);

  LotteryBetResultData copyWith({
    String? code,
    String? msg,
    Map<String, dynamic>? data,
  }) {
    return LotteryBetResultData(
      code: code ?? this.code,
      msg: msg ?? this.msg,
      data: data ?? this.data,
    );
  }
}
