// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_bet_result_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryBetResultModel _$LotteryBetResultModelFromJson(
        Map<String, dynamic> json) =>
    LotteryBetResultModel(
      openNum: json['openNum'] as String?,
      bonus: json['bonus'] as String?,
      gameType: json['gameType'] as String?,
      color: json['color'] as String?,
      sx: json['sx'] as String?,
      result: json['result'] as String?,
    );

Map<String, dynamic> _$LotteryBetResultModelToJson(
        LotteryBetResultModel instance) =>
    <String, dynamic>{
      'openNum': instance.openNum,
      'bonus': instance.bonus,
      'gameType': instance.gameType,
      'color': instance.color,
      'sx': instance.sx,
      'result': instance.result,
    };
