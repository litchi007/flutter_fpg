import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'lottery_bet_result_model.g.dart';

@JsonSerializable()
class LotteryBetResultModel {
  final String? openNum; //09,49,02,40,05,42,07 //结果
  final String? bonus; // 0.0000" 金额
  final String? gameType; // 六合彩系列" lhc
  final String? color; // blue,green,red,red,green,blue,red
  final String? sx; // 龙,鼠,猪,鸡,猴,羊,马
  final String? result; // 龙,鼠,猪,鸡,猴,羊,马

  const LotteryBetResultModel({
    this.openNum,
    this.bonus,
    this.gameType,
    this.color,
    this.sx,
    this.result,
  });

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory LotteryBetResultModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryBetResultModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryBetResultModelToJson(this);

  LotteryBetResultModel copyWith({
    String? openNum,
    String? bonus,
    String? gameType,
    String? color,
    String? sx,
    String? result,
  }) {
    return LotteryBetResultModel(
      openNum: openNum ?? this.openNum,
      bonus: bonus ?? this.bonus,
      gameType: gameType ?? this.gameType,
      color: color ?? this.color,
      sx: sx ?? this.sx,
      result: result ?? this.result,
    );
  }
}
