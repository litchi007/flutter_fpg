// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_bet_result_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryBetResultData _$LotteryBetResultDataFromJson(
        Map<String, dynamic> json) =>
    LotteryBetResultData(
      code: json['code'] as String?,
      msg: json['msg'] as String?,
      data: json['data'] as Map<String, dynamic>?,
    );

Map<String, dynamic> _$LotteryBetResultDataToJson(
        LotteryBetResultData instance) =>
    <String, dynamic>{
      'code': instance.code,
      'msg': instance.msg,
      'data': instance.data,
    };
