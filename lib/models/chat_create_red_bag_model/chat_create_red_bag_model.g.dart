// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_create_red_bag_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatCreateRedBagModel _$ChatCreateRedBagModelFromJson(
        Map<String, dynamic> json) =>
    ChatCreateRedBagModel(
      roomId: json['roomId'] as String?,
      uid: json['uid'] as String?,
      title: json['title'] as String?,
      genre: json['genre'] as String?,
      amount: json['amount'] as String?,
      quantity: (json['quantity'] as num?)?.toInt(),
      createDmlMultiple: json['createDmlMultiple'] as String?,
      grabDmlMultiple: json['grabDmlMultiple'] as String?,
      grabLevels: json['grabLevels'] as String?,
      oddsRate: (json['oddsRate'] as num?)?.toInt(),
      mineNumber: (json['mineNumber'] as num?)?.toInt(),
      createTime: (json['createTime'] as num?)?.toInt(),
      expireTime: (json['expireTime'] as num?)?.toInt(),
      updateTime: (json['updateTime'] as num?)?.toInt(),
      systemPosition: (json['systemPosition'] as num?)?.toInt(),
      status: (json['status'] as num?)?.toInt(),
      isSend: (json['isSend'] as num?)?.toInt(),
      id: json['id'] as String?,
    );

Map<String, dynamic> _$ChatCreateRedBagModelToJson(
        ChatCreateRedBagModel instance) =>
    <String, dynamic>{
      'roomId': instance.roomId,
      'uid': instance.uid,
      'title': instance.title,
      'genre': instance.genre,
      'amount': instance.amount,
      'quantity': instance.quantity,
      'createDmlMultiple': instance.createDmlMultiple,
      'grabDmlMultiple': instance.grabDmlMultiple,
      'grabLevels': instance.grabLevels,
      'oddsRate': instance.oddsRate,
      'mineNumber': instance.mineNumber,
      'createTime': instance.createTime,
      'expireTime': instance.expireTime,
      'updateTime': instance.updateTime,
      'systemPosition': instance.systemPosition,
      'status': instance.status,
      'isSend': instance.isSend,
      'id': instance.id,
    };
