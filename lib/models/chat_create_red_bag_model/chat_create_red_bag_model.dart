import 'package:json_annotation/json_annotation.dart';

part 'chat_create_red_bag_model.g.dart';

@JsonSerializable()
class ChatCreateRedBagModel {
  final String? roomId;
  final String? uid;
  final String? title;
  final String? genre;
  final String? amount;
  final int? quantity;
  final String? createDmlMultiple;
  final String? grabDmlMultiple;
  final String? grabLevels;
  final int? oddsRate;
  final int? mineNumber;
  final int? createTime;
  final int? expireTime;
  final int? updateTime;
  final int? systemPosition;
  final int? status;
  final int? isSend;
  final String? id;

  const ChatCreateRedBagModel({
    this.roomId,
    this.uid,
    this.title,
    this.genre,
    this.amount,
    this.quantity,
    this.createDmlMultiple,
    this.grabDmlMultiple,
    this.grabLevels,
    this.oddsRate,
    this.mineNumber,
    this.createTime,
    this.expireTime,
    this.updateTime,
    this.systemPosition,
    this.status,
    this.isSend,
    this.id,
  });

  @override
  String toString() {
    return 'ChatCreateRedBagModel(roomId: $roomId, uid: $uid, title: $title, genre: $genre, amount: $amount, quantity: $quantity, createDmlMultiple: $createDmlMultiple, grabDmlMultiple: $grabDmlMultiple, grabLevels: $grabLevels, oddsRate: $oddsRate, mineNumber: $mineNumber, createTime: $createTime, expireTime: $expireTime, updateTime: $updateTime, systemPosition: $systemPosition, status: $status, isSend: $isSend, id: $id)';
  }

  factory ChatCreateRedBagModel.fromJson(Map<String, dynamic> json) {
    return _$ChatCreateRedBagModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatCreateRedBagModelToJson(this);

  ChatCreateRedBagModel copyWith({
    String? roomId,
    String? uid,
    String? title,
    String? genre,
    String? amount,
    int? quantity,
    String? createDmlMultiple,
    String? grabDmlMultiple,
    String? grabLevels,
    int? oddsRate,
    int? mineNumber,
    int? createTime,
    int? expireTime,
    int? updateTime,
    int? systemPosition,
    int? status,
    int? isSend,
    String? id,
  }) {
    return ChatCreateRedBagModel(
      roomId: roomId ?? this.roomId,
      uid: uid ?? this.uid,
      title: title ?? this.title,
      genre: genre ?? this.genre,
      amount: amount ?? this.amount,
      quantity: quantity ?? this.quantity,
      createDmlMultiple: createDmlMultiple ?? this.createDmlMultiple,
      grabDmlMultiple: grabDmlMultiple ?? this.grabDmlMultiple,
      grabLevels: grabLevels ?? this.grabLevels,
      oddsRate: oddsRate ?? this.oddsRate,
      mineNumber: mineNumber ?? this.mineNumber,
      createTime: createTime ?? this.createTime,
      expireTime: expireTime ?? this.expireTime,
      updateTime: updateTime ?? this.updateTime,
      systemPosition: systemPosition ?? this.systemPosition,
      status: status ?? this.status,
      isSend: isSend ?? this.isSend,
      id: id ?? this.id,
    );
  }
}
