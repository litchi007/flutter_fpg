// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_bet_detail.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryBetDetailModel _$LotteryBetDetailModelFromJson(
        Map<String, dynamic> json) =>
    LotteryBetDetailModel(
      title: json['title'] as String?,
      sort: json['sort'] as String?,
      id: json['id'] as String?,
      gameId: json['gameId'] as String?,
      userId: json['userId'] as String?,
      statDate: json['statDate'] as String?,
      betCount: json['betCount'] as num?,
      betMoney: json['betMoney'] as num?,
      betMoneyDouble: json['betMoneyDouble'] as num?,
      reward: json['reward'] as num?,
      rewardDouble: json['rewardDouble'] as num?,
      rebateMoneyDouble: json['rebateMoneyDouble'] as num?,
      rewardRebateDouble: json['rewardRebateDouble'] as num?,
      rewardRebate: json['rewardRebate'] as num?,
      gameType: json['gameType'] as String?,
    );

Map<String, dynamic> _$LotteryBetDetailModelToJson(
        LotteryBetDetailModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'sort': instance.sort,
      'id': instance.id,
      'gameId': instance.gameId,
      'userId': instance.userId,
      'statDate': instance.statDate,
      'betCount': instance.betCount,
      'betMoney': instance.betMoney,
      'betMoneyDouble': instance.betMoneyDouble,
      'reward': instance.reward,
      'rewardDouble': instance.rewardDouble,
      'rebateMoneyDouble': instance.rebateMoneyDouble,
      'rewardRebateDouble': instance.rewardRebateDouble,
      'rewardRebate': instance.rewardRebate,
      'gameType': instance.gameType,
    };
