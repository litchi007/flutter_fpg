import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'lottery_bet_detail.model.g.dart';

@JsonSerializable()
class LotteryBetDetailModel {
  final String? title;
  final String? sort;
  final String? id;
  final String? gameId;
  final String? userId;
  final String? statDate;
  final num? betCount;
  final num? betMoney;
  final num? betMoneyDouble;
  final num? reward;
  final num? rewardDouble;
  final num? rebateMoneyDouble;
  final num? rewardRebateDouble;
  final num? rewardRebate;
  final String? gameType;

  const LotteryBetDetailModel({
    this.title,
    this.sort,
    this.id,
    this.gameId,
    this.userId,
    this.statDate,
    this.betCount,
    this.betMoney,
    this.betMoneyDouble,
    this.reward,
    this.rewardDouble,
    this.rebateMoneyDouble,
    this.rewardRebateDouble,
    this.rewardRebate,
    this.gameType,
  });
  @override
  String toString() {
    return jsonEncode(this);
  }

  factory LotteryBetDetailModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryBetDetailModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryBetDetailModelToJson(this);

  LotteryBetDetailModel copyWith({
    String? title,
    String? sort,
    String? id,
    String? gameId,
    String? userId,
    String? statDate,
    num? betCount,
    num? betMoney,
    num? betMoneyDouble,
    num? reward,
    num? rewardDouble,
    num? rebateMoneyDouble,
    num? rewardRebateDouble,
    num? rewardRebate,
    String? gameType,
  }) =>
      LotteryBetDetailModel(
        title: title ?? this.title,
        sort: sort ?? this.sort,
        id: id ?? this.id,
        gameId: gameId ?? this.gameId,
        userId: userId ?? this.userId,
        statDate: statDate ?? this.statDate,
        betCount: betCount ?? this.betCount,
        betMoney: betMoney ?? this.betMoney,
        betMoneyDouble: betMoneyDouble ?? this.betMoneyDouble,
        reward: reward ?? this.reward,
        rewardDouble: rewardDouble ?? this.rewardDouble,
        rebateMoneyDouble: rebateMoneyDouble ?? this.rebateMoneyDouble,
        rewardRebateDouble: rewardRebateDouble ?? this.rewardRebateDouble,
        rewardRebate: rewardRebate ?? this.rewardRebate,
        gameType: gameType ?? this.gameType,
      );
}
