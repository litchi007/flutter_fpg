// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mode_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ModeList _$ModeListFromJson(Map<String, dynamic> json) => ModeList(
      mode: json['mode'] as String?,
      name: json['name'] as String?,
      modes:
          (json['modes'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ModeListToJson(ModeList instance) => <String, dynamic>{
      'mode': instance.mode,
      'name': instance.name,
      'modes': instance.modes,
    };
