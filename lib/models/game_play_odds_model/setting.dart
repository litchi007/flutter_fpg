import 'package:json_annotation/json_annotation.dart';

import 'five_element.dart';
import 'zodiac_num.dart';

part 'setting.g.dart';

@JsonSerializable()
class Setting {
  final List<String>? redBalls;
  final List<String>? greenBalls;
  final List<String>? blueBalls;
  final List<String>? zodiacs;
  final List<String>? zodiacSky;
  final List<String>? zodiacEarth;
  final List<String>? zodiacFront;
  final List<String>? zodiacBehind;
  final List<String>? zodiacPoultry;
  final List<String>? zodiacBeast;
  final List<ZodiacNum>? zodiacNums;
  final List<FiveElement>? fiveElements;

  const Setting({
    this.redBalls,
    this.greenBalls,
    this.blueBalls,
    this.zodiacs,
    this.zodiacSky,
    this.zodiacEarth,
    this.zodiacFront,
    this.zodiacBehind,
    this.zodiacPoultry,
    this.zodiacBeast,
    this.zodiacNums,
    this.fiveElements,
  });

  @override
  String toString() {
    return 'Setting(redBalls: $redBalls, greenBalls: $greenBalls, blueBalls: $blueBalls, zodiacs: $zodiacs, zodiacSky: $zodiacSky, zodiacEarth: $zodiacEarth, zodiacFront: $zodiacFront, zodiacBehind: $zodiacBehind, zodiacPoultry: $zodiacPoultry, zodiacBeast: $zodiacBeast, zodiacNums: $zodiacNums, fiveElements: $fiveElements)';
  }

  factory Setting.fromJson(Map<String, dynamic> json) {
    return _$SettingFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SettingToJson(this);

  Setting copyWith({
    List<String>? redBalls,
    List<String>? greenBalls,
    List<String>? blueBalls,
    List<String>? zodiacs,
    List<String>? zodiacSky,
    List<String>? zodiacEarth,
    List<String>? zodiacFront,
    List<String>? zodiacBehind,
    List<String>? zodiacPoultry,
    List<String>? zodiacBeast,
    List<ZodiacNum>? zodiacNums,
    List<FiveElement>? fiveElements,
  }) {
    return Setting(
      redBalls: redBalls ?? this.redBalls,
      greenBalls: greenBalls ?? this.greenBalls,
      blueBalls: blueBalls ?? this.blueBalls,
      zodiacs: zodiacs ?? this.zodiacs,
      zodiacSky: zodiacSky ?? this.zodiacSky,
      zodiacEarth: zodiacEarth ?? this.zodiacEarth,
      zodiacFront: zodiacFront ?? this.zodiacFront,
      zodiacBehind: zodiacBehind ?? this.zodiacBehind,
      zodiacPoultry: zodiacPoultry ?? this.zodiacPoultry,
      zodiacBeast: zodiacBeast ?? this.zodiacBeast,
      zodiacNums: zodiacNums ?? this.zodiacNums,
      fiveElements: fiveElements ?? this.fiveElements,
    );
  }
}
