import 'package:json_annotation/json_annotation.dart';

import 'bet_num_limit.dart';
import 'game.dart';
import 'lottery_limit.dart';
import 'play_odd.dart';
import 'setting.dart';
part 'game_play_odds_model.g.dart';

@JsonSerializable()
class GamePlayOddsModel {
  final List<PlayOdd>? playOdds;
  final Setting? setting;
  final List<BetNumLimit>? betNumLimit;
  final LotteryLimit? lotteryLimit;
  final Game? game;

  const GamePlayOddsModel({
    this.playOdds,
    this.setting,
    this.betNumLimit,
    this.lotteryLimit,
    this.game,
  });

  @override
  String toString() {
    return 'GamePlayOddsModel(playOdds: $playOdds, setting: $setting, betNumLimit: $betNumLimit, lotteryLimit: $lotteryLimit, game: $game)';
  }

  factory GamePlayOddsModel.fromJson(Map<String, dynamic> json) {
    return _$GamePlayOddsModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GamePlayOddsModelToJson(this);

  GamePlayOddsModel copyWith({
    List<PlayOdd>? playOdds,
    Setting? setting,
    List<BetNumLimit>? betNumLimit,
    LotteryLimit? lotteryLimit,
    Game? game,
  }) {
    return GamePlayOddsModel(
      playOdds: playOdds ?? this.playOdds,
      setting: setting ?? this.setting,
      betNumLimit: betNumLimit ?? this.betNumLimit,
      lotteryLimit: lotteryLimit ?? this.lotteryLimit,
      game: game ?? this.game,
    );
  }
}
