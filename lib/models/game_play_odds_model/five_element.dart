import 'package:json_annotation/json_annotation.dart';

part 'five_element.g.dart';

@JsonSerializable()
class FiveElement {
  final String? name;
  final String? key;
  final List<String>? nums;

  const FiveElement({this.name, this.key, this.nums});

  @override
  String toString() => 'FiveElement(name: $name, key: $key, nums: $nums)';

  factory FiveElement.fromJson(Map<String, dynamic> json) {
    return _$FiveElementFromJson(json);
  }

  Map<String, dynamic> toJson() => _$FiveElementToJson(this);

  FiveElement copyWith({
    String? name,
    String? key,
    List<String>? nums,
  }) {
    return FiveElement(
      name: name ?? this.name,
      key: key ?? this.key,
      nums: nums ?? this.nums,
    );
  }
}
