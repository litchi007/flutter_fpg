// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Game _$GameFromJson(Map<String, dynamic> json) => Game(
      gameType: json['gameType'] as String?,
      isSeal: json['isSeal'] as String?,
      isClose: json['isClose'] as String?,
      title: json['title'] as String?,
      isInstant: json['isInstant'] as String?,
    );

Map<String, dynamic> _$GameToJson(Game instance) => <String, dynamic>{
      'gameType': instance.gameType,
      'isSeal': instance.isSeal,
      'isClose': instance.isClose,
      'title': instance.title,
      'isInstant': instance.isInstant,
    };
