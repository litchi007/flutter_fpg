// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'play.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Play _$PlayFromJson(Map<String, dynamic> json) => Play(
      id: json['id'] as String?,
      name: json['name'] as String?,
      alias: json['alias'] as String?,
      rebate: json['rebate'] as String?,
      code: json['code'] as String?,
      playedGroupid: json['played_groupid'] as String?,
      odds: json['odds'] as String?,
      offlineOdds: json['offlineOdds'] as String?,
      minMoney: json['minMoney'] as String?,
      maxMoney: json['maxMoney'] as String?,
      maxTurnMoney: json['maxTurnMoney'] as String?,
      isBan: json['isBan'] as String?,
      enable: json['enable'] as String?,
      fromId: json['from_id'] as String?,
      modeList: (json['modeList'] as List<dynamic>?)
          ?.map((e) => ModeList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PlayToJson(Play instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'alias': instance.alias,
      'rebate': instance.rebate,
      'code': instance.code,
      'played_groupid': instance.playedGroupid,
      'odds': instance.odds,
      'offlineOdds': instance.offlineOdds,
      'minMoney': instance.minMoney,
      'maxMoney': instance.maxMoney,
      'maxTurnMoney': instance.maxTurnMoney,
      'isBan': instance.isBan,
      'enable': instance.enable,
      'from_id': instance.fromId,
      'modeList': instance.modeList,
    };
