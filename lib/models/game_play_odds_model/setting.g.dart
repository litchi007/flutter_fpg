// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Setting _$SettingFromJson(Map<String, dynamic> json) => Setting(
      redBalls: (json['redBalls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      greenBalls: (json['greenBalls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      blueBalls: (json['blueBalls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      zodiacs:
          (json['zodiacs'] as List<dynamic>?)?.map((e) => e as String).toList(),
      zodiacSky: (json['zodiacSky'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      zodiacEarth: (json['zodiacEarth'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      zodiacFront: (json['zodiacFront'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      zodiacBehind: (json['zodiacBehind'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      zodiacPoultry: (json['zodiacPoultry'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      zodiacBeast: (json['zodiacBeast'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      zodiacNums: (json['zodiacNums'] as List<dynamic>?)
          ?.map((e) => ZodiacNum.fromJson(e as Map<String, dynamic>))
          .toList(),
      fiveElements: (json['fiveElements'] as List<dynamic>?)
          ?.map((e) => FiveElement.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SettingToJson(Setting instance) => <String, dynamic>{
      'redBalls': instance.redBalls,
      'greenBalls': instance.greenBalls,
      'blueBalls': instance.blueBalls,
      'zodiacs': instance.zodiacs,
      'zodiacSky': instance.zodiacSky,
      'zodiacEarth': instance.zodiacEarth,
      'zodiacFront': instance.zodiacFront,
      'zodiacBehind': instance.zodiacBehind,
      'zodiacPoultry': instance.zodiacPoultry,
      'zodiacBeast': instance.zodiacBeast,
      'zodiacNums': instance.zodiacNums,
      'fiveElements': instance.fiveElements,
    };
