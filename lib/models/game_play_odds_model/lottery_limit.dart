import 'package:json_annotation/json_annotation.dart';

part 'lottery_limit.g.dart';

@JsonSerializable()
class LotteryLimit {
  final String? gameType;
  final dynamic numbers;
  final dynamic min;
  final dynamic max;
  final dynamic add0;

  const LotteryLimit({
    this.gameType,
    this.numbers,
    this.min,
    this.max,
    this.add0,
  });

  @override
  String toString() {
    return 'LotteryLimit(gameType: $gameType, numbers: $numbers, min: $min, max: $max, add0: $add0)';
  }

  factory LotteryLimit.fromJson(Map<String, dynamic> json) {
    return _$LotteryLimitFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryLimitToJson(this);

  LotteryLimit copyWith({
    String? gameType,
    dynamic numbers,
    dynamic min,
    dynamic max,
    dynamic add0,
  }) {
    return LotteryLimit(
      gameType: gameType ?? this.gameType,
      numbers: numbers ?? this.numbers,
      min: min ?? this.min,
      max: max ?? this.max,
      add0: add0 ?? this.add0,
    );
  }
}
