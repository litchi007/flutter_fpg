// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_play_odds_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GamePlayOddsModel _$GamePlayOddsModelFromJson(Map<String, dynamic> json) =>
    GamePlayOddsModel(
      playOdds: (json['playOdds'] as List<dynamic>?)
          ?.map((e) => PlayOdd.fromJson(e as Map<String, dynamic>))
          .toList(),
      setting: json['setting'] == null
          ? null
          : Setting.fromJson(json['setting'] as Map<String, dynamic>),
      betNumLimit: (json['betNumLimit'] as List<dynamic>?)
          ?.map((e) => BetNumLimit.fromJson(e as Map<String, dynamic>))
          .toList(),
      lotteryLimit: json['lotteryLimit'] == null
          ? null
          : LotteryLimit.fromJson(json['lotteryLimit'] as Map<String, dynamic>),
      game: json['game'] == null
          ? null
          : Game.fromJson(json['game'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GamePlayOddsModelToJson(GamePlayOddsModel instance) =>
    <String, dynamic>{
      'playOdds': instance.playOdds,
      'setting': instance.setting,
      'betNumLimit': instance.betNumLimit,
      'lotteryLimit': instance.lotteryLimit,
      'game': instance.game,
    };
