import 'package:json_annotation/json_annotation.dart';

part 'game.g.dart';

@JsonSerializable()
class Game {
  final String? gameType;
  final String? isSeal;
  final String? isClose;
  final String? title;
  final String? isInstant;

  const Game({
    this.gameType,
    this.isSeal,
    this.isClose,
    this.title,
    this.isInstant,
  });

  @override
  String toString() {
    return 'Game(gameType: $gameType, isSeal: $isSeal, isClose: $isClose, title: $title, isInstant: $isInstant)';
  }

  factory Game.fromJson(Map<String, dynamic> json) => _$GameFromJson(json);

  Map<String, dynamic> toJson() => _$GameToJson(this);

  Game copyWith({
    String? gameType,
    String? isSeal,
    String? isClose,
    String? title,
    String? isInstant,
  }) {
    return Game(
      gameType: gameType ?? this.gameType,
      isSeal: isSeal ?? this.isSeal,
      isClose: isClose ?? this.isClose,
      title: title ?? this.title,
      isInstant: isInstant ?? this.isInstant,
    );
  }
}
