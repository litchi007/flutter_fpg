// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'play_odd.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PlayOdd _$PlayOddFromJson(Map<String, dynamic> json) => PlayOdd(
      code: json['code'] as String?,
      name: json['name'] as String?,
      playGroups: (json['playGroups'] as List<dynamic>?)
          ?.map((e) => PlayGroup.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PlayOddToJson(PlayOdd instance) => <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'playGroups': instance.playGroups,
    };
