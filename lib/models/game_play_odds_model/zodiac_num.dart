import 'package:json_annotation/json_annotation.dart';

part 'zodiac_num.g.dart';

@JsonSerializable()
class ZodiacNum {
  final String? key;
  final String? name;
  final List<String>? nums;

  const ZodiacNum({this.key, this.name, this.nums});

  @override
  String toString() => 'ZodiacNum(key: $key, name: $name, nums: $nums)';

  factory ZodiacNum.fromJson(Map<String, dynamic> json) {
    return _$ZodiacNumFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ZodiacNumToJson(this);

  ZodiacNum copyWith({
    String? key,
    String? name,
    List<String>? nums,
  }) {
    return ZodiacNum(
      key: key ?? this.key,
      name: name ?? this.name,
      nums: nums ?? this.nums,
    );
  }
}
