import 'package:json_annotation/json_annotation.dart';

part 'bet_num_limit.g.dart';

@JsonSerializable()
class BetNumLimit {
  final String? playId;
  final String? minSize;
  final String? maxSize;
  final String? allowZero;
  final String? onlyNumber;

  const BetNumLimit({
    this.playId,
    this.minSize,
    this.maxSize,
    this.allowZero,
    this.onlyNumber,
  });

  @override
  String toString() {
    return 'BetNumLimit(playId: $playId, minSize: $minSize, maxSize: $maxSize, allowZero: $allowZero, onlyNumber: $onlyNumber)';
  }

  factory BetNumLimit.fromJson(Map<String, dynamic> json) {
    return _$BetNumLimitFromJson(json);
  }

  Map<String, dynamic> toJson() => _$BetNumLimitToJson(this);

  BetNumLimit copyWith({
    String? playId,
    String? minSize,
    String? maxSize,
    String? allowZero,
    String? onlyNumber,
  }) {
    return BetNumLimit(
      playId: playId ?? this.playId,
      minSize: minSize ?? this.minSize,
      maxSize: maxSize ?? this.maxSize,
      allowZero: allowZero ?? this.allowZero,
      onlyNumber: onlyNumber ?? this.onlyNumber,
    );
  }
}
