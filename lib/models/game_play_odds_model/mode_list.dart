import 'package:json_annotation/json_annotation.dart';

part 'mode_list.g.dart';

@JsonSerializable()
class ModeList {
  final String? mode;
  final String? name;
  final List<String>? modes;
  const ModeList({this.mode, this.name, this.modes});

  @override
  String toString() => 'ModeList(mode: $mode, name: $name, modes: $modes)';

  factory ModeList.fromJson(Map<String, dynamic> json) {
    return _$ModeListFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ModeListToJson(this);

  ModeList copyWith({
    String? mode,
    String? name,
    List<String>? modes,
  }) {
    return ModeList(
        mode: mode ?? this.mode,
        name: name ?? this.name,
        modes: modes ?? this.modes);
  }
}
