// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'play_group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PlayGroup _$PlayGroupFromJson(Map<String, dynamic> json) => PlayGroup(
      id: json['id'] as String?,
      name: json['name'] as String?,
      code: json['code'] as String?,
      isShow: json['isShow'] as String?,
      enable: json['enable'] as String?,
      isBan: json['isBan'] as String?,
      fromId: json['from_id'] as String?,
      alias: json['alias'] as String?,
      plays: (json['plays'] as List<dynamic>?)
          ?.map((e) => Play.fromJson(e as Map<String, dynamic>))
          .toList(),
      select:
          (json['select'] as List<dynamic>?)?.map((e) => e as String?).toList(),
    );

Map<String, dynamic> _$PlayGroupToJson(PlayGroup instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'isShow': instance.isShow,
      'enable': instance.enable,
      'isBan': instance.isBan,
      'from_id': instance.fromId,
      'alias': instance.alias,
      'plays': instance.plays,
      'select': instance.select,
    };
