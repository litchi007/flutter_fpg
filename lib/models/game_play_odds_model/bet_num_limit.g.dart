// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bet_num_limit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BetNumLimit _$BetNumLimitFromJson(Map<String, dynamic> json) => BetNumLimit(
      playId: json['playId'] as String?,
      minSize: json['minSize'] as String?,
      maxSize: json['maxSize'] as String?,
      allowZero: json['allowZero'] as String?,
      onlyNumber: json['onlyNumber'] as String?,
    );

Map<String, dynamic> _$BetNumLimitToJson(BetNumLimit instance) =>
    <String, dynamic>{
      'playId': instance.playId,
      'minSize': instance.minSize,
      'maxSize': instance.maxSize,
      'allowZero': instance.allowZero,
      'onlyNumber': instance.onlyNumber,
    };
