// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'zodiac_num.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ZodiacNum _$ZodiacNumFromJson(Map<String, dynamic> json) => ZodiacNum(
      key: json['key'] as String?,
      name: json['name'] as String?,
      nums: (json['nums'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ZodiacNumToJson(ZodiacNum instance) => <String, dynamic>{
      'key': instance.key,
      'name': instance.name,
      'nums': instance.nums,
    };
