// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'five_element.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FiveElement _$FiveElementFromJson(Map<String, dynamic> json) => FiveElement(
      name: json['name'] as String?,
      key: json['key'] as String?,
      nums: (json['nums'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$FiveElementToJson(FiveElement instance) =>
    <String, dynamic>{
      'name': instance.name,
      'key': instance.key,
      'nums': instance.nums,
    };
