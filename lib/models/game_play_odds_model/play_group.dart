import 'package:json_annotation/json_annotation.dart';

import 'play.dart';

part 'play_group.g.dart';

@JsonSerializable()
class PlayGroup {
  final String? id;
  final String? name;
  final String? code;
  final String? isShow;
  final String? enable;
  final String? isBan;
  @JsonKey(name: 'from_id')
  final String? fromId;
  final String? alias;
  final List<Play>? plays;
  final List<String?>? select;
  const PlayGroup(
      {this.id,
      this.name,
      this.code,
      this.isShow,
      this.enable,
      this.isBan,
      this.fromId,
      this.alias,
      this.plays,
      this.select});

  @override
  String toString() {
    return 'PlayGroup(id: $id, name: $name, code: $code, isShow: $isShow, enable: $enable, isBan: $isBan, fromId: $fromId, alias: $alias, plays: $plays, select: $select)';
  }

  factory PlayGroup.fromJson(Map<String, dynamic> json) {
    return _$PlayGroupFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PlayGroupToJson(this);

  PlayGroup copyWith({
    String? id,
    String? name,
    String? code,
    String? isShow,
    String? enable,
    String? isBan,
    String? fromId,
    String? alias,
    List<Play>? plays,
    List<String>? select,
  }) {
    return PlayGroup(
        id: id ?? this.id,
        name: name ?? this.name,
        code: code ?? this.code,
        isShow: isShow ?? this.isShow,
        enable: enable ?? this.enable,
        isBan: isBan ?? this.isBan,
        fromId: fromId ?? this.fromId,
        alias: alias ?? this.alias,
        plays: plays ?? this.plays,
        select: select ?? this.select);
  }
}
