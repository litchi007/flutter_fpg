// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_limit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryLimit _$LotteryLimitFromJson(Map<String, dynamic> json) => LotteryLimit(
      gameType: json['gameType'] as String?,
      numbers: json['numbers'],
      min: json['min'],
      max: json['max'],
      add0: json['add0'],
    );

Map<String, dynamic> _$LotteryLimitToJson(LotteryLimit instance) =>
    <String, dynamic>{
      'gameType': instance.gameType,
      'numbers': instance.numbers,
      'min': instance.min,
      'max': instance.max,
      'add0': instance.add0,
    };
