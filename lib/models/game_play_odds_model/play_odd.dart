import 'package:json_annotation/json_annotation.dart';

import 'play_group.dart';

part 'play_odd.g.dart';

@JsonSerializable()
class PlayOdd {
  final String? code;
  final String? name;
  final List<PlayGroup>? playGroups;

  const PlayOdd({this.code, this.name, this.playGroups});

  @override
  String toString() {
    return 'PlayOdd(code: $code, name: $name, playGroups: $playGroups)';
  }

  factory PlayOdd.fromJson(Map<String, dynamic> json) {
    return _$PlayOddFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PlayOddToJson(this);

  PlayOdd copyWith({
    String? code,
    String? name,
    List<PlayGroup>? playGroups,
  }) {
    return PlayOdd(
      code: code ?? this.code,
      name: name ?? this.name,
      playGroups: playGroups ?? this.playGroups,
    );
  }
}
