import 'package:json_annotation/json_annotation.dart';
import 'mode_list.dart';
part 'play.g.dart';

@JsonSerializable()
class Play {
  final String? id;
  final String? name;
  final String? alias;
  final String? rebate;
  final String? code;
  @JsonKey(name: 'played_groupid')
  final String? playedGroupid;
  final String? odds;
  final String? offlineOdds;
  final String? minMoney;
  final String? maxMoney;
  final String? maxTurnMoney;
  final String? isBan;
  final String? enable;
  @JsonKey(name: 'from_id')
  final String? fromId;
  final List<ModeList>? modeList;

  const Play({
    this.id,
    this.name,
    this.alias,
    this.rebate,
    this.code,
    this.playedGroupid,
    this.odds,
    this.offlineOdds,
    this.minMoney,
    this.maxMoney,
    this.maxTurnMoney,
    this.isBan,
    this.enable,
    this.fromId,
    this.modeList,
  });

  @override
  String toString() {
    return 'Play(id: $id, name: $name, alias: $alias, rebate: $rebate, code: $code, playedGroupid: $playedGroupid, odds: $odds, offlineOdds: $offlineOdds, minMoney: $minMoney, maxMoney: $maxMoney, maxTurnMoney: $maxTurnMoney, isBan: $isBan, enable: $enable, fromId: $fromId, modeList: $modeList)';
  }

  factory Play.fromJson(Map<String, dynamic> json) => _$PlayFromJson(json);

  Map<String, dynamic> toJson() => _$PlayToJson(this);

  Play copyWith({
    String? id,
    String? name,
    String? alias,
    String? rebate,
    String? code,
    String? playedGroupid,
    String? odds,
    String? offlineOdds,
    String? minMoney,
    String? maxMoney,
    String? maxTurnMoney,
    String? isBan,
    String? enable,
    String? fromId,
    List<ModeList>? modeList,
  }) {
    return Play(
      id: id ?? this.id,
      name: name ?? this.name,
      alias: alias ?? this.alias,
      rebate: rebate ?? this.rebate,
      code: code ?? this.code,
      playedGroupid: playedGroupid ?? this.playedGroupid,
      odds: odds ?? this.odds,
      offlineOdds: offlineOdds ?? this.offlineOdds,
      minMoney: minMoney ?? this.minMoney,
      maxMoney: maxMoney ?? this.maxMoney,
      maxTurnMoney: maxTurnMoney ?? this.maxTurnMoney,
      isBan: isBan ?? this.isBan,
      enable: enable ?? this.enable,
      fromId: fromId ?? this.fromId,
      modeList: modeList ?? this.modeList,
    );
  }
}
