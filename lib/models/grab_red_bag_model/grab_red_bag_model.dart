import 'package:json_annotation/json_annotation.dart';

part 'grab_red_bag_model.g.dart';

@JsonSerializable()
class GrabRedBagModel {
  final int? redBagId;
  final String? miniRedBagAmount;
  final bool? isMine;
  final bool? isMax;
  final bool? isLuck;
  final int? luckAmount;
  final int? time;
  final int? createTime;
  final dynamic uid;
  final String? username;
  final String? id;

  const GrabRedBagModel({
    this.redBagId,
    this.miniRedBagAmount,
    this.isMine,
    this.isMax,
    this.isLuck,
    this.luckAmount,
    this.time,
    this.createTime,
    this.uid,
    this.username,
    this.id,
  });

  @override
  String toString() {
    return 'GrabRedBagModel(redBagId: $redBagId, miniRedBagAmount: $miniRedBagAmount, isMine: $isMine, isMax: $isMax, isLuck: $isLuck, luckAmount: $luckAmount, time: $time, createTime: $createTime, uid: $uid, username: $username, id: $id)';
  }

  factory GrabRedBagModel.fromJson(Map<String, dynamic> json) {
    return _$GrabRedBagModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GrabRedBagModelToJson(this);

  GrabRedBagModel copyWith({
    int? redBagId,
    dynamic miniRedBagAmount,
    bool? isMine,
    bool? isMax,
    bool? isLuck,
    int? luckAmount,
    int? time,
    int? createTime,
    String? uid,
    dynamic username,
    dynamic id,
  }) {
    return GrabRedBagModel(
      redBagId: redBagId ?? this.redBagId,
      miniRedBagAmount: miniRedBagAmount ?? this.miniRedBagAmount,
      isMine: isMine ?? this.isMine,
      isMax: isMax ?? this.isMax,
      isLuck: isLuck ?? this.isLuck,
      luckAmount: luckAmount ?? this.luckAmount,
      time: time ?? this.time,
      createTime: createTime ?? this.createTime,
      uid: uid ?? this.uid,
      username: username ?? this.username,
      id: id ?? this.id,
    );
  }
}
