// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'grab_red_bag_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GrabRedBagModel _$GrabRedBagModelFromJson(Map<String, dynamic> json) =>
    GrabRedBagModel(
      redBagId: (json['redBagId'] as num?)?.toInt(),
      miniRedBagAmount: json['miniRedBagAmount'] as String?,
      isMine: json['isMine'] as bool?,
      isMax: json['isMax'] as bool?,
      isLuck: json['isLuck'] as bool?,
      luckAmount: (json['luckAmount'] as num?)?.toInt(),
      time: (json['time'] as num?)?.toInt(),
      createTime: (json['createTime'] as num?)?.toInt(),
      uid: json['uid'],
      username: json['username'] as String?,
      id: json['id'] as String?,
    );

Map<String, dynamic> _$GrabRedBagModelToJson(GrabRedBagModel instance) =>
    <String, dynamic>{
      'redBagId': instance.redBagId,
      'miniRedBagAmount': instance.miniRedBagAmount,
      'isMine': instance.isMine,
      'isMax': instance.isMax,
      'isLuck': instance.isLuck,
      'luckAmount': instance.luckAmount,
      'time': instance.time,
      'createTime': instance.createTime,
      'uid': instance.uid,
      'username': instance.username,
      'id': instance.id,
    };
