// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lhcdoc_login_reg_page_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LhcdocLoginRegPageInfoModel _$LhcdocLoginRegPageInfoModelFromJson(
        Map<String, dynamic> json) =>
    LhcdocLoginRegPageInfoModel(
      logo: json['logo'] as String?,
      intro: json['intro'] as String?,
    );

Map<String, dynamic> _$LhcdocLoginRegPageInfoModelToJson(
        LhcdocLoginRegPageInfoModel instance) =>
    <String, dynamic>{
      'logo': instance.logo,
      'intro': instance.intro,
    };
