import 'package:json_annotation/json_annotation.dart';

part 'lhcdoc_login_reg_page_info_model.g.dart';

@JsonSerializable()
class LhcdocLoginRegPageInfoModel {
  final String? logo;
  final String? intro;

  const LhcdocLoginRegPageInfoModel({this.logo, this.intro});

  factory LhcdocLoginRegPageInfoModel.fromJson(Map<String, dynamic> json) {
    return _$LhcdocLoginRegPageInfoModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LhcdocLoginRegPageInfoModelToJson(this);

  LhcdocLoginRegPageInfoModel copyWith({
    String? logo,
    String? intro,
  }) {
    return LhcdocLoginRegPageInfoModel(
      logo: logo ?? this.logo,
      intro: intro ?? this.intro,
    );
  }
}
