import 'package:fpg_flutter/models/home_menu/home_menu_item_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'home_menu_model.g.dart';

// {
// "id": "1",
// "icon": "",
// "name": "优惠活动",
// "url": "",
// "category": "0",
// "levelType": "1",
// "sort": "0",
// "seriesId": "7",
// "subId": 9,
// "tipFlag": "0",
// "openWay": "0",
// "hotIcon": "",
// "gameCode": "-1",
// "subtitle": "",
// "list": {
// "subId": 9,
// "subtitle": "",
// "seriesId": "7",
// "name": "优惠活动",
// "gameId": 0,
// "realName": "",
// "title": "优惠活动"
// },
// "title": "优惠活动",
// "gameId": 9
// }

@JsonSerializable()
class HomeMenuModel {
  final String? id;
  final String? icon;
  final String? name;
  final String? url;
  final String? category;
  final String? levelType;
  final dynamic sort;
  final dynamic seriesId;
  final dynamic subId;
  final String? tipFlag;
  final String? openWay;
  final String? hotIcon;
  final String? gameCode;
  final String? subtitle;
  final String? title;
  final dynamic gameId;
  final HomeMenuItemModel? list;
  bool? isHot;
  bool? show;
  String? link;

  HomeMenuModel(
      {this.id,
      this.icon,
      this.name,
      this.url,
      this.category,
      this.levelType,
      this.sort,
      this.seriesId,
      this.subId,
      this.tipFlag,
      this.openWay,
      this.hotIcon,
      this.gameCode,
      this.subtitle,
      this.title,
      this.gameId,
      this.list,
      this.isHot,
      this.show = true,
      this.link});

  factory HomeMenuModel.fromJson(Map<String, dynamic> json) =>
      _$HomeMenuModelFromJson(json);

  Map<String, dynamic> toJson() => _$HomeMenuModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
