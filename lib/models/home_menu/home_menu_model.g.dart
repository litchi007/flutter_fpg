// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_menu_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeMenuModel _$HomeMenuModelFromJson(Map<String, dynamic> json) =>
    HomeMenuModel(
      id: json['id'] as String?,
      icon: json['icon'] as String?,
      name: json['name'] as String?,
      url: json['url'] as String?,
      category: json['category'] as String?,
      levelType: json['levelType'] as String?,
      sort: json['sort'],
      seriesId: json['seriesId'],
      subId: json['subId'],
      tipFlag: json['tipFlag'] as String?,
      openWay: json['openWay'] as String?,
      hotIcon: json['hotIcon'] as String?,
      gameCode: json['gameCode'] as String?,
      subtitle: json['subtitle'] as String?,
      title: json['title'] as String?,
      gameId: json['gameId'],
      list: json['list'] == null
          ? null
          : HomeMenuItemModel.fromJson(json['list'] as Map<String, dynamic>),
      isHot: json['isHot'] as bool?,
      show: json['show'] as bool? ?? true,
      link: json['link'] as String?,
    );

Map<String, dynamic> _$HomeMenuModelToJson(HomeMenuModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'icon': instance.icon,
      'name': instance.name,
      'url': instance.url,
      'category': instance.category,
      'levelType': instance.levelType,
      'sort': instance.sort,
      'seriesId': instance.seriesId,
      'subId': instance.subId,
      'tipFlag': instance.tipFlag,
      'openWay': instance.openWay,
      'hotIcon': instance.hotIcon,
      'gameCode': instance.gameCode,
      'subtitle': instance.subtitle,
      'title': instance.title,
      'gameId': instance.gameId,
      'list': instance.list,
      'isHot': instance.isHot,
      'show': instance.show,
      'link': instance.link,
    };
