import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'home_menu_item_model.g.dart';

//{
// "subId": 9,
// "subtitle": "",
// "seriesId": "7",
// "name": "优惠活动",
// "gameId": 0,
// "realName": "",
// "title": "优惠活动"
// "supportTrial": 0,
// "isInstant": "0",
// "isSeal": "0",
// "isClose": "0",
// "gameType": "cqssc",
// "logo": "https://wwwstatic01.fdgdggduydaa008aadsdf008.xyz/open_prize/images/icon/264.png?v=1727146878",
// "icon": "https://wwwstatic01.fdgdggduydaa008aadsdf008.xyz/open_prize/images/icon/264.png?v=1727146878"
//},

@JsonSerializable()
class HomeMenuItemModel {
  final dynamic subId;
  final String? subtitle;
  final dynamic seriesId;
  final String? name;
  final dynamic gameId;
  final String? realName;
  final String? title;
  final dynamic supportTrial;
  final String? isInstant;
  final String? isSeal;
  final String? isClose;
  final String? gameType;
  final String? logo;
  final String? icon;

  const HomeMenuItemModel(
      {this.realName,
      this.name,
      this.seriesId,
      this.subId,
      this.subtitle,
      this.title,
      this.gameId,
      this.supportTrial,
      this.isInstant,
      this.isSeal,
      this.isClose,
      this.gameType,
      this.logo,
      this.icon});

  factory HomeMenuItemModel.fromJson(Map<String, dynamic> json) =>
      _$HomeMenuItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$HomeMenuItemModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
