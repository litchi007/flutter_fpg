// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_menu_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeMenuItemModel _$HomeMenuItemModelFromJson(Map<String, dynamic> json) =>
    HomeMenuItemModel(
      realName: json['realName'] as String?,
      name: json['name'] as String?,
      seriesId: json['seriesId'],
      subId: json['subId'],
      subtitle: json['subtitle'] as String?,
      title: json['title'] as String?,
      gameId: json['gameId'],
      supportTrial: json['supportTrial'],
      isInstant: json['isInstant'] as String?,
      isSeal: json['isSeal'] as String?,
      isClose: json['isClose'] as String?,
      gameType: json['gameType'] as String?,
      logo: json['logo'] as String?,
      icon: json['icon'] as String?,
    );

Map<String, dynamic> _$HomeMenuItemModelToJson(HomeMenuItemModel instance) =>
    <String, dynamic>{
      'subId': instance.subId,
      'subtitle': instance.subtitle,
      'seriesId': instance.seriesId,
      'name': instance.name,
      'gameId': instance.gameId,
      'realName': instance.realName,
      'title': instance.title,
      'supportTrial': instance.supportTrial,
      'isInstant': instance.isInstant,
      'isSeal': instance.isSeal,
      'isClose': instance.isClose,
      'gameType': instance.gameType,
      'logo': instance.logo,
      'icon': instance.icon,
    };
