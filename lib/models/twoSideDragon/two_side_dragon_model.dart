import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';

part 'two_side_dragon_model.g.dart';

//{"gameId":"234","playCateId":"3022","playCateName":"第十名","playId":"7118254","playName":"双","count":9}

@JsonSerializable()
class TwoSideDragonModel {
  String? gameId;
  String? playCateId;
  String? playCateName;
  String? playId;
  String? playName;
  int? count;

  TwoSideDragonModel({
    this.gameId,
    this.playCateId,
    this.playCateName,
    this.playId,
    this.playName,
    this.count,
  });

  factory TwoSideDragonModel.fromJson(Map<String, dynamic> json) {
    return _$TwoSideDragonModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TwoSideDragonModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  TwoSideDragonModel copyWith(
      {String? gameId,
      String? playCateId,
      String? playCateName,
      String? playId,
      String? playName,
      int? count}) {
    return TwoSideDragonModel(
      gameId: gameId ?? this.gameId,
      playCateId: playCateId ?? this.playCateId,
      playCateName: playCateName ?? this.playCateName,
      playId: playId ?? this.playId,
      playName: playName ?? this.playName,
      count: count ?? this.count,
    );
  }
}
