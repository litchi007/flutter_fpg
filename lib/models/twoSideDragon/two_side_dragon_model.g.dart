// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'two_side_dragon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TwoSideDragonModel _$TwoSideDragonModelFromJson(Map<String, dynamic> json) =>
    TwoSideDragonModel(
      gameId: json['gameId'] as String?,
      playCateId: json['playCateId'] as String?,
      playCateName: json['playCateName'] as String?,
      playId: json['playId'] as String?,
      playName: json['playName'] as String?,
      count: (json['count'] as num?)?.toInt(),
    );

Map<String, dynamic> _$TwoSideDragonModelToJson(TwoSideDragonModel instance) =>
    <String, dynamic>{
      'gameId': instance.gameId,
      'playCateId': instance.playCateId,
      'playCateName': instance.playCateName,
      'playId': instance.playId,
      'playName': instance.playName,
      'count': instance.count,
    };
