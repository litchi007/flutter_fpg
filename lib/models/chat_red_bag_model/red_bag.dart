import 'package:json_annotation/json_annotation.dart';

part 'red_bag.g.dart';

@JsonSerializable()
class RedBag {
  String? roomId;
  dynamic uid;
  String? title;
  String? genre;
  String? amount;
  int? quantity;
  String? createDmlMultiple;
  String? grabDmlMultiple;
  String? grabLevels;
  int? oddsRate;
  int? mineNumber;
  int? createTime;
  int? expireTime;
  int? updateTime;
  int? systemPosition;
  int? status;
  int? isSend;
  String? id;
  @JsonKey(name: 'is_grab')
  int? isGrab;
  @JsonKey(name: 'grab_amount')
  int? grabAmount;
  List<dynamic>? grabList;

  RedBag({
    this.roomId,
    this.uid,
    this.title,
    this.genre,
    this.amount,
    this.quantity,
    this.createDmlMultiple,
    this.grabDmlMultiple,
    this.grabLevels,
    this.oddsRate,
    this.mineNumber,
    this.createTime,
    this.expireTime,
    this.updateTime,
    this.systemPosition,
    this.status,
    this.isSend,
    this.id,
    this.isGrab,
    this.grabAmount,
    this.grabList,
  });

  @override
  String toString() {
    return 'RedBag(roomId: $roomId, uid: $uid, title: $title, genre: $genre, amount: $amount, quantity: $quantity, createDmlMultiple: $createDmlMultiple, grabDmlMultiple: $grabDmlMultiple, grabLevels: $grabLevels, oddsRate: $oddsRate, mineNumber: $mineNumber, createTime: $createTime, expireTime: $expireTime, updateTime: $updateTime, systemPosition: $systemPosition, status: $status, isSend: $isSend, id: $id, isGrab: $isGrab, grabAmount: $grabAmount, grabList: $grabList)';
  }

  factory RedBag.fromJson(Map<String, dynamic> json) {
    return _$RedBagFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RedBagToJson(this);

  RedBag copyWith({
    String? roomId,
    dynamic uid,
    String? title,
    String? genre,
    String? amount,
    int? quantity,
    String? createDmlMultiple,
    String? grabDmlMultiple,
    String? grabLevels,
    int? oddsRate,
    int? mineNumber,
    int? createTime,
    int? expireTime,
    int? updateTime,
    int? systemPosition,
    int? status,
    int? isSend,
    String? id,
    int? isGrab,
    int? grabAmount,
    List<dynamic>? grabList,
  }) {
    return RedBag(
      roomId: roomId ?? this.roomId,
      uid: uid ?? this.uid,
      title: title ?? this.title,
      genre: genre ?? this.genre,
      amount: amount ?? this.amount,
      quantity: quantity ?? this.quantity,
      createDmlMultiple: createDmlMultiple ?? this.createDmlMultiple,
      grabDmlMultiple: grabDmlMultiple ?? this.grabDmlMultiple,
      grabLevels: grabLevels ?? this.grabLevels,
      oddsRate: oddsRate ?? this.oddsRate,
      mineNumber: mineNumber ?? this.mineNumber,
      createTime: createTime ?? this.createTime,
      expireTime: expireTime ?? this.expireTime,
      updateTime: updateTime ?? this.updateTime,
      systemPosition: systemPosition ?? this.systemPosition,
      status: status ?? this.status,
      isSend: isSend ?? this.isSend,
      id: id ?? this.id,
      isGrab: isGrab ?? this.isGrab,
      grabAmount: grabAmount ?? this.grabAmount,
      grabList: grabList ?? this.grabList,
    );
  }
}
