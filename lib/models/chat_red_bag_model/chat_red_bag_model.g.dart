// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_red_bag_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatRedBagModel _$ChatRedBagModelFromJson(Map<String, dynamic> json) =>
    ChatRedBagModel(
      code: json['code'] as String?,
      messageCode: json['messageCode'] as String?,
      roomId: json['roomId'] as String?,
      uid: json['uid'] as String?,
      time: json['time'] as String?,
      isManager: json['isManager'] as bool?,
      isTrial: (json['is_trial'] as num?)?.toInt(),
      ip: json['ip'] as String?,
      t: (json['t'] as num?)?.toInt(),
      createTime: (json['createTime'] as num?)?.toInt(),
      chatType: (json['chat_type'] as num?)?.toInt(),
      dataType: json['data_type'] as String?,
      isManagerIconTag: (json['isManagerIconTag'] as num?)?.toInt(),
      avator: json['avator'] as String?,
      avatarStatus: (json['avatar_status'] as num?)?.toInt(),
      username: json['username'] as String?,
      usernameBak: json['usernameBak'] as String?,
      levelImg: json['levelImg'] as String?,
      levelIconLeft: json['levelIconLeft'] as String?,
      levelIconRight: json['levelIconRight'] as String?,
      level: json['level'] as String?,
      redBag: json['redBag'] == null
          ? null
          : RedBag.fromJson(json['redBag'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatRedBagModelToJson(ChatRedBagModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'messageCode': instance.messageCode,
      'roomId': instance.roomId,
      'uid': instance.uid,
      'time': instance.time,
      'isManager': instance.isManager,
      'is_trial': instance.isTrial,
      'ip': instance.ip,
      't': instance.t,
      'createTime': instance.createTime,
      'chat_type': instance.chatType,
      'data_type': instance.dataType,
      'isManagerIconTag': instance.isManagerIconTag,
      'avator': instance.avator,
      'avatar_status': instance.avatarStatus,
      'username': instance.username,
      'usernameBak': instance.usernameBak,
      'levelImg': instance.levelImg,
      'levelIconLeft': instance.levelIconLeft,
      'levelIconRight': instance.levelIconRight,
      'level': instance.level,
      'redBag': instance.redBag,
    };
