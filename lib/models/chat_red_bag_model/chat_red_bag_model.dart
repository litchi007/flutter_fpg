import 'package:json_annotation/json_annotation.dart';

import 'red_bag.dart';

part 'chat_red_bag_model.g.dart';

@JsonSerializable()
class ChatRedBagModel {
  String? code;
  String? messageCode;
  String? roomId;
  String? uid;
  String? time;
  bool? isManager;
  @JsonKey(name: 'is_trial')
  int? isTrial;
  String? ip;
  int? t;
  int? createTime;
  @JsonKey(name: 'chat_type')
  int? chatType;
  @JsonKey(name: 'data_type')
  String? dataType;
  int? isManagerIconTag;
  String? avator;
  @JsonKey(name: 'avatar_status')
  int? avatarStatus;
  String? username;
  String? usernameBak;
  String? levelImg;
  String? levelIconLeft;
  String? levelIconRight;
  String? level;
  RedBag? redBag;

  ChatRedBagModel({
    this.code,
    this.messageCode,
    this.roomId,
    this.uid,
    this.time,
    this.isManager,
    this.isTrial,
    this.ip,
    this.t,
    this.createTime,
    this.chatType,
    this.dataType,
    this.isManagerIconTag,
    this.avator,
    this.avatarStatus,
    this.username,
    this.usernameBak,
    this.levelImg,
    this.levelIconLeft,
    this.levelIconRight,
    this.level,
    this.redBag,
  });

  @override
  String toString() {
    return 'ChatRedBagModel(code: $code, messageCode: $messageCode, roomId: $roomId, uid: $uid, time: $time, isManager: $isManager, isTrial: $isTrial, ip: $ip, t: $t, createTime: $createTime, chatType: $chatType, dataType: $dataType, isManagerIconTag: $isManagerIconTag, avator: $avator, avatarStatus: $avatarStatus, username: $username, usernameBak: $usernameBak, levelImg: $levelImg, levelIconLeft: $levelIconLeft, levelIconRight: $levelIconRight, level: $level, redBag: $redBag)';
  }

  factory ChatRedBagModel.fromJson(Map<String, dynamic> json) {
    return _$ChatRedBagModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatRedBagModelToJson(this);

  ChatRedBagModel copyWith({
    String? code,
    String? messageCode,
    String? roomId,
    String? uid,
    String? time,
    bool? isManager,
    int? isTrial,
    String? ip,
    int? t,
    int? createTime,
    int? chatType,
    String? dataType,
    int? isManagerIconTag,
    String? avator,
    int? avatarStatus,
    String? username,
    String? usernameBak,
    String? levelImg,
    String? levelIconLeft,
    String? levelIconRight,
    String? level,
    RedBag? redBag,
  }) {
    return ChatRedBagModel(
      code: code ?? this.code,
      messageCode: messageCode ?? this.messageCode,
      roomId: roomId ?? this.roomId,
      uid: uid ?? this.uid,
      time: time ?? this.time,
      isManager: isManager ?? this.isManager,
      isTrial: isTrial ?? this.isTrial,
      ip: ip ?? this.ip,
      t: t ?? this.t,
      createTime: createTime ?? this.createTime,
      chatType: chatType ?? this.chatType,
      dataType: dataType ?? this.dataType,
      isManagerIconTag: isManagerIconTag ?? this.isManagerIconTag,
      avator: avator ?? this.avator,
      avatarStatus: avatarStatus ?? this.avatarStatus,
      username: username ?? this.username,
      usernameBak: usernameBak ?? this.usernameBak,
      levelImg: levelImg ?? this.levelImg,
      levelIconLeft: levelIconLeft ?? this.levelIconLeft,
      levelIconRight: levelIconRight ?? this.levelIconRight,
      level: level ?? this.level,
      redBag: redBag ?? this.redBag,
    );
  }
}
