import 'package:json_annotation/json_annotation.dart';
import 'lottery_history_model.dart';
import 'dart:convert';
part 'lottery_history_list_model.g.dart';

@JsonSerializable()
class LotteryHistoryListModel {
  final List<LotteryHistoryModel>? list;
  final int? total;

  /// 数据总数
  final String? totalBetAmount;

  /// 总下注金额
  final String? totalWinAmount;

  /// 输赢总金额

  const LotteryHistoryListModel(
      {this.list, this.total, this.totalBetAmount, this.totalWinAmount});

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory LotteryHistoryListModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryHistoryListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryHistoryListModelToJson(this);

  LotteryHistoryListModel copyWith({
    List<LotteryHistoryModel>? list,
    int? total,
    String? totalBetAmount,
    String? totalWinAmount,
  }) {
    return LotteryHistoryListModel(
      list: list ?? this.list,
      total: total ?? this.total,
      totalBetAmount: totalBetAmount ?? this.totalBetAmount,
      totalWinAmount: totalWinAmount ?? this.totalWinAmount,
    );
  }
}
