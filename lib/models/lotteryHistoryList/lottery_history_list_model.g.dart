// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_history_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryHistoryListModel _$LotteryHistoryListModelFromJson(
        Map<String, dynamic> json) =>
    LotteryHistoryListModel(
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => LotteryHistoryModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      total: (json['total'] as num?)?.toInt(),
      totalBetAmount: json['totalBetAmount'] as String?,
      totalWinAmount: json['totalWinAmount'] as String?,
    );

Map<String, dynamic> _$LotteryHistoryListModelToJson(
        LotteryHistoryListModel instance) =>
    <String, dynamic>{
      'list': instance.list,
      'total': instance.total,
      'totalBetAmount': instance.totalBetAmount,
      'totalWinAmount': instance.totalWinAmount,
    };
