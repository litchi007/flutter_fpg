import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'lottery_history_model.g.dart';

@JsonSerializable()
class LotteryHistoryModel {
  final String? id;

  /// 注单ID
  final String? gameName;

  /// 游戏名称（彩种）
  final String? issue;

  /// 彩票期号（彩票)
  final String? displayNumber;

  /// 开奖期数  自营优先使用
  final String? playGroupName;

  /// 游戏分类名称（彩票）
  final String? playName;

  /// 游戏玩法（彩票）
  final String? lotteryNo;

  /// 开奖号（彩票）
  final dynamic betAmount;

  /// 下注金额
  final dynamic betCount;

  /// 笔数
  final String? rebateRate;

  /// 輸贏
  final String? rebateAmount;

  /// 退水金额
  final dynamic expectAmount;

  /// 可赢金额
  final dynamic winCount;

  /// 中奖笔数
  final dynamic winAmount;

  /// 输赢金额
  final dynamic settleAmount;

  /// 结算金额 （彩票）
  final String? odds;

  ///  赔率（彩票）
  final String? betInfo;

  /// 注单状态 1=待开奖，2=已中奖，3=未中奖，4=已撤单
  final dynamic status;

  /// 下注时间
  final String? betTime;

  /// 是否允许撤单
  final bool? isAllowCancel;

  final String? gameType;
  final String? gameId;

  const LotteryHistoryModel({
    this.id,
    this.gameName,
    this.issue,
    this.displayNumber,
    this.playGroupName,
    this.playName,
    this.lotteryNo,
    this.betAmount,
    this.betCount,
    this.rebateRate,
    this.rebateAmount,
    this.expectAmount,
    this.winCount,
    this.winAmount,
    this.settleAmount,
    this.odds,
    this.betInfo,
    this.status,
    this.betTime,
    this.isAllowCancel,
    this.gameType,
    this.gameId,
  });

  factory LotteryHistoryModel.fromJson(Map<String, dynamic> json) =>
      _$LotteryHistoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$LotteryHistoryModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  LotteryHistoryModel copyWith({
    String? id,
    String? gameName,
    String? issue,
    String? displayNumber,
    String? playGroupName,
    String? playName,
    String? lotteryNo,
    String? betAmount,
    String? betCount,
    String? rebateRate,
    String? rebateAmount,
    String? expectAmount,
    String? winCount,
    String? winAmount,
    String? settleAmount,
    String? odds,
    String? betInfo,
    String? status,
    String? betTime,
    bool? isAllowCancel,
    String? gameType,
    String? gameId,
  }) {
    return LotteryHistoryModel(
      id: id ?? this.id,
      gameName: gameName ?? this.gameName,
      issue: issue ?? this.issue,
      displayNumber: displayNumber ?? this.displayNumber,
      playGroupName: playGroupName ?? this.playGroupName,
      playName: playName ?? this.playName,
      lotteryNo: lotteryNo ?? this.lotteryNo,
      betAmount: betAmount ?? this.betAmount,
      betCount: betCount ?? this.betCount,
      rebateRate: rebateRate ?? this.rebateRate,
      rebateAmount: rebateAmount ?? this.rebateAmount,
      expectAmount: expectAmount ?? this.expectAmount,
      winCount: winCount ?? this.winCount,
      winAmount: winAmount ?? this.winAmount,
      settleAmount: settleAmount ?? this.settleAmount,
      odds: gameType ?? this.odds,
      betInfo: betInfo ?? this.betInfo,
      status: status ?? this.status,
      betTime: betTime ?? this.betTime,
      isAllowCancel: isAllowCancel ?? this.isAllowCancel,
      gameType: gameType ?? this.gameType,
      gameId: gameId ?? this.gameId,
    );
  }
}
