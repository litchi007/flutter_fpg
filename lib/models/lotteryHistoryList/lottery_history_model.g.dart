// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_history_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryHistoryModel _$LotteryHistoryModelFromJson(Map<String, dynamic> json) =>
    LotteryHistoryModel(
      id: json['id'] as String?,
      gameName: json['gameName'] as String?,
      issue: json['issue'] as String?,
      displayNumber: json['displayNumber'] as String?,
      playGroupName: json['playGroupName'] as String?,
      playName: json['playName'] as String?,
      lotteryNo: json['lotteryNo'] as String?,
      betAmount: json['betAmount'],
      betCount: json['betCount'],
      rebateRate: json['rebateRate'] as String?,
      rebateAmount: json['rebateAmount'] as String?,
      expectAmount: json['expectAmount'],
      winCount: json['winCount'],
      winAmount: json['winAmount'],
      settleAmount: json['settleAmount'],
      odds: json['odds'] as String?,
      betInfo: json['betInfo'] as String?,
      status: json['status'],
      betTime: json['betTime'] as String?,
      isAllowCancel: json['isAllowCancel'] as bool?,
      gameType: json['gameType'] as String?,
      gameId: json['gameId'] as String?,
    );

Map<String, dynamic> _$LotteryHistoryModelToJson(
        LotteryHistoryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'gameName': instance.gameName,
      'issue': instance.issue,
      'displayNumber': instance.displayNumber,
      'playGroupName': instance.playGroupName,
      'playName': instance.playName,
      'lotteryNo': instance.lotteryNo,
      'betAmount': instance.betAmount,
      'betCount': instance.betCount,
      'rebateRate': instance.rebateRate,
      'rebateAmount': instance.rebateAmount,
      'expectAmount': instance.expectAmount,
      'winCount': instance.winCount,
      'winAmount': instance.winAmount,
      'settleAmount': instance.settleAmount,
      'odds': instance.odds,
      'betInfo': instance.betInfo,
      'status': instance.status,
      'betTime': instance.betTime,
      'isAllowCancel': instance.isAllowCancel,
      'gameType': instance.gameType,
      'gameId': instance.gameId,
    };
