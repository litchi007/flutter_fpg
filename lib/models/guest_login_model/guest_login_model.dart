import 'package:json_annotation/json_annotation.dart';

part 'guest_login_model.g.dart';

@JsonSerializable()
class GuestLoginModel {
  @JsonKey(name: 'API-SID')
  final String? apiSid;
  @JsonKey(name: 'API-TOKEN')
  final String? apiToken;

  const GuestLoginModel({this.apiSid, this.apiToken});

  @override
  String toString() {
    return 'GuestLoginModel(apiSid: $apiSid, apiToken: $apiToken)';
  }

  factory GuestLoginModel.fromJson(Map<String, dynamic> json) {
    return _$GuestLoginModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GuestLoginModelToJson(this);

  GuestLoginModel copyWith({
    String? apiSid,
    String? apiToken,
  }) {
    return GuestLoginModel(
      apiSid: apiSid ?? this.apiSid,
      apiToken: apiToken ?? this.apiToken,
    );
  }
}
