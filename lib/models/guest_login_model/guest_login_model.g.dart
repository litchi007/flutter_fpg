// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'guest_login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GuestLoginModel _$GuestLoginModelFromJson(Map<String, dynamic> json) =>
    GuestLoginModel(
      apiSid: json['API-SID'] as String?,
      apiToken: json['API-TOKEN'] as String?,
    );

Map<String, dynamic> _$GuestLoginModelToJson(GuestLoginModel instance) =>
    <String, dynamic>{
      'API-SID': instance.apiSid,
      'API-TOKEN': instance.apiToken,
    };
