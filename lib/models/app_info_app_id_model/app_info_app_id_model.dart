import 'package:json_annotation/json_annotation.dart';

part 'app_info_app_id_model.g.dart';

@JsonSerializable()
class AppInfoAppIdModel {
  final String? id;
  @JsonKey(name: 'app_name')
  final String? appName;
  @JsonKey(name: 'package_domains')
  final List<String>? packageDomains;
  @JsonKey(name: 'check_package_domains')
  final String? checkPackageDomains;
  @JsonKey(name: 'site_number')
  final String? siteNumber;
  @JsonKey(name: 'easy_domain')
  final String? easyDomain;
  @JsonKey(name: 'site_url')
  final String? siteUrl;
  final String? version;
  @JsonKey(name: 'packag_url')
  final String? packagUrl;
  @JsonKey(name: 'main_domains')
  final String? mainDomains;
  @JsonKey(name: 'start_img')
  final String? startImg;
  @JsonKey(name: 'cover_img')
  final String? coverImg;
  final String? logo;
  final String? qrcode;
  final String? type;
  @JsonKey(name: 'expire_time')
  final String? expireTime;
  @JsonKey(name: 'wgt_url')
  final String? wgtUrl;
  @JsonKey(name: 'is_supersign')
  final String? isSupersign;
  @JsonKey(name: 'super_sign_url')
  final String? superSignUrl;
  @JsonKey(name: 'is_disable')
  final String? isDisable;
  @JsonKey(name: 'ios_plist')
  final String? iosPlist;
  @JsonKey(name: 'android_pack')
  final String? androidPack;
  @JsonKey(name: 'ios_pack')
  final String? iosPack;
  @JsonKey(name: 'app_type')
  final String? appType;
  @JsonKey(name: 'alert_log_level')
  final String? alertLogLevel;

  const AppInfoAppIdModel({
    this.id,
    this.appName,
    this.packageDomains,
    this.checkPackageDomains,
    this.siteNumber,
    this.easyDomain,
    this.siteUrl,
    this.version,
    this.packagUrl,
    this.mainDomains,
    this.startImg,
    this.coverImg,
    this.logo,
    this.qrcode,
    this.type,
    this.expireTime,
    this.wgtUrl,
    this.isSupersign,
    this.superSignUrl,
    this.isDisable,
    this.iosPlist,
    this.androidPack,
    this.iosPack,
    this.appType,
    this.alertLogLevel,
  });

  @override
  String toString() {
    return 'AppInfoAppIdModel(id: $id, appName: $appName, packageDomains: $packageDomains, checkPackageDomains: $checkPackageDomains, siteNumber: $siteNumber, easyDomain: $easyDomain, siteUrl: $siteUrl, version: $version, packagUrl: $packagUrl, mainDomains: $mainDomains, startImg: $startImg, coverImg: $coverImg, logo: $logo, qrcode: $qrcode, type: $type, expireTime: $expireTime, wgtUrl: $wgtUrl, isSupersign: $isSupersign, superSignUrl: $superSignUrl, isDisable: $isDisable, iosPlist: $iosPlist, androidPack: $androidPack, iosPack: $iosPack, appType: $appType, alertLogLevel: $alertLogLevel)';
  }

  factory AppInfoAppIdModel.fromJson(Map<String, dynamic> json) {
    return _$AppInfoAppIdModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AppInfoAppIdModelToJson(this);

  AppInfoAppIdModel copyWith({
    String? id,
    String? appName,
    List<String>? packageDomains,
    String? checkPackageDomains,
    String? siteNumber,
    String? easyDomain,
    String? siteUrl,
    String? version,
    String? packagUrl,
    String? mainDomains,
    String? startImg,
    String? coverImg,
    String? logo,
    String? qrcode,
    String? type,
    String? expireTime,
    String? wgtUrl,
    String? isSupersign,
    String? superSignUrl,
    String? isDisable,
    String? iosPlist,
    String? androidPack,
    String? iosPack,
    String? appType,
    String? alertLogLevel,
  }) {
    return AppInfoAppIdModel(
      id: id ?? this.id,
      appName: appName ?? this.appName,
      packageDomains: packageDomains ?? this.packageDomains,
      checkPackageDomains: checkPackageDomains ?? this.checkPackageDomains,
      siteNumber: siteNumber ?? this.siteNumber,
      easyDomain: easyDomain ?? this.easyDomain,
      siteUrl: siteUrl ?? this.siteUrl,
      version: version ?? this.version,
      packagUrl: packagUrl ?? this.packagUrl,
      mainDomains: mainDomains ?? this.mainDomains,
      startImg: startImg ?? this.startImg,
      coverImg: coverImg ?? this.coverImg,
      logo: logo ?? this.logo,
      qrcode: qrcode ?? this.qrcode,
      type: type ?? this.type,
      expireTime: expireTime ?? this.expireTime,
      wgtUrl: wgtUrl ?? this.wgtUrl,
      isSupersign: isSupersign ?? this.isSupersign,
      superSignUrl: superSignUrl ?? this.superSignUrl,
      isDisable: isDisable ?? this.isDisable,
      iosPlist: iosPlist ?? this.iosPlist,
      androidPack: androidPack ?? this.androidPack,
      iosPack: iosPack ?? this.iosPack,
      appType: appType ?? this.appType,
      alertLogLevel: alertLogLevel ?? this.alertLogLevel,
    );
  }
}
