// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_info_app_id_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppInfoAppIdModel _$AppInfoAppIdModelFromJson(Map<String, dynamic> json) =>
    AppInfoAppIdModel(
      id: json['id'] as String?,
      appName: json['app_name'] as String?,
      packageDomains: (json['package_domains'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      checkPackageDomains: json['check_package_domains'] as String?,
      siteNumber: json['site_number'] as String?,
      easyDomain: json['easy_domain'] as String?,
      siteUrl: json['site_url'] as String?,
      version: json['version'] as String?,
      packagUrl: json['packag_url'] as String?,
      mainDomains: json['main_domains'] as String?,
      startImg: json['start_img'] as String?,
      coverImg: json['cover_img'] as String?,
      logo: json['logo'] as String?,
      qrcode: json['qrcode'] as String?,
      type: json['type'] as String?,
      expireTime: json['expire_time'] as String?,
      wgtUrl: json['wgt_url'] as String?,
      isSupersign: json['is_supersign'] as String?,
      superSignUrl: json['super_sign_url'] as String?,
      isDisable: json['is_disable'] as String?,
      iosPlist: json['ios_plist'] as String?,
      androidPack: json['android_pack'] as String?,
      iosPack: json['ios_pack'] as String?,
      appType: json['app_type'] as String?,
      alertLogLevel: json['alert_log_level'] as String?,
    );

Map<String, dynamic> _$AppInfoAppIdModelToJson(AppInfoAppIdModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'app_name': instance.appName,
      'package_domains': instance.packageDomains,
      'check_package_domains': instance.checkPackageDomains,
      'site_number': instance.siteNumber,
      'easy_domain': instance.easyDomain,
      'site_url': instance.siteUrl,
      'version': instance.version,
      'packag_url': instance.packagUrl,
      'main_domains': instance.mainDomains,
      'start_img': instance.startImg,
      'cover_img': instance.coverImg,
      'logo': instance.logo,
      'qrcode': instance.qrcode,
      'type': instance.type,
      'expire_time': instance.expireTime,
      'wgt_url': instance.wgtUrl,
      'is_supersign': instance.isSupersign,
      'super_sign_url': instance.superSignUrl,
      'is_disable': instance.isDisable,
      'ios_plist': instance.iosPlist,
      'android_pack': instance.androidPack,
      'ios_pack': instance.iosPack,
      'app_type': instance.appType,
      'alert_log_level': instance.alertLogLevel,
    };
