// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_my_bet_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryMyBetModel _$LotteryMyBetModelFromJson(Map<String, dynamic> json) =>
    LotteryMyBetModel(
      id: json['id'],
      title: json['title'] as String?,
      uid: json['uid'],
      usr: json['usr'] as String?,
      isTest: json['isTest'] as String?,
      gameId: json['gameId'],
      playId: json['playId'],
      playCateId: json['playCateId'],
      addTime: json['addTime'] as String?,
      displayNumber: json['displayNumber'] as String?,
      issue: json['issue'] as String?,
      bonus: json['bonus'] as String?,
      odds: json['odds'],
      rebate: json['rebate'],
      betInfo: json['betInfo'] as String?,
      multiple: json['multiple'],
      status: json['status'],
      rebateMoney: json['rebateMoney'],
      resultMoney: json['resultMoney'],
      msg: json['msg'] as String?,
      isWin: json['isWin'],
      money: json['money'],
      playName: json['play_name'] as String?,
      playAlias: json['play_alias'] as String?,
      groupName: json['group_name'] as String?,
      orderNo: json['orderNo'] as String?,
      lotteryNo: json['lotteryNo'] as String?,
      openTime: json['openTime'] as String?,
      logo: json['logo'] as String?,
      isAllowCancel: json['isAllowCancel'] as bool?,
    );

Map<String, dynamic> _$LotteryMyBetModelToJson(LotteryMyBetModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'uid': instance.uid,
      'usr': instance.usr,
      'isTest': instance.isTest,
      'gameId': instance.gameId,
      'playId': instance.playId,
      'playCateId': instance.playCateId,
      'addTime': instance.addTime,
      'displayNumber': instance.displayNumber,
      'issue': instance.issue,
      'bonus': instance.bonus,
      'odds': instance.odds,
      'rebate': instance.rebate,
      'betInfo': instance.betInfo,
      'multiple': instance.multiple,
      'status': instance.status,
      'rebateMoney': instance.rebateMoney,
      'resultMoney': instance.resultMoney,
      'msg': instance.msg,
      'isWin': instance.isWin,
      'money': instance.money,
      'play_name': instance.playName,
      'play_alias': instance.playAlias,
      'group_name': instance.groupName,
      'orderNo': instance.orderNo,
      'lotteryNo': instance.lotteryNo,
      'openTime': instance.openTime,
      'logo': instance.logo,
      'isAllowCancel': instance.isAllowCancel,
    };
