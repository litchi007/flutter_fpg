import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
part 'lottery_my_bet_model.g.dart';

@JsonSerializable()
class LotteryMyBetModel {
  final dynamic id;

  /// 注单ID
  final String? title;
  final dynamic uid;
  final String? usr;
  final String? isTest;
  final dynamic gameId;
  final dynamic playId;
  final dynamic playCateId;
  final String? addTime;
  final String? displayNumber;

  /// 开奖期数  自营优先使用
  final String? issue;

  /// 彩票期号（彩票)
  final String? bonus;
  final dynamic odds;

  ///  赔率（彩票）
  final dynamic rebate;

  /// 輸贏
  final String? betInfo;
  final dynamic multiple;

  /// 注单状态 1=待开奖，2=已中奖，3=未中奖，4=已撤单
  final dynamic status;
  final dynamic rebateMoney;

  /// 退水金额
  final dynamic resultMoney;
  final String? msg;
  final dynamic isWin;
  final dynamic money;
  @JsonKey(name: 'play_name')
  final String? playName;

  /// 游戏分类名称（彩票）
  @JsonKey(name: 'play_alias')
  final String? playAlias;
  @JsonKey(name: 'group_name')
  final String? groupName;
  final String? orderNo;
  final String? lotteryNo;
  final String? openTime;
  final String? logo;
  final bool? isAllowCancel;

  /// 是否允许撤单

  const LotteryMyBetModel({
    this.id,
    this.title,
    this.uid,
    this.usr,
    this.isTest,
    this.gameId,
    this.playId,
    this.playCateId,
    this.addTime,
    this.displayNumber,
    this.issue,
    this.bonus,
    this.odds,
    this.rebate,
    this.betInfo,
    this.multiple,
    this.status,
    this.rebateMoney,
    this.resultMoney,
    this.msg,
    this.isWin,
    this.money,
    this.playName,
    this.playAlias,
    this.groupName,
    this.orderNo,
    this.lotteryNo,
    this.openTime,
    this.logo,
    this.isAllowCancel,
  });

  factory LotteryMyBetModel.fromJson(Map<String, dynamic> json) =>
      _$LotteryMyBetModelFromJson(json);

  Map<String, dynamic> toJson() => _$LotteryMyBetModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  LotteryMyBetModel copyWith({
    dynamic id,
    String? title,
    dynamic uid,
    String? usr,
    String? isTest,
    dynamic gameId,
    dynamic playId,
    dynamic playCateId,
    String? addTime,
    String? displayNumber,
    String? issue,
    String? bonus,
    dynamic odds,
    dynamic rebate,
    String? betInfo,
    dynamic multiple,
    dynamic status,
    dynamic rebateMoney,
    dynamic resultMoney,
    String? msg,
    String? isWin,
    dynamic money,
    String? playName,
    String? playAlias,
    String? groupName,
    dynamic orderNo,
    dynamic lotteryNo,
    String? openTime,
    String? logo,
    bool? isAllowCancel,
  }) {
    return LotteryMyBetModel(
      id: id ?? this.id,
      title: title ?? this.title,
      uid: uid ?? this.uid,
      usr: usr ?? this.usr,
      isTest: isTest ?? this.isTest,
      gameId: gameId ?? this.gameId,
      playId: playId ?? this.playId,
      playCateId: playCateId ?? this.playCateId,
      addTime: addTime ?? this.addTime,
      displayNumber: displayNumber ?? this.displayNumber,
      issue: issue ?? this.issue,
      bonus: bonus ?? this.bonus,
      odds: odds ?? this.odds,
      rebate: rebate ?? this.rebate,
      betInfo: betInfo ?? this.betInfo,
      multiple: multiple ?? this.multiple,
      status: status ?? this.status,
      rebateMoney: rebateMoney ?? this.rebateMoney,
      resultMoney: resultMoney ?? this.resultMoney,
      msg: msg ?? this.msg,
      isWin: isWin ?? this.isWin,
      money: money ?? this.money,
      playName: playName ?? this.playName,
      playAlias: playAlias ?? this.playAlias,
      groupName: groupName ?? this.groupName,
      orderNo: orderNo ?? this.orderNo,
      lotteryNo: lotteryNo ?? this.lotteryNo,
      openTime: openTime ?? this.openTime,
      logo: logo ?? this.logo,
      isAllowCancel: isAllowCancel ?? this.isAllowCancel,
    );
  }
}
