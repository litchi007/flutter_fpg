// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_switch_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanSwitchModel _$ExpertPlanSwitchModelFromJson(
        Map<String, dynamic> json) =>
    ExpertPlanSwitchModel(
      autoFollowStatus: (json['autoFollowStatus'] as num?)?.toInt(),
      enable: (json['enable'] as num?)?.toInt(),
      codeArea: (json['codeArea'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      gameArr: (json['gameArr'] as List<dynamic>?)
          ?.map((e) => ExpertPlanGameModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ExpertPlanSwitchModelToJson(
        ExpertPlanSwitchModel instance) =>
    <String, dynamic>{
      'autoFollowStatus': instance.autoFollowStatus,
      'enable': instance.enable,
      'codeArea': instance.codeArea,
      'gameArr': instance.gameArr,
    };
