// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_follow_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanFollowInfoModel _$ExpertPlanFollowInfoModelFromJson(
        Map<String, dynamic> json) =>
    ExpertPlanFollowInfoModel(
      statement: json['statement'] as Map<String, dynamic>?,
      taskList: (json['taskList'] as List<dynamic>?)
          ?.map((e) =>
              ExpertPlanFollowTaskModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ExpertPlanFollowInfoModelToJson(
        ExpertPlanFollowInfoModel instance) =>
    <String, dynamic>{
      'statement': instance.statement,
      'taskList': instance.taskList,
    };
