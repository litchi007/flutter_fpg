import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_game_model.g.dart';

// {
// "gameName": "澳洲幸运5",
// "gameId": "26"
// }

@JsonSerializable()
class ExpertPlanGameModel {
  final String? gameName;
  final String? gameId;

  const ExpertPlanGameModel({
    this.gameName,
    this.gameId,
  });

  factory ExpertPlanGameModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanGameModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanGameModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanGameModel copyWith({
    String? gameName,
    String? gameId,
  }) {
    return ExpertPlanGameModel(
      gameName: gameName ?? this.gameName,
      gameId: gameId ?? this.gameId,
    );
  }
}
