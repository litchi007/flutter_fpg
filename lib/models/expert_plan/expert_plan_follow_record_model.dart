import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_follow_record_model.g.dart';

// {
// "uid": "3",
// "id": "7",
// "position": "1",
// "length": "3",
// "issueCount": 4,
// "ename": "专家2号",
// "position_text": "特肖",
// "length_text": "3码"
// }

@JsonSerializable()
class ExpertPlanFollowRecordModel {
  final String? uid;
  final String? id;
  @JsonKey(name: 'ename')
  final String? name;
  final dynamic issueCount;
  final String? length;
  final String? position;
  @JsonKey(name: 'position_text')
  final String? positionText;
  @JsonKey(name: 'length_text')
  final String? lengthText;

  const ExpertPlanFollowRecordModel({
    this.uid,
    this.id,
    this.name,
    this.issueCount,
    this.length,
    this.position,
    this.positionText,
    this.lengthText,
  });

  factory ExpertPlanFollowRecordModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanFollowRecordModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanFollowRecordModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanFollowRecordModel copyWith(
      {String? uid,
      String? id,
      String? name,
      String? issueCount,
      String? length,
      String? position,
      String? positionText,
      String? lengthText}) {
    return ExpertPlanFollowRecordModel(
        uid: uid ?? this.uid,
        id: id ?? this.id,
        name: name ?? this.name,
        issueCount: issueCount ?? this.issueCount,
        length: length ?? this.length,
        position: position ?? this.position,
        positionText: positionText ?? this.positionText,
        lengthText: lengthText ?? this.lengthText);
  }
}
