// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_game_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanGameModel _$ExpertPlanGameModelFromJson(Map<String, dynamic> json) =>
    ExpertPlanGameModel(
      gameName: json['gameName'] as String?,
      gameId: json['gameId'] as String?,
    );

Map<String, dynamic> _$ExpertPlanGameModelToJson(
        ExpertPlanGameModel instance) =>
    <String, dynamic>{
      'gameName': instance.gameName,
      'gameId': instance.gameId,
    };
