import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_follow_issue_model.g.dart';

// {
// "issue": 21144615,
// "issue_displayNumber": 21144615,
// "startTime": 1724916181,
// "startDate": "2024-08-29 15:23:01",
// "endTime": 1724916475,
// "endDate": "2024-08-29 15:27:55",
// "actionTime": "2024-08-29 15:28:00",
// "lotteryTime": 1724916480,
// "lotteryDate": "2024-08-29 15:28:00"
// }

@JsonSerializable()
class ExpertPlanFollowIssueModel {
  final String? issue;
  @JsonKey(name: 'issue_displayNumber')
  final String? displayNumber;
  final dynamic startTime;
  final String? startDate;
  final dynamic endTime;
  final String? endDate;
  final String? actionTime;
  final dynamic lotteryTime;
  final String? lotteryDate;
  String? betType;
  String? betMoney;

  ExpertPlanFollowIssueModel({
    this.issue,
    this.displayNumber,
    this.startTime,
    this.startDate,
    this.endTime,
    this.endDate,
    this.actionTime,
    this.lotteryTime,
    this.lotteryDate,
  });

  factory ExpertPlanFollowIssueModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanFollowIssueModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanFollowIssueModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanFollowIssueModel copyWith(
      {String? issue,
      String? displayNumber,
      String? startTime,
      String? startDate,
      String? endTime,
      String? endDate,
      String? actionTime,
      String? lotteryTime,
      String? lotteryDate}) {
    return ExpertPlanFollowIssueModel(
        issue: issue ?? this.issue,
        displayNumber: displayNumber ?? this.displayNumber,
        startTime: startTime ?? this.startTime,
        startDate: startDate ?? this.startDate,
        endTime: endTime ?? this.endTime,
        endDate: endDate ?? this.endDate,
        actionTime: actionTime ?? this.actionTime,
        lotteryTime: lotteryTime ?? this.lotteryTime,
        lotteryDate: displayNumber ?? this.lotteryDate);
  }
}
