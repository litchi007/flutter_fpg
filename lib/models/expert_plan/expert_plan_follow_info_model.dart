import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_task_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_follow_info_model.g.dart';

// {
// "taskList": [
// {
// "id": "56",
// "issue": "21144619",
// "betType": "2",
// "money": 1
// },
// {
// "id": "57",
// "issue": "21144620",
// "betType": "1",
// "money": 1
// }
// ],
// "statement": {
// "taskCount": 2
// }
// }

@JsonSerializable()
class ExpertPlanFollowInfoModel {
  final Map<String, dynamic>? statement;
  final List<ExpertPlanFollowTaskModel>? taskList;

  const ExpertPlanFollowInfoModel({this.statement, this.taskList});

  factory ExpertPlanFollowInfoModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanFollowInfoModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanFollowInfoModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanFollowInfoModel copyWith({
    Map<String, dynamic>? statement,
    List<ExpertPlanFollowTaskModel>? taskList,
  }) {
    return ExpertPlanFollowInfoModel(
        statement: statement ?? this.statement,
        taskList: taskList ?? this.taskList);
  }
}
