// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_follow_task_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanFollowTaskModel _$ExpertPlanFollowTaskModelFromJson(
        Map<String, dynamic> json) =>
    ExpertPlanFollowTaskModel(
      id: json['id'] as String?,
      issue: json['issue'] as String?,
      betType: json['betType'] as String?,
      money: json['money'],
    );

Map<String, dynamic> _$ExpertPlanFollowTaskModelToJson(
        ExpertPlanFollowTaskModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'issue': instance.issue,
      'betType': instance.betType,
      'money': instance.money,
    };
