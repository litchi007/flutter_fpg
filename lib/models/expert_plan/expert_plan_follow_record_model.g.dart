// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_follow_record_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanFollowRecordModel _$ExpertPlanFollowRecordModelFromJson(
        Map<String, dynamic> json) =>
    ExpertPlanFollowRecordModel(
      uid: json['uid'] as String?,
      id: json['id'] as String?,
      name: json['ename'] as String?,
      issueCount: json['issueCount'],
      length: json['length'] as String?,
      position: json['position'] as String?,
      positionText: json['position_text'] as String?,
      lengthText: json['length_text'] as String?,
    );

Map<String, dynamic> _$ExpertPlanFollowRecordModelToJson(
        ExpertPlanFollowRecordModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'id': instance.id,
      'ename': instance.name,
      'issueCount': instance.issueCount,
      'length': instance.length,
      'position': instance.position,
      'position_text': instance.positionText,
      'length_text': instance.lengthText,
    };
