// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanDataModel _$ExpertPlanDataModelFromJson(Map<String, dynamic> json) =>
    ExpertPlanDataModel(
      eid: json['eid'] as String?,
      name: json['name'] as String?,
      position: json['position'] as String?,
      ycLength: json['yc_length'] as String?,
      ycData:
          (json['yc_data'] as List<dynamic>?)?.map((e) => e as String).toList(),
      playIds:
          (json['playIds'] as List<dynamic>?)?.map((e) => e as String).toList(),
      netAmount: json['netAmount'],
      winning: json['winning'],
      jsCount: json['js_count'] as String?,
      result: json['result'] as String?,
      kjData: json['kj_data'] as String?,
      number: json['number'] as String?,
      status: json['status'] as String?,
    );

Map<String, dynamic> _$ExpertPlanDataModelToJson(
        ExpertPlanDataModel instance) =>
    <String, dynamic>{
      'eid': instance.eid,
      'name': instance.name,
      'position': instance.position,
      'yc_length': instance.ycLength,
      'yc_data': instance.ycData,
      'playIds': instance.playIds,
      'netAmount': instance.netAmount,
      'winning': instance.winning,
      'js_count': instance.jsCount,
      'result': instance.result,
      'kj_data': instance.kjData,
      'number': instance.number,
      'status': instance.status,
    };
