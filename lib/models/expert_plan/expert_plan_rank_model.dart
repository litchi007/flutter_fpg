import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_rank_model.g.dart';

// {
// "name": "专家15号",
// "winning": 100,
// "status": "1",
// "eid": "15"
// }

@JsonSerializable()
class ExpertPlanRankModel {
  final String? name;
  final String? eid;
  final String? status;
  final dynamic winning;

  const ExpertPlanRankModel({this.name, this.eid, this.status, this.winning});

  factory ExpertPlanRankModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanRankModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanRankModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanRankModel copyWith({
    String? name,
    String? eid,
    String? status,
    String? winning,
  }) {
    return ExpertPlanRankModel(
      name: name ?? this.name,
      winning: winning ?? this.winning,
      status: status ?? this.status,
      eid: eid ?? this.eid,
    );
  }
}
