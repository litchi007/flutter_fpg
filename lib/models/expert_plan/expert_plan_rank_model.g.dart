// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_rank_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanRankModel _$ExpertPlanRankModelFromJson(Map<String, dynamic> json) =>
    ExpertPlanRankModel(
      name: json['name'] as String?,
      eid: json['eid'] as String?,
      status: json['status'] as String?,
      winning: json['winning'],
    );

Map<String, dynamic> _$ExpertPlanRankModelToJson(
        ExpertPlanRankModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'eid': instance.eid,
      'status': instance.status,
      'winning': instance.winning,
    };
