import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_follow_task_model.g.dart';

// {
// "id": "56",
// "issue": "21144619",
// "betType": "2",
// "money": 1
// }

@JsonSerializable()
class ExpertPlanFollowTaskModel {
  final String? id;
  final String? issue;
  final String? betType;
  final dynamic money;

  const ExpertPlanFollowTaskModel({
    this.id,
    this.issue,
    this.betType,
    this.money,
  });

  factory ExpertPlanFollowTaskModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanFollowTaskModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanFollowTaskModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanFollowTaskModel copyWith({
    String? id,
    String? issue,
    String? betType,
    String? money,
  }) {
    return ExpertPlanFollowTaskModel(
        id: id ?? this.id,
        issue: issue ?? this.issue,
        betType: betType ?? this.betType,
        money: money ?? this.money);
  }
}
