import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_data_model.g.dart';

/// 专家追号数据结构
// {
// "eid": "2",
// "name": "专家2号",
// "position": "3",
// "yc_data": [
// "4尾",
// "8尾",
// "0尾"
// ],
// "yc_length": "3",
// "playIds": [
// "7126576",
// "7126577",
// "7126578",
// "7126579",
// "7126580",
// "7126581",
// "7126582",
// "7126583",
// "7126584",
// "7126585"
// ],
// "netAmount": "18100.00",
// "winning": 100,
// "js_count": "15"
// }

/// 专家历史开奖数据结构
// {
// "eid": "8",
// "result": "1",
// "name": "专家8号",
// "position": "1",
// "yc_data": [
// "鼠",
// "牛",
// "龙"
// ],
// "netAmount": "0.00",
// "winning": 0,
// "kj_data": "牛",
// "number": "2408270279",
// "yc_length": "3",
// "status": "1",
// "playIds": [
// "7126101",
// "7126102",
// "7126103",
// "7126104",
// "7126105",
// "7126106",
// "7126107",
// "7126108",
// "7126109",
// "7126110",
// "7126111",
// "7126112"
// ],
// "js_count": "2"
// },

@JsonSerializable()
class ExpertPlanDataModel {
  final String? eid;
  final String? name;
  final String? position;
  @JsonKey(name: 'yc_length')
  final String? ycLength;
  @JsonKey(name: 'yc_data')
  final List<String>? ycData;
  final List<String>? playIds;
  final dynamic netAmount;
  final dynamic winning;
  @JsonKey(name: 'js_count')
  final String? jsCount;
  final String? result;
  @JsonKey(name: 'kj_data')
  final String? kjData;
  final String? number;
  final String? status;

  const ExpertPlanDataModel(
      {this.eid,
      this.name,
      this.position,
      this.ycLength,
      this.ycData,
      this.playIds,
      this.netAmount,
      this.winning,
      this.jsCount,
      this.result,
      this.kjData,
      this.number,
      this.status});

  factory ExpertPlanDataModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanDataModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanDataModel copyWith(
      {String? eid,
      String? name,
      String? position,
      String? ycLength,
      List<String>? ycData,
      List<String>? playIds,
      String? netAmount,
      String? winning,
      String? jsCount,
      String? result,
      String? kjData,
      String? number,
      String? status}) {
    return ExpertPlanDataModel(
        eid: eid ?? this.eid,
        name: name ?? this.name,
        position: position ?? this.position,
        ycLength: ycLength ?? this.ycLength,
        ycData: ycData ?? this.ycData,
        playIds: playIds ?? this.playIds,
        netAmount: position ?? this.netAmount,
        winning: winning ?? this.winning,
        jsCount: jsCount ?? this.jsCount,
        result: result ?? this.result,
        kjData: kjData ?? this.kjData,
        number: number ?? this.number,
        status: status ?? this.status);
  }
}
