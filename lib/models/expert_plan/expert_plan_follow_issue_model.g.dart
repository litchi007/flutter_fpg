// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expert_plan_follow_issue_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpertPlanFollowIssueModel _$ExpertPlanFollowIssueModelFromJson(
        Map<String, dynamic> json) =>
    ExpertPlanFollowIssueModel(
      issue: json['issue'] as String?,
      displayNumber: json['issue_displayNumber'] as String?,
      startTime: json['startTime'],
      startDate: json['startDate'] as String?,
      endTime: json['endTime'],
      endDate: json['endDate'] as String?,
      actionTime: json['actionTime'] as String?,
      lotteryTime: json['lotteryTime'],
      lotteryDate: json['lotteryDate'] as String?,
    )
      ..betType = json['betType'] as String?
      ..betMoney = json['betMoney'] as String?;

Map<String, dynamic> _$ExpertPlanFollowIssueModelToJson(
        ExpertPlanFollowIssueModel instance) =>
    <String, dynamic>{
      'issue': instance.issue,
      'issue_displayNumber': instance.displayNumber,
      'startTime': instance.startTime,
      'startDate': instance.startDate,
      'endTime': instance.endTime,
      'endDate': instance.endDate,
      'actionTime': instance.actionTime,
      'lotteryTime': instance.lotteryTime,
      'lotteryDate': instance.lotteryDate,
      'betType': instance.betType,
      'betMoney': instance.betMoney,
    };
