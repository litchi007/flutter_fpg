import 'dart:math';

import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'expert_plan_switch_model.g.dart';

// {
// "autoFollowStatus": 1,
// "enable": 0,
// "codeArea": [
// "3",
// "4",
// "5",
// "6",
// "7"
// ],
// "gameArr": [
// {
// "gameName": "澳洲幸运5",
// "gameId": "26"
// }]
// }

@JsonSerializable()
class ExpertPlanSwitchModel {
  final int? autoFollowStatus;
  final int? enable;
  final List<String>? codeArea;
  final List<ExpertPlanGameModel>? gameArr;

  const ExpertPlanSwitchModel({
    this.autoFollowStatus,
    this.enable,
    this.codeArea,
    this.gameArr,
  });

  factory ExpertPlanSwitchModel.fromJson(Map<String, dynamic> json) =>
      _$ExpertPlanSwitchModelFromJson(json);

  Map<String, dynamic> toJson() => _$ExpertPlanSwitchModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  ExpertPlanSwitchModel copyWith({
    int? autoFollowStatus,
    int? enable,
    List<String>? codeArea,
    List<ExpertPlanGameModel>? gameArr,
  }) {
    return ExpertPlanSwitchModel(
      autoFollowStatus: autoFollowStatus ?? this.autoFollowStatus,
      enable: enable ?? this.enable,
      codeArea: codeArea ?? this.codeArea,
      gameArr: gameArr ?? this.gameArr,
    );
  }
}
