// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_red_bag_setting.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatRedBagSetting _$ChatRedBagSettingFromJson(Map<String, dynamic> json) =>
    ChatRedBagSetting(
      isRedBag: (json['isRedBag'] as num?)?.toInt(),
      isRedBagPwd: (json['isRedBagPwd'] as num?)?.toInt(),
      maxAmount: json['maxAmount'],
      minAmount: json['minAmount'],
      maxQuantity: json['maxQuantity'],
    );

Map<String, dynamic> _$ChatRedBagSettingToJson(ChatRedBagSetting instance) =>
    <String, dynamic>{
      'isRedBag': instance.isRedBag,
      'isRedBagPwd': instance.isRedBagPwd,
      'maxAmount': instance.maxAmount,
      'minAmount': instance.minAmount,
      'maxQuantity': instance.maxQuantity,
    };
