// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rank_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RankData _$RankDataFromJson(Map<String, dynamic> json) => RankData(
      chatRank: (json['chatRank'] as num?)?.toInt(),
      chatRankTrue: (json['chatRankTrue'] as num?)?.toInt(),
      data: json['data'] as List<dynamic>?,
    );

Map<String, dynamic> _$RankDataToJson(RankData instance) => <String, dynamic>{
      'chatRank': instance.chatRank,
      'chatRankTrue': instance.chatRankTrue,
      'data': instance.data,
    };
