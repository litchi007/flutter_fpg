import 'package:json_annotation/json_annotation.dart';

part 'rank_data.g.dart';

@JsonSerializable()
class RankData {
  final int? chatRank;
  final int? chatRankTrue;
  final List<dynamic>? data;

  const RankData({this.chatRank, this.chatRankTrue, this.data});

  @override
  String toString() {
    return 'RankData(chatRank: $chatRank, chatRankTrue: $chatRankTrue, data: $data)';
  }

  factory RankData.fromJson(Map<String, dynamic> json) {
    return _$RankDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RankDataToJson(this);

  RankData copyWith({
    int? chatRank,
    int? chatRankTrue,
    List<dynamic>? data,
  }) {
    return RankData(
      chatRank: chatRank ?? this.chatRank,
      chatRankTrue: chatRankTrue ?? this.chatRankTrue,
      data: data ?? this.data,
    );
  }
}
