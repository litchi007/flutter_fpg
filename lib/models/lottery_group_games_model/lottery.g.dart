// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Lottery _$LotteryFromJson(Map<String, dynamic> json) => Lottery(
      id: json['id'] as String?,
      isSeal: json['isSeal'] as String?,
      isClose: json['isClose'] as String?,
      name: json['name'] as String?,
      title: json['title'] as String?,
      customise: json['customise'] as String?,
      isInstant: json['isInstant'] as String?,
      lowFreq: json['lowFreq'] as String?,
      enable: json['enable'] as String?,
      shortName: json['shortName'] as String?,
      onGetNoed: json['onGetNoed'] as String?,
      dataFtime: json['data_ftime'] as String?,
      gameType: json['gameType'] as String?,
      logo: json['logo'] as String?,
      serverTime: json['serverTime'] as String?,
      serverTimestamp: (json['serverTimestamp'] as num?)?.toInt(),
    );

Map<String, dynamic> _$LotteryToJson(Lottery instance) => <String, dynamic>{
      'id': instance.id,
      'isSeal': instance.isSeal,
      'isClose': instance.isClose,
      'name': instance.name,
      'title': instance.title,
      'customise': instance.customise,
      'isInstant': instance.isInstant,
      'lowFreq': instance.lowFreq,
      'enable': instance.enable,
      'shortName': instance.shortName,
      'onGetNoed': instance.onGetNoed,
      'data_ftime': instance.dataFtime,
      'gameType': instance.gameType,
      'logo': instance.logo,
      'serverTime': instance.serverTime,
      'serverTimestamp': instance.serverTimestamp,
    };
