import 'package:json_annotation/json_annotation.dart';

import 'lottery.dart';

part 'lottery_group_games_model.g.dart';

@JsonSerializable()
class LotteryGroupGamesModel {
  final dynamic id;
  final String? name;
  final String? logo;
  final List<Lottery>? lotteries;

  const LotteryGroupGamesModel({
    this.id,
    this.name,
    this.logo,
    this.lotteries,
  });

  @override
  String toString() {
    return 'LotteryGroupGamesModel(id: $id, name: $name, logo: $logo, lotteries: $lotteries)';
  }

  factory LotteryGroupGamesModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryGroupGamesModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryGroupGamesModelToJson(this);

  LotteryGroupGamesModel copyWith({
    dynamic id,
    String? name,
    String? logo,
    List<Lottery>? lotteries,
  }) {
    return LotteryGroupGamesModel(
      id: id ?? this.id,
      name: name ?? this.name,
      logo: logo ?? this.logo,
      lotteries: lotteries ?? this.lotteries,
    );
  }
}
