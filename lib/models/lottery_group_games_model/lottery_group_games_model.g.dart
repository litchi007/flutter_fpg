// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_group_games_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryGroupGamesModel _$LotteryGroupGamesModelFromJson(
        Map<String, dynamic> json) =>
    LotteryGroupGamesModel(
      id: json['id'],
      name: json['name'] as String?,
      logo: json['logo'] as String?,
      lotteries: (json['lotteries'] as List<dynamic>?)
          ?.map((e) => Lottery.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LotteryGroupGamesModelToJson(
        LotteryGroupGamesModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'logo': instance.logo,
      'lotteries': instance.lotteries,
    };
