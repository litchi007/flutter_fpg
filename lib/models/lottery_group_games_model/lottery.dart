import 'package:json_annotation/json_annotation.dart';

part 'lottery.g.dart';

@JsonSerializable()
class Lottery {
  final String? id;
  final String? isSeal;
  final String? isClose;
  final String? name;
  final String? title;
  final String? customise;
  final String? isInstant;
  final String? lowFreq;
  final String? enable;
  final String? shortName;
  final String? onGetNoed;
  @JsonKey(name: 'data_ftime')
  final String? dataFtime;
  final String? gameType;
  final String? logo;
  final String? serverTime;
  final int? serverTimestamp;

  const Lottery({
    this.id,
    this.isSeal,
    this.isClose,
    this.name,
    this.title,
    this.customise,
    this.isInstant,
    this.lowFreq,
    this.enable,
    this.shortName,
    this.onGetNoed,
    this.dataFtime,
    this.gameType,
    this.logo,
    this.serverTime,
    this.serverTimestamp,
  });

  @override
  String toString() {
    return 'Lottery(id: $id, isSeal: $isSeal, isClose: $isClose, name: $name, title: $title, customise: $customise, isInstant: $isInstant, lowFreq: $lowFreq, enable: $enable, shortName: $shortName, onGetNoed: $onGetNoed, dataFtime: $dataFtime, gameType: $gameType, logo: $logo, serverTime: $serverTime, serverTimestamp: $serverTimestamp)';
  }

  factory Lottery.fromJson(Map<String, dynamic> json) {
    return _$LotteryFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryToJson(this);

  Lottery copyWith({
    String? id,
    String? isSeal,
    String? isClose,
    String? name,
    String? title,
    String? customise,
    String? isInstant,
    String? lowFreq,
    String? enable,
    String? shortName,
    String? onGetNoed,
    String? dataFtime,
    String? gameType,
    String? logo,
    String? serverTime,
    int? serverTimestamp,
  }) {
    return Lottery(
      id: id ?? this.id,
      isSeal: isSeal ?? this.isSeal,
      isClose: isClose ?? this.isClose,
      name: name ?? this.name,
      title: title ?? this.title,
      customise: customise ?? this.customise,
      isInstant: isInstant ?? this.isInstant,
      lowFreq: lowFreq ?? this.lowFreq,
      enable: enable ?? this.enable,
      shortName: shortName ?? this.shortName,
      onGetNoed: onGetNoed ?? this.onGetNoed,
      dataFtime: dataFtime ?? this.dataFtime,
      gameType: gameType ?? this.gameType,
      logo: logo ?? this.logo,
      serverTime: serverTime ?? this.serverTime,
      serverTimestamp: serverTimestamp ?? this.serverTimestamp,
    );
  }
}
