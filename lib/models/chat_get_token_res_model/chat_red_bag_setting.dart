import 'package:json_annotation/json_annotation.dart';

part 'chat_red_bag_setting.g.dart';

@JsonSerializable()
class ChatRedBagSetting {
  final dynamic isRedBag;
  final dynamic isRedBagPwd;
  final dynamic maxAmount;
  final dynamic minAmount;
  final dynamic maxQuantity;

  const ChatRedBagSetting({
    this.isRedBag,
    this.isRedBagPwd,
    this.maxAmount,
    this.minAmount,
    this.maxQuantity,
  });

  @override
  String toString() {
    return 'ChatRedBagSetting(isRedBag: $isRedBag, isRedBagPwd: $isRedBagPwd, maxAmount: $maxAmount, minAmount: $minAmount, maxQuantity: $maxQuantity)';
  }

  factory ChatRedBagSetting.fromJson(Map<String, dynamic> json) {
    return _$ChatRedBagSettingFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatRedBagSettingToJson(this);

  ChatRedBagSetting copyWith({
    dynamic isRedBag,
    dynamic isRedBagPwd,
    dynamic maxAmount,
    dynamic minAmount,
    dynamic maxQuantity,
  }) {
    return ChatRedBagSetting(
      isRedBag: isRedBag ?? this.isRedBag,
      isRedBagPwd: isRedBagPwd ?? this.isRedBagPwd,
      maxAmount: maxAmount ?? this.maxAmount,
      minAmount: minAmount ?? this.minAmount,
      maxQuantity: maxQuantity ?? this.maxQuantity,
    );
  }
}
