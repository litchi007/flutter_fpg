import 'package:json_annotation/json_annotation.dart';

import 'chat_ary.dart';
import 'rank_data.dart';

part 'chat_get_token_res_model.g.dart';

@JsonSerializable()
class ChatGetTokenResModel {
  final String? nickname;
  final String? uid;
  final String? testFlag;
  final List<ChatAry>? chatAry;
  final List<dynamic>? announce;
  final String? ip;
  final String? token;
  final String? tokenImg;
  final String? tokenTxt;
  final int? level;
  final String? levelName;
  final bool? isManager;
  final int? isManagerIconTag;
  final bool? isTrial;
  final bool? isAllBan;
  final String? isPicBan;
  final String? isShareBill;
  final String? isChatToManager;
  final String? roomName;
  final String? placeholderFlag;
  final String? placeholder;
  final String? sendRedBagUserType;
  final String? sendRedBagMineUserType;
  final String? showRedBagDetailConfig;
  final String? titleStatus;
  final String? redBagMineRule;
  final String? redBagTotalAmountAllowFloat;
  final String? redBagMineTotalAmountAllowFloat;
  final String? chatOnlineMemberStatus;
  final int? chatOnlineMemberCount;
  final int? cronDataTopStatus;
  final String? cronDataTopType;
  final int? chatRoomRedirect;
  final int? checkCoinPwdStatus;
  final int? checkMineCoinPwdStatus;
  final int? isCallStatus;
  final int? isAddFriend;
  final int? isPickRecommended;
  final int? isShowChat;
  final int? chatBetAreaChange;
  final List<List<String>>? newchatAry;
  final int? chatShareBetMinAmount;
  final List<String>? callOperateAry;
  final RankData? rankData;

  const ChatGetTokenResModel({
    this.nickname,
    this.uid,
    this.testFlag,
    this.chatAry,
    this.announce,
    this.ip,
    this.token,
    this.tokenImg,
    this.tokenTxt,
    this.level,
    this.levelName,
    this.isManager,
    this.isManagerIconTag,
    this.isTrial,
    this.isAllBan,
    this.isPicBan,
    this.isShareBill,
    this.isChatToManager,
    this.roomName,
    this.placeholderFlag,
    this.placeholder,
    this.sendRedBagUserType,
    this.sendRedBagMineUserType,
    this.showRedBagDetailConfig,
    this.titleStatus,
    this.redBagMineRule,
    this.redBagTotalAmountAllowFloat,
    this.redBagMineTotalAmountAllowFloat,
    this.chatOnlineMemberStatus,
    this.chatOnlineMemberCount,
    this.cronDataTopStatus,
    this.cronDataTopType,
    this.chatRoomRedirect,
    this.checkCoinPwdStatus,
    this.checkMineCoinPwdStatus,
    this.isCallStatus,
    this.isAddFriend,
    this.isPickRecommended,
    this.isShowChat,
    this.chatBetAreaChange,
    this.newchatAry,
    this.chatShareBetMinAmount,
    this.callOperateAry,
    this.rankData,
  });

  @override
  String toString() {
    return 'ChatGetTokenResModel(nickname: $nickname, uid: $uid, testFlag: $testFlag, chatAry: $chatAry, announce: $announce, ip: $ip, token: $token, tokenImg: $tokenImg, tokenTxt: $tokenTxt, level: $level, levelName: $levelName, isManager: $isManager, isManagerIconTag: $isManagerIconTag, isTrial: $isTrial, isAllBan: $isAllBan, isPicBan: $isPicBan, isShareBill: $isShareBill, isChatToManager: $isChatToManager, roomName: $roomName, placeholderFlag: $placeholderFlag, placeholder: $placeholder, sendRedBagUserType: $sendRedBagUserType, sendRedBagMineUserType: $sendRedBagMineUserType, showRedBagDetailConfig: $showRedBagDetailConfig, titleStatus: $titleStatus, redBagMineRule: $redBagMineRule, redBagTotalAmountAllowFloat: $redBagTotalAmountAllowFloat, redBagMineTotalAmountAllowFloat: $redBagMineTotalAmountAllowFloat, chatOnlineMemberStatus: $chatOnlineMemberStatus, chatOnlineMemberCount: $chatOnlineMemberCount, cronDataTopStatus: $cronDataTopStatus, cronDataTopType: $cronDataTopType, chatRoomRedirect: $chatRoomRedirect, checkCoinPwdStatus: $checkCoinPwdStatus, checkMineCoinPwdStatus: $checkMineCoinPwdStatus, isCallStatus: $isCallStatus, isAddFriend: $isAddFriend, isPickRecommended: $isPickRecommended, isShowChat: $isShowChat, chatBetAreaChange: $chatBetAreaChange, newchatAry: $newchatAry, chatShareBetMinAmount: $chatShareBetMinAmount, callOperateAry: $callOperateAry, rankData: $rankData)';
  }

  factory ChatGetTokenResModel.fromJson(Map<String, dynamic> json) {
    return _$ChatGetTokenResModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatGetTokenResModelToJson(this);

  ChatGetTokenResModel copyWith({
    String? nickname,
    String? uid,
    String? testFlag,
    List<ChatAry>? chatAry,
    List<dynamic>? announce,
    String? ip,
    String? token,
    String? tokenImg,
    String? tokenTxt,
    int? level,
    String? levelName,
    dynamic isManager,
    int? isManagerIconTag,
    bool? isTrial,
    bool? isAllBan,
    String? isPicBan,
    String? isShareBill,
    String? isChatToManager,
    String? roomName,
    String? placeholderFlag,
    String? placeholder,
    String? sendRedBagUserType,
    String? sendRedBagMineUserType,
    String? showRedBagDetailConfig,
    String? titleStatus,
    String? redBagMineRule,
    String? redBagTotalAmountAllowFloat,
    String? redBagMineTotalAmountAllowFloat,
    String? chatOnlineMemberStatus,
    int? chatOnlineMemberCount,
    int? cronDataTopStatus,
    String? cronDataTopType,
    int? chatRoomRedirect,
    int? checkCoinPwdStatus,
    int? checkMineCoinPwdStatus,
    int? isCallStatus,
    int? isAddFriend,
    int? isPickRecommended,
    int? isShowChat,
    int? chatBetAreaChange,
    List<List<String>>? newchatAry,
    int? chatShareBetMinAmount,
    List<String>? callOperateAry,
    RankData? rankData,
  }) {
    return ChatGetTokenResModel(
      nickname: nickname ?? this.nickname,
      uid: uid ?? this.uid,
      testFlag: testFlag ?? this.testFlag,
      chatAry: chatAry ?? this.chatAry,
      announce: announce ?? this.announce,
      ip: ip ?? this.ip,
      token: token ?? this.token,
      tokenImg: tokenImg ?? this.tokenImg,
      tokenTxt: tokenTxt ?? this.tokenTxt,
      level: level ?? this.level,
      levelName: levelName ?? this.levelName,
      isManager: isManager ?? this.isManager,
      isManagerIconTag: isManagerIconTag ?? this.isManagerIconTag,
      isTrial: isTrial ?? this.isTrial,
      isAllBan: isAllBan ?? this.isAllBan,
      isPicBan: isPicBan ?? this.isPicBan,
      isShareBill: isShareBill ?? this.isShareBill,
      isChatToManager: isChatToManager ?? this.isChatToManager,
      roomName: roomName ?? this.roomName,
      placeholderFlag: placeholderFlag ?? this.placeholderFlag,
      placeholder: placeholder ?? this.placeholder,
      sendRedBagUserType: sendRedBagUserType ?? this.sendRedBagUserType,
      sendRedBagMineUserType:
          sendRedBagMineUserType ?? this.sendRedBagMineUserType,
      showRedBagDetailConfig:
          showRedBagDetailConfig ?? this.showRedBagDetailConfig,
      titleStatus: titleStatus ?? this.titleStatus,
      redBagMineRule: redBagMineRule ?? this.redBagMineRule,
      redBagTotalAmountAllowFloat:
          redBagTotalAmountAllowFloat ?? this.redBagTotalAmountAllowFloat,
      redBagMineTotalAmountAllowFloat: redBagMineTotalAmountAllowFloat ??
          this.redBagMineTotalAmountAllowFloat,
      chatOnlineMemberStatus:
          chatOnlineMemberStatus ?? this.chatOnlineMemberStatus,
      chatOnlineMemberCount:
          chatOnlineMemberCount ?? this.chatOnlineMemberCount,
      cronDataTopStatus: cronDataTopStatus ?? this.cronDataTopStatus,
      cronDataTopType: cronDataTopType ?? this.cronDataTopType,
      chatRoomRedirect: chatRoomRedirect ?? this.chatRoomRedirect,
      checkCoinPwdStatus: checkCoinPwdStatus ?? this.checkCoinPwdStatus,
      checkMineCoinPwdStatus:
          checkMineCoinPwdStatus ?? this.checkMineCoinPwdStatus,
      isCallStatus: isCallStatus ?? this.isCallStatus,
      isAddFriend: isAddFriend ?? this.isAddFriend,
      isPickRecommended: isPickRecommended ?? this.isPickRecommended,
      isShowChat: isShowChat ?? this.isShowChat,
      chatBetAreaChange: chatBetAreaChange ?? this.chatBetAreaChange,
      newchatAry: newchatAry ?? this.newchatAry,
      chatShareBetMinAmount:
          chatShareBetMinAmount ?? this.chatShareBetMinAmount,
      callOperateAry: callOperateAry ?? this.callOperateAry,
      rankData: rankData ?? this.rankData,
    );
  }
}
