// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_get_token_res_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatGetTokenResModel _$ChatGetTokenResModelFromJson(
        Map<String, dynamic> json) =>
    ChatGetTokenResModel(
      nickname: json['nickname'] as String?,
      uid: json['uid'] as String?,
      testFlag: json['testFlag'] as String?,
      chatAry: (json['chatAry'] as List<dynamic>?)
          ?.map((e) => ChatAry.fromJson(e as Map<String, dynamic>))
          .toList(),
      announce: json['announce'] as List<dynamic>?,
      ip: json['ip'] as String?,
      token: json['token'] as String?,
      tokenImg: json['tokenImg'] as String?,
      tokenTxt: json['tokenTxt'] as String?,
      level: (json['level'] as num?)?.toInt(),
      levelName: json['levelName'] as String?,
      isManager: json['isManager'] as bool?,
      isManagerIconTag: (json['isManagerIconTag'] as num?)?.toInt(),
      isTrial: json['isTrial'] as bool?,
      isAllBan: json['isAllBan'] as bool?,
      isPicBan: json['isPicBan'] as String?,
      isShareBill: json['isShareBill'] as String?,
      isChatToManager: json['isChatToManager'] as String?,
      roomName: json['roomName'] as String?,
      placeholderFlag: json['placeholderFlag'] as String?,
      placeholder: json['placeholder'] as String?,
      sendRedBagUserType: json['sendRedBagUserType'] as String?,
      sendRedBagMineUserType: json['sendRedBagMineUserType'] as String?,
      showRedBagDetailConfig: json['showRedBagDetailConfig'] as String?,
      titleStatus: json['titleStatus'] as String?,
      redBagMineRule: json['redBagMineRule'] as String?,
      redBagTotalAmountAllowFloat:
          json['redBagTotalAmountAllowFloat'] as String?,
      redBagMineTotalAmountAllowFloat:
          json['redBagMineTotalAmountAllowFloat'] as String?,
      chatOnlineMemberStatus: json['chatOnlineMemberStatus'] as String?,
      chatOnlineMemberCount: (json['chatOnlineMemberCount'] as num?)?.toInt(),
      cronDataTopStatus: (json['cronDataTopStatus'] as num?)?.toInt(),
      cronDataTopType: json['cronDataTopType'] as String?,
      chatRoomRedirect: (json['chatRoomRedirect'] as num?)?.toInt(),
      checkCoinPwdStatus: (json['checkCoinPwdStatus'] as num?)?.toInt(),
      checkMineCoinPwdStatus: (json['checkMineCoinPwdStatus'] as num?)?.toInt(),
      isCallStatus: (json['isCallStatus'] as num?)?.toInt(),
      isAddFriend: (json['isAddFriend'] as num?)?.toInt(),
      isPickRecommended: (json['isPickRecommended'] as num?)?.toInt(),
      isShowChat: (json['isShowChat'] as num?)?.toInt(),
      chatBetAreaChange: (json['chatBetAreaChange'] as num?)?.toInt(),
      newchatAry: (json['newchatAry'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>).map((e) => e as String).toList())
          .toList(),
      chatShareBetMinAmount: (json['chatShareBetMinAmount'] as num?)?.toInt(),
      callOperateAry: (json['callOperateAry'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      rankData: json['rankData'] == null
          ? null
          : RankData.fromJson(json['rankData'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatGetTokenResModelToJson(
        ChatGetTokenResModel instance) =>
    <String, dynamic>{
      'nickname': instance.nickname,
      'uid': instance.uid,
      'testFlag': instance.testFlag,
      'chatAry': instance.chatAry,
      'announce': instance.announce,
      'ip': instance.ip,
      'token': instance.token,
      'tokenImg': instance.tokenImg,
      'tokenTxt': instance.tokenTxt,
      'level': instance.level,
      'levelName': instance.levelName,
      'isManager': instance.isManager,
      'isManagerIconTag': instance.isManagerIconTag,
      'isTrial': instance.isTrial,
      'isAllBan': instance.isAllBan,
      'isPicBan': instance.isPicBan,
      'isShareBill': instance.isShareBill,
      'isChatToManager': instance.isChatToManager,
      'roomName': instance.roomName,
      'placeholderFlag': instance.placeholderFlag,
      'placeholder': instance.placeholder,
      'sendRedBagUserType': instance.sendRedBagUserType,
      'sendRedBagMineUserType': instance.sendRedBagMineUserType,
      'showRedBagDetailConfig': instance.showRedBagDetailConfig,
      'titleStatus': instance.titleStatus,
      'redBagMineRule': instance.redBagMineRule,
      'redBagTotalAmountAllowFloat': instance.redBagTotalAmountAllowFloat,
      'redBagMineTotalAmountAllowFloat':
          instance.redBagMineTotalAmountAllowFloat,
      'chatOnlineMemberStatus': instance.chatOnlineMemberStatus,
      'chatOnlineMemberCount': instance.chatOnlineMemberCount,
      'cronDataTopStatus': instance.cronDataTopStatus,
      'cronDataTopType': instance.cronDataTopType,
      'chatRoomRedirect': instance.chatRoomRedirect,
      'checkCoinPwdStatus': instance.checkCoinPwdStatus,
      'checkMineCoinPwdStatus': instance.checkMineCoinPwdStatus,
      'isCallStatus': instance.isCallStatus,
      'isAddFriend': instance.isAddFriend,
      'isPickRecommended': instance.isPickRecommended,
      'isShowChat': instance.isShowChat,
      'chatBetAreaChange': instance.chatBetAreaChange,
      'newchatAry': instance.newchatAry,
      'chatShareBetMinAmount': instance.chatShareBetMinAmount,
      'callOperateAry': instance.callOperateAry,
      'rankData': instance.rankData,
    };
