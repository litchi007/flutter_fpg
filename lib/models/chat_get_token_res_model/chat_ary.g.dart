// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_ary.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatAry _$ChatAryFromJson(Map<String, dynamic> json) => ChatAry(
      roomId: json['roomId'] as String?,
      roomName: json['roomName'] as String?,
      password: json['password'],
      isChatBan: json['isChatBan'] as bool?,
      isShareBet: json['isShareBet'] as bool?,
      isShareBill: json['isShareBill'] as bool?,
      isChatToManager: json['isChatToManager'] as bool?,
      typeId: json['typeId'] as String?,
      sortId: (json['sortId'] as num?)?.toInt(),
      cronDataTopStatus: (json['cronDataTopStatus'] as num?)?.toInt(),
      cronDataTopType: json['cronDataTopType'] as String?,
      typeIds:
          (json['typeIds'] as List<dynamic>?)?.map((e) => e as String).toList(),
      roomBanShareBetStatus: json['roomBanShareBetStatus'] as bool?,
      chatRedBagSetting: json['chatRedBagSetting'] == null
          ? null
          : ChatRedBagSetting.fromJson(
              json['chatRedBagSetting'] as Map<String, dynamic>),
      isMine: json['isMine'] as String?,
      minAmount: json['minAmount'] as String?,
      maxAmount: json['maxAmount'] as String?,
      oddsRate: json['oddsRate'] as String?,
      quantity: json['quantity'] as String?,
      isTalk: (json['isTalk'] as num?)?.toInt(),
      placeholder: json['placeholder'] as String?,
      shareBetStatus: (json['shareBetStatus'] as num?)?.toInt(),
      talkLevels: json['talkLevels'] as String?,
      talkGrade: json['talkGrade'] as String?,
    );

Map<String, dynamic> _$ChatAryToJson(ChatAry instance) => <String, dynamic>{
      'roomId': instance.roomId,
      'roomName': instance.roomName,
      'password': instance.password,
      'isChatBan': instance.isChatBan,
      'isShareBet': instance.isShareBet,
      'isShareBill': instance.isShareBill,
      'isChatToManager': instance.isChatToManager,
      'typeId': instance.typeId,
      'sortId': instance.sortId,
      'cronDataTopStatus': instance.cronDataTopStatus,
      'cronDataTopType': instance.cronDataTopType,
      'typeIds': instance.typeIds,
      'roomBanShareBetStatus': instance.roomBanShareBetStatus,
      'chatRedBagSetting': instance.chatRedBagSetting,
      'isMine': instance.isMine,
      'minAmount': instance.minAmount,
      'maxAmount': instance.maxAmount,
      'oddsRate': instance.oddsRate,
      'quantity': instance.quantity,
      'isTalk': instance.isTalk,
      'placeholder': instance.placeholder,
      'shareBetStatus': instance.shareBetStatus,
      'talkLevels': instance.talkLevels,
      'talkGrade': instance.talkGrade,
    };
