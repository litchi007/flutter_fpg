import 'package:json_annotation/json_annotation.dart';

import 'chat_red_bag_setting.dart';

part 'chat_ary.g.dart';

@JsonSerializable()
class ChatAry {
  final String? roomId;
  final String? roomName;
  final dynamic password;
  final bool? isChatBan;
  final bool? isShareBet;
  final bool? isShareBill;
  final bool? isChatToManager;
  final String? typeId;
  final int? sortId;
  final int? cronDataTopStatus;
  final String? cronDataTopType;
  final List<String>? typeIds;
  final bool? roomBanShareBetStatus;
  final ChatRedBagSetting? chatRedBagSetting;
  final String? isMine;
  final String? minAmount;
  final String? maxAmount;
  final String? oddsRate;
  final String? quantity;
  final int? isTalk;
  final String? placeholder;
  final int? shareBetStatus;
  final String? talkLevels;
  final String? talkGrade;

  const ChatAry({
    this.roomId,
    this.roomName,
    this.password,
    this.isChatBan,
    this.isShareBet,
    this.isShareBill,
    this.isChatToManager,
    this.typeId,
    this.sortId,
    this.cronDataTopStatus,
    this.cronDataTopType,
    this.typeIds,
    this.roomBanShareBetStatus,
    this.chatRedBagSetting,
    this.isMine,
    this.minAmount,
    this.maxAmount,
    this.oddsRate,
    this.quantity,
    this.isTalk,
    this.placeholder,
    this.shareBetStatus,
    this.talkLevels,
    this.talkGrade,
  });

  @override
  String toString() {
    return 'ChatAry(roomId: $roomId, roomName: $roomName, password: $password, isChatBan: $isChatBan, isShareBet: $isShareBet, isShareBill: $isShareBill, isChatToManager: $isChatToManager, typeId: $typeId, sortId: $sortId, cronDataTopStatus: $cronDataTopStatus, cronDataTopType: $cronDataTopType, typeIds: $typeIds, roomBanShareBetStatus: $roomBanShareBetStatus, chatRedBagSetting: $chatRedBagSetting, isMine: $isMine, minAmount: $minAmount, maxAmount: $maxAmount, oddsRate: $oddsRate, quantity: $quantity, isTalk: $isTalk, placeholder: $placeholder, shareBetStatus: $shareBetStatus, talkLevels: $talkLevels, talkGrade: $talkGrade)';
  }

  factory ChatAry.fromJson(Map<String, dynamic> json) {
    return _$ChatAryFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatAryToJson(this);

  ChatAry copyWith({
    String? roomId,
    String? roomName,
    dynamic password,
    bool? isChatBan,
    bool? isShareBet,
    bool? isShareBill,
    bool? isChatToManager,
    String? typeId,
    int? sortId,
    int? cronDataTopStatus,
    String? cronDataTopType,
    List<String>? typeIds,
    bool? roomBanShareBetStatus,
    ChatRedBagSetting? chatRedBagSetting,
    String? isMine,
    String? minAmount,
    String? maxAmount,
    String? oddsRate,
    String? quantity,
    int? isTalk,
    String? placeholder,
    int? shareBetStatus,
    String? talkLevels,
    String? talkGrade,
  }) {
    return ChatAry(
      roomId: roomId ?? this.roomId,
      roomName: roomName ?? this.roomName,
      password: password ?? this.password,
      isChatBan: isChatBan ?? this.isChatBan,
      isShareBet: isShareBet ?? this.isShareBet,
      isShareBill: isShareBill ?? this.isShareBill,
      isChatToManager: isChatToManager ?? this.isChatToManager,
      typeId: typeId ?? this.typeId,
      sortId: sortId ?? this.sortId,
      cronDataTopStatus: cronDataTopStatus ?? this.cronDataTopStatus,
      cronDataTopType: cronDataTopType ?? this.cronDataTopType,
      typeIds: typeIds ?? this.typeIds,
      roomBanShareBetStatus:
          roomBanShareBetStatus ?? this.roomBanShareBetStatus,
      chatRedBagSetting: chatRedBagSetting ?? this.chatRedBagSetting,
      isMine: isMine ?? this.isMine,
      minAmount: minAmount ?? this.minAmount,
      maxAmount: maxAmount ?? this.maxAmount,
      oddsRate: oddsRate ?? this.oddsRate,
      quantity: quantity ?? this.quantity,
      isTalk: isTalk ?? this.isTalk,
      placeholder: placeholder ?? this.placeholder,
      shareBetStatus: shareBetStatus ?? this.shareBetStatus,
      talkLevels: talkLevels ?? this.talkLevels,
      talkGrade: talkGrade ?? this.talkGrade,
    );
  }
}
