// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'public_avatar_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PublicAvatarList _$PublicAvatarListFromJson(Map<String, dynamic> json) =>
    PublicAvatarList(
      id: json['id'] as String?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$PublicAvatarListToJson(PublicAvatarList instance) =>
    <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
    };
