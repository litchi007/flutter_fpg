import 'package:json_annotation/json_annotation.dart';

import 'public_avatar_list.dart';

part 'user_get_avatar_setting_model.g.dart';

@JsonSerializable()
class UserGetAvatarSettingModel {
  final int? isAcceptUpload;
  final int? isReview;
  final List<PublicAvatarList>? publicAvatarList;

  const UserGetAvatarSettingModel({
    this.isAcceptUpload,
    this.isReview,
    this.publicAvatarList,
  });

  @override
  String toString() {
    return 'UserGetAvatarSettingModel(isAcceptUpload: $isAcceptUpload, isReview: $isReview, publicAvatarList: $publicAvatarList)';
  }

  factory UserGetAvatarSettingModel.fromJson(Map<String, dynamic> json) {
    return _$UserGetAvatarSettingModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$UserGetAvatarSettingModelToJson(this);

  UserGetAvatarSettingModel copyWith({
    int? isAcceptUpload,
    int? isReview,
    List<PublicAvatarList>? publicAvatarList,
  }) {
    return UserGetAvatarSettingModel(
      isAcceptUpload: isAcceptUpload ?? this.isAcceptUpload,
      isReview: isReview ?? this.isReview,
      publicAvatarList: publicAvatarList ?? this.publicAvatarList,
    );
  }
}
