// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_get_avatar_setting_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserGetAvatarSettingModel _$UserGetAvatarSettingModelFromJson(
        Map<String, dynamic> json) =>
    UserGetAvatarSettingModel(
      isAcceptUpload: (json['isAcceptUpload'] as num?)?.toInt(),
      isReview: (json['isReview'] as num?)?.toInt(),
      publicAvatarList: (json['publicAvatarList'] as List<dynamic>?)
          ?.map((e) => PublicAvatarList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UserGetAvatarSettingModelToJson(
        UserGetAvatarSettingModel instance) =>
    <String, dynamic>{
      'isAcceptUpload': instance.isAcceptUpload,
      'isReview': instance.isReview,
      'publicAvatarList': instance.publicAvatarList,
    };
