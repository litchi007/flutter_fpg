import 'package:json_annotation/json_annotation.dart';

part 'public_avatar_list.g.dart';

@JsonSerializable()
class PublicAvatarList {
  final String? id;
  final String? url;

  const PublicAvatarList({this.id, this.url});

  @override
  String toString() => 'PublicAvatarList(id: $id, url: $url)';

  factory PublicAvatarList.fromJson(Map<String, dynamic> json) {
    return _$PublicAvatarListFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PublicAvatarListToJson(this);

  PublicAvatarList copyWith({
    String? id,
    String? url,
  }) {
    return PublicAvatarList(
      id: id ?? this.id,
      url: url ?? this.url,
    );
  }
}
