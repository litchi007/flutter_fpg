import 'package:json_annotation/json_annotation.dart';

part 'statement.g.dart';

@JsonSerializable()
class Statement {
  final String? followBetStatStatus;

  const Statement({this.followBetStatStatus});

  @override
  String toString() {
    return 'Statement(followBetStatStatus: $followBetStatStatus)';
  }

  factory Statement.fromJson(Map<String, dynamic> json) {
    return _$StatementFromJson(json);
  }

  Map<String, dynamic> toJson() => _$StatementToJson(this);

  Statement copyWith({
    String? followBetStatStatus,
  }) {
    return Statement(
      followBetStatStatus: followBetStatStatus ?? this.followBetStatStatus,
    );
  }
}
