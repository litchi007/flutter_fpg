// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_message_record_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatMessageRecordModel _$ChatMessageRecordModelFromJson(
        Map<String, dynamic> json) =>
    ChatMessageRecordModel(
      id: json['id'] as String?,
      uid: json['uid'] as String?,
      avatar: json['avatar'] as String?,
      username: json['username'] as String?,
      level: json['level'],
      isTrial: json['is_trial'],
      isManager: json['isManager'] as bool?,
      createTime: json['createTime'],
      updateTime: json['updateTime'] as String?,
      dataId: json['dataId'] as String?,
      messageCode: json['messageCode'] as String?,
      chatType: json['chat_type'],
      dataType: json['data_type'] as String?,
      fullname: json['fullname'] as String?,
      usernameBak: json['usernameBak'] as String?,
      isChatCron: json['isChatCron'] as bool?,
      shareBillFlag: json['shareBillFlag'] as bool?,
      betFollowFlag: json['betFollowFlag'],
      ip: json['ip'] as String?,
      msg: json['msg'] as String?,
      extfield: json['extfield'] as String?,
      callJson: json['callJson'],
      t: json['t'],
      time: json['time'] as String?,
      roomId: json['roomId'],
      isManagerIconTag: json['isManagerIconTag'] as num?,
      callList: json['callList'] as List<dynamic>?,
      avator: json['avator'] as String?,
      avatarStatus: json['avatar_status'] as num?,
      levelImg: json['levelImg'] as String?,
      levelIconLeft: json['levelIconLeft'] as String?,
      levelIconRight: json['levelIconRight'] as String?,
      betUrl: json['betUrl'] == null
          ? null
          : BetUrl.fromJson(json['betUrl'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatMessageRecordModelToJson(
        ChatMessageRecordModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'avatar': instance.avatar,
      'username': instance.username,
      'level': instance.level,
      'is_trial': instance.isTrial,
      'isManager': instance.isManager,
      'createTime': instance.createTime,
      'updateTime': instance.updateTime,
      'dataId': instance.dataId,
      'messageCode': instance.messageCode,
      'chat_type': instance.chatType,
      'data_type': instance.dataType,
      'fullname': instance.fullname,
      'usernameBak': instance.usernameBak,
      'isChatCron': instance.isChatCron,
      'shareBillFlag': instance.shareBillFlag,
      'betFollowFlag': instance.betFollowFlag,
      'ip': instance.ip,
      'msg': instance.msg,
      'extfield': instance.extfield,
      'callJson': instance.callJson,
      't': instance.t,
      'time': instance.time,
      'roomId': instance.roomId,
      'isManagerIconTag': instance.isManagerIconTag,
      'callList': instance.callList,
      'avator': instance.avator,
      'avatar_status': instance.avatarStatus,
      'levelImg': instance.levelImg,
      'levelIconLeft': instance.levelIconLeft,
      'levelIconRight': instance.levelIconRight,
      'betUrl': instance.betUrl,
    };
