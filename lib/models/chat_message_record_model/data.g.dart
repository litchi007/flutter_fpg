// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      totalResultMoney: json['totalResultMoney'] as String?,
      totalBetMoney: json['totalBetMoney'] as String?,
      totalBonus: json['totalBonus'] as String?,
      userCount: json['userCount'] as String?,
      totalAmount: json['totalAmount'] as String?,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'totalResultMoney': instance.totalResultMoney,
      'totalBetMoney': instance.totalBetMoney,
      'totalBonus': instance.totalBonus,
      'userCount': instance.userCount,
      'totalAmount': instance.totalAmount,
    };
