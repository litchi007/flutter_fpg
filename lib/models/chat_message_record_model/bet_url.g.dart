// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bet_url.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BetUrl _$BetUrlFromJson(Map<String, dynamic> json) => BetUrl(
      statement: json['statement'] == null
          ? null
          : Statement.fromJson(json['statement'] as Map<String, dynamic>),
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BetUrlToJson(BetUrl instance) => <String, dynamic>{
      'statement': instance.statement,
      'data': instance.data,
    };
