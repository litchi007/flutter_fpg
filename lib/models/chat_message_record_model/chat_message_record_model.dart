import 'package:json_annotation/json_annotation.dart';
import 'bet_url.dart';
part 'chat_message_record_model.g.dart';

@JsonSerializable()
class ChatMessageRecordModel {
  final String? id;
  final String? uid;
  final String? avatar;
  final String? username;
  final dynamic level;
  @JsonKey(name: 'is_trial')
  final dynamic isTrial;
  final bool? isManager;
  final dynamic createTime;
  final String? updateTime;
  final String? dataId;
  final String? messageCode;
  @JsonKey(name: 'chat_type')
  final dynamic chatType;
  @JsonKey(name: 'data_type')
  final String? dataType;
  final String? fullname;
  final String? usernameBak;
  final bool? isChatCron;
  final bool? shareBillFlag;
  final dynamic betFollowFlag;
  final String? ip;
  final String? msg;
  final String? extfield;
  final dynamic callJson;
  final dynamic t;
  final String? time;
  final dynamic roomId;
  final num? isManagerIconTag;
  final List<dynamic>? callList;
  final String? avator;
  @JsonKey(name: 'avatar_status')
  final num? avatarStatus;
  final String? levelImg;
  final String? levelIconLeft;
  final String? levelIconRight;
  final BetUrl? betUrl;
  const ChatMessageRecordModel(
      {this.id,
      this.uid,
      this.avatar,
      this.username,
      this.level,
      this.isTrial,
      this.isManager,
      this.createTime,
      this.updateTime,
      this.dataId,
      this.messageCode,
      this.chatType,
      this.dataType,
      this.fullname,
      this.usernameBak,
      this.isChatCron,
      this.shareBillFlag,
      this.betFollowFlag,
      this.ip,
      this.msg,
      this.extfield,
      this.callJson,
      this.t,
      this.time,
      this.roomId,
      this.isManagerIconTag,
      this.callList,
      this.avator,
      this.avatarStatus,
      this.levelImg,
      this.levelIconLeft,
      this.levelIconRight,
      this.betUrl});

  @override
  String toString() {
    return 'ChatMessageRecordModel(id: $id, uid: $uid, avatar: $avatar, username: $username, level: $level, isTrial: $isTrial, isManager: $isManager, createTime: $createTime, updateTime: $updateTime, dataId: $dataId, messageCode: $messageCode, chatType: $chatType, dataType: $dataType, fullname: $fullname, usernameBak: $usernameBak, isChatCron: $isChatCron, shareBillFlag: $shareBillFlag, betFollowFlag: $betFollowFlag, ip: $ip, msg: $msg, extfield: $extfield, callJson: $callJson, t: $t, time: $time, roomId: $roomId, isManagerIconTag: $isManagerIconTag, callList: $callList, avator: $avator, avatarStatus: $avatarStatus, levelImg: $levelImg, levelIconLeft: $levelIconLeft, levelIconRight: $levelIconRight, betUrl: $betUrl)';
  }

  factory ChatMessageRecordModel.fromJson(Map<String, dynamic> json) {
    return _$ChatMessageRecordModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ChatMessageRecordModelToJson(this);

  ChatMessageRecordModel copyWith(
      {String? id,
      String? uid,
      String? avatar,
      String? username,
      dynamic level,
      dynamic isTrial,
      bool? isManager,
      dynamic createTime,
      String? updateTime,
      String? dataId,
      String? messageCode,
      dynamic chatType,
      String? dataType,
      String? fullname,
      String? usernameBak,
      bool? isChatCron,
      bool? shareBillFlag,
      dynamic betFollowFlag,
      String? ip,
      String? msg,
      String? extfield,
      dynamic callJson,
      dynamic t,
      String? time,
      dynamic roomId,
      int? isManagerIconTag,
      List<dynamic>? callList,
      String? avator,
      int? avatarStatus,
      String? levelImg,
      String? levelIconLeft,
      String? levelIconRight,
      BetUrl? betUrl}) {
    return ChatMessageRecordModel(
        id: id ?? this.id,
        uid: uid ?? this.uid,
        avatar: avatar ?? this.avatar,
        username: username ?? this.username,
        level: level ?? this.level,
        isTrial: isTrial ?? this.isTrial,
        isManager: isManager ?? this.isManager,
        createTime: createTime ?? this.createTime,
        updateTime: updateTime ?? this.updateTime,
        dataId: dataId ?? this.dataId,
        messageCode: messageCode ?? this.messageCode,
        chatType: chatType ?? this.chatType,
        dataType: dataType ?? this.dataType,
        fullname: fullname ?? this.fullname,
        usernameBak: usernameBak ?? this.usernameBak,
        isChatCron: isChatCron ?? this.isChatCron,
        shareBillFlag: shareBillFlag ?? this.shareBillFlag,
        betFollowFlag: betFollowFlag ?? this.betFollowFlag,
        ip: ip ?? this.ip,
        msg: msg ?? this.msg,
        extfield: extfield ?? this.extfield,
        callJson: callJson ?? this.callJson,
        t: t ?? this.t,
        time: time ?? this.time,
        roomId: roomId ?? this.roomId,
        isManagerIconTag: isManagerIconTag ?? this.isManagerIconTag,
        callList: callList ?? this.callList,
        avator: avator ?? this.avator,
        avatarStatus: avatarStatus ?? this.avatarStatus,
        levelImg: levelImg ?? this.levelImg,
        levelIconLeft: levelIconLeft ?? this.levelIconLeft,
        levelIconRight: levelIconRight ?? this.levelIconRight,
        betUrl: betUrl ?? this.betUrl);
  }
}
