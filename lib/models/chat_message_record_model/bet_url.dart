import 'package:json_annotation/json_annotation.dart';

import 'data.dart';
import 'statement.dart';

part 'bet_url.g.dart';

@JsonSerializable()
class BetUrl {
  final Statement? statement;
  final Data? data;

  const BetUrl({this.statement, this.data});

  @override
  String toString() => 'BetUrl(statement: $statement, data: $data)';

  factory BetUrl.fromJson(Map<String, dynamic> json) {
    return _$BetUrlFromJson(json);
  }

  Map<String, dynamic> toJson() => _$BetUrlToJson(this);

  BetUrl copyWith({
    Statement? statement,
    Data? data,
  }) {
    return BetUrl(
      statement: statement ?? this.statement,
      data: data ?? this.data,
    );
  }
}
