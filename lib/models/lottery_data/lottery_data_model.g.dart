// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lottery_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LotteryDataModel _$LotteryDataModelFromJson(Map<String, dynamic> json) =>
    LotteryDataModel(
      unbalancedMoney: json['unbalancedMoney'] as String?,
      totalTotalMoney: json['totalTotalMoney'] as String?,
    );

Map<String, dynamic> _$LotteryDataModelToJson(LotteryDataModel instance) =>
    <String, dynamic>{
      'unbalancedMoney': instance.unbalancedMoney,
      'totalTotalMoney': instance.totalTotalMoney,
    };
