import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'lottery_data_model.g.dart';

@JsonSerializable()
class LotteryDataModel {
  final String? unbalancedMoney;
  final String? totalTotalMoney;

  const LotteryDataModel({
    this.unbalancedMoney,
    this.totalTotalMoney,
  });

  @override
  String toString() {
    return jsonEncode(this);
  }

  factory LotteryDataModel.fromJson(Map<String, dynamic> json) {
    return _$LotteryDataModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LotteryDataModelToJson(this);

  LotteryDataModel copyWith({
    String? unbalancedMoney,
    String? totalTotalMoney,
  }) {
    return LotteryDataModel(
      unbalancedMoney: unbalancedMoney ?? this.unbalancedMoney,
      totalTotalMoney: totalTotalMoney ?? this.totalTotalMoney,
    );
  }
}
