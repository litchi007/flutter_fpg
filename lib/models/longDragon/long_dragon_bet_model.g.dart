// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'long_dragon_bet_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LongDragonBetModel _$LongDragonBetModelFromJson(Map<String, dynamic> json) =>
    LongDragonBetModel(
      odds: json['odds'] as String?,
      playId: json['playId'] as String?,
      playName: json['playName'] as String?,
      selected: json['selected'] as bool?,
    );

Map<String, dynamic> _$LongDragonBetModelToJson(LongDragonBetModel instance) =>
    <String, dynamic>{
      'odds': instance.odds,
      'playId': instance.playId,
      'playName': instance.playName,
      'selected': instance.selected,
    };
