import 'package:fpg_flutter/models/longDragon/long_dragon_bet_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'long_dragon_model.g.dart';

// {
// "title": "测试高频六合彩",
// "betList": [
// {
// "playName": "小",
// "playId": "7130522",
// "odds": "1.9800"
// },
// {
// "playName": "大",
// "playId": "7130521",
// "odds": "1.9800"
// }
// ],
// "playName": "小",
// "playCateName": "平四",
// "gameId": "269",
// "playCateId": "3537",
// "count": "11期",
// "sort": 11,
// "issue": "2407220504",
// "closeTime": "2024-07-22 08:22:55",
// "openTime": "2024-07-22 08:23:00",
// "displayNumber": "2407220504",
// "preIsOpen": true,
// "isSeal": "0",
// "serverTime": "2024-07-22 08:22:52",
// "lotteryCountdown": 8,
// "closeCountdown": 3,
// "logo": "https://wwwstatic01.fdgdggduydaa008aadsdf008.xyz/open_prize/images/icon/269.png?v=1721607755"
// }

@JsonSerializable()
class LongDragonModel {
  final String? title;
  final List<LongDragonBetModel>? betList;
  final String? playName;
  final String? playCateName;
  final String? gameId;
  final String? playCateId;
  final String? count;
  final dynamic sort;
  final dynamic issue;
  final String? closeTime;
  final String? openTime;
  final dynamic displayNumber;

  /// 开奖期数  自营优先使用
  final bool? preIsOpen;
  final String? isSeal;
  final String? serverTime;
  final dynamic lotteryCountdown;
  final dynamic closeCountdown;
  final String? logo;

  const LongDragonModel({
    this.title,
    this.betList,
    this.playName,
    this.playCateName,
    this.gameId,
    this.playCateId,
    this.count,
    this.sort,
    this.issue,
    this.closeTime,
    this.openTime,
    this.displayNumber,
    this.preIsOpen,
    this.isSeal,
    this.serverTime,
    this.lotteryCountdown,
    this.closeCountdown,
    this.logo,
  });

  /// 根据数据playNameColor返回颜色数据
  String getPlayNameColor() {
    if (playName == '大') {
      return '#DC3B40';
    }

    if (playName == '小') {
      return '#1ABC65';
    }

    if (playName == '单' || playName == '虎') {
      return '#6B73F5';
    }

    if (playName == '双' || playName == '龙') {
      return '#0000FF';
    }

    return '#6B73F5';

    if (playName == '小' || playName == '大') {
      return '#76B473';
    }

    if (playName == '单' || playName == '双') {
      return '#800080';
    }

    if (playName == '龙' || playName == '虎') {
      return '#DC143C';
    }

    return '#76B473';
  }

  /// 获取期数
  String getLotteryIssue() {
    if (displayNumber != null) {
      return displayNumber.toString();
    }
    if (issue != null) {
      return issue.toString();
    }
    return '0';
  }

  factory LongDragonModel.fromJson(Map<String, dynamic> json) =>
      _$LongDragonModelFromJson(json);

  Map<String, dynamic> toJson() => _$LongDragonModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  LongDragonModel copyWith({
    String? title,
    List<LongDragonBetModel>? betList,
    String? playName,
    String? playCateName,
    String? gameId,
    String? playCateId,
    String? count,
    String? sort,
    dynamic issue,
    String? closeTime,
    String? openTime,
    dynamic displayNumber,

    /// 开奖期数  自营优先使用
    bool? preIsOpen,
    String? isSeal,
    String? serverTime,
    String? lotteryCountdown,
    String? closeCountdown,
    String? logo,
  }) {
    return LongDragonModel(
      title: title ?? this.title,
      betList: betList ?? this.betList,
      playName: playName ?? this.playName,
      playCateName: playCateName ?? this.playCateName,
      gameId: gameId ?? this.gameId,
      playCateId: playCateId ?? this.playCateId,
      count: count ?? this.count,
      sort: sort ?? this.sort,
      issue: issue ?? this.issue,
      closeTime: closeTime ?? this.closeTime,
      openTime: openTime ?? this.openTime,
      displayNumber: displayNumber ?? this.displayNumber,
      preIsOpen: preIsOpen ?? this.preIsOpen,
      isSeal: isSeal ?? this.isSeal,
      serverTime: serverTime ?? this.serverTime,
      lotteryCountdown: lotteryCountdown ?? this.lotteryCountdown,
      closeCountdown: closeCountdown ?? this.closeCountdown,
      logo: logo ?? this.logo,
    );
  }
}
