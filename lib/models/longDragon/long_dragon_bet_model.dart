import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'long_dragon_bet_model.g.dart';

// {
// "playName": "小",
// "playId": "7130522",
// "odds": "1.9800"
// }

@JsonSerializable()
class LongDragonBetModel {
  final String? odds;
  final String? playId;
  final String? playName;
  bool? selected;

  LongDragonBetModel({this.odds, this.playId, this.playName, this.selected});

  factory LongDragonBetModel.fromJson(Map<String, dynamic> json) {
    return _$LongDragonBetModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LongDragonBetModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }

  LongDragonBetModel copyWith({
    String? odds,
    String? playId,
    String? playName,
    bool? selected,
  }) {
    return LongDragonBetModel(
      odds: odds ?? this.odds,
      playId: playId ?? this.playId,
      playName: playName ?? this.playName,
      selected: selected ?? this.selected,
    );
  }
}
