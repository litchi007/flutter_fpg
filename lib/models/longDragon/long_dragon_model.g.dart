// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'long_dragon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LongDragonModel _$LongDragonModelFromJson(Map<String, dynamic> json) =>
    LongDragonModel(
      title: json['title'] as String?,
      betList: (json['betList'] as List<dynamic>?)
          ?.map((e) => LongDragonBetModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      playName: json['playName'] as String?,
      playCateName: json['playCateName'] as String?,
      gameId: json['gameId'] as String?,
      playCateId: json['playCateId'] as String?,
      count: json['count'] as String?,
      sort: json['sort'],
      issue: json['issue'],
      closeTime: json['closeTime'] as String?,
      openTime: json['openTime'] as String?,
      displayNumber: json['displayNumber'],
      preIsOpen: json['preIsOpen'] as bool?,
      isSeal: json['isSeal'] as String?,
      serverTime: json['serverTime'] as String?,
      lotteryCountdown: json['lotteryCountdown'],
      closeCountdown: json['closeCountdown'],
      logo: json['logo'] as String?,
    );

Map<String, dynamic> _$LongDragonModelToJson(LongDragonModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'betList': instance.betList,
      'playName': instance.playName,
      'playCateName': instance.playCateName,
      'gameId': instance.gameId,
      'playCateId': instance.playCateId,
      'count': instance.count,
      'sort': instance.sort,
      'issue': instance.issue,
      'closeTime': instance.closeTime,
      'openTime': instance.openTime,
      'displayNumber': instance.displayNumber,
      'preIsOpen': instance.preIsOpen,
      'isSeal': instance.isSeal,
      'serverTime': instance.serverTime,
      'lotteryCountdown': instance.lotteryCountdown,
      'closeCountdown': instance.closeCountdown,
      'logo': instance.logo,
    };
