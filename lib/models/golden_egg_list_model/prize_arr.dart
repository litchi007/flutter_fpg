import 'package:json_annotation/json_annotation.dart';

part 'prize_arr.g.dart';

@JsonSerializable()
class PrizeArr {
  final int? prizeId;
  final String? prizeIcon;
  final String? prizeIconName;
  final String? prizeName;
  final String? prizeType;
  final String? prizeAmount;
  final String? prizeAmount2;

  const PrizeArr({
    this.prizeId,
    this.prizeIcon,
    this.prizeIconName,
    this.prizeName,
    this.prizeType,
    this.prizeAmount,
    this.prizeAmount2,
  });

  @override
  String toString() {
    return 'PrizeArr(prizeId: $prizeId, prizeIcon: $prizeIcon, prizeIconName: $prizeIconName, prizeName: $prizeName, prizeType: $prizeType, prizeAmount: $prizeAmount, prizeAmount2: $prizeAmount2)';
  }

  factory PrizeArr.fromJson(Map<String, dynamic> json) {
    return _$PrizeArrFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PrizeArrToJson(this);

  PrizeArr copyWith({
    int? prizeId,
    String? prizeIcon,
    String? prizeIconName,
    String? prizeName,
    String? prizeType,
    String? prizeAmount,
    String? prizeAmount2,
  }) {
    return PrizeArr(
      prizeId: prizeId ?? this.prizeId,
      prizeIcon: prizeIcon ?? this.prizeIcon,
      prizeIconName: prizeIconName ?? this.prizeIconName,
      prizeName: prizeName ?? this.prizeName,
      prizeType: prizeType ?? this.prizeType,
      prizeAmount: prizeAmount ?? this.prizeAmount,
      prizeAmount2: prizeAmount2 ?? this.prizeAmount2,
    );
  }
}
