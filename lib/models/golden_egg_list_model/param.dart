import 'package:json_annotation/json_annotation.dart';

import 'arc_arr.dart';
import 'prize_arr.dart';

part 'param.g.dart';

@JsonSerializable()
class Param {
  final String? buy;
  @JsonKey(name: 'buy_amount')
  final int? buyAmount;
  @JsonKey(name: 'check_in_user_levels')
  final String? checkInUserLevels;
  @JsonKey(name: 'content_turntable')
  final List<String>? contentTurntable;
  final String? membergame;
  @JsonKey(name: 'golden_egg_times')
  final int? goldenEggTimes;
  @JsonKey(name: 'usr_golden_egg_times')
  final int? usrGoldenEggTimes;
  @JsonKey(name: 'scrath_buy_type')
  final String? scrathBuyType;
  @JsonKey(name: 'visitor_show')
  final String? visitorShow;
  final List<PrizeArr>? prizeArr;
  @JsonKey(name: 'activity_buy_type_model')
  final String? activityBuyTypeModel;
  final List<ArcArr>? arcArr;

  const Param({
    this.buy,
    this.buyAmount,
    this.checkInUserLevels,
    this.contentTurntable,
    this.membergame,
    this.goldenEggTimes,
    this.usrGoldenEggTimes,
    this.scrathBuyType,
    this.visitorShow,
    this.prizeArr,
    this.activityBuyTypeModel,
    this.arcArr,
  });

  @override
  String toString() {
    return 'Param(buy: $buy, buyAmount: $buyAmount, checkInUserLevels: $checkInUserLevels, contentTurntable: $contentTurntable, membergame: $membergame, goldenEggTimes: $goldenEggTimes, usrGoldenEggTimes: $usrGoldenEggTimes, scrathBuyType: $scrathBuyType, visitorShow: $visitorShow, prizeArr: $prizeArr, activityBuyTypeModel: $activityBuyTypeModel, arcArr: $arcArr)';
  }

  factory Param.fromJson(Map<String, dynamic> json) => _$ParamFromJson(json);

  Map<String, dynamic> toJson() => _$ParamToJson(this);

  Param copyWith({
    String? buy,
    int? buyAmount,
    String? checkInUserLevels,
    List<String>? contentTurntable,
    String? membergame,
    int? goldenEggTimes,
    int? usrGoldenEggTimes,
    String? scrathBuyType,
    String? visitorShow,
    List<PrizeArr>? prizeArr,
    String? activityBuyTypeModel,
    List<ArcArr>? arcArr,
  }) {
    return Param(
      buy: buy ?? this.buy,
      buyAmount: buyAmount ?? this.buyAmount,
      checkInUserLevels: checkInUserLevels ?? this.checkInUserLevels,
      contentTurntable: contentTurntable ?? this.contentTurntable,
      membergame: membergame ?? this.membergame,
      goldenEggTimes: goldenEggTimes ?? this.goldenEggTimes,
      usrGoldenEggTimes: usrGoldenEggTimes ?? this.usrGoldenEggTimes,
      scrathBuyType: scrathBuyType ?? this.scrathBuyType,
      visitorShow: visitorShow ?? this.visitorShow,
      prizeArr: prizeArr ?? this.prizeArr,
      activityBuyTypeModel: activityBuyTypeModel ?? this.activityBuyTypeModel,
      arcArr: arcArr ?? this.arcArr,
    );
  }
}
