// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'param.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Param _$ParamFromJson(Map<String, dynamic> json) => Param(
      buy: json['buy'] as String?,
      buyAmount: (json['buy_amount'] as num?)?.toInt(),
      checkInUserLevels: json['check_in_user_levels'] as String?,
      contentTurntable: (json['content_turntable'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      membergame: json['membergame'] as String?,
      goldenEggTimes: (json['golden_egg_times'] as num?)?.toInt(),
      usrGoldenEggTimes: (json['usr_golden_egg_times'] as num?)?.toInt(),
      scrathBuyType: json['scrath_buy_type'] as String?,
      visitorShow: json['visitor_show'] as String?,
      prizeArr: (json['prizeArr'] as List<dynamic>?)
          ?.map((e) => PrizeArr.fromJson(e as Map<String, dynamic>))
          .toList(),
      activityBuyTypeModel: json['activity_buy_type_model'] as String?,
      arcArr: (json['arcArr'] as List<dynamic>?)
          ?.map((e) => ArcArr.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ParamToJson(Param instance) => <String, dynamic>{
      'buy': instance.buy,
      'buy_amount': instance.buyAmount,
      'check_in_user_levels': instance.checkInUserLevels,
      'content_turntable': instance.contentTurntable,
      'membergame': instance.membergame,
      'golden_egg_times': instance.goldenEggTimes,
      'usr_golden_egg_times': instance.usrGoldenEggTimes,
      'scrath_buy_type': instance.scrathBuyType,
      'visitor_show': instance.visitorShow,
      'prizeArr': instance.prizeArr,
      'activity_buy_type_model': instance.activityBuyTypeModel,
      'arcArr': instance.arcArr,
    };
