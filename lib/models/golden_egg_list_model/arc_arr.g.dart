// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'arc_arr.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArcArr _$ArcArrFromJson(Map<String, dynamic> json) => ArcArr(
      arcPid: (json['arcPid'] as num?)?.toInt(),
      condition: json['condition'] as String?,
      arcTimes: json['arcTimes'] as String?,
      arcGetTimes: json['arcGetTimes'],
    );

Map<String, dynamic> _$ArcArrToJson(ArcArr instance) => <String, dynamic>{
      'arcPid': instance.arcPid,
      'condition': instance.condition,
      'arcTimes': instance.arcTimes,
      'arcGetTimes': instance.arcGetTimes,
    };
