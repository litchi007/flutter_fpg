import 'package:json_annotation/json_annotation.dart';

part 'arc_arr.g.dart';

@JsonSerializable()
class ArcArr {
  final int? arcPid;
  final String? condition;
  final String? arcTimes;
  final dynamic arcGetTimes;

  const ArcArr({
    this.arcPid,
    this.condition,
    this.arcTimes,
    this.arcGetTimes,
  });

  @override
  String toString() {
    return 'ArcArr(arcPid: $arcPid, condition: $condition, arcTimes: $arcTimes, arcGetTimes: $arcGetTimes)';
  }

  factory ArcArr.fromJson(Map<String, dynamic> json) {
    return _$ArcArrFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ArcArrToJson(this);

  ArcArr copyWith({
    int? arcPid,
    String? condition,
    String? arcTimes,
    dynamic arcGetTimes,
  }) {
    return ArcArr(
      arcPid: arcPid ?? this.arcPid,
      condition: condition ?? this.condition,
      arcTimes: arcTimes ?? this.arcTimes,
      arcGetTimes: arcGetTimes ?? this.arcGetTimes,
    );
  }
}
