import 'package:json_annotation/json_annotation.dart';

import 'param.dart';

part 'golden_egg_list_model.g.dart';

@JsonSerializable()
class GoldenEggListModel {
  final String? id;
  final Param? param;
  final String? type;
  final String? start;
  final String? end;

  const GoldenEggListModel({
    this.id,
    this.param,
    this.type,
    this.start,
    this.end,
  });

  @override
  String toString() {
    return 'GoldenEggListModel(id: $id, param: $param, type: $type, start: $start, end: $end)';
  }

  factory GoldenEggListModel.fromJson(Map<String, dynamic> json) {
    return _$GoldenEggListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GoldenEggListModelToJson(this);

  GoldenEggListModel copyWith({
    String? id,
    Param? param,
    String? type,
    String? start,
    String? end,
  }) {
    return GoldenEggListModel(
      id: id ?? this.id,
      param: param ?? this.param,
      type: type ?? this.type,
      start: start ?? this.start,
      end: end ?? this.end,
    );
  }
}
