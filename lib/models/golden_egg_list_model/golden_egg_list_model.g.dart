// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'golden_egg_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GoldenEggListModel _$GoldenEggListModelFromJson(Map<String, dynamic> json) =>
    GoldenEggListModel(
      id: json['id'] as String?,
      param: json['param'] == null
          ? null
          : Param.fromJson(json['param'] as Map<String, dynamic>),
      type: json['type'] as String?,
      start: json['start'] as String?,
      end: json['end'] as String?,
    );

Map<String, dynamic> _$GoldenEggListModelToJson(GoldenEggListModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'param': instance.param,
      'type': instance.type,
      'start': instance.start,
      'end': instance.end,
    };
