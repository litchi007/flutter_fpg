import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class BbsHttp {
  /// 获取文档列表数据
  /// *[params] id -> 传入首页游戏列表接口的type参数 title -> 搜索关键字 category -> 游戏ID（香港六合彩或澳门六合彩的ID）
  static bbsGameDocList(BbsGameDocListParams params) {
    return HttpUtil.fetchModel(FetchType.get,
        url: ApiName.bbsGameDocList, queryParameters: params.toJson());
  }
}

class BbsGameDocListParams {
  String? id;
  String? title;
  String? category;
  int? page;
  int? rows;

  BbsGameDocListParams(
      {this.id, this.title, this.category, this.page, this.rows});

  BbsGameDocListParams.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    category = json['category'];
    page = json['page'];
    rows = json['rows'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['title'] = title;
    data['category'] = category;
    data['page'] = page;
    data['rows'] = rows;
    return data;
  }
}
