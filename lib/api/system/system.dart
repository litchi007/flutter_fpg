import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/models/home_menu/home_menu_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/models/system_avatar_list_model/system_avatar_list_model.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class SystemHttp {
  /// 头像列表
  static Future<ResponseModel<SystemAvatarListModel>> systemAvatarList() {
    return HttpUtil.fetchModel<SystemAvatarListModel>(FetchType.get,
        url: ApiName.systemAvatarList);
  }

  /// 首页右侧导航栏
  static Future<ResponseModel<HomeMenuModel>> systemMobileRight() {
    return HttpUtil.fetchModel<HomeMenuModel>(FetchType.get,
        url: ApiName.systemMobileRight);
  }
}
