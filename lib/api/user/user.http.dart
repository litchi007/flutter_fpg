import 'dart:convert';
import 'dart:io';

import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_info_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_issue_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_record_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_rank_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_switch_model.dart';
import 'package:fpg_flutter/models/guest_login_model/guest_login_model.dart';
import 'package:fpg_flutter/models/lotteryBetDetail/lottery_bet_detail.model.dart';
import 'package:fpg_flutter/models/lotteryBetResult/lottery_bet_result_data.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/models/user_balance_model/user_balance_model.dart';
import 'package:fpg_flutter/models/user_get_avatar_setting_model/user_get_avatar_setting_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/http_util.dart';
import 'package:crypto/crypto.dart';

class UserHttp {
  /// 游客登录
  static Future<ResponseModel<GuestLoginModel>> guestLogin() {
    return HttpUtil.fetchModel<GuestLoginModel>(FetchType.post,
        url: ApiName.userGuestLogin,
        queryParameters: {
          'usr': '46da83e1773338540e1e1c973f6c8a68',
          'pwd': '46da83e1773338540e1e1c973f6c8a68',
        });
  }

  /// 用户登录
  static Future<ResponseModel<GuestLoginModel>> userLogin(
      String usr, String pwd,
      {String? fullName, String? imgCode}) {
    return HttpUtil.fetchModel<GuestLoginModel>(FetchType.post,
        url: ApiName.userLogin,
        body: {
          'usr': usr,
          'pwd': md5.convert(utf8.encode(pwd)).toString(),
          'fullName': fullName,
          'imgCode': imgCode,
          'accessToken': AppDefine.accessToken,
          'device': Platform.isAndroid ? '2' : '3',
        });
  }

  /// 获取用户余额
  static Future<ResponseModel<UserBalanceModel>> userBalance() {
    return HttpUtil.fetchModel<UserBalanceModel>(FetchType.get,
        url: ApiName.userBalance,
        queryParameters: {'token': GlobalService.to.userLogin.value.apiSid});
  }

  /// 用户取消注单
  ///*[orderId] 订单id
  static Future userCancelBet(int orderId) {
    return HttpUtil.fetchModel(FetchType.post,
        url: ApiName.userCancelBet,
        body: {
          'orderId': orderId,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 下注明细(彩票每日统计)
  ///*[date] 投注日期
  static Future<ResponseModel<LotteryBetDetailModel>> userLotteryDayStat(
      {String? date}) {
    Map<String, dynamic>? queryParameters = {};
    if (date != null) {
      queryParameters['date'] = date;
    }
    queryParameters['token'] = GlobalService.to.userLogin.value.apiSid;

    return HttpUtil.fetchModel<LotteryBetDetailModel>(FetchType.post,
        url: ApiName.userLotteryDayStat, body: queryParameters);
  }

  /// 长龙注单投注
  static Future<ResponseModel<LotteryBetResultData>> changLongUserBet(
      BetParams params) {
    Map<String, dynamic> betParams = params.toJson();
    if (GlobalService.isTest) {
      //是测试账号
      if (params.isInstant == false) {
        return userGuestBet(betParams);
      } else {
        return userInstantBet(betParams);
      }
    } else {
      //不是测试账号
      if (params.isInstant == false) {
        return userBet(betParams);
      } else {
        return userInstantBet(betParams);
      }
    }
  }

  // 普通游戏 注单投注
  // static userGameBetWithParams(params: IBetLotteryParams) {
  //   if (UGStore.globalProps.userInfo?.isTest) {
  //     //是测试账号
  //     return params?.isInstant === '1'
  //       ? api_user.instantBet(params)
  //       : api_user.guestBet(params);
  //   } else {
  //     //不是测试账号
  //     return params?.isInstant === '1'
  //       ? api_user.instantBet(params)
  //       : api_user.bet(params);
  //   }
  // }
  /// 普通下注
  /// *[params] 下注参数
  static Future<ResponseModel<LotteryBetResultData>> userGameBetWithParams(
      BetParams params) {
    final body = params.toJson();
    if (GlobalService.isTest) {
      return userGuestBet(body);
    } else {
      return userBet(body);
    }
  }

  /// 游客投注
  static Future<ResponseModel<LotteryBetResultData>> userGuestBet(
      Map<String, dynamic> params) {
    params['token'] = GlobalService.to.userLogin.value.apiSid;
    return HttpUtil.fetchModel<LotteryBetResultData>(FetchType.post,
        url: ApiName.userGuestBet, body: params);
  }

  /// 用户投注
  static Future<ResponseModel<LotteryBetResultData>> userBet(
      Map<String, dynamic> params) {
    params['token'] = GlobalService.to.userLogin.value.apiSid;
    return HttpUtil.fetchModel<LotteryBetResultData>(FetchType.post,
        url: ApiName.userBet, body: params);
  }

  /// 即时投注
  static Future<ResponseModel<LotteryBetResultData>> userInstantBet(
      Map<String, dynamic> params) {
    params['token'] = GlobalService.to.userLogin.value.apiSid;
    return HttpUtil.fetchModel<LotteryBetResultData>(FetchType.post,
        url: ApiName.userInstantBet, body: params);
  }

  /// 专家追号开关
  ///*[gameId] 游戏ID
  static Future<ResponseModel<ExpertPlanSwitchModel>> userExpertSwitch(
      String gameId) {
    return HttpUtil.fetchModel<ExpertPlanSwitchModel>(FetchType.get,
        url: ApiName.userExpertSwitch,
        queryParameters: {
          'gameId': gameId,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 专家追号数据
  ///*[gameId] 游戏ID
  ///*[actionNo] 当前期号
  ///*[preIssue] 上期期号
  ///*[ycLength] 预测长度
  ///*[position] 预测位置
  static Future<ResponseModel<ExpertPlanDataModel>> userExpertDataList(
      String? gameId, String? actionNo, String? preIssue,
      {String? ycLength = '3', String? position = '1'}) {
    return HttpUtil.fetchModel<ExpertPlanDataModel>(FetchType.get,
        url: ApiName.userExpertDataList,
        queryParameters: {
          'gameId': gameId,
          'actionNo': actionNo,
          'preIssue': preIssue,
          'yc_length': ycLength,
          'position': position,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 专家排行数据
  ///*[gameId] 游戏ID
  ///*[preIssue] 上期期号
  ///*[ycLength] 预测长度
  ///*[position] 预测位置
  static Future<ResponseModel<ExpertPlanRankModel>> userExpertRank(
      String? gameId, String? preIssue,
      {String? ycLength = '3', String? position = '1'}) {
    return HttpUtil.fetchModel<ExpertPlanRankModel>(FetchType.get,
        url: ApiName.userExpertRank,
        queryParameters: {
          'gameId': gameId,
          'preIssue': preIssue,
          'yc_length': ycLength,
          'position': position,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 专家历史数据
  ///*[gameId] 游戏ID
  ///*[eid] 专家ID
  ///*[actionNo] 当前期号
  ///*[ycLength] 预测长度
  ///*[position] 预测位置
  static Future<ResponseModel<ExpertPlanDataModel>> userExpertHistoryList(
      String gameId, String actionNo, String eid,
      {String? ycLength = '3', String? position = '1'}) {
    return HttpUtil.fetchModel<ExpertPlanDataModel>(FetchType.get,
        url: ApiName.userExpertHistoryList,
        queryParameters: {
          'gameId': gameId,
          'actionNo': actionNo,
          'eid': eid,
          'yc_length': ycLength,
          'position': position,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 获取对应的期数
  ///*[gameId] 游戏ID
  ///*[maxCount] 获取条数
  static Future<ResponseModel<ExpertPlanFollowIssueModel>>
      getAutoFollowIssueList(String? gameId, String maxCount) {
    return HttpUtil.fetchModel<ExpertPlanFollowIssueModel>(FetchType.get,
        url: ApiName.autofollowIssueList,
        queryParameters: {
          'gameId': gameId,
          'maxCount': maxCount,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 获取期号的信息
  ///*[gameId] 游戏ID
  ///*[eid] 专家ID
  ///*[issueDot] 期号
  ///*[ycLength] 预测长度
  ///*[position] 预测位置
  static Future<ResponseModel> getAutoFollowIssueFollowList(
      String gameId, String issueDot, String eid, int ycLength, int position) {
    return HttpUtil.fetchModel(FetchType.get,
        url: ApiName.autofollowIssueFollowList,
        queryParameters: {
          'gameId': gameId,
          'eid': eid,
          'issueDot': issueDot,
          'yc_length': ycLength,
          'position': position,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 获取跟单记录
  ///*[gameId] 游戏ID
  static Future<ResponseModel<ExpertPlanFollowRecordModel>>
      getAutoFollowFollowList(String gameId) {
    return HttpUtil.fetchModel<ExpertPlanFollowRecordModel>(FetchType.get,
        url: ApiName.autofollowFollowList,
        queryParameters: {
          'gameId': gameId,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 获取跟单详情
  ///*[gameId] 游戏ID
  ///*[eid] 专家ID
  ///*[ycLength] 预测长度
  ///*[position] 预测位置
  static Future<ResponseModel<ExpertPlanFollowInfoModel>>
      getAutoFollowFollowInfo(
          String? gameId, String? eid, String? ycLength, String? position) {
    return HttpUtil.fetchModel<ExpertPlanFollowInfoModel>(FetchType.get,
        url: ApiName.autofollowFollowInfo,
        queryParameters: {
          'gameId': gameId,
          'eid': eid,
          'yc_length': ycLength,
          'position': position,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 保存跟单
  ///*[gameId] 游戏ID
  ///*[eid] 专家ID
  ///*[ycLength] 预测长度
  ///*[position] 预测位置
  ///*[content] 跟单内容
  static Future<ResponseModel> autoFollowSaveFollow(String? gameId, String? eid,
      String? ycLength, String? position, String? content) {
    return HttpUtil.fetchModel(FetchType.post,
        url: ApiName.autofollowSaveFollow,
        body: {
          'gameId': gameId,
          'eid': eid,
          'content': content,
          'yc_length': ycLength,
          'position': position,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 删除跟单
  ///*[orderId] 跟单ID
  static Future<ResponseModel> autoFollowDeleteFollow(String orderId) {
    return HttpUtil.fetchModel(FetchType.get,
        url: ApiName.autofollowDeleteFollow,
        queryParameters: {
          'id': orderId,
          'token': GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 站内信一键置为已读
  static Future<ResponseModel> userReadMsgAll() {
    return HttpUtil.fetchModel(FetchType.get,
        url: ApiName.userReadMsgAll,
        queryParameters: {'token': GlobalService.to.userLogin.value.apiSid});
  }

  /// 站内信一键删除
  static Future<ResponseModel> userDeleteMsgAll() {
    return HttpUtil.fetchModel(
      FetchType.post,
      url: ApiName.userDeleteMsgAll,
    );
  }

  /// 用户头像列表
  static Future<ResponseModel<UserGetAvatarSettingModel>>
      userGetAvatarSetting() {
    return HttpUtil.fetchModel<UserGetAvatarSettingModel>(FetchType.post,
        url: ApiName.userGetAvatarSetting,
        body: {'token': GlobalService.to.userLogin.value.apiSid});
  }

  /// 站内信单条删除
  ///*[msgId] 消息记录ID
  static Future<ResponseModel> userDeleteMsg(String? msgId) {
    return HttpUtil.fetchModel(FetchType.post,
        url: ApiName.userDeleteMsg,
        body: {'id': msgId, 'token': GlobalService.to.userLogin.value.apiSid});
  }
}

// export class BetMode {
//   gameId?: string;
//   betIssue?: string;
//   totalNum?: string;
//   endTime?: string;
//   totalMoney?: string;
//   tag?: string;
//   isInstant?: boolean = false;
//   betBean?: Array<BetBean>;
// }

class BetParams {
  List<BetBeanParams>? betBean;
  String? gameId;
  String? betIssue;
  String? totalNum;
  String? endTime;
  String? totalMoney;
  String? tag;
  bool? isInstant;
  String? activeReturnCoinRatio;
  String? position;
  String? ycLength;
  String? eid;

  BetParams({
    this.gameId,
    this.betIssue,
    this.totalNum,
    this.endTime,
    this.totalMoney,
    this.tag,
    this.isInstant = false,
    this.betBean,
    this.activeReturnCoinRatio = '0',
    this.position,
    this.ycLength,
    this.eid,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['activeReturnCoinRatio'] = activeReturnCoinRatio;
    data['gameId'] = gameId;
    data['betIssue'] = betIssue;
    data['totalNum'] = totalNum;
    data['endTime'] = endTime;
    data['totalMoney'] = totalMoney;
    data['tag'] = tag;
    data['betBean'] = betBean?.map((bet) => bet.toJson()).toList();
    data['position'] = position;
    data['yc_length'] = ycLength;
    data['eid'] = eid;
    return data;
  }
}

// export interface BetLotteryData {
//   betInfo?: string; // 连码等累计选中的数字 7,6,3
//   betType?: string; // 欧洲彩票下注方式，1/3, 2/3 ...
//   money?: string; // 金额10.00
//   odds?: string; // 赔率48.8000
//   playId?: string; // 当前彩球id 7127749
//   playIds?: string; // 230 彩票ID

//   betNum?: string; //"1",
//   name?: string; //1,  彩球编号
//   rebate?: string; //"0" 退水

//   exFlag?: string; //本地使用，该条目唯一识别标识
// }

class BetBeanParams {
  String? playId;
  String? money;
  String? betInfo;
  String? playIds;
  String? betType;
  String? odds;
  String? betNum;
  String? name;
  String? rebate;
  BetBeanParams(
      {this.playId,
      this.money,
      this.betInfo,
      this.playIds,
      this.betType,
      this.odds,
      this.betNum,
      this.name,
      this.rebate});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['playId'] = playId;
    data['money'] = money;
    data['betInfo'] = betInfo;
    data['playIds'] = playIds;
    data['betType'] = betType;
    data['odds'] = odds;
    data['betNum'] = betNum;
    data['name'] = name;
    data['rebate'] = rebate;
    return data;
  }
}
