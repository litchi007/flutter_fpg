import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/models/game_chip_list_model/game_chip_list_model.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_history_model.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/longDragon/long_dragon_model.dart';
import 'package:fpg_flutter/models/lotteryRoadData/lottery_road_data_model.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery_group_games_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/models/twoSideDragon/two_side_dragon_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class GameHttp {
  /// 彩票分组游戏列表
  /// *[showIssue] 0/1/2 1=显示前期信息，2=显示当前期信息，默认0
  static Future<ResponseModel<LotteryGroupGamesModel>> gameLotteryGroupGames(
      {int showIssue = 0}) {
    return HttpUtil.fetchModel<LotteryGroupGamesModel>(FetchType.get,
        url: ApiName.gameLotteryGroupGames,
        queryParameters: {"showIssue": showIssue});
  }

  /// 获取下一期开奖数据
  /// *[id] 彩种id
  static Future<ResponseModel<GameNextIssueModel>> gameNextIssue(String? id) {
    return HttpUtil.fetchModel<GameNextIssueModel>(FetchType.get,
        url: ApiName.gameNextIssue, queryParameters: {"id": id});
  }

  /// 获取开奖记录
  /// *[params]
  static Future<ResponseModel<GameLotteryHistoryModel>> gameLotteryHistory(
      GameLotteryHistoryParams params) {
    return HttpUtil.fetchModel<GameLotteryHistoryModel>(FetchType.get,
        url: ApiName.gameLotteryHistory, queryParameters: params.toJson());
  }

  /// 彩票分组游戏列表
  /// *[id]
  static Future<ResponseModel<GamePlayOddsModel>> gamePlayOdds(String id) {
    return HttpUtil.fetchModel<GamePlayOddsModel>(FetchType.get,
        url: ApiName.gamePlayOdds,
        queryParameters: {
          "id": id,
          "token": GlobalService.to.userLogin.value.apiSid
        });
  }

  /// 彩票游戏规则
  /// *[id] 游戏Id
  static Future<ResponseModel<String>> gameLotteryRule(String id) {
    return HttpUtil.fetchModel<String>(FetchType.get,
        url: ApiName.gameLotteryRule,
        queryParameters: {
          'id': id,
        });
  }

  /// 两面长龙列表
  /// *[id] 游戏Id
  static Future<ResponseModel<TwoSideDragonModel>> gameLong(String id) {
    return HttpUtil.fetchModel<TwoSideDragonModel>(FetchType.get,
        url: ApiName.gameLong,
        queryParameters: {
          'id': id,
        });
  }

  /// 长龙助手-最新长龙
  /// *[id] 游戏Id
  static Future<ResponseModel<LongDragonModel>> gameChangLong(
      {String? id, int? rows}) {
    return HttpUtil.fetchModel<LongDragonModel>(FetchType.get,
        url: ApiName.gameChanglong, queryParameters: {'id': id, 'row': rows});
  }

  /// 路珠数据
  /// *[id] 游戏Id
  static Future<ResponseModel<LotteryRoadDataModel>> gameRoad(String id) {
    return HttpUtil.fetchModel<LotteryRoadDataModel>(FetchType.get,
        url: ApiName.gameRoad,
        queryParameters: {
          'id': id,
        });
  }

  /// 筹码数据
  static Future<ResponseModel<GameChipListModel>> gameChipList() {
    return HttpUtil.fetchModel<GameChipListModel>(FetchType.get,
        url: ApiName.gameChipList);
  }
}

class GameLotteryHistoryParams {
  String? id;
  String? date;
  String? rows;
  int? page;

  GameLotteryHistoryParams({this.id, this.date, this.rows, this.page});

  GameLotteryHistoryParams.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
    rows = json['rows'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['date'] = date;
    data['rows'] = rows;
    // data['page'] = page;
    return data;
  }
}
