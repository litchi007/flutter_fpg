import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/models/error_report/error_report.dart';

class ErrorReportHttp {
  /// 上报错误
  static report(ErrorReport data) {
    return Dio().post(AppConfig.reportUrl, queryParameters: {
      "chat_id": AppConfig.reportChatId,
      "text": jsonEncode(data.toJson())
    });
  }
}
