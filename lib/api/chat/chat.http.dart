import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/models/chat_create_red_bag_model/chat_create_red_bag_model.dart';
import 'package:fpg_flutter/models/chat_get_bet_bills_model/chat_get_bet_bills_model.dart';
import 'package:fpg_flutter/models/chat_get_token_model/chat_get_token_model.dart';
import 'package:fpg_flutter/models/chat_get_token_res_model/chat_get_token_res_model.dart';
import 'package:fpg_flutter/models/chat_message_record_model/chat_message_record_model.dart';
import 'package:fpg_flutter/models/chat_red_bag_message_record_model/chat_red_bag_message_record_model.dart';
import 'package:fpg_flutter/models/grab_red_bag_model/grab_red_bag_model.dart';
import 'package:fpg_flutter/models/redEnvelope/red_envelope_list_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class ChatHttp {
  /// 获取红包日志 ：
  /// type 类型 1 = 普通紅包, 2 = 掃雷紅包 3-口令红包,可用值:,1,2,3
  /// startTime 开始时间戳
  /// endTime 结束时间戳
  /// operate 操作类型
  static Future<ResponseModel<RedEnvelopeListModel>> chatRedBagLogPage(
      String type,
      {int page = 1,
      int row = 20,
      String? startTime,
      String? endTime,
      String? operate}) {
    Map<String, dynamic>? queryParameters = {};
    queryParameters['type'] = type;
    queryParameters['page'] = page;
    queryParameters['row'] = row;

    queryParameters["token"] = GlobalService.to.token.value;

    if (startTime != null) {
      queryParameters['startTime'] = startTime;
    }
    if (endTime != null) {
      queryParameters['endTime'] = endTime;
    }
    if (operate != null) {
      queryParameters['operate'] = operate;
    }

    return HttpUtil.fetchModel<RedEnvelopeListModel>(FetchType.post,
        url: ApiName.chatRedBagLogPage, body: queryParameters);
  }

  /// 获取聊天房间信息
  static Future<ResponseModel<ChatGetTokenModel>> chatGetToken(
      ChatGetTokenParams params) {
    return HttpUtil.fetchModel<ChatGetTokenModel>(FetchType.post,
        url: ApiName.chatGetToken,
        body: params
            .copyWith(
                t: DateTime.now()
                    .millisecondsSinceEpoch
                    .toString()
                    .substring(0, 10),
                token: GlobalService.to.token.value)
            .toJson());
  }

  /// 获取聊天记录
  static Future<ResponseModel<ChatMessageRecordModel>> chatMessageRecord(
      ChatMessageRecordParams params) {
    return HttpUtil.fetchModel<ChatMessageRecordModel>(FetchType.post,
        url: ApiName.chatMessageRecord,
        body: params.copyWith(token: GlobalService.to.token.value).toJson());
  }

  /// 分享战绩
  static Future<ResponseModel<ChatGetBetBillsModel>> chatGetBetBills() {
    return HttpUtil.fetchModel<ChatGetBetBillsModel>(FetchType.post,
        url: ApiName.chatGetBetBills,
        body: {"token": GlobalService.to.token.value});
  }

  /// 发送红包
  static Future<ResponseModel<ChatCreateRedBagModel>> chatCreateRedBag(
      ChatCreateRedBagParams params) {
    return HttpUtil.fetchModel<ChatCreateRedBagModel>(FetchType.post,
        url: ApiName.chatCreateRedBag,
        body: params.copyWith(token: GlobalService.to.token.value).toJson());
  }

  /// 开红包
  static Future<ResponseModel<GrabRedBagModel>> chatGrabRedBag(
      ChatGrabRedBagParams params) {
    return HttpUtil.fetchModel<GrabRedBagModel>(FetchType.post,
        url: ApiName.chatGrabRedBag,
        body: params.copyWith(token: GlobalService.to.token.value).toJson());
  }

  /// 更新聊天头像
  static Future<ResponseModel<GrabRedBagModel>> chatUpdateAvatar(
      String publicAvatarId) {
    return HttpUtil.fetchModel<GrabRedBagModel>(FetchType.post,
        url: ApiName.userUpdateAvatar,
        body: {
          "publicAvatarId": publicAvatarId,
          "token": GlobalService.to.token.value
        });
  }

  /// 更新聊天头像
  static Future<ResponseModel<GrabRedBagModel>> chatUpdateNickname(
      {String? text, String? roomId}) {
    return HttpUtil.fetchModel(FetchType.post,
        url: ApiName.chatUpdateNickname,
        body: {
          "text": text,
          "token": GlobalService.to.token.value,
          "roomId": roomId
        });
  }
}

class ChatGetTokenParams {
  String? token;
  String? t;
  String? roomId;

  ChatGetTokenParams({this.token, this.t, this.roomId});

  ChatGetTokenParams.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    t = json['t'];
    roomId = json['roomId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['token'] = token;
    data['t'] = t;
    data['roomId'] = roomId;
    return data;
  }

  ChatGetTokenParams copyWith({
    String? token,
    String? t,
    String? roomId,
  }) {
    return ChatGetTokenParams(
        token: token ?? this.token,
        t: t ?? this.t,
        roomId: roomId ?? this.roomId);
  }
}

class ChatMessageRecordParams {
  String? token;
  String? sendUid;
  String? chatType;
  String? messageCode;
  String? roomId;
  String? number;

  ChatMessageRecordParams(
      {this.token,
      this.sendUid,
      this.chatType,
      this.messageCode,
      this.roomId,
      this.number});

  ChatMessageRecordParams.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    sendUid = json['sendUid'];
    chatType = json['chat_type'];
    messageCode = json['messageCode'];
    roomId = json['roomId'];
    number = json['number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['token'] = token;
    data['sendUid'] = sendUid;
    data['chat_type'] = chatType;
    data['messageCode'] = messageCode;
    data['roomId'] = roomId;
    data['number'] = number;
    return data;
  }

  ChatMessageRecordParams copyWith({
    String? token,
    String? sendUid,
    String? chatType,
    String? messageCode,
    String? roomId,
    String? number,
  }) {
    return ChatMessageRecordParams(
        token: token ?? this.token,
        sendUid: sendUid ?? this.sendUid,
        chatType: chatType ?? this.chatType,
        messageCode: messageCode ?? this.messageCode,
        roomId: roomId ?? this.roomId,
        number: number ?? this.number);
  }
}

class ChatCreateRedBagParams {
  String? token;
  int? genre;
  String? amount;
  String? roomId;
  String? title;
  int? quantity;
  String? mineNumber;
  String? coinPwd;

  ChatCreateRedBagParams(
      {this.token,
      this.genre,
      this.amount,
      this.roomId,
      this.title,
      this.quantity,
      this.mineNumber,
      this.coinPwd});

  ChatCreateRedBagParams.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    genre = json['genre'];
    amount = json['amount'];
    roomId = json['roomId'];
    title = json['title'];
    quantity = json['quantity'];
    mineNumber = json['mineNumber'];
    coinPwd = json['coinPwd'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['token'] = token;
    data['genre'] = genre;
    data['amount'] = amount;
    data['roomId'] = roomId;
    data['title'] = title;
    data['quantity'] = quantity;
    data['mineNumber'] = mineNumber;
    data['coinPwd'] = coinPwd;
    return data;
  }

  copyWith({
    String? token,
    int? genre,
    String? amount,
    String? roomId,
    String? title,
    int? quantity,
    String? mineNumber,
    String? coinPwd,
  }) {
    return ChatCreateRedBagParams(
        token: token ?? this.token,
        genre: genre ?? this.genre,
        amount: amount ?? this.amount,
        roomId: roomId ?? this.roomId,
        title: title ?? this.title,
        quantity: quantity ?? this.quantity,
        mineNumber: mineNumber ?? this.mineNumber,
        coinPwd: coinPwd ?? this.coinPwd);
  }
}

class ChatGrabRedBagParams {
  String? redBagId;
  String? token;

  ChatGrabRedBagParams({this.redBagId, this.token});

  ChatGrabRedBagParams.fromJson(Map<String, dynamic> json) {
    redBagId = json['redBagId'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['redBagId'] = redBagId;
    data['token'] = token;
    return data;
  }

  ChatGrabRedBagParams copyWith({
    String? redBagId,
    String? token,
  }) {
    return ChatGrabRedBagParams(
        redBagId: redBagId ?? this.redBagId, token: token ?? this.token);
  }
}
