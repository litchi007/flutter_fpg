import 'dart:convert';

import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/models/lhcdoc_login_reg_page_info_model/lhcdoc_login_reg_page_info_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class LhcdocHttp {
  /// 获取六合社区页面信息
  static Future<ResponseModel<LhcdocLoginRegPageInfoModel>>
      lhcdocLoginRegPageInfo() {
    return HttpUtil.fetchModel<LhcdocLoginRegPageInfoModel>(FetchType.get,
        url: ApiName.lhcdocLoginRegPageInfo);
  }

  // const params = {
  //   alias: route.params.alias ?? 'forum', // 栏目别名，必填
  //   title: title, // 选填，作者ID
  //   content: content, // 分页页码，非必填
  //   price: 0, // 分页条数，非必填
  //   images: images,
  // };
  /// 发帖子
  static Future<ResponseModel> lhcdocPostContent(
      LhcdocPostContentParams params) {
    return HttpUtil.fetchModel(FetchType.post,
        url: ApiName.lhcdocPostContent,
        body: params.copyWith(token: GlobalService.to.token.value).toJson());
  }
}

class LhcdocPostContentParams {
  String? alias;
  String? title;
  String? content;
  int? price;
  List<String>? images;
  String? token;
  LhcdocPostContentParams(
      {this.alias,
      this.title,
      this.content,
      this.price,
      this.images,
      this.token});

  LhcdocPostContentParams.fromJson(Map<String, dynamic> json) {
    alias = json['alias'];
    title = json['title'];
    content = json['content'];
    price = json['price'];
    images = json['images'].cast<String>();
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['alias'] = alias;
    data['title'] = title;
    data['content'] = content;
    data['price'] = price;
    data['images'] = images ?? [];
    data['token'] = token;
    return data;
  }

  LhcdocPostContentParams copyWith({
    String? alias,
    String? title,
    String? content,
    int? price,
    List<String>? images,
    String? token,
  }) {
    return LhcdocPostContentParams(
        alias: alias ?? this.alias,
        title: title ?? this.title,
        content: content ?? this.content,
        price: price ?? this.price,
        images: images ?? this.images,
        token: token ?? this.token);
  }
}
