import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/models/lottery_my_bet_model/lottery_my_bet_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class ReportHttp {
  /// 长龙助手-我的投注
  /// *[betId] 投注Id
  /// *[gameId] 游戏Id
  /// *[tag] 投注类别, 0:普通投注，1:长龙助手，2:美女直播，3:跟注
  static Future<ResponseModel<LotteryMyBetModel>> reportGetUserRecentBet({
    String? betId,
    String? gameId,
    int? tag,
  }) {
    Map<String, dynamic>? queryParameters = {};
    queryParameters["token"] = GlobalService.to.userLogin.value.apiSid;

    if (betId != null) {
      queryParameters['betId'] = betId;
    }
    if (gameId != null) {
      queryParameters['gameId'] = gameId;
    }
    if (tag != null) {
      queryParameters['tag'] = tag;
    }

    return HttpUtil.fetchModel<LotteryMyBetModel>(FetchType.get,
        url: ApiName.reportGetUserRecentBet, queryParameters: queryParameters);
  }
}
