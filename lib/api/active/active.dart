import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/models/activity_gold_egg_log_model/activity_gold_egg_log_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class ActiveHttp {
  /// 开金蛋日志
  static Future<ResponseModel<ActivityGoldEggLogModel>> activityEggoldenEggLog(String activityId) {
    return HttpUtil.fetchModel<ActivityGoldEggLogModel>(FetchType.get,
        url: ApiName.activityGoldenEggLog,
        queryParameters: {
          "activityId": activityId,
          "token": GlobalService.to.token.value
        });
  }

  /// 开金蛋
  static Future<ResponseModel> activityGoldenEggWin(String activityId) {
    return HttpUtil.fetchModel(FetchType.post,
        url: ApiName.activityGoldenEggWin,
        body: {
          "activityId": activityId,
          "token": GlobalService.to.token.value
        });
  }
}
