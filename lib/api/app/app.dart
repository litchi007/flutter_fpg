import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/key_config.dart';
import 'package:fpg_flutter/models/app_info_app_id_model/app_info_app_id_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class AppHttp {
  /// 获取app信息
  static Future<ResponseModel<AppInfoAppIdModel>> appInfoAppId() {
    return HttpUtil.fetchModel<AppInfoAppIdModel>(FetchType.get,
        url: "${ApiName.app_infoApp_id}${KeyConfig().appId}",
        baseUrl: "${AppDefine.appInfoDomain}api.php?");
  }
}
