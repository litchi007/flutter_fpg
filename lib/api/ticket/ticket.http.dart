import 'package:fpg_flutter/api/api.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_list_model.dart';
import 'package:fpg_flutter/models/lotteryTickets/lottery_tickets_model.dart';
import 'package:fpg_flutter/models/lottery_data/lottery_data_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/http_util.dart';

class TicketHttp {
  /// 获取注单列表 ：
  /// status 订单状态：1=待开奖;2=已中奖3=未中奖;4=已撤单;5=已结算)
  /// category 游戏类型：lottery彩票,real=-真人，game=-电子，card=棋牌，sport=体育, fish=捕鱼
  /// timeZone 时区,可用值:,8,-4
  static Future<ResponseModel<LotteryHistoryListModel>> ticketHistory(
      {String? gameId,
      int? betId,
      int page = 1,
      int row = 30,
      String? category = 'lottery',
      int? status,
      String? startDate,
      String? endDate,
      int? timeZone}) {
    Map<String, dynamic>? queryParameters = {};

    queryParameters["page"] = page;
    queryParameters["row"] = row;
    queryParameters["token"] = GlobalService.to.userLogin.value.apiSid;

    if (gameId != null) {
      queryParameters["gameId"] = gameId;
    }
    if (betId != null) {
      queryParameters["betId"] = betId;
    }
    if (status != null) {
      queryParameters["status"] = status;
    }
    if (category != null) {
      queryParameters["category"] = category;
    }
    if (startDate != null) {
      queryParameters["startDate"] = startDate;
    }
    if (endDate != null) {
      queryParameters["endDate"] = endDate;
    }
    if (timeZone != null) {
      queryParameters["timeZone"] = timeZone;
    }
    return HttpUtil.fetchModel<LotteryHistoryListModel>(FetchType.get,
        url: ApiName.ticketHistory, queryParameters: queryParameters);
  }

  /// 获取彩票统计
  static Future<ResponseModel<LotteryTicketsModel>> ticketLotteryStatistics({
    String? startDate,
    String? endDate,
  }) {
    Map<String, dynamic>? queryParameters = {};
    if (startDate != null) {
      queryParameters["startDate"] = startDate;
    }
    if (endDate != null) {
      queryParameters["endDate"] = endDate;
    }
    queryParameters["token"] = GlobalService.to.userLogin.value.apiSid;

    return HttpUtil.fetchModel<LotteryTicketsModel>(FetchType.get,
        url: ApiName.ticketLotteryStatistics, queryParameters: queryParameters);
  }

  /// 获取注单统计
  static Future<ResponseModel<LotteryDataModel>> ticketLotteryData() {
    return HttpUtil.fetchModel<LotteryDataModel>(FetchType.get,
        url: ApiName.ticketLotteryData,
        queryParameters: {'token': GlobalService.to.userLogin.value.apiSid});
  }
}
