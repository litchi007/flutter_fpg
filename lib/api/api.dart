class ApiName {
  /// * 名称：首页右侧导航栏 请求方式：get
  static String get systemMobileRight => "c=system&a=mobileRight";

  /// * 名称：彩票分组游戏列表 请求方式：post
  static String get gameLotteryGroupGames => "c=game&a=lotteryGroupGames";

  /// * 名称：彩票分组游戏列表 请求方式：get
  static String get gamePlayOdds => "c=game&a=playOdds";

  /// * 名称：彩票当前期数据 请求方式：get
  static String get gameCurIssue => "c=game&a=curIssue";

  /// * 名称：游客投注 请求方式：post
  static String get userGuestBet => "c=user&a=guestBet";

  /// * 名称：用户下注 请求方式：post
  static String get userBet => "c=user&a=bet";

  /// * 名称：取消注单 请求方式：post
  static String get userCancelBet => "c=user&a=cancelBet";

  /// * 名称：即时投注 请求方式：post
  static String get userInstantBet => "c=user&a=instantBet";

  /// * 名称：用户登录 请求方式：post
  static String get userLogin => "c=user&a=login";

  /// * 名称：登录试玩账号 请求方式：post
  static String get userGuestLogin => "c=user&a=guestLogin";

  /// * 名称：获取用户余额 请求方式：post
  static String get userBalance => "c=user&a=balance";

  /// * 名称：彩票每日统计 请求方式：post
  static String get userLotteryDayStat => "c=user&a=lotteryDayStat";

  /// * 名称：获取下一期开奖数据 请求方式：get
  static String get gameNextIssue => "c=game&a=nextIssue";

  /// * 名称：长龙助手 请求方式：get
  static String get gameChanglong => "c=game&a=changlong";

  /// * 名称：彩票规则 请求方式：get
  static String get gameLotteryRule => "c=game&a=lotteryRule";

  /// * 名称：彩票开奖记录 请求方式：get
  static String get gameLotteryHistory => "c=game&a=lotteryHistory";

  /// * 名称：路珠数据 请求方式：get
  static String get gameRoad => "c=game&a=road";

  /// * 名称：两面长龙数据 请求方式：get
  static String get gameLong => "c=game&a=long";

  /// * 名称：获取文档列表数据 请求方式：get
  static String get bbsGameDocList => "c=bbs&a=gameDocList";

  /// * 名称：注单列表 请求方式：get
  static String get ticketHistory => "c=ticket&a=history";

  /// * 名称：彩票统计 请求方式：get
  static String get ticketLotteryStatistics => "c=ticket&a=lotteryStatistics";

  /// * 名称：注单统计 请求方式：get
  static String get ticketLotteryData => "c=ticket&a=lotteryData";

  /// * 名称：红包扫雷日志 请求方式：post
  static String get chatRedBagLogPage => "c=chat&a=redBagLogPage";

  /// * 名称：长龙助手-我的投注 请求方式：get
  static String get reportGetUserRecentBet => "c=report&a=getUserRecentBet";

  /// * 名称：筹码数据 请求方式：get
  static String get gameChipList => "c=game&a=chipList";

  /// * 名称：专家追号开关 请求方式：get
  static String get userExpertSwitch => "c=user&a=expertSwitch";

  /// * 名称：专家追号数据 请求方式：get
  static String get userExpertDataList => "c=user&a=expertDataList";

  /// * 名称：专家排行数据 请求方式：get
  static String get userExpertRank => "c=user&a=expertRank";

  /// * 名称：专家历史数据 请求方式：get
  static String get userExpertHistoryList => "c=user&a=expertHistoryList";

  /// * 名称：获取对应的期数 请求方式：get
  static String get autofollowIssueList => "c=autofollow&a=issueList";

  /// * 名称：获取期号信息 请求方式：get
  static String get autofollowIssueFollowList =>
      "c=autofollow&a=issueFollowList";

  /// * 名称：获取跟单记录 请求方式：get
  static String get autofollowFollowList => "c=autofollow&a=followList";

  /// * 名称：获取跟单详情 请求方式：get
  static String get autofollowFollowInfo => "c=autofollow&a=followInfo";

  /// * 名称：保存跟单 请求方式：post
  static String get autofollowSaveFollow => "c=autofollow&a=saveFollow";

  /// * 名称：删除跟单 请求方式：get
  static String get autofollowDeleteFollow => "c=autofollow&a=deleteFollow";

  /// * 名称：获取app信息 请求方式：get
  static String get app_infoApp_id => "m=app_info&app_id=";

  /// * 名称：获取聊天房间信息 请求方式：get
  static String get chatGetToken => "c=chat&a=getToken";

  /// * 名称：获取聊天记录 请求方式：get
  static String get chatMessageRecord => "c=chat&a=messageRecord";

  /// * 名称：分享战绩 请求方式：post
  static String get chatGetBetBills => "c=chat&a=getBetBills";

  /// * 名称：发送红包 请求方式：post
  static String get chatCreateRedBag => "c=chat&a=createRedBag";

  /// * 名称：打开红包 请求方式：post
  static String get chatGrabRedBag => "c=chat&a=grabRedBag";

  /// * 名称：站内信一键置为已读 请求方式：get
  static String get userReadMsgAll => "c=user&a=readMsgAll";

  /// * 名称：站内信一键删除 请求方式：post
  static String get userDeleteMsgAll => "c=user&a=deleteMsgAll";

  /// * 名称：站内信单条删除 请求方式：post
  static String get userDeleteMsg => "c=user&a=deleteMsg";

  /// * 名称：更新头像 请求方式：post
  static String get userUpdateAvatar => "c=user&a=updateAvatar";

  /// * 名称：头像列表 请求方式：get
  static String get systemAvatarList => "c=system&a=avatarList";

  /// * 名称：用户头像列表 请求方式：get
  static String get userGetAvatarSetting => "c=user&a=getAvatarSetting";

  /// * 名称：修改昵称 请求方式：post
  static String get chatUpdateNickname => "c=chat&a=updateNickname";

  /// * 名称：获取六合社区页面信息 请求方式：get
  static String get lhcdocLoginRegPageInfo => "c=lhcdoc&a=loginRegPageInfo";

  /// * 名称：发帖 请求方式：get
  static String get lhcdocPostContent => "c=lhcdoc&a=postContent";

  /// * 名称：开金蛋日志 请求方式：get
  static String get activityGoldenEggLog => "c=activity&a=goldenEggLog";

  /// * 名称：开金蛋 请求方式：get
  static String get activityGoldenEggWin => "c=activity&a=goldenEggWin";
}
