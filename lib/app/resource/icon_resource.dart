import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/favcor.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class IconResource {
  static String red_ball() {
    return img_root('images/lhc/icon_red');
  }

  static String green_ball() {
    return img_root('images/lhc/icon_green');
  }

  static String blue_ball() {
    return img_root('images/lhc/icon_blue');
  }

  static String grey_ball() {
    return img_root('images/ball/grey_ball');
  }

  static String golden_ball() {
    return img_root('images/europe/europe_ball');
  }

  static String red_ball_3d() {
    return img_root('images/ball/red3');
  }

  static String blue_ball_3d() {
    return img_root('images/ball/blue3');
  }

  static String green_ball_3d() {
    return img_root('images/ball/green3');
  }

  static String sound01() {
    return img_root('images/lhc/icon_sound01');
  }

  static String sound02() {
    return img_root('images/lhc/icon_sound02');
  }
}

String img_mobileTemplateByStaticServer(
    int staticServer, int templateId, String path,
    {String type = 'png'}) {
  String host = UGImageHost.staticServer01;
  switch (staticServer) {
    case 2:
      host = UGImageHost.staticServer02;
      break;
    case 3:
      host = UGImageHost.staticServer03;
      break;
    case 4:
      host = UGImageHost.staticServer04;
      break;
    case 5:
      host = UGImageHost.staticServer05;
      break;
    case 6:
      host = UGImageHost.staticServer06;
      break;
    case 7:
      host = UGImageHost.staticServer07;
      break;
    case 8:
      host = UGImageHost.staticServer08;
      break;
    case 9:
      host = UGImageHost.staticServer09;
      break;
    case 10:
      host = UGImageHost.staticServer10;
      break;
    default:
      break;
  }
  // host = 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz';

  return getImage(
    host: host,
    pType: 'views/mobileTemplate/{id}/images/{name}',
    id: templateId.toString(),
    name: path,
    suffix: type,
  );
}

String getImage(
    {String? host,
    String? pType = '',
    String? id = '',
    String? name = '',
    String? suffix = 'png'}) {
  // Default host if not provided
  host ??= AppDefine.host ?? UGImageHost.staticServer01;
  // Replace {id} and {name} in pType
  pType = pType!.replaceAll('{id}', id!).replaceAll('{name}', name!);
  String s = '$host/$pType.$suffix';

  return s.replaceAll(RegExp(r'([^:]\/)\/+'), '\$1');
}

String img_mobileTemplate(String id, String name, {String? type = 'png'}) {
  return getImage(
    pType: 'views/mobileTemplate/{id}/images/{name}',
    id: id,
    name: name,
    suffix: type,
  );
}

String img_root(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer02, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: path,
      suffix: type); //
}

String img_images(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer02, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_lotteryResult(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer02, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'open_prize/hx/images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_rechageChannelIcon(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer02, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'upload/${Favcor.currentName}/customise/images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_static01(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_static02(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_static03(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_static04(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_static05(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_static06(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_jt(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'jt/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_assets(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'assets/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_web(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer01, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'web/$path',
      suffix: type); // UGImageHost.staticServer02
}

String getImageByIndex(int index, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer02, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'images/grab/course/$index',
      suffix: type); // UGImageHost.staticServer02
}

String img_vueTemplate(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer02, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'static/vueTemplate/vue/images/${path}',
      suffix: type); // UGImageHost.staticServer02
}

// 线上服务器的图片
String getHtml5Image(int id, String path, {String? type = 'png'}) {
  return getHtml5ImageByHost(id: id, path: path, type: type);
}

String getHtml5ImageByHost({
  String? host,
  int? id,
  String? path,
  String? type,
}) {
  if (id!.isNaN) {
    return getImage(
      //AppDefine.host ?? UGImageHost.staticServer01,
      pType: 'views/mobileTemplate/{id}/images/{name}',
      id: id.toString(),
      name: path,
      suffix: type,
    );
  } else {
    return img_images(path!, type: type);
  }
}

String img_home(String path, {String? type = 'png'}) {
  return getImage(
      host: UGImageHost
          .staticServer02, // 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz',
      pType: 'views/home/images/$path',
      suffix: type); // UGImageHost.staticServer02
}

String img_chat(String path, {String type = 'png'}) {
  return getImage(
    host: UGImageHost.staticServer02,
    pType: 'upload/chat/{id}',
    id: path,
    name: null,
    suffix: type,
  );
}

class PayIcon {
  static final bank_online = Res.bank_online;
  static final alipay_online = Res.zfb_icon;
  static final bank_transfer = img_images('online-pay');
  static final xnb_transfer = Res.xnb_icon;
  static final qq_online = Res.qq_online;
  static final wechat_online = img_images('wechatpay-icon');
  static final topay_online = Res.topay_online;
  static final gopay_online = !AppDefine.inSites(['f070', 'f070b'])
      ? img_images('online-gopay')
      : img_root('/upload/f070/customise/images/bank_sort_icongopay_online',
          type: 'jpg');
  static final upay_online = img_images('online-upay');
  static final okp_online = img_images('online-okp');
  static final kdou_online = img_images('kdou-online');
  static final abpay_online = img_images('online-abpay');
  static final jiujiubapay_online = img_images('online-998pay');
  static final douyin_online = img_images('douyin-online');
  static final jingdong_online = img_images('jingdong-icon');
  static final quick_online = img_images('quick-pay');
  static final zalo_online = img_images('online-zalo');
  static final momopay_online = img_images('online-zalo');
  static final viettelpay_online = img_images('online-viettelpay');
  static final siyu_transfer = img_images('transfer_siyu');
  static final ez_recharge = img_images('ez_recharge');
  static final ecny_online = img_images('ecny-online');
  static final cbrpay_online = img_images('cbrpay-online', type: 'jpg');
  static final edan_online = img_images('edan-online');
  static final goubao_online = img_images('online-goubao');
  static final rmbpay_online = img_images('online-rmbpay');
  static final wechat_transfer = img_images('wechat-icon');
  static final yunshanfu_transfer = img_images('yunshanfu');
  static final qqpay_transfer = img_images('qqpay-icon');
  static final wechat_alipay_transfer = img_images('wechat_alipy');
  static final alipay_transfer = img_images('zfb-icon');
  static final tenpay_online = img_images('yunshanfu');
  static final tenpay_transfer = img_images('cft-icon');
  static final baidu_online = img_images('baidu-icon');
  static final jdzz_transfer = img_images('jingdong1-icon');
  static final ddhb_transfer = img_images('dingding-icon');
  static final wxsm_transfer = img_images('wesm-icon');
  static final dshb_transfer = img_images('duoshan-icon');
  static final xlsm_transfer = img_images('xianliaosm-icon');
  static final zhifubao_transfer = img_images('zhifubao-icon');
  static final zfbzyhk_transfer = img_images('zfb-icon');
  static final wxzfbsm_transfer = img_images('wechat_alipy');
  static final wxzsm_transfer = img_images('wechat-icon');
  static final yxsm_transfer = img_images('yxsm-icon');
  static final ysf_transfer = img_images('ysf-icon');
  static final liaobei_transfer = img_images('liaobei-icon');
  static final upi_transfer = img_images('upi-icon');
  static final bankalipay_transfer = img_images('bankalipay-icon');
  static final alipayenvelope_transfer = img_images('alipayenvelope-icon');
  static final aliyin_transfer = img_images('aliyin-icon');
  static final huobi_online = img_images('huobi-icon');
  static final yinlian_online = img_images('online-pay');
  static final xnb_online = !AppDefine.inSites(['f070', 'f070b'])
      ? img_images('xnb-icon')
      : img_root('/upload/f070/customise/images/bank_sort_iconxnb_online',
          type: 'jpg');
  static final dk_online = img_images('xnb-icon');
  static final alihb_online = img_images('xnb-icon');
  static final doy_transfer = img_images('doy-icon');
  static final zalo_transfer = img_images('zalo-icon');
  static final momo_transfer = img_images('momo-icon');
  static final viettelpay_transfer = img_images('viettelpay-icon');
  static final vippay_online = img_images('online-vippay');
  static final pending_order = img_images('pending_order');
  static final tgpay_online = !AppDefine.inSites(['f058a', 'f058b'])
      ? img_images('online-tgpay', type: 'jpg')
      : img_root('/upload/f058/customise/images/bank_sort_icontgpay_online',
          type: 'jpg');
  static final agpay_online = img_images('online-agpay');
  static final obpay_online = img_images('online-obpay');
  static final bobipay_online = img_images('online-bobipay', type: 'jpg');
  static final mpay_online = img_images('online-mpay', type: 'jpg');
  static final guantianshouyintai_online =
      img_images('online-guantianshouyintai');
  static final embedwallet_online = img_images('online-embedwallet');
  static final embedwallet_online_f028 =
      img_static01('app/online-embedwallet_f028', type: 'jpg');
  static final miaofupay_online = img_images('online-miaofupay');
  static final cgpay_online = img_images('online-cgpay');
  static final ebpay_online = img_images('online-ebpay');
  static final fpay_online = img_images('online-fpay');
  static final didishoyintai_online = img_images('online-didipay');
  static final wanbipay_online = img_images('online-wanbipay');
  static final kbao_online = img_images('online-kbaopay');
  static final jdpay_online = !AppDefine.inSites(['f058a', 'f058b'])
      ? img_images('online-jdpay')
      : img_root('/upload/f058/customise/images/bank_sort_iconjdpay_online',
          type: 'jpg');
  static final zfbzk_transfer =
      !AppDefine.inSites(['f036b', 'f058a', 'f058b', 'f070', 'f070b'])
          ? img_images('zfb-icon')
          : img_root(
              '/upload/f058/customise/images/bank_sort_iconzfbzk_transfer',
              type: 'jpg');
  static final balingba_online = img_root(
      '/upload/f036/customise/images/bank_sort_iconxnb_online',
      type: 'jpg');
  static final qiannengpay_online = img_images('online-qiannengpay');

  static final Map<String, String> _icons = {
    'bank_online': Res.bank_online,
    'alipay_online': Res.zfb_icon,
    'bank_transfer': img_images('online-pay'),
    'xnb_transfer':
        img_rechageChannelIcon('bank_sort_iconxnb_transfer', type: 'jpg'),
    'qq_online': Res.qq_online,
    'wechat_online': img_images('wechatpay-icon'),
    'topay_online': Res.topay_online,
    'gopay_online': !AppDefine.inSites(['f070', 'f070b'])
        ? img_images('online-gopay')
        : img_root('/upload/f070/customise/images/bank_sort_icongopay_online',
            type: 'jpg'),
    'upay_online': img_images('online-upay'),
    'okp_online': img_images('online-okp'),
    'kdou_online': img_images('kdou-online'),
    'abpay_online': img_images('online-abpay'),
    'jiujiubapay_online': img_images('online-998pay'),
    'douyin_online': img_images('douyin-online'),
    'jingdong_online': img_images('jingdong-icon'),
    'quick_online': img_images('quick-pay'),
    'zalo_online': img_images('online-zalo'),
    'momopay_online': img_images('online-zalo'),
    'viettelpay_online': img_images('online-viettelpay'),
    'siyu_transfer': img_images('transfer_siyu'),
    'ez_recharge': img_images('ez_recharge'),
    'ecny_online': img_images('ecny-online'),
    'cbrpay_online': img_images('cbrpay-online', type: 'jpg'),
    'edan_online': img_images('edan-online'),
    'goubao_online': img_images('online-goubao'),
    'rmbpay_online': img_images('online-rmbpay'),
    'wechat_transfer': img_images('wechat-icon'),
    'yunshanfu_transfer': img_images('yunshanfu'),
    'qqpay_transfer': img_images('qqpay-icon'),
    'wechat_alipay_transfer': img_images('wechat_alipy'),
    'alipay_transfer': img_images('zfb-icon'),
    'tenpay_online': img_images('yunshanfu'),
    'tenpay_transfer': img_images('cft-icon'),
    'baidu_online': img_images('baidu-icon'),
    'jdzz_transfer': img_images('jingdong1-icon'),
    'ddhb_transfer': img_images('dingding-icon'),
    'wxsm_transfer': img_images('wesm-icon'),
    'dshb_transfer': img_images('duoshan-icon'),
    'xlsm_transfer': img_images('xianliaosm-icon'),
    'zhifubao_transfer': img_images('zhifubao-icon'),
    'zfbzyhk_transfer': img_images('zfb-icon'),
    'wxzfbsm_transfer': img_images('wechat_alipy'),
    'wxzsm_transfer': img_images('wechat-icon'),
    'yxsm_transfer': img_images('yxsm-icon'),
    'ysf_transfer': img_images('ysf-icon'),
    'liaobei_transfer': img_images('liaobei-icon'),
    'upi_transfer': img_images('upi-icon'),
    'bankalipay_transfer': img_images('bankalipay-icon'),
    'alipayenvelope_transfer': img_images('alipayenvelope-icon'),
    'aliyin_transfer': img_images('aliyin-icon'),
    'huobi_online': img_images('huobi-icon'),
    'yinlian_online': img_images('online-pay'),
    'xnb_online': !AppDefine.inSites(['f070', 'f070b'])
        ? img_images('xnb-icon')
        : img_root('/upload/f070/customise/images/bank_sort_iconxnb_online',
            type: 'jpg'),
    'dk_online': img_images('xnb-icon'),
    'alihb_online': img_images('xnb-icon'),
    'doy_transfer': img_images('doy-icon'),
    'zalo_transfer': img_images('zalo-icon'),
    'momo_transfer': img_images('momo-icon'),
    'viettelpay_transfer': img_images('viettelpay-icon'),
    'vippay_online': img_images('online-vippay'),
    'pending_order': img_images('pending_order'),
    'tgpay_online': !AppDefine.inSites(['f058a', 'f058b'])
        ? img_images('online-tgpay', type: 'jpg')
        : img_root('/upload/f058/customise/images/bank_sort_icontgpay_online',
            type: 'jpg'),
    'agpay_online': img_images('online-agpay'),
    'obpay_online': img_images('online-obpay'),
    'bobipay_online': img_images('online-bobipay', type: 'jpg'),
    'mpay_online': img_images('online-mpay', type: 'jpg'),
    'guantianshouyintai_online': img_images('online-guantianshouyintai'),
    'embedwallet_online': img_images('online-embedwallet'),
    'embedwallet_online_f028':
        img_static01('app/online-embedwallet_f028', type: 'jpg'),
    'miaofupay_online': img_images('online-miaofupay'),
    'cgpay_online': img_images('online-cgpay'),
    'ebpay_online': img_images('online-ebpay'),
    'fpay_online': img_images('online-fpay'),
    'didishoyintai_online': img_images('online-didipay'),
    'wanbipay_online': img_images('online-wanbipay'),
    'kbao_online': img_images('online-kbaopay'),
    'jdpay_online': !AppDefine.inSites(['f058a', 'f058b'])
        ? img_images('online-jdpay')
        : img_root('/upload/f058/customise/images/bank_sort_iconjdpay_online',
            type: 'jpg'),
    'zfbzk_transfer':
        !AppDefine.inSites(['f036b', 'f058a', 'f058b', 'f070', 'f070b'])
            ? img_images('zfb-icon')
            : img_root(
                '/upload/f058/customise/images/bank_sort_iconzfbzk_transfer',
                type: 'jpg'),
    // 'balingba_online': img_root(
    //     '/upload/f036/customise/images/bank_sort_iconxnb_online',
    //     type: 'jpg'),
    'balingba_online': img_images('online-808pay'),
    'qiannengpay_online': img_images('online-qiannengpay'),
    'truemoney_transfer': img_images('online-truemoney')
  };
  static String getIcon(String key) {
    return _icons[key] ??
        'Default Path'; // Provide a default path or handle missing keys
  }
}

class Res {
  static final bankhl1 = img_static06('bankhl1', type: 'svg');
  static final btc = img_static06('btc', type: 'svg');
  static final edit = img_assets('szxxhdpi');

  // 球类
  static final red_ball_3 = img_assets('ball/ball_red_3');
  static final green_ball_3 = img_assets('ball/ball_green_3');
  static final blue_ball_3 = img_assets('ball/ball_blue_3');
  static final red_ball = img_root('images/lhc/icon_red');
  static final green_ball = img_root('images/lhc/icon_green');
  static final blue_ball = img_root('images/lhc/icon_blue');
  static final grey_ball = img_root('images/ball/grey_ball');
  static final golden_ball = img_root('images/europe/europe_ball');
  static final red_ball_3d = img_root('images/ball/red3');
  static final blue_ball_3d = img_root('images/ball/blue3');
  static final green_ball_3d = img_root('images/ball/green3');

  // 骰子
  static final sz1 = img_images('ball/s_2_1');
  static final sz2 = img_images('ball/s_2_2');
  static final sz3 = img_images('ball/s_2_3');
  static final sz4 = img_images('ball/s_2_4');
  static final sz5 = img_images('ball/s_2_5');
  static final sz6 = img_images('ball/s_2_6');

  // // 彩色骰子
  // static final  colorSz1 = require('../../../rn/assets/images/dice-color/1.png');
  // static final  colorSz2 = require('../../../rn/assets/images/dice-color/2.png');
  // static final  colorSz3 = require('../../../rn/assets/images/dice-color/3.png');
  // static final  colorSz4 = require('../../../rn/assets/images/dice-color/4.png');
  // static final  colorSz5 = require('../../../rn/assets/images/dice-color/5.png');
  // static final  colorSz6 = require('../../../rn/assets/images/dice-color/6.png');

  // // 大乐透
  // static final  dltBlue = require('../../../rn/assets/images/ball/dltblue.png');
  // static final  dltRed = require('../../../rn/assets/images/ball/dltred.png');

  // 主页菜单
  static final activity = img_assets('tab/activity');
  static final bank = img_assets('tab/bank');
  static final bet = img_assets('tab/bet');
  static final bottom_chess_red = img_assets('tab/bottom_chess_red');
  static final bottom_electronic_black =
      img_assets('tab/bottom_electronic_black');
  static final bottom_elesports_black =
      img_assets('tab/bottom_elesports_black');
  static final bottom_fishicon_black = img_assets('tab/bottom_fishicon_black');
  static final capital = img_assets('tab/capital');
  static final chat = img_assets('tab/chat');
  static final chess_electronic1 = img_assets('tab/chess_electronic1');
  static final earnings = img_assets('tab/earnings');
  static final home = img_assets('tab/home');
  static final info = img_assets('tab/info');
  static final interest = img_assets('tab/interest');
  static final kf = img_assets('tab/kf');
  static final lottery = img_assets('tab/lottery');
  static final new_bottom_chess_black =
      img_assets('tab/new_bottom_chess_black');
  static final new_draw = img_assets('tab/new_draw');
  static final bet_lottery = img_assets('tab/tab_bet');
  static final new_lottery_ticket_hall1 =
      img_assets('tab/new_lottery_ticket_hall1');
  static final real_video1 = img_assets('tab/real_video1');
  static final safety = img_assets('tab/safety');
  static final shenqing = img_assets('tab/shenqing');
  static final task = img_assets('tab/task');
  static final transform = img_assets('tab/transform');
  static final trends = img_assets('tab/trends');
  static final tzjl = img_assets('tab/tzjl');
  static final user = img_assets('tab/user');
  static final community = img_assets('tab/fa-information');
  static final yboLottery = img_assets('tab/ybo_bottom_lottery');
  static final nationalQuiz = img_assets('tab/bottom_qmjc');
  static final hotIcon = img_assets('tab/hot_icon', type: 'gif');
  static final expertOrder = img_assets('tab/expertPlan');

  // 彩票分类图标
  static final c11x5 = img_assets('lottery/c11x5');
  static final car = img_assets('lottery/car');
  static final ft = img_assets('lottery/ft');
  static final happy = img_assets('lottery/happy');
  static final hot = img_assets('lottery/hot');
  static final js = img_assets('lottery/js');
  static final k3 = img_assets('lottery/k3');
  static final lh = img_assets('lottery/lh');
  static final nn = img_assets('lottery/nn');
  static final pcdd = img_assets('lottery/pcdd');
  static final qxc = img_assets('lottery/qxc');
  static final shishi = img_assets('lottery/shishi');
  static final other = img_assets('lottery/other');

  // 存款图标
  static final zfb_icon = img_images('zfb-icon');
  static final wechat_online = img_images('wechat-icon');
  static final wechatpay_icon = img_images('wechatpay-icon');
  static final weixinhaoyou = img_images('weixinhaoyou');
  static final wx_zfb = img_images('wx-zfb');
  static final yxsm_transfer = img_images('yxsm-icon');
  static final bank_online = img_images('online-pay');
  static final bank_card = img_images('bank_card');
  static final transfer = img_images('transfer');
  static final cft_icon = img_images('cft-icon');
  static final yunshanfu = img_images('yunshanfu');
  static final qq_online = img_images('qqpay-icon');
  static final baidu = img_images('baidu-icon');
  static final jd = img_images('jingdong-icon');
  static final quick_online = img_images('quick-pay');
  static final xnb_icon = img_root(
    '/upload/f053/customise/images/bank_sort_iconxnb_transfer',
    type: 'jpg',
  );
  static final dingding = img_images('dingding-icon');
  static final duosan = img_images('duoshan-icon');
  static final xlsm = img_images('xianliaosm-icon');
  static final aliyin = img_images('aliyin-icon');
  static final ht = img_images('huobi-icon');
  static final btc_deposit_icon = img_images('xnbcz-icon');
  static final bank_alipay = img_images('bankalipay-icon');
  static final upi = img_images('upi-icon');
  static final promotion_more = img_images('zh/more', type: 'gif');
  static final upi_icon = img_images('upi-icon');
  static final alipayPay_icon = img_images('alipayenvelope-icon');
  static final viettelpay_icon = img_images('online-viettelpay');
  static final topay_online = !AppDefine.inSites(['f070', 'f070b'])
      ? img_images('online-top')
      : img_root(
          '/upload/f070/customise/images/bank_sort_icontopay_online',
          type: 'jpg',
        );

  // 秒秒彩
  static final mmckjz = img_root('web/images/zh/mmckjz'); // 开奖中
  static final mmcwzj = img_root('web/images/zh/mmcwzj'); // 未中奖
  static final mmczjl = img_root('web/images/zh/mmczjl'); // 中奖啦
  static final mmczt = img_root('web/images/zh/mmczt'); // 暂停
  static final mmczdtz = img_root('web/images/zh/mmczdtz'); // 自动投注
  static final mmcbackpic =
      img_root('web/static final /vueTemplate/vue/images/mmcbackpic'); // 背景
  static final mmcBg23 = img_root('web/images/mmcBg23'); // 牛仔背景

  // 筹码
  static final chip0 = img_assets('chip/chip1');
  static final chip1 = img_assets('chip/chip2');
  static final chip2 = img_assets('chip/chip3');
  static final chip3 = img_assets('chip/chip4');
  static final chip4 = img_assets('chip/chip5');
  static final chip5 = img_assets('chip/chip6');
  static final chip6 = img_assets('chip/chip7');
  static final clr = img_assets('chip/clear2');

  // 彩票功能跳转图片
  static final tv1 = img_assets('tv/tv');
  static final tv_long = img_assets('tv/long');
  static final tv_data = img_assets('tv/data');
  static final tv_trophy = img_static01('kjw_01');
  static final public_icon = img_static06('publicIcon');

  // 虚拟币教程

  static final hbjc_img = img_assets('tutorial/hbjc_img', type: 'jpg');
  static final c012_virtualcoin_icon =
      img_assets('tutorial/c012_virtualcoin_icon', type: 'jpg');
  static final c200_virtualcoin_icon = img_assets('tutorial/c200');
  static final c200_GOpay_icon = img_assets('tutorial/c200gopay1');
  static final c200_OKpay_icon = img_assets('tutorial/c200okpay');
  static final c200_CGpay_icon = img_assets('tutorial/c200CGpay');
  static final c137_virtualcoin_icon = img_assets('tutorial/c137');
  static final c108_virtualcoin_icon = img_assets('tutorial/c108', type: 'jpg');
  static final c158_virtualcoin_icon = img_assets('tutorial/c158');
  static final c069_virtualcoin_icon = img_assets('tutorial/c069', type: 'jpg');
  static final c134_virtualcoin_icon = img_assets('tutorial/c134');
  static final c134_ok_virtualcoin_icon = img_assets('tutorial/OkPayTutorial');
  static final c208_virtualcoin_icon = img_assets('tutorial/c208');
  static final c208_okpaycoin_icon = img_assets('tutorial/OKpayC208');
  static final c208_cgpaycoin_icon = img_assets('tutorial/CGpayC208');
  static final c213_virtualcoin_icon = img_assets('tutorial/c213');
  static final c205_virtualcoin_icon = img_assets('tutorial/c205');
  static final c269_virtualcoin_icon = img_assets('tutorial/c269');
  static final okpay_virtualcoin_icon = img_assets('tutorial/okpay_jc');

  // 农场

  static final xync_num_01 = img_assets('ball/xync_num_01');
  static final xync_num_02 = img_assets('ball/xync_num_02');
  static final xync_num_03 = img_assets('ball/xync_num_03');
  static final xync_num_04 = img_assets('ball/xync_num_04');
  static final xync_num_05 = img_assets('ball/xync_num_05');
  static final xync_num_06 = img_assets('ball/xync_num_06');
  static final xync_num_07 = img_assets('ball/xync_num_07');
  static final xync_num_08 = img_assets('ball/xync_num_08');
  static final xync_num_09 = img_assets('ball/xync_num_09');
  static final xync_num_10 = img_assets('ball/xync_num_10');
  static final xync_num_11 = img_assets('ball/xync_num_11');
  static final xync_num_12 = img_assets('ball/xync_num_12');
  static final xync_num_13 = img_assets('ball/xync_num_13');
  static final xync_num_14 = img_assets('ball/xync_num_14');
  static final xync_num_15 = img_assets('ball/xync_num_15');
  static final xync_num_16 = img_assets('ball/xync_num_16');
  static final xync_num_17 = img_assets('ball/xync_num_17');
  static final xync_num_18 = img_assets('ball/xync_num_18');
  static final xync_num_19 = img_assets('ball/xync_num_19');
  static final xync_num_20 = img_assets('ball/xync_num_20');

  // 飞机

  static final aircraft_01 = img_assets('ball/aircraft_01');
  static final aircraft_02 = img_assets('ball/aircraft_02');
  static final aircraft_03 = img_assets('ball/aircraft_03');
  static final aircraft_04 = img_assets('ball/aircraft_04');
  static final aircraft_05 = img_assets('ball/aircraft_05');
  static final aircraft_06 = img_assets('ball/aircraft_06');
  static final aircraft_07 = img_assets('ball/aircraft_07');
  static final aircraft_08 = img_assets('ball/aircraft_08');
  static final aircraft_09 = img_assets('ball/aircraft_09');
  static final aircraft_10 = img_assets('ball/aircraft_10');

  // 水果

  static final fruit_01 = img_assets('ball/fruit_01');
  static final fruit_02 = img_assets('ball/fruit_02');
  static final fruit_03 = img_assets('ball/fruit_03');
  static final fruit_04 = img_assets('ball/fruit_04');
  static final fruit_05 = img_assets('ball/fruit_05');
  static final fruit_06 = img_assets('ball/fruit_06');
  static final fruit_07 = img_assets('ball/fruit_07');
  static final fruit_08 = img_assets('ball/fruit_08');
  static final fruit_09 = img_assets('ball/fruit_09');
  static final fruit_10 = img_assets('ball/fruit_10');

  // 游戏大厅

  static final lottery_ticket = img_assets('lottery/lo');
  static final game = img_assets('lottery/ga');
  static final fish = img_assets('lottery/fi');
  static final chess = img_assets('lottery/chess');
  static final dj = img_assets('lottery/d');
  static final real_person = img_assets('lottery/re');
  static final sports = img_assets('lottery/sp');
  static final blockchain = img_assets('lottery/blockchain');
  static final usericon = img_assets('lottery/userwt');
  static final refreshicon = img_assets('lottery/refresh');

  // 导航 三根线

  static final gengduo = img_assets('gengduo');

  // 长龙注单详情背景
  static final betDetailBg = img_images('paper');

  // 第三方游戏返回

  static final back_home = img_assets('back_home');

  // 红包

  static final redBg =
      img_root('web/static/vueTemplate/vue/images/my/red_pack_big');

  // 对话框关闭按钮
  static final closeDialog = img_assets('dialog/dialog_close');

  // 猪
  static final pig = img_images('pig');

  // 红包雨
  static final redBagRain = img_static01('app/redBagRain');

  // 边框、声音

  static final border = img_root('images/lhc/icon_border');
  static final sound01 = img_root('images/lhc/icon_sound01');
  static final sound02 = img_root('images/lhc/icon_sound02');

  // 菜单

  static final menu = img_assets('menu/menu');
  static final bottom_menu = img_assets('menu/memu');

  // 咪牌、通知、按钮

  static final squint = img_assets('gyg');
  static final notice = img_assets('notice');
  static final ck = img_assets('ck');
  static final qk = img_assets('qk');
  static final zxkf = img_assets('zxkf');
  static final yhkgl = img_assets('yhkgl');

  // 刮刮乐

  static final gyg_top = img_home('gyg/top');
  static final gyg_close = img_home('gyg/close');
  static final gyg_bg = img_home('gyg/gyg_bg');
  static final gyg_bgd = img_home('gyg/gyg_bgd');
  static final gyg_big_bg = img_root('web/views/home/images/gyg/gyg_bg_big');
  static final gyg_mask = img_root('web/views/home/images/gyg/gyg_mask');

  static final home_top = img_static01('backTop01', type: 'gif');

  // 任务

  static final task_title = img_root('web/images/zh/task_title');

  // 六合图库顶部收藏

  static final collect = img_images('lhc/collect');

  // 加载中、暂无数据

  static final no_data = img_static05('lhc/search_noData');
  static final empty = img_images('grab/ico_noData', type: 'svg');

  // 中大奖

  static final zdj = img_static03('zdj', type: 'gif');

  // 5个icon

  static final publicIcon = img_images('publicIcon');

  // 筹码

  static final chipGroup = img_images('chouma-bg');
}
