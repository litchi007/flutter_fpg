import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/information/Information_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/sysConfModel/MobileMenu.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class InformationPage extends StatefulWidget {
  const InformationPage({super.key});

  @override
  State<StatefulWidget> createState() => _InformationPageState();
}

class _InformationPageState extends State<InformationPage>
    with TickerProviderStateMixin {
  final InformationController controller = Get.put(InformationController());
  late TabController _tabController;

  final List<Tab> tabs = <Tab>[
    const Tab(text: '其他'),
    const Tab(text: '港彩'),
    const Tab(text: '新彩')
  ];

  @override
  void initState() {
    // _tabController = TabController(length: 2, vsync: this);
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          titleWidget: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '网址大全',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.sp,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        body: Column(children: [
          Container(
              color: AppColors.ffe9e9e9,
              child: TabBar(
                controller: _tabController,
                physics: const ClampingScrollPhysics(),
                indicatorSize: TabBarIndicatorSize.label,
                tabAlignment: TabAlignment.fill,
                labelStyle:
                    TextStyle(fontSize: 17, fontWeight: FontWeight.w900),
                indicator: null,
                labelColor: AppColors.drawMenu,
                indicatorColor: AppColors.drawMenu,
                unselectedLabelColor: Colors.black,
                tabs: tabs,
              )),
          Expanded(
            child: Obx(() => TabBarView(
                    controller: _tabController,
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      _getTabView(controller.a.value),
                      _getTabView(controller.b.value),
                      _getTabView(controller.c.value)
                    ])),
          )
        ]));
  }

  Widget _getTabView(List<SettingItem> lists) {
    return GridView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
        physics: const BouncingScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4, // Number of columns
          crossAxisSpacing: 20.w,
          mainAxisSpacing: 20.w,
          // mainAxisExtent: 110.w,
          // childAspectRatio: 3, // Adjust the ratio to match your design
        ),
        itemCount: lists.length,
        itemBuilder: (context, index) {
          final item = lists[index];
          return GestureDetector(
              onTap: () async {
                RegExp reg = RegExp('^(http|https)://');
                if (item.link != null && reg.hasMatch(item.link ?? "")) {
                  String url = item.link ?? "";
                  // AppNavigator.toNamed(webAppPage, arguments: [url, item.name]);
                  launchUrl(Uri.parse(url));
                }
              },
              child: Column(
                children: [
                  AppImage.network('${item.url}'),
                  Gap(10.h),
                  Text(
                    item.name ?? "",
                    style:
                        TextStyle(fontSize: 17.sp, fontWeight: FontWeight.bold),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ));
        });
  }
}
