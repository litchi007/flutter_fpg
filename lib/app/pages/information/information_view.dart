import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_notice_widget.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';

class InformationView extends StatefulWidget {
  const InformationView({super.key});

  @override
  State<StatefulWidget> createState() => _InformationViewState();
}

class _InformationViewState extends State<InformationView> {
  final List<String> images = [
    'https://wwwstatic04.fdgdggduydaa008aadsdf008.xyz/upload/t002/customise/images/m_banner_2.jpg?v=1702477340',
  ];
  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();

    return Scaffold(
      backgroundColor: appThemeColors?.homeBackgroundColor,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        titleWidget: Row(
          children: [
            AppImage.network(
              'https://images.7h79e5.com/upload/f058/customise/images/m_logo_2.jpg?v=1678795487',
              height: 50.w,
              width: 300.w,
              fit: BoxFit.fill,
            ),
            const Text('title')
          ],
        ),
        actionWidgets: const [
          // AppImage.svgByAsset(
          //   'tabbar/iconwode.svg',
          //   width: 25.w,
          //   height: 25.w,
          // ),
          Icon(
            Icons.menu,
            color: Colors.white,
          )
        ],
      ),
      body: Column(
        children: [
          CarouselSlider(
            options: CarouselOptions(
              height: 300.w,
              viewportFraction: 1.0,
              enlargeCenterPage: false,
              autoPlay: true,
            ),
            items: images
                .map((item) => Container(
                      child: Center(
                          child: AppImage.network(
                        item,
                        fit: BoxFit.cover,
                        height: 300.w,
                      )),
                    ))
                .toList(),
          ),
          // Container(
          //   width: double.infinity,
          //   padding: EdgeInsets.symmetric(vertical: 10.w),
          //   color: const Color(0xffB7CBDD),
          //   child: const AppNoticeWidget(
          //     title: "notice",
          //   ),
          // ),
          Gap(
            10.w,
          ),
          Container(
            width: double.infinity,
            height: 100,
            decoration: BoxDecoration(
              border: Border.all(),
            ),
            child: const Column(
              children: [],
            ),
          ),
          Expanded(
              child: GridView.builder(
            shrinkWrap: true,
            // physics: BouncingScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, // Number of columns
              crossAxisSpacing: 0.0,
              mainAxisSpacing: 0.0,
              childAspectRatio: 3,
              // mainAxisExtent: 110.w,
              // childAspectRatio: 3, // Adjust the ratio to match your design
            ),
            itemCount: 22,
            itemBuilder: (context, index) {
              return Container(
                decoration: const BoxDecoration(
                    // borderRadius: BorderRadius.circular(8.0),
                    // border: Border.all(color: Colors.grey),
                    border: Border(
                        bottom: BorderSide(color: Colors.grey),
                        right: BorderSide(color: Colors.grey))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppImage.network(
                      '',
                      width: 50,
                      height: 50,
                    ),
                    // SizedBox(width: 10),
                    const Text(
                      'label',
                      style: TextStyle(color: Color(0xff659DC7)),
                    ),
                  ],
                ),
              );
            },
          ))
        ],
      ),
    );
  }
}
