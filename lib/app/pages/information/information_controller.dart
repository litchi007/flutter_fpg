import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/sysConfModel/MobileMenu.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class InformationController extends GetxController {
  final AppGameRepository _appGameRepository = AppGameRepository();

  RxList<SettingItem> a = RxList();
  RxList<SettingItem> b = RxList();
  RxList<SettingItem> c = RxList();
  Rx<MobileMenuSetting> infoData = MobileMenuSetting().obs;
  RxBool showView = true.obs;

  @override
  void onReady() {
    // _appGameRepository.homeGames().then((data) {
    //   data.navs
    // });
    try {
      LogUtil.w(
          "AppDefine.systemConfig?.mobileMenu ${AppDefine.systemConfig?.mobileMenu}");
      var settings = AppDefine.systemConfig?.mobileMenu
          ?.firstWhere((element) =>
              element.name == '寻宝' || element.path == "/information")
          .setting;
      MobileMenuSetting data = MobileMenuSetting.fromJson(settings);
      infoData.value = data;
      for (int i = 0; i < infoData.value.items!.length; i++) {
        if (infoData.value.items![i].name!.contains('港彩')) {
          b.add(infoData.value.items![i]);
        } else if (infoData.value.items![i].name!.contains('新彩')) {
          c.add(infoData.value.items![i]);
        } else {
          a.add(infoData.value.items![i]);
        }
      }
    } catch (e) {
      AppLogger.e('InformationController', error: e);
    }

    // TODO: implement onReady
    super.onReady();
  }
}
