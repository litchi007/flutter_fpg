import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/chess/chess_controller.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_custom_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/GameCategory.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class SeriesLobbyPage extends StatefulWidget {
  const SeriesLobbyPage({super.key});

  @override
  State<StatefulWidget> createState() => _SeriesLobbyPageState();
}

class _SeriesLobbyPageState extends State<SeriesLobbyPage> {
  final ChessController controller = Get.put(ChessController());
  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();

    return Scaffold(
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '棋牌游戏',
                style: TextStyle(color: Colors.white, fontSize: 28.sp),
              ),
            ],
          ),
        ),
        body: Obx(() => GridView.builder(
              physics: const BouncingScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 20.h),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                crossAxisSpacing: 10.w,
                mainAxisSpacing: 10.w,
                childAspectRatio: 0.95.w,
              ),
              itemCount: controller.gameLobbys.length,
              itemBuilder: (context, index) {
                final item = controller.gameLobbys[index];
                return GestureDetector(
                  onTap: () => gotoGame(item),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AppImage.network(
                        '${item.pic}',
                        width: 80.w,
                        height: 80.w,
                      ),
                      Gap(10.w),
                      Text(
                        '${item.title}',
                        style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            height: 1),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                );
              },
            )));
  }

  void gotoGame(Game game) {
    List<String> notSuportTrial = [
      '78',
      '70',
      '71',
      '294',
      '94',
      '199',
      '41',
      '722',
      '101',
      '66',
      '112',
      '772',
      '769',
      '89',
      '148',
      '320',
      '795',
      '198',
      '771',
      '112',
      '778',
      '102',
      '133',
      '773',
      '301',
      '311',
      '277',
      '276',
      '88',
      '405',
      '721',
      '132',
      '497',
      '136',
      '828',
      '59'
    ];
    if (GlobalService.to.userInfo.value?.isTest == true &&
        notSuportTrial.contains(game.id)) {
      AppCommonDialog(
          title: '该游戏暂未开启试玩',
          msg: '请先登录',
          okLabel: '马上登录',
          onOk: () => AppNavigator.toNamed(loginPath));
      return;
    }
    if (game.isPopup == 1) {
      AppNavigator.toNamed(realGameTypesPath, arguments: [game.id, game.title]);
    } else {
      controller.gotoGame(game.id ?? "", "-1").then((data) async {
        if (data.code == 1) {
          AppToast.showError(msg: data.msg);
        } else {
          String url = data.data;
          url = url.replaceFirst("http://", "https://");
          AppNavigator.toNamed(webAppPage, arguments: [url, game.title]);
        }
      });
    }
  }
}
