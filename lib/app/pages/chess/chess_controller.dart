import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/data/models/GameCategory.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:fpg_flutter/data/repositories/app_real_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

class ChessController extends GetxController {
  final AuthController _authController = Get.find();
  final AppGameRepository _appGameRepository = AppGameRepository();
  final AppRealRepository _appRealRepository = AppRealRepository();

  RxList<Game> gameLobbys = RxList();

  @override
  void onReady() {
    // TODO: implement onReady
    _appGameRepository.homeRecommend().then((data) {
      if (data == null) return;
      gameLobbys.value = data
              .firstWhere((v) => v.category == 'card',
                  orElse: () => GameCategory())
              .games ??
          [];
    });
    super.onReady();
  }

  Future<dynamic> gotoGame(String id, String game) async {
    String token = GlobalService.to.userInfo.value?.token ?? "";
    dynamic data =
        await _appRealRepository.gotoGame(id: id, game: game, token: token);
    return data;
  }
}
