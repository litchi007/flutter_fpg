import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';

class ChessCardView extends StatefulWidget {
  const ChessCardView({super.key});

  @override
  State<StatefulWidget> createState() => _ChessCardViewState();
}

class _ChessCardViewState extends State<ChessCardView> {
  final List<GridItem> items = [
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '天游棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: 'LEG乐游棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '幸运棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '美博棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: 'KX棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '欢乐棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: 'VG棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: 'DB棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '新世界棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '博乐棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '双赢棋牌',
        subtitle: '立即游戏'),
    GridItem(
        imageUrl:
            'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz/upload/t002/photo/1119Z5X3W82CW04WSS000GK.png',
        title: '百胜棋牌',
        subtitle: '立即游戏'),
  ];

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Container(
              padding: EdgeInsets.only(left: 10.w),
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.ffffffff, size: 18.w),
                Text('返回',
                    style: AppTextStyles.ffffffff(context, fontSize: 18)),
              ]),
            )),
        titleWidget: const Center(
            child: Text('棋牌游戏', style: TextStyle(color: Colors.white))),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.builder(
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, // Number of columns
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10.0,
                childAspectRatio: 3, // Adjust the ratio to match your design
              ),
              itemCount: items.length,
              itemBuilder: (context, index) {
                return GridItemWidget(item: items[index]);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class GridItem {
  final String imageUrl;
  final String title;
  final String subtitle;

  GridItem(
      {required this.imageUrl, required this.title, required this.subtitle});
}

class GridItemWidget extends StatelessWidget {
  final GridItem item;

  const GridItemWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: const EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(color: Colors.grey),
      ),
      child: Row(
        children: [
          SizedBox(width: 10.w),
          AppImage.network(
            item.imageUrl,
            width: 50,
            height: 50,
          ),
          const SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.title,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                item.subtitle,
                style: const TextStyle(
                  fontSize: 14,
                  color: Colors.red,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
