import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/component/live_lottery/live_lottery_widget.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/community_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lh_gallery_controller.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_notice_widget.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/LhcdocCategoryModel.dart';
import 'package:fpg_flutter/page/homeMenu/homeMenu.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class CommunityPage extends StatefulWidget {
  const CommunityPage({super.key});

  @override
  State<StatefulWidget> createState() => _CommunityPageState();
}

class _CommunityPageState extends State<CommunityPage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  final GlobalService globalService = Get.find();
  final CommunityController controller = Get.put(CommunityController());
  final HomeViewController homeViewController = Get.find();
  final LhGalleryController galleryController = Get.put(LhGalleryController());
  final GameCenterController gameCenterController = Get.find();
  final AuthController authController = Get.find();

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      key: _homeKey,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        titleWidget: Row(
          children: [
            Obx(
              () => AppImage.network(
                controller.appLogo.value,
                height: 50.w,
                width: 250.w,
                fit: BoxFit.fill,
              ),
            ),
            Expanded(
              child: Center(
                child: Text(
                  '六合社区',
                  style: TextStyle(color: Colors.white, fontSize: 20.sp),
                ),
              ),
            ),
          ],
        ),
        actionWidgets: [
          GlobalService.to.isAuthenticated.value
              ? _showMenu()
              : const SizedBox()
        ],
      ),
      // endDrawer: GlobalService.to.isAuthenticated.value
      //     ? Container(
      //         color: Colors.white,
      //         width: Get.width * 0.5,
      //         height: Get.height,
      //         child: const HomeMenuWidget(),
      //       )
      //     : null,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Obx(
              () => CarouselSlider(
                options: CarouselOptions(
                  height: 300.w,
                  viewportFraction: 1.0,
                  enlargeCenterPage: false,
                  autoPlay: true,
                ),
                items: controller.bannerPics.map((item) {
                  return Center(
                      child: AppImage.network(
                    item.pic ?? '',
                    fit: BoxFit.cover,
                    height: 300.w,
                  ));
                }).toList(),
              ),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 10.w),
              color: Colors.white,
              child: Obx(
                () => AppNoticeWidget(
                  titleList: homeViewController.homeNoticeData.value.scroll,
                  onPressed: () =>
                      controller.showNoticeDialog(), //controller.notice,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10.w),
              color: Colors.white,
              child: const LiveLotteryWidget(),
            ),
            SizedBox(
              height: 10.w,
            ),
            Obx(
              () => GridView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, // Number of columns
                  crossAxisSpacing: 0.0,
                  mainAxisSpacing: 0.0,
                  childAspectRatio: 2.7,
                  // mainAxisExtent: 110.w,
                  // childAspectRatio: 3, // Adjust the ratio to match your design
                ),
                itemCount: controller.categoryList.length,
                itemBuilder: (context, index) {
                  final item = controller.categoryList[index];
                  return Container(
                    decoration: BoxDecoration(
                      // borderRadius: BorderRadius.circular(8.0),
                      // border: Border.all(color: Colors.grey),
                      border: Border.all(color: AppColors.ffDDDDDD, width: 0.5),
                      // Border(
                      //     bottom: BorderSide(color: Colors.grey, width: 0.5),
                      //     right: BorderSide(color: Colors.grey))
                    ),
                    child: GestureDetector(
                        onTap: () => onPressedItem(item),
                        child: Padding(
                          padding: EdgeInsets.only(left: 20.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              AppImage.network(
                                '${item.icon}',
                                width: 50,
                                height: 50,
                              ),
                              SizedBox(width: 10.w),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${item.name}',
                                    style: TextStyle(
                                        color: appThemeColors?.primary,
                                        fontSize: 24.sp,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  if ((item.desc ?? '').isNotEmpty)
                                    Text(
                                      '${item.desc}',
                                      style: TextStyle(
                                        color: appThemeColors?.primary,
                                        fontSize: 18.sp,
                                      ),
                                    )
                                ],
                              ),
                            ],
                          ),
                        )),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void onPressedItem(LhcdocCategoryModel item) {
    LogUtil.i('${item.toJson()}');
    switch (item.name) {
      case '幽默猜测':
        item.id = '6';
        break;
      case '跑马团玄机':
        item.id = '7';
        break;
      case '一句真言':
        item.id = '8';
        break;
      case '澳门挂牌':
        item.id = '2580';
        break;
      case '澳门四不像':
        item.id = '2579';
        break;
      case '澳门签牌':
        item.id = '2582';
        break;
      case '澳门跑狗图':
        item.id = '2578';
        break;
      case '秘典玄机':
        item.id = '2583';
        break;
      case '澳门传真':
        item.id = '2581';
        break;
      case '网红流照':
        item.id = '944';
        break;
      case '澳门高手榜':
        item.id = '2892';
        break;
      case '118内幕图':
        item.id = '2893';
        break;
      case '澳门AI资料':
        item.id = '2894';
        break;
      case '澳门心水推荐':
        item.id = '3603';
        break;
      case '香港心水推荐':
        item.id = '3593';
        break;
      case '香港挂牌':
        item.id = '3597';
        break;
      case '主持爆内幕':
        item.id = '3601';
        break;
    }

    if (item.readPri == '1' &&
        (GlobalService.isTest || !GlobalService.to.isAuthenticated.value)) {
      AppNavigator.toNamed(loginPath);
      return;
    }

    RegExp reg = RegExp('^(http|https)://');
    if (item.appLink!.isNotEmpty) {
      if (reg.hasMatch(item.appLink ?? "")) {
        String url = item.appLink ?? "";
        // AppNavigator.toNamed(webAppPage, arguments: [url, item.name??""]);
        launchUrl(Uri.parse(url));
        return;
      }
    }

    if (item.alias == 'yellowCale') {
      // 老黄历
      AppNavigator.toNamed(oldYearPagePath);
      return;
    }

    if (item.alias == 'zxkf') {
      // 在线客服
      AppNavigator.toNamed(onlineServicePath);
      return;
    }

    if (item.alias == 'appdl') {
      // APP下载
      globalService.downloadApp();
      return;
    }

    if (item.alias == 'lxb') {
      // 利息宝
      if (!GlobalService.to.isAuthenticated.value) {
        AppNavigator.toNamed(loginPath);
      } else {
        AppNavigator.toNamed(alipayprofitPath);
      }
      return;
    }

    if (item.alias == 'clzs' || item.name == '长龙助手') {
      AppNavigator.toNamed(AppRoutes.longDragonTool);
      return;
    }

    if (item.name == '查询助手') {
      AppNavigator.toNamed(lhLittleHelperPath);
      return;
    }

    if (item.name == '官方直播' || item.name == '现场开奖' || item.name == '49.cc') {
      if (reg.hasMatch(item.link ?? "")) {
        String url = item.link ?? "";
        // AppNavigator.toNamed(webAppPage, arguments: [url, item.name??""]);
        launchUrl(Uri.parse(url));
        return;
      }
    }

    if (item.link!.contains('Open_prize')) {
      String url = "${AppDefine.host}${item.link ?? ""}";
      AppNavigator.toNamed(lotteryWebsitePath);
    }

    if (item.link!.contains('/forum') ||
        item.name == '港澳高手贴吧' ||
        item.alias == 'forum' ||
        item.alias == 'gourmet') {
      //arguments: title, alias, type, category
      AppNavigator.toNamed(postListPath,
          arguments: [item.name, item.alias, '', 'forum']);
      return;
    }

    if (item.name == '综合精准资料') {
      // AppNavigator.toNamed(ForumListPage) name: listItem.name,
      // category: 'forum',
    }

    if (item.link!.contains('/sixpic') || item.name!.contains('图库')) {
      AppNavigator.toNamed(lhGalleryPath,
          arguments: [item.name, item.alias, item.id]);
      return;
    }

    if (item.name == '美女社区' ||
        item.name == '澳门规律' ||
        item.name == '香港规律' ||
        item.name == '独家解谜语' ||
        item.name == '香港五点来料' ||
        item.link!.contains('/mystery') ||
        item.alias == 'mystery' || // 香港规律
        item.alias == 'rule') {
      // 澳门规律
      //arguments: title, alias, type, category
      AppNavigator.toNamed(postListPath,
          arguments: [item.name, item.alias, 'mystery', '']);
      return;
    }

    if (item.name == '网红流照' ||
        item.name == '性感网红流照' ||
        item.name == '幽默猜测' ||
        item.name == '一句真言' ||
        item.name == '网红流照' ||
        item.name == '跑马团玄机') {
      //arguments: type2, title, category, cid, extra, from
      AppNavigator.toNamed(lhkPostDetail, arguments: [
        "",
        item.name ?? "",
        item.alias ?? "",
        item.id,
        'humorGuess',
        'gallery'
      ]);
      return;
    }

    if (item.name == '性感网红流照' ||
        item.alias == 'lQWcjMlA' ||
        item.alias == 'Qar5hNHG' ||
        item.alias == 'ZsD81C53' ||
        item.name == '澳门四不像' ||
        item.name == '澳门签牌' ||
        item.name == '澳门跑狗图' ||
        item.name == '秘典玄机' ||
        item.name == '澳门传真' ||
        item.name == '澳门高手榜' ||
        item.name == '118内幕图' ||
        item.name == '澳门AI资料' ||
        item.name == '澳门心水推荐' ||
        item.name == '香港心水推荐' ||
        item.name == '香港挂牌' ||
        item.name == '主持爆内幕' ||
        item.name == '123玄机图' ||
        item.name == '幽默视频猜测' ||
        item.name == '铁板神算' ||
        item.name == '四不像' ||
        item.name == '澳门挂牌') {
      //arguments: type2, title, category, cid, extra, from
      AppNavigator.toNamed(lhkPostDetail, arguments: [
        "",
        item.name ?? "",
        item.alias ?? "",
        "",
        'humorGuess',
        'humorGuess'
      ]);
      return;
    }

    if (item.alias == 'rwzx') {
      if (!GlobalService.to.isAuthenticated.value) {
        AppNavigator.toNamed(loginPath);
      } else {
        AppNavigator.toNamed(taskPath, arguments: [3]);
      }
      return;
    }
  }

  Widget _showMenu() {
    return GestureDetector(
      onTap: () {
        Get.to(() => HomeMenuPage(),
            opaque: false,
            fullscreenDialog: true,
            transition: Transition.noTransition);
        // _homeKey.currentState?.openEndDrawer();
      },
      child: Container(
        padding: EdgeInsets.only(right: 10.w),
        child: AppImage.asset('menu_btn_white.png', width: 30.w, height: 30.w),
      ),
    );
  }
}
