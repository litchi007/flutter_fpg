import 'package:fpg_flutter/data/models/LhcdocContent.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:get/get.dart';

class PostListController extends GetxController {
  String alias = "",
      sort = "",
      model = "",
      type = "",
      category = "",
      pageName = "";
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  Rx<LhcdocContent> lhcdocContent = LhcdocContent().obs;
  RxInt curPage = 1.obs;
  int perPage = 1000;
  RxInt catIndex1 = 0.obs;
  RxInt catIndex2 = 0.obs;
  RxInt sortIndex = 0.obs;
  RxBool isLoading = false.obs;

  List<String> categories1 = ['全部', '澳彩', '港彩', '台彩', '新彩'];
  List<String> categories2 = ['全部主题'];
  List<String> sorts = ['综合', '精华', '点赞数', '最新', '规则'];
  List<String> sortsForum = ['综合', '精华帖', '最新'];
  List<String> lotteryPosAliasMapping = [
    'forum',
    'bBQkjJ0F',
    'Q874XU7y',
    'Az8k34sI',
    '4B3333Ve'
  ];

  @override
  void onReady() {}

  Future<void> getContentList() async {
    _setSort();
    if (category == "forum") {
      alias = lotteryPosAliasMapping[catIndex1.value];
    }
    if (alias != "") {
      isLoading.value = true;
      await _appLHCDocRepository
          .getLhcContentList(
        alias: alias,
        page: curPage.value,
        model: pageName == '高手论坛' ? 'forum' : null,
        // sort: sort,
        rows: perPage,
      )
          .then((data) {
        if (data != null) {
          lhcdocContent.value = data;
        }
        isLoading.value = false;
      });
    }
  }

  void _setSort() {
    sort = "";
    if (sortIndex.value == 1) {
      sort = "hot";
    } else if (sortIndex.value == 2) {
      sort = "like";
    } else if (sortIndex.value == 3) {
      sort = "new";
    }
  }
}
