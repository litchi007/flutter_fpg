import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class LhcdocHistoryContentPage extends StatefulWidget {
  const LhcdocHistoryContentPage({super.key});

  @override
  State<LhcdocHistoryContentPage> createState() =>
      _LhcdocHistoryContentPageState();
}

class _LhcdocHistoryContentPageState extends State<LhcdocHistoryContentPage> {
  String uid = "";
  UserProfileController controller = Get.find();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    List<String>? args = Get.arguments;
    uid = args?[0] ?? "";
    if (uid != "") {
      controller.getHistoryContent(uid);
    }
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.homeBackgroundColor,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '历史帖子',
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          // actionWidgets: [GlobalService.to.isAuthenticated.value? _showMenu(): Container()],
        ),
        body: Obx(() => controller.isLoading2.value
            ? const Center(child: CircularProgressIndicator())
            : (controller.lhcdocHistoryContent.value.list ?? []).isEmpty
                ? Center(child: AppImage.asset('pl.png'))
                : ListView.builder(
                    itemCount:
                        (controller.lhcdocHistoryContent.value.list ?? [])
                            .length,
                    itemBuilder: (BuildContext context, int index) {
                      LhcdocContentList item =
                          controller.lhcdocHistoryContent.value.list![index];
                      return _getHistoryContentItem(item);
                    },
                  )));
  }

  Widget _getHistoryContentItem(LhcdocContentList item) {
    return GestureDetector(
        onTap: () {
          controller.lhcdocContentList.value = item;
          AppNavigator.toNamed(userContentDetailPath, arguments: [item]);
          // AppNavigator.toNamed(userContentDetailPath);
        },
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.w),
                border: Border.all(color: Colors.grey, width: 1.w),
                color: Colors.white),
            child: Column(
              children: [
                Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
                    child: Row(
                      children: [
                        AppImage.network(item.headImg ?? "", width: 50.w),
                        Gap(10.w),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('${item.nickname}',
                                  style: TextStyle(
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.bold)),
                              Text('${item.createTime}',
                                  style: TextStyle(
                                      fontSize: 18.sp, color: Colors.grey))
                            ])
                      ],
                    )),
                Gap(20.h),
                Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 5.w),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3.w),
                                color: AppColors.ff1976D2),
                            child: Text(item.periods ?? "",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18.sp))),
                        Gap(10.w),
                        Text('${item.title}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 28.sp)),
                      ],
                    )),
                HtmlWidget("${item.content}"),
                if (!(item.fullContent ?? "").contains('<img ') &&
                    (item.contentPic ?? []).isNotEmpty)
                  Wrap(
                    alignment: WrapAlignment.center,
                    children: [
                      ...List.generate((item.contentPic ?? []).length, (idx) {
                        return AppImage.network(item.contentPic?[idx] ?? "",
                            width: 100.w, height: 100.w);
                      })
                    ],
                  ),
                Gap(20.h),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Container(
                            height: 70.h,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey, width: 1.w),
                                    right: BorderSide(
                                        color: Colors.grey, width: 1.w))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  CustomIcons.thumbs_up,
                                  color: item.isLike == "1"
                                      ? Colors.red
                                      : AppColors.ff666666,
                                  weight: 20.w,
                                ),
                                Gap(5.w),
                                Text('${item.likeNum}',
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 18.sp))
                              ],
                            ))),
                    Expanded(
                        flex: 1,
                        child: Container(
                            height: 70.h,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey, width: 1.w),
                                    right: BorderSide(
                                        color: Colors.grey, width: 1.w))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  CustomIcons.eye,
                                  color: AppColors.ff666666,
                                  weight: 20.w,
                                ),
                                Gap(5.w),
                                Text('${item.viewNum}',
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 18.sp))
                              ],
                            ))),
                    Expanded(
                        flex: 1,
                        child: Container(
                            height: 70.h,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey, width: 1.w))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  CustomIcons.comment_dots,
                                  color: AppColors.ff666666,
                                  weight: 20.w,
                                ),
                                Gap(5.w),
                                Text('${item.replyCount}',
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 18.sp))
                              ],
                            )))
                  ],
                )
              ],
            )));
  }
}
