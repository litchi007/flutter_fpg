import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lh_gallery_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/TkListItemModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';

class LhGalleryPage extends StatefulWidget {
  const LhGalleryPage({super.key});

  @override
  State<LhGalleryPage> createState() => _LhGalleryPageState();
}

class _LhGalleryPageState extends State<LhGalleryPage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  final AuthController authController = Get.find();
  final LhGalleryController controller = Get.put(LhGalleryController());
  final GameCenterController gameCenterController = Get.find();
  final ScrollController _scrollController = ScrollController();
  late ListObserverController observerController;
  final TextEditingController _searchController = TextEditingController();
  String pageName = "";
  RxInt scrollIndex = 0.obs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    observerController = ListObserverController(controller: _scrollController);
    List<String?> args = ((Get.arguments as List<dynamic>?) ?? [])
        .map((e) => e as String?)
        .toList();
    pageName = args[0] ?? "";
    controller.alias = args[1] ?? "";
    controller.cid = args[2] ?? "";
    controller.getTkList();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        key: _homeKey,
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                pageName,
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          actionWidgets: [
            GlobalService.to.isAuthenticated.value ? _showMenu() : Container()
          ],
        ),
        endDrawer: GlobalService.to.isAuthenticated.value
            ? Container(
                color: Colors.white,
                width: Get.width * 0.5,
                height: Get.height,
                child: const HomeMenuWidget(),
              )
            : null,
        body: Obx(() => Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: Colors.grey.withOpacity(0.8),
                              width: 1.w))),
                  padding: EdgeInsets.only(right: 8.w),
                  height: 50.h,
                  child: Row(
                    children: [
                      DropdownButtonHideUnderline(
                        child: DropdownButton2<String>(
                          isExpanded: true,
                          items: controller.colorTypes
                              .map((String item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Text(item,
                                        style: AppTextStyles.ff000000(context,
                                            fontSize: 17),
                                        overflow: TextOverflow.ellipsis),
                                  ))
                              .toList(),
                          value: controller.colorType.value,
                          onChanged: (String? value) {
                            if (value != controller.colorType.value) {
                              setState(() {
                                controller.colorType.value = value ?? "彩色";
                                scrollIndex.value = 0;
                                observerController.jumpTo(index: 0);
                              });
                              controller.getTkList();
                            }
                          },
                          buttonStyleData: ButtonStyleData(
                            height: 50.h,
                            width: 80.w,
                            padding: EdgeInsets.only(left: 10.w, right: 10.w),
                            elevation: 2,
                          ),
                          iconStyleData: const IconStyleData(
                              icon: Icon(CustomIcons.chevron_down),
                              iconSize: 14,
                              iconEnabledColor: Colors.grey,
                              openMenuIcon: Icon(CustomIcons.chevron_up)),
                          dropdownStyleData: DropdownStyleData(
                            maxHeight: 200,
                            width: 80.w,
                            decoration: BoxDecoration(
                                border:
                                    Border.all(width: 2, color: Colors.grey),
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10.w),
                                    bottomRight: Radius.circular(10.w))),
                            // offset: const Offset(-20, 0),
                          ),
                          menuItemStyleData: MenuItemStyleData(
                            height: 35.h,
                            padding: EdgeInsets.only(left: 10.w, right: 10.w),
                            selectedMenuItemBuilder: (context, child) {
                              return Container(
                                  color: AppColors.dialog, child: child);
                            },
                          ),
                        ),
                      ),
                      Expanded(
                          child: TextField(
                        controller: _searchController,
                        style: AppTextStyles.ff000000(context, fontSize: 17),
                        maxLines: 1,
                        decoration: InputDecoration(
                            hintText: '请输入图库名称',
                            hintStyle: AppTextStyles.ff969696(fontSize: 18),
                            filled: true,
                            fillColor: AppColors.ffeeeeee,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5.h, horizontal: 0.w),
                            border: InputBorder.none,
                            prefixIcon: const Align(
                              widthFactor: 1.0,
                              heightFactor: 1.0,
                              child: Icon(
                                Icons.search,
                                color: Colors.grey,
                              ),
                            )),
                      )),
                      ElevatedButton(
                          onPressed: () {
                            controller.filterResult(_searchController.text);
                            scrollIndex.value = 0;
                            setState(() {
                              observerController.jumpTo(index: 0);
                            });
                          },
                          style: AppButtonStyles.elevatedStyle(
                              backgroundColor: AppColors.drawMenu,
                              foregroundColor: Colors.white,
                              width: 80,
                              height: 30,
                              padding: EdgeInsets.all(5.w),
                              fontSize: 17),
                          child: const Text("搜索"))
                    ],
                  ),
                ),
                SizedBox(
                    height: Get.height - 150.h,
                    child: Stack(
                      alignment: Alignment.centerRight,
                      children: [
                        if (controller.isLoading.value)
                          const CircularProgressIndicator(),
                        if (!controller.isLoading.value &&
                            controller.chineseCharactersByPinyin.isEmpty)
                          Center(child: AppImage.asset("pl.png")),
                        if (!controller.isLoading.value &&
                            controller.chineseCharactersByPinyin.isNotEmpty)
                          ListViewObserver(
                            controller: observerController,
                            onObserve: (resultModel) {
                              scrollIndex.value =
                                  resultModel.firstChild?.index ?? 0;
                            },
                            child: ListView.builder(
                              controller: _scrollController,
                              itemCount:
                                  controller.chineseCharactersByPinyin.length,
                              itemBuilder: (BuildContext context, int index) {
                                String initial = controller
                                    .chineseCharactersByPinyin.keys
                                    .elementAt(index);
                                List<TkListItemModel> listItem = controller
                                    .chineseCharactersByPinyin[initial]!;
                                return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text('    $initial',
                                          style: TextStyle(
                                              color: AppColors.drawMenu,
                                              fontSize: 22.sp,
                                              fontWeight: FontWeight.bold)),
                                      ...List.generate(listItem.length, (idx) {
                                        TkListItemModel item = listItem[idx];
                                        return GestureDetector(
                                            onTap: () async {
                                              AppNavigator.toNamed(
                                                  lhkPostDetail,
                                                  arguments: [
                                                    item.id ?? "",
                                                    item.name ?? "",
                                                    "",
                                                    "",
                                                    "",
                                                    "gallery"
                                                  ]);
                                            },
                                            child: Container(
                                                padding: EdgeInsets.only(
                                                    left: 15.w,
                                                    right: 30.w,
                                                    top: 15.h,
                                                    bottom: 15.h),
                                                decoration: BoxDecoration(
                                                    border: Border(
                                                        bottom: BorderSide(
                                                            color: Colors.grey,
                                                            width: 1.w))),
                                                child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(item.name ?? "",
                                                          style: TextStyle(
                                                              fontSize: 18.sp)),
                                                      Icon(
                                                          Icons
                                                              .arrow_forward_ios_rounded,
                                                          color: Colors.grey,
                                                          size: 20.w)
                                                    ])));
                                      })
                                    ]);
                              },
                            ),
                          ),
                        if (!controller.isLoading.value &&
                            controller.chineseCharactersByPinyin.isNotEmpty)
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Icon(
                                Icons.search_rounded,
                                color: AppColors.ff666666,
                              ),
                              ...List.generate(
                                  controller.chineseCharactersByPinyin.length,
                                  (index) {
                                String initial = controller
                                    .chineseCharactersByPinyin.keys
                                    .elementAt(index);
                                return GestureDetector(
                                    onTap: () {
                                      scrollIndex.value = index;
                                      observerController.animateTo(
                                        index: index,
                                        duration:
                                            const Duration(milliseconds: 100),
                                        curve: Curves.ease,
                                      );
                                    },
                                    child: Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 1.h, horizontal: 8.w),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15.w),
                                            color: scrollIndex.value == index
                                                ? AppColors.drawMenu
                                                : Colors.transparent),
                                        child: Text(
                                          initial,
                                          style: TextStyle(
                                              color: scrollIndex.value == index
                                                  ? Colors.white
                                                  : AppColors.ff666666),
                                        )));
                              })
                            ],
                          )
                      ],
                    ))
              ],
            )));
  }

  Widget _showMenu() {
    return GestureDetector(
        onTap: () {
          _homeKey.currentState?.openEndDrawer();
        },
        child: Container(
          padding: EdgeInsets.only(right: 10.w),
          child:
              AppImage.asset('menu_btn_white.png', width: 30.w, height: 30.w),
        ));
  }
}
