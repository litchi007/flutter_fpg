import 'package:fpg_flutter/data/models/LhcdocCategoryModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/app/pages/comunity/community_controller.dart';
import 'package:fpg_flutter/data/models/LhcdocContent.dart';
import 'package:get/get.dart';

//postlistpage.ts
class PostlistViewController extends GetxController {
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  final CommunityController communityController = Get.find();
  RxString type = 'mystery'.obs; //format

  RxList<LhcdocCategoryModel> categoryList = RxList();
  Rx<LhcdocCategoryModel> callItem =
      LhcdocCategoryModel().obs; //<=>route in the rn(format)
  RxBool isLHTKSite = true.obs;
  RxBool isLogin = false.obs;
  RxInt curPage = 1.obs;
  int perPage = 25;
  Rx<LhcdocContent> lhcdocContent = LhcdocContent().obs;
  RxBool loading = false.obs;

  @override
  void onReady() {
    getCallData();
  }

  void getCallData() {
    for (var item in communityController.categoryList) {
      if (item.id == communityController.callId.value) {
        callItem.value = item;
      }
    }
  }

  Future getContentList(String? alias, int page) async {
    curPage.value = page;
    loading.value = true;
    if (alias != null) {
      await _appLHCDocRepository
          .getLhcContentList(alias: alias, page: page, rows: perPage)
          .then((data) {
        if (data != null) {
          lhcdocContent.value = data;
        }
      });
    }
    loading.value = false;
  }
}
