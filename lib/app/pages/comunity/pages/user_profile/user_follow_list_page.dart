import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/FollowListModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_page.dart';

class UserFollowListPage extends StatefulWidget {
  const UserFollowListPage({super.key});

  @override
  State<UserFollowListPage> createState() => _UserFollowListPageState();
}

class _UserFollowListPageState extends State<UserFollowListPage> {
  UserProfileController controller = Get.find();
  final RxInt _tabIndex = 0.obs;

  @override
  void initState() {
    List<String>? args = Get.arguments;
    controller.uid = args?[0] ?? "";
    _tabIndex.value = int.parse(args?[1] ?? '0');
    controller.getFollowFavList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.homeBackgroundColor,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '我的关注',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        body: Obx(() => Column(
              children: [
                Container(
                    color: Colors.white,
                    padding:
                        EdgeInsets.symmetric(horizontal: 40.w, vertical: 10.h),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: ElevatedButton(
                                onPressed: () {
                                  if (_tabIndex.value == 0) return;
                                  _tabIndex.value = 0;
                                },
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10.w),
                                          bottomLeft: Radius.circular(10.w)),
                                    ),
                                    side: _tabIndex.value == 0
                                        ? BorderSide.none
                                        : BorderSide(
                                            color: Colors.black, width: 1.h),
                                    foregroundColor: _tabIndex.value == 0
                                        ? Colors.white
                                        : Colors.black,
                                    backgroundColor: _tabIndex.value == 0
                                        ? AppColors.drawMenu
                                        : Colors.white),
                                child: Text('关注专家',
                                    style: TextStyle(fontSize: 22.sp)))),
                        Expanded(
                            flex: 1,
                            child: ElevatedButton(
                                onPressed: () {
                                  if (_tabIndex.value == 1) return;
                                  _tabIndex.value = 1;
                                },
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10.w),
                                          bottomRight: Radius.circular(10.w)),
                                    ),
                                    side: _tabIndex.value == 1
                                        ? BorderSide.none
                                        : BorderSide(
                                            color: Colors.black, width: 1.h),
                                    foregroundColor: _tabIndex.value == 1
                                        ? Colors.white
                                        : Colors.black,
                                    backgroundColor: _tabIndex.value == 1
                                        ? AppColors.drawMenu
                                        : Colors.white),
                                child: Text('关注帖子',
                                    style: TextStyle(fontSize: 22.sp)))),
                      ],
                    )),
                Gap(10.h),
                Expanded(
                    child: controller.isLoading2.value == true
                        ? const Center(child: CircularProgressIndicator())
                        : _tabIndex.value == 0
                            ? ListView.separated(
                                itemBuilder: (BuildContext context, int index) {
                                  FollowListModel item =
                                      controller.followList.value.list![index];
                                  return Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 25.w),
                                      color: Colors.white,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              AppNavigator.toNamed(
                                                  userProfilePath,
                                                  parameters: {
                                                    'uid': item.posterUid ?? ""
                                                  },
                                                  preventDuplicates: false);
                                            },
                                            child: Row(
                                              children: [
                                                item.headImg != null &&
                                                        item.headImg != ""
                                                    ? AppImage.network(
                                                        item.headImg ?? "",
                                                        width: 50.w)
                                                    : AppImage.asset(
                                                        'money-2.png',
                                                        width: 50.w),
                                                Gap(10.w),
                                                item.nickname != null &&
                                                        item.nickname != ""
                                                    ? Text(item.nickname ?? "",
                                                        style: TextStyle(
                                                            color: AppColors
                                                                .ff666666,
                                                            fontSize: 18.sp))
                                                    : Text("昵称已被禁用",
                                                        style: TextStyle(
                                                            color: AppColors
                                                                .ff666666,
                                                            fontSize: 18.sp)),
                                              ],
                                            ),
                                          ),
                                          ElevatedButton(
                                              onPressed: () async {
                                                await controller.followPoster(
                                                    item.posterUid ?? "", 0);
                                                controller.getFollowList();
                                              },
                                              style:
                                                  AppButtonStyles.elevatedStyle(
                                                backgroundColor:
                                                    AppColors.drawMenu,
                                                foregroundColor: Colors.white,
                                                fontSize: 16,
                                                width: 110.w,
                                                height: 30,
                                                padding: EdgeInsets.zero,
                                              ),
                                              child: const Text('取消关注'))
                                        ],
                                      ));
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return Divider(
                                    height: 1.h,
                                  );
                                },
                                itemCount:
                                    (controller.followList.value.list ?? [])
                                        .length)
                            : Center(child: AppImage.asset('pl.png')))
              ],
            )));
  }
}
