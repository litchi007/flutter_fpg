import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_page.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/FollowListModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class UserFunsListPage extends StatefulWidget {
  const UserFunsListPage({super.key});

  @override
  State<UserFunsListPage> createState() => _UserFunsListPageState();
}

class _UserFunsListPageState extends State<UserFunsListPage> {
  UserProfileController controller = Get.find();
  final RxInt _tabIndex = 0.obs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    List<String>? args = Get.arguments;
    controller.uid = args?[0] ?? "";
    _tabIndex.value = int.parse(args?[1] ?? '0');
    // controller.getFollowFavList();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.homeBackgroundColor,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '我的关注',
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        body: Obx(() => Column(
              children: [
                Container(
                    color: Colors.white,
                    padding:
                        EdgeInsets.symmetric(horizontal: 40.w, vertical: 10.h),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: ElevatedButton(
                                onPressed: () {
                                  if (_tabIndex.value == 0) return;
                                  _tabIndex.value = 0;
                                },
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10.w),
                                          bottomLeft: Radius.circular(10.w)),
                                    ),
                                    side: _tabIndex.value == 0
                                        ? BorderSide.none
                                        : BorderSide(
                                            color: Colors.black, width: 1.h),
                                    foregroundColor: _tabIndex.value == 0
                                        ? Colors.white
                                        : Colors.black,
                                    backgroundColor: _tabIndex.value == 0
                                        ? AppColors.drawMenu
                                        : Colors.white),
                                child: Text('我的关注',
                                    style: TextStyle(fontSize: 22.sp)))),
                        Expanded(
                            flex: 1,
                            child: ElevatedButton(
                                onPressed: () {
                                  if (_tabIndex.value == 1) return;
                                  _tabIndex.value = 1;
                                },
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10.w),
                                          bottomRight: Radius.circular(10.w)),
                                    ),
                                    side: _tabIndex.value == 1
                                        ? BorderSide.none
                                        : BorderSide(
                                            color: Colors.black, width: 1.h),
                                    foregroundColor: _tabIndex.value == 1
                                        ? Colors.white
                                        : Colors.black,
                                    backgroundColor: _tabIndex.value == 1
                                        ? AppColors.drawMenu
                                        : Colors.white),
                                child: Text('帖子粉丝',
                                    style: TextStyle(fontSize: 22.sp)))),
                      ],
                    )),
                Gap(10.h),
                Expanded(
                    child: controller.isLoading2.value == true
                        ? const Center(child: CircularProgressIndicator())
                        : _tabIndex.value == 0
                            ? Center(child: AppImage.asset('pl.png'))
                            : Center(child: AppImage.asset('pl.png')))
              ],
            )));
  }
}
