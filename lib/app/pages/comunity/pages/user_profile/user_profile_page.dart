import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:mobkit_dashed_border/mobkit_dashed_border.dart';

class UserProfilePage extends StatefulWidget {
  const UserProfilePage({super.key});

  @override
  State<UserProfilePage> createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  final UserProfileController controller = Get.put(UserProfileController());
  String uid = "";

  @override
  void initState() {
    super.initState();
    Map<String, String?> params = Get.parameters;
    controller.uid = params['uid'] ?? "";
    uid = params['uid'] ?? "";
    controller.getUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.homeBackgroundColor,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '用户主页',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          // actionWidgets: [GlobalService.to.isAuthenticated.value? _showMenu(): Container()],
        ),
        body: Obx(() => SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 20.h),
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      alignment: Alignment.topCenter,
                      image: ExactAssetImage("assets/images/bc.png"),
                      fit: BoxFit.fitWidth),
                ),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 30.w, right: 15.w),
                          child: AppImage.network(
                              controller.userProfile[uid]?.value.face ?? "",
                              width: 100.w),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                                controller.userProfile[uid]?.value.nickname ??
                                    "",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold)),
                            Container(
                                padding: EdgeInsets.symmetric(horizontal: 3.w),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.w)),
                                    color: const Color(0xff55c6ff)),
                                child: Text(
                                    "等级：${controller.userProfile[uid]?.value.levelName ?? ""}",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.sp)))
                          ],
                        )
                      ],
                    ),
                    Gap(20.h),
                    Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 40.w, vertical: 30.h),
                        margin: EdgeInsets.symmetric(horizontal: 20.w),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.w)),
                            color: Colors.white),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    AppNavigator.toNamed(followListPath,
                                        arguments: [uid, '0']);
                                  },
                                  child: Text(
                                    "${controller.userProfile[uid]?.value.followNum ?? ""}\n关注专家",
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 17.sp),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    AppNavigator.toNamed(funsListPath,
                                        arguments: [uid, '0']);
                                  },
                                  child: Text(
                                    "${controller.userProfile[uid]?.value.fansNum ?? ""}\n关注粉丝",
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 17.sp),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    AppNavigator.toNamed(followListPath,
                                        arguments: [uid, '1']);
                                  },
                                  child: Text(
                                    "${controller.userProfile[uid]?.value.favContentNum ?? ""}\n关注帖子",
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 17.sp),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    AppNavigator.toNamed(
                                        lhcdocHistoryContentPath,
                                        arguments: [uid]);
                                  },
                                  child: Text(
                                    "${controller.userProfile[uid]?.value.contentNum ?? ""}\n历史帖子",
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 17.sp),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                                margin: EdgeInsets.symmetric(vertical: 20.h),
                                decoration: const BoxDecoration(
                                    border: DashedBorder.fromBorderSide(
                                        dashLength: 5,
                                        side: BorderSide(
                                            color: Colors.grey, width: 1))),
                                height: 1.h),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(children: [
                                  AppImage.asset('hz.png'),
                                  Gap(5.w),
                                  Text(
                                      '获赞数: ${controller.userProfile[uid]?.value.likeNum ?? ""}')
                                ]),
                                ElevatedButton(
                                    onPressed: () async {
                                      if (checkIsTest()) return;
                                      if (!GlobalService
                                          .to.isAuthenticated.value) {
                                        AppNavigator.toNamed(loginPath);
                                      }
                                      int followFlag = (controller
                                                      .userProfile[uid]
                                                      ?.value
                                                      .isFollow !=
                                                  null &&
                                              controller.userProfile[uid]?.value
                                                      .isFollow ==
                                                  1)
                                          ? 0
                                          : 1;
                                      bool result = await controller
                                          .followPoster(uid, followFlag);
                                      if (result) {
                                        controller.userProfile[uid]?.value
                                            .isFollow = followFlag;
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                        padding: EdgeInsets.zero,
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.zero,
                                        ),
                                        side: BorderSide(
                                            width: 2.w,
                                            color: const Color.fromARGB(
                                                255, 252, 169, 4)),
                                        foregroundColor: const Color.fromARGB(
                                            255, 252, 169, 4),
                                        backgroundColor: Colors.white),
                                    child: Text(controller.userProfile[uid]
                                                    ?.value.isFollow !=
                                                null &&
                                            controller.userProfile[uid]?.value
                                                    .isFollow ==
                                                1
                                        ? '+ 关注'
                                        : '取消关注'))
                              ],
                            )
                          ],
                        )),
                    Gap(10.h),
                    Container(
                        color: Colors.white,
                        padding: EdgeInsets.symmetric(
                            horizontal: 40.w, vertical: 10.h),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: ElevatedButton(
                                    onPressed: () {
                                      if (controller.alias.value == 'forum') {
                                        return;
                                      }
                                      controller.alias.value = 'forum';
                                      controller.getContentList();
                                    },
                                    style: ElevatedButton.styleFrom(
                                        padding: EdgeInsets.zero,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10.w),
                                              bottomLeft:
                                                  Radius.circular(10.w)),
                                        ),
                                        side: controller.alias.value == 'forum'
                                            ? BorderSide.none
                                            : BorderSide(
                                                color: Colors.black,
                                                width: 1.h),
                                        foregroundColor:
                                            controller.alias.value == 'forum'
                                                ? Colors.white
                                                : Colors.black,
                                        backgroundColor:
                                            controller.alias.value == 'forum'
                                                ? AppColors.drawMenu
                                                : Colors.white),
                                    child: Text('高手论坛',
                                        style: TextStyle(fontSize: 22.sp)))),
                            Expanded(
                                flex: 1,
                                child: ElevatedButton(
                                    onPressed: () {
                                      if (controller.alias.value == 'gourmet') {
                                        return;
                                      }
                                      controller.alias.value = 'gourmet';
                                      controller.getContentList();
                                    },
                                    style: ElevatedButton.styleFrom(
                                        padding: EdgeInsets.zero,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(10.w),
                                              bottomRight:
                                                  Radius.circular(10.w)),
                                        ),
                                        side:
                                            controller.alias.value == 'gourmet'
                                                ? BorderSide.none
                                                : BorderSide(
                                                    color: Colors.black,
                                                    width: 1.h),
                                        foregroundColor:
                                            controller.alias.value == 'gourmet'
                                                ? Colors.white
                                                : Colors.black,
                                        backgroundColor:
                                            controller.alias.value == 'gourmet'
                                                ? AppColors.drawMenu
                                                : Colors.white),
                                    child: Text('极品发帖',
                                        style: TextStyle(fontSize: 22.sp)))),
                          ],
                        )),
                    if ((controller.lhcdocContent[uid]?.value.list ?? [])
                        .isEmpty)
                      Center(child: AppImage.asset('pl.png')),
                    if (controller.isLoading.value)
                      const CircularProgressIndicator(),
                    if (controller.isLoading.value == false)
                      ...List.generate(
                          (controller.lhcdocContent[uid]?.value.list ?? [])
                              .length, (index) {
                        LhcdocContentList item =
                            controller.lhcdocContent[uid]?.value.list![index] ??
                                LhcdocContentList();
                        return getContentItem(item);
                      })
                  ],
                )))));
  }

  Widget getContentItem(LhcdocContentList item) {
    return GestureDetector(
        onTap: () {
          controller.lhcdocContentList.value = item;
          AppNavigator.toNamed(userContentDetailPath);
        },
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.w),
                border: Border.all(color: Colors.grey, width: 1.w),
                color: Colors.white),
            child: Column(
              children: [
                Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
                    child: Row(
                      children: [
                        AppImage.network(item.headImg ?? "", width: 50.w),
                        Gap(10.w),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('${item.nickname}',
                                  style: TextStyle(
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.bold)),
                              Text('${item.createTime}',
                                  style: TextStyle(
                                      fontSize: 18.sp, color: Colors.grey))
                            ])
                      ],
                    )),
                Gap(20.h),
                Text('${item.title}',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 28.sp)),
                HtmlWidget("${item.content}"),
                if (!(item.fullContent ?? "").contains('<img ') &&
                    (item.contentPic ?? []).isNotEmpty)
                  Wrap(
                    alignment: WrapAlignment.center,
                    children: [
                      ...List.generate((item.contentPic ?? []).length, (idx) {
                        return AppImage.network(item.contentPic?[idx] ?? "",
                            width: 100.w, height: 100.w);
                      })
                    ],
                  ),
                Gap(20.h),
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Container(
                            height: 70.h,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey, width: 1.w),
                                    right: BorderSide(
                                        color: Colors.grey, width: 1.w))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  CustomIcons.thumbs_up,
                                  color: item.isLike == "1"
                                      ? Colors.red
                                      : AppColors.ff666666,
                                  weight: 20.w,
                                ),
                                Gap(5.w),
                                Text('${item.likeNum}',
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 18.sp))
                              ],
                            ))),
                    Expanded(
                        flex: 1,
                        child: Container(
                            height: 70.h,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey, width: 1.w),
                                    right: BorderSide(
                                        color: Colors.grey, width: 1.w))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  CustomIcons.eye,
                                  color: AppColors.ff666666,
                                  weight: 20.w,
                                ),
                                Gap(5.w),
                                Text('${item.viewNum}',
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 18.sp))
                              ],
                            ))),
                    Expanded(
                        flex: 1,
                        child: Container(
                            height: 70.h,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey, width: 1.w))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  CustomIcons.comment_dots,
                                  color: AppColors.ff666666,
                                  weight: 20.w,
                                ),
                                Gap(5.w),
                                Text('${item.replyCount}',
                                    style: TextStyle(
                                        color: AppColors.ff666666,
                                        fontSize: 18.sp))
                              ],
                            )))
                  ],
                )
              ],
            )));
  }

  bool checkIsTest() {
    if (GlobalService.to.userInfo.value?.isTest == true) {
      AppToast.showToast('请先登录正式账号');
      return true;
    } else {
      return false;
    }
  }
}
