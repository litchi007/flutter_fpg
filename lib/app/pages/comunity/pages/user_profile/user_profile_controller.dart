import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/FollowModel.dart';
import 'package:fpg_flutter/data/models/LhcdocContent.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/data/models/UserProfileModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class UserProfileController extends GetxController {
  String uid = "";
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  Map<String, Rx<UserProfileModel>> userProfile = {};
  Map<String, Rx<LhcdocContent>> lhcdocContent = {};
  Rx<LhcdocContent> lhcdocHistoryContent = LhcdocContent().obs;
  RxList<LhcdocContentList> lhcdocFavContentList = RxList();
  RxBool isLoading = false.obs;
  RxBool isLoading2 = false.obs;

  RxString alias = 'forum'.obs;
  Rx<LhcdocContentList> lhcdocContentList = LhcdocContentList().obs;
  Rx<FollowModel> followList = FollowModel().obs;

  Future<void> getUserProfile() async {
    String token = AppDefine.userToken?.apiSid ?? "";
    UserProfileModel? data = await _appLHCDocRepository.getUserProfile(
        token: token, uid: uid); //.then((data) {
    userProfile[uid] = (data ?? UserProfileModel()).obs;
    await getContentList();
    //});
  }

  Future<void> getContentList() async {
    // lhcdocContent.value = LhcdocContent();
    isLoading.value = true;
    await _appLHCDocRepository
        .getLhcContentList(alias: alias.value, page: 1, uid: uid, rows: 1000)
        .then((data) {
      if (data != null) {
        lhcdocContent[uid] = data.obs;
      }
      isLoading.value = false;
    });
  }

  Future getHistoryContent(String uid) async {
    lhcdocHistoryContent.value = LhcdocContent();
    String token = AppDefine.userToken?.apiSid ?? "";
    isLoading2.value = true;
    await _appLHCDocRepository
        .getLhcHistoryContent(uid: uid, token: token)
        .then((data) {
      if (data != null) {
        lhcdocHistoryContent.value = data;
      }
      isLoading2.value = false;
    });
  }

  Future<void> getFollowFavList() async {
    await getFavContentList();
    await getFollowList();
  }

  Future<void> getFavContentList() async {
    lhcdocFavContentList.value = RxList();
    String token = AppDefine.userToken?.apiSid ?? "";
    isLoading2.value = true;

    await _appLHCDocRepository
        .getFavContentList(uid: uid, token: token)
        .then((data) {
      if (data != null) {
        lhcdocFavContentList.value = data;
      }
      isLoading2.value = false;
    });
  }

  Future<void> getFollowList() async {
    followList.value = FollowModel();
    String token = AppDefine.userToken?.apiSid ?? "";
    isLoading2.value = true;

    await _appLHCDocRepository
        .getFollowList(uid: uid, token: token)
        .then((data) {
      if (data != null) {
        followList.value = data;
      }
      isLoading2.value = false;
    });
  }

  Future<bool> doFavorites(String id, int favFlag) async {
    String token = AppDefine.userToken?.apiToken ?? "";
    dynamic data = await _appLHCDocRepository.doFavorites(
        id: id, favFlag: favFlag, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0);
  }

  Future<bool> likePost(String rid, int likeFlag) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.likePost(
        rid: rid, likeFlag: likeFlag, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0 || data.code == 400);
  }

  Future<bool> followPoster(String posterUid, int followFlag) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.followPoster(
        posterUid: posterUid, followFlag: followFlag, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0);
  }

  Future<bool> postContentReply(
      String cid, String rid, String toUid, String content) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.postContentReply(
        cid: cid, rid: rid, toUid: toUid, content: content, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0);
  }
}
