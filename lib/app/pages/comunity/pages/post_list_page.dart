import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/post_list_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/widget/lhcdoc_content_list_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class PostListPage extends StatefulWidget {
  const PostListPage({super.key});

  @override
  State<PostListPage> createState() => _PostListPageState();
}

class _PostListPageState extends State<PostListPage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  final PostListController controller = Get.put(PostListController());
  final AuthController authController = Get.find();
  final GameCenterController gameCenterController = Get.find();
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    List<String?> args = ((Get.arguments as List<dynamic>?) ?? [])
        .map((e) => e as String?)
        .toList();
    controller.pageName = args[0] ?? "";
    controller.alias = args[1] ?? "";
    controller.type = args[2] ?? "";
    controller.category = args[3] ?? "";
    controller.getContentList();
  }

  @override
  void dispose() {
    Get.delete<PostListController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      key: _homeKey,
      backgroundColor: appThemeColors?.surface,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        leading: GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Container(
            padding: EdgeInsets.only(left: 10.w),
            child: Row(children: [
              Icon(Icons.arrow_back_ios, color: AppColors.ffffffff, size: 18.w),
              Text('返回', style: AppTextStyles.ffffffff(context, fontSize: 18)),
            ]),
          ),
        ),
        titleWidget: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              controller.pageName,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        actionWidgets: [
          if (controller.category == 'forum')
            Row(children: [
              GestureDetector(
                onTap: () {
                  AppNavigator.toNamed(newPostPath, arguments: {
                    'alias': controller.alias,
                  });
                },
                child: AppImage.network(
                  '${img_static01('lhc/write')}',
                  width: 40.w,
                  height: 40.w,
                ),
              ),
              Gap(20.w),
              GestureDetector(
                onTap: () {},
                child: AppImage.network(
                  '${img_static01('lhc/search')}',
                  width: 40.w,
                  height: 40.w,
                ),
              ),
              Gap(20.w),
            ]),
          GlobalService.to.isAuthenticated.value
              ? GestureDetector(
                  onTap: () {
                    _homeKey.currentState?.openEndDrawer();
                  },
                  child: Container(
                    padding: EdgeInsets.only(right: 10.w),
                    child: AppImage.asset('menu_btn_white.png',
                        width: 30.w, height: 30.w),
                  ),
                )
              : const SizedBox()
        ],
      ),
      endDrawer: GlobalService.to.isAuthenticated.value
          ? Container(
              color: Colors.white,
              width: Get.width * 0.5,
              height: Get.height,
              child: const HomeMenuWidget(),
            )
          : null,
      body: Stack(
        children: [
          Obx(
            () => Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // Container(
                //   padding:
                //       EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.h),
                //   decoration: BoxDecoration(
                //       border: Border(
                //           bottom: BorderSide(color: Colors.grey, width: 1.h))),
                //   child: Row(
                //     children: [
                //       Text('广告',
                //           style: TextStyle(
                //               color: AppColors.drawMenu, fontSize: 20.sp)),
                //       Gap(10.w),
                //       Text('1777彩票1777.cc注册有惊喜！',
                //           style: TextStyle(fontSize: 20.sp))
                //     ],
                //   ),
                // ),
                // Container(
                //   padding:
                //       EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.h),
                //   decoration: BoxDecoration(
                //       border: Border(
                //           bottom: BorderSide(color: Colors.grey, width: 1.h))),
                //   child: Row(
                //     children: [
                //       Text('公告',
                //           style: TextStyle(
                //               color: AppColors.drawMenu, fontSize: 20.sp)),
                //       Gap(10.w),
                //       Text('49图库049TTK.com 有您更精彩～',
                //           style: TextStyle(fontSize: 20.sp))
                //     ],
                //   ),
                // ),
                // Center(
                //   child: Container(
                //     margin: EdgeInsets.only(
                //         left: 20.w, right: 20.w, top: 20.h, bottom: 0),
                //     padding: EdgeInsets.zero,
                //     decoration: BoxDecoration(
                //         border:
                //             Border.all(color: AppColors.drawMenu, width: 2.w)),
                //     child: IntrinsicWidth(
                //       child: Row(
                //         children: List.generate(controller.categories1.length,
                //             (index) {
                //           return _categoryButton1(index);
                //         }),
                //       ),
                //     ),
                //   ),
                // ),
                // Center(
                //     child: IntrinsicWidth(
                //         child: Row(
                //             children: List.generate(
                //                 controller.categories2.length, (index) {
                //   return _categoryButton2(index);
                // })))),
                // if (controller.alias != 'rule' && controller.alias != 'mystery')
                //   Container(
                //     padding:
                //         EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
                //     decoration: BoxDecoration(
                //         border: Border(
                //             top: BorderSide(color: Colors.grey, width: 2.w),
                //             bottom: BorderSide(color: Colors.grey, width: 2.w)),
                //         color: Colors.white),
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //       children: List.generate(
                //         controller.sorts.length,
                //         (index) {
                //           return _sortButton(index);
                //         },
                //       ),
                //     ),
                //   ),

                Obx(
                  () => Container(
                    margin: EdgeInsets.symmetric(vertical: 10.h),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(10.w),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        GestureDetector(
                          onTap: () {
                            controller.sortIndex.value = 0;
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 40.w,
                              vertical: 5.w,
                            ),
                            decoration: BoxDecoration(
                              color: controller.sortIndex.value == 0
                                  ? appThemeColors?.primary
                                  : null,
                              // border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.w),
                                bottomLeft: Radius.circular(10.w),
                              ),
                            ),
                            child: Text(
                              '综合',
                              style: TextStyle(
                                  color: controller.sortIndex.value == 0
                                      ? Colors.white
                                      : Colors.black),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            controller.sortIndex.value = 1;
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 40.w,
                              vertical: 5.w,
                            ),
                            decoration: BoxDecoration(
                              color: controller.sortIndex.value == 1
                                  ? appThemeColors?.primary
                                  : null,
                              border: controller.sortIndex.value == 1
                                  ? null
                                  : const Border(
                                      left: BorderSide(color: Colors.black),
                                      right: BorderSide(color: Colors.black),
                                    ),
                            ),
                            child: Text(
                              '精华帖',
                              style: TextStyle(
                                  color: controller.sortIndex.value == 1
                                      ? Colors.white
                                      : Colors.black),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            controller.sortIndex.value = 2;
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: 40.w,
                              vertical: 5.w,
                            ),
                            decoration: BoxDecoration(
                              color: controller.sortIndex.value == 2
                                  ? appThemeColors?.primary
                                  : null,
                              // border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10.w),
                                bottomRight: Radius.circular(10.w),
                              ),
                            ),
                            child: Text(
                              '最新',
                              style: TextStyle(
                                  color: controller.sortIndex.value == 2
                                      ? Colors.white
                                      : Colors.black),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: _getBody(),
                )
              ],
            ),
          ),
          Positioned(
            right: 10.w,
            top: Get.height * 0.6,
            child: GestureDetector(
              onTap: () {
                AppNavigator.toNamed(newPostPath, arguments: {
                  'alias': controller.alias,
                });
              },
              child: AppImage.network(
                img_images('lhc/add'),
                width: 80.w,
                height: 80.w,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getBody() {
    if (controller.alias == 'rule' || controller.alias == 'mystery') {
      return _getRuleBody();
    } else {
      return _getTabView();
    }
  }

  Widget _getRuleBody() {
    if (controller.isLoading.value) {
      return const Center(child: CircularProgressIndicator());
    } else {
      int itemCount = (controller.lhcdocContent.value.list ?? []).length;
      if (itemCount == 0) {
        return Center(child: AppImage.asset("pl.png"));
      } else {
        return ListView.builder(
          itemBuilder: (context, index) {
            LhcdocContentList item =
                controller.lhcdocContent.value.list![index];
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.grey, width: 1.h))),
              child: GestureDetector(
                onTap: () {
                  if (item.hyperlink != null && item.hyperlink != "") {
                    // AppNavigator.toNamed(webAppPage, arguments: [item.hyperlink, item.title]);
                    launchUrl(Uri.parse(item.hyperlink ?? ""));
                  } else {
                    AppNavigator.toNamed(postDetailPath, arguments: {
                      'postId': item.cid ?? "",
                      'title': item.title,
                      'category': controller.category,
                      'from': 'mystery'
                    });
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${item.periods}期:${item.title}",
                      style:
                          TextStyle(color: AppColors.ff252525, fontSize: 20.sp),
                      overflow: TextOverflow.ellipsis,
                    ),
                    Icon(Icons.arrow_forward_ios_rounded,
                        color: Colors.grey, size: 20.w)
                  ],
                ),
              ),
            );
          },
          itemCount: itemCount,
        );
      }
    }
  }

  Widget _getTabView() {
    if (controller.sortIndex.value == 4) {
      return _getRulesBlock();
    } else {
      return _getList();
    }
  }

  Widget _getList() {
    if (controller.isLoading.value) {
      return const Center(child: CircularProgressIndicator());
    } else {
      int itemCount = (controller.lhcdocContent.value.list ?? []).length;
      if (itemCount == 0) {
        return Center(child: AppImage.asset("pl.png"));
      } else {
        return ListView.builder(
            itemBuilder: (context, index) {
              LhcdocContentList item =
                  controller.lhcdocContent.value.list![index];
              return LhcdocContentListWidget(item: item);
            },
            itemCount: itemCount);
      }
    }
  }

  Widget _categoryButton1(int index) {
    String label = controller.categories1[index];
    return InkWell(
      onTap: () {
        controller.catIndex1.value = index;
        controller.getContentList();
      },
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 5.h),
          color: (index == controller.catIndex1.value)
              ? AppColors.ff4caf50
              : Colors.white,
          child: Text(label,
              style: TextStyle(
                  color: (index == controller.catIndex1.value)
                      ? AppColors.ffffffff
                      : AppColors.ff4caf50,
                  fontSize: 18.sp))),
    );
  }

  Widget _categoryButton2(int index) {
    String label = controller.categories2[index];
    return InkWell(
        onTap: () {
          controller.catIndex2.value = index;
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 0.w, vertical: 10.h),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        color: (index == controller.catIndex2.value)
                            ? AppColors.drawMenu
                            : Colors.white,
                        width: 3.h))),
            child: Text(label,
                style: TextStyle(
                    color: (index != controller.catIndex2.value)
                        ? AppColors.ffffffff
                        : AppColors.ff4caf50,
                    fontSize: 20.sp))));
  }

  Widget _sortButton(int index) {
    String label = controller.sorts[index];
    return InkWell(
      onTap: () {
        controller.sortIndex.value = index;
        if (index < 4) {
          controller.getContentList();
        }
      },
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 5.h),
          child: Text(label,
              style: TextStyle(
                  color: (index == controller.sortIndex.value)
                      ? AppColors.drawMenu
                      : AppColors.ff000000,
                  fontSize: 18.sp))),
    );
  }

  Widget _getRulesBlock() {
    return SingleChildScrollView(
        controller: _scrollController,
        child: Padding(
            padding: EdgeInsets.all(10.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Gap(10.h),
                // if((controller.lhcdocContent.value.list??[]).isEmpty)
                // Center(child: AppImage.asset("pl.png")),
                Center(
                    child: Text('49图库用户规则',
                        style: TextStyle(
                            fontSize: 22.sp, fontWeight: FontWeight.bold))),
                Gap(10.h),
                Text(
                    '①:禁止发布包含任何网址，任何联系方式，二维码,网站名称！资料请先马赛克处理后在进行分享发布！如有违规将永久禁言！',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('②:用户昵称禁止使用敏感词汇以及国家领导人名字，用户头像禁止使用二维码，色情图片,国家领导人,如有发现永久禁言！',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('③:为不影响用户正常查看心水贴,禁止发表消极内容，重复，空贴，辱骂，跟六合无关，马后炮等多次不听劝阻者永久禁言！',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('④:禁止索要联系方式,网投网址,外网网址,微信群图片，其他平台名称,一经发现即刻禁言！',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('⑤:禁止对用户发表的心水贴冷嘲热讽,辱骂攻击，恶语中伤！违者禁言处理！',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('⑥:高手们研究发帖不易，请大家理性参考，心存感恩查阅资料且文明交流对错不怪！因为没人能保证谁的一定准。',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('⑦:禁止高手主动要求粉丝给自己打赏，发现当卖料处理。轻者删贴，重者封号！',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text(
                    '⑧:发帖规则：每人每期只可以发布三个主题贴，达到上限后可以自行修改已发布的帖子，发帖账号必须是中文昵称三个以上汉字，不能带数字和字母，请联系在线客服修改昵称，望大家悉知，禁止注册多账号发帖套利{每位用户仅限注册一个账号}高手论坛发帖内容禁止空降料，必须实战记录，附带网料图片的不能带有网址广告。',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text(' 网络安全，从自身做起，勿轻信他人，别轻易去加广告狗子发的微信，QQ等任何联系方式。',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('遵守平台规则你得到的不仅仅是尊重, 49图库愿与各位携手并进共同成长！！',
                    style: TextStyle(fontSize: 18.sp)),
                Gap(5.h),
                Text('所有49图库会员放心投注！49图库为您保驾护航',
                    style: TextStyle(fontSize: 18.sp)),
              ],
            )));
  }
}
