import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EmojiWidget extends StatelessWidget {
  const EmojiWidget({
    super.key,
    required this.content,
  });
  final String content;
  static final RegExp emojiReg = RegExp(r'\[em_.*?\]');
  @override
  Widget build(BuildContext context) {
    final List<String> emojiArr =
        emojiReg.allMatches(content).map((m) => m.group(0) ?? '').toList();
    List<String> str = [];
    String textMsg = content.replaceAll('\\n', '');

    // final textStyle = TextStyle(
    //   fontSize: size ?? 14.0,
    //   height: lineHeight != null ? lineHeight! / (size ?? 14.0) : 1.5,
    //   color: color ?? Colors.black,
    //   fontWeight: weight ?? FontWeight.normal,
    // );

    if (emojiArr.isNotEmpty) {
      emojiArr.add('');
      for (var s in emojiArr) {
        final int length = s.length;
        final int strIdx = textMsg.indexOf(s);
        if (strIdx != 0) {
          str.add(textMsg.substring(0, strIdx));
        }
        if (s.isNotEmpty) {
          str.add(s);
        } else {
          str.add(textMsg);
        }
        textMsg = textMsg.substring(strIdx + length, textMsg.length);
      }

      return Wrap(
        direction: Axis.horizontal,
        children: str.map((s) {
          if (s.contains('[em_')) {
            s = s.replaceAll('[em_', '').replaceAll(']', '');
            final String uri =
                'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz/images/arclist/${int.parse(s)}.gif';
            return AppImage.network(
              uri,
              width: 20.w, height: 20.w,
              // width: lineHeight ?? 20.0,
              // height: lineHeight ?? 20.0,
              // errorWidget: (context, url, error) => Icon(Icons.error),
            );
          } else {
            return Text(
              s,
              // style: textStyle,
              // maxLines: one == true ? 1 : null,
              // overflow:
              // one == true ? TextOverflow.ellipsis : TextOverflow.visible,
            );
          }
        }).toList(),
      );
    } else {
      return Text(
        content,
        // style: textStyle,
        // maxLines: one == true ? 1 : null,
        // overflow: one == true ? TextOverflow.ellipsis : TextOverflow.visible,
      );
    }
  }

  // return HtmlWidget(parseEmojiMessage(content));
  // }
}
