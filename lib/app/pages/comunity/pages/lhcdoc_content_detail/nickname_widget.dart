import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';

class NicknameWidget extends StatelessWidget {
  const NicknameWidget({
    super.key,
    required this.onSuccess,
  });

  final ValueChanged<String> onSuccess;

  @override
  Widget build(BuildContext context) {
    final TextEditingController nickNameTextController =
        TextEditingController();
    return AlertDialog(
      title: Text(
        '论坛昵称',
        style: AppTextStyles.bodyText1(context, fontSize: 24),
        textAlign: TextAlign.center,
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextField(
            decoration: const InputDecoration(hintText: '请输入昵称:1-8个汉字'),
            autofocus: true,
            controller: nickNameTextController,
          ),
          Text('为了保护您的隐私，请绑定论坛昵称'),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            '取消',
            style: AppTextStyles.bodyText1(context, fontSize: 18),
          ),
        ),
        TextButton(
          onPressed: () async {
            if (nickNameTextController.text.isEmpty) {
              AppToast.showToast('请输入昵称');
              return;
            }
            onSuccess.call(nickNameTextController.text);
            Navigator.pop(context);
          },
          child: Text(
            '确定',
            style: AppTextStyles.bodyText1(context, fontSize: 18),
          ),
        ),
      ],
    );
  }
}
