import 'package:fpg_flutter/app/component/live_lottery/LotteryNum.dart';
import 'package:fpg_flutter/app/component/live_lottery/live_lottery_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lhcdoc_content_detail/nickname_widget.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/CommentModel.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/data/models/PostDetailModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';

class PostDetailController extends GetxController {
  String postId = '';
  RxString title = ''.obs;
  String from = "", category = "";
  String typeTwo = "";
  String extra = "";

  Rx<PostDetailModel> postDetail = PostDetailModel().obs;
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  LiveLotteryController _liveLotteryController = Get.find();
  RxList<CommentModel> comments = RxList();

  Rx<LotteryNumberData> lotteryNumber = LotteryNumberData().obs;
  RxList<LotteryNum> showlotterys = RxList();

  Future<void> initData() async {
    if (postId != '') {
      getContentDetail();
    }
    getContentReplyList();
    if (from == 'gallery') {
    } else if (from == 'humorGuess') {}
  }

  Future<void> getContentDetail() async {
    // contentDetail.value = LhcdocContentDetail();

    await _appLHCDocRepository.getLhcContentDetail(postId).then((data) {
      if (data != null) {
        postDetail.value = data;
        getLotteryData(postDetail.value.baomaId ?? "");
      }
    });
  }

  Future getContentReplyList() async {
    await _appLHCDocRepository.contentReplyList(postId: postId).then((data) {
      comments.value = data ?? [];
    });
  }

  Future getLotteryData(String gameId) async {
    await _appLHCDocRepository
        .lotteryNumber(gameId: int.parse(gameId))
        .then((data) {
      if (data != null) {
        lotteryNumber.value = data;

        try {
          List<String> lotteryNumbers = data.numbers?.split(',') ?? [];
          List<String> numColors = data.numColor?.split(',') ?? [];
          List<String> numSxs = data.numSx?.split(',') ?? [];
          List<LotteryNum> nums = lotteryNumbers.asMap().entries.map((entry) {
            int index = entry.key;
            String item = entry.value;
            return LotteryNum(
              number: item,
              color: numColors[index],
              sx: numSxs[index],
            );
          }).toList();
          showlotterys.value = nums;
        } catch (e) {
          AppLogger.e('requestLHN calculator error', error: e);
        }
      }
    });
  }

  Future<bool> followPoster(String posterUid, int followFlag) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.followPoster(
        posterUid: posterUid, followFlag: followFlag, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0);
  }

  Future<bool> doFavorites(String id, int favFlag) async {
    String token = AppDefine.userToken?.apiToken ?? "";
    dynamic data = await _appLHCDocRepository.doFavorites(
        id: id, favFlag: favFlag, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0);
  }

  Future<bool> likePost(String rid, int likeFlag, {int? type = 1}) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.likePost(
      rid: rid,
      likeFlag: likeFlag,
      token: token,
      type: type,
    );
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0 || data.code == 400);
  }

  Future<bool> postContentReply(
      String cid, String rid, String toUid, String content) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.postContentReply(
        cid: cid, toUid: toUid, content: content, token: token);
    // if (data.code == 1) {
    //   AppToast.showToast(data.msg);
    // }
    AppToast.showToast(data.msg);
    if (data.msg == '请先设置昵称后发帖') {
      Get.dialog(NicknameWidget(
        onSuccess: (nickName) async {
          dynamic data = await _appLHCDocRepository.setNickName(
            nickName: nickName,
          );
          // Get.back(closeOverlays: true);
        },
      ));
    }
    return (data.code == 0);
  }

  Future<dynamic> vote(String cid, int animalId) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.vote(
      cid: cid,
      animalId: animalId,
      token: token,
    );
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return data;
  }

  Future<dynamic> tipContent(String cid, String amount) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.tipContent(
      cid: cid,
      amount: amount,
      token: token,
    );

    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return data;
  }
}
