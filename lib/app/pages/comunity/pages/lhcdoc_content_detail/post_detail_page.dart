import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/component/live_lottery/lottery_ball.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lhcdoc_content_detail/emoji_widget.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lhcdoc_content_detail/post_detail_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/CommentModel.dart';
import 'package:fpg_flutter/data/models/PostDetailModel.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'dart:math';

class PostDetailView extends StatefulWidget {
  const PostDetailView({super.key});

  @override
  State<PostDetailView> createState() => _PostDetailViewState();
}

class _PostDetailViewState extends State<PostDetailView>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey(); // Create a key
  final PostDetailController controller = Get.put(PostDetailController());
  final _textEditingController = TextEditingController();
  final _scrollController = ScrollController();
  RxInt animalFlag = 0.obs;
  final RxBool _isLastLotteyCheck = false.obs;

  @override
  void initState() {
    // List<String?> args = Get.arguments;
    final args = Get.arguments;
    if (args != null) {
      controller.postId = args['postId'] ?? '';
      controller.title.value = args['title'] ?? '';
      controller.from = args['from'] ?? '';
      controller.category = args['category'] ?? '';
      controller.typeTwo = args['type2'] ?? '';
      controller.extra = args['extra'] ?? '';
    }
    controller.initData();
    super.initState();
  }

  Widget getNormalDetail() {
    if (controller.postDetail.value.content != null &&
        controller.from != 'gallery') {
      return Column(
        children: [
          HtmlWidget((controller.postDetail.value.lhcdocWords ?? "")),
          Text(
            controller.postDetail.value.title ?? "",
            style: TextStyle(
                color: Colors.black,
                fontSize: 24.sp,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          (controller.postDetail.value.content ?? "").contains('[em]')
              ? EmojiWidget(
                  content: controller.postDetail.value.content ?? "",
                )
              : HtmlWidget(controller.postDetail.value.content ?? ""),
        ],
      );
    }

    return Text(
      controller.postDetail.value.title ?? "",
      style: TextStyle(
          color: Colors.black, fontSize: 24.sp, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Obx(
      () => Scaffold(
        key: _homeKey,
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: Center(
            child: Text(
              controller.category == "forum"
                  ? "论坛详情"
                  : controller.title.value == ""
                      ? '帖子详情'
                      : controller.title.value,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          actionWidgets: [
            GlobalService.to.isAuthenticated.value
                ? _showMenu()
                : const SizedBox()
          ],
        ),
        endDrawer: GlobalService.to.isAuthenticated.value
            ? Container(
                color: Colors.white,
                width: Get.width * 0.5,
                height: Get.height,
                child: const HomeMenuWidget(),
              )
            : null,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (controller.postDetail.value.title == null)
              Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AppImage.asset("pl.png", width: 400.w),
                      SizedBox(height: 20.h),
                      Text("正在加载, 请稍等...", style: TextStyle(fontSize: 20.sp))
                    ]),
              ),
            if (controller.postDetail.value.title != null &&
                controller.postDetail.value.alias != 'rule' &&
                controller.postDetail.value.alias != 'mystery')
              getTopWidget(),
            if (controller.postDetail.value.title != null)
              Expanded(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.h, horizontal: 30.w),
                  child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          if (controller.postDetail.value.topAdWap != null)
                            GestureDetector(
                              onTap: () {},
                              child: AppImage.network(
                                '${controller.postDetail.value.topAdWap.pic}',
                                height: 120.h,
                                fit: BoxFit.contain,
                              ),
                            ),

                          if (controller.postDetail.value.baomaId != null &&
                              controller.postDetail.value.baomaId!.isNotEmpty &&
                              (controller.postDetail.value.lanmuBaoma ?? false))
                            _getLHCWidget(),
                          // if (controller.postDetail.value.lhcdocWords != null &&
                          //     controller.from == 'gallery' &&
                          //     controller.extra != 'humorGuess')
                          //   HtmlWidget(
                          //     controller.postDetail.value.lhcdocWords ?? "",
                          //   ),
                          Gap(20.h),
                          if (controller.from == 'gallery' &&
                              controller.extra == 'humorGuess' &&
                              (controller.category == 'tt8Yp69n' /* 网红流照 */))
                            AppImage.network(
                              'image',
                              errorWidget: AppImage.network(
                                img_images('lhc/err'),
                              ),
                            ),

                          Gap(20.h),
                          getNormalDetail(), //content html

                          if (controller.from != 'gallery' ||
                              (controller.extra == 'humorGuess' &&
                                  controller.category !=
                                      'tt8Yp69n' /* 网红流照 */ &&
                                  controller.postDetail.value.content != null))
                            ...List.generate(
                                controller.postDetail.value.contentPic!.length,
                                (idx) {
                              return AppImage.network(
                                  controller.postDetail.value.contentPic?[idx]);
                            }),

                          Gap(10.h),
                          if (controller.postDetail.value.plugins != null &&
                              controller.postDetail.value.plugins!.isNotEmpty &&
                              (controller.postDetail.value.plugins![0]
                                      ['type'] ==
                                  1) &&
                              controller.postDetail.value.plugins![0]['content']
                                  is List)
                            Column(
                              children: [
                                CarouselSlider(
                                  options: CarouselOptions(
                                    height: 280.w,
                                    viewportFraction: 1.0,
                                    enlargeCenterPage: false,
                                    autoPlay: true,
                                  ),
                                  items: (controller.postDetail.value
                                          .plugins![0]['content'] as List?)
                                      ?.map((item) {
                                    return GestureDetector(
                                      onTap: () => null,
                                      child: Center(
                                        child: AppImage.network(
                                          item['photo'] ?? "",
                                          fit: BoxFit.contain,
                                          height: 280.w,
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          if (controller.postDetail.value.plugins != null &&
                              controller.postDetail.value.plugins!.isNotEmpty &&
                              ((controller.postDetail.value.plugins?[0] !=
                                      null) &&
                                  controller.postDetail.value.plugins![0]
                                          ['content'] !=
                                      null &&
                                  controller.postDetail.value.plugins![0]
                                          ['type'] ==
                                      2))
                            Column(
                              children: [
                                HtmlWidget(controller.postDetail.value
                                    .plugins![0]['content']['html']),
                                Gap(10.h),
                              ],
                            ),
                          if (controller.postDetail.value.plugins != null &&
                              controller.postDetail.value.plugins!.isNotEmpty &&
                              (controller.postDetail.value.plugins![1]
                                      ['type'] ==
                                  1) &&
                              controller.postDetail.value.plugins![1]['content']
                                  is List)
                            Column(
                              children: [
                                CarouselSlider(
                                  options: CarouselOptions(
                                    height: 280.w,
                                    viewportFraction: 1.0,
                                    enlargeCenterPage: false,
                                    autoPlay: true,
                                  ),
                                  items: (controller.postDetail.value
                                          .plugins![1]['content'] as List?)
                                      ?.map((item) {
                                    return GestureDetector(
                                      onTap: () => null,
                                      child: Center(
                                        child: AppImage.network(
                                          item['photo'] ?? "",
                                          fit: BoxFit.contain,
                                          height: 280.w,
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          if (controller.postDetail.value.plugins != null &&
                              controller.postDetail.value.plugins!.isNotEmpty &&
                              ((controller.postDetail.value.plugins?[1] !=
                                      null) &&
                                  controller.postDetail.value.plugins![1]
                                          ['content'] !=
                                      null &&
                                  controller.postDetail.value.plugins![1]
                                          ['type'] ==
                                      2))
                            Column(
                              children: [
                                HtmlWidget(controller.postDetail.value
                                    .plugins![1]['content']['html']),
                                Gap(10.h),
                              ],
                            ),
                          Gap(10.h),

                          Gap(10.h),
                          if (controller.postDetail.value.vote != null)
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("第 ",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.bold)),
                                  Text(
                                      controller.postDetail.value.issue != null
                                          ? controller.postDetail.value.issue!
                                              .replaceAll("2024", "")
                                          : "",
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.bold)),
                                  Text(" 期投票",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.bold)),
                                ]),
                          SizedBox(height: 10.h),
                          if (controller.postDetail.value.vote != null)
                            GridView.count(
                                primary: false,
                                padding: EdgeInsets.all(2.w),
                                shrinkWrap: true,
                                crossAxisSpacing: 20.w,
                                mainAxisSpacing: 5.h,
                                crossAxisCount: 2,
                                childAspectRatio: 7,
                                children: [
                                  if (controller.postDetail.value.vote != null)
                                    ...List.generate(
                                        controller.postDetail.value.vote!
                                            .length, (index) {
                                      Vote vote = controller
                                          .postDetail.value.vote![index];
                                      return SizedBox(
                                          height: 30.h,
                                          child: Row(children: [
                                            Text(vote.animal ?? "",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.sp)),
                                            SizedBox(width: 5.w),
                                            Expanded(
                                                child: Container(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 2.h,
                                                            horizontal: 3.w),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  3.w)),
                                                      color: vote.percent ==
                                                              "100.00"
                                                          ? AppColors.drawMenu
                                                          : const Color
                                                              .fromARGB(255,
                                                              231, 231, 231),
                                                    ),
                                                    alignment:
                                                        Alignment.centerRight,
                                                    child: Text(
                                                        "${vote.percent.toString().replaceAll(".00", "")}%",
                                                        style: TextStyle(
                                                            color: AppColors
                                                                .ff5C5869,
                                                            fontSize: 16.sp))))
                                          ]));
                                    })
                                ]),
                          if (controller.postDetail.value.vote != null)
                            ElevatedButton(
                                onPressed: showVoteDialog,
                                style: AppButtonStyles.elevatedStyle(
                                    fontSize: 20,
                                    backgroundColor: AppColors.drawMenu,
                                    foregroundColor: Colors.white,
                                    width: 250,
                                    height: 40,
                                    padding: EdgeInsets.symmetric(
                                        vertical: 5.w, horizontal: 10.w)),
                                child: const Text("投票")),

                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                    "最后更新时间：${controller.postDetail.value.updateTime}",
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 18.sp))
                              ]),
                          Gap(10.h),
                          if (controller.postDetail.value.bottomAdWap != null)
                            AppImage.network(
                              '${controller.postDetail.value.bottomAdWap['pic']}',
                              fit: BoxFit.contain,
                              height: 120.h,
                            ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              width: 1, color: Colors.black))),
                                  padding: EdgeInsets.only(bottom: 10.h),
                                  child: Text(
                                    "全部评论",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18.sp,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ]),
                          // if(controller.contentDetail.value.)
                          if (controller.comments.isEmpty) commentEmpty(),
                          ...List.generate(controller.comments.value.length,
                              (index) {
                            return commentItemWidget(
                              controller.comments[index],
                              index,
                              controller.comments.length - index,
                            );
                          }),

                          if (controller.comments.isNotEmpty)
                            const Text("没有更多了"),
                        ]),
                  ),
                ),
              ),
            if (controller.postDetail.value.title != null)
              Container(
                color: AppColors.drawMenu,
                padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 10.w),
                child: IntrinsicHeight(
                  child: Row(
                    children: [
                      AppImage.asset("yhj.png", width: 50.w),
                      SizedBox(width: 5.w),
                      Expanded(
                          child: GestureDetector(
                        onTap: () {
                          if (!GlobalService.to.isAuthenticated.value) {
                            AppNavigator.toNamed(loginPath);
                            return;
                          }
                          if (checkIsTest()) return;

                          AppNavigator.toNamed(postConentReply);
                        },
                        child: Container(
                            height: 40.h,
                            padding: EdgeInsets.symmetric(horizontal: 20.w),
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.w)),
                                color: Colors.white),
                            child: const Text(
                              '说点什么...',
                              style: TextStyle(color: Colors.grey),
                            )),
                      )),
                      SizedBox(width: 5.w),
                      GestureDetector(
                        onTap: () async {
                          if (checkIsTest()) return;
                          if (!GlobalService.to.isAuthenticated.value) {
                            AppNavigator.toNamed(loginPath);
                            return;
                          }
                          int likeFlag =
                              (controller.postDetail.value.isLike != null &&
                                      controller.postDetail.value.isLike == 1)
                                  ? 0
                                  : 1;
                          bool result = await controller.likePost(
                              controller.postDetail.value.id ?? "", likeFlag);
                          if (result) {
                            await controller.getContentDetail();
                          }
                        },
                        child: Icon(CustomIcons.thumbs_up,
                            color:
                                (controller.postDetail.value.isLike != null &&
                                        controller.postDetail.value.isLike == 1)
                                    ? Colors.red
                                    : Colors.white),
                      ),
                      if (controller.postDetail.value.likeNum != null)
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            controller.postDetail.value.likeNum ?? "",
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      Gap(10.w),
                      GestureDetector(
                        onTap: () {
                          if (!GlobalService.to.isAuthenticated.value) {
                            AppNavigator.toNamed(loginPath);
                            return;
                          }
                          if (checkIsTest()) return;
                          AppNavigator.toNamed(postConentReply);
                        },
                        child: const Icon(CustomIcons.commenting_o,
                            color: Colors.white),
                      ),
                      if (controller.postDetail.value.replyCount != null)
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            '${controller.postDetail.value.replyCount ?? 0}',
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      Gap(10.w),
                      GestureDetector(
                        onTap: () async {
                          if (!GlobalService.to.isAuthenticated.value) {
                            AppNavigator.toNamed(loginPath);
                            return;
                          }

                          if (checkIsTest()) return;

                          int favFlag =
                              (controller.postDetail.value.isFav != null &&
                                      controller.postDetail.value.isFav == 1)
                                  ? 0
                                  : 1;
                          bool result = await controller.doFavorites(
                              controller.postDetail.value.id ?? "", favFlag);
                          if (result) {
                            await controller.getContentDetail();
                          }
                        },
                        child: Icon(FontAwesomeIcons.heart,
                            color: (controller.postDetail.value.isFav != null &&
                                    controller.postDetail.value.isFav == 1)
                                ? Colors.red
                                : Colors.white),
                      ),
                      if (controller.postDetail.value.favNum != null)
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            controller.postDetail.value.favNum.toString(),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }

  Widget _showMenu() {
    return GestureDetector(
        onTap: () {
          _homeKey.currentState!.openEndDrawer();
        },
        child: Container(
          padding: EdgeInsets.only(right: 10.w),
          child:
              AppImage.asset('menu_btn_white.png', width: 30.w, height: 30.w),
        ));
  }

  Widget getTopWidget() {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 1.h))),
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
      child: Row(
        children: [
          if (controller.postDetail.value.headImg == null ||
              controller.postDetail.value.headImg == "")
            Container(
              width: 80.w,
              height: 80.w,
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromARGB(255, 221, 221, 221)),
            ),
          if (controller.postDetail.value.headImg != "")
            GestureDetector(
              onTap: () {
                AppNavigator.toNamed(userProfilePath,
                    parameters: {'uid': controller.postDetail.value.uid ?? ""});
              },
              child: AppImage.network(controller.postDetail.value.headImg ?? "",
                  width: 90.w, height: 90.w),
            ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(controller.postDetail.value.nickname ?? "",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22.sp,
                        fontWeight: FontWeight.bold)),
                Text(
                  "楼主 ${controller.postDetail.value.createTime}",
                  style: TextStyle(color: AppColors.ff5C5869, fontSize: 18.sp),
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: rewardPost,
                child: AppImage.asset("redBag_1.png", width: 60.w),
              ),
              SizedBox(width: 10.w),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        if (!GlobalService.to.isAuthenticated.value) {
                          ToastUtils.show('请先登录正式账号');
                          return;
                        }
                        if (checkIsTest()) return;
                        if (!GlobalService.to.isAuthenticated.value) {
                          AppNavigator.toNamed(loginPath);
                        }
                        int followFlag =
                            (controller.postDetail.value.isFollow != null &&
                                    controller.postDetail.value.isFollow == 1)
                                ? 0
                                : 1;
                        bool result = await controller.followPoster(
                            controller.postDetail.value.uid ?? "", followFlag);
                        if (result) {
                          await controller.getContentDetail();
                        }
                      },
                      style: AppButtonStyles.elevatedStyle(
                          fontSize: 18,
                          backgroundColor: AppColors.drawMenu,
                          foregroundColor: Colors.white,
                          width: 100,
                          height: 40,
                          padding: EdgeInsets.symmetric(
                              vertical: 5.w, horizontal: 10.w)),
                      child: Text(
                          (controller.postDetail.value.isFollow != null &&
                                  controller.postDetail.value.isFollow == 1)
                              ? "取消关注"
                              : "关注楼主")),
                  ElevatedButton(
                      onPressed: () {
                        if (!GlobalService.to.isAuthenticated.value) {
                          ToastUtils.show('请先登录正式账号');
                          return;
                        }
                        AppNavigator.toNamed(lhcdocHistoryContentPath,
                            arguments: [controller.postDetail.value.uid ?? ""]);
                      },
                      style: AppButtonStyles.elevatedStyle(
                          fontSize: 18,
                          backgroundColor: AppColors.drawMenu,
                          foregroundColor: Colors.white,
                          width: 100,
                          height: 40,
                          padding: EdgeInsets.symmetric(
                              vertical: 5.w, horizontal: 10.w)),
                      child: const Text("历史帖子"))
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  void rewardPost() {
    if (!GlobalService.to.isAuthenticated.value) {
      ToastUtils.show('请先登录正式账号');
      return;
    }

    if (checkIsTest()) {
      return;
    }

    final random = Random();
    double p = 3 + random.nextDouble() * 17;
    _textEditingController.text = p.toStringAsFixed(2);
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.w))),
            child: SizedBox(
                height: 350.h,
                child: Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            height: 60.h,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10.w),
                                  topRight: Radius.circular(10.w)),
                              color: AppColors.drawMenu,
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              "帖子打赏",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )),
                        SizedBox(height: 20.h),
                        (controller.postDetail.value.headImg != null &&
                                controller.postDetail.value.headImg != "")
                            ? AppImage.network(
                                controller.postDetail.value.headImg ?? "",
                                width: 80.w,
                                height: 80.w)
                            : SizedBox(height: 80.w),
                        SizedBox(height: 20.h),
                        Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: AppColors.ff5C5869, width: 1),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.w))),
                            padding: EdgeInsets.symmetric(
                                vertical: 5.h, horizontal: 15.w),
                            width: 300.w,
                            height: 50.h,
                            child: IntrinsicHeight(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Text("¥",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                SizedBox(width: 10.w),
                                Expanded(
                                  child: Center(
                                      child: TextField(
                                    controller: _textEditingController,
                                    scrollController: _scrollController,
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          left: 10.w,
                                          right: 10.w,
                                          bottom: 20.h),
                                      border: InputBorder.none,
                                    ),
                                  )),
                                ),
                                const VerticalDivider(
                                    thickness: 1, color: AppColors.ff5C5869),
                                SizedBox(width: 7.w),
                                GestureDetector(
                                    onTap: () {
                                      p = 3 + random.nextDouble() * 17;
                                      _textEditingController.text =
                                          p.toStringAsFixed(2);
                                    },
                                    child: const Icon(CustomIcons.dice_three,
                                        color: AppColors.ff2089dc)),
                              ],
                            ))),
                        SizedBox(height: 30.h),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 5.h, horizontal: 10.w),
                          alignment: Alignment.center,
                          child: ElevatedButton(
                              onPressed: () async {
                                AppToast.show();
                                var data = await controller.tipContent(
                                    controller.postDetail.value.id ?? "",
                                    _textEditingController.text);
                                AppToast.dismiss();
                                if (data.code == 0) {
                                  AppToast.showSuccess(msg: '打赏成功');
                                  Navigator.of(context).pop();
                                }
                              },
                              style: AppButtonStyles.elevatedStyle(
                                  backgroundColor: AppColors.drawMenu,
                                  foregroundColor: Colors.white,
                                  fontSize: 20,
                                  width: 400,
                                  height: 30),
                              child: const Text("立即支付")),
                        )
                      ],
                    ),
                    Positioned(
                        top: 0,
                        right: 5.w,
                        child: IconButton(
                            onPressed: () => Navigator.of(context).pop(),
                            icon: const Icon(Icons.close, color: Colors.white)))
                  ],
                )),
          );
        });
  }

  void showVoteDialog() {
    if (!GlobalService.to.isAuthenticated.value) {
      ToastUtils.show('请先登录正式账号');
      return;
    }
    animalFlag.value = 0;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.w))),
              child: Obx(() => Container(
                  padding: EdgeInsets.all(10.w),
                  height: 350.h,
                  child: Column(
                    children: [
                      GridView.count(
                          primary: false,
                          padding: EdgeInsets.all(5.w),
                          shrinkWrap: true,
                          crossAxisSpacing: 20.w,
                          mainAxisSpacing: 20.h,
                          crossAxisCount: 3,
                          childAspectRatio: 3,
                          children: [
                            if (controller.postDetail.value.vote != null)
                              ...List.generate(
                                  controller.postDetail.value.vote!.length,
                                  (index) {
                                Vote vote =
                                    controller.postDetail.value.vote![index];
                                return GestureDetector(
                                    onTap: () =>
                                        animalFlag.value = vote.animalFlag ?? 0,
                                    child: Container(
                                        height: 50.h,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: vote.animalFlag ==
                                                        animalFlag.value
                                                    ? Colors.red
                                                    : Colors.grey,
                                                width: 2.w),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.w))),
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(vote.animal ?? "",
                                                  style: TextStyle(
                                                      color: Colors.red,
                                                      fontSize: 16.sp)),
                                              SizedBox(width: 5.w),
                                              Text(
                                                  "${vote.percent.toString().replaceAll(".00", "")}%",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16.sp)),
                                            ])));
                              })
                          ]),
                      Gap(
                        20.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                              onPressed: () => Navigator.of(context).pop(),
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: Colors.grey, width: 2.w),
                                      borderRadius:
                                          BorderRadius.circular(10.w)),
                                  backgroundColor: Colors.white,
                                  foregroundColor: Colors.black,
                                  textStyle: TextStyle(fontSize: 20.sp),
                                  minimumSize: Size(200.w, 50.w)),
                              child: const Text("取消")),
                          ElevatedButton(
                              onPressed: animalFlag.value == 0
                                  ? null
                                  : () {
                                      controller
                                          .vote(
                                              controller.postDetail.value.id ??
                                                  "",
                                              animalFlag.value)
                                          .then((data) {
                                        if (data.code == 0) {
                                          AppToast.showSuccess(msg: data.msg);
                                          Navigator.of(context).pop();
                                        } else {
                                          AppToast.showError(msg: data.msg);
                                        }
                                      });
                                    },
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: AppColors.drawMenu,
                                          width: 2.w),
                                      borderRadius:
                                          BorderRadius.circular(10.w)),
                                  backgroundColor: AppColors.drawMenu,
                                  foregroundColor: Colors.white,
                                  textStyle: TextStyle(fontSize: 20.sp),
                                  minimumSize: Size(200.w, 50.w)),
                              child: const Text("确定"))
                        ],
                      )
                    ],
                  ))));
        });
  }

  Widget _getLHCWidget() {
    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/icon_border.png'),
            fit: BoxFit.fill, // Makes sure the image covers the whole area
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () => AppNavigator.toNamed(AppRoutes.lotteryResult,
                    arguments: [controller.lotteryNumber.value.gameId]),
                child: Text(
                  '历史记录>',
                  style: TextStyle(
                      color: const Color(0xff666666), fontSize: 15.sp),
                ),
              ),
              Row(
                children: [
                  AppImage.network(IconResource.sound01(),
                      width: 21.w, height: 21.w),
                  SizedBox(width: 10.w),
                  Row(children: [
                    Text('第',
                        style: TextStyle(
                            fontSize: 20.sp,
                            color: const Color(0xff333333),
                            fontWeight: FontWeight.bold)),
                    Obx(
                      () => Text(
                          getLotteryNoOrIssueSubstring(
                              controller.lotteryNumber.value),
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.red,
                              fontWeight: FontWeight.bold)),
                    ),
                    Text('期开奖结果',
                        style: TextStyle(
                            fontSize: 20.sp,
                            color: const Color(0xff333333),
                            fontWeight: FontWeight.bold)),
                  ]),
                ],
              ),
              Row(children: [
                Text('咪',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold)),
                SizedBox(width: 10.w),
                FlutterSwitch(
                  width: 50.h,
                  height: 30.h,
                  value: _isLastLotteyCheck.value,
                  activeColor: const Color(0xff11c1f3),
                  onToggle: (bool value) => _isLastLotteyCheck.value = value,
                )
              ])
            ],
          ),
          Padding(
            padding: EdgeInsets.all(10.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ...List.generate(controller.showlotterys.value.length, (index) {
                  final item = controller.showlotterys.value[index];
                  return LotteryBallWidget(
                    score: item.number ?? '',
                    color: item.color ?? '',
                    text: item.sx ?? '',
                    isLastItem: index == 6,
                    isEnableLastItem: _isLastLotteyCheck.value,
                    onChange: (value) {
                      _isLastLotteyCheck.value = value;
                    },
                  );
                }),
              ],
            ),
          ),
        ]));
  }

  String getLotteryNoOrIssueSubstring(LotteryNumberData lotteryNum) {
    if (lotteryNum.lhcdocLotteryNo != null &&
        lotteryNum.lhcdocLotteryNo!.isNotEmpty) {
      return lotteryNum.lhcdocLotteryNo!;
    } else if (lotteryNum.issue != null &&
        (lotteryNum.issue ?? '').length >= 3) {
      return (lotteryNum.issue ?? '')
          .substring((lotteryNum.issue ?? '').length - 3);
    } else {
      return ''; // or any default value you prefer
    }
  }

  Widget commentEmpty() {
    return Column(
      children: [
        AppImage.asset("pl.png", width: 400.w),
        const Text("还没有评论，你的机会来了！"),
      ],
    );
  }

  Widget commentItemWidget(CommentModel model, int index, int orderIndex) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h),
      child: GestureDetector(
        onTap: () {
          AppNavigator.toNamed(commentPagePath, arguments: {
            'commentId': controller.postId,
            'replyPId': model.id,
          });
        },
        child: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      AppNavigator.toNamed(userProfilePath,
                          parameters: {'uid': model.uid ?? ""});
                    },
                    child: AppImage.network(model.headImg ?? "",
                        width: 30.w, height: 30.w, fit: BoxFit.cover),
                  ),
                  Gap(20.w),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Container(
                                padding: EdgeInsets.symmetric(horizontal: 10.w),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: const Color(0xffdad6d6))),
                                child: Text(
                                  '${orderIndex}楼',
                                  style:
                                      const TextStyle(color: Color(0xffa3adad)),
                                )),
                            Gap(20.w),
                            Text(
                              '${model.nickname ?? '昵称已被禁用'}',
                              style: const TextStyle(color: Color(0xffa3adad)),
                            ),
                            if (model.isAdmin == 1)
                              AppImage.asset(
                                'gly.png',
                                width: 50.w,
                                height: 20.h,
                                fit: BoxFit.cover,
                              ),
                            if (model.isAuthor == 1)
                              AppImage.asset(
                                'lz.png',
                                width: 50.w,
                                height: 20.h,
                                fit: BoxFit.cover,
                              ),
                          ],
                        ),
                        Text(
                          '${model.actionTime}',
                          style: const TextStyle(color: Color(0xffafafaf)),
                        ),
                        Gap(10.h),
                        (model.content ?? "").contains('[em_')
                            ? EmojiWidget(content: model.content ?? '')
                            : Text('${model.content}'),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      controller
                          .likePost(model.id ?? '', model.isLike == 1 ? 0 : 1,
                              type: 2)
                          .then((data) {
                        if (data) {
                          model.isLike = model.isLike == 1 ? 0 : 1;
                          if (model.isLike == 1) {
                            model.likeNum =
                                (int.parse(model.likeNum ?? '0') + 1)
                                    .toString();
                          } else {
                            model.likeNum =
                                (int.parse(model.likeNum ?? '0') - 1)
                                    .toString();
                          }
                          controller.comments[index] = model;
                          controller.comments.refresh();
                        }
                      });
                    },
                    child: Row(
                      children: [
                        Icon(
                          CustomIcons.thumbs_up,
                          color: (model.isLike != null && model.isLike == 1)
                              ? Colors.red
                              : Colors.black,
                          size: 20.w,
                        ),
                        Text(
                          ' ${(model.likeNum == null || model.likeNum == '0') ? "" : model.likeNum}',
                          style: const TextStyle(color: Colors.black),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 60.w, top: 10.h),
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 3.w),
                decoration: BoxDecoration(
                    color: AppColors.drawMenu,
                    borderRadius: BorderRadius.circular(20.w)),
                child: const Text(
                  '回复',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Column(
                children: [
                  ...(model.secReplyList ?? [])
                      .take(3)
                      .map<Widget>((res) => Column(
                            children: [
                              Row(
                                children: [
                                  // CircleAvatar(
                                  //   backgroundImage: NetworkImage(
                                  //     res['headImg'] == ''
                                  //         ? defaultAvatar
                                  //         : res['headImg'],
                                  //   ),
                                  //   radius: 10,
                                  // ),
                                  GestureDetector(
                                    onTap: () {
                                      // AppNavigator.toNamed(userProfilePath,
                                      //     parameters: {'uid': res.uid ?? ""});
                                    },
                                    child: AppImage.network(res.headImg ?? "",
                                        width: 30.w,
                                        height: 30.w,
                                        fit: BoxFit.cover),
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    res.nickname ?? '昵称已被禁用 :',
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.blue,
                                    ),
                                  ),
                                  if (res.isAdmin == 1)
                                    AppImage.asset(
                                      'gly.png',
                                      width: 50.w,
                                      height: 20.h,
                                      fit: BoxFit.cover,
                                    ),
                                  if (res.isAuthor == 1)
                                    AppImage.asset(
                                      'lz.png',
                                      width: 50.w,
                                      height: 20.h,
                                      fit: BoxFit.cover,
                                    ),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 5, right: 40),
                                child: Text(
                                  '${res.content ?? ''}',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ],
                          )),
                  if ((model.secReplyList ?? []).length > 3)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 10),
                      child: Text('查看全部${model.replyCount ?? ""}条回复 >'),
                    ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10.h),
                height: 10.h,
                color: const Color(0xfff9f9f9),
              )
            ],
          ),
        ),
      ),
    );
  }
}
