import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/data/models/LhlDataModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class OldYearPage extends StatefulWidget {
  const OldYearPage({super.key});

  @override
  State<OldYearPage> createState() => _OldYearPageState();
}

class _OldYearPageState extends State<OldYearPage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  final AuthController authController = Get.find();
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  Rx<LhlDataModel> lhlData = LhlDataModel().obs;
  bool _isLoading = true; // To manage loading state

  @override
  void initState() {
    super.initState();
    print("Error fetching data  11111 ");
    _fetchData(); // Call your data fetching method
  }

  Future<void> _fetchData() async {
    AppToast.show();
    await _appLHCDocRepository.getlhlItem().then((data) {
      _isLoading = false;
      AppToast.dismiss();
      lhlData.value = LhlDataModel.fromJson(data);

      print({'object ====== 11111': lhlData.value});
    });
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        key: _homeKey,
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "老黃历",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ]),
          actionWidgets: [
            GlobalService.to.isAuthenticated.value ? _showMenu() : Container()
          ],
        ),
        endDrawer: GlobalService.to.isAuthenticated.value
            ? Container(
                color: Colors.white,
                width: Get.width * 0.5,
                height: Get.height,
                child: const HomeMenuWidget(),
              )
            : null,
        body: _isLoading
            ? AppImage.network(
                '${GlobalService.to.appInfo.value.siteUrl}images/lhc/oldYear.png',
                width: Get.width,
                height: Get.height,
                fit: BoxFit.fill,
              )
            : Stack(
                children: [
                  // Background image
                  AppImage.network(
                    '${GlobalService.to.appInfo.value.siteUrl}images/lhc/oldYear.png',
                    width: Get.width,
                    height: Get.height,
                    fit: BoxFit.fill,
                  ),

                  // Row to align the text horizontally
                  const Positioned(
                    top: 0.0, // Adjust vertical position as needed
                    left: 0,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .spaceBetween, // Distribute text evenly
                        children: [
                          Text(
                            '9月',
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(
                                  173, 87, 0, 1), // Adjust color for visibility
                            ),
                          ),
                          Text(
                            'Septemper',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                          Text(
                            '2024年',
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Positioned(
                    top: 50.0, // Adjust vertical position as needed
                    left: 0,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment:
                            MainAxisAlignment.center, // Distribute text evenly
                        children: [
                          Text(
                            '19',
                            style: TextStyle(
                              fontSize: 100,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Positioned(
                    top: 200.0, // Adjust vertical position as needed
                    left: 0,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment:
                            MainAxisAlignment.end, // Distribute text evenly
                        children: [
                          Text(
                            '甲辰年 八月 小',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  const Positioned(
                    top: 290.0, // Adjust vertical position as needed
                    left: 0,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .spaceBetween, // Distribute text evenly
                        children: [
                          Text(
                            '星期四',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                          Text(
                            '十七',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Positioned(
                    top: 340.0, // Adjust vertical position as needed
                    left: 0,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .spaceBetween, // Distribute text evenly
                        children: [
                          Text(
                            '星期四',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                          Text(
                            '星期四',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                          Text(
                            '星期四',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                          Text(
                            '星期四',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                          Text(
                            '十七',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Positioned(
                    top: 480.0, // Adjust vertical position as needed
                    left: 0,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment:
                            MainAxisAlignment.center, // Distribute text evenly
                        children: [
                          Text(
                            '十七',
                            style: TextStyle(
                              fontSize: 18,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Positioned(
                    top: 560.0, // Adjust vertical position as needed
                    left: 0,
                    right: 0,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .spaceBetween, // Distribute text evenly
                        children: [
                          Text(
                            '9月',
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(
                                  173, 87, 0, 1), // Adjust color for visibility
                            ),
                          ),
                          SizedBox(
                            width: 100, // 设置文本的宽度为 200
                            child: Text(
                              'Left Text',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Color.fromRGBO(173, 87, 0, 1),
                              ),
                            ),
                          ),
                          Text(
                            '2024年',
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(173, 87, 0, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Positioned(
                    top: 620.0, // Adjust vertical position as needed
                    left: 0,
                    right: -5,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 5.0), // Add padding on left and right
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .spaceBetween, // Distribute text evenly
                        children: [
                          Text(
                            '9月',
                            style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(
                                  173, 87, 0, 1), // Adjust color for visibility
                            ),
                          ),
                          SizedBox(
                            width: 80, // 设置文本的宽度为 200
                            child: Text(
                              'Left Text',
                              style: TextStyle(
                                fontSize: 16,
                                color: Color.fromRGBO(173, 87, 0, 1),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ));
  }

  Widget _showMenu() {
    return GestureDetector(
        onTap: () {
          _homeKey.currentState?.openEndDrawer();
        },
        child: Container(
          padding: EdgeInsets.only(right: 10.w, top: 20.h),
          child: const Text(
            '选择日期',
            style: TextStyle(
              fontSize: 14,
              color: Colors.white, // Adjust color for visibility
            ),
          ),
        ));
  }
}
