import 'package:fpg_flutter/app/component/live_lottery/LotteryNum.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/ColdHotModel.dart';
import 'package:fpg_flutter/data/models/LhcNoListItemModel.dart';
import 'package:fpg_flutter/data/models/LhcNoListModel.dart';
import 'package:fpg_flutter/data/models/PostDetailModel.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/data/models/QueryAssisModel.dart';
import 'package:fpg_flutter/data/models/TkListItemModel.dart';
import 'package:fpg_flutter/data/models/TkListModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/common_util.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:lpinyin/lpinyin.dart';

class LhGalleryController extends GetxController {
  String alias = "", sort = "py", cid = "", from = "", extra = "";
  RxString colorType = "彩色".obs;
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  TkListModel tkList = TkListModel();
  List<TkListItemModel> tkListItem = [];
  RxInt curPage = 1.obs;
  int perPage = 1000;
  List<String> colorTypes = ['彩色', '黑白'];
  RxMap<String, List<TkListItemModel>> chineseCharactersByPinyin = RxMap();
  RxBool isLoading = false.obs;
  RxBool isLoading2 = false.obs;
  LhcNoListModel lhcNoList = LhcNoListModel();
  Rx<LotteryNumberData> lotteryNumber = LotteryNumberData().obs;
  RxList<LotteryNum> showlotterys = RxList();

  RxList<String> yearResult = RxList();
  RxString curYear = "".obs;
  RxList<LhcNoListItemModel> lhcNoListItem = RxList();
  Rx<LhcNoListItemModel> curListItem = LhcNoListItemModel().obs;
  Rx<PostDetailModel> contentDetail = PostDetailModel().obs;
  RxList<ColdHotModel> lhcColdHot = RxList();
  RxMap<String, MixBallModel> lhcItemZodaic = RxMap();
  RxMap<String, MixBallModel> lhcItemColur = RxMap();
  Rxn<QueryAssisModel> lhcQueryAssis = Rxn();
  RxInt lotteryCloseTime = 0.obs;

  @override
  void onReady() {}

  Future<void> getTkList() async {
    tkList = TkListModel();
    tkListItem = [];
    String token = AppDefine.userToken?.apiSid ?? "";
    if (alias != "") {
      isLoading.value = true;
      await _appLHCDocRepository
          .getTkList(
              token: token,
              alias: alias,
              sort: sort,
              colorType: getColorType(),
              showFav: "0",
              rows: perPage)
          .then((data) {
        if (data != null) {
          tkList = data;
          tkListItem = tkList.list ?? [];
          _chineseCharactersGroupedByPinyin();
        }
        isLoading.value = false;
      });
    }
  }

  Future<void> getPostDetail(String type2) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    yearResult.value = [];
    lhcNoListItem.value = [];
    isLoading2.value = true;
    if (from == 'gallery') {
      await _appLHCDocRepository
          .getLhcNoList(token: token, type: cid, type2: type2)
          .then((data) async {
        if (data != null) {
          lhcNoList = data;
          if (lhcNoList.list!.isNotEmpty) {
            getItemYearResult();
          } else {
            isLoading2.value = false;
          }
        } else {
          isLoading2.value = false;
        }
      });
    } else if (from == 'humorGuess') {
      await _appLHCDocRepository
          .getLhcContentList(alias: alias, page: 1, rows: 1000)
          .then((data) async {
        if (data != null) {
          lhcNoList = LhcNoListModel(title: data.title, list: []);
          for (int i = 0; i < data.list!.length; i++) {
            LhcdocContentList cItem = data.list![i];
            lhcNoList.list?.add(LhcNoListItemModel(
                fullLhcNo: cItem.fullLhcno ?? cItem.createTime ?? "",
                contentPic: "",
                id: cItem.cid,
                lhcNo: cItem.fullLhcno));
          }
          if (data.list!.isNotEmpty) {
            getItemYearResult();
          } else {
            isLoading2.value = false;
          }
        } else {
          isLoading2.value = false;
        }
      });
    }
  }

  Future getContentDetail({bool showLoading = true}) async {
    String id = curListItem.value.id ?? "";
    if (isLoading2.value == false && showLoading) {
      isLoading2.value = true;
    }
    await _appLHCDocRepository.getLhcContentDetail(id).then((data) async {
      if (data != null) {
        contentDetail.value = data;
        await requestLHN(data.baomaId ?? "");
      }
      isLoading2.value = false;
    });
  }

  void getItemYearResult() {
    yearResult.value = [];
    lhcNoListItem.value = [];
    int len = (lhcNoList.list ?? []).length;
    for (int i = 0; i < len; i++) {
      LhcNoListItemModel item = lhcNoList.list![i];
      String y = item.fullLhcNo?.substring(0, 4) ?? "";
      if (yearResult.indexWhere((item) => item == y) < 0) {
        yearResult.value.add(y);
      }
    }
    yearResult.value.sort((a, b) => int.parse(b) - int.parse(a));
    curYear.value = yearResult.value[0];
    getLhcNoListItem();
  }

  void getLhcNoListItem() async {
    lhcNoListItem.value = [];
    int len = (lhcNoList.list ?? []).length;
    for (int i = 0; i < len; i++) {
      LhcNoListItemModel item = lhcNoList.list![i];
      String y = item.fullLhcNo?.substring(0, 4) ?? "";
      if (y == curYear.value) {
        lhcNoListItem.value.add(item);
      }
    }
    curListItem.value = lhcNoListItem.value[0];
    await getContentDetail();
  }

  Future requestLHN(String gameId) async {
    await _appLHCDocRepository
        .lotteryNumber(gameId: int.parse(gameId), gameName: "")
        .then((data) {
      if (data != null) {
        lotteryNumber.value = data;
        try {
          lotteryCloseTime.value = DateUtil.getCloseTimes(data.endtime ?? "0");
          List<String> lotteryNumbers = data.numbers?.split(',') ?? [];
          List<String> numColors = data.numColor?.split(',') ?? [];
          List<String> numSxs = data.numSx?.split(',') ?? [];
          List<LotteryNum> nums = lotteryNumbers.asMap().entries.map((entry) {
            int index = entry.key;
            String item = entry.value;
            return LotteryNum(
              number: item,
              color: numColors[index],
              sx: numSxs[index],
            );
          }).toList();
          showlotterys.value = nums;
        } catch (e) {
          AppLogger.e('requestLHN calculator error', error: e);
        }
      }
    });
  }

  void _chineseCharactersGroupedByPinyin() {
    chineseCharactersByPinyin.value = RxMap();
    for (int index = 0; index < tkListItem.length; index++) {
      TkListItemModel item = tkListItem[index];
      String pinyin = PinyinHelper.getShortPinyin(item.name ?? "");
      String initial = pinyin[0].toUpperCase();
      if (CommonUtil.isNumeric(initial) || initial == "(") {
        initial = "#";
      } // Assuming you want the first letter of the Pinyin

      if (!chineseCharactersByPinyin.containsKey(initial)) {
        chineseCharactersByPinyin[initial] = [];
      }
      chineseCharactersByPinyin[initial]?.add(item);
    }

    // Sort the groups by their keys (Pinyin initials)
    chineseCharactersByPinyin.value = Map.fromEntries(
        chineseCharactersByPinyin.entries.toList()
          ..sort((a, b) => a.key.compareTo(b.key)));

    if (chineseCharactersByPinyin.containsKey("#")) {
      List<TkListItemModel> item = chineseCharactersByPinyin["#"] ?? [];
      chineseCharactersByPinyin.value.removeWhere((key, value) => key == "#");
      chineseCharactersByPinyin.value['#'] = item;
    }
  }

  void filterResult(String keyword) {
    if (keyword.isNotEmpty) {
      tkListItem =
          tkList.list!.where((item) => item.name!.contains(keyword)).toList();
    } else {
      tkListItem = tkList.list ?? [];
    }
    _chineseCharactersGroupedByPinyin();
  }

  String getColorType() {
    int index = colorTypes.indexWhere((item) => item == colorType.value);
    return index.toString();
  }

  Future<bool> doFavorites(String id, int favFlag) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.doFavorites(
        id: id, favFlag: favFlag, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0);
  }

  Future<bool> likePost(String rid, int likeFlag) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    final data = await _appLHCDocRepository.likePost(
        rid: rid, likeFlag: likeFlag, token: token);
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0 || data.code == 400);
  }

  Future<void> getColdHot() async {
    if (lhcColdHot.isEmpty) {
      lhcColdHot.value = [];
      AppToast.show();
      await _appLHCDocRepository.getColdHot().then((data) {
        AppToast.dismiss();
        if (data != null) {
          lhcColdHot.value = data;
        }
      });
    }
  }

  Future<void> getQueryAssis() async {
    if (lhcQueryAssis.value == null) {
      lhcItemZodaic.value = RxMap();
      lhcItemColur.value = RxMap();
      AppToast.show();
      await _appLHCDocRepository.getQueryAssis().then((data) {
        AppToast.dismiss();
        if (data != null) {
          lhcQueryAssis.value = data;
          Map<String, MixBallModel> mixBall = data.mixBall;
          mixBall.forEach((key, value) {
            if (value.group.toString() == '6') {
              lhcItemColur[key] = value;
            } else if (value.group.toString() == '9') {
              lhcItemZodaic[key] = value;
            }
          });
        }
      });
    }
  }
}
