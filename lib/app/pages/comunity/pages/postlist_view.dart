import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/postlist_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';

import 'package:fpg_flutter/app/widgets/app_image.dart';
// import 'package:flutter_html/flutter_html.dart';

import 'package:get/get.dart';

//UserInfoPage.ts
class PostListView extends StatefulWidget {
  const PostListView({super.key});

  @override
  State<PostListView> createState() => _PostListView();
}

class _PostListView extends State<PostListView> with TickerProviderStateMixin {
  final PostlistViewController controller = Get.put(PostlistViewController());
  List<String> forumHead1 = ['全部', '澳彩', '港彩', '台彩', '新彩'];
  List<String> forumHead2 = ['全部主题'];
  late TabController _tabController;
  late TabController _tab2Controller;
  String? type = 'mystery';
  late String name;
  late String alias;
  late String category;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: forumHead1.length, vsync: this);
    _tab2Controller = TabController(length: forumHead2.length, vsync: this);
    name = controller.callItem.value.name ?? '';
    alias = controller.callItem.value.alias ?? '';
    category = controller.callItem.value.model ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(children: [
            Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
            Text(
              '返回',
              style: AppTextStyles.ffffffff(context, fontSize: 25),
            ),
          ]),
        ),
        title: Obx(
          () => Text(
            controller.callItem.value.name ?? '',
            style: AppTextStyles.ffffffff(context, fontSize: 25),
          ),
        ),
        centerTitle: true,
        actions: [
          _action(),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _lineText('广告', '1777彩票1777.cc注册有惊喜!'),
            _lineText('公告', '49图库049TTK.com 有您更精彩～'),
            const SizedBox(
              height: 20,
            ),
            Container(
              width: 0.8.sw,
              height: 74.w,
              decoration:
                  BoxDecoration(border: Border.all(color: AppColors.ffbfa46d)),
              child: Column(children: [
                _tabBar(forumHead1, _tabController, AppColors.navBarTitleColor),
              ]),
            ),
            const SizedBox(
              height: 2,
            ),
            Container(
              width: 0.8.sw,
              height: 74.w,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: forumHead2.length == 1
                          ? AppColors.surface
                          : AppColors.ffbfa46d)),
              child: Column(children: [
                _tabBar(
                    forumHead2,
                    _tab2Controller,
                    forumHead2.length == 1
                        ? AppColors.surface
                        : AppColors.navBarTitleColor),
              ]),
            ),

            // controller.callItem.value.
            // type=='mystery'?
            Container(
              child: ListView.builder(
                shrinkWrap: true,
                physics:
                    const NeverScrollableScrollPhysics(), // Prevent ListView from scrolling
                itemCount: controller.lhcdocContent.value.list!.length,
                itemBuilder: (context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _lineText(String title, String content) {
    return Container(
        width: 1.sw,
        padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 20.w),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    width: 1.w,
                    color: AppColors.ffC5CBC6,
                    style: BorderStyle.solid))),
        child: Row(
          children: [
            Text(
              title,
              style: AppTextStyles.ffbfa56d_(context, fontSize: 22),
            ),
            SizedBox(
              width: 10.w,
            ),
            Text(
              content,
              style: AppTextStyles.ff000000(context, fontSize: 22),
            )
          ],
        ));
  }

  Widget _action() {
    return Obx(
      () => Row(
        children: [
          if (controller.callItem.value.model == 'forum')
            controller.isLHTKSite.value
                ? GestureDetector(
                    onTap: () {},
                    child: AppImage.network(
                        img_mobileTemplate('61', 'forum_search'),
                        width: 30.w,
                        height: 30.w),
                  )
                : const SizedBox(),
          GestureDetector(
            onTap: () {
              if (!controller.isLogin.value) {
                // Get.toNamed(
                //   TemplatePages.getAllPages()[
                //           AppDefine.systemConfig?.mobileTemplateCategory]
                //       .signIn,
                // );
              } else {
                // push(PageName.PostNewPage, {
                //   alias: route?.params?.alias,
                // });
              }
            },
            child: AppImage.network(
                (!controller.isLHTKSite.value)
                    ? img_static01('lhc/write')
                    : img_mobileTemplate('61', 'forum_write'),
                width: 30.w,
                height: 30.w),
          ),
          !controller.isLHTKSite.value
              ? GestureDetector(
                  onTap: () {
                    if (!controller.isLogin.value) {
                      print(
                          AppDefine.systemConfig!.mobileTemplateCategory ?? '');

                      // Get.toNamed(
                      //   TemplatePages.getAllPages()[
                      //           AppDefine.systemConfig?.mobileTemplateCategory]
                      //       .signIn,
                      // );
                    } else {
                      // push(PageName.PostNewPage, {
                      //   alias: route?.params?.alias,
                      // });
                    }
                  },
                  child: AppImage.network(
                      img_mobileTemplate(
                          '61', 'lhc/search'), //img_git_images('lhc/search')
                      width: 30.w,
                      height: 30.w),
                )
              : const SizedBox(),
          // !controller.isLHTKSite.value?HomeRightMenuToggle():const SizedBox(),
        ],
      ),
    );
  }

  TabBar _tabBar(
      List<String> tabs, TabController tabController, Color? fillColor) {
    return TabBar(
      controller: tabController,
      onTap: (index) {},
      tabs: tabs.map((item) => Tab(text: item)).toList(),
      unselectedLabelColor: AppColors.navBarTitleColor,
      labelColor:
          tabs.length == 1 ? AppColors.navBarTitleColor : AppColors.ffffffff,
      labelStyle:
          AppTextStyles.ff000000(context, fontSize: tabs.length == 1 ? 24 : 18),
      indicatorSize: TabBarIndicatorSize.tab,
      indicatorWeight: 1.0,
      // indicatorPadding: EdgeInsets.symmetric(vertical: 1.0.w),
      indicator: BoxDecoration(
        color: fillColor ??
            AppColors.navBarTitleColor, // Change to your desired color
        borderRadius: BorderRadius.circular(2.0.w),
      ),
      indicatorColor: AppColors.ffbfa46d,
    );
  }
}
