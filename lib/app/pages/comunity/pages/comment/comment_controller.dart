import 'package:fpg_flutter/app/component/live_lottery/live_lottery_controller.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/CommentModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:get/get.dart';

class CommentController extends GetxController {
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  LiveLotteryController _liveLotteryController = Get.find();
  RxList<CommentModel> comments = RxList();

  String commentId = '';
  Future getContentReplyList() async {
    await _appLHCDocRepository.contentReplyList(postId: commentId).then((data) {
      comments.value = data ?? [];
    });
  }

  Future<bool> likePost(String rid, int likeFlag, {int? type = 1}) async {
    String token = AppDefine.userToken?.apiSid ?? "";
    dynamic data = await _appLHCDocRepository.likePost(
      rid: rid,
      likeFlag: likeFlag,
      token: token,
      type: type,
    );
    if (data.code == 1) {
      AppToast.showToast(data.msg);
    }
    return (data.code == 0 || data.code == 400);
  }
}
