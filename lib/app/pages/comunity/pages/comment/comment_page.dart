import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/comment/comment_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/CommentModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class CommentPage extends StatefulWidget {
  const CommentPage({super.key});

  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  final CommentController controller = Get.put(CommentController());
  @override
  void initState() {
    controller.commentId = Get.arguments['commentId'] ?? '';
    controller.getContentReplyList();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Container(
              padding: EdgeInsets.only(left: 10.w),
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.ffffffff, size: 18.w),
                Text('返回',
                    style: AppTextStyles.ffffffff(context, fontSize: 18)),
              ]),
            )),
        titleWidget: const Center(
          child: Text(
            "论坛详情",
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        ),
        // actionWidgets: [
        //   GlobalService.to.isAuthenticated.value ? _showMenu() : Container()
        // ],
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Obx(
                () => Column(
                  children: [
                    ...List.generate(controller.comments.value.length, (index) {
                      return CommentItemWidget(controller.comments[index],
                          index, (controller.comments ?? []).length - index);
                    }),
                    if (controller.comments.isNotEmpty) Text('我是有底线的'),
                  ],
                ),
              ),
            ),
          ),
          Container(
            color: AppColors.drawMenu,
            padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 10.w),
            child: IntrinsicHeight(
              child: Row(
                children: [
                  AppImage.asset("yhj.png", width: 50.w),
                  SizedBox(width: 5.w),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      if (checkIsTest()) return;
                      if (!GlobalService.to.isAuthenticated.value) {
                        AppNavigator.toNamed(loginPath);
                      }
                      AppNavigator.toNamed(postConentReply);
                    },
                    child: Container(
                        height: 40.h,
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        alignment: Alignment.centerLeft,
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.w)),
                            color: Colors.white),
                        child: const Text(
                          '説點什麽...',
                          style: TextStyle(color: Colors.grey),
                        )),
                  )),
                  SizedBox(width: 5.w),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget CommentItemWidget(CommentModel model, int index, int orderIndex) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h),
      child: GestureDetector(
        onTap: () {},
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 30.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      AppNavigator.toNamed(userProfilePath,
                          parameters: {'uid': model.uid ?? ""});
                    },
                    child: AppImage.network(model.headImg ?? "",
                        width: 30.w, height: 30.w, fit: BoxFit.cover),
                  ),
                  Gap(20.w),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            // Container(
                            //     padding: EdgeInsets.symmetric(horizontal: 10.w),
                            //     decoration: BoxDecoration(
                            //         border: Border.all(
                            //             color: const Color(0xffdad6d6))),
                            //     child: Text(
                            //       '${orderIndex}楼',
                            //       style:
                            //           const TextStyle(color: Color(0xffa3adad)),
                            //     )),
                            // Gap(20.w),
                            Text(
                              '${model.nickname ?? '昵称已被禁用'}',
                              style: const TextStyle(color: Colors.red),
                            ),
                            if (model.isAdmin == 1)
                              AppImage.asset(
                                'gly.png',
                                width: 50.w,
                                height: 20.h,
                                fit: BoxFit.cover,
                              ),
                            if (model.isAuthor == 1)
                              AppImage.asset(
                                'lz.png',
                                width: 50.w,
                                height: 20.h,
                                fit: BoxFit.cover,
                              ),
                          ],
                        ),
                        Gap(10.h),
                        Text('${model.content}'),
                        Text(
                          '${model.actionTime}',
                          style: const TextStyle(color: Color(0xffafafaf)),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      controller
                          .likePost(model.id ?? '', model.isLike == 1 ? 0 : 1,
                              type: 2)
                          .then((data) {
                        if (data) {
                          model.isLike = model.isLike == 1 ? 0 : 1;
                          if (model.isLike == 1) {
                            model.likeNum =
                                (int.parse(model.likeNum ?? '0') + 1)
                                    .toString();
                          } else {
                            model.likeNum =
                                (int.parse(model.likeNum ?? '0') - 1)
                                    .toString();
                          }
                          controller.comments[index] = model;
                          controller.comments.refresh();
                        }
                      });
                    },
                    child: Row(
                      children: [
                        Icon(
                          CustomIcons.thumbs_up,
                          color: (model.isLike != null && model.isLike == 1)
                              ? Colors.red
                              : Colors.black,
                          size: 20.w,
                        ),
                        Text(
                          ' ${(model.likeNum == null || model.likeNum == '0') ? "" : model.likeNum}',
                          style: const TextStyle(color: Colors.black),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              // Container(
              //   margin: EdgeInsets.only(left: 60.w, top: 10.h),
              //   padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 3.w),
              //   decoration: BoxDecoration(
              //       color: AppColors.drawMenu,
              //       borderRadius: BorderRadius.circular(20.w)),
              //   child: const Text(
              //     '回复',
              //     style: TextStyle(color: Colors.white),
              //   ),
              // ),
              Column(
                children: [
                  ...(model.secReplyList ?? [])
                      .take(3)
                      .map<Widget>((res) => Column(
                            children: [
                              Row(
                                children: [
                                  // CircleAvatar(
                                  //   backgroundImage: NetworkImage(
                                  //     res['headImg'] == ''
                                  //         ? defaultAvatar
                                  //         : res['headImg'],
                                  //   ),
                                  //   radius: 10,
                                  // ),
                                  GestureDetector(
                                    onTap: () {
                                      // AppNavigator.toNamed(userProfilePath,
                                      //     parameters: {'uid': res.uid ?? ""});
                                    },
                                    child: AppImage.network(res.headImg ?? "",
                                        width: 30.w,
                                        height: 30.w,
                                        fit: BoxFit.cover),
                                  ),
                                  SizedBox(width: 5),
                                  Text(
                                    res.nickname ?? '昵称已被禁用 :',
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.blue,
                                    ),
                                  ),
                                  if (res.isAdmin == 1)
                                    AppImage.asset(
                                      'gly.png',
                                      width: 50.w,
                                      height: 20.h,
                                      fit: BoxFit.cover,
                                    ),
                                  if (res.isAuthor == 1)
                                    AppImage.asset(
                                      'lz.png',
                                      width: 50.w,
                                      height: 20.h,
                                      fit: BoxFit.cover,
                                    ),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 5, right: 40),
                                child: Text(
                                  '${res.content ?? ''}',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ],
                          )),
                  if ((model.secReplyList ?? []).length > 3)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 10),
                      child: Text('查看全部${model.replyCount ?? ""}条回复 >'),
                    ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10.h),
                height: 10.h,
                color: const Color(0xfff9f9f9),
              )
            ],
          ),
        ),
      ),
    );
  }
}
