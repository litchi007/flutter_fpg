import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

class LongDragonPage extends StatefulWidget {
  const LongDragonPage({super.key});

  @override
  State<LongDragonPage> createState() => _LongDragonPageState();
}

class _LongDragonPageState extends State<LongDragonPage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  final AuthController authController = Get.find();

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        key: _homeKey,
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "长龙助手",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ]),
          actionWidgets: [
            GlobalService.to.isAuthenticated.value ? _showMenu() : Container()
          ],
        ),
        endDrawer: GlobalService.to.isAuthenticated.value
            ? Container(
                color: Colors.white,
                width: Get.width * 0.5,
                height: Get.height,
                child: const HomeMenuWidget(),
              )
            : null,
        body: Container());
  }

  Widget _showMenu() {
    return GestureDetector(
        onTap: () {
          _homeKey.currentState?.openEndDrawer();
        },
        child: Container(
          padding: EdgeInsets.only(right: 10.w),
          child:
              AppImage.asset('menu_btn_white.png', width: 30.w, height: 30.w),
        ));
  }
}
