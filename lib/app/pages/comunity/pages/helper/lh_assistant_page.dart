import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lh_gallery_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

class LhAssistantPage extends StatefulWidget {
  const LhAssistantPage({super.key});

  @override
  State<LhAssistantPage> createState() => _LhAssistantPageState();
}

class _LhAssistantPageState extends State<LhAssistantPage> {
  LhGalleryController controller = Get.find();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.getQueryAssis();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "挑码助手",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ]),
        ),
        body: Container());
  }
}
