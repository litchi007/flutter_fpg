import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';

class LhLittleHelperPage extends StatefulWidget {
  const LhLittleHelperPage({super.key});

  @override
  State<LhLittleHelperPage> createState() => _LhLittleHelperPageState();
}

class _LhLittleHelperPageState extends State<LhLittleHelperPage> {
  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "查询助手",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ]),
        ),
        body: Column(children: [
          AppImage.asset("tiaoma.png", fit: BoxFit.fitWidth),
          Row(children: [
            Expanded(
                flex: 1,
                child: _getWidgetButton("tmzs.png", "挑码助手", lhAssistantPath)),
            Expanded(
                flex: 1,
                child: _getWidgetButton("lrhm.png", "冷热号码查询", lhNumberPath))
          ]),
          Row(children: [
            Expanded(
                flex: 1,
                child: _getWidgetButton("lhcs.png", "六合常识查询", lhCommonPath)),
            Expanded(
                flex: 1, child: _getWidgetButton("sxlm.png", "生肖灵码表", lhLMPath))
          ])
        ]));
  }

  Widget _getWidgetButton(String icon, String label, String path) {
    return GestureDetector(
        onTap: () => AppNavigator.toNamed(path),
        child: Container(
          padding: EdgeInsets.all(20.w),
          decoration: BoxDecoration(
              border: Border.all(color: AppColors.ff999999, width: 1.h)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AppImage.asset(icon, width: 100.w),
                Gap(20.h),
                Text(label, style: TextStyle(fontSize: 22.sp))
              ]),
        ));
  }
}
