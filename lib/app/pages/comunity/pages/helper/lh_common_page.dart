import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lh_gallery_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/data/models/QueryAssisModel.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class LhCommonPage extends StatefulWidget {
  const LhCommonPage({super.key});

  @override
  State<LhCommonPage> createState() => _LhCommonPageState();
}

class _LhCommonPageState extends State<LhCommonPage>
    with TickerProviderStateMixin {
  final List<Tab> tabs = <Tab>[
    const Tab(text: '生肖号码'),
    const Tab(text: '波色号码')
  ];
  late TabController _tabController;
  LhGalleryController controller = Get.find();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
    controller.getQueryAssis();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "六合常识查询",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ]),
        ),
        body: Column(children: [
          Container(
              color: AppColors.ffe9e9e9,
              child: TabBar(
                controller: _tabController,
                physics: const ClampingScrollPhysics(),
                indicatorSize: TabBarIndicatorSize.tab,
                tabAlignment: TabAlignment.start,
                isScrollable: true,
                labelStyle: TextStyle(fontSize: 20.sp),
                indicator: null,
                labelColor: AppColors.drawMenu,
                indicatorColor: AppColors.drawMenu,
                tabs: tabs,
              )),
          Expanded(
            child: TabBarView(
                controller: _tabController,
                physics: const NeverScrollableScrollPhysics(),
                children: [getItemZodaic(), getItemColur()]),
          )
        ]));
  }

  Widget getItemZodaic() {
    return Obx(() => ListView.separated(
        itemBuilder: (BuildContext context, int index) {
          String key = controller.lhcItemZodaic.keys.elementAt(index);
          MixBallModel item = controller.lhcItemZodaic.values.elementAt(index);
          List<String> nums = item.nums.split(',');
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child: Row(children: [
              Text(key, style: TextStyle(fontSize: 22.sp)),
              Gap(5.w),
              ...List.generate(nums.length, (idx) {
                String ball = "green.png";
                if (RedNum.contains(int.parse(nums[idx]))) {
                  ball = "red.png";
                } else if (BlueNum.contains(int.parse(nums[idx]))) {
                  ball = "blue.png";
                }
                return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5.w),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        AppImage.asset("ball/$ball", width: 45.w, height: 45.w),
                        Positioned(
                            top: 6.h,
                            left: 8.w,
                            child: Text(nums[idx],
                                style: TextStyle(
                                    fontSize: 20.sp,
                                    fontWeight: FontWeight.bold)))
                      ],
                    ));
              })
            ]),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider(height: 0.h);
        },
        itemCount: controller.lhcItemZodaic.length));
  }

  Widget getItemColur() {
    return Obx(() => ListView.separated(
        itemBuilder: (BuildContext context, int index) {
          String key = controller.lhcItemColur.keys.elementAt(index);
          MixBallModel item = controller.lhcItemColur.values.elementAt(index);
          List<String> nums = item.nums.split(',');
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(key, style: TextStyle(fontSize: 20.sp)),
              Gap(5.w),
              Wrap(
                children: [
                  ...List.generate(nums.length, (idx) {
                    String ball = "green.png";
                    if (RedNum.contains(int.parse(nums[idx]))) {
                      ball = "red.png";
                    } else if (BlueNum.contains(int.parse(nums[idx]))) {
                      ball = "blue.png";
                    }
                    return Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 3.w, vertical: 3.h),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            AppImage.asset("ball/$ball",
                                width: 45.w, height: 45.w),
                            Positioned(
                                top: 6.h,
                                left: 8.w,
                                child: Text(nums[idx],
                                    style: TextStyle(
                                        fontSize: 20.sp,
                                        fontWeight: FontWeight.bold)))
                          ],
                        ));
                  })
                ],
              )
            ]),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider(height: 0.h);
        },
        itemCount: controller.lhcItemColur.length));
  }
}
