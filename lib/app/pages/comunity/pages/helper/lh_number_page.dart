import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lh_gallery_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/data/models/ColdHotModel.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class LhNumberPage extends StatefulWidget {
  const LhNumberPage({super.key});

  @override
  State<LhNumberPage> createState() => _LhNumberPageState();
}

class _LhNumberPageState extends State<LhNumberPage>
    with TickerProviderStateMixin {
  final List<Tab> tabs = <Tab>[
    const Tab(text: '热号排序'),
    const Tab(text: '冷号排序')
  ];
  late TabController _tabController;
  LhGalleryController controller = Get.find();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
    controller.getColdHot();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "冷热号码查询",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ]),
          actionWidgets: [
            Container(
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
                margin: EdgeInsets.symmetric(horizontal: 10.w),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.w)),
                    color: AppColors.ffcf352e),
                child: Text("50期",
                    style: TextStyle(color: Colors.white, fontSize: 20.sp)))
          ],
        ),
        body: Column(children: [
          Container(
              color: AppColors.ffe9e9e9,
              child: TabBar(
                controller: _tabController,
                physics: const ClampingScrollPhysics(),
                indicatorSize: TabBarIndicatorSize.tab,
                tabAlignment: TabAlignment.start,
                isScrollable: true,
                labelStyle: TextStyle(fontSize: 20.sp),
                indicator: null,
                labelColor: AppColors.drawMenu,
                indicatorColor: AppColors.drawMenu,
                tabs: tabs,
              )),
          Expanded(
            child: TabBarView(
                controller: _tabController,
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  showColdHotList(),
                  showColdHotList(isReverse: true)
                ]),
          )
        ]));
  }

  Widget showColdHotList({bool isReverse = false}) {
    return Obx(() => ListView.separated(
        itemBuilder: (BuildContext context, int index) {
          ColdHotModel item = controller.lhcColdHot.value[index];
          if (isReverse)
            item = controller
                .lhcColdHot.value[controller.lhcColdHot.length - index - 1];
          String ball = "green.png";
          if (RedNum.contains(int.parse(item.number))) {
            ball = "red.png";
          } else if (BlueNum.contains(int.parse(item.number))) {
            ball = "blue.png";
          }
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            child: Row(children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  AppImage.asset("ball/$ball", width: 45.w, height: 45.w),
                  Positioned(
                      top: 6.h,
                      left: 8.w,
                      child: Text(item.number,
                          style: TextStyle(
                              fontSize: 20.sp, fontWeight: FontWeight.bold)))
                ],
              ),
              Gap(10.w),
              Text("该号码当前出现次数：", style: TextStyle(fontSize: 20.sp)),
              Text("${item.count}",
                  style: TextStyle(fontSize: 20.sp, color: Colors.red)),
              Text("次", style: TextStyle(fontSize: 20.sp)),
            ]),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider(height: 0.h);
        },
        itemCount: controller.lhcColdHot.value.length));
  }
}
