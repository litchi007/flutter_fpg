import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/component/live_lottery/lottery_ball.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lh_gallery_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/components/custom.btn.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/LhcNoListItemModel.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/page/expertPlanBet/expertPlanBet.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';

class LhkPostDetailView extends StatefulWidget {
  const LhkPostDetailView({super.key});

  @override
  State<LhkPostDetailView> createState() => _LhkPostDetailViewViewState();
}

class _LhkPostDetailViewViewState extends State<LhkPostDetailView>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey(); // Create
  final LhGalleryController controller = Get.find();
  final AuthController authController = Get.find();
  late ListObserverController observerController1;
  final ScrollController _scrollController1 = ScrollController();
  late GridObserverController observerController2;
  final ScrollController _scrollController2 = ScrollController();
  final RxBool _offstage = true.obs;
  final RxBool _isLastLotteyCheck = false.obs;

  String type2 = "", title = "";

  @override
  void initState() {
    super.initState();
    print("args_____${Get.arguments}");
    observerController1 =
        ListObserverController(controller: _scrollController1);
    observerController2 =
        GridObserverController(controller: _scrollController2);
    List<String?> args = Get.arguments ?? [];
    type2 = args[0] ?? "";
    title = args[1] ?? "";
    controller.alias = args[2] ?? "";
    if (args.length > 3) {
      controller.cid = args[3] ?? "";
    }
    if (args.length > 4) {
      controller.extra = args[4] ?? "";
    }
    if (args.length > 5) {
      controller.from = args[5] ?? "";
    }
    controller.getPostDetail(type2);
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();

    return Scaffold(
      key: _homeKey,
      // resizeToAvoidBottomInset: false,
      backgroundColor: appThemeColors?.surface,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Container(
              padding: EdgeInsets.only(left: 10.w),
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.ffffffff, size: 18.w),
                Text('返回',
                    style: AppTextStyles.ffffffff(context, fontSize: 18)),
              ]),
            )),
        titleWidget: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        actionWidgets: [
          Row(
            children: [
              Obx(
                () => CustomButton(
                    margin: const EdgeInsets.only(left: 10),
                    onTap: () async {
                      if (checkIsTest()) return;
                      if (!GlobalService.to.isAuthenticated.value) {
                        AppNavigator.toNamed(loginPath);
                      }
                      int favFlag =
                          (controller.contentDetail.value.isFav != null &&
                                  controller.contentDetail.value.isFav == 1)
                              ? 0
                              : 1;
                      bool result = await controller.doFavorites(
                          controller.contentDetail.value.id ?? "", favFlag);
                      if (result) {
                        await controller.getContentDetail(showLoading: false);
                      }
                    },
                    child: Text(
                      (controller.contentDetail.value.isFav != null &&
                              controller.contentDetail.value.isFav == 1)
                          ? "取消收藏"
                          : "收藏",
                    )),
              ),
              GlobalService.to.isAuthenticated.value ? _showMenu() : Container()
            ],
          )
        ],
      ),
      endDrawer: GlobalService.to.isAuthenticated.value
          ? Container(
              color: Colors.white,
              width: Get.width * 0.5,
              height: Get.height,
              child: const HomeMenuWidget(),
            )
          : null,
      body: Obx(
        () => Column(
          children: _getBody(),
        ),
      ),
    );
  }

  List<Widget> _getBody() {
    if (controller.isLoading2.value) {
      return [
        const Expanded(
            child: Center(
          child: CircularProgressIndicator(),
        ))
      ];
    } else {
      if (controller.yearResult.isEmpty) {
        return [Expanded(child: AppImage.asset('pl.png'))];
      } else {
        return [
          Container(
              height: 50.h,
              width: Get.width,
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              decoration: BoxDecoration(
                  border: Border(
                      bottom:
                          BorderSide(color: AppColors.ff999999, width: 1.w))),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                    children:
                        List.generate(controller.yearResult.length, (index) {
                  String y = controller.yearResult.value[index];
                  return GestureDetector(
                      onTap: () {
                        controller.curYear.value = y;
                        controller.getLhcNoListItem();
                        observerController1.jumpTo(index: 0);
                        observerController2.jumpTo(index: 0);
                      },
                      child: Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 5.w, vertical: 6.h),
                          padding: EdgeInsets.symmetric(horizontal: 10.w),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.w),
                              color: controller.curYear.value == y
                                  ? AppColors.drawMenu
                                  : Colors.white,
                              border: Border.all(
                                  color: controller.curYear.value == y
                                      ? AppColors.drawMenu
                                      : AppColors.ff666666)),
                          child: Text('$y年',
                              style: TextStyle(
                                  color: controller.curYear.value == y
                                      ? Colors.white
                                      : AppColors.ff666666,
                                  fontSize: 20.sp))));
                })),
              )),
          Container(
              width: Get.width,
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              decoration: BoxDecoration(
                  border: Border(
                      bottom:
                          BorderSide(color: AppColors.ff999999, width: 1.w))),
              child: Column(
                children: [
                  SizedBox(
                    height: 50.h,
                    child: Row(
                      children: [
                        Expanded(
                            child: ListViewObserver(
                                controller: observerController1,
                                child: ListView(
                                  controller: _scrollController1,
                                  scrollDirection: Axis.horizontal,
                                  children: List.generate(
                                      controller.lhcNoListItem.length, (index) {
                                    LhcNoListItemModel item =
                                        controller.lhcNoListItem.value[index];
                                    return GestureDetector(
                                        onTap: () {
                                          controller.curListItem.value = item;
                                          controller.getContentDetail();
                                          observerController2.jumpTo(
                                              index: index);
                                        },
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                left: 5.w,
                                                right: 5.w,
                                                top: 6.h,
                                                bottom: 1.h),
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.w),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color: controller
                                                                    .curListItem
                                                                    .value
                                                                    .id ==
                                                                item.id
                                                            ? AppColors.drawMenu
                                                            : Colors.white,
                                                        width: 3.w))),
                                            child: Text('${item.fullLhcNo!.replaceAll(controller.curYear.value, "")}期',
                                                style: TextStyle(
                                                    color: controller
                                                                .curListItem
                                                                .value
                                                                .id ==
                                                            item.id
                                                        ? AppColors.drawMenu
                                                        : AppColors.ff666666,
                                                    fontSize: 20.sp))));
                                  }),
                                ))),
                        GestureDetector(
                            onTap: () => _offstage.value = !_offstage.value,
                            child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 5.w, vertical: 8.h),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5.w, vertical: 3.h),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20.w),
                                  color: AppColors.drawMenu,
                                ),
                                child: Row(children: [
                                  Text('请选择',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.sp)),
                                  Icon(Icons.arrow_drop_down_rounded,
                                      color: Colors.white, size: 20.w)
                                ])))
                      ],
                    ),
                  ),
                  Offstage(
                      offstage: _offstage.value,
                      child: Container(
                          constraints: BoxConstraints(maxHeight: 300.h),
                          // height: 300.h,
                          child: GridViewObserver(
                              controller: observerController2,
                              child: GridView.count(
                                controller: _scrollController2,
                                crossAxisCount: 4,
                                primary: false,
                                shrinkWrap: true,
                                childAspectRatio: 3,
                                mainAxisSpacing: 15.w,
                                crossAxisSpacing: 15.w,
                                children: List.generate(
                                    controller.lhcNoListItem.length, (index) {
                                  LhcNoListItemModel item =
                                      controller.lhcNoListItem.value[index];
                                  return GestureDetector(
                                      onTap: () {
                                        controller.curListItem.value = item;
                                        controller.getContentDetail();
                                        _offstage.value = true;
                                        observerController1.jumpTo(
                                            index: index);
                                      },
                                      child: Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: controller.curListItem
                                                            .value.id ==
                                                        item.id
                                                    ? AppColors.drawMenu
                                                    : AppColors.ff666666),
                                            borderRadius:
                                                BorderRadius.circular(5.w),
                                            color: controller
                                                        .curListItem.value.id ==
                                                    item.id
                                                ? AppColors.drawMenu
                                                : Colors.white,
                                          ),
                                          child: Text(
                                              '${item.fullLhcNo!.replaceAll(controller.curYear.value, "")}期',
                                              style: TextStyle(
                                                color: controller.curListItem
                                                            .value.id ==
                                                        item.id
                                                    ? Colors.white
                                                    : AppColors.ff666666,
                                              ))));
                                }),
                              ))))
                ],
              )),
          Expanded(
            child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 10.w),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _getLHCWidget(),
                    Gap(10.h),
                    (controller.contentDetail.value.contentPic!.isNotEmpty &&
                            controller.contentDetail.value.contentPic?[0] != "")
                        ? AppImage.network(
                            controller.contentDetail.value.contentPic?[0] ?? "")
                        : HtmlWidget(
                            controller.contentDetail.value.content ?? ""),
                    Gap(10.h),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                            "最后更新时间：${controller.contentDetail.value.createTime}",
                            style: TextStyle(
                                color: AppColors.ff999999, fontSize: 18.sp))),
                    Gap(10.h),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        decoration: const BoxDecoration(
                            border: Border(
                                bottom:
                                    BorderSide(width: 1, color: Colors.black))),
                        padding: EdgeInsets.only(bottom: 10.h),
                        child: Text("全部评论",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold)),
                      )
                    ]),
                    // if(controller.contentDetail.value.)
                    AppImage.asset("pl.png", width: 400.w),
                    Text("还没有评论，你的机会来了！"),
                    Text("没有更多了"),
                  ],
                )),
          ),
          Container(
            color: AppColors.drawMenu,
            padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 10.w),
            child: IntrinsicHeight(
              child: Row(
                children: [
                  AppImage.asset("yhj.png", width: 50.w),
                  SizedBox(width: 5.w),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      if (checkIsTest()) return;
                      if (!GlobalService.to.isAuthenticated.value) {
                        AppNavigator.toNamed(loginPath);
                      }
                      AppNavigator.toNamed(postConentReply);
                    },
                    child: Container(
                        height: 40.h,
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        alignment: Alignment.centerLeft,
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.w)),
                            color: Colors.white),
                        child: const Text(
                          '说点什么...',
                          style: TextStyle(color: Colors.grey),
                        )),
                  )),
                  SizedBox(width: 5.w),
                  GestureDetector(
                    onTap: () async {
                      if (checkIsTest()) return;
                      if (!GlobalService.to.isAuthenticated.value) {
                        AppNavigator.toNamed(loginPath);
                      }
                      int likeFlag =
                          (controller.contentDetail.value.isLike != null &&
                                  controller.contentDetail.value.isLike == 1)
                              ? 0
                              : 1;
                      bool result = await controller.likePost(
                          controller.contentDetail.value.id ?? "", likeFlag);
                      if (result) {
                        controller.getContentDetail(showLoading: false);
                      }
                    },
                    child: Icon(CustomIcons.thumbs_up,
                        color: (controller.contentDetail.value.isLike != null &&
                                controller.contentDetail.value.isLike == 1)
                            ? Colors.red
                            : Colors.white),
                  ),
                  if (controller.contentDetail.value.likeNum != null)
                    Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        controller.contentDetail.value.likeNum ?? "",
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  SizedBox(width: 10.w),
                  GestureDetector(
                    onTap: () {
                      if (checkIsTest()) return;
                      if (!GlobalService.to.isAuthenticated.value) {
                        AppNavigator.toNamed(loginPath);
                      }
                      AppNavigator.toNamed(postConentReply);
                    },
                    child: const Icon(CustomIcons.commenting_o,
                        color: Colors.white),
                  ),
                  if (controller.contentDetail.value.replyCount != null)
                    Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        controller.contentDetail.value.replyCount ?? "",
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  SizedBox(width: 10.w),
                  GestureDetector(
                    onTap: () async {
                      if (checkIsTest()) return;
                      if (!GlobalService.to.isAuthenticated.value) {
                        AppNavigator.toNamed(loginPath);
                      }
                      int favFlag =
                          (controller.contentDetail.value.isFav != null &&
                                  controller.contentDetail.value.isFav == 1)
                              ? 0
                              : 1;
                      bool result = await controller.doFavorites(
                          controller.contentDetail.value.id ?? "", favFlag);
                      if (result) {
                        await controller.getContentDetail(showLoading: false);
                      }
                    },
                    child: Icon(FontAwesomeIcons.heart,
                        color: (controller.contentDetail.value.isFav != null &&
                                controller.contentDetail.value.isFav == 1)
                            ? Colors.red
                            : Colors.white),
                  ),
                  if (controller.contentDetail.value.favNum != null)
                    Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        controller.contentDetail.value.favNum.toString(),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ];
      }
    }
  }

  Widget _showMenu() {
    return GestureDetector(
        onTap: () {
          _homeKey.currentState?.openEndDrawer();
        },
        child: Container(
          padding: EdgeInsets.only(right: 10.w),
          child:
              AppImage.asset('menu_btn_white.png', width: 30.w, height: 30.w),
        ));
  }

  Widget _getLHCWidget() {
    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/icon_border.png'),
            fit: BoxFit.fill, // Makes sure the image covers the whole area
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                // onTap: ()=>AppNavigator.toNamed(lotteryHistoryPath, arguments: [controller.lotteryNumber.value.gameId]),
                onTap: () => Get.toNamed(AppRoutes.lotteryResult,
                    arguments: controller.lotteryNumber.value.gameId),
                child: Text(
                  '历史记录>',
                  style: TextStyle(
                      color: const Color(0xff666666), fontSize: 15.sp),
                ),
              ),
              Row(
                children: [
                  AppImage.network(IconResource.sound01(),
                      width: 21.w, height: 21.w),
                  SizedBox(width: 10.w),
                  Row(children: [
                    Text('第',
                        style: TextStyle(
                            fontSize: 20.sp,
                            color: const Color(0xff333333),
                            fontWeight: FontWeight.bold)),
                    Obx(
                      () => Text(
                          getLotteryNoOrIssueSubstring(
                              controller.lotteryNumber.value),
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.red,
                              fontWeight: FontWeight.bold)),
                    ),
                    Text('期开奖结果',
                        style: TextStyle(
                            fontSize: 20.sp,
                            color: const Color(0xff333333),
                            fontWeight: FontWeight.bold)),
                  ]),
                ],
              ),
              Row(children: [
                Text('咪',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold)),
                SizedBox(width: 10.w),
                FlutterSwitch(
                  width: 50.h,
                  height: 30.h,
                  value: _isLastLotteyCheck.value,
                  activeColor: const Color(0xff11c1f3),
                  onToggle: (bool value) => _isLastLotteyCheck.value = value,
                )
              ])
            ],
          ),
          Padding(
            padding: EdgeInsets.all(10.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ...List.generate(controller.showlotterys.length, (index) {
                  final item = controller.showlotterys[index];
                  return LotteryBallWidget(
                    score: item.number ?? '',
                    color: item.color ?? '',
                    text: item.sx ?? '',
                    isLastItem: index == 6,
                    isEnableLastItem: _isLastLotteyCheck.value,
                    onChange: (value) {
                      _isLastLotteyCheck.value = value;
                    },
                  );
                }),
              ],
            ),
          ),
        ]));
  }

  String getLotteryNoOrIssueSubstring(LotteryNumberData lotteryNum) {
    if (lotteryNum.lhcdocLotteryNo != null &&
        lotteryNum.lhcdocLotteryNo!.isNotEmpty) {
      return lotteryNum.lhcdocLotteryNo!;
    } else if (lotteryNum.issue != null &&
        (lotteryNum.issue ?? '').length >= 3) {
      return (lotteryNum.issue ?? '')
          .substring((lotteryNum.issue ?? '').length - 3);
    } else {
      return ''; // or any default value you prefer
    }
  }
}
