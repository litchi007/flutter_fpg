import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/BannersData.dart';
import 'package:fpg_flutter/data/models/LhcdocCategoryModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/widgets/activity_reward_dialog.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class CommunityController extends GetxController {
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();

  RxList<SliderBanner> bannerPics = RxList();
  // RxList<String> marqueerList = RxList();
  RxList<LhcdocCategoryModel> categoryList = RxList();
  RxList<LhcdocCategoryModel> categories = RxList();
  RxString appLogo = ''.obs;
  RxString callId = '3651'.obs; //name:澳门十点半

  @override
  void onReady() {
    appLogo.value = AppDefine.systemConfig?.mobileLogo ?? '';

    _appLHCDocRepository.getCategoryList().then((data) {
      categories.value = data ?? [];
    });

    _appLHCDocRepository.lhcForum2().then((data) {
      bannerPics.value = data?.shequ?.banner?.list ?? [];
      categoryList.value = (data?.categoryList ?? []).sublist(3);
    });

    // _appSystemRepository.notices().then((data) {
    //   // homeNoticeData.value = data ?? HomeNoticeData();
    //   if (data == null) return;
    //   marqueerList.clear();
    //   marqueerList.addAll(data.scroll!
    //       .map((toElement) => CommonUtil.removeHTMLTag(toElement.title ?? ''))
    //       .toList());
    // });

    // TODO: implement onReady
    super.onReady();
  }

  void showNoticeDialog() {
    print('ok');
  }

  // void _showApplyDialog(BuildContext context, String? winApplyContent) {
  //   return ActivityRewardDialog(
  //           context: context,
  //           title: 'title',
  //           titleBackgroundColor: AppColors.surface,
  //           action: _action(context),
  //           htmlstring: (winApplyContent ?? ''))
  //       .dialogBuilder();
  // }

  String getCategoryId(LhcdocCategoryModel item) {
    int index = categories.indexWhere((category) =>
        category.name == item.name || category.alias == item.alias);
    if (index >= 0) {
      return categories[index].id ?? "";
    } else {
      return item.id ?? "";
    }
  }

  // Widget _action(BuildContext context) {
  //   return Padding(
  //     padding: const EdgeInsets.all(10),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: [
  //         GestureDetector(
  //           onTap: () {
  //             Get.back();
  //           },
  //           child: Container(
  //             padding: EdgeInsets.only(top: 8.w),
  //             height: 40.w,
  //             width: 0.25.sw,
  //             decoration: BoxDecoration(
  //               color: AppColors.surface,
  //               border: Border.all(
  //                 width: 1.sp,
  //               ),
  //             ),
  //             child: Text('删除',
  //                 textAlign: TextAlign.center,
  //                 style: AppTextStyles.ff000000(context, fontSize: 16)),
  //           ),
  //         ),
  //         SizedBox(
  //           width: 0.1.sw,
  //         ),
  //         GestureDetector(
  //           onTap: () {
  //             // controller.user_msg!.value.list!
  //             //     .removeAt(2);
  //             Get.back();
  //           },
  //           child: Container(
  //             height: 40.w,
  //             width: 0.25.sw,
  //             padding: EdgeInsets.only(top: 8.w),
  //             color: AppColors.ffbfa46d,
  //             child: Text('确定',
  //                 textAlign: TextAlign.center,
  //                 style: AppTextStyles.ff000000(context, fontSize: 16)),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
