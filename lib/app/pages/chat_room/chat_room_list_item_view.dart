import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatRoomData.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class ChatRoomListItemView extends StatefulWidget {
  ChatRoomListItemView({
    super.key,
    required this.item,
    required this.onPressed,
  });

  ChatRoomChatAry item;
  VoidCallback onPressed;
  @override
  State<ChatRoomListItemView> createState() => _ChatRoomListItemViewState();
}

class _ChatRoomListItemViewState extends State<ChatRoomListItemView> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.onPressed,
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 0.w, horizontal: 10.w),
            decoration: const BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: AppColors.ffC5CBC6, width: 1.0))),
            child: Row(
              children: [
                Expanded(
                    child: Text(
                  '${widget.item.roomName}',
                  overflow: TextOverflow.ellipsis,
                )),
                OutlinedButton(
                  onPressed: widget.onPressed,
                  style: OutlinedButton.styleFrom(
                      minimumSize: Size.zero,
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.w, vertical: 8.h)),
                  child: Text('进入',
                      style: TextStyle(
                          fontSize: 15.sp, color: AppColors.textColor3)),
                )
              ],
            )));
  }
}
