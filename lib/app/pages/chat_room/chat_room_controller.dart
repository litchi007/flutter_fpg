import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/pages/chat_room/socket/chat_web_socket_api.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatMessageModel.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatRoomData.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatTokenModel.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatUserInfoResModel.dart';
import 'package:fpg_flutter/data/models/NextIssueData.dart';
import 'package:fpg_flutter/data/models/RedBagGrabModel.dart';
import 'package:fpg_flutter/data/models/SendMsgTextModel.dart';
import 'package:flutter/services.dart';
import 'package:fpg_flutter/data/repositories/app_bet_chat_repository.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/page/lottery/components/champion_play.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/widgets/app_custom_dialog.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:image_picker/image_picker.dart';

class Message {
  final bool? isMine;
  final bool? isManager;
  final String? username;
  final String? account;
  final String? roomId;
  final String? uid;
  final int? avatarStatus;
  final String? messageCode;
  final String? roomName;

  Message({
    this.isMine,
    this.isManager,
    this.username,
    this.account,
    this.roomId,
    this.uid,
    this.avatarStatus,
    this.messageCode,
    this.roomName,
  });
}

class ChatProvider {
  bool isAdmin;
  bool isChatBan;
  ChatProvider({this.isAdmin = false, this.isChatBan = false});
}

enum eLoadingType { banned, bannedIP, operateNickname }

enum eStatus { disable, enable }

class ChatRoomController extends GetxController {
  final BuildContext? context = Get.context;

  final _authController = Get.find<AuthController>();
  final _mainHomeController = Get.find<MainHomeController>();
  final AppBetChatRepository _appBetChatRepository = AppBetChatRepository();
  final AppUserRepository _appUserRepository = AppUserRepository();
  RxList<ChatRoomChatAry> chatRoomList = RxList();
  late TabController? tabController;
  RxBool isAdmin = false.obs;
  String roomId = '0';
  RxInt chatType = 0.obs;
  late List<CarouselItem> carouselItems;
  late ChatWebSocketApi chatWebSocketApi;
  Rxn<ChatTokenModel> chatToken = Rxn();
  RxInt chatOnlineMemberCount = 0.obs;
  Rxn<ChatRoomChatAry> chatRoom = Rxn();
  RxList<NextIssueData> nextIssueData = RxList();
  RxList<ChatMessageModel> chatMessages = RxList();
  bool flag = false;
  RxList<String> chatImages = RxList();
  RxList<int> closeTimes = RxList();
  Rxn<RedBagGrabModel> redBagGrab = Rxn();

  Rx<ChatUserInfoResModel> chatUserInfo = ChatUserInfoResModel().obs;
  RxInt curImageIndex = 0.obs;
  int redBagPanelIndex = 0;
  RxString pageTitle = "聊天室列表".obs;
  Rx<Message> msg = Message(
    roomId: '',
    uid: '',
    avatarStatus: 0,
    isManager: false,
    username: '',
    messageCode: '',
    account: '',
    roomName: '',
    isMine: false,
  ).obs;
  Rx<ChatProvider> chatProvider = ChatProvider().obs;

  RxBool needToScroll = false.obs;
  final RxBool showRedbagPanel = false.obs;
  final RxBool showRedbagDetail = false.obs;
  final VoidCallback onRebuild;

  ChatRoomController(this.onRebuild);

  @override
  void onReady() {
    updateChatToken();
    _authController.getAvatarList();
    init();
    carouselItems = [
      CarouselItem(id: 0, label: "表情", iconData: CustomIcons.smile),
      CarouselItem(
          id: 1,
          label: "图片",
          iconData: CustomIcons.picture,
          onPressed: openImagePicker),
      CarouselItem(
          id: 2, label: "扫雷规则", iconData: CustomIcons.exclamation_circle),
      CarouselItem(
          id: 3,
          label: "在线客服",
          iconData: CustomIcons.user_1,
          onPressed: () => AppNavigator.toNamed(onlineServicePath)),
      CarouselItem(
          id: 4,
          label: "发红包",
          iconAsset: AppImage.asset("redBag.png", height: 40.w),
          onPressed: () {
            redBagPanelIndex = 0;
            showRedbagPanel.value = true;
          }),
      CarouselItem(
          id: 5,
          label: "红包扫雷",
          iconAsset: AppImage.asset("mineRedBag.png", height: 40.w),
          onPressed: () {
            redBagPanelIndex = 1;
            showRedbagPanel.value = true;
          }),
      CarouselItem(
          id: 6,
          label: "存款",
          iconData: CustomIcons.credit_card,
          onPressed: () => AppNavigator.toNamed(depositPath)),
      CarouselItem(
          id: 7,
          label: "取款",
          iconData: CustomIcons.yen_sign,
          onPressed: () => AppNavigator.toNamed(depositPath, arguments: [1])),
      CarouselItem(
          id: 8,
          label: "利息宝",
          iconData: CustomIcons.gg,
          onPressed: () => AppNavigator.toNamed(interestTreasurePath)),
      CarouselItem(
          id: 9,
          label: "每日签到",
          iconData: CustomIcons.star,
          onPressed: () => AppNavigator.toNamed(mobilePath)),
      CarouselItem(
          id: 10,
          label: "任务大厅",
          iconData: CustomIcons.tasks,
          onPressed: () => AppNavigator.toNamed(taskPath)),
      CarouselItem(
          id: 11,
          label: "会员中心",
          iconData: CustomIcons.user,
          onPressed: () {
            _mainHomeController.selectedPath.value = userPath;
            AppNavigator.back();
          }),
      CarouselItem(
          id: 12,
          label: "额度转换",
          iconData: CustomIcons.exchange_alt,
          onPressed: () => AppNavigator.toNamed(realtransPath)),
      CarouselItem(
          id: 13,
          label: "异常反馈",
          iconData: CustomIcons.exclamation_circle,
          onPressed: () => AppNavigator.toNamed(feedbackPath)),
      CarouselItem(
          id: 14,
          label: "口令红包",
          iconAsset: AppImage.asset("kl_radbag.png", height: 40.w),
          onPressed: () {
            redBagPanelIndex = 2;
            showRedbagPanel.value = true;
          })
    ];
    super.onReady();
  }

  void init() async {
    pageTitle.value = chatRoom.value?.roomName ?? "";
    initWebSocketApi();
    await updateChatToken();
    await getChatMessageRecord();
  }

  void initWebSocketApi() {
    chatWebSocketApi = ChatWebSocketApi(roomId: roomId, onListen: onReciveMsg);
  }

  void getUserInfoById(String? uid) async {
    _appBetChatRepository
        .getUserInfoById(token: AppDefine.userToken?.apiSid, uid: uid)
        .then((data) {
      if (data == null) return;
      chatUserInfo.value = data;
      // } else {
      //   AppToast.showError(msg: msg);
      // }
    });
  }

  Future<void> updateChatToken() async {
    await _appBetChatRepository
        .getChatToken(token: AppDefine.userToken?.apiSid, roomId: roomId)
        .then((data) {
      chatToken.value = data;

      chatProvider.value.isAdmin = chatToken.value?.isManager == null
          ? false
          : (chatToken.value?.isManager! is bool)
              ? (chatToken.value?.isManager ?? false)
              : (chatToken.value?.isManager! is String)
                  ? chatToken.value?.isManager ==
                      GlobalService.to.userInfo.value?.usr
                  : false;

      AppLogger.d(GlobalService.to.userInfo.value?.usr);
      // pageTitle.value = data?.roomName ?? "";
      chatRoomList.value = data?.chatAry ?? [];
      chatOnlineMemberCount.value =
          int.parse(data?.chatOnlineMemberCount ?? '0');
    });
  }

  void banned(String? uid, int? type) {
    _appBetChatRepository
        .banned(token: AppDefine.userToken?.apiSid, uid: uid, operate: type)
        .then((data) {
      if (data == null) return;
      // chatUserInfo.value = data;
      // } else {
      //   AppToast.showError(msg: msg);
      // }
    });
    Get.back();
  }

  void privateChat(String? uid) {
    // for (var item in chatMessages) {
    //   if (item.uid == uid || item.uid == GlobalService.to.userInfo.value?.uid) {
    //     temp.add(item);
    //   }
    // }
    // chatMessages.clear();
    // chatMessages.addAll(temp);
    roomId = uid ?? '';
    init();
  }

  void publicChat() {
    roomId = '0';
    init();
  }

  void operateNickname(String? uid, String? roomId) {
    _appBetChatRepository
        .operateNickname(
            token: AppDefine.userToken?.apiSid, uid: uid, roomId: roomId)
        .then((data) {
      if (data == null) return;
      getUserInfoById(uid);
      // chatUserInfo.value = data;
      // } else {
      //   AppToast.showError(msg: msg);
      // }
    });
    Get.back();
  }

  void bannedIp(String? uid, int? type, String? ip) {
    _appBetChatRepository
        .bannedIp(
            token: AppDefine.userToken?.apiSid, uid: uid, operate: type, ip: ip)
        .then((data) {
      if (data == null) return;
      // chatUserInfo.value = data;
      // } else {
      //   AppToast.showError(msg: msg);
      // }
    });
    Get.back();
  }

  void getMemberInfoById(String? uid) {
    _appBetChatRepository
        .getMemberInfoById(token: AppDefine.userToken?.apiSid, uid: uid)
        .then((data) {
      if (data == null) return;
      // } else {
      //   AppToast.showError(msg: msg);
      // }
    });
  }

  void updateNickname(String name) {
    String msg = "服务器错误";
    _appBetChatRepository
        .updateNickname(
            token: AppDefine.userToken?.apiSid, text: name, roomId: roomId)
        .then((data) {
      if (data != null) {
        if (data.code == 0) {
          AppToast.showSuccess(msg: "更改昵称成功");
          chatToken.value?.nickname = name;
          updateChatToken();
        } else {
          AppToast.showError(msg: data.msg);
        }
      } else {
        AppToast.showError(msg: data.msg);
      }
      // } else {
      //   AppToast.showError(msg: msg);
      // }
    });
  }

  void updateChatImages() {
    chatImages.value = [];
    for (int i = 0; i < chatMessages.length; i++) {
      ChatMessageModel item = chatMessages[i];
      if (item.dataType == "image") {
        chatImages.add(item.msg ?? "");
      }
    }
  }

  void setTabController(TabController tabController) {
    this.tabController = tabController;
    // this.tabController?.addListener(_onTabChanged);
  }

  Future<String?> showConfirmPassword(String? title) async {
    AppCustomDialog appCustomDialog = AppCustomDialog(
        title: title ?? "", hintText: "输入对应房间密码", textOk: "匹配密码");
    return await appCustomDialog.showTextInputDialog(context!);
  }

  Future<void> gotoChatRoom(ChatRoomChatAry item) async {
    if (item.password == null) {
      // Navigator.pushNamed(context, homePath, arguments: item);
      roomId = item.roomId ?? '0';
      chatRoom.value = item;
      // chatType = 0;
      tabController?.animateTo(1);
      init();
    } else {
      String password = await showConfirmPassword(item.roomName) ?? "";
      if (password != "") {
        if (password != item.password) {
          // final scaffold = ScaffoldMessenger.of(context);
          AppToast.showError(msg: '密码错误');
        } else {
          chatRoom.value = item;
          roomId = item.roomId ?? '0';
          // chatType = 1;
          tabController?.animateTo(1);
          init();
        }
      }
    }
  }

  Future<String?> showConfirmName() async {
    AppCustomDialog appCustomDialog = AppCustomDialog(
        title: "更改昵称",
        hintText: "请输入昵称",
        textOk: "确定",
        defaultText: chatToken.value?.nickname ?? '');
    return await appCustomDialog.showTextInputDialog(context!);
  }

  Future<void> modifyName() async {
    String name = await showConfirmName() ?? "";
    if (name.isNotEmpty) {
      updateNickname(name);
    }
  }

  Future<void> updateUserAvatar(String publicAvatarId, String url) async {
    _appUserRepository
        .updateUserAvatar(
            token: AppDefine.userToken?.apiSid, publicAvatarId: publicAvatarId)
        .then((data) {
      if (data != null) {
        if (data.code == 0) {
          AppToast.showSuccess(msg: "修改头像成功");
          GlobalService.to.userInfo.value?.avatar = url;
        } else {
          AppToast.showError(msg: data.msg);
        }
      }
    });
  }

  Future<void> getChatMessageRecord() async {
    chatMessages.value = [];
    await _appBetChatRepository
        .getChatMessageRecord(
            token: AppDefine.userToken?.apiSid,
            chatType: chatType.value,
            roomId: roomId,
            num: 100)
        .then((data) {
      if (data != null) {
        chatMessages.value = data;
        needToScroll.value = true;
        onRebuild.call();
        updateChatImages();
      }
    });
  }

  void getNextIssueData() async {
    List<String>? cronTypes = chatRoom.value?.cronDataTopType!.split(',');
    nextIssueData.value = [];
    closeTimes.value = [];

    if (cronTypes != null && cronTypes.isNotEmpty) {
      for (int i = 0; i < cronTypes.length; i++) {
        await __getNextIssueData(cronTypes[i]);
      }
    }
  }

  Future<void> __getNextIssueData(String id) async {
    var data = await _appBetChatRepository.getNextIssueData(id: id);
    if (data != null) {
      int index = nextIssueData.indexWhere((item) => item.id == data.id);
      if (index < 0) {
        nextIssueData.add(data);
        closeTimes.add(DateUtil.getCloseTimes(data.curCloseTime ?? "0"));
      }
    }
  }

  void onReciveMsg(dynamic message) {
    message = jsonDecode(message);
    if (message['code'] == "0001") {
      ChatMessageModel msg = ChatMessageModel.fromJson(message);
      int index = chatMessages
          .indexWhere((item) => item.messageCode == msg.messageCode);

      if (!(index > 0)) {
        chatMessages.add(msg);
        needToScroll.value = true;
        onRebuild.call();
        updateChatImages();
      }
    }
  }

  Future<dynamic> getBetBills() async {
    _appBetChatRepository
        .getBetBills(token: AppDefine.userToken?.apiSid)
        .then((data) {
      if (data.data == null) {
        AppToast.showToast(data.msg);
      } else {
        sendShareBetBillText(data.data);
      }
    });
  }

  Future<bool> getRedBag(String redBagId) async {
    redBagGrab.value = RedBagGrabModel();
    var data = await _appBetChatRepository.getRedBag(
        token: AppDefine.userToken?.apiSid, redBagId: redBagId);
    if (data != null) {
      redBagGrab.value = data;
      return true;
    } else {
      return false;
    }
  }

  Future<void> openImagePicker() async {
    final ImagePicker picker = ImagePicker();
    final XFile? pickedFile =
        await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      final File file = File(pickedFile.path);
      final bytes = file.readAsBytesSync();
      String base64Image = "data:image/png;base64, ${base64Encode(bytes)}";
      sendImageMsg(base64Image);
    }
  }

  void sendShareBetBillText(dynamic data) {
    var _data = data['data'];
    int totalAmount = _data['totalAmount'] ?? 0,
        totalBetMoney = _data['totalBetMoney'] ?? 0,
        totalBonus = _data['totalBonus'] ?? 0,
        totalResultMoney = _data['totalResultMoney'] ?? 0,
        userCount = _data['userCount'] ?? 0;
    int followBetStatStatus = 1;
    var statement = _data['statement'] ?? {followBetStatStatus: 1};

    var betUrl = {
      statement: statement,
      data: {
        totalAmount: totalAmount,
        totalBetMoney: totalBetMoney,
        totalBonus: totalBonus,
        totalResultMoney: totalResultMoney,
        userCount: userCount
      }
    };
    String msg =
        "投注金额：${totalBetMoney}，奖金：${totalBonus}，盈亏：${totalResultMoney}，跟投人数：${userCount}，跟投金额：${totalAmount}";

    var msgText = SendMsgTextModel(
        code: "R0002",
        dataType: "text",
        chatType: chatType.value,
        roomId: roomId,
        username: GlobalService.to.userInfo.value?.usr,
        level: GlobalService.to.userInfo.value?.curLevelInt.toString(),
        ip: GlobalService.to.userInfo.value?.clientIp,
        t: DateTime.now().millisecondsSinceEpoch.toString(),
        token: AppDefine.userToken?.apiSid,
        shareBillFlag: true,
        betUrl: jsonEncode(betUrl),
        msg: msg);
    chatWebSocketApi.send(jsonEncode(msgText.toJson()));
  }

  void sendTextMsg(String msg) {
    var data = SendMsgTextModel(
        code: "R0002",
        dataType: "text",
        chatType: chatType.value,
        roomId: roomId,
        username: GlobalService.to.userInfo.value?.usr,
        level: GlobalService.to.userInfo.value?.curLevelInt.toString(),
        ip: GlobalService.to.userInfo.value?.clientIp,
        t: DateTime.now().millisecondsSinceEpoch.toString(),
        token: AppDefine.userToken?.apiSid,
        msg: msg);

    if (data.chatType == 1) {
      data.chatUid = chatToken.value?.uid ?? "";
      data.chatName = chatToken.value?.nickname ?? "";
    }
    chatWebSocketApi.send(jsonEncode(data.toJson()));
  }

  void sendImageMsg(String msg) {
    var data = SendMsgTextModel(
        code: "R0002",
        dataType: "image",
        chatType: chatType.value,
        roomId: roomId,
        username: GlobalService.to.userInfo.value?.usr,
        level: GlobalService.to.userInfo.value?.curLevelInt.toString(),
        ip: GlobalService.to.userInfo.value?.clientIp,
        t: DateTime.now().millisecondsSinceEpoch.toString(),
        token: AppDefine.userToken?.apiSid,
        msg: msg);
    chatWebSocketApi.send(jsonEncode(data.toJson()));
  }

  void removeMsg(String messageCode) {
    AppLogger.d('asdf');
    var data = SendMsgTextModel(
      code: "0006",
      messageCode: messageCode,
      token: AppDefine.userToken?.apiSid,
    );
    chatWebSocketApi.send(jsonEncode(data.toJson()));
    Get.back();
  }

  void copy(String? account) {
    copyToClipboard(account ?? '');
    AppToast.showDuration(msg: '复制成功');
    Get.back();
  }

  void copyToClipboard(String text) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      print('Text copied to clipboard!');
    });
  }

  Future<void> createRedBag(
      {int? genre,
      String? amount,
      String? title,
      int? quantity,
      String? mineNumber,
      String? coinPwd}) async {
    _appBetChatRepository
        .createRedBag(
            token: AppDefine.userToken?.apiSid,
            genre: genre,
            amount: amount,
            roomId: roomId,
            title: title,
            quantity: quantity,
            mineNumber: mineNumber,
            coinPwd: coinPwd)
        .then((data) {
      if (data.data == null) {
        AppToast.showToast(data.msg);
      } else {
        AppToast.showSuccess(msg: "发送红包成功");
      }
    });
  }
}

class CarouselItem {
  int id = 0;
  String label = "";
  IconData? iconData;
  VoidCallback? onPressed;
  Widget? iconAsset;
  CarouselItem(
      {required this.id,
      required this.label,
      this.iconData,
      this.onPressed,
      this.iconAsset});
}
