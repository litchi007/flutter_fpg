import 'dart:async';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/chat_room/chat_room_controller.dart';
import 'package:fpg_flutter/app/pages/chat_room/chat_room_list_item_view.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/action_button_widget.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/expanding_action_button.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/redbag_details_view.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/send_redbag_panel.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/triangle_painter.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/components/custom.chat.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/data/models/AvatarModel.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatMessageModel.dart';
import 'package:fpg_flutter/data/models/NextIssueData.dart';
import 'package:fpg_flutter/data/models/RedBagModel.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/chat_get_token_model/chat_ary.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'dart:math' as math;
import 'package:fpg_flutter/app/pages/chat_room/widget/avatar_overlay_widget.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

import 'package:sliding_up_panel2/sliding_up_panel2.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

enum eLoadingType { close, operateNickname, operateAvatar, banned, bannedIP }

enum eStatus { disable, enable }

class ChatRoomPage extends StatefulWidget {
  const ChatRoomPage({super.key});

  @override
  State<StatefulWidget> createState() => _ChatRoomPageState();
}

class _ChatRoomPageState extends State<ChatRoomPage>
    with TickerProviderStateMixin {
  final _authController = Get.find<AuthController>();
  late TabController _tabController;
  late ChatRoomController chatRoomController =
      Get.put(ChatRoomController(_onRebuild));
  final PanelController _panelController = PanelController();
  final _textEditingController = TextEditingController();
  final _searchController = TextEditingController();
  final _scrollController = ScrollController();
  final _scrollController2 = ScrollController();

  final CarouselSliderController _buttonCarouselController =
      CarouselSliderController();
  RxInt curCarouselIndex = 0.obs;

  final RxBool _showEmojiPicker = false.obs;
  final RxBool _showRule = false.obs;
  final RxBool _showSearchBar = false.obs;
  final RxBool _showOffstage = false.obs;
  final RxBool _showActionButtons = false.obs;
  final RxBool _isPrivate = false.obs;
  bool _isOverlayVisible = false;

  void _toggleOverlay() {
    Get.back();
  }

  late final AnimationController _animationController;
  late final Animation<double> _expandAnimation;
  final CarouselSliderController _carouselController =
      CarouselSliderController();
  Offset fabPosition = Offset(450.w, 350.h);
  String? _selectedValue = '用户名';

  double offStageHeight = 300.w;
  bool isFABVisible = true; // Tracks FAB visibility
  bool _isDrag = false;
  final double _fabDy = 150.w;
  static const int duration = 700;
  static const int reverseDuration = 300;
  final RxString _curAvatar =
      ''.obs; //_authController.userInfo.value?.avatar??''.obs;
  final RxString _curAvatarId = '1'.obs;
  final bool _initialFabOpen = false;
  final double _fabDistance = 180.w;
  final List<ExpandingActionButton> _actionButtons = [];

  Timer? _timer;
  bool _isTimerRunning = false;

  final List<Tab> tabs = <Tab>[
    const Tab(text: '群组'),
    const Tab(text: '会话'),
    const Tab(text: '充值'),
    const Tab(text: '提现'),
    const Tab(text: '投注区')
  ];

  init() {
    _showRule.value = false;
    _showEmojiPicker.value = false;
    _showOffstage.value = false;
    _showActionButtons.value = false;
    _showSearchBar.value = false;
    _isPrivate.value = false;
    chatRoomController.showRedbagPanel.value = false;
    chatRoomController.showRedbagDetail.value = false;
  }

  _onTabTap() {
    init();

    if (_tabController.index == 2) {
      AppNavigator.toNamed(depositPath);
    } else if (_tabController.index == 3) {
      AppNavigator.toNamed(depositPath, arguments: [1]);
    }
    if (_tabController.index > 1) {
      int index = _tabController.previousIndex;
      setState(() {
        _tabController.index = index;
      });
    }
    if (_tabController.index == 0) {
      chatRoomController.pageTitle.value = "聊天室列表";
    }
    if (_tabController.index == 1) {
      chatRoomController.getNextIssueData();
      startTimer();
      chatRoomController.init();
    }
  }

  void _toggleActionButtons() {
    _showActionButtons.value = !_showActionButtons.value;
    setState(() {
      if (_showActionButtons.value) {
        _animationController.forward();
      } else {
        _animationController.reverse();
      }
    });
  }

  void _resetAvatar() {
    _curAvatar.value = GlobalService.to.userInfo.value?.avatar ?? '';
  }

  void startTimer() {
    if (_isTimerRunning) return;

    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      for (int i = 0; i < chatRoomController.closeTimes.length; i++) {
        chatRoomController.closeTimes[i]--;
        if (chatRoomController.closeTimes[i] < 0) {
          chatRoomController.closeTimes[i] = 0;
        }
      }
    });
    TimerManager().addTimer(_timer!);
    setState(() {
      _isTimerRunning = true;
    });
  }

  @override
  void initState() {
    super.initState();
    init();
    _tabController = TabController(length: tabs.length, vsync: this);
    _tabController.addListener(_onTabTap);
    _showActionButtons.value = _initialFabOpen;
    _animationController = AnimationController(
      value: _showActionButtons.value ? 1.0 : 0.0,
      duration: const Duration(milliseconds: duration),
      reverseDuration: const Duration(milliseconds: reverseDuration),
      vsync: this,
    );
    _expandAnimation = CurvedAnimation(
      curve: Curves.fastOutSlowIn,
      reverseCurve: Curves.easeOutQuad,
      parent: _animationController,
    );
    _resetAvatar();
  }

  void _onRebuild() {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _onScrollEndMessageList());
    WidgetsBinding.instance.ensureVisualUpdate();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // String formattedTime = getFormattedTime();
    chatRoomController.setTabController(_tabController);

    BorderRadiusGeometry radius = const BorderRadius.only(
      topLeft: Radius.circular(24.0),
      topRight: Radius.circular(24.0),
    );

    final appThemeColors = Theme.of(context).extension<AppThemeColors>();

    return SlidingUpPanel(
      controller: _panelController,
      backdropEnabled: true,
      minHeight: 0,
      maxHeight: MediaQuery.of(context).size.height / 2,
      onPanelOpened: () {
        _showActionButtons.value = false;
        setState(() {
          _animationController.reverse();
        });
      },
      panelBuilder: _slideUpPanel,
      body: Scaffold(
        backgroundColor: appThemeColors?.homeBackgroundColor,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: Row(children: [
            Gap(20.w),
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: const Icon(Icons.home, color: Colors.white),
            ),
          ]),
          titleWidget: Obx(
            () => Text(
              chatRoomController.pageTitle.value,
              style: const TextStyle(color: Colors.white),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        body: Container(
            decoration: const BoxDecoration(
                // image: DecorationImage(
                //   image: AssetImage('assets/images/bg_1.jpg'),
                //   fit: BoxFit.cover, // Makes sure the image covers the whole area
                // ),
                ),
            child: Column(children: [
              Container(
                  color: AppColors.ffe9e9e9,
                  child: TabBar(
                    controller: _tabController,
                    physics: const ClampingScrollPhysics(),
                    indicatorSize: TabBarIndicatorSize.tab,
                    tabAlignment: TabAlignment.fill,
                    labelStyle: TextStyle(fontSize: 20.sp),
                    indicator: null,
                    onTap: (v) {
                      // print(v);
                      // 进入投注区域
                      if (v == 4) {
                        Get.toNamed("${AppRoutes.lottery}/0");
                      }
                    },
                    labelColor: AppColors.drawMenu,
                    indicatorColor: AppColors.drawMenu,
                    tabs: tabs,
                  )),
              Expanded(
                child: TabBarView(
                    controller: _tabController,
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      chatRoomListPage(),
                      chatRoomPage(),
                      const SizedBox(),
                      const SizedBox(),
                      const SizedBox(),
                    ]),
              )
            ])),
      ),
      borderRadius: radius,
    );
  }

  Widget chatRoomListPage() {
    return Obx(
      () => Column(
        mainAxisAlignment: chatRoomController.chatRoomList.isEmpty
            ? MainAxisAlignment.center
            : MainAxisAlignment.start,
        children: [
          if (chatRoomController.chatRoomList.isEmpty &&
              chatRoomController.chatToken.value?.uid != null)
            AppImage.svgByAsset('empty.svg'),
          if (chatRoomController.chatRoomList.isNotEmpty)
            ...List.generate(chatRoomController.chatRoomList.length, (index) {
              final item = chatRoomController.chatRoomList[index];
              return ChatRoomListItemView(
                  item: item,
                  onPressed: () => chatRoomController.gotoChatRoom(item));
            }),
        ],
      ),
    );
  }

  Widget chatRoomPage() {
    // return Obx(
    //   () => Stack(children: [
    //     Column(
    //         mainAxisAlignment: MainAxisAlignment.start,
    //         mainAxisSize: MainAxisSize.min,
    //         crossAxisAlignment: CrossAxisAlignment.stretch,
    //         children: [
    //           SizedBox(
    //               width: MediaQuery.of(context).size.width,
    //               height: 180.w,
    //               child: getNextIssueBanner()),
    //           if (_isPrivate.value)
    //             Center(
    //                 child: Text('私聊清单',
    //                     style: AppTextStyles.ff000000_(context, fontSize: 22))),
    //           chatMessageList(),
    //           if (!_isPrivate.value) chatMessageEditText(),
    //           chatOffstage()
    //         ]),
    //     getChatOnlieUsers(),
    //     _buildTapToOpenFab(),
    //     if (_showRule.value) chatMineRule(),
    //     if (_showSearchBar.value) _searchBar(),
    //     if (_showActionButtons.value) _buildBackground(),
    //     ..._buildExpandingActionButtons(),
    //     if (chatRoomController.showRedbagPanel.value)
    //       SendRedbagPanel(tabIndex: chatRoomController.redBagPanelIndex),
    //     if (chatRoomController.showRedbagDetail.value) RedbagDetailsView()
    //   ]),
    // );
    final data = chatRoomController.chatRoom.value;
    return Obx(() => ChatArea(
        roomId: chatRoomController.roomId,
        data: ChatAry(
          isTalk: int.tryParse("${data?.isTalk}"),
          cronDataTopType: data?.cronDataTopType,
          placeholder: data?.placeholder,
        )));
  }

  void _showApplyDialog(ChatMessageModel item) {
    chatRoomController.getUserInfoById(item.uid);

    if (chatRoomController.chatProvider.value.isAdmin == true) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return ChatOverlay(
              visible: true,
              close: _toggleOverlay,
              isNickEnabled: true,
              item: item,
              chatProvider: chatRoomController.chatProvider.value,
              remove: () =>
                  chatRoomController.removeMsg(item.messageCode ?? ''),
              copy: () => chatRoomController.copy(item.username ?? ''),
              banned: (int type) => chatRoomController.banned(item.uid, type),
              bannedIP: (int type) =>
                  chatRoomController.bannedIp(item.uid, type, item.ip),
              operateNickname: (int? type) =>
                  chatRoomController.operateNickname(item.uid, item.roomId),
              privateChat: (uid) => chatRoomController.privateChat(uid),
              loadingType: 'close',
              status: 'disable',
            );
          });
    }
  }

  Widget getNextIssueBanner() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.only(left: 20.w),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [...getNextIssueItems()]),
    );
  }

  List<Widget> getNextIssueItems() {
    List<Widget> list = [];
    for (int i = 0; i < chatRoomController.nextIssueData.length; i++) {
      NextIssueData item = chatRoomController.nextIssueData.value[i];
      List<String> preNum = item.preNum?.split(',') ?? [];
      List<String> preResult = item.preResult?.split(',') ?? [];
      double width = 500.w;
      if (preNum.length > 7) {
        width = 530.w;
      }
      if (item.preNum != "" && preNum.isNotEmpty) {
        list.add(Container(
            width: width,
            height: 160.h,
            padding: EdgeInsets.symmetric(horizontal: 12.w),
            margin: EdgeInsets.only(right: 12.w),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      width: 80.w,
                      padding: EdgeInsets.only(right: 12.w),
                      child: AppImage.network(item.logo ?? '')),
                  Expanded(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(item.title ?? '',
                                  style: TextStyle(
                                      fontSize: 20.sp,
                                      fontWeight: FontWeight.bold)),
                              Text('第${item.preDisplayNumber ?? ''}期')
                            ]),
                        lotteryWidget(item, preNum, preResult),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Row(children: [
                                const Text('距离第'),
                                Text(item.displayNumber ?? '',
                                    style: const TextStyle(color: Colors.red))
                              ]),
                              Row(children: [
                                const Text('期截止还有'),
                                Text(
                                    DateUtil.displayCloseTime(
                                        chatRoomController.closeTimes[i]),
                                    style: const TextStyle(color: Colors.red))
                              ])
                            ])
                      ]))
                ])));
      }
    }
    return list;
  }

  Widget lotteryWidget(
      NextIssueData item, List<String> preNum, List<String> preResult) {
    if (item.gameType == 'lhc') {
      return Row(
        children: [
          ...List.generate(preNum.length - 1, (index) {
            return getBallItem(item, preNum[index], preResult[index]);
          }),
          const Text('+'),
          getBallItem(
              item, preNum[preNum.length - 1], preResult[preResult.length - 1])
        ],
      );
    } else if (item.gameType == 'cqssc' ||
        item.gameType == 'gd11x5' ||
        item.gameType == 'pk10' ||
        item.gameType == 'xyft' ||
        item.gameType == 'pk10nn') {
      return Row(
        children: [
          ...List.generate(preNum.length, (index) {
            BoxDecoration decoration;
            if (item.gameType == 'cqssc' || item.gameType == 'gd11x5') {
              decoration = BoxDecoration(
                borderRadius: BorderRadius.circular(30.w),
                color: AppColors.ff900909,
              );
            } else {
              decoration = BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5.w)),
                color: Color(SquareColors[int.parse(preNum[index]) % 10]),
              );
            }
            return Container(
                margin: EdgeInsets.all(3.w),
                child: Column(
                  children: [
                    Container(
                        width: 35.w,
                        height: 35.w,
                        alignment: Alignment.center,
                        decoration: decoration,
                        child: Text(int.parse(preNum[index]).toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold))),
                    Gap(7.w),
                    if (index < preResult.length)
                      Container(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.w))),
                          child: Text(preResult[index])),
                    if (index >= preResult.length)
                      SizedBox(
                        width: 35.w,
                        height: 27.w,
                      )
                  ],
                ));
          }),
        ],
      );
    } else if (item.gameType == 'jsk3') {
      return Row(
        children: [
          ...List.generate(preNum.length, (index) {
            int idx = int.parse(preNum[index]) % 7;
            if (idx == 0) {
              idx = 1;
            }
            return Container(
                margin: EdgeInsets.all(3.w),
                child: Column(
                  children: [
                    AppImage.asset('dice-color/$idx.png', width: 35.w),
                    Gap(7.w),
                    if (index < preResult.length)
                      Container(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.w))),
                          child: Text(preResult[index])),
                    if (index >= preResult.length)
                      Container(
                        width: 35.w,
                        height: 27.w,
                      )
                  ],
                ));
          }),
        ],
      );
    } else {
      return Row(
        children: [
          ...List.generate(preNum.length, (index) {
            return Container(
                margin: EdgeInsets.all(3.w),
                child: Column(
                  children: [
                    Text(preNum[index],
                        style: TextStyle(
                            color: AppColors.textColor4, fontSize: 18.sp)),
                    Gap(7.w),
                    if (index < preResult.length)
                      Container(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.w))),
                          child: Text(preResult[index])),
                    if (index >= preResult.length)
                      SizedBox(
                        width: 35.w,
                        height: 27.w,
                      )
                  ],
                ));
          }),
        ],
      );
    }
  }

  Widget getBallItem(NextIssueData item, String preNum, String preResult) {
    String iconName = 'red';
    bool isRed = item.redBalls?.contains(preNum) ?? false;
    bool isGreen = item.greenBalls?.contains(preNum) ?? false;
    bool isBlue = item.blueBalls?.contains(preNum) ?? false;
    if (isRed) iconName = 'red';
    if (isGreen) iconName = 'green';
    if (isBlue) iconName = 'blue';
    String ballPath = 'assets/images/ball/$iconName.png';
    Widget ball = Container(
        width: 35.w,
        height: 35.w,
        padding: EdgeInsets.only(top: 5.w, left: 4.w),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ballPath),
            fit: BoxFit.fill, // Makes sure the image covers the whole area
          ),
        ),
        child: Text(preNum,
            style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold)));

    return Container(
        margin: EdgeInsets.all(3.w),
        child: Column(
          children: [
            ball,
            Gap(7.w),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 4.w),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.all(Radius.circular(5.w))),
                child: Text(preResult))
          ],
        ));
  }

  Widget chatMessageList() {
    return Obx(() => Expanded(
          child: GestureDetector(
              onTap: () => _showOffstage.value = false,
              child: Container(
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/chatroom-bg.jpg'),
                      fit: BoxFit
                          .cover, // Makes sure the image covers the whole area
                    ),
                  ),
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.w),
                  child: SingleChildScrollView(
                    controller: _scrollController2,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: List.generate(
                            chatRoomController.chatMessages.length, (index) {
                          ChatMessageModel item =
                              chatRoomController.chatMessages[index];
                          if (chatRoomController.flag == false) {
                            chatRoomController.flag = true;
                          }

                          if (item.uid ==
                              GlobalService.to.userInfo.value?.uid) {
                            return getChatMessageMyItem(item);
                          } else {
                            return getChatMessageOtherItem(item);
                          }
                        })),
                  ))),
        ));
  }

  Widget getChatMessageOtherItem(ChatMessageModel item) {
    Color msgColor = AppColors.ff2089dc;
    if (item.dataType == "text" && item.betFollowFlag == true) {
      msgColor = AppColors.ff34650b;
    }
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 10.h),
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          CircleAvatar(
                  child: AppImage.network(item.avatar ?? item.avator ?? ""))
              .onTap(() => _showApplyDialog(item)),
          Gap(2.w),
          Container(
            width: 400.w,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(children: [
                Text(_getDisplayName(item),
                    style:
                        TextStyle(color: AppColors.ff4e57dd, fontSize: 16.sp)),
                Gap(3.w),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.w)),
                        color: AppColors.ff4ca0e2),
                    child: Text("VIP${item.level ?? "0"}",
                        style:
                            TextStyle(color: Colors.white, fontSize: 16.sp))),
                Gap(3.w),
                Container(
                    alignment: Alignment.center,
                    padding:
                        EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.w)),
                        color: const Color.fromARGB(255, 202, 201, 201)),
                    child: Text(item.time ?? "",
                        style: TextStyle(
                            color: AppColors.ff5C5869, fontSize: 14.sp))),
              ]),
              SizedBox(height: 5.h),
              Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Center(
                  child: CustomPaint(
                    painter: TrianglePainter(
                      strokeColor: msgColor,
                      strokeWidth: 0,
                      paintingStyle: PaintingStyle.fill,
                    ),
                    child: SizedBox(
                      height: 15.w,
                      width: 15.w,
                    ),
                  ),
                ),
                Container(
                    // padding: EdgeInsets.all(8.w),
                    constraints: BoxConstraints(maxWidth: Get.width - 110.w),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.w),
                          bottomRight: Radius.circular(10.w),
                          topRight: Radius.circular(10.w),
                        ),
                        color: msgColor),
                    child: _showMsgItem(item))
              ])
            ]),
          )
        ]));
  }

  Widget getChatMessageMyItem(ChatMessageModel item) {
    Color msgColor = AppColors.ff2089dc;
    String msg = item.msg ?? "";
    msg = msg.replaceAll("\\n", "\n").replaceAll("\\", "");
    if (item.dataType == "text" && item.betFollowFlag == true) {
      msgColor = AppColors.ff34650b;
    }
    if (item.dataType == "redBag") {
      msgColor = AppColors.ffbe8716;
    }
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 10.h),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                Row(children: [
                  Container(
                      alignment: Alignment.center,
                      padding:
                          EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.w)),
                          color: const Color.fromARGB(255, 202, 201, 201)),
                      child: Text(item.time ?? "",
                          style: TextStyle(
                              color: AppColors.ff5C5869, fontSize: 14.sp))),
                  SizedBox(width: 3.w),
                  Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 4.w),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.w)),
                          color: AppColors.ff4ca0e2),
                      child: Text("VIP${item.level ?? "0"}",
                          style:
                              TextStyle(color: Colors.white, fontSize: 16.sp))),
                  SizedBox(width: 3.w),
                  Text(_getDisplayName(item),
                      style: TextStyle(
                          color: AppColors.ff4e57dd, fontSize: 16.sp)),
                ]),
                SizedBox(height: 5.h),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Container(
                      constraints: BoxConstraints(maxWidth: Get.width - 110.w),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10.w),
                            bottomRight: Radius.circular(10.w),
                            topLeft: Radius.circular(10.w),
                          ),
                          color: msgColor),
                      child: _showMsgItem(item)),
                  Center(
                    child: CustomPaint(
                      painter: TrianglePainter(
                          strokeColor: msgColor,
                          strokeWidth: 0,
                          paintingStyle: PaintingStyle.fill,
                          isLeft: false),
                      child: SizedBox(
                        height: 15.w,
                        width: 15.w,
                      ),
                    ),
                  ),
                ])
              ]),
              SizedBox(width: 2.w),
              CircleAvatar(
                      child: AppImage.network(item.avatar ?? item.avator ?? ""))
                  .onTap(() => _showApplyDialog(item)),
            ]));
  }

  String _getDisplayName(ChatMessageModel item) {
    String displayName = item.username ?? "";
    String usernameBak = item.usernameBak ?? "";
    if (usernameBak.contains("****") && !displayName.contains("****")) {
      displayName = usernameBak;
    }
    return displayName;
  }

  Widget _showMsgItem(ChatMessageModel item) {
    String msg = item.msg ?? "";
    msg = msg.replaceAll("\\n", "\n").replaceAll("\\", "");
    List<Widget> widgets = [];
    if (item.dataType == "text") {
      widgets.add(Padding(
          padding: EdgeInsets.all(8.w),
          child: Container(
              // width: 350.w,
              constraints: BoxConstraints(maxWidth: 350.w),
              child: Text(msg,
                  style: TextStyle(color: Colors.white, fontSize: 16.sp),
                  softWrap: true))));
      if (item.betFollowFlag == true) {
        widgets.add(Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.w),
                  bottomRight: Radius.circular(10.w),
                ),
                color: AppColors.ff244707),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                    onPressed: () {},
                    child: Text("跟注",
                        style:
                            TextStyle(color: Colors.white, fontSize: 16.sp))),
                TextButton(
                    onPressed: () {},
                    child: Text("返回投注",
                        style:
                            TextStyle(color: Colors.white, fontSize: 16.sp))),
              ],
            )));
      } else if (item.shareBillFlag == true) {}
    } else if (item.dataType == "image") {
      widgets.add(GestureDetector(
          onTap: () {
            chatRoomController.curImageIndex.value =
                chatRoomController.chatImages.indexWhere((e) => e == item.msg);
            zoomImage();
          },
          child: Padding(
              padding: EdgeInsets.all(8.w),
              child: AppImage.network(item.msg ?? "",
                  width: 150.w, height: 150.h, fit: BoxFit.contain))));
    } else if (item.dataType == "redBag") {
      RedBagModel? redBag = item.redBag;

      List<String> genreTitles = ["普通红包", "扫雷红包", "口令红包"];
      widgets.add(Column(children: [
        GestureDetector(
          onTap: () async {
            if (redBag?.isGrab == 0) {
              bool data = await chatRoomController.getRedBag(redBag?.id ?? "");
              if (data == true) {
                showRedBagGrab();
                item.redBag?.isGrab = '1';
              }
            }
          },
          child: Padding(
            padding: EdgeInsets.all(8.w),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AppImage.asset("sao.png", width: 70.w),
                Gap(10.w),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(redBag?.title ?? "",
                        style: TextStyle(color: Colors.white, fontSize: 17.sp)),
                    Gap(5.h),
                    Text(redBag?.isGrab == 1 ? "已过期" : "点击领取",
                        style: TextStyle(color: Colors.white, fontSize: 15.sp))
                  ],
                ),
                Gap(10.w),
                if (redBag?.genre == "2")
                  AppImage.asset("slxxhdpi.png", width: 30.w)
              ],
            ),
          ),
        ),
        Container(
            alignment: Alignment.centerLeft,
            height: 30.h,
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            decoration: BoxDecoration(
                color: const Color.fromARGB(255, 148, 105, 18),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.w),
                    bottomRight: Radius.circular(10.w))),
            child: Text(genreTitles[int.parse(redBag?.genre ?? "0") - 1],
                style: TextStyle(color: Colors.white, fontSize: 15.sp)))
      ]));
    }
    return IntrinsicWidth(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: widgets,
    ));
  }

  Widget chatMessageEditText() {
    return Container(
        height: 65.h,
        decoration: BoxDecoration(
          color: AppColors.ffffffff,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 4,
              offset: const Offset(0, -2), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            iconButtonCirularBorder(onTap: () {
              _showOffstage.value = !_showOffstage.value;
              _showEmojiPicker.value = false;
            }, CustomIcons.rss_feed, iconSize: 25.0, rotate: 45),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.w, horizontal: 5.w),
                child: TextField(
                  controller: _textEditingController,
                  scrollController: _scrollController,
                  style: AppTextStyles.ff000000(context, fontSize: 17),
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: '请输入聊天内容',
                    hintStyle: AppTextStyles.ff969696(fontSize: 18),
                    filled: true,
                    fillColor: AppColors.ffE6E6E6,
                    contentPadding: EdgeInsets.only(left: 10.w, right: 10.w),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.w),
                    ),
                  ),
                ),
              ),
            ),
            iconButton(onTap: () {
              if (_textEditingController.text.isEmpty) return;
              chatRoomController.sendTextMsg(_textEditingController.text);
              _textEditingController.text = "";
            }, CustomIcons.paper_plane, iconSize: 30.w),
            if (!_showOffstage.value)
              iconButton(onTap: () {
                _showOffstage.value = true;
                _showEmojiPicker.value = false;
              }, CustomIcons.add_circle, iconSize: 35.w),
            if (_showOffstage.value)
              iconButton(onTap: () {
                _showOffstage.value = false;
                _showEmojiPicker.value = false;
              }, CustomIcons.minus_circle, iconSize: 35.w),
          ],
        ));
  }

  Widget iconButton(IconData iconData,
      {VoidCallback? onTap, double? iconSize}) {
    return Container(
      width: 60.w,
      height: 60.w,
      padding: EdgeInsets.symmetric(vertical: 10.w),
      child: Material(
        color: Colors.transparent,
        child: IconButton(
          padding: EdgeInsets.all(1.w),
          onPressed: () => onTap?.call(),
          icon: Icon(
            iconData,
            color: Colors.grey,
            size: iconSize,
          ),
        ),
      ),
    );
  }

  Widget iconButtonCirularBorder(IconData iconData,
      {VoidCallback? onTap, double? iconSize, double rotate = 0}) {
    return Container(
        margin: EdgeInsets.only(left: 15.w, right: 10.w),
        child: Material(
            color: Colors.white,
            shape: const CircleBorder(
                side: BorderSide(color: Colors.grey, width: 2)),
            child: Padding(
              padding: EdgeInsets.only(left: 4.w),
              child: Ink(
                  width: 50.w,
                  height: 50.w,
                  child: Transform.rotate(
                      angle: rotate * math.pi / 180,
                      child: IconButton(
                        icon: Icon(iconData, size: 30.w),
                        color: Colors.grey,
                        onPressed: () => onTap?.call(),
                      ))),
            )));
  }

  Widget chatOffstage() {
    return Offstage(
        offstage: !_showOffstage.value,
        child: Column(children: [
          if (_showEmojiPicker.value == false) getCarouselSlider(),
          if (_showEmojiPicker.value == true) getEmojiPicker(),
        ]));
  }

  Widget getCarouselSlider() {
    return Column(children: [
      CarouselSlider(
        options: CarouselOptions(
          height: offStageHeight,
          enlargeCenterPage: true,
          viewportFraction: 1,
          aspectRatio: 2.0,
          onPageChanged: (index, reason) => curCarouselIndex.value = index,
        ),
        items: [getCarouselItems(0), getCarouselItems(1)],
        carouselController: _buttonCarouselController,
      ),
      DotsIndicator(
        dotsCount: 2,
        position: curCarouselIndex.value,
        decorator: const DotsDecorator(
          color: Colors.grey, // Inactive color
          activeColor: Colors.black87,
        ),
      )
    ]);
  }

  GridView getCarouselItems(int pageIndex) {
    List<CarouselItem> lists = chatRoomController.carouselItems;
    int startIndex = pageIndex * 8;
    int endIndex =
        (startIndex + 7) > lists.length - 1 ? lists.length - 1 : startIndex + 7;
    List<Widget> itemViews = [];
    for (int i = startIndex; i <= endIndex; i++) {
      CarouselItem item = lists[i];
      Widget? widget;
      if (item.iconData != null) {
        widget =
            Icon(item.iconData, color: const Color(0xff8D9099), size: 40.w);
      } else {
        widget = item.iconAsset;
      }
      itemViews.add(GestureDetector(
          onTap: () {
            if (item.id == 0) {
              _showEmojiPicker.value = true;
            } else if (item.id == 2) {
              _showRule.value = true;
            } else {
              item.onPressed?.call();
            }
          },
          child: Column(children: [
            SizedBox(
              width: 75.w,
              height: 75.w,
              child: Container(
                  padding: EdgeInsets.all(18.w),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: widget),
            ),
            Gap(5.w),
            Center(child: Text(item.label))
          ])));
    }
    return GridView.count(
        primary: false,
        padding: EdgeInsets.all(20.w),
        crossAxisSpacing: 10.w,
        mainAxisSpacing: 10.w,
        crossAxisCount: 4,
        children: itemViews);
  }

  Widget getEmojiPicker() {
    return Stack(
      children: [
        EmojiPicker(
          textEditingController: _textEditingController,
          scrollController: _scrollController,
          config: Config(
            height: offStageHeight,
            checkPlatformCompatibility: true,
            emojiViewConfig: EmojiViewConfig(
              // Issue: https://github.com/flutter/flutter/issues/28894
              emojiSizeMax: 28 *
                  (foundation.defaultTargetPlatform == TargetPlatform.iOS
                      ? 1.2
                      : 1.0),
            ),
            swapCategoryAndBottomBar: true,
            skinToneConfig: const SkinToneConfig(),
            categoryViewConfig: const CategoryViewConfig(),
            bottomActionBarConfig:
                const BottomActionBarConfig(showBackspaceButton: false),
            searchViewConfig: const SearchViewConfig(),
          ),
        ),
        Positioned(
            top: -3.w,
            right: 3.w,
            child: IconButton(
                onPressed: () => _showEmojiPicker.value = false,
                icon: const Icon(CustomIcons.cancel_circled,
                    color: Colors.white)))
      ],
    );
  }

  List<ActionButton> getActionButtons() {
    return <ActionButton>[
      ActionButton(
          id: 1,
          label: '分享战绩',
          iconData: CustomIcons.share_square,
          onPressed: () {
            chatRoomController.getBetBills;
            if (_showActionButtons.value) {
              _toggleActionButtons();
            }
          }),
      ActionButton(
          id: 2,
          label: _isPrivate.value == true ? '聊天室' : '私聊',
          iconData: CustomIcons.comment_dots,
          onPressed: () {
            _isPrivate.value = !_isPrivate.value;
            chatRoomController.chatType.value = _isPrivate.value ? 1 : 0;
            setState(() {
              ExpandingActionButton a = _actionButtons[1];
              a.child.label = '聊天室';

              if (_showActionButtons.value) {
                _toggleActionButtons();
              }
            });
            if (chatRoomController.chatType.value == 1) {
              chatRoomController
                  .privateChat(GlobalService.to.userInfo.value?.uid);
            } else {
              chatRoomController.publicChat();
            }
          }),
      ActionButton(
          id: 3,
          label: '清屏',
          iconData: CustomIcons.window_maximize,
          onPressed: () {
            chatRoomController.chatMessages.value = [];
            if (_showActionButtons.value) {
              _toggleActionButtons();
            }
          }),
      ActionButton(
          id: 4,
          label: '扫雷规则',
          iconData: CustomIcons.file_alt,
          onPressed: () {
            _showRule.value = true;
            if (_showActionButtons.value) {
              _toggleActionButtons();
            }
          }),
      ActionButton(
          id: 5,
          label: '排行榜',
          iconData: CustomIcons.anchor,
          onPressed: () {
            AppNavigator.toNamed(lhcBoardPath);
            if (_showActionButtons.value) {
              _toggleActionButtons();
            }
          }),
      ActionButton(
          id: 6,
          label: '昵称',
          iconData: CustomIcons.github,
          onPressed: () {
            chatRoomController.modifyName;
            if (_showActionButtons.value) {
              _toggleActionButtons();
            }
          }),
      ActionButton(
          id: 7,
          label: '头像',
          iconData: CustomIcons.user_circle,
          onPressed: () {
            _panelController.open();
            if (_showActionButtons.value) {
              _toggleActionButtons();
            }
          }),
      if (chatRoomController.chatProvider.value.isAdmin)
        ActionButton(
            id: 8,
            label: '在线会员',
            iconData: CustomIcons.users,
            onPressed: () {
              if (!_showSearchBar.value) {
                _showSearchBar.value = true;
                _searchController.text = "";
              }
              if (_showActionButtons.value) {
                _toggleActionButtons();
              }
            })
    ];
  }

  Widget chatMineRule() {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
              Stack(
                alignment: Alignment.topCenter,
                children: [
                  AppImage.asset('chat/zh/1.jpg'),
                  Positioned(
                    top: 10.w,
                    right: 10.w,
                    child: ElevatedButton(
                      onPressed: () => _showRule.value = false,
                      style: ElevatedButton.styleFrom(
                        shape: const CircleBorder(),
                        padding: EdgeInsets.all(5.w),
                        backgroundColor: Colors.black,
                        foregroundColor: Colors.cyan,
                      ),
                      child: const Icon(Icons.close, color: Colors.white),
                    ),
                  ),
                ],
              ),
              AppImage.asset('chat/zh/2.jpg'),
              AppImage.asset('chat/zh/3.jpg'),
              Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    onPressed: () => _showRule.value = false,
                    style: AppButtonStyles.elevatedStyle(
                        fontSize: 18,
                        color: Colors.white,
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                        width: 180,
                        height: 40),
                    child: const Text("我知道了"),
                  ))
            ])),
      ],
    );
  }

  Widget getChatOnlieUsers() {
    return Positioned(
        top: 5.w,
        right: 20.w,
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 10.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.grey.withOpacity(0.7),
            ),
            child: Text(
                "当前在线: ${chatRoomController.chatOnlineMemberCount.value}",
                style: AppTextStyles.ffffffff(context, fontSize: 18))));
  }

  Widget _buildBackground() {
    return GestureDetector(
        onTap: () {
          if (_showActionButtons.value) {
            _toggleActionButtons();
          }
        },
        child: Container(
          width: MediaQuery.sizeOf(context).width,
          height: MediaQuery.sizeOf(context).height,
          decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.5),
          ),
        ));
  }

  List<ExpandingActionButton> _buildExpandingActionButtons() {
    List<ActionButton> actionButtons0 = getActionButtons();
    _actionButtons.clear();
    final count = actionButtons0.length;
    final step = 360.0 / count;
    for (var i = 0, angleInDegrees = 0.0;
        i < count;
        i++, angleInDegrees += step) {
      _actionButtons.add(
        ExpandingActionButton(
            directionInDegrees: angleInDegrees,
            maxDistance: _fabDistance,
            progress: _expandAnimation,
            isClose: false,
            child: actionButtons0[i]),
      );
    }
    _actionButtons.add(
      ExpandingActionButton(
          directionInDegrees: 0,
          maxDistance: _fabDistance,
          progress: _expandAnimation,
          isClose: true,
          child: ActionButton(
              id: 0,
              iconData: Icons.close,
              color: AppColors.ffC5CBC6,
              iconSize: 50.w,
              onPressed: _toggleActionButtons)),
    );
    return _actionButtons;
  }

  Widget _buildTapToOpenFab() {
    return Positioned(
      left: fabPosition.dx,
      top: fabPosition.dy - _fabDy,
      child: Draggable(
        feedback: _getFloatingActionButton(() => {}),
        child: !_isDrag
            ? AnimatedOpacity(
                opacity: _showActionButtons.value ? 0.0 : 1.0,
                curve: const Interval(0.25, 1.0, curve: Curves.easeInOut),
                duration: const Duration(milliseconds: 250),
                child: _getFloatingActionButton(() {
                  _toggleActionButtons();
                  _resetAvatar();
                }))
            : Container(),
        onDragStarted: () => setState(() => _isDrag = true),
        onDragEnd: (details) {
          setState(() {
            _isDrag = false;
            fabPosition = details.offset; // Update FAB position when dragged
          });
        },
      ),
    );
  }

  Widget _getFloatingActionButton(VoidCallback onPressed) {
    return Container(
        height: 65.w,
        width: 65.w,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            border: Border.all(width: 5, color: Colors.black.withOpacity(0.5))),
        child: FloatingActionButton(
          shape: RoundedRectangleBorder(
              side: BorderSide(width: 5, color: Colors.grey.withOpacity(0.6)),
              borderRadius: BorderRadius.circular(100)),
          onPressed: onPressed,
          child: const Icon(CustomIcons.list),
        ));
  }

  Widget _searchBar() {
    final List<String> items = ['用户名', '昵称', 'UID'];
    return Positioned(
        top: 110.h,
        left: 0,
        child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 50.h,
            child: Container(
                margin: EdgeInsets.only(left: 30.w, right: 30.w),
                padding: EdgeInsets.only(left: 10.w),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        width: 2, color: Colors.grey.withOpacity(0.7)),
                    color: Colors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        child: TextField(
                      controller: _searchController,
                      style: AppTextStyles.ff000000(context, fontSize: 17),
                      maxLines: 1,
                      decoration: InputDecoration.collapsed(
                        hintText: '搜索在线会员',
                        hintStyle: AppTextStyles.ff969696(fontSize: 18),
                        filled: true,
                        fillColor: Colors.white,
                      ),
                    )),
                    Container(width: 2, color: Colors.grey),
                    DropdownButtonHideUnderline(
                      child: DropdownButton2<String>(
                        isExpanded: true,
                        items: items
                            .map((String item) => DropdownMenuItem<String>(
                                  value: item,
                                  child: Text(item,
                                      style: AppTextStyles.ff000000(context,
                                          fontSize: 17),
                                      overflow: TextOverflow.ellipsis),
                                ))
                            .toList(),
                        value: _selectedValue,
                        onChanged: (String? value) {
                          setState(() => _selectedValue = value);
                        },
                        buttonStyleData: ButtonStyleData(
                          height: 50.h,
                          width: 100.w,
                          padding: EdgeInsets.only(left: 10.w, right: 10.w),
                          elevation: 2,
                        ),
                        iconStyleData: const IconStyleData(
                            icon: Icon(CustomIcons.chevron_down),
                            iconSize: 14,
                            iconEnabledColor: Colors.grey,
                            openMenuIcon: Icon(CustomIcons.chevron_up)),
                        dropdownStyleData: DropdownStyleData(
                          maxHeight: 200,
                          width: 100.w,
                          decoration: BoxDecoration(
                              border: Border.all(width: 2, color: Colors.grey),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10.w),
                                  bottomRight: Radius.circular(10.w))),
                          // offset: const Offset(-20, 0),
                        ),
                        menuItemStyleData: MenuItemStyleData(
                          height: 35.h,
                          padding: EdgeInsets.only(left: 10.w, right: 10.w),
                          selectedMenuItemBuilder: (context, child) {
                            return Container(
                                color: AppColors.dialog, child: child);
                          },
                        ),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () {},
                        style: AppButtonStyles.elevatedStyle(
                            color: Colors.white,
                            radius: 0,
                            fontSize: 18,
                            backgroundColor: AppColors.dialog,
                            foregroundColor: Colors.white,
                            width: 80,
                            height: 50),
                        child: const Text('搜索')),
                    Container(width: 2, color: Colors.white),
                    ElevatedButton(
                        onPressed: () => _showSearchBar.value = false,
                        style: AppButtonStyles.elevatedOnlyRightRadius(
                            color: Colors.white,
                            fontSize: 18,
                            backgroundColor: AppColors.dialog,
                            foregroundColor: Colors.white,
                            width: 80,
                            height: 50),
                        child: const Text('关闭'))
                  ],
                ))));
  }

  Widget _slideUpPanel() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          CircleAvatar(
            radius: 80.w, // Image radius
            backgroundImage: NetworkImage(_curAvatar.value),
          ),
          const Text('头像预览', style: TextStyle(fontSize: 18)),
        ]),
        SizedBox(
          height: 100.h,
          child: Row(children: [
            IconButton(
                onPressed: () => _carouselController.previousPage(),
                icon: const Icon(CustomIcons.chevron_left)),
            Expanded(
              child: CarouselSlider(
                options: CarouselOptions(
                  viewportFraction: 0.25,
                  aspectRatio: 16 / 9,
                  enableInfiniteScroll: false,
                  onPageChanged: (index, reason) => {},
                ),
                items: [
                  ...List.generate(_authController.avatarList.length, (index) {
                    AvatarModel item = _authController.avatarList[index];
                    return GestureDetector(
                        onTap: () {
                          _curAvatar.value = item.url;
                          _curAvatarId.value = item.filename;
                        },
                        child: CircleAvatar(
                          radius: 80.w, // Image radius
                          backgroundImage: NetworkImage(item.url),
                        ));
                  })
                ],
                carouselController: _carouselController,
              ),
            ),
            IconButton(
                onPressed: () => _carouselController.nextPage(),
                icon: const Icon(CustomIcons.chevron_right)),
          ]),
        ),
        SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        chatRoomController.updateUserAvatar(
                            _curAvatarId.value, _curAvatar.value);
                        _panelController.close();
                      },
                      style: AppButtonStyles.elevatedStyle(
                          fontSize: 20,
                          color: Colors.white,
                          backgroundColor: Colors.red,
                          foregroundColor: Colors.white,
                          width: 180,
                          height: 40),
                      child: const Text("保存头像")),
                  ElevatedButton(
                      onPressed: () => _panelController.close(),
                      style: AppButtonStyles.elevatedStyle(
                          fontSize: 20,
                          color: Colors.white,
                          backgroundColor: AppColors.ff8D8B8B,
                          foregroundColor: Colors.white,
                          width: 180,
                          height: 40),
                      child: const Text("取消"))
                ]))
      ],
    );
  }

  void _onScrollEndMessageList() async {
    if (chatRoomController.needToScroll.value) {
      if (_scrollController2.positions.isNotEmpty) {
        _scrollController2.animateTo(
          _scrollController2.position.maxScrollExtent,
          duration: const Duration(milliseconds: 100),
          curve: Curves.easeOut,
        );
      }
      chatRoomController.needToScroll.value = false;
    }
  }

  void zoomImage() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.w))),
              child: Obx(() => SizedBox(
                  height: 500.h,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                              onPressed: () {
                                if (chatRoomController.curImageIndex > 0) {
                                  chatRoomController.curImageIndex.value--;
                                }
                              },
                              icon:
                                  const Icon(Icons.arrow_back_ios_new_rounded)),
                          AppImage.network(
                              chatRoomController.chatImages[
                                  chatRoomController.curImageIndex.value],
                              width: 270.w,
                              // height: 450.h,
                              fit: BoxFit.fitWidth),
                          IconButton(
                              onPressed: () {
                                if (chatRoomController.curImageIndex <
                                    chatRoomController.chatImages.length - 1) {
                                  chatRoomController.curImageIndex.value++;
                                }
                              },
                              icon:
                                  const Icon(Icons.arrow_forward_ios_rounded)),
                        ],
                      ),
                      Positioned(
                          top: 0,
                          right: 5.w,
                          child: IconButton(
                              onPressed: () => Navigator.of(context).pop(),
                              icon: const Icon(Icons.close,
                                  color: AppColors.navBarBgColor)))
                    ],
                  ))));
        });
  }

  void showRedBagGrab() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.w))),
              child: Obx(() => Container(
                  height: 500.h,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.w)),
                      image: const DecorationImage(
                          image: AssetImage("assets/images/unGrab.png"),
                          fit: BoxFit.cover)),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              AppImage.network(
                                  chatRoomController.redBagGrab.value
                                          ?.grabList?[0].avatar ??
                                      "",
                                  width: 70.w),
                              Gap(10.h),
                              Text(
                                  chatRoomController.redBagGrab.value
                                          ?.grabList?[0].nickname ??
                                      "",
                                  style: TextStyle(
                                      color: AppColors.ffffeb3b,
                                      fontSize: 19.sp)),
                              Gap(10.h),
                              Text("发了一个红包，金额随机",
                                  style: TextStyle(
                                      color: AppColors.ffffeb3b,
                                      fontSize: 20.sp)),
                              Gap(20.h),
                              Text(
                                  "¥${chatRoomController.redBagGrab.value?.amount}",
                                  style: TextStyle(
                                      color: AppColors.ffffeb3b,
                                      fontSize: 45.sp,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                              chatRoomController.showRedbagDetail.value = true;
                            },
                            child: Text("看看大家的手气>",
                                style: TextStyle(
                                    color: AppColors.ffffeb3b,
                                    fontSize: 23.sp)),
                          )
                        ],
                      ),
                      Positioned(
                          top: 0,
                          left: 0,
                          child: IconButton(
                              onPressed: () => Navigator.of(context).pop(),
                              icon: const Icon(Icons.close,
                                  color: AppColors.ff000000)))
                    ],
                  ))));
        });
  }
}
