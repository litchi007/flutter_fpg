import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/action_button_widget.dart';
import 'dart:math' as math;

@immutable
class ExpandingActionButton extends StatelessWidget {
  const ExpandingActionButton(
      {super.key,
      required this.directionInDegrees,
      required this.maxDistance,
      required this.progress,
      required this.child,
      required this.isClose});

  final double directionInDegrees;
  final double maxDistance;
  final Animation<double> progress;
  final ActionButton child;
  final bool isClose;

  @override
  Widget build(BuildContext context) {
    double scrWidth = MediaQuery.sizeOf(context).width;
    double scrHeight = MediaQuery.sizeOf(context).height;
    return AnimatedBuilder(
      animation: progress,
      builder: (context, child) {
        final offset = Offset.fromDirection(
          directionInDegrees * (math.pi / 180.0),
          progress.value * maxDistance,
        );
        double left = progress.value * scrWidth / 2 - 60.w;
        double top = progress.value * scrHeight / 3;
        if (!isClose) {
          left += offset.dx;
          top += offset.dy;
        }
        return Positioned(
          left: left,
          top: top,
          child: Transform.rotate(
            angle: (1.0 - progress.value) * math.pi / 2,
            child: child!,
          ),
        );
      },
      child: FadeTransition(
        opacity: progress,
        child: child,
      ),
    );
  }
}
