import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/chat_room/chat_room_controller.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatMessageModel.dart';
import 'package:fpg_flutter/services/global_service.dart';

enum eLoadingType { close, operateNickname, operateAvatar, banned, bannedIP }

enum eStatus { disable, enable }

class ChatOverlay extends StatelessWidget {
  final bool visible;
  final VoidCallback close;
  final bool isNickEnabled;

  final ChatProvider chatProvider;
  final Function remove;
  final Function copy;
  final Function banned;
  final Function bannedIP;
  final Function operateNickname;
  final Function privateChat;
  final String loadingType;
  final String status;
  final ChatMessageModel item;

  ChatOverlay({
    required this.visible,
    required this.close,
    required this.isNickEnabled,
    required this.chatProvider,
    required this.remove,
    required this.copy,
    required this.banned,
    required this.bannedIP,
    required this.operateNickname,
    required this.privateChat,
    required this.loadingType,
    required this.status,
    required this.item,
  });

  @override
  Widget build(BuildContext context) {
    String account = '';
    bool isMine = GlobalService.to.userInfo.value?.uid == item.uid;
    late ChatRoomController chatRoomController = Get.find();
    return Visibility(
      visible: visible,
      child: GestureDetector(
        onTap: () => close(),
        child: Material(
          color: Colors.transparent,
          child: Center(
            child: Container(
              padding: EdgeInsets.all(0),
              constraints: BoxConstraints(maxHeight: 600.h, maxWidth: 450.w),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    if (((isMine) || (item.isManager ?? false)) &&
                        (chatProvider.isAdmin)) ...[
                      _rowWidget(first: '删除此条发言')
                          .onTap(() => remove())
                          .paddingOnly(bottom: 18.h)
                      // ListTile(
                      //   title: Text('删除此条发言'),
                      //   onTap: () => remove(),
                      // ),
                    ] else ...[
                      if ((item.isManager ?? false) ||
                          (chatProvider.isAdmin)) ...[
                        _rowWidget(
                                first: '私聊:  ',
                                second: (item.isManager ?? false)
                                    ? item.username ?? ''
                                    : '${item.usernameBak ?? ''}(${item.username ?? ''})')
                            .onTap(() {
                          if (AppDefine.inSites('c316') &&
                              (item.isManager ?? false)) {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: Text('提示'),
                                content: Text('不能私聊管理员'),
                                actions: <Widget>[
                                  TextButton(
                                    child: Text('确定'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              ),
                            );
                            return;
                          }
                          privateChat(item.uid);
                        }),
                      ],
                      if (chatProvider.isAdmin) ...[
                        _rowWidget(first: '删除此条发言').onTap(() => remove()),
                        _rowWidget(first: '复制账号', second: item.username ?? '')
                            .onTap(() => copy()),
                        _rowWidget(
                                first: '会员类型',
                                second:
                                    item.isManager ?? false ? '管理员' : '普通会员')
                            .onTap(() => copy()),
                        _rowWidget(
                                first: '禁言玩家', second: item.usernameBak ?? '')
                            .onTap(() => banned(1)),
                        _rowWidget(first: '解除玩家禁言', second: item.username ?? '')
                            .onTap(() => banned(0)),
                        _rowWidget(first: '禁言ip', second: item.username ?? '')
                            .onTap(() => bannedIP(1)),
                        _rowWidget(first: '解除禁言ip', second: item.username ?? '')
                            .onTap(() => bannedIP(0)),
                        _rowWidget(first: '禁用昵称')
                            .onTap(() => operateNickname(1)),
                        _rowWidget(first: '解禁昵称')
                            .onTap(() => operateNickname(0))
                            .paddingOnly(bottom: 18.h),
                      ],
                    ],
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _rowWidget({String? first, String? second}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(first ?? ''),
        Text(second ?? ''),
      ],
    ).paddingOnly(top: 18.h);
  }
}
