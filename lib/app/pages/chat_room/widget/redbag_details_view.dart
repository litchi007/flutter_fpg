import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/chat_room/chat_room_controller.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/data/models/GrabItemModel.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

@immutable
class RedbagDetailsView extends StatefulWidget {
  RedbagDetailsView({
    super.key,
  });

  @override
  State<RedbagDetailsView> createState() => _RedbagDetailsViewState();
}

class _RedbagDetailsViewState extends State<RedbagDetailsView> {
  final chatRoomController = Get.find<ChatRoomController>();

  @override
  Widget build(BuildContext context) {
    String desc =
        "共${chatRoomController.redBagGrab.value?.quantity ?? ""}个红包，总金额：¥${chatRoomController.redBagGrab.value?.amount ?? ""}，已抢${chatRoomController.redBagGrab.value?.getNum ?? ""}个";
    if (chatRoomController.redBagGrab.value?.remainingNum == 0) {
      desc += "，已被抢光";
    }
    List<GrabItemModel> grabList =
        chatRoomController.redBagGrab.value?.grabList ?? [];
    return Container(
        color: Colors.white,
        child: Stack(children: [
          AppImage.asset("redBagDetail.png", fit: BoxFit.fitWidth),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () =>
                          chatRoomController.showRedbagDetail.value = false,
                      child: Row(children: [
                        Gap(10.w),
                        Text(
                          "关闭",
                          style:
                              TextStyle(color: Colors.white, fontSize: 20.sp),
                        ),
                        Gap(20.w),
                        Text(
                          "| 红包详情",
                          style:
                              TextStyle(color: Colors.white, fontSize: 19.sp),
                        )
                      ])),
                  IconButton(
                    onPressed: () =>
                        chatRoomController.showRedbagDetail.value = false,
                    icon: const Icon(Icons.close_rounded),
                    color: Colors.white,
                  )
                ],
              ),
              Gap(125.h),
              AppImage.network(
                  chatRoomController.redBagGrab.value?.grabList?[0].avatar ??
                      "",
                  width: 100.w),
              Gap(20.h),
              Text(
                  chatRoomController.redBagGrab.value?.grabList?[0].nickname ??
                      "",
                  style: TextStyle(fontSize: 22.sp)),
              Gap(10.h),
              Text(
                chatRoomController.redBagGrab.value?.title ?? "",
                style: TextStyle(fontSize: 18.sp, color: Colors.grey),
                textAlign: TextAlign.center,
              ),
              Gap(15.h),
              Text(chatRoomController.redBagGrab.value?.amount ?? "",
                  style: TextStyle(fontSize: 70.sp)),
              Gap(10.h),
              Container(
                color: const Color.fromARGB(255, 221, 221, 221),
                width: Get.width,
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                child: Text(desc,
                    style: TextStyle(color: Colors.grey, fontSize: 20.sp)),
              ),
              Expanded(
                  child: SingleChildScrollView(
                child: Column(children: [
                  ...List.generate(grabList.length, (index) {
                    GrabItemModel item = grabList[index];
                    return Container(
                        padding: EdgeInsets.symmetric(horizontal: 15.w),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: const Color(0xFF999999), width: 1.h)),
                        ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  AppImage.network(item.avatar ?? "",
                                      width: 70.w),
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(item.nickname ?? "",
                                            style: TextStyle(fontSize: 20.sp)),
                                        Text(item.date ?? "",
                                            style: TextStyle(
                                                fontSize: 19.sp,
                                                color: Colors.grey)),
                                      ])
                                ],
                              ),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(item.miniRedBagAmount ?? "",
                                        style: TextStyle(fontSize: 22.sp)),
                                    Text("♛手气最佳",
                                        style: TextStyle(
                                            fontSize: 20.sp,
                                            color: AppColors.ffDDA815)),
                                  ])
                            ]));
                  })
                ]),
              ))
            ],
          )
        ]));
  }
}
