import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:gap/gap.dart';

@immutable
class ActionButton extends StatelessWidget {
  ActionButton({
    super.key,
    this.label,
    this.onPressed,
    this.color,
    this.iconSize,
    required this.id,
    required this.iconData,
  });

  final int id;
  final VoidCallback? onPressed;
  final IconData iconData;
  String? label;
  final Color? color;
  final double? iconSize;

  @override
  Widget build(BuildContext context) {
    Color mainColor = AppColors.ff252525;
    if (label == null) mainColor = AppColors.ff5C5869;
    return SizedBox(
        width: 110.w,
        height: 110.w,
        child: ClipOval(
            child: Material(
                shape: const CircleBorder(),
                clipBehavior: Clip.antiAlias,
                color: mainColor,
                elevation: 4,
                child: InkWell(
                    splashColor: AppColors.ff8D8B8B,
                    onTap: onPressed,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(iconData,
                              color: color ?? Colors.white,
                              size: iconSize ?? 30.w),
                          if (label != null) Gap(3.w),
                          if (label != null)
                            Text(label ?? '',
                                style: const TextStyle(color: Colors.white)),
                        ])))));
  }
}
