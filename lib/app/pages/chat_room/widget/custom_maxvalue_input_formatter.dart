import 'package:flutter/services.dart';

class CustomMaxvalueInputFormatter extends TextInputFormatter {
  final int maxInputValue;
  CustomMaxvalueInputFormatter({required this.maxInputValue});

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final int? value = int.tryParse(newValue.text);
    if (value == null) {
      return newValue;
    }
    if (value > maxInputValue) {
      return const TextEditingValue().copyWith(text: maxInputValue.toString());
    }
    return newValue;
  }
}
