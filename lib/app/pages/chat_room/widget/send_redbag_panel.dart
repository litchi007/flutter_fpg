import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/chat_room/chat_room_controller.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/custom_maxvalue_input_formatter.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatRoomData.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatTokenModel.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'dart:math' as math;

@immutable
class SendRedbagPanel extends StatefulWidget {
  SendRedbagPanel({super.key, required this.tabIndex});
  int tabIndex;

  @override
  State<SendRedbagPanel> createState() => _SendRedbagPanelState();
}

class _SendRedbagPanelState extends State<SendRedbagPanel> {
  final chatRoomController = Get.find<ChatRoomController>();
  final textEditingController1 = TextEditingController();
  final textEditingController2 = TextEditingController();
  final textEditingController3 = TextEditingController();
  final textEditingController4 = TextEditingController();
  final textEditingController5 = TextEditingController();
  final textEditingController6 = TextEditingController();
  final textEditingController7 = TextEditingController();
  final RxInt curIndex = 0.obs;
  final RxInt curNumber = 11.obs;

  late int maxAmount1, maxAmount2;
  late int minAmount1, minAmount2;
  late int quantity;
  late String oddsRate;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    curIndex.value = widget.tabIndex;
    textEditingController1.addListener(() => setState(() {}));
    textEditingController2.addListener(() => setState(() {}));
    textEditingController4.addListener(() => setState(() {}));
    textEditingController5.addListener(() => setState(() {}));
    textEditingController6.addListener(() => setState(() {}));
    textEditingController7.addListener(() => setState(() {}));
    ChatRoomChatAry? chatRoom = chatRoomController.chatRoom.value;
    maxAmount1 = int.parse(chatRoom?.chatRedBagSetting?.maxAmount ?? '0');
    minAmount1 = int.parse(chatRoom?.chatRedBagSetting?.minAmount ?? '0');
    maxAmount2 = double.tryParse(chatRoom?.maxAmount ?? "0")!.toInt();
    minAmount2 = double.tryParse(chatRoom?.minAmount ?? "0")!.toInt();
    quantity = int.parse(chatRoom?.quantity ?? "0");
    oddsRate = chatRoom?.oddsRate ?? "0";
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
        color: const Color.fromARGB(255, 240, 238, 238),
        child: Column(
          children: [
            Container(
                width: Get.width,
                height: 50.h,
                color: Colors.red,
                child: Stack(alignment: Alignment.center, children: [
                  Positioned(
                    top: 10.h,
                    right: 10.w,
                    child: GestureDetector(
                        onTap: () =>
                            chatRoomController.showRedbagPanel.value = false,
                        child: Text("关闭",
                            style: TextStyle(
                                color: Colors.white, fontSize: 18.sp))),
                  ),
                  Text(
                    "发红包",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 22.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ])),
            Container(
              color: Colors.white,
              margin: EdgeInsets.symmetric(horizontal: 30.w, vertical: 20.h),
              child: IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                        onPressed: () => curIndex.value = 0,
                        child: Text("普通红包",
                            style: TextStyle(
                                color: curIndex.value == 0
                                    ? Colors.red
                                    : AppColors.textColor3,
                                fontSize: 18.sp))),
                    VerticalDivider(
                      thickness: 1.w,
                      color: AppColors.ff8D8B8B,
                      indent: 13.h,
                      endIndent: 13.h,
                    ),
                    TextButton(
                        onPressed: () => curIndex.value = 1,
                        child: Text("口令红包",
                            style: TextStyle(
                                color: curIndex.value == 1
                                    ? Colors.red
                                    : AppColors.textColor3,
                                fontSize: 18.sp))),
                    VerticalDivider(
                      thickness: 1.w,
                      color: AppColors.ff8D8B8B,
                      indent: 13.h,
                      endIndent: 13.h,
                    ),
                    TextButton(
                        onPressed: () => curIndex.value = 2,
                        child: Text("扫雷红包",
                            style: TextStyle(
                                color: curIndex.value == 2
                                    ? Colors.red
                                    : AppColors.textColor3,
                                fontSize: 18.sp))),
                  ],
                ),
              ),
            ),
            if (curIndex.value == 0)
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("最大数量：$quantity个，金额范围：￥$minAmount1~$maxAmount1",
                      style: TextStyle(
                          color: AppColors.ff9e9e9e, fontSize: 18.sp)),
                  _getTextField("红包个数", "填写个数", "个", quantity,
                      quantity.toString().length, textEditingController1),
                  _getTextField("红包金额", "填写金额", "元", maxAmount1,
                      maxAmount1.toString().length, textEditingController2),
                  _getTextarea("恭喜发财，大吉大利", textEditingController3),
                  Gap(20.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.w),
                    child: ElevatedButton(
                      onPressed: textEditingController1.text.isNotEmpty &&
                              textEditingController2.text.isNotEmpty
                          ? sendPutongRedBag
                          : null,
                      style: AppButtonStyles.elevatedFullWidthStyle(
                          fontSize: 18,
                          backgroundColor: Colors.red,
                          foregroundColor: Colors.white),
                      child: const Text("塞钱进红包"),
                    ),
                  ),
                ],
              ),
            if (curIndex.value == 1)
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("最大数量：$quantity个，金额范围：￥$minAmount1~$maxAmount1",
                      style: TextStyle(
                          color: AppColors.ff9e9e9e, fontSize: 18.sp)),
                  _getTextField("红包个数", "填写个数", "个", quantity,
                      quantity.toString().length, textEditingController4),
                  _getTextField("红包金额", "填写金额", "元", maxAmount1,
                      maxAmount1.toString().length, textEditingController5),
                  _getTextarea("口令", textEditingController6),
                  Gap(20.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.w),
                    child: ElevatedButton(
                      onPressed: textEditingController4.text.isNotEmpty &&
                              textEditingController5.text.isNotEmpty &&
                              textEditingController5.text.isNotEmpty
                          ? sendKoulingRedBag
                          : null,
                      style: AppButtonStyles.elevatedFullWidthStyle(
                          fontSize: 18,
                          backgroundColor: Colors.red,
                          foregroundColor: Colors.white),
                      child: const Text("塞钱进红包"),
                    ),
                  ),
                ],
              ),
            if (curIndex.value == 2)
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                      "金额范围：¥$minAmount2~$maxAmount2，数量：$quantity，赔率：$oddsRate",
                      style: TextStyle(
                          color: AppColors.ff9e9e9e, fontSize: 18.sp)),
                  _getTextField2(textEditingController7),
                  _getChooseMoneyPanel(),
                  _getChooseNumberPanel(),
                  Gap(20.h),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.w),
                    child: ElevatedButton(
                      onPressed: textEditingController7.text.isNotEmpty &&
                              curNumber.value < 10
                          ? sendSaoleiRedBag
                          : null,
                      style: AppButtonStyles.elevatedFullWidthStyle(
                          fontSize: 18,
                          backgroundColor: Colors.red,
                          foregroundColor: Colors.white),
                      child: const Text("发送红包"),
                    ),
                  ),
                ],
              ),
          ],
        )));
  }

  Widget _getTextField(
      String prefix,
      String hintText,
      String surfix,
      int maxValue,
      int maxLength,
      TextEditingController textEditingController) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
        margin: EdgeInsets.symmetric(horizontal: 30.w, vertical: 5.h),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10.w)),
        child: Row(
          children: [
            Text(prefix,
                style: TextStyle(color: AppColors.textColor3, fontSize: 20.sp)),
            Gap(20.w),
            Expanded(
                child: TextField(
              controller: textEditingController,
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
              maxLines: 1,
              keyboardType: TextInputType.number,
              inputFormatters: [
                CustomMaxvalueInputFormatter(maxInputValue: maxValue),
                FilteringTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(maxLength),
              ],
              decoration: InputDecoration.collapsed(
                  hintText: hintText,
                  hintStyle:
                      TextStyle(fontSize: 16.sp, color: AppColors.ff9e9e9e)),
              textAlign: TextAlign.right,
            )),
            Gap(20.w),
            Text(surfix,
                style: TextStyle(color: AppColors.textColor3, fontSize: 20.sp)),
          ],
        ));
  }

  Widget _getTextarea(
      String hintText, TextEditingController textEditingController) {
    ScrollController scrollController = ScrollController();
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 0.h),
        margin: EdgeInsets.symmetric(horizontal: 30.w, vertical: 5.h),
        height: 100.h,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10.w)),
        child: TextField(
          controller: textEditingController,
          scrollController: scrollController,
          style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
          maxLines: null,
          expands: true,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(fontSize: 16.sp, color: AppColors.ff9e9e9e),
            border: InputBorder.none,
          ),
        ));
  }

  Widget _getTextField2(TextEditingController textEditingController) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 13.h),
        margin: EdgeInsets.only(left: 30.w, right: 30.w, top: 5.h),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(3.w), topRight: Radius.circular(3.w))),
        child: Row(
          children: [
            AppImage.asset("sao.png", width: 30.w),
            Gap(5.w),
            Text("总金额",
                style: TextStyle(color: AppColors.textColor3, fontSize: 22.sp)),
            Gap(20.w),
            Expanded(
                child: TextField(
              controller: textEditingController,
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
              maxLines: 1,
              keyboardType: TextInputType.number,
              inputFormatters: [
                CustomMaxvalueInputFormatter(maxInputValue: maxAmount2),
                FilteringTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(maxAmount2.toString().length),
              ],
              decoration: InputDecoration.collapsed(
                  hintText: "请输入金额",
                  hintStyle:
                      TextStyle(fontSize: 16.sp, color: AppColors.ff9e9e9e)),
              textAlign: TextAlign.right,
            )),
          ],
        ));
  }

  Widget _getChooseMoneyPanel() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
        margin: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 5.h),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(3.w),
                bottomRight: Radius.circular(3.w)),
            border:
                Border(top: BorderSide(color: AppColors.ff8e8e8e, width: 1.h))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _getIntWellButton(value: 10, color: const Color(0xFF80a3ea)),
            _getIntWellButton(value: 20, color: const Color(0xFF1dd89b)),
            _getIntWellButton(value: 50, color: const Color(0xFF1dbed8)),
            _getIntWellButton(value: 100, color: const Color(0xFF99d81d)),
            _getIntWellButton(value: 200, color: const Color(0xFFe61c38)),
            _getIntWellButton(value: 500, color: const Color(0xFFc319a7)),
            _getIntWellButton(label: "重置", color: const Color(0xFFcccccc)),
          ],
        ));
  }

  Widget _getIntWellButton({String? label, int? value, Color? color}) {
    return InkWell(
        onTap: () {
          int v = 0;
          if (value == null) {
            textEditingController7.text = "";
          } else {
            if (textEditingController7.text.isNotEmpty) {
              v = int.parse(textEditingController7.text);
            }
            v += value;
            textEditingController7.text = v.toString();
          }
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
            decoration: BoxDecoration(
                color: color, borderRadius: BorderRadius.circular(40.w)),
            child: Text(label ?? value.toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.sp,
                  shadows: const [
                    Shadow(
                      color: Colors.black,
                      offset: Offset(2.0, 2.0),
                      blurRadius: 10,
                    ),
                  ],
                ))));
  }

  Widget _getChooseNumberPanel() {
    return Container(
        padding:
            EdgeInsets.only(left: 10.w, right: 10.w, top: 0.h, bottom: 15.h),
        margin: EdgeInsets.symmetric(horizontal: 30.w, vertical: 10.h),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(3.w),
              bottomRight: Radius.circular(3.w)),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("选择雷号",
                    style:
                        TextStyle(color: AppColors.ff666666, fontSize: 20.sp)),
                TextButton(
                    onPressed: () {
                      final random = math.Random();
                      curNumber.value = random.nextInt(10);
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 15.w, vertical: 2.h),
                        decoration: BoxDecoration(
                            color: const Color(0xFFf5f5f5),
                            borderRadius: BorderRadius.circular(20.w),
                            border: Border.all(
                                color: const Color(0xFFdddddd), width: 1.w)),
                        child: Text("随机",
                            style: TextStyle(
                              color: AppColors.ff666666,
                              fontSize: 16.sp,
                            ))))
              ],
            ),
            GridView.count(
              crossAxisCount: 5,
              mainAxisSpacing: 20.h,
              crossAxisSpacing: 20.w,
              childAspectRatio: 1.2,
              primary: false,
              shrinkWrap: true,
              children: List.generate(10, (index) {
                return TextButton(
                  onPressed: () => curNumber.value = index,
                  style: OutlinedButton.styleFrom(
                      shape: CircleBorder(
                          side: BorderSide(
                              color: curNumber.value == index
                                  ? Colors.red
                                  : Colors.grey)),
                      foregroundColor: curNumber.value == index
                          ? Colors.white
                          : Colors.black,
                      backgroundColor:
                          curNumber.value == index ? Colors.red : Colors.white,
                      textStyle: TextStyle(fontSize: 25.sp)),
                  child: Text(index.toString()),
                );
              }),
            )
          ],
        ));
  }

  void sendKoulingRedBag() {
    chatRoomController
        .createRedBag(
            genre: 3,
            amount: textEditingController5.text,
            title: textEditingController6.text,
            quantity: int.parse(textEditingController4.text))
        .then((data) {
      chatRoomController.showRedbagPanel.value = false;
    });
  }

  void sendPutongRedBag() {
    chatRoomController
        .createRedBag(
            genre: 1,
            amount: textEditingController2.text,
            title: textEditingController3.text,
            quantity: int.parse(textEditingController1.text))
        .then((data) {
      chatRoomController.showRedbagPanel.value = false;
    });
  }

  void sendSaoleiRedBag() {
    chatRoomController
        .createRedBag(
            genre: 2,
            amount: textEditingController7.text,
            mineNumber: curNumber.toString())
        .then((data) {
      chatRoomController.showRedbagPanel.value = false;
    });
  }
}
