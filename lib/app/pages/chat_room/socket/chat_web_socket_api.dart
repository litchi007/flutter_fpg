import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

class ChatWebSocketApi {
  String websocketUrl = 'wss://t002-chat.fh-chat.cc';
  late WebSocketChannel? channel;
  String roomId;
  String url = "";
  void Function(dynamic) onListen;
  Timer? timer;

  ChatWebSocketApi({required this.roomId, required this.onListen}) {
    String token = AppDefine.userToken?.apiToken ?? '';
    String sessid = AppDefine.userToken?.apiSid ?? '';
    print("创建socket $token $sessid");

    if (token != '' && sessid != '') {
      url =
          '$websocketUrl?loginsessid=$sessid&logintoken=$token&channel=${2}&isReload=${0}&isLoadRecord=${0}';
      print("websocket url: $url");
      // channel = IOWebSocketChannel.connect(Uri.parse(url));
      init();
    }
  }

  Future<void> init() async {
    channel = IOWebSocketChannel.connect(Uri.parse(url));
    try {
      await channel?.ready;
      if (channel != null) {
        channel?.stream.listen((e) => onListen.call(e), onError: _onError);
        //enter message
        final msg = {
          "code": "R0000",
          "channel": 2,
          "operate": 1, // 操作类型 1-进入房间 2-退出房间
          "roomId": roomId
        };
        channel?.sink.add(jsonEncode(msg));
        print('=== wss 发送进房间口令 成功 ===> $msg');
        if (timer?.isActive ?? false) {
          timer?.cancel();
          TimerManager().stopAllTimers();
        }
        timer = Timer.periodic(4000.milliseconds, (v) {
          channel?.sink.add(jsonEncode({"code": "R0001"}));
        });
      } else {
        _reconnect();
      }
    } catch (e) {
      print("WebSocketChannel Error: ${e.toString()}");
    }
  }

  void close() {
    final msg = {
      "code": "R0000",
      "channel": 2,
      "operate": 2, // 操作类型 1-进入房间 2-退出房间
      "roomId": roomId
    };
    channel?.sink.add(jsonEncode(msg));
    channel?.sink.close();
    timer?.cancel();
  }

  Future<void> send(dynamic data) async {
    try {
      await channel?.ready;
    } on SocketException catch (e) {
      // Handle the exception.
      channel = null;
      print("SocketException --> $e");
      await init();
    } on WebSocketChannelException catch (e) {
      // Handle the exception.
      print("WebSocketChannelException --> $e");
    }

    if (channel != null) {
      channel?.sink.add(data);
    }
  }

  void _reconnect() {
    if (channel != null) {
      channel?.sink.close();
      channel = null;
    }
    print("重连");
    init();
  }

  _onError(err) {
    print("err___$err");
    init();
  }
}
