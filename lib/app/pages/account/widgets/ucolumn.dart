import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class Ucolumn extends Container {
  final List<String>? data;
  VoidCallback? onPressed;
  final int index;
  Ucolumn({
    required this.data,
    required this.index,
    this.onPressed,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 1.sw,
        height: 50.h,
        padding: const EdgeInsets.symmetric(horizontal: 10).w,
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    width: 1.w,
                    color: AppColors.ffC5CBC6 ?? Colors.grey,
                    style: BorderStyle.solid))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ...List.generate(data!.length, (index) {
              return
                  // SizedBox(
                  //   width: 1.sw / data!.length - 80.w,
                  //   child:
                  Text(
                data!.elementAt(index),
                textAlign: TextAlign.center,
                style: AppTextStyles.ff000000(context, fontSize: 18),
                // ),
              );
            }),
          ],
        ),
      ),
    );
  }
}
