import 'package:flutter/material.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fpg_flutter/app/pages/account/widgets/activity_reward_dialog.dart';

class ActivityRewardDialog {
  final BuildContext context;
  final String title;
  final Color titleBackgroundColor;
  final Widget action;
  final String htmlstring;
  TextAlign? align = TextAlign.center;
  double? height = 500.h;
  double? width = 300.w;
  TextStyle? titleTextStyle;
  ActivityRewardDialog(
      {required this.context,
      required this.title,
      required this.titleBackgroundColor,
      required this.action,
      required this.htmlstring,
      this.align,
      this.height,
      this.width,
      this.titleTextStyle});
  Widget _createTitle() {
    return Center(
      child: Text(
        title,
        style: titleTextStyle ?? AppTextStyles.bodyText1(context, fontSize: 20),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _createContent() {
    return Container(
        height: height,
        width: width,
        child: ListView(
          children: [
            Html(
              data: htmlstring,
              style: {
                "div": Style(
                  padding: HtmlPaddings.only(top: 2.w),
                  height: Height.auto(),
                  textAlign: align,
                ),
              },
            ),
          ],
        ));
  }

// AspectRatio(
  Widget _createActions() {
    return action;
  }

  Dialog _createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 1.sw,
            height: 80.h,
            decoration: BoxDecoration(
                color: titleBackgroundColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: SizedBox(
              width: .8.sw,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: _createTitle(),
              ),
            ),
          ),
          Container(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: _createContent(),
            ),
          ),
          _createActions(),
          SizedBox(
            height: 20.h,
          )
        ],
      ),
    );
  }

  void dialogBuilder() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: _createDialog());
        });
  }
}
