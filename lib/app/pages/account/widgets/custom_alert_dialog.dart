import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';

class CustomAlertDialog {
  String title;
  final List<String> categories;
  List<dynamic> dataForDialog = <dynamic>[];
  final Color titleBackgroundColor;
  final BuildContext context;
  final double? borderRadius;
  final AuthController _authController = Get.find();
  dynamic action;
  CustomAlertDialog({
    required this.context,
    required this.title,
    this.borderRadius,
    required this.categories,
    required this.titleBackgroundColor,
    this.action = 'normal',
  });
  Text _createTitle() {
    return Text(
      title,
      style: AppTextStyles.bodyText1(context, fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  void setTitle(String title) {
    title = title;
  }

  Column _createContent() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(
            categories.length,
            (index) => Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text('${categories[index]} : ${dataForDialog[index]}',
                      style: AppTextStyles.ff5C5869_(context, fontSize: 16)),
                )));
  }

  List<Widget>? _createActions() {
    if (action is String) {
      return [
        GestureDetector(
            onTapDown: (done) => Get.back(),
            child: Container(
              width: 0.38.sw,
              height: 45.h,
              decoration: BoxDecoration(
                  color: AppColors.ffeaeaea,
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "取消",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.bodyText2(context, fontSize: 15),
                  ),
                ],
              ),
            )),
        GestureDetector(
            onTapDown: (done) {
              Get.back();
              if (action == 'logout') _authController.logout();
            },
            child: Container(
              width: 0.38.sw,
              height: 45.h,
              decoration: BoxDecoration(
                  color: AppColors.ffbfa46d,
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "确定",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.surface_(context, fontSize: 15),
                  ),
                ],
              ),
            ))
      ];
    } else if (action is Widget) {
      return [action];
    }
  }

  Dialog _createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius ?? 8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 1.sw,
            height: 50.h,
            decoration: BoxDecoration(
                color: titleBackgroundColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(borderRadius ?? 8),
                    topRight: Radius.circular(borderRadius ?? 8))),
            child: SizedBox(
              width: .8.sw,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: _createTitle(),
              ),
            ),
          ),
          SizedBox(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: _createContent(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: _createActions() ?? [const SizedBox()],
          ),
          if (action is String)
            SizedBox(
              height: 20.h,
            )
        ],
      ),
    );
  }

  void dialogBuilder(List<dynamic> data) {
    dataForDialog = data;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: _createDialog());
        });
  }
}
