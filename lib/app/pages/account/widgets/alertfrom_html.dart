import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class AlertFromHtml {
  final BuildContext context;
  final String title;
  final Color titleBackgroundColor;
  final Widget action;
  final String htmlstring;

  AlertFromHtml({
    required this.context,
    required this.title,
    required this.titleBackgroundColor,
    required this.action,
    required this.htmlstring,
  });
  Text createTitle() {
    return Text(
      title,
      style: AppTextStyles.bodyText1(context, fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  Widget createContent() {
    return Container(
      height: 500.w,
      width: 300.w,
      child: ListView(
        children: [
          Html(
            data: htmlstring,
            style: {
              "div": Style(
                padding: HtmlPaddings.only(top: 2.w),
                height: Height.auto(),
                textAlign: TextAlign.center,
              ),
            },
          ),
        ],
      ),
    );
  }

// AspectRatio(
  Widget createActions() {
    return action;
  }

  Dialog createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 1.sw,
            height: 50.h,
            decoration: BoxDecoration(
                color: titleBackgroundColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: SizedBox(
              width: .8.sw,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: createTitle(),
              ),
            ),
          ),
          SizedBox(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: createContent(),
            ),
          ),
          createActions(),
          SizedBox(
            height: 20.h,
          )
        ],
      ),
    );
  }

  void dialogBuilder() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: createDialog());
        });
  }
}

class BaseAlertWidget extends Container {
  final BuildContext context;
  Widget? title;
  Widget? content;
  Widget? action;
  Color? titleColor = AppColors.ffffffff;
  Color? actionColor = AppColors.ffffffff;
  Color? contentColor = AppColors.ffffffff;
  BaseAlertWidget({
    required this.context,
    this.title,
    this.content,
    this.action,
  });
  Widget createTitle() {
    return title ?? const SizedBox();
  }

  Widget createContent() {
    return SizedBox(
      child: SingleChildScrollView(
        child: content,
      ),
    );
  }

  Widget createActions() {
    return action ?? const SizedBox();
  }

  void showAlert() {
    title = createTitle();
    content = createContent();
    action = createActions();
    dialogBuilder();
  }

  Dialog createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 1.sw,
            height: 70.w,
            padding: EdgeInsets.all(5.w),
            decoration: BoxDecoration(
                color: titleColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.w),
                    topRight: Radius.circular(8.w))),
            child: title ?? const SizedBox(),
          ),
          SizedBox(
            height: 20.w,
          ),
          Container(
            width: 1.sw,
            padding: EdgeInsets.all(5.w),
            decoration: BoxDecoration(
                color: contentColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8.w),
                    bottomRight: Radius.circular(8.w))),
            child: content ?? const SizedBox(),
          ),
          Container(
            width: 1.sw,
            // height: 80.w,
            padding: EdgeInsets.all(5.w),
            decoration: BoxDecoration(
                color: actionColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8.w),
                    bottomRight: Radius.circular(8.w))),
            child: action,
          ),
        ],
      ),
    );
  }

  void dialogBuilder() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: createDialog());
        });
  }
}

class CustomButton extends Container {
  CustomButton(
      {super.key,
      this.width,
      this.height,
      this.btnStr,
      this.onPressed,
      this.backColor,
      this.textColor,
      this.fontSize});
  double? width = 50.w;
  double? height = 40.w;
  String? btnStr;
  VoidCallback? onPressed;
  Color? backColor;
  Color? textColor;
  double? fontSize = 22;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          backgroundColor: backColor ?? AppColors.ffffffff,
          // padding: EdgeInsets.symmetric(vertical: 1.h),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.w),
          ),
        ),
        child: Text(
          btnStr ?? '',
          style: TextStyle(
            fontSize: fontSize,
            color: textColor ?? AppColors.ff000000,
          ),
        ),
      ),
    );
  }
}
