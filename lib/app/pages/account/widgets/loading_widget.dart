import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class CustomLoadingWidget extends StatelessWidget {
  const CustomLoadingWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50).h,
      child: Center(
        child: LoadingAnimationWidget.discreteCircle(
            color: Color.fromARGB(255, 27, 146, 136), size: 25),
      ),
    );
  }
}
