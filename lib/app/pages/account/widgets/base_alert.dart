import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BaseAlert {
  final BuildContext context;
  final String title;
  final Color titleBackgroundColor;
  final Widget action;
  final Widget content;

  BaseAlert({
    required this.context,
    required this.title,
    required this.titleBackgroundColor,
    required this.action,
    required this.content,
  });
  Text _createTitle() {
    return Text(
      title,
      style: AppTextStyles.bodyText1(context, fontSize: 22),
      textAlign: TextAlign.center,
    );
  }

  Widget createContent() {
    return SizedBox(
      child: SingleChildScrollView(
        child: content,
      ),
    );
  }

  Widget createActions() {
    return action;
  }

  Dialog createDialog() {
    return Dialog(
      backgroundColor: titleBackgroundColor,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 1.sw,
            height: 50.h,
            decoration: BoxDecoration(
                color: titleBackgroundColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: SizedBox(
              width: .8.sw,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: _createTitle(),
              ),
            ),
          ),
          SizedBox(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: createContent(),
            ),
          ),
          createActions(),
          SizedBox(
            height: 20.h,
          )
        ],
      ),
    );
  }

  void dialogBuilder(List<dynamic> data) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: createDialog());
        });
  }
}
