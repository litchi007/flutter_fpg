import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class CustomNotitleDialog {
  String bodyText;
  List<dynamic> dataForDialog = <dynamic>[];
  final Color titleBackgroundColor;
  final BuildContext context;
  final String actionPath;
  dynamic action;

  CustomNotitleDialog({
    required this.context,
    required this.bodyText,
    required this.titleBackgroundColor,
    required this.actionPath,
    this.action = 'normal',
  });

  void setTitle(String title) {
    title = title;
  }

  Widget _createContent() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0).w,
        child: Text(bodyText,
            textAlign: TextAlign.center,
            style: AppTextStyles.headline2(
              context,
              fontSize: 20,
            )),
      ),
    );
  }

  List<Widget> _createActions() {
    if (action is String) {
      return [
        GestureDetector(
            onTapDown: (done) => Get.back(),
            child: Container(
              width: 0.38.sw,
              height: 45.h,
              decoration: const BoxDecoration(
                  color: AppColors.ffeaeaea,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "取消",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.bodyText2(context, fontSize: 15),
                  ),
                ],
              ),
            )),
        GestureDetector(
            onTapDown: (done) {
              Get.toNamed(actionPath);
            },
            child: Container(
              width: 0.38.sw,
              height: 45.h,
              decoration: const BoxDecoration(
                  color: AppColors.ffbfa46d,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "确定",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.surface_(context, fontSize: 15),
                  ),
                ],
              ),
            ))
      ];
    }
    return [action];
  }

  Dialog _createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: _createContent(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: _createActions(),
          ),
          SizedBox(
            height: 20.h,
          )
        ],
      ),
    );
  }

  void dialogBuilder(List<dynamic> data) {
    dataForDialog = data;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: _createDialog());
        });
  }
}
