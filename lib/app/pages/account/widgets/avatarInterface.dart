import 'package:flutter/material.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/pages/account/account_view_controller.dart';

class AvatarModal {
  AvatarModal({required this.context});
  final BuildContext? context;
  List<List<String>>? avatarList;
  AccountViewController controller = Get.find();
  Widget avatar_(String url) {
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      child:
          AppImage.network(url, width: 75.w, height: 75.h, fit: BoxFit.contain),
    );
  }

  Widget _createContent() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.ffbe8716, size: 18.w),
        ),
        SizedBox(
          width: 0.65.sw,
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                ...List.generate(
                    controller.avatarSetting.value.avatarList!.length, (index) {
                  return Avatar(
                      path: controller.avatarSetting.value.avatarList!
                          .elementAt(index)
                          .url!);
                }),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Transform.rotate(
            angle: 3.14,
            child: SizedBox(
              height: 30.h,
              child: Icon(Icons.arrow_back_ios,
                  color: AppColors.ffbe8716, size: 18.w),
            ),
          ),
        ),
      ],
    );
  }

  Widget createActions() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 1.w, vertical: 1.w),
          child: SizedBox(
            child: ElevatedButton(
              child: Text(
                '保存头像',
                style: AppTextStyles.ff000000(context!, fontSize: 20),
              ),
              onPressed: () {
                Get.back();
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.ffcf352e,
                // padding: EdgeInsets.only(bottom: 10.w),
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: AppColors.ff8e8e8e),
                  //
                  borderRadius: BorderRadius.circular(5.0.w),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: 100.w,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 1.w, vertical: 1.w),
          child: SizedBox(
            // height: 40.w,
            child: ElevatedButton(
              child: Text(
                '取消',
                style: AppTextStyles.ff000000(context!, fontSize: 20),
              ),
              onPressed: () {
                Get.back();
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.surface,
                // padding: EdgeInsets.all(5.w),
                shape: RoundedRectangleBorder(
                  side: const BorderSide(color: AppColors.ff8e8e8e),
                  borderRadius: BorderRadius.circular(1.0.w),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Dialog _createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(2))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // Custom title with specified background color
          Container(
            width: 1.sw,
            height: 80.w,
            decoration: BoxDecoration(
                color: AppColors.surface,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5.w),
                    topRight: Radius.circular(5.w))),
            child: SizedBox(
              width: 1.sw,
              child: Padding(
                padding: EdgeInsets.only(top: 1.h),
                child: Obx(
                  () => Column(
                    children: [
                      avatar_(controller.avatarPath.value),
                    ],
                  ),
                ),
              ),
            ),
          ),

          SizedBox(
            height: 20.w,
          ),
          // Content area
          SizedBox(
            width: 1.sw,
            height: 100.w,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: _createContent(),
            ),
          ),
          SizedBox(
            width: 1.sw,
            height: 60.w,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: createActions(),
            ),
          ),

          // Actions
        ],
      ),
    );
  }

  void dialogBuilder() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        clipBehavior: Clip.antiAlias,
        context: context!,
        builder: (BuildContext context) {
          return Container(height: 450.w, width: 1.sw, child: _createDialog());
        });
  }
}

class Avatar extends Container {
  final String path;
  Avatar({super.key, required this.path});
  AccountViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        controller.avatarPath.value = path;
        GlobalService.to.userInfo.value?.avatar = path;
      },
      child: Container(
        color: Colors.transparent,
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        child: AppImage.network(path,
            width: 75.w, height: 75.h, fit: BoxFit.contain),
      ),
    );
  }
}
