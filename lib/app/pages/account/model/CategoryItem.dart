class CategoryItem {
  final String title;
  final String imagePath;

  CategoryItem({
    required this.title,
    required this.imagePath,
  });
}

class TabData {
  final String name;
  final String imagePath;
  TabData({required this.name, required this.imagePath});
}
