import 'package:fpg_flutter/app/pages/account/model/CategoryItem.dart';

final List<CategoryItem> categoryItems = [
  CategoryItem(title: '我的六合', imagePath: 'wdlha'),
  CategoryItem(title: '优惠申请大厅', imagePath: 'dlsq1'),
  CategoryItem(title: '任务大厅', imagePath: 'menu-activity'),
  CategoryItem(title: '资金转换', imagePath: 'tzjl'),
  CategoryItem(title: '每日签到', imagePath: 'menu-betting'),
  CategoryItem(title: '聊天室', imagePath: 'liaot'),
  CategoryItem(title: '抢单', imagePath: 'user_grab'),
  CategoryItem(title: '安全中心', imagePath: 'menu-password'),
  CategoryItem(title: '资金管理', imagePath: 'money'),
  CategoryItem(title: '提款账户', imagePath: 'bank'),
  CategoryItem(title: '今日已结', imagePath: 'menu-account'),
  CategoryItem(title: '彩票注单', imagePath: 'menu-rule-1'),
  CategoryItem(title: '其它注单', imagePath: 'menu-rule'),
  CategoryItem(title: '站内信', imagePath: 'menu-notice'),
  CategoryItem(title: '利息宝', imagePath: 'lxb'),
  CategoryItem(title: '推荐收益', imagePath: 'dlsq1'),
];

final List<TabData> tabItems = [
  TabData(name: '额度转换', imagePath: 'wdqb'),
  TabData(name: '存款', imagePath: 'tzjl'),
  TabData(name: '未结注单', imagePath: 'wjzd'),
  TabData(name: '交易记录', imagePath: 'jyjl'),
  TabData(name: '设置中心', imagePath: 'szzx'),
];
