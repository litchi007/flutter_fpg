import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/data/models/UGUserCenterItem.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/configs/pushhelper.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';

class AccountViewController extends GetxController {
  RxBool isAuthenticated = false.obs;
  final storage = GetStorage();
  final AppSystemRepository _appSysRepository = AppSystemRepository();
  RxString avatarPath = ''.obs;
  Rxn<UGUserModel> userInfo = Rxn();
  Rx<AvatarSettingModel> avatarSetting = AvatarSettingModel().obs;
  bool autoBet = false;
  bool agent = false;
  RxList<UGUserCenterItem> userCenter = RxList();
  bool isYYHTemplate = AppDefine.systemConfig?.mobileTemplateCategory == '63';
  RxBool isIniting = false.obs;
  @override
  void onReady() {
    initController();
    super.onReady();
  }

  void initController() async {
    isIniting.value = true;
    requestData();

    avatarPath.value = GlobalService.to.userInfo.value?.isTest == true
        ? AppDefine.defaultAvatar
        : GlobalService.to.userInfo.value?.avatar ?? AppDefine.defaultAvatar;

    _appSysRepository
        .getAvatarSetting(token: AppDefine.userToken?.apiSid)
        .then((data) {
      avatarSetting.value = data!;
    });
  }

  void requestData() async {
    const bool autoBet = false;
    const bool agent = true;
    List<UGUserCenterItem> items = [];
    items = AppConfig.getDefaultUserCenterItems(autoBet: autoBet, agent: agent);
    if (items.isEmpty) {
      AppDefine.systemConfig?.userCenters?.forEach((item) {
        UGUserCenterItem temp = UGUserCenterItem();
        final fallbackIcon =
            AppConfig.defaultUserCenterLogos[int.parse(item.code ?? '0')];
        temp.logo = item.logo?.isNotEmpty ?? false ? item.logo : fallbackIcon;
        temp.name = item.name;
        temp.fallbackIcon = fallbackIcon;
        temp.code = getUserCenterTypeFromValue(int.parse(item.code ?? '0'));
        items.add(temp);
      });
    } else {}
    List<UGUserCenterItem> userItems =
        checkAgent4UserItems(GlobalService.to.userInfo.value, items);
    userItems.insert(
        0,
        UGUserCenterItem(
          code: UGUserCenterType.mySixHarmonies,
          logo: img_mobileTemplate(
            '38',
            AppDefine.siteId == 'f022' ? 'wdlha_f022' : 'wdlha',
          ),
          name: '我的六合',
        ));
    // userItems.add(UGUserCenterItem(
    //     code: UGUserCenterType.aboutApp,
    //     logo: img_mobileTemplate(
    //       '38',
    //       AppDefine.siteId == 'f022' ? 'menu-activity_f022' : 'dlsq1',
    //     ),
    //     name: '关于${AppDefine.systemConfig?.webName ?? ''}'));
    // const HOTFIX_removeItems = [
    //   UGUserCenterType.aboutApp,
    //   UGUserCenterType.redEnvelope,
    // ];
    // userItems = userItems
    //     .where((item) => !HOTFIX_removeItems.contains(item.code))
    //     .toList();

    userCenter.value = userItems;
    isIniting.value = false;
  }

  List<UGUserCenterItem> checkAgent4UserItems(userInfo, userCenterItems) {
    List<UGUserCenterItem> items = userCenterItems.sublist(0);

    if (GlobalService.to.userInfo.value?.isTest ??
        false || GlobalService.to.userInfo.value?.uid == null) {
      items
          .where((item) => item.code == UGUserCenterType.recommendedIncome)
          .map((item) => item.name = '申请代理')
          .toList();
    }
    return items;
  }

  void updateUserInfo() {}

  void updateUserToken() {}
}
