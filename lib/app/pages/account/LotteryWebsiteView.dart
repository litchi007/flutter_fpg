import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';

class LotteryWebsite extends StatefulWidget {
  const LotteryWebsite({super.key});
  @override
  State<LotteryWebsite> createState() => _LotteryWebsiteState();
}

class _LotteryWebsiteState extends State<LotteryWebsite> {
  late final WebViewController _controller;
  var loadingPercentage = 0;
  String url = '${AppDefine.host}/Open_prize/index.php';

  @override
  void initState() {
    super.initState();
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
          },
          onHttpError: (HttpResponseError error) {
            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            openDialog(request);
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      )
      ..loadRequest(Uri.parse('${AppDefine.host}/Open_prize/index.php'));
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }

    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 28.w),
          ),
          title: Text('开奖网',
              style: AppTextStyles.ffffffff(context,
                  fontSize: 30, fontWeight: FontWeight.bold)),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: WebViewWidget(controller: _controller),
            ),
            Container(
                height: 100.h, // Adjust the height as needed
                color: AppColors.CLBgColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    _footerWidget(
                            Icon(Icons.arrow_circle_left_outlined,
                                color: AppColors.onBackground, size: 45.w),
                            '退出')
                        .onTap(() => _controller.goBack()),
                    _footerWidget(
                            Container(
                                    width: 40.w, // Adjust size as needed
                                    height: 40.w, // Adjust size as needed
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            color: AppColors.onBackground,
                                            width: 4.w) // Circle color
                                        ),
                                    child: Center(
                                        child: Icon(
                                            Icons.cleaning_services_rounded,
                                            color: AppColors.onBackground,
                                            size: 25.w)))
                                .paddingOnly(bottom: 5.h),
                            '清緩存')
                        .onTap(() => _controller.reload()),
                    _footerWidget(
                            Icon(Icons.change_circle_outlined,
                                color: AppColors.onBackground, size: 45.w),
                            '刷新')
                        .onTap(() => _controller.clearCache()),
                  ],
                ).paddingOnly(top: 15.h)),
          ],
        ));
  }

  Widget _footerWidget(Widget icon, String title) {
    return Column(
      children: [
        icon,
        Text(
          title,
          style: AppTextStyles.bodyText2(context, fontSize: 20),
        )
      ],
    );
  }

  Future<void> openDialog(HttpAuthRequest httpRequest) async {
    final TextEditingController usernameTextController =
        TextEditingController();
    final TextEditingController passwordTextController =
        TextEditingController();

    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('${httpRequest.host}: ${httpRequest.realm ?? '-'}'),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  decoration: const InputDecoration(labelText: 'Username'),
                  autofocus: true,
                  controller: usernameTextController,
                ),
                TextField(
                  decoration: const InputDecoration(labelText: 'Password'),
                  controller: passwordTextController,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            // Explicitly cancel the request on iOS as the OS does not emit new
            // requests when a previous request is pending.
            TextButton(
              onPressed: () {
                httpRequest.onCancel();
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                httpRequest.onProceed(
                  WebViewCredential(
                    user: usernameTextController.text,
                    password: passwordTextController.text,
                  ),
                );
                Navigator.of(context).pop();
              },
              child: const Text('Authenticate'),
            ),
          ],
        );
      },
    );
  }
}
