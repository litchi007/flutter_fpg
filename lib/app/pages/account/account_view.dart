import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/configs/pushhelper.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/account_view_controller.dart';
import 'package:fpg_flutter/data/models/UGUserCenterItem.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/pages/account/widgets/avatarInterface.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_alert_dialog.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';

class AccountView extends StatefulWidget {
  const AccountView({super.key});

  @override
  State<AccountView> createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  final AccountViewController controller = Get.put(AccountViewController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    CustomAlertDialog customAlertDialog = CustomAlertDialog(
        context: context,
        title: '确认要退出该帐号？',
        categories: [],
        titleBackgroundColor: AppColors.surface,
        action: 'logout');

    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppGeneralBar(
          backgroundColor: AppColors.surface,
          titleWidget: Center(
              child: Text('我的',
                  style: AppTextStyles.ffbfa46d_(context, fontSize: 24))),
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Column(
          children: <Widget>[
            UserdataWidget(),
            Gap(18.h),
            const MoneyWidget(),
            TabWidget(),
            SizedBox(
              width: 1.sw,
              height: 8.h,
              child: ColoredBox(color: AppColors.ffF5F5F5),
            ),
            ListWidgets(customAlertDialog: customAlertDialog),
          ],
        ))));
  }
}

class UserdataWidget extends StatelessWidget {
  UserdataWidget({super.key});

  AccountViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    AvatarModal avatarModal = AvatarModal(context: context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16).w,
      child: Row(children: [
        Column(
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              GestureDetector(
                onTap: () {
                  if (GlobalService.to.userInfo.value?.isTest == true) {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) => AppCommonDialog(
                            onOk: () => Get.toNamed(loginPath),
                            okLabel: '马上登录',
                            msg: '温馨提示  请登录正式账号',
                            cancelLabel: '取消'));
                  } else {
                    avatarModal.dialogBuilder();
                  }
                },
                child: Obx(() => Padding(
                      padding: const EdgeInsets.only(top: 8).w,
                      child: controller.avatarPath.value == ''
                          ? const SizedBox()
                          : CircleAvatar(
                              backgroundColor: Colors.transparent,
                              backgroundImage: NetworkImage(
                                controller.avatarPath.value,
                              ),
                              radius: 45.w,
                            ),
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30).w,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        GlobalService.to.userInfo.value?.isTest == true
                            ? '游客 ${GlobalService.to.userInfo.value?.curLevelGrade}'
                            : ('${GlobalService.to.userInfo.value?.usr ?? ''}' +
                                (AppDefine.lhltMineSet()
                                    ? '${GlobalService.to.userInfo.value?.curLevelGrade ?? ''}'
                                    : '')),
                        style: AppTextStyles.ffbfa56d_(context, fontSize: 16)),
                    Text(
                        "加入${AppDefine.systemConfig?.webName ?? ''}第${GlobalService.to.userInfo.value?.regUntilNow ?? ''}天",
                        style: AppTextStyles.bodyText2(context, fontSize: 18))
                  ],
                ),
              ),
            ]),
          ],
        ),
        const Spacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {
                if (GlobalService.to.userInfo.value?.isTest == true) {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) => AppCommonDialog(
                          onOk: () => Get.toNamed(loginPath),
                          okLabel: '马上登录',
                          msg: '温馨提示  请登录正式账号',
                          cancelLabel: '取消'));
                } else {
                  Get.toNamed(myInfoPath);
                }
              },
              child: Text("个人信息 >",
                  style: AppTextStyles.ffbfa56d_(context, fontSize: 18)),
            ),
          ],
        ),
      ]),
    );
  }
}

class MoneyWidget extends StatelessWidget {
  const MoneyWidget({super.key});
  @override
  Widget build(BuildContext context) {
    final balance =
        ((convertToDouble(GlobalService.to.userInfo.value?.balance ?? 0) ?? 0) *
                    100)
                .round() /
            100;
    return Container(
      width: 0.95.sw,
      height: 130.h,
      decoration: const BoxDecoration(
          image: DecorationImage(
            image: ExactAssetImage("assets/images/money.png"),
            fit: BoxFit.fill,
          ),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: Column(children: [
        SizedBox(
          height: 50.h,
          width: 0.9.sw,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(15, 0, 30, 0).h,
            child: Row(
              children: [
                AppImage.network(img_mobileTemplate('38', 'yhk'),
                    width: 25.w, height: 25.h, fit: BoxFit.contain),
                Text("总资产",
                    style: AppTextStyles.ffbfa56d_(context,
                        fontSize: 16, fontWeight: FontWeight.bold)),
                Padding(
                  padding: const EdgeInsets.only(left: 2).w,
                  child: AppImage.network(img_mobileTemplate('38', 'sxan'),
                      width: 18.w, height: 18.h, fit: BoxFit.contain),
                ),
                const Spacer(),
                Text("￥ ${balance}",
                    style: AppTextStyles.ffbfa56d_(context,
                        fontSize: 16, fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 80.h,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 30, 0).h,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () async {
                      if (GlobalService.to.userInfo.value?.isTest == true) {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) => AppCommonDialog(
                                onOk: () => Get.toNamed(loginPath),
                                okLabel: '马上登录',
                                msg: '温馨提示  请登录正式账号',
                                cancelLabel: '取消'));
                      } else {
                        await Get.toNamed(taskPath, arguments: [3]);
                      }
                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            AppImage.network(img_mobileTemplate('38', 'vipa'),
                                width: 60.w, height: 60.h, fit: BoxFit.contain),
                            Column(
                              children: [
                                Text("VIP特权",
                                    style: AppTextStyles.ffbfa56d_(context,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Text("PRIVILEGE",
                                    style: AppTextStyles.ffbfa56d_(context,
                                        fontSize: 16)),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      if (GlobalService.to.userInfo.value?.isTest == true) {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) => AppCommonDialog(
                                onOk: () => Get.toNamed(loginPath),
                                okLabel: '马上登录',
                                msg: '温馨提示  请登录正式账号',
                                cancelLabel: '取消'));
                      } else {
                        await Get.toNamed(taskPath, arguments: [0]);
                      }
                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            AppImage.network(img_mobileTemplate('38', 'hga'),
                                width: 60.w, height: 60.h, fit: BoxFit.contain),
                            Column(
                              children: [
                                Text("任务中心",
                                    style: AppTextStyles.ffbfa56d_(context,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Text("CENTER",
                                    style: AppTextStyles.ffbfa56d_(context,
                                        fontSize: 16)),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ]),
          ),
        ),
      ]),
    );
  }
}

class TabWidget extends StatelessWidget {
  TabWidget({super.key});
  final AuthController authController = Get.find();

  // List<TabData> tabItems = AppConfig.userCenterItems??[];
  List<UGUserCenterItem> tabItems = AppConfig.userCenterItems;
  final List<dynamic> accountTabPath = [
    realtransPath,
    depositPath,
    AppRoutes.lotteryLiveBet,
    capitalDetailPath,
    safeCenterPath
  ];
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 5).h,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ...List.generate(tabItems.length, (index) {
              return Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      final route = accountTabPath[index];

                      /// 若是未结注单, 则直接访问
                      if (route == AppRoutes.lotteryLiveBet) {
                        Get.toNamed(route);
                      } else {
                        if (!GlobalService.isShowTestToast) {
                          Get.toNamed(route);
                        }

                        // if (GlobalService.isTest) {
                        //   showDialog(
                        //       context: context,
                        //       barrierDismissible: false,
                        //       builder: (BuildContext context) =>
                        //           AppCommonDialog(
                        //               onOk: () => Get.toNamed(loginPath),
                        //               okLabel: '马上登录',
                        //               msg: '温馨提示  请登录正式账号',
                        //               cancelLabel: '取消'));
                        // } else {
                        //   Get.toNamed(route);
                        // }
                      }
                    },
                    child: tabItems[index].logo == null
                        ? const SizedBox()
                        : AppImage.network(tabItems[index].logo.toString(),
                            width: 40.w, height: 40.h, fit: BoxFit.contain),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10).h,
                    child: Text(tabItems[index].name.toString(),
                        style: AppTextStyles.bodyText2(context, fontSize: 16)),
                  )
                ],
              );
            })
          ],
        ));
  }
}

class ListWidgets extends StatelessWidget {
  final AccountViewController controller = Get.find();

  ListWidgets({super.key, required this.customAlertDialog});
  final CustomAlertDialog customAlertDialog;

  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
          children: [
            ...List.generate(controller.userCenter.length, (index) {
              return GestureDetector(
                  onTap: () {
                    final centerType = controller.userCenter[index].code ??
                        UGUserCenterType.mySixHarmonies;

                    /// 长龙助手
                    /// 全民竞猜
                    /// 开奖走势
                    /// QQ客服
                    /// 开奖网
                    /// 未结注单
                    /// 已结注单
                    /// 优惠活动
                    /// 聊天室
                    /// 我的关注
                    /// 我的动态
                    /// 我的粉丝
                    /// 抢单
                    /// 红包
                    /// 挂机投注
                    /// 真人大厅
                    /// 棋牌大厅
                    /// 游戏注单
                    /// 电子大厅
                    /// 体育大厅
                    /// 电竞大厅
                    /// 彩票大厅
                    /// 捕鱼大厅
                    /// 路珠
                    /// 六合助手
                    /// 历史帖子
                    /// 游戏大厅
                    /// 我的页
                    /// 登录页(試玩帳號必須先登出才能進入)
                    /// 注册页 (試玩帳號必須先登出才能進入)
                    /// 开奖结果
                    /// 即时注单
                    /// 眯牌
                    /// APP下载
                    /// 我的六合
                    /// 关于APP
                    /// 排行榜
                    /// 若是今日已结, 则可以点进去
                    if (centerType == UGUserCenterType.interestTreasure ||
                        centerType == UGUserCenterType.recommendedIncome ||
                        centerType == UGUserCenterType.siteMessage ||
                        centerType == UGUserCenterType.onlineCustomerService ||
                        centerType == UGUserCenterType.longDragonAssistant ||
                        centerType == UGUserCenterType.allPeopleQuiz ||
                        centerType == UGUserCenterType.resultTrend ||
                        centerType == UGUserCenterType.lotteryWebsite ||
                        centerType == UGUserCenterType.pendingOrder ||
                        centerType == UGUserCenterType.settledOrder ||
                        centerType == UGUserCenterType.promotionalActivities ||
                        centerType == UGUserCenterType.chatRoom ||
                        centerType == UGUserCenterType.myFavorites ||
                        centerType == UGUserCenterType.myDynamics ||
                        centerType == UGUserCenterType.myFans ||
                        centerType == UGUserCenterType.grabOrder ||
                        centerType == UGUserCenterType.redEnvelope ||
                        centerType == UGUserCenterType.automaticBet ||
                        centerType == UGUserCenterType.liveHall ||
                        centerType == UGUserCenterType.chessHall ||
                        centerType == UGUserCenterType.gameOrder ||
                        centerType == UGUserCenterType.electronicHall ||
                        centerType == UGUserCenterType.sportsHall ||
                        centerType == UGUserCenterType.lotteryHall ||
                        centerType == UGUserCenterType.fishingHall ||
                        centerType == UGUserCenterType.roadBeads ||
                        centerType == UGUserCenterType.historicalPosts ||
                        centerType == UGUserCenterType.gameLobby ||
                        centerType == UGUserCenterType.myPage ||
                        centerType == UGUserCenterType.loginPage ||
                        centerType == UGUserCenterType.registerPage ||
                        centerType == UGUserCenterType.result ||
                        centerType == UGUserCenterType.realTimeOrder ||
                        centerType == UGUserCenterType.sleepyCards ||
                        centerType == UGUserCenterType.appDownload ||
                        centerType == UGUserCenterType.aboutApp ||
                        centerType == UGUserCenterType.leaderboard ||
                        centerType == UGUserCenterType.mySixHarmonies) {
                      PushHelper.pushUserCenterType(centerType);
                    } else {
                      if (!GlobalService.isShowTestToast) {
                        PushHelper.pushUserCenterType(centerType);
                      }
                      // if (GlobalService.isTest) {
                      //   showDialog(
                      //       context: context,
                      //       barrierDismissible: false,
                      //       builder: (BuildContext context) => AppCommonDialog(
                      //           onOk: () => Get.toNamed(loginPath),
                      //           okLabel: '马上登录',
                      //           msg: '温馨提示  请登录正式账号',
                      //           cancelLabel: '取消'));
                      // } else {
                      //   PushHelper.pushUserCenterType(centerType);
                      // }
                    }
                  },
                  child: Container(
                      width: 1.sw,
                      height: 70.h,
                      padding: const EdgeInsets.symmetric(horizontal: 15).w,
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 1.w,
                                  color: AppColors.ffC5CBC6,
                                  style: BorderStyle.solid))),
                      child: Row(
                        children: [
                          controller.userCenter[index].logo == null
                              ? const SizedBox()
                              : AppImage.network(
                                  controller.userCenter[index].logo!,
                                  width: 30.w,
                                  height: 30.h,
                                  fit: BoxFit.contain),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 20).w,
                            child: Text(controller.userCenter[index].name ?? '',
                                style: AppTextStyles.bodyText2(context,
                                    fontSize: 22)),
                          ),
                          const Spacer(),
                          //查看六合社区动态 - View Liuhe Community News
                          if (index == 0)
                            Text('查看六合社区动态',
                                style: AppTextStyles.ff8D8B8B_(context,
                                    fontSize: 18)),
                          //各种彩金,等您认领 : Various bonuses, waiting for you to claim
                          if (index == 1)
                            Text('各种彩金,等您认领',
                                style: AppTextStyles.ff8D8B8B_(context,
                                    fontSize: 18)),
                          Icon(Icons.arrow_forward_ios,
                              color: AppColors.ff8D8B8B, size: 20.w),
                        ],
                      )));
            }),
            GestureDetector(
                onTap: () {
                  customAlertDialog.dialogBuilder([]);
                },
                child: SizedBox(
                  width: 1.sw,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 18).h,
                      child: Text(
                        '退出登录',
                        style: AppTextStyles.bodyText2(context, fontSize: 22),
                      ),
                    ),
                  ),
                ))
          ],
        ));
  }
}
