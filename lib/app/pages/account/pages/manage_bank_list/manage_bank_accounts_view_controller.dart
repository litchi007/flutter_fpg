import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'dart:math';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class ManageBankAccountsViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();
  RxBool isIniting = false.obs;
  RxBool isInitingForAdd = false.obs;
  RxInt selectedTabIndex = 0.obs;
  // final List<String> arrTitles = ['登录密码', '取款密码', '常用登录地'];

  UserToken? userToken = AppDefine.userToken;
  UGUserModel? userInfo = GlobalService.to.userInfo.value;

  // Map<String, dynamic>? fullAddrData;
  // Rx<ManageBankCardData>? bankCardData;
  RxList<AllAccountListData> arrAllAccountData = RxList();
  RxList<BankInfoParam> arrFullBankList = RxList();
  RxBool bIsEditing = false.obs;
  Rx<BankInfoParam>? selectedItem;

  String typeOnEdit = '';
  String statusOnEdit = '';

  ///
  ///
  ///For the Add/Edit Bank detils
  Rx<String> selectedBankType = "".obs; // First Dropdown
  RxList<String> arrBankTypes = RxList(); // First Dropdown source

  //QRCode
  RxList<String> scanCodeImage = <String>[].obs; // Store image paths as strings
  RxString qrText = "".obs;

  @override
  void onInit() async {
    super.onInit();
    if (GlobalService.to.userInfo.value?.hasFundPwd == null ||
        GlobalService.to.userInfo.value?.hasFundPwd == false) {
      AppToast.showDuration(msg: "请先设置取款密码");
      Future.delayed(Duration(milliseconds: 1000), () {
        Get.toNamed(safeCenterPath, arguments: [1]);
      });
    } else {
      initController();
    }
  }

  void initController() async {
    // await AppJSON.loadJsonObjFromAssets('json/address.json').then((data) {
    //   fullAddrData = data;
    //   updateCountryOption(0);
    // });
    arrBankTypes.value = ['银行卡', '支付宝', '微信', '虚拟币'];
    getBankCardData();
  }

  void getBankCardData() async {
    isIniting.value = true;
    arrFullBankList.clear();
    AppLogger.d(userToken?.apiSid);
    await _appUserRepository
        .bankCard(token: userToken?.apiSid)
        .then((data) async {
      if (data != null) {
        arrAllAccountData.value = data.allAccountList
                ?.where((element) => element.isshow == true)
                .toList() ??
            [];

        for (var bank in arrAllAccountData) {
          // arrAllAccountData.forEach((bank) {
          if (bank.data != null && bank.data!.isNotEmpty) {
            bank.data =
                bank.data!.where((item) => item.bankCode != 'mtzc').toList();
          }
        }

        if (arrAllAccountData.isNotEmpty) {
          for (var item in arrAllAccountData) {
            if ((item.data?.isEmpty ?? true) ||
                ((item.data != null && item.data?.length == 1) &&
                    (item.data != null &&
                        item.data?.isNotEmpty == true &&
                        item.data![0].bankCode == 'mtzc'))) {
              if (item.isshow == true) {
                final random = Random();
                arrFullBankList.add(BankInfoParam(
                  parentTypeName: item.name,
                  bankName: '未绑定',
                  ownerName: '无',
                  bankCard: '无',
                  type: item.type?.toString(),
                  notBind: true,
                  id: (item.data != null && item.data?.isNotEmpty == true)
                      ? item.data![0].id
                      : (random.nextInt(1) * 9000000000 + 1000000000)
                          .toString(),
                ));
              }
            } else {
              for (BankInfoParam res in item.data ?? []) {
                res.parentTypeName = item.name;
                if (res.bankCode == 'mtzc') {
                  res.id = '-1';
                  res.type = '-1';
                }
                res.id = res.id ??
                    (Random().nextInt(9000000000) + 1000000000).toString();
              }

              if (item.data != null) {
                arrFullBankList.addAll(item.data!
                    .map((data) => BankInfoParam(
                          parentTypeName: data.parentTypeName,
                          bankName: data.bankName,
                          ownerName: data.ownerName,
                          bankCard: data.bankCard,
                          type: data.type,
                          notBind: data.notBind,
                          id: data.id,
                          bankCode: data.bankCode,
                        ))
                    .toList());
              }
            }
          }
        }
      }
    });
    isIniting.value = false;
  }
}
