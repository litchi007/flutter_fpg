import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/manage_bank_list/add_bank_account_view_controller.dart';
import 'package:fpg_flutter/data/models/BankListDetailModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class AddBankCardView extends StatefulWidget {
  const AddBankCardView({super.key});

  @override
  State<AddBankCardView> createState() => _AddBankCardViewState();
}

class _AddBankCardViewState extends State<AddBankCardView> {
  final AddBankAccountViewController controller =
      Get.put(AddBankAccountViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        toolbarHeight: 65.h,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back_ios,
            color: (AppDefine.siteId ?? "").contains('f099')
                ? AppColors.ff000000
                : AppColors.ffffffff,
            size: 18.w,
          ),
        ),
        title: Text(
          '绑定提款账户',
          style: (AppDefine.siteId ?? "").contains('f099')
              ? AppTextStyles.ff000000(context, fontSize: 24)
              : AppTextStyles.ffffffff(context, fontSize: 24),
        ),
        centerTitle: true,
      ),
      body: Obx(() {
        if (controller.isIniting.value) {
          return const CustomLoadingWidget();
        } else {
          return SingleChildScrollView(
            child: Center(
              child: controller.userInfo?.hasFundPwd == false
                  ? RenderBindPwd()
                  : controller.arrBankDetailItems.isEmpty
                      ? const SizedBox()
                      : Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30.w, vertical: 30.h),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BankTypeDropdown(),
                              if (controller.arrBankTypes.indexOf(
                                          controller.selectedBankType.value) ==
                                      0 ||
                                  controller.arrBankTypes.indexOf(
                                          controller.selectedBankType.value) ==
                                      3)
                                BankOptionDropdown(),
                              if (controller.arrBankTypes.indexOf(
                                      controller.selectedBankType.value) ==
                                  0)
                                RenderBankWidgets(),
                              if (controller.arrBankTypes.indexOf(
                                      controller.selectedBankType.value) ==
                                  3)
                                RenderBtcWidgets(),
                              if (controller.arrBankTypes.indexOf(
                                      controller.selectedBankType.value) ==
                                  1)
                                RenderAliWidget(),
                              if (controller.arrBankTypes.indexOf(
                                      controller.selectedBankType.value) ==
                                  2)
                                RenderWxWidget(),
                              if (controller.arrBankTypes.indexOf(
                                      controller.selectedBankType.value) ==
                                  0)
                                BankDisplayTips(
                                    fullName:
                                        controller.userInfo?.fullName ?? ""),
                              if (controller.arrBankTypes.indexOf(
                                          controller.selectedBankType.value) ==
                                      1 ||
                                  controller.arrBankTypes.indexOf(
                                          controller.selectedBankType.value) ==
                                      2)
                                DisplayWxAndAliTips(
                                    fullName:
                                        controller.userInfo?.fullName ?? ""),
                              ElevatedButton(
                                onPressed: !controller.isUpdating
                                    ? controller.updateBankAccount
                                    : null,
                                style: ElevatedButton.styleFrom(
                                  shape: const RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                  minimumSize: Size.fromHeight(50.h),
                                  backgroundColor: AppColors.ffbfa46d,
                                ),
                                child: Text('提交',
                                    style: AppTextStyles.bodyText1(context,
                                        fontSize: 18)),
                              ),
                            ],
                          ),
                        ),
            ),
          );
        }
      }),
    );
  }
}

/// 绘制绑定密码

class RenderBindPwd extends StatelessWidget {
  RenderBindPwd({super.key});

  AddBankAccountViewController controller = Get.find();
  final TextEditingController _fundPwdController = TextEditingController();
  final TextEditingController _loginPwdController = TextEditingController();
  final TextEditingController _fundPwd2Controller = TextEditingController();

  void bindPassword(BuildContext context) async {
    // Example:
    final loginPwd = _loginPwdController.text;
    final fundPwd = _fundPwdController.text;
    final fundPwd2 = _fundPwd2Controller.text;
    if (loginPwd.isEmpty) {
      AppToast.showToast('请填写密码(至少6位数字加字母组合)');
      return;
    } else if (fundPwd.isEmpty) {
      AppToast.showToast('请输入您的4位数字提款密码');
      return;
    } else if (fundPwd != fundPwd2) {
      AppToast.showToast('两次输入提款密码不一致');
      return;
    }

    await controller.addFundPwd(loginPwd, fundPwd).then((msg) {
      AppToast.showToast(msg);
      _loginPwdController.clear();
      _fundPwdController.clear();
      _fundPwd2Controller.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.w),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 8.h),
            child: TextField(
              controller: _loginPwdController,
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: '请输入当前登录密码',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 8.h),
            child: TextField(
              controller: _fundPwdController,
              maxLength: 4,
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: '请输入您的4位数字提款密码',
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 8.h),
            child: TextField(
              controller: _fundPwd2Controller,
              maxLength: 4,
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: '请确认您的提款密码',
              ),
            ),
          ),
          SizedBox(
            width: 300.w,
            height: 60.h,
            child: ElevatedButton(
              onPressed: () => bindPassword(context),
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.ffbfa46d,
                padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 12.h),
              ),
              child: Text('提交',
                  style: AppTextStyles.ffffffff(context, fontSize: 18)),
            ),
          ),
        ],
      ),
    );
  }
}

class BankTypeDropdown extends StatelessWidget {
  final AddBankAccountViewController controller = Get.find();

  BankTypeDropdown({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 0.h),
      decoration: BoxDecoration(
          border:
              Border.all(color: AppColors.ff666666, style: BorderStyle.solid)),
      child: Obx(
        () => (DropdownButton<String>(
          isExpanded: true,
          style: AppTextStyles.ff252525_(
            context,
            fontSize: 18,
          ),
          underline: const SizedBox(),
          value: controller.selectedBankType.value != ""
              ? controller.selectedBankType.value
              : controller.arrBankTypes[0],
          onChanged: controller.bIsEditing.value == true
              ? null
              : (String? value) {
                  if (controller.selectedBankType.value == value) return;
                  controller.selectedBankType.value = value ?? "";
                  controller.changeBankType();
                },
          items: controller.arrBankTypes.map<DropdownMenuItem<String>>(
            (value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: AppTextStyles.ff666666(context, fontSize: 18),
                  overflow: TextOverflow.ellipsis,
                ),
              );
            },
          ).toList(),
        )),
      ),
    );
  }
}

class BankOptionDropdown extends StatelessWidget {
  final AddBankAccountViewController controller = Get.find();

  BankOptionDropdown({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 0.h),
      decoration: BoxDecoration(
          border:
              Border.all(color: AppColors.ff666666, style: BorderStyle.solid)),
      child: Obx(
        () => (DropdownButton<String>(
          isExpanded: true,
          hint: const Text("请输入"),
          style: AppTextStyles.ff252525_(
            context,
            fontSize: 18,
          ),
          underline: const SizedBox(),
          value: controller.selectedBankItem.value,
          onChanged: (dropdownItem) {
            controller.selectedBankItem.value = dropdownItem ?? "";
            controller.changeBankId();
          },
          items: controller.arrBankDetailItems.map<DropdownMenuItem<String>>(
            (BankDetailListData item) {
              return DropdownMenuItem<String>(
                // value: item.name ?? "",
                value: item.name,
                child: Text(
                  item.name ?? "",
                  style: AppTextStyles.ff666666(context, fontSize: 18),
                ),
              );
            },
          ).toList(),
        )),
      ),
    );
  }
}

/// 绘制银行
class RenderBankWidgets extends StatelessWidget {
  final AddBankAccountViewController controller = Get.find();
  // final TextEditingController _bankAddrController = TextEditingController();
  // final TextEditingController _bankCardController = TextEditingController();
  // final TextEditingController _bankPasswordController = TextEditingController();

  RenderBankWidgets({super.key});

  @override
  Widget build(BuildContext context) {
    // _bankAddrController.value( controller.selectedBankAddr.value);
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.zero,
          child: TextFormField(
            initialValue: controller.selectedBankAddr.value,
            onChanged: (value) {
              controller.selectedBankAddr.value = value;
            },
            decoration: InputDecoration(
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.zero)),
              hintText: '请输入您的银行卡开户行地址',
              labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
            ),
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
        ),
        Padding(
          padding: EdgeInsets.zero,
          child: TextFormField(
            initialValue: controller.selectedBankCard.value,
            onChanged: (value) {
              controller.selectedBankCard.value = value;
            },
            decoration: InputDecoration(
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.zero)),
              hintText: '请输入您的银行卡卡号',
              labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
            ),
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
        ),
        if (AppDefine.systemConfig?.switchBindVerify != "")
          Padding(
            padding: EdgeInsets.zero,
            child: TextFormField(
              initialValue: controller.selectedBankPwd.value,
              onChanged: (value) {
                controller.selectedBankPwd.value = value;
              },
              decoration: InputDecoration(
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.zero)),
                hintText: '请输入提款密码',
                labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
              ),
              style: AppTextStyles.ff666666(context, fontSize: 18),
            ),
          ),
      ],
    );
  }
}

/// 绘制虚拟币
class RenderBtcWidgets extends StatelessWidget {
  // final TextEditingController _btcAddrController = TextEditingController();
  // final TextEditingController _bankPasswordController = TextEditingController();
  final AddBankAccountViewController controller = Get.find();

  RenderBtcWidgets({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.zero,
          child: TextFormField(
            initialValue: controller.selectedBankCard.value,
//            controller: _btcAddrController,
            onChanged: (value) {
              controller.selectedBankCard.value = value;
            },
            decoration: InputDecoration(
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.zero)),
              hintText: '请输入您的虚拟币收款钱包地址',
              labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
            ),
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
        ),
        if (AppDefine.systemConfig?.switchBindVerify != "")
          Padding(
            padding: EdgeInsets.zero,
            child: TextFormField(
              initialValue: controller.selectedBankPwd.value,
//              controller: _bankPasswordController,
              onChanged: (value) {
                controller.selectedBankPwd.value = value;
              },
              decoration: InputDecoration(
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.zero)),
                hintText: '请输入提款密码',
                labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
              ),
              style: AppTextStyles.ff666666(context, fontSize: 18),
            ),
          ),
      ],
    );
  }
}

/// 绘制微信
class RenderWxWidget extends StatelessWidget {
  // final TextEditingController _wxAccountController = TextEditingController();
  // final TextEditingController _wxPhoneController = TextEditingController();
  // final TextEditingController _bankPasswordController = TextEditingController();

  final AddBankAccountViewController controller = Get.find();
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  // List<String> scanCodeImage; // List to hold image URIs or QR code values

  RenderWxWidget({
    super.key,
    // required this.scanCodeImage,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.zero,
          child: TextFormField(
            initialValue: controller.selectedBankCard.value,
            // controller: _wxAccountController,
            onChanged: (value) {
              controller.selectedBankCard.value = value;
            },
            decoration: InputDecoration(
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.zero)),
              hintText: '请输入微信号',
              labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
            ),
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
        ),
        Padding(
          padding: EdgeInsets.zero,
          child: TextFormField(
            initialValue: controller.selectedBankAddr.value,
            // controller: _wxPhoneController,
            onChanged: (value) {
              controller.selectedBankAddr.value = value;
            },
            decoration: InputDecoration(
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.zero)),
              hintText: '请输入微信所绑定手机号',
              labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
            ),
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
        ),
        if (AppDefine.systemConfig?.switchBindVerify != "")
          Padding(
            padding: EdgeInsets.zero,
            child: TextFormField(
              initialValue: controller.selectedBankPwd.value,
              // controller: _bankPasswordController,
              onChanged: (value) {
                controller.selectedBankPwd.value = value;
              },
              decoration: InputDecoration(
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.zero)),
                hintText: '请输入提款密码',
                labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
              ),
              style: AppTextStyles.ff666666(context, fontSize: 18),
            ),
          ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '收款码',
                style: AppTextStyles.ff666666(context, fontSize: 18),
              ),
              Row(
                children: [
                  Obx(() {
                    if (controller.scanCodeImage.isNotEmpty) {
                      return Stack(
                        children: [
                          SizedBox(
                            width: 150.w,
                            height: 150.h,
                            child: QrImageView(
                              data: controller.scanCodeImage.first,
                              version: QrVersions.auto,
                              size: 150.w,
                            ),
                          ),
                          Positioned(
                            top: 0.w,
                            right: 0.w,
                            child: GestureDetector(
                              onTap: controller.removeImage,
                              child: Container(
                                padding: EdgeInsets.all(4.w),
                                color: AppColors.background,
                                child: Text(
                                  'X',
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 24),
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }
                    return controller.scanCodeImage.isEmpty ||
                            controller.scanCodeImage.first == ""
                        ? Row(
                            children: [
                              GestureDetector(
                                onTap: controller.scanQRCode,
                                child: Container(
                                  width: 130.w,
                                  height: 130.h,
                                  margin: EdgeInsets.all(8.w),
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: AppColors.ff666666),
                                    color: AppColors.fff7dB88,
                                  ),
                                  child: Center(
                                    child: Text(
                                      '+',
                                      style: AppTextStyles.ff666666(context,
                                          fontSize: 24),
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                '上传二维码图片大小限制2MB之内',
                                style: AppTextStyles.ffff0000_(context,
                                    fontSize: 18),
                              )
                            ],
                          )
                        : const SizedBox();
                  }),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

/// 绘制支付宝
class RenderAliWidget extends StatelessWidget {
  // final TextEditingController _aliAccountController = TextEditingController();
  // final TextEditingController _bankPasswordController = TextEditingController();

  final AddBankAccountViewController controller = Get.find();

  RenderAliWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.zero,
          child: TextFormField(
            initialValue: controller.selectedBankCard.value,
            // controller: _aliAccountController,
            onChanged: (value) {
              controller.selectedBankCard.value = value;
            },
            decoration: InputDecoration(
              border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.zero)),
              hintText: '请输入您的支付宝账号',
              labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
            ),
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
        ),
        if (AppDefine.systemConfig?.switchBindVerify != null &&
            AppDefine.systemConfig?.switchBindVerify == '1')
          Padding(
            padding: EdgeInsets.zero,
            child: TextFormField(
              initialValue: controller.selectedBankPwd.value,
              // controller: _bankPasswordController,
              onChanged: (value) {
                controller.selectedBankPwd.value = value;
              },
              decoration: InputDecoration(
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.zero)),
                hintText: '请输入提款密码',
                labelStyle: AppTextStyles.ff666666(context, fontSize: 18),
              ),
              style: AppTextStyles.ff666666(context, fontSize: 18),
            ),
          ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 20.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '收款码',
                style: AppTextStyles.ff666666(context, fontSize: 18),
              ),
              Row(
                children: [
                  Obx(() {
                    if (controller.scanCodeImage.isNotEmpty) {
                      return Stack(
                        children: [
                          SizedBox(
                            width: 150.w,
                            height: 150.h,
                            child: QrImageView(
                              data: controller.scanCodeImage.first,
                              version: QrVersions.auto,
                              size: 150.w,
                            ),
                          ),
                          Positioned(
                            top: 0.w,
                            right: 0.w,
                            child: GestureDetector(
                              onTap: controller.removeImage,
                              child: Container(
                                padding: EdgeInsets.all(4.w),
                                color: AppColors.background,
                                child: Text(
                                  'X',
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 24),
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    }
                    return controller.scanCodeImage.isEmpty ||
                            controller.scanCodeImage.first == ""
                        ? Row(
                            children: [
                              GestureDetector(
                                onTap: controller.scanQRCode,
                                child: Container(
                                  width: 130.w,
                                  height: 130.h,
                                  margin: EdgeInsets.all(8.w),
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: AppColors.ff666666),
                                    color: AppColors.fff7dB88,
                                  ),
                                  child: Center(
                                    child: Text(
                                      '+',
                                      style: AppTextStyles.ff666666(context,
                                          fontSize: 24),
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                '上传二维码图片大小限制2MB之内',
                                style: AppTextStyles.ffff0000_(context,
                                    fontSize: 18),
                              )
                            ],
                          )
                        : const SizedBox();
                  }),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class BankDisplayTips extends StatelessWidget {
  final String? fullName;

  const BankDisplayTips({super.key, required this.fullName});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '真实姓名：$fullName',
            style: AppTextStyles.ff000000(context, fontSize: 18),
          ),
          SizedBox(
            height: 10.h,
          ),
          Text(
            '提示：绑定银行卡必须与姓名一致，否则无法出款',
            style: AppTextStyles.ffff0000_(context, fontSize: 18),
          ),
        ],
      ),
    );
  }
}

class DisplayWxAndAliTips extends StatelessWidget {
  final String? fullName;

  const DisplayWxAndAliTips({super.key, required this.fullName});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '真实姓名：$fullName',
            style: AppTextStyles.ff000000(context, fontSize: 18),
          ),
        ],
      ),
    );
  }
}
