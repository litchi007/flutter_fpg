import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/app/pages/account/pages/manage_bank_list/manage_bank_accounts_view_controller.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/routes.dart';

String accountType(String type) {
  switch (type) {
    case '1':
      return '银行卡';
    case '2':
      return '支付宝';
    case '3':
      return '微信';
    case '4':
      return '虚拟币';
    case '-1':
      return '免提充值';
    default:
      return '未知渠道';
  }
}

class ManageBankListView extends StatefulWidget {
  const ManageBankListView({super.key});

  @override
  State<ManageBankListView> createState() => _ManageBankListViewState();
}

class _ManageBankListViewState extends State<ManageBankListView> {
  final ManageBankAccountsViewController controller =
      Get.put(ManageBankAccountsViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        toolbarHeight: 65.h,
        leading: GestureDetector(
          onTap: () => {
            if (AppDefine.systemConfig?.mobileTemplateCategory == '63' ||
                AppDefine.systemConfig?.mobileTemplateCategory == '38')
              {
                // jumpTo(PageName.LHRTMinePage);
                Navigator.pop(context),
              }
            else if (AppDefine.systemConfig?.mobileTemplateCategory == '64')
              {
                Navigator.pop(context),
              }
            else
              {
                Navigator.pop(context),
                // Get.toNamed(
                //   TemplatePages.getAllPages()[
                //     AppDefine.systemConfig?.mobileTemplateCategory
                //   ].mine,
                // );
              },
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title: Text('我的提款账户',
            style: AppTextStyles.surface_(context, fontSize: 24)),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20).w,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.ffe5c06f,
                foregroundColor: AppColors.surface,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3.w),
                ),
                padding: EdgeInsets.all(0.w),
                fixedSize: Size(60.w, 35.h),
                minimumSize: Size(60.w, 35.h),
              ),
              onPressed: () {
                controller.selectedBankType.value = accountType("1");
                controller.bIsEditing = false.obs;

                Get.toNamed(addBankCardPath,
                    arguments: ['1', '0', false]); //1:bank, 1: blocked
              },
              child: Text(
                '新增',
                style: AppTextStyles.ffffffff(context, fontSize: 18),
              ),
            ),
          )
        ],
      ),
      body: Obx(
        () => (controller.isIniting.value)
            ? const CustomLoadingWidget()
            : controller.arrAllAccountData.isEmpty == true
                ? const SizedBox()
                : DefaultTabController(
                    length: controller.arrAllAccountData.length + 1,
                    child: Column(
                      children: [
                        TabBar(
                          tabs: [
                            ...List.generate(
                                controller.arrAllAccountData.length + 1,
                                (index) {
                              if (index == 0) {
                                return const Tab(text: '全部');
                              } else {
                                return Tab(
                                  text: controller
                                          .arrAllAccountData[index - 1].name ??
                                      "",
                                  // style: AppTextStyles.bodyText1(context, fontSize: 18),
                                );
                              }
                            }),
                          ],
                          labelColor: AppColors.ffff0000,
                          indicatorColor: AppColors.ffff0000,
                          labelStyle:
                              AppTextStyles.bodyText1(context, fontSize: 18),
                        ),
                        Expanded(
                          child: Obx(() => TabBarView(
                                children: [
                                  ...List.generate(
                                      controller.arrAllAccountData.length + 1,
                                      (index) {
                                    if (index == 0) {
                                      return controller.arrAllAccountData.every(
                                              (bank) =>
                                                  bank.data == null ||
                                                  bank.data!.isEmpty)
                                          ? const EmptyView(
                                              text: '点击右上角“新增”添加提款账户吧')
                                          :
                                          // for the first tab - 全部
                                          ListView.builder(
                                              itemCount: controller
                                                  .arrFullBankList.length,
                                              itemBuilder: (context, subInd) {
                                                return RenderItem(
                                                    item: controller
                                                        .arrFullBankList
                                                        .elementAt(subInd));
                                              },
                                            );
                                    } else {
                                      return (controller
                                                      .arrAllAccountData[
                                                          index - 1]
                                                      .data !=
                                                  null &&
                                              controller
                                                      .arrAllAccountData[
                                                          index - 1]
                                                      .data
                                                      ?.isNotEmpty ==
                                                  true)
                                          ? ListView.builder(
                                              itemCount: controller
                                                  .arrAllAccountData[index - 1]
                                                  .data
                                                  ?.length,
                                              itemBuilder: (context, subInd) {
                                                return RenderItem(
                                                    item: controller
                                                            .arrAllAccountData[
                                                                index - 1]
                                                            .data
                                                            ?.elementAt(
                                                                subInd) ??
                                                        BankInfoParam());
                                              },
                                            )
                                          : const EmptyView(
                                              text: '点击右上角“新增”添加提款账户吧');
                                    }
                                  }),
                                ],
                              )),
                        ),
                      ],
                    ),
                  ),
      ),
    );
  }
}

class EmptyView extends StatelessWidget {
  final String text;
  final TextStyle? textStyle;
  final BoxDecoration? decoration;

  const EmptyView({
    super.key,
    required this.text,
    this.textStyle,
    this.decoration,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decoration,
      child: Center(
        child: Text(
          text,
          style: textStyle ?? AppTextStyles.ff000000(context, fontSize: 18),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

class RenderItem extends StatelessWidget {
  final BankInfoParam item;

  const RenderItem({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    String? bankNo;

    if (item.bankCard != null && item.bankCard!.length >= 4) {
      bankNo = item.bankCard?.substring(item.bankCard!.length - 4);
    } else if (item.bankCard == null) {
      bankNo = '';
    }

    // if (item.ownerName == '' || bankNo == null) {
    //   return const SizedBox();
    // }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 15.w),
      child: Container(
        padding: EdgeInsets.all(20.w),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            cardHeader(context, item),
            if (item.type == BankConst.BANK)
              RenderBank(
                item: item,
              ),
            if (item.type == BankConst.BTC)
              RenderBtc(
                item: item,
              ),
            if (item.type == BankConst.ALI)
              RenderAli(
                item: item,
              ),
            if (item.type == BankConst.WX)
              RenderWx(
                item: item,
              ),
          ],
        ),
      ),
    );
  }
}

/// 绘制银行信息
/// @param item

class RenderBank extends StatelessWidget {
  final BankInfoParam item;
  const RenderBank({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    String? bankNo;
    if (item.bankCard != null && item.bankCard!.length >= 4) {
      bankNo = item.bankCard?.substring(item.bankCard!.length - 4);
    } else if (item.bankCard == null) {
      bankNo = '';
    }

    return Padding(
      padding: EdgeInsets.zero,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
          Text(
            '开户姓名: ${item.ownerName ?? ''}',
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
          Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
          Text(
            '开户账号: 尾号${bankNo ?? ''}',
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
          Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
          Text(
            '开户银行: ${item.bankName ?? ''}',
            style: AppTextStyles.ff666666(context, fontSize: 18),
          ),
        ],
      ),
    );
  }
}

/// 绘制虚拟币信息
/// @param item
///
class RenderBtc extends StatelessWidget {
  final BankInfoParam item;

  const RenderBtc({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Text(
          '币种: ${item.bankName ?? ''}',
          style: AppTextStyles.ff666666(context, fontSize: 18),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Text(
          '钱包地址: ${item.bankCard ?? ''}',
          style: AppTextStyles.ff666666(context, fontSize: 18),
        ),
        if (item.bankAddr != null && item.bankAddr!.isNotEmpty)
          Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Text(
          '链名称: ${item.bankAddr ?? ''}',
          style: AppTextStyles.ff666666(context, fontSize: 18),
        ),
      ],
    );
  }
}

/// 绘制微信信息
/// @param item
class RenderWx extends StatelessWidget {
  final BankInfoParam item;

  const RenderWx({super.key, required this.item});

  String getFormattedPhoneNumber(String? bankAddr) {
    if (bankAddr != null && bankAddr.isNotEmpty) {
      return bankAddr.substring(bankAddr.length - 4);
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    String phoneNumber = getFormattedPhoneNumber(item.bankAddr);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Text(
          '真实姓名： ${item.ownerName ?? ''}',
          style: AppTextStyles.ff666666(context, fontSize: 18),
        ),
        if (phoneNumber.isNotEmpty)
          Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Text(
          '绑定手机号: 尾号$phoneNumber',
          style: AppTextStyles.ff666666(context, fontSize: 18),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Row(
          children: [
            SizedBox(
              width: 280.w,
              child: Text(
                '微信号: ${item.bankCard2 ?? ''}',
                style: AppTextStyles.ff666666(context, fontSize: 18),
              ),
            ),
            if (item.bankCode != null)
              QrCodeView(
                item: item,
              ),
          ],
        )
      ],
    );
  }
}

/// 绘制支付宝信息
/// @param item
class RenderAli extends StatelessWidget {
  final BankInfoParam item;

  const RenderAli({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Text(
          '真实姓名： ${item.ownerName}',
          style: AppTextStyles.ff666666(context, fontSize: 18),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 6.h)),
        Row(
          children: [
            SizedBox(
              width: 280.w,
              child: Text(
                '支付宝账户: ${item.bankCard}',
                style: AppTextStyles.ff666666(context, fontSize: 18),
              ),
            ),
            if (item.bankCode != null)
              QrCodeView(
                item: item,
              ),
          ],
        )
      ],
    );
  }
}

///Qr Code
/// @param item
class QrCodeView extends StatelessWidget {
  final BankInfoParam item;

  const QrCodeView({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          '收款码：',
          style: AppTextStyles.ff666666(context, fontSize: 18),
        ),
        item.qrcode == null || (item.qrcode == null && item.qrcode!.isEmpty)
            ? Text(
                '未绑定',
                style: AppTextStyles.ff666666(context, fontSize: 18),
              )
            : AppImage.network(
                img_root('images/qrCode/qrcodeico'),
                width:
                    40.w, // Replace with appropriate scale function if needed
                height: 40.h, // This ensures the aspect ratio is 1
              ),
      ],
    );
  }
}

Widget editIcon(BankInfoParam item, Function(BankInfoParam) clickEdit) {
  bool bShow = false;

  if (item.type == BankConst.BANK &&
      AppDefine.systemConfig?.addEditBankSw == '2') {
    bShow = true;
  } else if (item.type == BankConst.ALI &&
      AppDefine.systemConfig?.addEditAlipaySw == '2') {
    bShow = true;
  } else if (item.type == BankConst.WX &&
      AppDefine.systemConfig?.addEditWechatSw == '2') {
    bShow = true;
  } else if (item.type == BankConst.BTC &&
      AppDefine.systemConfig?.addEditVcoinSw == '2') {
    bShow = true;
  }

  return bShow
      ? Padding(
          padding: EdgeInsets.only(left: 220.w),
          child: Container(
            width: 30.w,
            height: 30.w,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.w),
                border: Border.all(
                  color: AppColors.ff666666,
                )),
            child: GestureDetector(
              onTap: () {
                clickEdit(item);
              },
              child: Icon(
                Icons.edit, // Replace with your desired icon
                size: 20.w,
                color:
                    AppColors.ff666666, // Replace with your desired icon color
              ),
            ),
          ),
        )
      : const SizedBox(); // Empty container if condition is not met
}

Widget cardHeader(BuildContext context, BankInfoParam item) {
  IBankIcon? bankIcon = getBankIcon(item.type);
  String iconType = bankIcon?.uri
          ?.substring((bankIcon.uri?.length ?? 4) - 3, bankIcon.uri?.length) ??
      "";
  ManageBankAccountsViewController controller = Get.find();
  return Padding(
    padding: EdgeInsets.zero,
    child: Row(
      children: [
        if (iconType == 'svg')
          SvgPicture.network(
            bankIcon?.uri ?? "",
            width: 30.w, // Adjust as needed
          )
        else
          AppImage.network(
            bankIcon?.uri ?? "",
            width: 30.w,
            height: 30.h,
            fit: BoxFit.contain,
          ),
        SizedBox(
          width: 20.w,
        ),
        SizedBox(
          width: 80.w,
          child: Text(accountType(item.type ?? '-1'),
              // style: TextStyle(
              //   color: Skin1.isBlack ? changeStyle()[1] : Skin1.textColor5,
              //   // Add more styles as needed
              // ),
              style: AppTextStyles.bodyText1(context, fontSize: 20)),
        ),
        editIcon(item, (item) {
          controller.selectedItem = item.obs;
          controller.selectedBankType.value = accountType(item.type ?? "0");
          controller.bIsEditing = true.obs;
          Get.toNamed(addBankCardPath, arguments: [
            (int.parse(item.type ?? "1") - 1).toString(),
            '0',
            controller.bIsEditing.value
          ]); // 1: blocked
        }),
      ],
    ),
  );
}

// Define IBankIcon interface
class IBankIcon {
  final String? uri;

  IBankIcon({this.uri});
}

// Define getBankIcon function
IBankIcon? getBankIcon(String? type) {
  switch (type) {
    case BankConst.BANK:
      return IBankIcon(uri: img_root('images/bank/ico-bcard', type: 'svg'));
    case BankConst.ALI:
      return IBankIcon(uri: img_root('images/bank/ico-alipay', type: 'svg'));

    case BankConst.WX:
      return IBankIcon(uri: img_root('images/bank/ico-wechat', type: 'svg'));
    case BankConst.BTC:
      return IBankIcon(uri: img_root('images/bank/ico-xnb', type: 'svg'));
    case BankConst.Mtzc:
      return IBankIcon(uri: img_root('images/bank/ico-mtzc', type: 'svg'));
    default:
      return null;
  }
}
