import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/data/models/BankListDetailModel.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:google_mlkit_barcode_scanning/google_mlkit_barcode_scanning.dart';
import 'dart:io';
import 'package:fpg_flutter/routes.dart';

class AddBankAccountViewController extends GetxController {
  RxBool isIniting = true.obs;

  RxList<BankDetailListData> arrBankDetailItems = RxList(); //
  Rx<String> selectedBankItem = "".obs; // Second Dropdown
  Rx<String> selectedBankId = "".obs; // BankId
  Rx<String> selectedBankAddr = "".obs; // BankId
  Rx<String> selectedBankCard = "".obs; // BankId
  Rx<String> selectedBankPwd = "".obs; // BankId
  final AppUserRepository _appUserRepository = AppUserRepository();
  final AppSystemRepository _appSystemRepository = AppSystemRepository();
  bool isUpdating = false;
  UserToken? userToken = AppDefine.userToken;
  UGUserModel? userInfo = GlobalService.to.userInfo.value;

  ///For the Add/Edit Bank detils
  Rx<String> selectedBankType = "".obs; // First Dropdown
  RxList<String> arrBankTypes =
      ['银行卡', '支付宝', '微信', '虚拟币'].obs; // First Dropdown source

  RxList<AllAccountListData> arrAllAccountData = RxList();
  RxList<BankInfoParam> arrFullBankList = RxList();
  RxBool bIsEditing = false.obs;
  Rx<BankInfoParam>? selectedItem;
  String type = '';
  String status = '';

  //QRCode
  RxList<String> scanCodeImage = <String>[].obs;
  RxString qrText = "".obs;

  @override
  void onInit() {
    super.onInit();
    type = Get.arguments[0];
    status = Get.arguments[1];
    bIsEditing.value = Get.arguments[2];

    selectedBankType.value = arrBankTypes[int.parse(type)];
    getBankListDetails(Get.arguments[0], Get.arguments[1]);
  }

  Future<void> scanQRCode() async {
    if (scanCodeImage.isNotEmpty) {
      AppToast.showToast('最多添加1张图片');
      return;
    }
    final ImagePicker picker = ImagePicker();
    final XFile? pickedFile =
        await picker.pickImage(source: ImageSource.gallery);

    String qrText = "";
    if (pickedFile != null) {
      final File file = File(pickedFile.path);
      final inputImage = InputImage.fromFile(file);

      try {
        final barcodeScanner = BarcodeScanner();
        final List<Barcode> barcodes =
            await barcodeScanner.processImage(inputImage);

        if (barcodes.isNotEmpty) {
          qrText = barcodes.first.rawValue ??
              ""; // Assuming you want the first QR code found
          if (qrText != "") {
            scanCodeImage.value = [qrText];
            return;
          }
        } else {
          qrText = '识别二维码失败';
        }
      } catch (e) {
        qrText = '识别二维码失败';
      }
    }
    if (qrText != '') AppToast.showToast(qrText);
    return;
  }

  Future<String?> addFundPwd(String loginPwd, String fundPwd) async {
    String? resMsg = '';
    await _appUserRepository
        .changeFundPwd(loginPwd, fundPwd, token: userToken?.apiSid)
        .then((data) async {
      resMsg = data?.msg;
    });

    return resMsg;
  }

  void removeImage() {
    scanCodeImage.value = [];
  }

  //Bind or Update BankCard
  Future<void> updateBankAccount() async {
    isUpdating = true;
    final int curAccountType = arrBankTypes.indexOf(selectedBankType.value) + 1;

    if (curAccountType == 1 && AppDefine.systemConfig?.addEditBankSw == '0') {
      AppToast.showToast('当前渠道新增功能暂未开放，可联系在线客服咨询');
      return;
    } else if (curAccountType == 2 &&
        AppDefine.systemConfig?.addEditAlipaySw == '0') {
      AppToast.showToast('当前渠道新增功能暂未开放，可联系在线客服咨询');
      return;
    } else if (curAccountType == 3 &&
        AppDefine.systemConfig?.addEditWechatSw == '0') {
      AppToast.showToast('当前渠道新增功能暂未开放，可联系在线客服咨询');
      return;
    } else if (curAccountType == 4 &&
        AppDefine.systemConfig?.addEditVcoinSw == '0') {
      AppToast.showToast('当前渠道新增功能暂未开放，可联系在线客服咨询');
      return;
    } else if ((curAccountType == 2 || curAccountType == 3) &&
        scanCodeImage.isEmpty) {
      AppToast.showToast('请上传正确的付款码');
      return;
    }

    switch (curAccountType) {
      case 1:
        if (selectedBankAddr.value == "") {
          AppToast.showToast('请输入您的银行卡开户行地址');
          return;
        } else if (selectedBankCard.value == "") {
          AppToast.showToast('请输入您的银行卡卡号');
          return;
        } else if (AppDefine.systemConfig?.switchBindVerify != "") {
          if (selectedBankPwd.value == "") {
            AppToast.showToast('请输入取款密码');
            return;
          }
        }
        break;
      case 4: //BankConst.BTC:
        if (selectedBankCard.value == "") {
          AppToast.showToast('请输入您的虚拟币收款钱包地址');
          return;
        } else if (AppDefine.systemConfig?.switchBindVerify != "") {
          if (selectedBankPwd.value == "") {
            AppToast.showToast('请输入取款密码');
            return;
          }
        }
        break;
      case 3: //BankConst.WX:
        if (selectedBankCard.value == "") {
          AppToast.showToast('请输入微信号');
          return;
        } else if (selectedBankAddr.value == "" ||
            selectedBankAddr.value.length < 11) {
          AppToast.showToast('请输入11位手机号码');
          return;
        } else if (AppDefine.systemConfig?.switchBindVerify != "") {
          if (selectedBankPwd.value == "") {
            AppToast.showToast('请输入取款密码');
            return;
          }
        }
        break;
      case 2: //BankConst.ALI:
        if (selectedBankCard.value == "") {
          AppToast.showToast('请输入您的支付宝账号');
          return;
        } else if (AppDefine.systemConfig?.switchBindVerify != null &&
            AppDefine.systemConfig?.switchBindVerify == '1') {
          if (selectedBankPwd.value == "") {
            AppToast.showToast('请输入取款密码');
            return;
          }
        }
        break;
    }
    String? resMsg = '';
    if (bIsEditing.value == false) {
      // 绑定提款账户
      try {
        await _appUserRepository
            .bindBank(
          curAccountType.toString(),
          selectedBankId.value,
          selectedBankAddr.value,
          selectedBankCard.value,
          scanCodeImage.first,
          userInfo?.fullName ?? "",
          // "米米",
          selectedBankPwd.value,
          token: userToken?.apiSid,
        )
            .then((data) async {
          isUpdating = false;

          resMsg = data?.msg ?? "";
          AppLogger.d(resMsg);
        });
      } catch (e) {
        AppLogger.d("Error from Binding Bank Account");
      }
    } else {
      // 绑定提款账户
      try {
        await _appUserRepository
            .editBank(
          curAccountType.toString(),
          selectedBankId.value,
          selectedBankAddr.value,
          selectedBankCard.value,
          scanCodeImage.first,
          userInfo?.fullName ?? "",
          selectedBankPwd.value,
          token: userToken?.apiSid,
        )
            .then((data) {
          isUpdating = false;

          resMsg = data?.msg ?? "";
        });
      } catch (e) {
        AppLogger.d("Error from Editing Bank Account");
      }
    }
    AppToast.showToast(resMsg);
    Get.offNamedUntil(
        manageBankAccountsPath, (route) => route.settings.name == homePath);
  }

  ///Add/Remove BankCard Details
  ///
  //ChangeBankType
  void changeBankType() {
    getBankListDetails(
        (arrBankTypes.indexOf(selectedBankType.value) + 1).toString(), '0');
  }

  ///Change BankId
  void changeBankId() {
    List<BankDetailListData> bankIdArr = arrBankDetailItems.where((item) {
      return item.name == selectedBankItem.value;
    }).toList();
    selectedBankId.value = bankIdArr.first.id ?? "";
  }

  //Change First dropdown
  void getBankListDetails(String? type, String? status) async {
    isIniting.value = true;

    arrBankDetailItems.value = [];
    selectedBankItem.value = "";
    if (bIsEditing.value == true) {
      selectedBankId.value =
          selectedBankId.value == "" ? selectedItem?.value.bankId ?? "" : "";
      selectedBankAddr.value = selectedItem?.value.bankAddr ?? "";
      selectedBankCard.value = selectedItem?.value.bankCard2 ?? "";
      scanCodeImage.value = [selectedItem?.value.qrcode ?? ""];
    } else {
      selectedBankItem.value = "";
      selectedBankAddr.value = "";
      selectedBankCard.value = "";
      scanCodeImage.value = [""];
      selectedBankPwd.value = "";
    }
    _appSystemRepository.BankList(type: type, status: status)
        .then((data) async {
      arrBankDetailItems.value = data ?? [];
      arrBankDetailItems.value =
          arrBankDetailItems.where((BankDetailListData item) {
        return item.code != 'mtzc';
      }).toList();
      selectedBankItem.value = selectedBankItem.value == ""
          ? arrBankDetailItems.elementAt(0).name ?? ""
          : "";
      changeBankId();
      isIniting.value = false;
    });
  }
}
