import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:get/get.dart';

JdrecommendedIncomeViewController controller = Get.find();

List<String>? listLevel = controller.inviteInfo.value.totalMember?.split(',');

List<String> dropdownMenu = [
  "全部下线",
  "一",
  "二",
  "三",
  "四",
  "五",
  "六",
  "七",
  "八",
  "九",
  "十"
];

Map<String, int> levelArray = {
  "全部下线": 0,
  "一": 1,
  "二": 2,
  "三": 3,
  "四": 4,
  "五": 5,
  "六": 6,
  "七": 7,
  "八": 8,
  "九": 9,
  "十": 10
};
