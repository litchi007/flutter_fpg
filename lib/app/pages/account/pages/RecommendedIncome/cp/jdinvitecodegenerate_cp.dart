import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/sysConfModel/InviteCode.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class JDInviteCodeGenerateCP extends StatefulWidget {
  JDInviteCodeGenerateProps? props;
  JDInviteCodeGenerateCP? cref;
  bool? showTop;
  VoidCallback? reloadBlock;

  JDInviteCodeGenerateCP(
      {this.props, this.cref, this.showTop, this.reloadBlock});

  @override
  _JDInviteCodeGenerateCPState createState() => _JDInviteCodeGenerateCPState();
}

class _JDInviteCodeGenerateCPState extends State<JDInviteCodeGenerateCP> {
  late JDInviteCodeGenerateProps props;
  late JDSignInHistoryVars v;
  JdrecommendedIncomeViewController controller = Get.find();
  InviteCode inviteCode = AppDefine.systemConfig!.inviteCode!;
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    v = JDSignInHistoryVars(show: true, length: '0', number: '10');
    controller.getData();

    confirmAction(0);
    initialize();
  }

  void initialize() {}

  void handleTabsChange(int index) {
    debugPrint('选择index == $index');
    setState(() {});
  }

  bool noticeSwitch() {
    if (inviteCode.noticeSwitch == '1')
      return true;
    else
      return false;
  }

  void confirmAction(int tabindex) {
    String? err;
    if (v.length?.isEmpty ?? true) {
      err = '请输入${inviteCode.displayWord}长度';
    } else if (v.number?.isEmpty ?? true) {
      err = '请输入${inviteCode.displayWord}数量';
    } else if (AppDefine.siteId == 'c069' &&
        !((int.tryParse(v.length ?? '') ?? 0) >= 4 &&
            (int.tryParse(v.length ?? '') ?? 0) <= 10)) {
      err = '设置数字在4-10以内';
    }

    if (err != null) {
      print(err);
      return;
    }
    controller.confirmAction(tabindex);
    v.show = controller.v.value.show;
    v.length = controller.v.value.length;
    v.number = controller.v.value.number;
  }

  void radomGenerateAction(int tabindex) {
    controller.radomGenerateAction(tabindex);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width - 55,
        height: inviteCode.noticeSwitch == '1' ? 370 : 270,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: AppColors.ffc5c5c5,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 50,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [AppColors.navBarBgColor, AppColors.navBarBgColor],
                  begin: Alignment.bottomLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Center(
                child: Text(
                  inviteCode.displayWord ?? '',
                  textAlign: TextAlign.center,
                  style: AppTextStyles.ff000000(context, fontSize: 20),
                ),
              ),
            ),
            SizedBox(height: 15.w),
            Row(
              children: <Widget>[
                Container(
                  width: 95.w,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      '${inviteCode.displayWord}类型',
                      textAlign: TextAlign.right,
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              children: <Widget>[
                Container(
                  width: 95,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      inviteCode.displayWord ?? '',
                      textAlign: TextAlign.right,
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: TextField(
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                      decoration: InputDecoration(
                        hintText: '请输入${inviteCode.displayWord}长度',
                        hintStyle:
                            AppTextStyles.ff000000(context, fontSize: 20),
                        filled: true,
                        fillColor: AppColors.textColor4,
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: AppColors.textColor3,
                          ),
                          borderRadius: BorderRadius.circular(3),
                        ),
                      ),
                      onChanged: (text) {
                        setState(() {
                          v!.length = text;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: <Widget>[
                Container(
                  width: 95,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      '生成数量',
                      textAlign: TextAlign.right,
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: TextField(
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                      decoration: InputDecoration(
                        hintText:
                            '最多可生成${inviteCode.canGenNum}${inviteCode.displayWord}',
                        hintStyle:
                            AppTextStyles.ff000000(context, fontSize: 20),
                        filled: true,
                        fillColor: AppColors.textColor4,
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: AppColors.textColor3,
                          ),
                          borderRadius: BorderRadius.circular(3),
                        ),
                      ),
                      onChanged: (text) {
                        setState(() {
                          v!.number = text;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            if (noticeSwitch()) SizedBox(height: 10),
            Row(
              children: <Widget>[
                Container(
                  width: 95,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      '${inviteCode.displayWord}说明:',
                      textAlign: TextAlign.right,
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      inviteCode.noticeText ?? '',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 90,
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: TextButton(
                    onPressed: () {
                      setState(() {
                        v.show = !v.show!;
                      });
                    },
                    child: Text(
                      '取消',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all(AppColors.textColor4),
                      overlayColor: WidgetStateProperty.all(
                          AppColors.textColor1.withOpacity(0.1)),
                      side: WidgetStateProperty.all(BorderSide(
                        color: AppColors.textColor1,
                      )),
                    ),
                  ),
                ),
                Container(
                  width: 150,
                  margin: EdgeInsets.symmetric(horizontal: 10.w),
                  child: TextButton(
                    onPressed: () {
                      radomGenerateAction(tabIndex);
                    },
                    child: Text(
                      '自动生成',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all(AppColors.textColor4),
                      overlayColor: WidgetStateProperty.all(
                          AppColors.textColor1.withOpacity(0.1)),
                      side: WidgetStateProperty.all(BorderSide(
                        color: AppColors.textColor1,
                      )),
                    ),
                  ),
                ),
                Container(
                  width: 90,
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: TextButton(
                    onPressed: () {
                      confirmAction(tabIndex);
                    },
                    child: Text(
                      '确定',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                    style: ButtonStyle(
                      side: WidgetStateProperty.all(BorderSide(
                        color: AppColors.themeColor,
                      )),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class JDInviteCodeGenerateProps {
  JDInviteCodeGenerateCP cref;
  void Function()? reloadBlock;
  bool? showTop;

  JDInviteCodeGenerateProps({
    required this.cref,
    this.reloadBlock,
    this.showTop,
  });
}

class JDSignInHistoryVars {
  bool? show;
  String? length;
  String? number;

  JDSignInHistoryVars({
    this.show,
    this.length,
    this.number,
  });
}
