import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/alertuserinfo_modal.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/basewidgets.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class JDPromotionTabWithDrawlRecordCP extends StatefulWidget {
  const JDPromotionTabWithDrawlRecordCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  JDPromotionTabWithDrawlRecordCPState createState() =>
      JDPromotionTabWithDrawlRecordCPState();
}

class JDPromotionTabWithDrawlRecordCPState
    extends State<JDPromotionTabWithDrawlRecordCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;

  int exceptionLength() {
    try {
      return controller.itemwithrawalReportCP.value.list == null
          ? 0
          : controller.itemwithrawalReportCP.value.list.length;
    } catch (e) {
      return 0;
    }
  }

  List<String> titleArray = [
    '用户',
    '日期',
    '提款金额',
  ];
  String exceptionPageTitle() {
    try {
      return controller.vwithrawalReportCP.value.pageTitle ?? '';
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    AlertUserInfoModal dialog =
        AlertUserInfoModal(context: context, title: "用户信息");
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border:
                    TableBorder.all(color: AppColors.ff999999, width: 0.5.w),
                children: [
                  TableRow(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 8.w, horizontal: 1.w),
                        child: (exceptionPageTitle() != '域名绑定' &&
                                controller.indextabwithrawalReportCP.value == 0)
                            ? Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:
                                        8.0), // Adjust padding if needed
                                child: DropdownButtonFormField<String>(
                                  value: controller
                                      .dropdownvaluetabDrawRcordCP.value,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                  isExpanded: true,
                                  menuMaxHeight: 300.h,
                                  iconSize: 15.w,
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 18),
                                  items: controller.dropdownMenu
                                      .map((String item) {
                                    return DropdownMenuItem<String>(
                                      value: item,
                                      child: Center(
                                          child: Text(
                                        item,
                                        overflow: TextOverflow.visible,
                                      )),
                                    );
                                  }).toList(),
                                  onChanged: (String? newValue) {
                                    controller.dropdownvaluetabDrawRcordCP
                                        .value = newValue ?? '';
                                    controller.getteamWithdrawListData(
                                        level: levelArray[newValue]);
                                  },
                                ),
                              )
                            : TouchableWidget(
                                title: controller.title.value,
                                pageTitle: exceptionPageTitle(),
                                mobileTemplateCategory: AppDefine
                                        .systemConfig?.mobileTemplateCategory ??
                                    '0',
                                isBlack: true,
                                capitalControllerToggle: () {
                                  capitalControllerToggle();
                                },
                                onPressDate: () {
                                  onPressDate();
                                },
                              ),
                      ),
                      ..._renderTitleArray()
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 5.h,
              ),
              Obx(() => controller.isInitingDrawRcordCP.value == true
                  ? const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomLoadingWidget(),
                      ],
                    )
                  : controller.teamWithdrawListData.value.list?.isNotEmpty ==
                          true
                      ? Obx(() => Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          border: TableBorder.all(
                              color: AppColors.ff999999, width: 0.5.w),
                          children: _renderList(
                              controller.teamWithdrawListData.value.list)))
                      : Text('沒有數據',
                          style:
                              AppTextStyles.bodyText2(context, fontSize: 20)))
            ],
          ),
        ),
      ),
    );
  }

  List<TableRow> _renderList(List<M_WithdrawListItem>? list) {
    List<TableRow> temp = [];
    for (M_WithdrawListItem item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvaluetabDrawRcordCP.value),
        _tableCell(item.username ?? ''),
        _tableCell(item.date ?? ''),
        _tableCell(item.amount ?? ''),
      ]));
    }
    return temp;
  }

  Widget _tableCell(
    String title,
  ) {
    return TableCell(
      child: SizedBox(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20)),
          ],
        ),
      ),
    );
  }

  void onHeaderrefresh() {
    controller.vwithrawalReportCP.value.state!.isRefreshing = true;
    controller.vwithrawalReportCP.value.pageNumber = 1;
    controller.teamDepositStatData();
  }

  void onEndReached() {
    if (controller.vwithrawalReportCP.value.state!.showFoot != 0) {
      return;
    }
    if (controller.vwithrawalReportCP.value.state!.isLastPage!) {
      return;
    }
    if (controller.vwithrawalReportCP.value.state!.isRefreshing!) {
      return;
    }
    onFooterRefresh();
  }

  void onFooterRefresh() {
    controller.vwithrawalReportCP.value.pageNumber =
        controller.vwithrawalReportCP.value.pageNumber! + 1;
    controller.vwithrawalReportCP.value.state!.showFoot = 1;
    controller.teamDepositStatData();
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
  List<Widget> _renderTitleArray() {
    return List.generate(titleArray.length, (index) {
      return Text(
        titleArray.elementAt(index),
        textAlign: TextAlign.center,
        style: AppTextStyles.ff000000(context, fontSize: 18),
      );
    });
  }
}
