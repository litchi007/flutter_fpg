import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';

class JDPromotionYJCP extends StatefulWidget {
  final List<UGinviteInfoModel>? list;
  String? selItemContent;
  final VoidCallback? onRoad;

  JDPromotionYJCP({
    required this.list,
    required this.selItemContent,
    required this.onRoad,
  });

  @override
  _JDPromotionYJCPState createState() => _JDPromotionYJCPState();
}

class _JDPromotionYJCPState extends State<JDPromotionYJCP> {
  List<UGinviteInfoModel> _list = [];

  @override
  void initState() {
    super.initState();
    _list = widget.list ?? [];
  }

  void onPressItem(UGinviteInfoModel item) {
    falseItem();
    setState(() {
      item.isPress = true;
      widget.selItemContent = item.content;
    });
    widget.onRoad?.call();
  }

  void falseItem() {
    setState(() {
      for (var item in _list) {
        item.isPress = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: Colors.grey.shade300),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 20.0),
            child: Text(
              '佣金比例',
              style: AppTextStyles.ff000000(context, fontSize: 20),
            ),
          ),
          if (true)
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width - 30,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 100.0,
                      child: Column(
                        children: _list.map((obj) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                falseItem();
                              });
                            },
                            onTapDown: (value) {
                              setState(() {
                                onPressItem(obj);
                              });
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 35.0,
                              decoration: BoxDecoration(
                                border: const Border(
                                  bottom: BorderSide(
                                    width: 1.0,
                                    color: AppColors.ff8D8B8B,
                                  ),
                                ),
                                color: obj.isPress == true
                                    ? Colors.transparent
                                    : AppColors.fffef7ff,
                              ),
                              child: Text(
                                obj.title ?? '',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: obj.isPress!
                                      ? AppColors.ffbfa56d
                                      : AppColors.ffde0b0b,
                                ),
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Text(
                          widget.selItemContent ?? _list.first.content ?? '',
                          style: AppTextStyles.ff000000(context, fontSize: 20),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          SizedBox(height: 10.0),
        ],
      ),
    );
  }
}
