import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class JDPromotionInfoText2CP extends StatelessWidget {
  final String content;

  JDPromotionInfoText2CP({required this.content});

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = AppColors.surface;
    Color textColor = AppColors.ff34650b;

    return Container(
      decoration: BoxDecoration(
        border: const Border(
          bottom: BorderSide(
            color: AppColors.textColor3,
            width: 1,
          ),
        ),
        color: backgroundColor,
      ),
      padding: EdgeInsets.symmetric(vertical: 22.sp),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 0.90.sw,
            margin: EdgeInsets.symmetric(horizontal: 20.sp),
            child: Text(
              content,
              maxLines: 5,
              overflow: TextOverflow.clip,
              style: TextStyle(
                fontSize: 22.sp,
                color: textColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
