import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/alertuserinfo_modal.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/basewidgets.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';

class JDPromotionTabRealityReportCP extends StatefulWidget {
  const JDPromotionTabRealityReportCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  _JDPromotionTabRealityReportCP createState() =>
      _JDPromotionTabRealityReportCP();
}

class _JDPromotionTabRealityReportCP
    extends State<JDPromotionTabRealityReportCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;
  List<String> titleArray = [
    '日期▾',
    '投注金额',
    '会员输赢',
    '返点',
  ];
  int exceptionLength() {
    try {
      return controller.itemtabRealityReportCP.value.list!.length;
    } catch (e) {
      return 0;
    }
  }

  String exceptionPageTitle() {
    try {
      return controller.vtabRealityReportCP.value.pageTitle!;
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    AlertUserInfoModal dialog =
        AlertUserInfoModal(context: context, title: "用户信息");
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border:
                    TableBorder.all(color: AppColors.ff999999, width: 0.5.w),
                children: [
                  TableRow(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 8.w, horizontal: 1.w),
                        child: (exceptionPageTitle() != '域名绑定' &&
                                controller.indextabRealityReportCP.value == 0)
                            ? Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:
                                        8.0), // Adjust padding if needed
                                child: DropdownButtonFormField<String>(
                                  value: controller
                                      .dropdownvaluetabRealityReportCP.value,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                  isExpanded: true,
                                  menuMaxHeight: 300.h,
                                  iconSize: 15.w,
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 18),
                                  items: controller.dropdownMenu
                                      .map((String item) {
                                    return DropdownMenuItem<String>(
                                      value: item,
                                      child: Center(
                                          child: Text(
                                        item,
                                        overflow: TextOverflow.visible,
                                      )),
                                    );
                                  }).toList(),
                                  onChanged: (String? newValue) {
                                    controller.dropdownvaluetabRealityReportCP
                                        .value = newValue ?? '';
                                    controller.getteamRealBetStatData(
                                        level: levelArray[newValue]);
                                  },
                                ),
                              )
                            : TouchableWidget(
                                title: controller.title.value,
                                pageTitle: exceptionPageTitle(),
                                mobileTemplateCategory: AppDefine
                                        .systemConfig?.mobileTemplateCategory ??
                                    '0',
                                isBlack: true,
                                capitalControllerToggle: () {
                                  capitalControllerToggle();
                                },
                                onPressDate: () {
                                  onPressDate();
                                },
                              ),
                      ),
                      ...List.generate(titleArray.length, (index) {
                        return Text(
                          titleArray.elementAt(index),
                          textAlign: TextAlign.center,
                          style: AppTextStyles.ff000000(context, fontSize: 18),
                        );
                      }),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 5.h,
              ),
              Obx(() => controller.isInitingRealityReportCP.value == true
                  ? const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomLoadingWidget(),
                      ],
                    )
                  : controller.teamRealBetStatData.value.list?.isNotEmpty ==
                          true
                      ? Obx(() => Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          border: TableBorder.all(
                              color: AppColors.ff999999, width: 0.5.w),
                          children: _renderList(
                              controller.teamRealBetStatData.value.list)))
                      : Text('沒有數據',
                          style:
                              AppTextStyles.bodyText2(context, fontSize: 20)))
            ],
          ),
        ),
      ),
    );
  }

  List<TableRow> _renderList(List<RealBetStat_Data>? list) {
    List<TableRow> temp = [];
    for (RealBetStat_Data item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvaluetabRealityReportCP.value),
        _tableCell(item.date ?? ''),
        _tableCell(item.validBetAmount ?? ''),
        _tableCell(item.netAmount ?? ''),
        _tableCell(item.fandianSum ?? ''),
      ]));
    }
    return temp;
  }

  Widget _tableCell(
    String title,
  ) {
    return TableCell(
      child: SizedBox(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20)),
          ],
        ),
      ),
    );
  }

  void onHeaderrefresh() {
    controller.vtabRealityReportCP.value.state!.isRefreshing = true;
    controller.vtabRealityReportCP.value.pageNumber = 1;
    controller.teamDepositStatData();
  }

  void onEndReached() {
    if (controller.vtabRealityReportCP.value.state!.showFoot != 0) {
      return;
    }
    if (controller.vtabRealityReportCP.value.state!.isLastPage!) {
      return;
    }
    if (controller.vtabRealityReportCP.value.state!.isRefreshing!) {
      return;
    }
    onFooterRefresh();
  }

  void onFooterRefresh() {
    controller.vtabRealityReportCP.value.pageNumber =
        controller.vtabRealityReportCP.value.pageNumber! + 1;
    controller.vtabRealityReportCP.value.state!.showFoot = 1;
    controller.teamDepositStatData();
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
}
