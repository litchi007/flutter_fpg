import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/alertuserinfo_modal.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/basewidgets.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';

class JDPromotionTabDepostRecordCP extends StatefulWidget {
  const JDPromotionTabDepostRecordCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  JDPromotionTabDepostRecordCPState createState() =>
      JDPromotionTabDepostRecordCPState();
}

class JDPromotionTabDepostRecordCPState
    extends State<JDPromotionTabDepostRecordCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;
  List<String> titleArray = [
    '用户',
    '日期',
    '存款金额',
  ];

  int exceptionLength() {
    try {
      return controller.itemtabDepostCP.value.list.length;
    } catch (e) {
      return 0;
    }
  }

  String exceptionPageTitle() {
    try {
      return controller.itemtabBettingReportCP.value.pageTitle;
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    AlertUserInfoModal dialog =
        AlertUserInfoModal(context: context, title: "用户信息");
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border:
                    TableBorder.all(color: AppColors.ff999999, width: 0.5.w),
                children: [
                  TableRow(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 8.w, horizontal: 1.w),
                        child: (exceptionPageTitle() != '域名绑定' &&
                                controller.indextabDepostCP.value == 0)
                            ? Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:
                                        8.0), // Adjust padding if needed
                                child: DropdownButtonFormField<String>(
                                  value: controller.dropdownvalueDepostCp.value,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                  isExpanded: true,
                                  menuMaxHeight: 300.h,
                                  iconSize: 15.w,
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 18),
                                  items: controller.dropdownMenu
                                      .map((String item) {
                                    return DropdownMenuItem<String>(
                                      value: item,
                                      child: Center(
                                          child: Text(
                                        item,
                                        overflow: TextOverflow.visible,
                                      )),
                                    );
                                  }).toList(),
                                  onChanged: (String? newValue) {
                                    controller.dropdownvalueDepostCp.value =
                                        newValue ?? '';
                                    controller.getteamDepositListData(
                                        level: levelArray[newValue]);
                                  },
                                ),
                              )
                            : TouchableWidget(
                                title: controller.title.value,
                                pageTitle: exceptionPageTitle(),
                                mobileTemplateCategory: AppDefine
                                        .systemConfig?.mobileTemplateCategory ??
                                    '0',
                                isBlack: true,
                                capitalControllerToggle: () {
                                  capitalControllerToggle();
                                },
                                onPressDate: () {
                                  onPressDate();
                                },
                              ),
                      ),
                      ...List.generate(titleArray.length, (index) {
                        return Text(
                          titleArray.elementAt(index),
                          textAlign: TextAlign.center,
                          style: AppTextStyles.ff000000(context, fontSize: 18),
                        );
                      }),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 5.h,
              ),
              Obx(() => controller.isInitingDepositStateCp.value == true
                  ? const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomLoadingWidget(),
                      ],
                    )
                  : controller.teamDepositListData.value.list?.isNotEmpty ==
                          true
                      ? Obx(() => Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          border: TableBorder.all(
                              color: AppColors.ff999999, width: 0.5.w),
                          children: _renderList(
                              controller.teamDepositListData.value.list)))
                      : Text('沒有數據',
                          style:
                              AppTextStyles.bodyText2(context, fontSize: 20)))
            ],
          ),
        ),
      ),
    );
  }

  List<TableRow> _renderList(List<M_DepositListItem>? list) {
    List<TableRow> temp = [];
    for (M_DepositListItem item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvalueDepostCp.value),
        _tableCell(item.username ?? ''),
        _tableCell(item.date ?? ''),
        _tableCell(item.amount ?? ''),
      ]));
    }
    return temp;
  }

  Widget _tableCell(
    String title,
  ) {
    return TableCell(
      child: SizedBox(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20)),
          ],
        ),
      ),
    );
  }

  void onHeaderrefresh() {
    controller.vtabDepostCP.value.state!.isRefreshing = true;
    controller.vtabDepostCP.value.pageNumber = 1;
    controller.teamDepositListData();
  }

  void onEndReached() {
    if (controller.vtabDepostCP.value.state!.showFoot != 0) {
      return;
    }
    if (controller.vtabDepostCP.value.state!.isLastPage!) {
      return;
    }
    if (controller.vtabDepostCP.value.state!.isRefreshing!) {
      return;
    }
    onFooterRefresh();
  }

  void onFooterRefresh() {
    controller.vtabDepostCP.value.pageNumber =
        controller.vtabDepostCP.value.pageNumber! + 1;
    controller.vtabDepostCP.value.state!.showFoot = 1;
    controller.teamDepositListData();
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
}
