import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class JDPromotionInfoText1CP extends StatelessWidget {
  final String? title;
  final String? content;
  final TextAlign textAlign;

  JDPromotionInfoText1CP({
    this.title,
    this.content,
    this.textAlign = TextAlign.right,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 0),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: AppColors.ffC5CBC6),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.only(left: 20.0.w, top: 20.h, bottom: 20.h),
              child: Text(
                title ?? '',
                style: AppTextStyles.ff000000(context, fontSize: 24),
              ),
            ),
          ),
          Container(
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width.w - 150.w,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22.0),
              child: Text(
                content ?? '',
                textAlign: textAlign,
                style: AppTextStyles.ff000000(context, fontSize: 24),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
