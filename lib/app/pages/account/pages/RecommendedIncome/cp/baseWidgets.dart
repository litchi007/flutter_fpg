import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

TableRow unitRow(BuildContext context, List<String> data, int fontsize) {
  return TableRow(
    children: [
      ...List.generate(
        data.length,
        (index) {
          return unitcell(context, data.elementAt(index), fontsize);
        },
      ),
    ],
  );
}

Widget unitcell(BuildContext context, String data, int fontsize) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 15.h),
    child: Text(
      data,
      textAlign: TextAlign.center,
      style: AppTextStyles.ff000000(context, fontSize: fontsize),
    ),
  );
}

Widget renderFooter(int flag) {
  if (flag == 0) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.only(bottom: 150),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.transparent,
          ),
          child: const Text(
            '上拉加载',
            style: TextStyle(color: AppColors.textColor2),
          ),
        ),
      ),
    );
  } else if (flag == 1) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.only(bottom: 150),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.transparent,
          ),
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              Text(
                '正在加载...',
                style: TextStyle(color: AppColors.textColor2),
              ),
            ],
          ),
        ),
      ),
    );
  } else if (flag == 2) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.only(bottom: 150),
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.transparent,
          ),
          child: const Text(
            '',
            style: TextStyle(color: AppColors.textColor2),
          ),
        ),
      ),
    );
  } else {
    return const SizedBox.shrink();
  }
}

class TouchableWidget extends StatelessWidget {
  final String title;
  final String pageTitle;
  final String mobileTemplateCategory;
  final bool isBlack;
  final Function()? capitalControllerToggle;
  final Function()? onPressDate;

  TouchableWidget({
    required this.title,
    required this.pageTitle,
    required this.mobileTemplateCategory,
    required this.isBlack,
    this.capitalControllerToggle,
    this.onPressDate,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (pageTitle != '域名绑定' &&
            title == '日期' &&
            (AppDefine.siteId == 'c309' || AppDefine.siteId!.contains('t59'))) {
          onPressDate?.call();
        } else if (pageTitle != '域名绑定') {
          capitalControllerToggle?.call();
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: AppColors.ff9e9e9e,
              width: 1.0,
            ),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 24.w),
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 16.0.sp,
                  color: mobileTemplateCategory == '0'
                      ? AppColors.ffbe8716
                      : AppColors.ff000000,
                ),
              ),
            ),
            if (pageTitle != '域名绑定' &&
                title == '日期' &&
                (AppDefine.siteId == 'c309' ||
                    AppDefine.siteId!.contains('t59')))
              Image(
                height: 18,
                width: 18,
                image: AssetImage(
                    isBlack ? 'assets/baijiantou1.png' : 'assets/jiantou1.png'),
              ),
          ],
        ),
      ),
    );
  }
}
