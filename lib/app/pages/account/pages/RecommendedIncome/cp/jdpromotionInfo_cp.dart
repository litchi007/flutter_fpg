import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfocopy_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfoimage_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfotext1_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfotext2_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfotext3_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotion_yjcp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';

class JDPromotionInfoCP extends StatefulWidget {
  final UGinviteInfoModel? inviteInfoModel;
  final List<UGinviteInfoModel>? list;
  final String? selItemContent;
  final VoidCallback? onRealPress;
  final VoidCallback? onEarnPress;
  final VoidCallback? onSportsPress;
  final String? tabLabel;

  JDPromotionInfoCP({
    this.inviteInfoModel,
    this.list,
    this.selItemContent,
    this.onRealPress,
    this.onEarnPress,
    this.onSportsPress,
    this.tabLabel,
  });

  @override
  _JDPromotionInfoCPState createState() => _JDPromotionInfoCPState();
}

class _JDPromotionInfoCPState extends State<JDPromotionInfoCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  void teamBetListData() {}

  String yjstring() {
    String returnStr = '';

    if (controller.inviteInfo.value.agentProfitInfo != null) {
      returnStr = controller.inviteInfo.value.agentProfitInfo ?? '';
    }
    return returnStr;
  }

  @override
  Widget build(BuildContext context) {
    AppLogger.d(controller.inviteInfo.value.toJson());
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          JDPromotionInfoImageCP(),
          Obx(() => JDPromotionInfoText1CP(
                title: '我的用户名:',
                content: controller.inviteInfo.value.username,
              )),
          Obx(() => JDPromotionInfoText1CP(
                title: '我的推荐ID:',
                content: controller.inviteInfo.value.rid,
              )),
          JDPromotionInfoText2CP(
            content: '如果想赚取丰厚的推荐佣金，请复制推荐链接发给您的好友注册。（推荐网址请用浏览器打开）',
          ),
          Obx(() => JDPromotionInfoCopyCP(
              title: '首页推荐地址：(点击复制)',
              content: controller.inviteInfo.value.linkI,
              imgUrl: controller.inviteInfo.value.linkI,
              type: 0)),
          Obx(() => JDPromotionInfoCopyCP(
              title: '注册推荐地址：(点击复制)',
              content: controller.inviteInfo.value.linkR,
              imgUrl: controller.inviteInfo.value.linkR,
              type: 1)),
          Obx(() {
            if (controller.listInviteInfo.isNotEmpty) {
              return JDPromotionYJCP(
                list: controller.listInviteInfo,
                selItemContent: widget.selItemContent ?? '',
                onRoad: () {},
              );
            }
            return const SizedBox();
          }),
          Obx(() => JDPromotionInfoText3CP(
                title: '本月推荐收益:',
                content: controller.inviteInfo.value.allEarn?.toString(),
                monthRealEarn: controller.inviteInfo.value.monthRealEarn,
                monthEarn: controller.inviteInfo.value.monthEarn,
                monthSportsEarn: controller.inviteInfo.value.monthMember ?? '',
                onRealPress: () {
                  widget.onRealPress?.call();
                },
                onEarnPress: () {
                  widget.onEarnPress?.call();
                },
                onSportsPress: () {
                  widget.onSportsPress?.call();
                },
              )),
          Obx(() => JDPromotionInfoText1CP(
                title: '本月推荐会员:',
                content: controller.inviteInfo.value.monthMember,
              )),
          // Obx(() => JDPromotionInfoText1CP(
          //       title: '推荐会员总计:',
          //       content: controller.inviteInfo.value.totalMember,
          //     )),
          _totalRecommendedMembers(context, '推荐会员总计:',
              content: controller.inviteInfo.value.totalMember),
          JDPromotionInfoText2CP(
            content: yjstring(),
          ),
          SizedBox(height: 100),
        ],
      ),
    );
  }

  Widget _totalRecommendedMembers(BuildContext context, String? title,
      {String? content}) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: AppColors.ffC5CBC6),
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                title ?? '',
                style: AppTextStyles.ff000000(context, fontSize: 24),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 0.9.sw,
                child: Padding(
                  padding: const EdgeInsets.all(8.0).w,
                  child: Text(content ?? '',
                      style: AppTextStyles.bodyText2(context, fontSize: 20)),
                ),
              )
            ],
          )
        ],
      ).paddingOnly(left: 20.w, right: 20.w),
    );
    // return Container(
    //   padding: const EdgeInsets.only(top: 0),
    //   decoration: const BoxDecoration(
    //     border: Border(
    //       bottom: BorderSide(width: 1.0, color: AppColors.ffC5CBC6),
    //     ),
    //   ),
    //   child: Row(
    //     children: [
    //       Padding(
    //         padding: EdgeInsets.only(left: 20.0.w, top: 20.h, bottom: 20.h),
    //         child: Text(
    //           title ?? '',
    //           style: AppTextStyles.ff000000(context, fontSize: 24),
    //         ),
    //       ),
    //       Padding(
    //         padding: const EdgeInsets.symmetric(horizontal: 22.0),
    //         child: Text(
    //           content ?? '',
    //           textAlign: TextAlign.center,
    //           style: AppTextStyles.ff000000(context, fontSize: 24),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
