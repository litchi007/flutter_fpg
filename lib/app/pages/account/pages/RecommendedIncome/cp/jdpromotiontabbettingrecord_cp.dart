import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/alertuserinfo_modal.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/baseWidgets.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';
import 'package:calendar_date_picker2/calendar_date_picker2.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_alert_dialog.dart';
import 'package:fpg_flutter/configs/constants.dart';

class JDPromotionTabBettingRecordCP extends StatefulWidget {
  const JDPromotionTabBettingRecordCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  _JDPromotionTabBettingRecordCP createState() =>
      _JDPromotionTabBettingRecordCP();
}

class _JDPromotionTabBettingRecordCP
    extends State<JDPromotionTabBettingRecordCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;

  List<String> titleArray = [
    '用户',
    '日期▾',
    '金额',
  ];

  int exceptionLength() {
    try {
      return controller.itemtabBettingReportCP.value.list.length;
    } catch (e) {
      return 0;
    }
  }

  late final CustomAlertDialog customAlertDialog;

  @override
  void initState() {
    super.initState();
    customAlertDialog = CustomAlertDialog(
      context: context,
      title: '消息',
      categories: controller.getCategories(),
      titleBackgroundColor: AppColors.ffd1cfd0,
    );
  }

  String exception_2(String element) {
    String result = '';
    try {
      switch (element) {
        case 'total_bet_sum':
          result = controller.itemtabBettingReportCP.value.total_bet_sum;
        case 'total_sunyi':
          result = controller.itemtabBettingReportCP.value.total_sunyi;
        case 'total_fandian_sum':
          result = controller.itemtabBettingReportCP.value.total_fandian_sum;
        default:
      }
      return result;
    } catch (e) {
      return '';
    }
  }

  String exceptionPageTitle() {
    try {
      return controller.itemtabBettingReportCP.value.pageTitle;
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    AlertUserInfoModal dialog =
        AlertUserInfoModal(context: context, title: "用户信息");
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => controller.isInitingBettingRecord.value == true
              ? const Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CustomLoadingWidget(),
                  ],
                )
              : Column(
                  children: [
                    controller.showCalendar.value == true
                        ? showCalendar()
                        : const SizedBox(),
                    Table(
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      border: TableBorder.all(
                          color: AppColors.ff999999, width: 0.5.w),
                      children: [
                        TableRow(
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8.w, horizontal: 1.w),
                              child: (exceptionPageTitle() != '域名绑定' &&
                                      controller
                                              .indextabBettingreportCP.value ==
                                          0)
                                  ? Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal:
                                              8.0), // Adjust padding if needed
                                      child: DropdownButtonFormField<String>(
                                        value: controller
                                            .dropdownvalueBettingRecord.value,
                                        decoration: const InputDecoration(
                                          border: InputBorder.none,
                                        ),
                                        isExpanded: true,
                                        menuMaxHeight: 300.h,
                                        iconSize: 15.w,
                                        style: AppTextStyles.ff000000(context,
                                            fontSize: 18),
                                        items: controller.dropdownMenu
                                            .map((String item) {
                                          return DropdownMenuItem<String>(
                                            value: item,
                                            child: Center(
                                                child: Text(
                                              item,
                                              overflow: TextOverflow.visible,
                                            )),
                                          );
                                        }).toList(),
                                        onChanged: (String? newValue) {
                                          controller.dropdownvalueBettingRecord
                                              .value = newValue ?? '';
                                          controller.getteamBetListData(
                                              level: levelArray[newValue]);
                                        },
                                      ),
                                    )
                                  : TouchableWidget(
                                      title: controller.title.value,
                                      pageTitle: exceptionPageTitle(),
                                      mobileTemplateCategory: AppDefine
                                              .systemConfig
                                              ?.mobileTemplateCategory ??
                                          '0',
                                      isBlack: true,
                                      capitalControllerToggle: () {
                                        capitalControllerToggle();
                                      },
                                      onPressDate: () {
                                        onPressDate();
                                      },
                                    ),
                            ),
                            ...List.generate(titleArray.length, (index) {
                              if (index == 1) {
                                return Text(
                                  titleArray.elementAt(index),
                                  textAlign: TextAlign.center,
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 18),
                                ).onTap(() => controller.showCalendar.value =
                                    !controller.showCalendar.value);
                              } else {
                                return Text(
                                  titleArray.elementAt(index),
                                  textAlign: TextAlign.center,
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 18),
                                );
                              }
                            }),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Obx(() => controller.isIniting.value == true
                        ? const Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CustomLoadingWidget(),
                            ],
                          )
                        : controller.teamBetListData.value.list?.isNotEmpty ==
                                true
                            ? Obx(() => Table(
                                defaultVerticalAlignment:
                                    TableCellVerticalAlignment.middle,
                                border: TableBorder.all(
                                    color: AppColors.ff999999, width: 0.5.w),
                                children: _renderList(
                                    controller.teamBetListData.value.list)))
                            : Text('沒有數據',
                                style: AppTextStyles.bodyText2(context,
                                    fontSize: 20)))
                  ],
                ),
        ),
      ),
    );
  }

  List<TableRow> _renderList(List<M_BetData_list>? list) {
    List<TableRow> temp = [];
    for (M_BetData_list item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvalueBettingRecord.value, item),
        _tableCell(item.username ?? '', item),
        _tableCell(item.date ?? '', item),
        _tableCell(item.money ?? '', item),
      ]));
    }
    return temp;
  }

  Widget showCalendar() {
    return Container(
        color: AppColors.CLBgColor,
        child: Column(children: [
          CalendarDatePicker2(
            value: controller.singleDatePickerValue,
            config: CalendarDatePicker2Config(
              // selectedDayHighlightColor: Colors.amber[900],
              // weekdayLabels: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
              weekdayLabelTextStyle:
                  AppTextStyles.ff1976D2_(context, fontSize: 20),
              firstDayOfWeek: 1,
              controlsHeight: 50,
              calendarType: CalendarDatePicker2Type.single, // Single mode
            ),
            // Handle the date selection
            onValueChanged: (dates) {
              controller.singleDatePickerValue.value = dates;
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _selectDate('起始时间', 1),
              _selectDate('结束时间', 2),
              _selectDate('开始查询', 3),
            ],
          ).paddingOnly(bottom: 20.h),
        ]));
  }

  Widget _selectDate(String title, int index) {
    return Container(
      width: 100.w,
      height: 50.h,
      decoration: BoxDecoration(
          color: index == 3 ? AppColors.ff9fc0dc : AppColors.surface,
          border: index == controller.calendarDateIndex.value
              ? Border.all(color: AppColors.ff1976D2, width: 0.5)
              : Border.all(color: Colors.transparent),
          borderRadius: BorderRadius.all(Radius.circular(4.w))),
      child: Center(
          child: Text(
        title,
        style: AppTextStyles.ff1976D2_(context, fontSize: 20),
      )),
    ).onTap(() {
      if (index == 3) {
        controller.startQueryDepositStatData();
        controller.showCalendar.value = false;
      } else {
        controller.calendarDateIndex.value = index;
      }
    });
  }

  Widget _tableCell(String title, M_BetData_list row) {
    return GestureDetector(
      onTap: () {
        customAlertDialog.dialogBuilder(_getDataForDialog(row));
      },
      child: SizedBox(
        height: 70.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20)),
          ],
        ),
      ),
    );
  }

  List<dynamic> _getDataForDialog(M_BetData_list row) {
    return [
      row.lotteryName,
      row.date,
      row.actionNo,
      row.actionData,
      row.groupName,
      row.lotteryNo,
      row.odds,
      '${currencyLogo[AppDefine.systemConfig?.currency] ?? ''} ${row.bonus}',
    ];
  }

  void onHeaderrefresh() {
    controller.vtabBettingReportCP.value.state?.isRefreshing = true;
    controller.vtabBettingReportCP.value.pageNumber = 1;
    controller.teamBetStatData();
  }

  void onEndReached() {
    if (controller.vtabBettingReportCP.value.state?.showFoot != 0) {
      return;
    }
    if (controller.vtabBettingReportCP.value.state?.isLastPage ?? false) {
      return;
    }
    if (controller.vtabBettingReportCP.value.state?.isRefreshing ?? false) {
      return;
    }
    onFooterRefresh();
  }

  void onFooterRefresh() {
    controller.vtabBettingReportCP.value.pageNumber =
        controller.vtabBettingReportCP.value.pageNumber ?? 0 + 1;
    controller.vtabBettingReportCP.value.state?.showFoot = 1;
    controller.teamBetStatData();
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
}
