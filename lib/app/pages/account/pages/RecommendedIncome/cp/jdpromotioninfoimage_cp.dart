import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class JDPromotionInfoImageCP extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final sysConf = AppDefine.systemConfig;
    double headImgHeight() {
      double returnNumber;
      returnNumber = 400.w;

      return returnNumber;
    }

    String headImg() {
      String returnStr = '';

      switch (AppDefine.siteId) {
        case 'c126b':
          returnStr = img_images('c126bHeaderBgImg', type: 'jpg');
          break;
        case 'c126':
          returnStr = img_images('c126HeaderBgImg');
          break;
        case 'c186':
          returnStr = img_images('c186HeaderBgImg');
          break;
        case 'c069':
          returnStr = img_images('c069dl');
          break;
        default:
      }
      returnStr = img_images(
        'zh/myreco-mobile-0',
      );
      return returnStr;
    }

    final img = headImg();

    return Center(
      child: sysConf?.myrecoImg == '0'
          ? (img != ''
              ? AppImage.network(
                  img,
                  height: 400.h,
                  width: 1.sw,
                  fit: BoxFit.contain,
                )
              : Container(
                  child: Column(
                    children: [
                      // FastImage equivalent in Flutter
                      AppImage.network(
                        img_images('zh/myreco-mobile-0'),
                        width: 1.sw,
                        fit: BoxFit.fill,
                      ),
                      Text('平台',
                          style: AppTextStyles.ff000000(context, fontSize: 18)),
                      Text('二级返佣',
                          style: AppTextStyles.ffcf352e(context, fontSize: 18)),
                      Text(
                          '您的收益=所有B产生的税收*一级返佣比例+所有C产生的税收*二级返佣比例+所有D产生的税收*三级返佣比例',
                          style: AppTextStyles.ffcf352e(context, fontSize: 18)),
                    ],
                  ),
                ))
          : const SizedBox.shrink(),
    );
  }
}
