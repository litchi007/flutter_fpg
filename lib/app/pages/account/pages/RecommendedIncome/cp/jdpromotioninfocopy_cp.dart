import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/services.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:get/get.dart';

class JDPromotionInfoCopyCP extends StatefulWidget {
  final String? title;
  final String? content;
  final Function(GestureTapCallback)? onPress;
  final Function(bool)? onValueChange;
  final String? imgUrl;
  final int? type;

  JDPromotionInfoCopyCP({
    this.title,
    this.content,
    this.onPress,
    this.onValueChange,
    this.imgUrl,
    this.type,
  });

  @override
  _JDPromotionInfoCopyCPState createState() => _JDPromotionInfoCopyCPState();
}

class _JDPromotionInfoCopyCPState extends State<JDPromotionInfoCopyCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.symmetric(vertical: 8.0.w),
      // padding: EdgeInsets.only(bottom: 8.0.w),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0.w, color: AppColors.ffe0e0e0),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, top: 10),
                child: Text(
                  widget.title ?? '',
                  style: AppTextStyles.bodyText2(context, fontSize: 24),
                ),
              ).onTap(() {
                copyToClipboard(widget.imgUrl ?? '');
              }),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.w),
                child: SizedBox(
                  height: 50.w,
                  child: Text(
                    '显示/关闭二维码',
                    style: AppTextStyles.bodyText2(context, fontSize: 20),
                  ),
                ),
              ).onTap(() {
                controller.switchValue[widget.type!].value =
                    !controller.switchValue[widget.type!].value;
              }),
            ],
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     Expanded(
          //       child: Padding(
          //         padding: const EdgeInsets.symmetric(horizontal: 20.0),
          //         child: Text(
          //           widget.content ?? '',
          //           style: AppTextStyles.bodyText2(context, fontSize: 20),
          //         ),
          //       ),
          //     ),
          //   ],
          // ).onTap(() {
          //   copyToClipboard(widget.content ?? '');
          // }),
          Obx(() => (controller.switchValue[widget.type!].value &&
                  widget.imgUrl != null)
              ? Column(
                  children: [
                    Container(
                      height: 1,
                      color: AppColors.ffe0e0e0,
                    ),
                    Container(
                      height: 200.0.w,
                      // width: double.infinity,
                      alignment: Alignment.centerLeft,
                      child: QrImageView(
                        data: widget.imgUrl ?? '',
                        version: QrVersions.auto,
                        errorCorrectionLevel: QrErrorCorrectLevel.Q,
                        size: 190,
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        '二维码请使用浏览器识别打开',
                        textAlign: TextAlign.left,
                        style: AppTextStyles.bodyText2(context, fontSize: 24),
                      ),
                    ),
                  ],
                )
              : const SizedBox()),
          SizedBox(height: 8.0.w),
        ],
      ),
    );
  }

  void copyToClipboard(String text) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      ToastUtils.show('复制成功');
    });
  }
}
