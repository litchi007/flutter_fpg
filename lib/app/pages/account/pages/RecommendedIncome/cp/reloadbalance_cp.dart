import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

class ReLoadBalanceComponent extends StatefulWidget {
  final String balance;
  final String balanceDecimal;
  final String? currency;
  final String? iconColor;
  final double? size;
  final String? title;
  final bool showTitle;
  final bool hideRefresh;
  final TextStyle? titleStyle;
  final Widget? titleView;
  final TextStyle? balanceStyle;
  final TextStyle? currencyStyle;
  final TextStyle? animatedContainerStyle;
  final bool? showK;
  final String? imageUri;
  final Widget? imgView;
  final String? moreText;
  final bool? smallIcon;
  final bool showCurrency;
  VoidCallback? onRefresh;

  ReLoadBalanceComponent(
      {Key? key,
      required this.balance,
      required this.balanceDecimal,
      this.currency,
      this.iconColor,
      this.size,
      this.title,
      this.showTitle = true,
      this.hideRefresh = false,
      this.titleStyle,
      this.titleView,
      this.balanceStyle,
      this.currencyStyle,
      this.animatedContainerStyle,
      this.showK,
      this.imageUri,
      this.imgView,
      this.moreText,
      this.smallIcon,
      this.showCurrency = true,
      this.onRefresh})
      : super(key: key);

  @override
  _ReLoadBalanceComponentState createState() => _ReLoadBalanceComponentState();
}

class _ReLoadBalanceComponentState extends State<ReLoadBalanceComponent>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  final AuthController authController = Get.find();

  bool _reload = false;
  late String _money;

  @override
  void initState() {
    super.initState();

    _money = widget.balance;

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.linear,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _fetchBalance() {
    if (!GlobalService.to.isAuthenticated.value) {
      return;
    }

    _money = widget.balance;
  }

  void _setMoney(String? newMoney) {
    setState(() {
      _money = newMoney ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minHeight: 25),
      decoration: const BoxDecoration(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.showTitle)
            widget.titleView ??
                Text(
                  widget.title ?? '',
                  style: widget.titleStyle,
                ),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  if (!_reload) {
                    setState(() {
                      _reload = true;
                    });
                    _fetchBalance();
                    _controller.forward(from: 0).then((_) {
                      _controller.reset();
                      setState(() {
                        _reload = false;
                      });
                    });
                  }
                },
                child: Obx(
                  () => Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(right: 5.w),
                      child: Row(
                        children: [
                          Text(
                            !GlobalService.to.isAuthenticated.value
                                ? double.parse(widget.balanceDecimal)
                                    .toStringAsFixed(0)
                                : double.parse(GlobalService
                                            .to.userInfo.value?.balance
                                            ?.toString() ??
                                        '0')
                                    .toString(),
                            style: widget.balanceStyle,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          // Padding(
                          //   padding: EdgeInsets.only(top: 2.w),
                          //   child: const Text(
                          //     'RMB',
                          //     style: TextStyle(
                          //         color: AppColors.ff000000, fontSize: 10),
                          //   ),
                          // )
                        ],
                      )),
                ),
              ),
              if (!widget.hideRefresh)
                AnimatedBuilder(
                  animation: _spinAnimation,
                  builder: (context, child) {
                    return Transform.rotate(
                      angle: _spinAnimation.value * 10 * 3.14159,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                          color: AppColors.ff8acfb8,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: AppColors.surface,
                            width: 2,
                          ),
                        ),
                        child: GestureDetector(
                          onTap: () {
                            if (!_reload) {
                              setState(() {
                                _reload = true;
                              });
                              _fetchBalance();
                              _controller.forward(from: 0).then((_) {
                                _controller.reset();
                                setState(() {
                                  _reload = false;
                                });
                              });
                              widget.onRefresh!();
                            }
                          },
                          child: const Icon(
                            Icons.autorenew_rounded,
                            color: AppColors.surface,
                            size: 12,
                          ),
                        ),
                      ),
                    );
                  },
                ),
            ],
          ),
        ],
      ),
    );
  }
}

bool indexOfSkin(List<String> keys) {
  bool check = false;

  return check;
}
