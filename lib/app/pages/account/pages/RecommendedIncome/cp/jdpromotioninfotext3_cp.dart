import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:get/get.dart';

class JDPromotionInfoText3CP extends StatefulWidget {
  final String? title;
  final String? content;
  final TextAlign textAlign;
  final ValueChanged<bool>? onValueChange;
  final String? monthRealEarn;
  final String? monthEarn;
  final String? monthSportsEarn;
  final VoidCallback? onRealPress;
  final VoidCallback? onEarnPress;
  final VoidCallback? onSportsPress;

  JDPromotionInfoText3CP({
    this.title,
    this.content,
    this.textAlign = TextAlign.right,
    this.onValueChange,
    this.monthRealEarn,
    this.monthEarn,
    this.monthSportsEarn,
    this.onRealPress,
    this.onEarnPress,
    this.onSportsPress,
  });

  @override
  _JDPromotionInfoText3CPState createState() => _JDPromotionInfoText3CPState();
}

class _JDPromotionInfoText3CPState extends State<JDPromotionInfoText3CP> {
  bool switchValue = true;
  bool isPressed_earn = false;
  bool isPressed_real = false;
  JdrecommendedIncomeViewController controller = Get.find();

  String name(String? params) {
    double number = 0;
    try {
      number = double.parse(params ?? '0');
    } catch (e) {
      print('Error parsing string: $params');
    }
    return '${currencyLogo[AppDefine.systemConfig?.currency ?? '']} ${number.toStringAsFixed(2)}';
  }

  Color nameColor(String? params) {
    double number = 0;
    try {
      number = double.parse(params ?? '0');
    } catch (e) {
      print('Error parsing string: $params');
    }
    return number == 0.0 ? AppColors.textColor3 : AppColors.textColor1;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              switchValue = !switchValue;
            });
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20.w),
                child: Text(
                  widget.title ?? '',
                  style: AppTextStyles.ff000000_(context, fontSize: 24),
                ),
              ),
              Row(
                children: [
                  Text(
                    name(widget.content),
                    style: AppTextStyles.bodyText1(context),
                  ),
                  Icon(
                    switchValue ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                    color: AppColors.ff000000,
                    size: 25,
                  ),
                ],
              ),
            ],
          ),
        ),
        if (switchValue)
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              buildEarnButton(),
              const Spacer(),
              buildRealButton(),
            ],
          ).paddingOnly(left: 20.w, right: 20.w),
        SizedBox(height: 8.w),
      ],
    );
  }

  Widget buildEarnButton() {
    return GestureDetector(
      onTap: () {
        double number = double.parse(widget.monthEarn ?? '0');
        if (number == 0) {
          AppToast.showToast('不能跳彩票');
        } else {
          widget.onEarnPress?.call();
        }
        setState(() {
          isPressed_earn = false;
        });
      },
      onTapDown: (value) {
        setState(() {
          isPressed_earn = true;
        });
      },
      child: Container(
        // constraints: BoxConstraints(maxWidth: 250.w),
        width: 220.w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.w),
          color: isPressed_earn ? AppColors.ff9e9e9e : AppColors.ffd1cfd0,
        ),
        padding: EdgeInsets.all(10.w),
        child: Row(
          children: [
            Text('彩票收益: ',
                style: AppTextStyles.bodyText1(context, fontSize: 20)),
            Text(
              name(widget.monthEarn),
              style: AppTextStyles.ff4caf50(context, fontSize: 20),
            ),
            Icon(
              Icons.arrow_right,
              color: nameColor(widget.monthEarn),
              size: 25,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildRealButton() {
    return GestureDetector(
      onTap: () {
        double number = double.parse(widget.monthRealEarn ?? '0');
        if (number == 0) {
          AppToast.showToast('不能跳真人');
        } else {
          widget.onRealPress?.call();
        }
        setState(() {
          isPressed_real = !isPressed_real;
        });
      },
      onTapDown: (value) {
        setState(() {
          isPressed_real = !isPressed_real;
        });
      },
      child: Container(
        width: 220.w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: isPressed_real ? AppColors.ff9e9e9e : AppColors.ffd1cfd0,
        ),
        padding: EdgeInsets.all(10.w),
        child: Row(
          children: [
            Text(
              '真人收益: ',
              style: TextStyle(
                fontSize: 20.sp,
                color: nameColor(widget.monthRealEarn),
              ),
            ),
            Text(
              name(widget.monthRealEarn),
              style: AppTextStyles.ff4caf50(context, fontSize: 20),
            ),
            Icon(
              Icons.arrow_right,
              color: nameColor(widget.monthRealEarn),
              size: 25,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildSportsButton() {
    return GestureDetector(
      onTap: () {
        double number = double.parse(widget.monthSportsEarn ?? '0');
        if (number == 0) {
          AppToast.showToast('不能跳体育');
        } else {
          widget.onSportsPress?.call();
        }
      },
      child: Container(
        constraints: BoxConstraints(maxWidth: 350.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.w),
          color: AppColors.CLBgColor,
        ),
        padding: EdgeInsets.all(10.w),
        margin: EdgeInsets.only(left: 30.w),
        child: Row(
          children: [
            Text(
              '体育收益: ',
              style: TextStyle(
                fontSize: 24,
                color: nameColor(widget.monthSportsEarn),
              ),
            ),
            Text(
              name(widget.monthSportsEarn),
              style: AppTextStyles.ff4caf50(context, fontSize: 24),
            ),
            Icon(
              Icons.arrow_right,
              color: nameColor(widget.monthSportsEarn),
              size: 25,
            ),
          ],
        ),
      ),
    );
  }
}
