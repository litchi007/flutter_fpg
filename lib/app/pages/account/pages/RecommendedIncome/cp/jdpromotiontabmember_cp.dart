import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/alertuserinfo_modal.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';

class JDPromotionTabMemberCP extends StatefulWidget {
  JDPromotionTabMemberCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  _JDPromotionTabMemberCP createState() => _JDPromotionTabMemberCP();
}

class _JDPromotionTabMemberCP extends State<JDPromotionTabMemberCP>
    with TickerProviderStateMixin {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;

  List<String> titleArray = [
    '用户名',
    '状态',
    '最近登录',
    '注册时间',
    '下线盈亏',
    '操作',
  ];
  @override
  Widget build(BuildContext context) {
    AlertUserInfoModal dialog =
        AlertUserInfoModal(context: context, title: "用户信息");
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Table(
                columnWidths: const {
                  0: FlexColumnWidth(1),
                  1: FlexColumnWidth(1),
                  2: FlexColumnWidth(1),
                  3: FlexColumnWidth(1),
                  4: FlexColumnWidth(1),
                  5: FixedColumnWidth(50),
                  6: FixedColumnWidth(60),
                },
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border:
                    TableBorder.all(color: AppColors.ff999999, width: 0.5.w),
                children: [
                  TableRow(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8.0), // Adjust padding if needed
                        child: DropdownButtonFormField<String>(
                          value: controller.dropdownvaluetabMemberCP.value,
                          decoration: const InputDecoration(
                            border: InputBorder.none,
                          ),
                          isExpanded: true,
                          menuMaxHeight: 300.h,
                          iconSize: 15.w,
                          style: AppTextStyles.ff000000(context, fontSize: 18),
                          items: controller.dropdownMenu.map((String item) {
                            return DropdownMenuItem<String>(
                              value: item,
                              child: Center(
                                  child: Text(
                                item,
                                overflow: TextOverflow.visible,
                              )),
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            controller.dropdownvaluetabMemberCP.value =
                                newValue ?? '';
                            RegExp regExp = RegExp(r'\d+');
                            controller.teamInviteListData(
                              level: int.parse(regExp
                                      .firstMatch(newValue ?? '0')
                                      ?.group(0) ??
                                  '0'),
                            );
                          },
                        ),
                      ),
                      ...List.generate(titleArray.length, (index) {
                        return _tableCell(titleArray.elementAt(index),
                            fontSize: 18);
                      }),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 5.h,
              ),
              Obx(() => controller.isInitingMember.value == true
                  ? const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomLoadingWidget(),
                      ],
                    )
                  : controller.inviteListData.value.list?.isNotEmpty == true
                      ? Obx(() => Table(
                              columnWidths: const {
                                0: FlexColumnWidth(1),
                                1: FlexColumnWidth(1),
                                2: FlexColumnWidth(1),
                                3: FlexColumnWidth(1),
                                4: FlexColumnWidth(1),
                                5: FixedColumnWidth(50),
                                6: FixedColumnWidth(60),
                              },
                              defaultVerticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              border: TableBorder.all(
                                  color: AppColors.ff999999, width: 0.5.w),
                              children: _renderList(
                                  controller.inviteListData.value.list)))
                      : Text('沒有數據',
                          style:
                              AppTextStyles.bodyText2(context, fontSize: 20)))
            ],
          ),
        ),
      ),
    );
  }

  List<TableRow> _renderList(List<InviteListItem>? list) {
    List<TableRow> temp = [];
    for (InviteListItem item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvaluetabMemberCP.value),
        _tableCell("${item.username ?? ''} \n${item.name ?? ''}"),
        _tableCell(item.enable ?? ''),
        _tableCell(item.accessTime ?? ''),
        _tableCell(item.regtime ?? ''),
        _tableCell(item.is_online?.toString() ?? ''),
        _tableCell(item.is_setting == '0' ? '' : 'error'),
      ]));
    }
    return temp;
  }

  Widget _tableCell(String title, {int fontSize = 20}) {
    return TableCell(
      child: SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
          ],
        ),
      ),
    );
  }
}
