import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class AlertUserInfoModal {
  final BuildContext context;
  final String title;
  List<Widget>? content;
  List<Widget>? action;
  bool? showPopView;
  VoidCallback? closePop;

  AlertUserInfoModal(
      {required this.context,
      required this.title,
      this.showPopView,
      this.closePop,
      this.content,
      this.action});
  JdrecommendedIncomeViewController controller = Get.find();
  Widget createTitle() {
    return Container(
      padding: EdgeInsets.only(top: 30.w),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0.sp, color: AppColors.ff000000),
        ),
      ),
      child: Text(
        title,
        style: AppTextStyles.bodyText1(context, fontSize: 24),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget createContent() {
    return Container(
      width: 300.w,
      height: 500.w,
      child: Flexible(
        child: SingleChildScrollView(
          child: (content!.isEmpty)
              ? Column(
                  children: [
                    u_content('账户状态:', 'controller.'),
                    u_content('用户姓名:', 'controller.'),
                    u_content('注册时间:', 'controller.'),
                    u_content('上级关系:', 'controller.'),
                    u_content('用户余额:', 'controller.'),
                    u_content('我的余额:', 'controller.'),
                    Text(
                      '充值金额:',
                      textAlign: TextAlign.left,
                      style: AppTextStyles.ff000000(context,
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        hintText: '范围: 0-100000元',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(4.0),
                          borderSide:
                              BorderSide(color: Colors.black, width: 1.0.w),
                        ),
                        contentPadding: EdgeInsets.all(10.0.w),
                        isDense: true, // Reduces the height of the input field
                        // You can adjust other styling properties as needed
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (String text) {
                        // Assuming setMoney is a function to update the state
                        // setMoney(int.parse(text));  // Convert input text to an integer
                      },
                    ),
                    Text(
                      '资金密码:',
                      textAlign: TextAlign.left,
                      style: AppTextStyles.ff000000(context,
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    TextField(
                      obscureText: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(4.0),
                          borderSide:
                              BorderSide(color: Colors.black, width: 1.0.w),
                        ),
                        hintText: '密码******',
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 12.0, horizontal: 12.0.w),
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (text) {},
                    ),
                  ],
                )
              : Column(
                  children: [
                    ...content ?? [],
                  ],
                ),
        ),
      ),
    );
  }

  Widget u_content(String title, String content) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 15.w),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0.sp, color: AppColors.ff000000),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: AppTextStyles.ff000000(context,
                fontSize: 20, fontWeight: FontWeight.bold),
          ),
          Text(
            content,
            textAlign: TextAlign.right,
            style: AppTextStyles.ff000000(context,
                fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

// AspectRatio(
  Widget createActions() {
    return (action!.isEmpty)
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.w),
                child: SizedBox(
                  child: ElevatedButton(
                    child: Text(
                      '取消',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                    onPressed: () {
                      Get.back();
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.surface,
                      padding: EdgeInsets.all(5.w),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: AppColors.ff8e8e8e),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 150.w,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.w),
                child: SizedBox(
                  // height: 50.w,
                  child: ElevatedButton(
                    child: Text(
                      '确认',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.ffcc5c54,
                      padding: EdgeInsets.all(5.w),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: AppColors.ff8e8e8e),
                        //
                        borderRadius: BorderRadius.circular(5.0.w),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        : Row(
            children: [
              ...action ?? [],
            ],
          );
  }

  Dialog createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 1.sw,
            height: 100.w,
            decoration: const BoxDecoration(
                color: AppColors.surface,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: SizedBox(
              width: .8.sw,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: createTitle(),
              ),
            ),
          ),
          SizedBox(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: createContent(),
            ),
          ),
          createActions(),
          SizedBox(
            height: 20.h,
          )
        ],
      ),
    );
  }

  void dialogBuilder() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: createDialog());
        });
  }
}
