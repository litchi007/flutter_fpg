import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/basewidgets.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';

class JDPromotionTabDepositStateCP extends StatefulWidget {
  const JDPromotionTabDepositStateCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  _JDPromotionTabDepositStateCP createState() =>
      _JDPromotionTabDepositStateCP();
}

class _JDPromotionTabDepositStateCP
    extends State<JDPromotionTabDepositStateCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;

  List<String> titleArray = [
    '日期',
    '存款金额',
    '存款人数',
  ];
  int exceptionLength() {
    try {
      return controller.itemtabDepositStateCP.value.list?.length ?? 0;
    } catch (e) {
      return 0;
    }
  }

  String exceptionPageTitle() {
    try {
      return controller.vtabDepositStateCP.value.pageTitle ?? '';
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border:
                    TableBorder.all(color: AppColors.ff999999, width: 0.5.w),
                children: [
                  TableRow(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 8.w, horizontal: 1.w),
                        child: (exceptionPageTitle() != '域名绑定' &&
                                controller.indextabDepositStateCP.value == 0)
                            ? Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:
                                        8.0), // Adjust padding if needed
                                child: DropdownButtonFormField<String>(
                                  value: controller
                                      .dropdownvalueDepositStateCp.value,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                  isExpanded: true,
                                  menuMaxHeight: 300.h,
                                  iconSize: 15.w,
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 18),
                                  items: controller.dropdownMenu
                                      .map((String item) {
                                    return DropdownMenuItem<String>(
                                      value: item,
                                      child: Center(
                                          child: Text(
                                        item,
                                        overflow: TextOverflow.visible,
                                      )),
                                    );
                                  }).toList(),
                                  onChanged: (String? newValue) {
                                    controller.dropdownvalueDepositStateCp
                                        .value = newValue ?? '';
                                    controller.getteamDepositStatData(
                                        level: levelArray[newValue]);
                                  },
                                ),
                              )
                            : TouchableWidget(
                                title: controller.title.value,
                                pageTitle: exceptionPageTitle(),
                                mobileTemplateCategory: AppDefine
                                        .systemConfig?.mobileTemplateCategory ??
                                    '0',
                                isBlack: true,
                                capitalControllerToggle: () {
                                  capitalControllerToggle();
                                },
                                onPressDate: () {
                                  onPressDate();
                                },
                              ),
                      ),
                      ...List.generate(titleArray.length, (index) {
                        return Text(
                          titleArray.elementAt(index),
                          textAlign: TextAlign.center,
                          style: AppTextStyles.ff000000(context, fontSize: 18),
                        );
                      }),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 5.h,
              ),
              Obx(() => controller.isIniting.value == true
                  ? const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomLoadingWidget(),
                      ],
                    )
                  : controller.teamDepositStatData.value.list?.isNotEmpty ==
                          true
                      ? Obx(() => Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          border: TableBorder.all(
                              color: AppColors.ff999999, width: 0.5.w),
                          children: _renderList(
                              controller.teamDepositStatData.value.list)))
                      : Text('沒有數據',
                          style:
                              AppTextStyles.bodyText2(context, fontSize: 20)))
            ],
          ),
        ),
      ),
    );
  }

  List<TableRow> _renderList(List<M_DepositStat_List>? list) {
    List<TableRow> temp = [];
    for (M_DepositStat_List item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvalueDepositStateCp.value),
        _tableCell(item.date ?? ''),
        _tableCell(item.amount ?? ''),
        _tableCell(item.member.toString())
      ]));
    }
    return temp;
  }

  Widget _tableCell(
    String title,
  ) {
    return TableCell(
      child: SizedBox(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20)),
          ],
        ),
      ),
    );
  }

  String getPeopleText(String currentLanCode) {
    switch (currentLanCode) {
      case 'en':
        return 'people';
      case 'vi':
        return 'Người';
      case 'ja':
        return '人';
      case 'th':
        return 'คนคน';
      case 'hi':
        return 'Orang';
      case 'id':
        return 'आदमी';
      case 'zh-cn':
        return '人';
      default:
        return '人';
    }
  }

  void onHeaderrefresh() {
    controller.vtabDepositStateCP.value.state!.isRefreshing = true;
    controller.vtabDepositStateCP.value.pageNumber = 1;
    controller.teamDepositStatData();
  }

  void onEndReached() {
    if (controller.vtabDepositStateCP.value.state!.showFoot != 0) {
      return;
    }
    if (controller.vtabDepositStateCP.value.state!.isLastPage!) {
      return;
    }
    if (controller.vtabDepositStateCP.value.state!.isRefreshing!) {
      return;
    }
    onFooterRefresh();
  }

  void onFooterRefresh() {
    controller.vtabDepositStateCP.value.pageNumber =
        controller.vtabDepositStateCP.value.pageNumber! + 1;
    controller.vtabDepositStateCP.value.state!.showFoot = 1;
    controller.teamDepositStatData();
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
}
