import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/alertuserinfo_modal.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/baseWidgets.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';

class JdPromotionTabSportsBetCP extends StatefulWidget {
  const JdPromotionTabSportsBetCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  _JdPromotionTabSportsBetCPState createState() =>
      _JdPromotionTabSportsBetCPState();
}

class _JdPromotionTabSportsBetCPState extends State<JdPromotionTabSportsBetCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;

  List<String> titleArray = [
    '日期',
    '投注金额',
    '会员输赢',
    '返点',
  ];

  int exceptionLength() {
    try {
      return controller.itemtabBettingReportCP.value.list.length;
    } catch (e) {
      return 0;
    }
  }

  String exception_2(String element) {
    String result = '';
    try {
      switch (element) {
        case 'total_bet_sum':
          result = controller.itemtabBettingReportCP.value.total_bet_sum;
        case 'total_sunyi':
          result = controller.itemtabBettingReportCP.value.total_sunyi;
        case 'total_fandian_sum':
          result = controller.itemtabBettingReportCP.value.total_fandian_sum;
        default:
      }
      return result;
    } catch (e) {
      return '';
    }
  }

  String exceptionPageTitle() {
    try {
      return controller.itemtabBettingReportCP.value.pageTitle;
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    AlertUserInfoModal dialog =
        AlertUserInfoModal(context: context, title: "用户信息");
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border:
                    TableBorder.all(color: AppColors.ff999999, width: 0.5.w),
                children: [
                  TableRow(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 8.w, horizontal: 1.w),
                        child: (exceptionPageTitle() != '域名绑定' &&
                                controller.indextabBettingreportCP.value == 0)
                            ? Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal:
                                        8.0), // Adjust padding if needed
                                child: DropdownButtonFormField<String>(
                                  value: controller
                                      .dropdownvaluetabSportsBetCP.value,
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                  isExpanded: true,
                                  menuMaxHeight: 300.h,
                                  iconSize: 15.w,
                                  style: AppTextStyles.ff000000(context,
                                      fontSize: 18),
                                  items: controller.dropdownMenu
                                      .map((String item) {
                                    return DropdownMenuItem<String>(
                                      value: item,
                                      child: Center(
                                          child: Text(
                                        item,
                                        overflow: TextOverflow.visible,
                                      )),
                                    );
                                  }).toList(),
                                  onChanged: (String? newValue) {
                                    controller.dropdownvaluetabSportsBetCP
                                        .value = newValue ?? '';
                                    controller.getteamBetListData(
                                        level: levelArray[newValue]);
                                  },
                                ),
                              )
                            : TouchableWidget(
                                title: controller.title.value,
                                pageTitle: exceptionPageTitle(),
                                mobileTemplateCategory: AppDefine
                                        .systemConfig?.mobileTemplateCategory ??
                                    '0',
                                isBlack: true,
                                capitalControllerToggle: () {
                                  capitalControllerToggle();
                                },
                                onPressDate: () {
                                  onPressDate();
                                },
                              ),
                      ),
                      ...List.generate(titleArray.length, (index) {
                        return Text(
                          titleArray.elementAt(index),
                          textAlign: TextAlign.center,
                          style: AppTextStyles.ff000000(context, fontSize: 18),
                        );
                      }),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 5.h,
              ),
              Obx(() => controller.isIniting.value == true
                  ? const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomLoadingWidget(),
                      ],
                    )
                  : controller.teamBetListData.value.list?.isNotEmpty == true
                      ? Obx(() => Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          border: TableBorder.all(
                              color: AppColors.ff999999, width: 0.5.w),
                          children: _renderList(
                              controller.teamSportsBetStat.value.list)))
                      : Text('沒有數據',
                          style:
                              AppTextStyles.bodyText2(context, fontSize: 20)))
            ],
          ),
        ),
      ),
    );
  }

  List<TableRow> _renderList(List<RealBetStat_Data>? list) {
    List<TableRow> temp = [];
    for (RealBetStat_Data item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvaluetabSportsBetCP.value),
        _tableCell(item.date ?? ''),
        _tableCell(item.validBetAmount ?? ''),
        _tableCell(item.netAmount ?? ''),
        _tableCell(item.fandianSum ?? ''),
      ]));
    }
    return temp;
  }

  Widget _tableCell(
    String title,
  ) {
    return TableCell(
      child: SizedBox(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20)),
          ],
        ),
      ),
    );
  }

  void onHeaderrefresh() {
    controller.vtabBettingReportCP.value.state?.isRefreshing = true;
    controller.vtabBettingReportCP.value.pageNumber = 1;
    controller.teamBetStatData();
  }

  void onEndReached() {
    if (controller.vtabBettingReportCP.value.state?.showFoot != 0) {
      return;
    }
    if (controller.vtabBettingReportCP.value.state?.isLastPage ?? false) {
      return;
    }
    if (controller.vtabBettingReportCP.value.state?.isRefreshing ?? false) {
      return;
    }
    onFooterRefresh();
  }

  void onFooterRefresh() {
    controller.vtabBettingReportCP.value.pageNumber =
        controller.vtabBettingReportCP.value.pageNumber ?? 0 + 1;
    controller.vtabBettingReportCP.value.state?.showFoot = 1;
    controller.teamBetStatData();
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
}
