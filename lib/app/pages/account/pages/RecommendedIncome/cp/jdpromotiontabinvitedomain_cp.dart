﻿import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/alertuserinfo_modal.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/baseWidgets.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';

class JDPromotionTabInviteDomainCP extends StatefulWidget {
  const JDPromotionTabInviteDomainCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  _JDPromotionTabInviteDomainCP createState() =>
      _JDPromotionTabInviteDomainCP();
}

class _JDPromotionTabInviteDomainCP
    extends State<JDPromotionTabInviteDomainCP> {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;
  List<String> titleArray = [
    '用户',
    '日期',
    '存款金额',
  ];

//域名绑定
  int exceptionLength() {
    try {
      return controller.itemtabInviteDomainCP.value.list?.length ?? 0;
    } catch (e) {
      return 0;
    }
  }

  String exceptionPageTitle() {
    try {
      return controller.vtabInviteDomainCP.value.pageTitle ?? '';
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    AlertUserInfoModal dialog =
        AlertUserInfoModal(context: context, title: "用户信息");
    return Container(
      color: AppColors.surface,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border: TableBorder.all(
                  color: AppColors.ff999999,
                ),
                children: [
                  TableRow(
                    children: [
                      ...List.generate(titleArray.length, (index) {
                        return TableCell(
                            child: SizedBox(
                                height: 80.h,
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        titleArray.elementAt(index),
                                        textAlign: TextAlign.center,
                                        style: AppTextStyles.ff000000(context,
                                            fontSize: 18),
                                      )
                                    ])));
                      }),
                    ],
                  ),
                ],
              ),
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border: TableBorder.all(),
                children: [
                  ...List.generate(exceptionLength(), (index) {
                    dynamic item = controller.itemtabDepositStateCP.value.list!
                        .elementAt(index);
                    return unitRow(
                        context,
                        [
                          'http:// ${item.domain}',
                          'http:// ${item.domain}',
                        ],
                        18);
                  }),
                ],
              ),
              exceptionLength() == 0
                  ? const Text('沒有數據')
                  : SizedBox(
                      height: 20.w,
                    ),
            ],
          ),
        ),
      ),
    );
  }

  void onHeaderrefresh() {
    controller.vtabBettingReportCP.value.state?.isRefreshing = true;
    controller.vtabBettingReportCP.value.pageNumber = 1;
    controller.teamBetStatData();
  }

  void onEndReached() {
    if (controller.vtabBettingReportCP.value.state!.showFoot != 0) {
      return;
    }
    if (controller.vtabBettingReportCP.value.state!.isLastPage!) {
      return;
    }
    if (controller.vtabBettingReportCP.value.state!.isRefreshing!) {
      return;
    }
    onFooterRefresh();
  }

  void onFooterRefresh() {
    controller.vtabBettingReportCP.value.pageNumber =
        controller.vtabBettingReportCP.value.pageNumber! + 1;
    controller.vtabBettingReportCP.value.state!.showFoot = 1;
    controller.teamBetStatData();
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
}
