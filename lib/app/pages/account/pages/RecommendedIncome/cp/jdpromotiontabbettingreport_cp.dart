import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/baseWidgets.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:calendar_date_picker2/calendar_date_picker2.dart';

class JDPromotionTabBettingReportCP extends StatefulWidget {
  const JDPromotionTabBettingReportCP({super.key, required this.tabLabel});
  final String? tabLabel;

  @override
  _JDPromotionTabBettingReportCP createState() =>
      _JDPromotionTabBettingReportCP();
}

class _JDPromotionTabBettingReportCP
    extends State<JDPromotionTabBettingReportCP> with TickerProviderStateMixin {
  JdrecommendedIncomeViewController controller = Get.find();
  int flag = 1;

  List<String> titleArray = [
    '日期▾',
    '投注金额',
    '会员输赢',
    '佣金',
  ];

  int exceptionLength() {
    try {
      return controller.itemtabBettingReportCP.value.list.length;
    } catch (e) {
      return 0;
    }
  }

  String exceptionPageTitle() {
    try {
      return controller.itemtabBettingReportCP.value.pageTitle;
    } catch (e) {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.surface,
        child:

            // This will center the entire content including showCalendar
            SingleChildScrollView(
          child: Obx(() => Column(
                children: [
                  controller.showCalendar.value == true
                      ? showCalendar()
                      : const SizedBox(),
                  Table(
                    columnWidths: const {
                      0: FlexColumnWidth(1.5),
                      1: FlexColumnWidth(1.5),
                      2: FlexColumnWidth(1.5),
                      3: FlexColumnWidth(1.5),
                      4: FlexColumnWidth(1.5),
                      5: FixedColumnWidth(1.5),
                    },
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    border: TableBorder.all(
                        color: AppColors.ff999999, width: 0.5.w),
                    children: [
                      TableRow(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 8.w, horizontal: 1.w),
                            child: (exceptionPageTitle() != '域名绑定' &&
                                    controller.indextabBettingreportCP.value ==
                                        0)
                                ? Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal:
                                            8.0), // Adjust padding if needed
                                    child: DropdownButtonFormField<String>(
                                      value: controller
                                          .dropdownvalueBettingReport.value,
                                      decoration: const InputDecoration(
                                        border: InputBorder.none,
                                      ),
                                      isExpanded: true,
                                      menuMaxHeight: 300.h,
                                      iconSize: 15.w,
                                      style: AppTextStyles.ff000000(context,
                                          fontSize: 18),
                                      items: controller.dropdownMenu
                                          .map((String item) {
                                        return DropdownMenuItem<String>(
                                          value: item,
                                          child: Center(
                                              child: Text(
                                            item,
                                            overflow: TextOverflow.visible,
                                          )),
                                        );
                                      }).toList(),
                                      onChanged: (String? newValue) {
                                        controller.dropdownvalueBettingReport
                                            .value = newValue ?? '';

                                        controller.getteamBetStatData(
                                            level: levelArray[newValue]);
                                      },
                                    ),
                                  )
                                : TouchableWidget(
                                    title: controller.title.value,
                                    pageTitle: exceptionPageTitle(),
                                    mobileTemplateCategory: AppDefine
                                            .systemConfig
                                            ?.mobileTemplateCategory ??
                                        '0',
                                    isBlack: true,
                                    capitalControllerToggle: () {
                                      capitalControllerToggle();
                                    },
                                    onPressDate: () {
                                      onPressDate();
                                    },
                                  ),
                          ),
                          ...List.generate(titleArray.length, (index) {
                            if (index == 0) {
                              return Text(
                                titleArray.elementAt(index),
                                textAlign: TextAlign.center,
                                style: AppTextStyles.ff000000(context,
                                    fontSize: 18),
                              ).onTap(() => controller.showCalendar.value =
                                  !controller.showCalendar.value);
                            } else {
                              return Text(
                                titleArray.elementAt(index),
                                textAlign: TextAlign.center,
                                style: AppTextStyles.ff000000(context,
                                    fontSize: 18),
                              );
                            }
                          }),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Obx(() => controller.isInitingBettingReport.value == true
                      ? const Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CustomLoadingWidget(),
                          ],
                        )
                      : controller.teamBetStatData.value.list?.isNotEmpty ==
                              true
                          ? Obx(() => Table(
                                  columnWidths: const {
                                    0: FlexColumnWidth(1.5),
                                    1: FlexColumnWidth(1.5),
                                    2: FlexColumnWidth(1.5),
                                    3: FlexColumnWidth(1.5),
                                    4: FlexColumnWidth(1.5),
                                    5: FixedColumnWidth(1.5),
                                  },
                                  defaultVerticalAlignment:
                                      TableCellVerticalAlignment.middle,
                                  border: TableBorder.all(
                                      color: AppColors.ff999999, width: 0.5.w),
                                  children: _renderList(
                                    controller.teamBetStatData.value.list,
                                  )))
                          : Text('沒有數據',
                              style: AppTextStyles.bodyText2(context,
                                  fontSize: 20))),
                  // Call the calendar widget here
                ],
              )),
        ),
      ),
    );
  }

  Widget showCalendar() {
    return Container(
        color: AppColors.CLBgColor,
        child: Column(children: [
          CalendarDatePicker2(
            value: controller.singleDatePickerValue,
            config: CalendarDatePicker2Config(
              // selectedDayHighlightColor: Colors.amber[900],
              // weekdayLabels: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
              weekdayLabelTextStyle:
                  AppTextStyles.ff1976D2_(context, fontSize: 20),
              firstDayOfWeek: 1,
              controlsHeight: 50,
              calendarType: CalendarDatePicker2Type.single, // Single mode
            ),
            // Handle the date selection
            onValueChanged: (dates) {
              controller.singleDatePickerValue.value = dates;
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _selectDate('起始时间', 1),
              _selectDate('结束时间', 2),
              _selectDate('开始查询', 3),
            ],
          ).paddingOnly(bottom: 20.h),
        ]));
  }

  Widget _selectDate(String title, int index) {
    return Container(
      width: 100.w,
      height: 50.h,
      decoration: BoxDecoration(
          color: index == 3 ? AppColors.ff9fc0dc : AppColors.surface,
          border: index == controller.calendarDateIndex.value
              ? Border.all(color: AppColors.ff1976D2, width: 0.5)
              : Border.all(color: Colors.transparent),
          borderRadius: BorderRadius.all(Radius.circular(4.w))),
      child: Center(
          child: Text(
        title,
        style: AppTextStyles.ff1976D2_(context, fontSize: 20),
      )),
    ).onTap(() {
      if (index == 3) {
        controller.startQueryBetStatData();
        controller.showCalendar.value = false;
      } else {
        controller.calendarDateIndex.value = index;
      }
    });
  }

  List<TableRow> _renderList(List<BetStat_Data>? list) {
    List<TableRow> temp = [];
    for (BetStat_Data item in list ?? []) {
      temp.add(TableRow(children: [
        _tableCell(controller.dropdownvalueBettingReport.value),
        _tableCell(item.date ?? ''),
        _tableCell(item.betSum ?? ''),
        _tableCell(item.winAmount ?? ''),
        _tableCell(item.fandianSum ?? ''),
      ]));
    }
    return temp;
  }

  Widget _tableCell(
    String title,
  ) {
    return TableCell(
      child: SizedBox(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20)),
          ],
        ),
      ),
    );
  }

  void onPressDate() {}
  void capitalControllerToggle() {}
}
