import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfotext1_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfotext2_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfotext3_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdinvitecodegenerate_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotion_yjcp.dart';
import 'package:fpg_flutter/data/models/sysConfModel/InviteCode.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class JDPromotionInfoYQM extends StatefulWidget {
  final VoidCallback? onRealPress;
  final VoidCallback? onEarnPress;
  final VoidCallback? onSportsPress;
  final String? tabLabel;
  JDPromotionInfoYQM(
      {this.tabLabel, this.onRealPress, this.onEarnPress, this.onSportsPress});
  @override
  _JDPromotionInfoYQMState createState() => _JDPromotionInfoYQMState();
}

class _JDPromotionInfoYQMState extends State<JDPromotionInfoYQM> {
  UGSysConfModel systemInfo = AppDefine.systemConfig!;
  UGinviteInfoModel? inviteInfoModel;
  List<UGinviteInfoModel>? list = [];
  String selItemContent = '';
  JDInviteCodeGenerateCP? codeCP;
  JDInviteCodeGenerateProps? props;
  int pageSize = 50;
  int pageNumber = 1;
  List<InviteCodeModel> items = [];
  int showFoot = 0;
  bool isLastPage = false;
  bool isRefreshing = true;
  String tabLabel = '';

  JdrecommendedIncomeViewController controller = Get.find();
  void teamBetListData() {
    inviteInfoModel = controller.inviteInfo.value;
    YJlist(inviteInfoModel!);
    inviteCodeListData(1, 4);
  }

  void inviteCodeListData(int page, int row) {
    controller.inviteCodeListData(page, row);
    InviteCodeList dicData = controller.inviteCodelist.value;
    if (dicData.list == null) {
      isLastPage = true;
      showFoot = 2;
      isRefreshing = false;
    }
    if (pageNumber == 1) {
      isRefreshing = false;
      if (items == []) {
        items.length = 0;
      }
      if (dicData.list != null) {
        for (int i = 0; i < (dicData.list!.length ?? 0); i++) {
          items.add(InviteCodeModel(
            clsName: 'InviteCodeModel',
            id: dicData.list!.elementAt(i).id,
            created_time: dicData.list!.elementAt(i).createdTime,
            invite_code: dicData.list!.elementAt(i).inviteCode,
            status: dicData.list!.elementAt(i).status,
            uid: dicData.list!.elementAt(i).uid,
            url: dicData.list!.elementAt(i).url,
            used_num: dicData.list!.elementAt(i).usedNum,
            user_type: dicData.list!.elementAt(i).userType,
            user_type_txt: dicData.list!.elementAt(i).userTypeTxt,
          ));
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    controller.getData();
    teamBetListData();
  }

  void updateInviteCodeStatus(int id, int status) {}
  @override
  Widget build(BuildContext context) {
    InviteCode inviteCode = AppDefine.systemConfig!.inviteCode!;
    return Scaffold(
      backgroundColor: AppColors.ffc5c5c5,
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 70,
              color: AppColors.ffc5c5c5,
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      '每个${inviteCode.displayWord}可以使用${inviteCode.canUseNum}次',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Transform.scale(
                    scale: 0.5,
                    child: ElevatedButton(
                      onPressed: () {
                        rightClicked();
                      },
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(120, 50),
                      ),
                      child: Text(
                        '生成${inviteCode.displayWord}',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10.w),
              child: Text(
                '如果想赚取丰厚的佣金，请复制邀请码或者邀请链接给您的好友注册(推荐网址请用浏览器打开)',
                style: AppTextStyles.ff000000(context, fontSize: 20),
              ),
            ),
            if (items != null && items.isNotEmpty)
              flatTopMenu(inviteCode.displayWord!),
            ...List.generate(items.length, (index) {
              return RenderItem(
                index: index,
                item: items.elementAt(index),
              );
            }),
            if (list!.length != 0)
              JDPromotionYJCP(
                list: list,
                selItemContent: selItemContent,
                onRoad: () {},
              ),
            JDPromotionInfoText3CP(
              title: '本月推荐收益:',
              content: inviteInfoModel?.allEarn.toString(),
              monthRealEarn: inviteInfoModel?.monthRealEarn ?? '',
              monthEarn: inviteInfoModel?.monthEarn ?? '',
              onRealPress: () {
                widget.onRealPress?.call();
              },
              onEarnPress: () {
                widget.onEarnPress?.call();
              },
              onSportsPress: () {
                widget.onSportsPress?.call();
              },
            ),
            JDPromotionInfoText1CP(
              title: '本月推荐会员:',
              content: inviteInfoModel?.monthMember ?? '',
              textAlign: TextAlign.left,
            ),
            JDPromotionInfoText1CP(
              title: '推荐会员总计:',
              content: inviteInfoModel?.totalMember ?? '',
              textAlign: TextAlign.left,
            ),
            JDPromotionInfoText2CP(
              content: inviteInfoModel!.agentProfitInfo ?? '',
            ),
            Container(
              color: AppColors.ffc5c5c5,
              height: 100,
            ),
            JDInviteCodeGenerateCP(
              cref: codeCP,
              showTop: true,
              reloadBlock: () {
                onHeaderRefresh();
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> onHeaderRefresh() async {
    controller.isRefreshing.value = controller.isRefreshing.value;
  }

  void rightClicked() {}

  void onEndReached() {}

  void YJlist(UGinviteInfoModel item) {
    if (item.fandianIntro != '') {
      list!.add(UGinviteInfoModel(
        title: '彩票返点',
        content: item.fandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.realFandianIntro != '') {
      list!.add(UGinviteInfoModel(
        title: '真人返点',
        content: item.realFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.fishFandianIntro != '') {
      list!.add(UGinviteInfoModel(
        title: '捕鱼返点',
        content: item.fishFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.gameFandianIntro != '') {
      list!.add(UGinviteInfoModel(
        title: '电子返点',
        content: item.gameFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.esportFandianIntro != '') {
      list!.add(UGinviteInfoModel(
        title: '电竞返点',
        content: item.esportFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.cardFandianIntro != '') {
      list!.add(UGinviteInfoModel(
        title: '体育返点',
        content: item.cardFandianIntro ?? '',
        isPress: false,
      ));
    }

    if (list!.isNotEmpty) {
      selItemContent = list![0].content ?? "";
    }

    setState(() {
      list!.forEach((element) {
        element.isPress = true;
      });
    });
  }
}

class RenderItem extends Container {
  final int index;
  final InviteCodeModel item;
  RenderItem({super.key, required this.index, required this.item});
  void updateInviteCodeStatus(int id, int status) {}

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: AppColors.textColor3,
          ),
        ),
        color: AppColors.textColor4,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              Clipboard.setData(ClipboardData(text: item.invite_code ?? ''));
            },
            child: _renderItem(item.invite_code ?? '', 1.sw / 7, 16.sp),
          ),
          _renderItem(item.user_type_txt ?? '', 1.sw / 10, 16.sp),
          GestureDetector(
            onTap: () {
              Clipboard.setData(ClipboardData(text: item.url ?? ''));
            },
            child: _renderItem(item.url ?? '', 1.sw / 2.5, 16.sp),
          ),
          _renderItem(item.created_time ?? '', 1.sw / 6, 16.sp),
          _renderItem(item.used_num!.toString(), 1.sw / 12, 16.sp),
          Expanded(
            child: GestureDetector(
              onTap: () {
                if (item.status == '1') {
                  updateInviteCodeStatus(int.parse(item.id ?? '0'), 0);
                } else {
                  updateInviteCodeStatus(int.parse(item.id ?? '0'), 1);
                }
              },
              child: Container(
                height: 66,
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                  border: Border(
                    right: BorderSide(
                      width: 1,
                      color: Colors.grey,
                    ),
                  ),
                ),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 6).w,
                  decoration: BoxDecoration(
                    color: item.status == '1'
                        ? AppColors.textColor1
                        : AppColors.textColor2,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    item.status == '1' ? '关闭' : '开启',
                    style: TextStyle(
                      fontSize: 16.sp,
                      color: item.status == '1'
                          ? AppColors.textColor4
                          : AppColors.textColor2,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget renderFooter(int showFoot) {
  switch (showFoot) {
    case 0:
      return GestureDetector(
        onTap: () {},
        child: Container(
          padding: EdgeInsets.all(10.w),
          alignment: Alignment.center,
          child: Text(
            '上拉加载',
          ),
        ),
      );
    case 1:
      return GestureDetector(
        onTap: () {},
        child: Container(
          padding: EdgeInsets.all(10.w),
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              SizedBox(width: 10.w),
              Text(
                '正在加载...',
              ),
            ],
          ),
        ),
      );
    case 2:
      return GestureDetector(
        onTap: () {},
        child: Container(
          padding: EdgeInsets.all(10.w),
          alignment: Alignment.center,
          child: Text(
            '',
          ),
        ),
      );
    default:
      return Container();
  }
}

Widget flatTopMenu(String displayWord) {
  return Container(
    decoration: const BoxDecoration(
      border: Border(
        bottom: BorderSide(
          color: AppColors.textColor3,
          width: 1.0,
        ),
      ),
      color: AppColors.ffc5c5c5,
    ),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildMenuItem(displayWord, 1.sw / 7, 18.sp),
        _buildMenuItem('招募', 1.sw / 10, 16.sp),
        _buildMenuItem('邀请地址', 1.sw / 2.5, 16.sp),
        _buildMenuItem('创建时', 1.sw / 6, 16.sp),
        _buildMenuItem('注册', 1.sw / 12, 18.sp),
        Expanded(
          child: _buildMenuItem('操作', 1.sw / 4, 18.sp),
        ),
      ],
    ),
  );
}

Widget _buildMenuItem(String text, double width, double fontSize) {
  return Container(
    width: width,
    decoration: BoxDecoration(
      border: Border(
        right: BorderSide(
          color: AppColors.ff9e9e9e,
          width: 1,
        ),
      ),
    ),
    height: 100.h,
    alignment: Alignment.center,
    child: Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: fontSize,
        color: AppColors.textColor3,
      ),
    ),
  );
}

Widget _renderItem(String text, double width, double fontSize) {
  return Container(
    width: width,
    height: 100.w,
    alignment: Alignment.center,
    decoration: const BoxDecoration(
      border: Border(
        right: BorderSide(
          width: 1,
          color: Colors.grey,
        ),
      ),
    ),
    child: Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        color: AppColors.textColor3,
      ),
    ),
  );
}

class InviteCodeModel {
  String clsName = 'InviteCodeModel';
  String? id;
  String? created_time;
  String? invite_code;
  String? status;
  String? uid;
  String? url;
  int? used_num;
  String? user_type;
  String? user_type_txt;

  InviteCodeModel({
    this.clsName = 'InviteCodeModel',
    this.id,
    this.created_time,
    this.invite_code,
    this.status,
    this.uid,
    this.url,
    this.used_num,
    this.user_type,
    this.user_type_txt,
  });
}
