import 'package:flutter/material.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/reloadbalance_cp.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/JDRecommendedAddMember.dart';

import 'package:fpg_flutter/routes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class JDUserInfoArea extends StatelessWidget {
  final bool isYYHTemplate;
  JDUserInfoArea({required this.isYYHTemplate});
  bool isChat = true;
  UGSysConfModel? sysInfo = AppDefine.systemConfig;
  UGUserModel? userInfo = GlobalService.to.userInfo.value;
  String? balance;
  String? usr;
  bool? isTest;
  String? fullName;
  String? missionSwitch;
  String? checkinSwitch;
  String? avatar;
  String? frontend_agent_add_member;

  @override
  Widget build(BuildContext context) {
    sysInfo = AppDefine.systemConfig;
    userInfo = GlobalService.to.userInfo.value;
    balance = userInfo?.balance ?? '';
    usr = userInfo?.usr ?? '';
    isTest = userInfo?.isTest ?? false;
    fullName = userInfo?.fullName ?? '';
    missionSwitch = sysInfo?.missionSwitch ?? '';
    checkinSwitch = sysInfo?.checkinSwitch ?? '';
    avatar = userInfo?.avatar ?? '';
    frontend_agent_add_member = sysInfo?.frontendAgentAddMember ?? '';
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 8).w,
          margin: const EdgeInsets.only(top: 8).w,
          decoration: BoxDecoration(
            border: const Border(
              bottom: BorderSide(color: AppColors.ff8D8B8B, width: 0.5),
            ),
            color: isYYHTemplate ? AppColors.surface : Colors.transparent,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Row(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(isChat ? 35 : 0),
                        child: AppImage.network(
                            GlobalService.to.userInfo.value?.isTest == true
                                ? AppDefine.defaultAvatar
                                : GlobalService.to.userInfo.value?.avatar ??
                                    AppDefine.defaultAvatar,
                            width: 75,
                            height: 75,
                            fit: BoxFit.contain),
                      ),
                      SizedBox(width: 1.w),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            usr ?? '',
                            style: TextStyle(
                              color: isYYHTemplate
                                  ? AppColors.surface
                                  : AppColors.ff000000,
                              fontSize: 24.w,
                            ),
                          ),
                          SizedBox(height: 4.w),
                          ReLoadBalanceComponent(
                              title: '用户余额：',
                              balance: balance ?? '0',
                              balanceDecimal: '2',
                              titleStyle: const TextStyle(
                                  color: AppColors.ff000000, fontSize: 12),
                              balanceStyle: const TextStyle(
                                  color: AppColors.error, fontSize: 12),
                              onRefresh: () {}),
                          SizedBox(height: 4.w),
                          Row(
                            children: <Widget>[
                              Text(
                                '真实姓名：',
                                style: TextStyle(
                                  fontSize: 20.w,
                                  color: isYYHTemplate
                                      ? AppColors.surface
                                      : AppColors.textColor2,
                                ),
                              ),
                              Text(
                                fullName ?? '',
                                style: TextStyle(
                                  fontSize: 20.w,
                                  color: isYYHTemplate
                                      ? AppColors.surface
                                      : AppColors.error,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 8).w,
                width: 150.w,
                alignment: Alignment.bottomLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    if (frontend_agent_add_member != '0')
                      _buildImageBtn(
                          img_root('images/zh/addmember'), taskPath, context),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildBalanceRow(String title, double balance, String pageName,
      bool isYYHTemplate, BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          '$title ',
          style: TextStyle(
              fontSize: 20, color: isYYHTemplate ? Colors.red : Colors.yellow),
        ),
        Text(
          '$balance',
          style: TextStyle(color: isYYHTemplate ? Colors.red : Colors.yellow),
        ),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, pageName),
          child: Text(
            '教程',
            style: TextStyle(
                color:
                    isYYHTemplate ? AppColors.surface : AppColors.textColor2),
          ),
        ),
      ],
    );
  }

  Widget _buildImageBtn(
      String imageUrl, String pageName, BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!GlobalService.isShowTestToast) {
          Get.toNamed(addMemberPath);
          JDRecommendedAddMember();
        }
      },
      child: AppImage.network(
        imageUrl,
        width: 150.w,
        height: 40.w,
        fit: BoxFit.contain,
      ),
    );
  }
}
