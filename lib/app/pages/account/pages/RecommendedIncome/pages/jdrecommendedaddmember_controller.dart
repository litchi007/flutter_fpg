import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/api_team.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/http/tools/rsa_encrypt.dart';

class JdrecommendedAddMemberController extends GetxController {
  RxBool isIniting = false.obs;
  RxBool paswordFlag = false.obs;
  RxBool isRefreshing = false.obs;
  final RxList<RxString> stateV = [
    ''.obs,
    ''.obs,
    ''.obs,
    ''.obs,
    ''.obs,
    ''.obs,
    ''.obs
  ].obs; //[regName,regPhone,acc,pwd,cPwd,phone,realname]
  RxBool? checkPwd = false.obs;
  RxBool? pwdShow = true.obs;
  RxBool? cPwdShow = true.obs;
  RxBool? pPwdShow = true.obs;

  final ApiTeam _apiTeam = ApiTeam();

  @override
  void onReady() async {
    super.onReady();
  }

  void addMember(String? pwd) {
    String pwd = stateV[3].value;
    _apiTeam.appHttpClient
        .addMember(token: AppDefine.userToken!.apiSid, pwd: pwd)
        .then((data) {});
    stateV[0].value = AppDefine.systemConfig!.regName ?? '';
    stateV[1].value = AppDefine.systemConfig!.regPhone ?? '';
    stateV[3].value = md5('a123456');
  }

  void insertMember() {
    try {
      if (stateV[2].isEmpty) {
        AppToast.showToast('请输入会员账号');
        return;
      }
      if (stateV[0] == '2') {
        if (stateV[6].isEmpty) {
          AppToast.showToast('请输入真实姓名');
          return;
        }
      }
      if (stateV[3].isEmpty) {
        AppToast.showToast('请输入会员密码');
        return;
      }
      if (stateV[3] != stateV[4]) {
        AppToast.showToast('密码不一致');
        return;
      }
      if (stateV[1] == '2') {
        if (stateV[5].isEmpty) {
          AppToast.showToast('请输入手机号');
          return;
        }
      }
      _apiTeam.appHttpClient
          .addMember(
              token: AppDefine.userToken!.apiSid,
              username: stateV[2].value,
              pwd: md5(stateV[3].value),
              pwd2: md5(stateV[3].value),
              fullName: stateV[6].value,
              phone: stateV[5].value)
          .then((data) {
        AppToast.showDuration(msg: data.msg);
      });
    } catch (e) {}
  }

  void setcheckPwd() {
    try {
      checkPwd!.value = stateV[3].value.trim().isNotEmpty &&
          stateV[4].value.trim().isNotEmpty &&
          stateV[3] != stateV[4];
    } catch (e) {}
  }

  void reset() {
    stateV[2].value = '';
    stateV[3].value = '';
    stateV[4].value = '';
    stateV[5].value = '';
  }
}
