import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/const/RecommendIncomeConst.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdinvitecodegenerate_cp.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/repositories/api_language.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/api_team.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:intl/intl.dart';
import 'package:fpg_flutter/services/global_service.dart';

class JdrecommendedIncomeViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxBool isRefreshing = false.obs;
  RxInt calendarDateIndex = 1.obs;
  RxList<DateTime?> singleDatePickerValue = RxList();
  final ApiTeam _apiTeam = ApiTeam();
  final ApiLanguage _apiLanguage = ApiLanguage();
  RxInt selectedTabIndex = 0.obs;
  RxString title = '推荐信息'.obs;
  Rx<LanguageData> currentLanguageCode = LanguageData().obs;
  RxBool isFirstTabBarSelected = true.obs;
  RxBool showAvatar = true.obs;
  Rx<M_RealBetStat> teamSportsBetStat = M_RealBetStat().obs;
  RxBool showCalendar = false.obs;
  RxString dropdownvalue = '全部下线'.obs;
  RxString pagetitle = '推荐信息'.obs;
  RxString startDate = '2024-03-01'.obs;
  RxString endDate = '2024-07-17'.obs;
  Rx<UGinviteInfoModel> inviteInfo = UGinviteInfoModel().obs;
  Rx<InviteCodeList> inviteCodelist = InviteCodeList().obs;
  Rx<JDSignInHistoryVars> v = JDSignInHistoryVars().obs;
  RxInt tabindex = 0.obs;
  Rx<InviteListModel> inviteListData = InviteListModel().obs;
  Rx<M_BetStat> teamBetStatData = M_BetStat().obs;
  Rx<M_BetList> teamBetListData = M_BetList().obs;
  Rx<M_DepositStatData> teamDepositStatData = M_DepositStatData().obs;
  Rx<M_DepositListtData> teamDepositListData = M_DepositListtData().obs;
  Rx<M_WithdrawStatData> teamWithdrawStatData = M_WithdrawStatData().obs;
  Rx<M_WithdrawListData> teamWithdrawListData = M_WithdrawListData().obs;
  Rx<M_RealBetStat> teamRealBetStatData = M_RealBetStat().obs;
  Rx<M_InviteDomainList> inviteDomainData = M_InviteDomainList().obs;
  Rx<M_RealBetList> teamRealBetListData = M_RealBetList().obs;

  Rx<State_> vtabMember = State_().obs;
  RxBool isInitingMember = false.obs;

  Rx<Current_V> vtabBettingReportCP = Current_V().obs;
  Rx<dynamic> itemtabBettingReportCP = dynamic.obs;
  RxString dropdownvalueBettingReport = '全部下线'.obs;
  RxBool isInitingBettingReport = false.obs;
  RxInt indextabBettingreportCP = 0.obs;
  Rx<Current_V> vtabBettingRecordCP = Current_V().obs;
  Rx<M_BetList> itemtabBettingRecordCP = M_BetList().obs;
  RxString dropdownvalueBettingRecord = '全部下线'.obs;
  RxBool isInitingBettingRecord = false.obs;
  RxInt indextabBettingrecordCP = 0.obs;
  Rx<Current_V> vtabInviteDomainCP = Current_V().obs;
  Rx<dynamic> itemtabInviteDomainCP = M_BetList().obs;
  RxString dropdownvalueInviteDomain = '全部下线'.obs;
  RxBool isInitingInviteDomain = false.obs;
  RxInt indextabInviteDomainCP = 0.obs;
  Rx<Current_V> vtabDepostCP = Current_V().obs;
  Rx<dynamic> itemtabDepostCP = M_BetList().obs;
  RxString dropdownvalueDepostCp = '全部下线'.obs;
  RxBool isInitingDepostCp = false.obs;
  RxInt indextabDepostCP = 0.obs;
  Rx<Current_V> vtabDepositStateCP = Current_V().obs;
  Rx<M_DepositStatData> itemtabDepositStateCP = M_DepositStatData().obs;
  RxString dropdownvalueDepositStateCp = '全部下线'.obs;
  RxBool isInitingDepositStateCp = false.obs;
  RxInt indextabDepositStateCP = 0.obs;
  Rx<Current_V> vwithrawalReportCP = Current_V().obs;
  Rx<dynamic> itemwithrawalReportCP = dynamic.obs;
  RxString dropdownvaluetabwithrawalReportCP = '全部下线'.obs;
  RxBool isInitingwithrawalReportCP = false.obs;
  RxInt indextabwithrawalReportCP = 0.obs;
  Rx<Current_V> vtabDrawRcordCP = Current_V().obs;
  Rx<M_BetList> itemtabDrawRcordCP = M_BetList().obs;
  RxString dropdownvaluetabDrawRcordCP = '全部下线'.obs;
  RxBool isInitingDrawRcordCP = false.obs;
  RxInt indextabDrawRcordCP = 0.obs;
  Rx<Current_V> vtabRealityReportCP = Current_V().obs;
  Rx<dynamic> itemtabRealityReportCP = dynamic.obs;
  RxString dropdownvaluetabRealityReportCP = '全部下线'.obs;
  RxBool isInitingRealityReportCP = false.obs;
  RxInt indextabRealityReportCP = 0.obs;
  Rx<Current_V> vtabRealityRcordCP = Current_V().obs;
  Rx<dynamic> itemtabRealityRcordCP = dynamic.obs;
  RxString dropdownvaluetabRealityRcordCP = '全部下线'.obs;
  RxBool isInitingRealityRcordCP = false.obs;
  RxInt indextabRealityRcordCP = 0.obs;
  RxString dropdownvaluetabMemberCP = '全部下线'.obs;
  RxString dropdownvaluetabSportsBetCP = '全部下线'.obs;

  RxList<UGinviteInfoModel> listInviteInfo = RxList();
  List<RxBool> switchValue = [false.obs, false.obs];
  RxList<String> dropdownMenu = [
    "全部下线",
  ].obs;

  @override
  void onInit() async {
    super.onInit();
    initController();
    singleDatePickerValue.value = [DateTime.now()];
    // singleDatePickerValue
  }

  List<String> getCategories() {
    return [
      '游戏名称:  ',
      '投注日期:  ',
      '投注期数:  ',
      '投注号码:  ',
      '玩法:  ',
      '开奖号码:  ',
      '赔率:  ',
      '中奖金额:  ',
    ];
  }

  void initController() async {
    getData();
    teamInviteListData(level: 0, page: 1, row: 1000);
    getteamBetStatData(level: 0, page: 1, row: 1000);
    getteamBetListData(level: 0, page: 1, row: 1000);
    teamInviteDomainData();
    getteamDepositStatData(level: 0, page: 1, row: 1000);
    getteamDepositListData(
      level: 0,
      page: 1,
      row: 1000,
    );
    getteamWithdrawStatData(level: 0, page: 1, row: 1000);
    getteamWithdrawListData(level: 0, page: 1, row: 1000);
    getteamRealBetStatData(level: 0, page: 1, row: 1000);
    getteamRealBetListData(level: 0, page: 1, row: 1000);
    getteamSportsBetStat(level: 0, page: 1, row: 1000);
  }

  void startQueryBetStatData() {
    if (calendarDateIndex.value == 1) {
      getteamBetStatData(
          level: levelArray[dropdownvalueBettingReport.value],
          page: 1,
          row: 1000,
          startDate: DateFormat('yyyy-MM-dd').format(singleDatePickerValue[0]!),
          endDate: DateFormat('yyyy-MM-dd').format(DateTime.now()));
    } else if (calendarDateIndex.value == 2) {
      getteamBetStatData(
          level: levelArray[dropdownvalueBettingReport.value],
          page: 1,
          row: 1000,
          endDate: DateFormat('yyyy-MM-dd').format(singleDatePickerValue[0]!));
    }
  }

  void startQueryDepositStatData() {
    if (calendarDateIndex.value == 1) {
      getteamBetListData(
          level: levelArray[dropdownvalueBettingReport.value],
          page: 1,
          row: 1000,
          startDate: DateFormat('yyyy-MM-dd').format(singleDatePickerValue[0]!),
          endDate: DateFormat('yyyy-MM-dd').format(DateTime.now()));
    } else if (calendarDateIndex.value == 2) {
      getteamBetListData(
          level: levelArray[dropdownvalueBettingReport.value],
          page: 1,
          row: 1000,
          endDate: DateFormat('yyyy-MM-dd').format(singleDatePickerValue[0]!));
    }
  }

  void YJlist(UGinviteInfoModel item) {
    listInviteInfo.clear();
    if (item.fandianIntro != null && item.fandianIntro != "") {
      listInviteInfo.add(UGinviteInfoModel(
        title: '彩票返点',
        content: item.fandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.realFandianIntro != null && item.realFandianIntro != '') {
      listInviteInfo.add(UGinviteInfoModel(
        title: '真人返点',
        content: item.realFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.fishFandianIntro != null && item.fishFandianIntro != '') {
      listInviteInfo.add(UGinviteInfoModel(
        title: '捕鱼返点',
        content: item.fishFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.gameFandianIntro != null && item.gameFandianIntro != '') {
      listInviteInfo.add(UGinviteInfoModel(
        title: '电子返点',
        content: item.gameFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.esportFandianIntro != null && item.esportFandianIntro != '') {
      listInviteInfo.add(UGinviteInfoModel(
        title: '电竞返点',
        content: item.esportFandianIntro ?? '',
        isPress: false,
      ));
    }
    if (item.cardFandianIntro != null && item.cardFandianIntro != '') {
      listInviteInfo.add(UGinviteInfoModel(
        title: '体育返点',
        content: item.cardFandianIntro ?? '',
        isPress: false,
      ));
    }

    if (listInviteInfo.isNotEmpty) {
      listInviteInfo[0].isPress = true;
    }
  }

  void getData() async {
    isIniting.value = true;
    try {
      try {
        await _apiTeam.appHttpClient
            .inviteInfo(token: AppDefine.userToken?.apiSid)
            .then((data) {
          isIniting.value = false;
          inviteInfo.value = data.data ?? UGinviteInfoModel();

          List<String> listLevel =
              inviteInfo.value.totalMember?.split(', ') ?? [];

          for (var i in listLevel) {
            dropdownMenu.add(i.substring(0, 1));
          }
          YJlist(inviteInfo.value);
        });
      } catch (e) {
        AppLogger.e(e);
      }
    } catch (e) {
      AppLogger.e(e);
    }
  }

  String getCurrentDate(int daysBeforeNow) {
    DateTime now = DateTime.now();

    DateTime adjustedDate = now.subtract(Duration(days: daysBeforeNow));

    String formattedCurrentDate = DateFormat('yyyy-MM-dd').format(adjustedDate);

    return formattedCurrentDate;
  }

  void getteamSportsBetStat({int? level, int? page, int? row}) async {
    isInitingRealityReportCP.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .sportsBetStat(
      token: AppDefine.userToken?.apiSid,
      level: level,
      // startDate: getCurrentDate(100),
      // endDate: getCurrentDate(0),
      page: page,
      rows: row,
    )
        .then((data) {
      isInitingRealityReportCP.value = false;
      teamSportsBetStat.value = data ?? M_RealBetStat();
    });
  }

  void inviteCodeListData(int page, int row) async {
    try {
      page = 1;
      row = 1000;
      _apiTeam.appHttpClient
          .inviteCodeList(
              token: AppDefine.userToken?.apiSid, page: page, rows: row)
          .then((data) {
        inviteCodelist.value = data.data ?? InviteCodeList();
      });
    } catch (e) {
      AppLogger.e(e);
    }
  }

  void updateInviteCodeStatus(int id, int status) async {
    try {
      _apiTeam.appHttpClient
          .updateInviteCodeStatus(id: id, status: status)
          .then((data) {});
    } catch (e) {
      AppLogger.e(e);
    }
  }

  void confirmAction(int tabIndex) async {
    try {
      _apiTeam.appHttpClient
          .createInviteCode(
        token: AppDefine.userToken?.apiSid,
        length: int.parse(v.value.length ?? '0'),
        count: int.parse(v.value.number ?? '0'),
        user_type: tabIndex,
      )
          .then((data) {
        v.value.show = !(v.value.show ?? false);
        v.value.length = '';
        v.value.number = '';
      });
    } catch (e) {
      AppLogger.e(e);
    }
  }

  void radomGenerateAction(int tabIndex) async {
    try {
      _apiTeam.appHttpClient
          .createInviteCode(
              token: AppDefine.userToken?.apiSid,
              length: 4,
              count: 1,
              user_type: tabIndex,
              randomCheck: 1)
          .then((data) {
        v.value.show = !(v.value.show ?? false);
      });
    } catch (e) {
      AppLogger.e(e);
    }
  }

  void teamInviteListData({int? level, int? page, int? row}) async {
    isInitingMember.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .inviteList(
      token: AppDefine.userToken?.apiSid,
      level: level,
      page: page,
      rows: row,
    )
        .then((data) {
      inviteListData.value = data ?? InviteListModel();
      isInitingMember.value = false;
    });
  }

  void getteamBetStatData(
      {int? level,
      int? page,
      int? row,
      String? startDate,
      String? endDate}) async {
    isInitingBettingReport.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .betStat(
            level: level,
            page: page,
            rows: row,
            startDate: startDate,
            endDate: endDate)
        .then((data) {
      isInitingBettingReport.value = false;

      teamBetStatData.value = data ?? M_BetStat();
    });
  }

  void getteamBetListData(
      {int? level,
      int? page,
      int? row,
      String? startDate,
      String? endDate}) async {
    isInitingBettingRecord.value = true;

    page = 1;
    row = 1000;

    _apiTeam
        .betList(
            level: level,
            page: page,
            rows: row,
            startDate: startDate,
            endDate: endDate)
        .then((data) {
      isInitingBettingRecord.value = false;
      teamBetListData.value = data ?? M_BetList();
    });
  }

  void getteamDepositStatData({int? level, int? page, int? row}) async {
    isInitingDepositStateCp.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .depositStat(
      level: level,
      page: page,
      rows: row,
    )
        .then((data) {
      isInitingDepositStateCp.value = false;
      teamDepositStatData.value = data ?? M_DepositStatData();
    });
  }

  void getteamDepositListData({int? level, int? page, int? row}) async {
    isInitingDepostCp.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .depositList(
      level: level,
      page: page,
      rows: row,
    )
        .then((data) {
      isInitingDepostCp.value = false;
      teamDepositListData.value = data ?? M_DepositListtData();
    });
  }

  void getteamWithdrawListData({int? level, int? page, int? row}) async {
    isInitingDrawRcordCP.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .withdrawList(
      level: level,
      page: page,
      rows: row,
    )
        .then((data) {
      isInitingDrawRcordCP.value = false;
      teamWithdrawListData.value = data ?? M_WithdrawListData();
    });
  }

  void getteamWithdrawStatData({int? level, int? page, int? row}) async {
    isInitingwithrawalReportCP.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .withdrawStat(
      level: level,
      page: page,
      rows: row,
    )
        .then((data) {
      isInitingwithrawalReportCP.value = false;
      teamWithdrawStatData.value = data ?? M_WithdrawStatData();
    });
  }

  void getteamRealBetStatData({int? level, int? page, int? row}) async {
    isInitingRealityReportCP.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .realBetStat(
      level: level,
      page: page,
      rows: row,
    )
        .then((data) {
      isInitingRealityReportCP.value = false;
      teamRealBetStatData.value = data ?? M_RealBetStat();
    });
  }

  void getteamRealBetListData({int? level, int? page, int? row}) async {
    isInitingRealityRcordCP.value = true;
    page = 1;
    row = 1000;
    _apiTeam
        .realBetList(
      level: level,
      page: page,
      rows: row,
    )
        .then((data) {
      isInitingRealityRcordCP.value = false;
      teamRealBetListData.value = data ?? M_RealBetList();
    });
  }

  void teamInviteDomainData({int? page, int? row}) async {
    page = 1;
    row = 1000;
    try {
      _apiTeam
          .inviteDomain(
        token: AppDefine.userToken?.apiSid,

        //ex:'2024-07-17'
        page: page,
        rows: row,
      )
          .then((data) {
        inviteDomainData.value = data ?? M_InviteDomainList();
      });
    } catch (e) {
      AppLogger.e(e);
    }
  }

  void team(Rx<Current_V> v, data) {
    List<dynamic> arrayData = data.data?.list ?? [];

    if (arrayData.length == 0) {
      v.value.state?.isLastPage = true;
      v.value.state?.showFoot = 2;
      v.value.state?.isRefreshing = false;
    }
    if (v.value.pageNumber == 1) {
      v.value.state?.isRefreshing = false;
      v.value.items?.clear();
      v.value.items?.addAll(arrayData);
    } else {
      v.value.items?.addAll(arrayData);
    }

    v.value.state?.showFoot = 0;
    if (arrayData.length < (vtabInviteDomainCP.value.pageSize ?? 0)) {
      v.value.state?.isLastPage = true;
      v.value.state?.showFoot = 2;
    } else {
      v.value.state?.isLastPage = false;
      v.value.state?.showFoot = 0;
    }
  }

  dynamic returnData(dynamic data) {
    if (data is List) {
      return data;
    } else {
      return data['list'];
    }
  }
}

class State_ {
  int? showFoot = 0;
  bool? isRefreshing = true;
  bool? isLastPage = false;
  State_({
    this.showFoot,
    this.isRefreshing,
    this.isLastPage,
  });
}

class Current_V {
  String? pageTitle;
  List<String>? titleArray;
  List<dynamic>? items;
  List<String>? levelArray;
  int? pageSize = 20;
  int? pageNumber = 1;
  int? levelindex = 1;
  State_? state;
}
