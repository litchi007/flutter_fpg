import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/widgets/alertfrom_html.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedaddmember_controller.dart';

import 'package:get/get.dart';

class JDRecommendedAddMember extends StatefulWidget {
  const JDRecommendedAddMember({super.key});

  @override
  State<JDRecommendedAddMember> createState() => _JDRecommendedAddMember();
}

class _JDRecommendedAddMember extends State<JDRecommendedAddMember> {
  JdrecommendedAddMemberController controller =
      Get.put(JdrecommendedAddMemberController());
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  int selectedGender = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Text('添加会员',
              style: AppTextStyles.ffffffff(context, fontSize: 25)),
        ),
        centerTitle: true,
      ),
      body: Obx(
        () => Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // SizedBox(
            //   child: Row(
            //     children: [
            //       Text(
            //         '添加类型：',
            //         style: AppTextStyles.bodyText2(context, fontSize: 20),
            //       ),selectedGender
            SizedBox(
              height: 54.h,
              child: Row(
                children: [
                  Text('添加类型：',
                      style: AppTextStyles.bodyText2(context, fontSize: 20)),
                  Radio(
                    fillColor: WidgetStateProperty.all(AppColors.ff387EF5),
                    value: selectedGender,
                    groupValue: selectedGender,
                    onChanged: (value) {},
                  ),
                  Text('会员',
                      style: AppTextStyles.bodyText2(context, fontSize: 20)),
                ],
              ).paddingOnly(left: 10.w),
            ),
            //     ],
            //   ),
            // ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.w),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AddMemberInput(
                      recolor: AppColors.onBackground,
                      placeholder: '输入会员账号',
                      isPwd: false,
                      text: controller.stateV[2].value,
                      onChangeText: (data) {
                        controller.stateV[2].value = data;
                      },
                    ),
                    Text('* 用户名由6-16位的字母或数字组成',
                        style: AppTextStyles.error_(context, fontSize: 20)),
                    if (controller.stateV[0].value != '0')
                      AddMemberInput(
                        recolor: AppColors.onBackground,
                        placeholder: '请输入真实姓名',
                        isPwd: false,
                        text: controller.stateV[6].value,
                        onChangeText: (data) {
                          controller.stateV[6].value = data;
                        },
                      ),
                    if (controller.stateV[0].value != '0')
                      Text(
                          controller.stateV[0].value == '1'
                              ? '* 请输入真实姓名（选填）'
                              : '* 真实姓名必须为汉字，2-12个汉字',
                          style: AppTextStyles.error_(context, fontSize: 20)),
                    AddMemberInput(
                      recolor: AppColors.onBackground,
                      placeholder: '输入会员密码',
                      isPwd: controller.pwdShow!.value,
                      text: controller.stateV[3].value,
                      iconShow: true,
                      onChangeText: (data) {
                        controller.stateV[3].value = data;
                        controller.setcheckPwd();
                      },
                      showPassword: () {
                        controller.pwdShow?.value = !controller.pwdShow!.value;
                      },
                    ),
                    AddMemberInput(
                      recolor: AppColors.onBackground,
                      placeholder: '请确认密码',
                      isPwd: controller.cPwdShow!.value,
                      iconShow: true,
                      text: controller.stateV[4].value,
                      onChangeText: (data) {
                        controller.stateV[4].value = data;
                      },
                      showPassword: () {
                        controller.cPwdShow!.value =
                            !controller.cPwdShow!.value;
                      },
                    ),
                    (controller.stateV[3] != controller.stateV[4])
                        ? Text('* 密码不一致',
                            style: AppTextStyles.error_(context, fontSize: 20))
                        : const SizedBox(),
                    if (controller.stateV[5].value != '0')
                      AddMemberInput(
                          recolor: AppColors.onBackground,
                          placeholder: '请输入手机号',
                          isPwd: controller.pPwdShow!.value,
                          iconShow: true,
                          text: controller.stateV[5].value,
                          onChangeText: (data) {
                            controller.stateV[5].value = data;
                          },
                          showPassword: () {
                            controller.pPwdShow!.value =
                                !controller.pPwdShow!.value;
                          }),
                    SizedBox(height: 20.h),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20.h),
              child: Center(
                child: CustomButton(
                  btnStr: '增加成员',
                  width: 0.95.sw,
                  height: 60.w,
                  backColor: AppColors.ffbfa46d,
                  textColor: AppColors.surface,
                  onPressed: () {
                    controller.insertMember();
                  },
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20.h),
              child: Center(
                child: CustomButton(
                  btnStr: '重置',
                  width: 0.95.sw,
                  height: 60.w,
                  backColor: AppColors.ffbfa46d,
                  textColor: AppColors.surface,
                  onPressed: () {
                    controller.reset();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AddMemberInput extends StatelessWidget {
  final String? placeholder;
  final bool? isPwd;
  final String? text;
  bool? iconShow;
  final Color? recolor;
  final Function(String value)? onChangeText;
  VoidCallback? showPassword;
  bool _obscured = false;
  AddMemberInput(
      {this.placeholder,
      this.isPwd = false,
      this.text,
      this.recolor,
      this.onChangeText,
      this.showPassword,
      this.iconShow});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4.w),
      padding: EdgeInsets.symmetric(horizontal: 4.w),
      alignment: Alignment.center,
      height: 54.h,
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.ff8D8B8B, width: 0.5),
        borderRadius: BorderRadius.circular(4),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: TextFormField(
            obscureText: isPwd ?? false,
            style: TextStyle(fontSize: 13, color: recolor),
            decoration: InputDecoration(
              hintText: placeholder,
              hintStyle: TextStyle(color: recolor),
              border: InputBorder.none,
            ),
            onChanged: onChangeText!,
            initialValue: text,
          )),
          if (iconShow ?? false)
            GestureDetector(
              onTap: showPassword,
              child: Container(
                padding: const EdgeInsets.all(8),
                child: isPwd == true
                    ? Icon(
                        Icons.visibility_off_outlined,
                        size: 23.w,
                      )
                    : Icon(
                        Icons.visibility_outlined,
                        size: 23.w,
                      ),
              ),
            ),
        ],
      ).paddingOnly(bottom: 10.0.w),
    );
  }
}
