import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jduserInfoarea.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotionInfo_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotioninfo_yqm.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabmember_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabbettingreport_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabbettingrecord_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabinvitedomain_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabdepositstate_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabdepost_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabwithdrawalreport_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabdrawlrcord_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabrealityreport_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabrealityrcord_cp.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/jdpromotiontabsportsbet_cp.dart';

class JDRecommendedIncomeView extends StatefulWidget {
  const JDRecommendedIncomeView({super.key});

  @override
  State<JDRecommendedIncomeView> createState() => _JDRecommendedIncomeView();
}

class _JDRecommendedIncomeView extends State<JDRecommendedIncomeView>
    with TickerProviderStateMixin {
  JdrecommendedIncomeViewController controller =
      Get.put(JdrecommendedIncomeViewController());
  bool isYYHTemplate = true;
  bool isGCW = false;
  late TabController _tabController;
  late TabController _tabController_1;
  late TabController _tabController_2;
  bool isFirstSeletion_1 = true;
  bool isFirstSeletion_2 = true;
  bool isTabbar_1_selected = false;
  List<String> getTabNames(int i) {
    if (isYYHTemplate) {
      if (i == 0) {
        return [
          '推荐信息',
          '会员管理',
          '投注报表',
          '投注记录',
          '域名绑定',
        ];
      } else {
        return [
          '存款报表',
          '存款记录',
          '提款报表',
          '提款记录',
          '真人报表',
          '真人记录',
          '体育报表',
        ];
      }
    } else if (isGCW) {
      return [
        '推荐信息',
        '会员管理',
        '投注报表',
        '投注记录',
        '域名绑定',
        '真人报表',
        '真人记录',
      ];
    } else {
      return [];
    }
  }

  @override
  void initState() {
    super.initState();
    controller.isFirstTabBarSelected.value = true;
    _tabController_1 =
        TabController(length: getTabNames(0).length, vsync: this);
    _tabController_2 =
        TabController(length: getTabNames(1).length, vsync: this);
    _tabController_1.addListener(_handleTabChange_1);
    // _handleTabChange_1();
    _tabController_2.addListener(_handleTabChange_2);
  }

  void _handleTabChange_1() {
    if (_tabController_1.indexIsChanging ||
        isFirstSeletion_1 ||
        !isTabbar_1_selected) {
      isFirstSeletion_1 = true;
      controller.isFirstTabBarSelected.value = true;
      bool _temp;
      if (_tabController_1.index == 0) {
        controller.showAvatar.value = true;
      } else {
        controller.showAvatar.value = false;
      }
    }
  }

  void _handleTabChange_2() {
    if (_tabController_2.indexIsChanging ||
        isFirstSeletion_2 ||
        isTabbar_1_selected) {
      controller.isFirstTabBarSelected.value = false;
      isFirstSeletion_2 = false;
      controller.showAvatar.value = false;
    }
  }

  @override
  void dispose() {
    _tabController_1.dispose();
    _tabController_2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 28.w),
        ),
        title: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Text('推荐收益',
              style: AppTextStyles.ffffffff(context, fontSize: 25)),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Obx(() => controller.showAvatar.value == true
                ? JDUserInfoArea(
                    isYYHTemplate: false,
                  )
                : const SizedBox()),
            Container(
              width: 1.sw,
              height: 800.h,
              padding: EdgeInsets.only(left: 0.w),
              child: Column(
                children: [
                  Obx(
                    () => Container(
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 1.w,
                                    color: AppColors.ff8D8B8B,
                                    style: BorderStyle.solid))),
                        child: TabBar(
                          controller: _tabController_1,
                          onTap: (index) {
                            if (index == 0) {
                              controller.showAvatar.value = true;
                            } else {
                              controller.showAvatar.value = false;
                            }
                            controller.isFirstTabBarSelected.value = true;
                          },
                          labelPadding:
                              EdgeInsets.only(left: 10.w, right: 10.w),
                          tabAlignment: TabAlignment.start,
                          padding: EdgeInsets.all(0.w),
                          tabs: getTabNames(0)
                              .map((item) => Tab(text: item))
                              .toList(),
                          isScrollable: true,
                          unselectedLabelColor: AppColors.ff000000,
                          labelColor:
                              controller.isFirstTabBarSelected.value == true
                                  ? AppColors.ffcf352e
                                  : AppColors.onBackground,
                          indicatorColor:
                              controller.isFirstTabBarSelected.value == true
                                  ? AppColors.ffcf352e
                                  : Colors.transparent,
                        )),
                  ),
                  Obx(() => Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 1.w,
                                  color: AppColors.ff8D8B8B,
                                  style: BorderStyle.solid))),
                      child: TabBar(
                        controller: _tabController_2,
                        onTap: (_) {
                          controller.showAvatar.value = false;
                          controller.isFirstTabBarSelected.value = false;
                        },
                        labelPadding: EdgeInsets.only(left: 10.w, right: 10.w),
                        tabAlignment: TabAlignment.start,
                        padding: EdgeInsets.all(0.w),
                        tabs: getTabNames(1)
                            .map((item) => Tab(text: item))
                            .toList(),
                        isScrollable: true,
                        unselectedLabelColor: AppColors.ff000000,
                        labelColor:
                            controller.isFirstTabBarSelected.value == false
                                ? AppColors.ffcf352e
                                : AppColors.onBackground,
                        indicatorColor:
                            controller.isFirstTabBarSelected.value == false
                                ? AppColors.ffcf352e
                                : Colors.transparent,
                      ))),
                  Expanded(
                    child: SizedBox(
                      width: 1.sw,
                      child: Obx(() => controller.isFirstTabBarSelected.value ==
                              true
                          ? TabBarView(controller: _tabController_1, children: [
                              ...getTabNames(0).map(
                                (item) {
                                  controller.title.value = item;
                                  return renderTab(item);
                                },
                              ).toList(),
                            ])
                          : TabBarView(controller: _tabController_2, children: [
                              ...getTabNames(1).map(
                                (item) {
                                  controller.title.value = item;
                                  return renderTab(item);
                                },
                              ).toList(),
                            ])),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget renderTab(String item) {
    switch (item) {
      case '推荐信息':
        return JDPromotionInfoCP(
          tabLabel: item,
        );
      case '邀请码':
        return JDPromotionInfoYQM(
          tabLabel: item,
          onRealPress: () {
            _tabController_1.animateTo(9);
          },
          onEarnPress: () {
            _tabController_1.animateTo(2);
          },
        );
      case '会员管理':
        return JDPromotionTabMemberCP(tabLabel: item);
      case '投注报表':
        return JDPromotionTabBettingReportCP(tabLabel: item);
      case '投注记录':
        return JDPromotionTabBettingRecordCP(tabLabel: item);
      case '域名绑定':
        return JDPromotionTabInviteDomainCP(tabLabel: item);
      case '存款报表':
        return JDPromotionTabDepositStateCP(tabLabel: item);
      case '存款记录':
        return JDPromotionTabDepostRecordCP(tabLabel: item);
      case '提款报表':
        return JDPromotionTabWithdrawalReportCP(tabLabel: item);
      case '提款记录':
        return JDPromotionTabWithDrawlRecordCP(tabLabel: item);
      case '真人报表':
        return JDPromotionTabRealityReportCP(tabLabel: item);
      case '真人记录':
        return JDPromotionTabRealityRcordCP(tabLabel: item);
      case '体育报表':
        return JdPromotionTabSportsBetCP(tabLabel: item);
      default:
        return Container();
    }
  }
}
