import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/alipay_view_controller.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'dart:async';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class AlipayView extends StatefulWidget {
  const AlipayView({super.key});
  @override
  _AlipayViewState createState() => _AlipayViewState();
}

class _AlipayViewState extends State<AlipayView> {
  final AlipayViewController controller = Get.put(AlipayViewController());

  final int _duration = 60;
  final CountDownController _counterController = CountDownController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _counterController.start();
      delayedFunction(() {
        controller.showMoneyImg.value = true;
      }, Duration(seconds: _duration - 3));
    });
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
            backgroundColor: AppColors.ffbfa46d,
            automaticallyImplyLeading: false,
            titleSpacing: 0,
            toolbarHeight: 65.h,
            leading: GestureDetector(
              onTap: () => Get.back(),
              child: Icon(Icons.home, color: AppColors.surface, size: 36.w),
            ),
            title: Text('利息宝',
                style: AppTextStyles.surface_(context, fontSize: 28)),
            centerTitle: true,
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 24).w,
                child: _counterDownWidget(),
              )
            ]),
        body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Obx(
              () => controller.isIniting.value == true
                  ? const Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomLoadingWidget(),
                      ],
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 24).h,
                          child: Column(
                            children: [
                              Text(
                                '今日收益',
                                style: AppTextStyles.bodyText2(context,
                                    fontSize: 20),
                              ),
                              Text(
                                controller.yuebao == null
                                    ? ''
                                    : '¥${double.parse(controller.yuebao?.value.todayProfit ?? '0.0').toStringAsFixed(2)}',
                                style: AppTextStyles.bodyText2(context,
                                    fontSize: 36),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    controller.yuebao == null
                                        ? ''
                                        : '${controller.yuebao?.value.yuebaoName ?? '0.0'}余额：',
                                    style: AppTextStyles.bodyText2(context,
                                        fontSize: 20),
                                  ),
                                  Text(
                                      controller.yuebao == null
                                          ? ''
                                          : controller.yuebao?.value.balance ==
                                                  null
                                              ? ''
                                              : '¥${double.parse(controller.yuebao?.value.balance ?? '0.0').toStringAsFixed(10)}',
                                      style: AppTextStyles.error_(context,
                                          fontSize: 20)),
                                  Text(', ',
                                      style: AppTextStyles.bodyText2(context,
                                          fontSize: 20)),
                                  Text('年化率：',
                                      style: AppTextStyles.bodyText2(context,
                                          fontSize: 20)),
                                  Text(
                                    controller.yuebao == null
                                        ? ''
                                        : controller.yuebao?.value
                                                    .annualizedRate ==
                                                null
                                            ? ''
                                            : '${(double.parse(controller.yuebao?.value.annualizedRate ?? '0.0') * 100).toStringAsFixed(2)}%',
                                    style: AppTextStyles.error_(context,
                                        fontSize: 20),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 12).h,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('体验金：',
                                  style: AppTextStyles.bodyText2(context,
                                      fontSize: 20)),
                              Text(
                                controller.yuebao == null
                                    ? ''
                                    : controller.yuebao?.value.giftBalance ==
                                            null
                                        ? ''
                                        : '¥${double.parse(controller.yuebao?.value.giftBalance ?? '0.0').toStringAsFixed(2)}',
                                style:
                                    AppTextStyles.error_(context, fontSize: 20),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 1.sw,
                          height: 1.sh,
                          child: Stack(
                              fit: StackFit.expand,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              children: [
                                Column(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border(
                                          top: BorderSide(
                                              width: 1,
                                              color: Colors.grey[300]!),
                                        ),
                                        color: Colors.white.withOpacity(0.3),
                                      ),
                                      child: Row(
                                        children: [
                                          _statItem(
                                              context,
                                              controller.yuebao == null
                                                  ? ''
                                                  : double.parse(controller
                                                              .yuebao
                                                              ?.value
                                                              .weekProfit ??
                                                          '0.0')
                                                      .toString(),
                                              '本周收益'),
                                          _statItem(
                                              context,
                                              controller.yuebao == null
                                                  ? ''
                                                  : double.parse(controller
                                                              .yuebao
                                                              ?.value
                                                              .monthProfit ??
                                                          '0.0')
                                                      .toString(),
                                              '本月收益'),
                                          _statItem(
                                              context,
                                              controller.yuebao == null
                                                  ? ''
                                                  : double.parse(controller
                                                              .yuebao
                                                              ?.value
                                                              .totalProfit ??
                                                          '0.0')
                                                      .toStringAsFixed(10)
                                                      .toString(),
                                              '总收益'),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border(
                                          top: BorderSide(
                                              width: 1,
                                              color: Colors.grey[300]!),
                                          bottom: BorderSide(
                                              width: 1,
                                              color: Colors.grey[300]!),
                                        ),
                                      ),
                                      child: Row(
                                        children: [
                                          _linkItem(
                                              context,
                                              newalipayTransInPath,
                                              'https://gsyahcot002uqmhd.aaasfjyt.com/images/edzrzc.png',
                                              '资金转换'),
                                          _linkItem(
                                              context,
                                              alipayprofitPath,
                                              'https://gsyahcot002uqmhd.aaasfjyt.com/images/sybb.png',
                                              '收益报表'),
                                          _linkItem(
                                              context,
                                              alipayTransferRecordPath,
                                              'https://gsyahcot002uqmhd.aaasfjyt.com/images/zrzcjl.png',
                                              '转换记录'),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.black.withOpacity(
                                              0.5), // Replace with your Skin1.isBlack ? '#fff' : '#c5bfbf' color
                                          width: 0.5,
                                        ),
                                      ),
                                      width: 300,
                                      padding: EdgeInsets.all(16).w,
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(top: 28).w,
                                      child: Text(
                                        controller.yuebao == null
                                            ? ''
                                            : controller.yuebao?.value.intro ??
                                                '利息宝上线了，欢迎大家试用！\n1、复利结算，利滚利，收益更高。\n2、结算快，每分钟结算一次，存入即开始收益。\n3、转入转出无限制，随时随地享收益。',
                                        style: AppTextStyles.ff1976D2_(context,
                                            fontSize: 20),
                                      ),
                                    ),
                                    SizedBox(height: 196.h),
                                  ],
                                ),
                                // Spacer at the bottom of the screen
                                Obx(() => Positioned(
                                      top: 0,
                                      child: (controller.showMoneyImg.value)
                                          ? Center(
                                              child: AppImage.network(
                                                img_root('images/yuebaoMoney'),
                                                width: 400,
                                                height: 400,
                                              ),
                                            )
                                          : SizedBox(
                                              width: 10.w,
                                              height: 10.h,
                                            ),
                                    )),
                              ]),
                        )
                      ],
                    ),
            )));
  }

  Expanded _linkItem(
      BuildContext context, String url, String imagePath, String name) {
    return Expanded(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          Get.toNamed(url);
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              right: BorderSide(width: 1, color: Colors.grey[300]!),
            ),
          ),
          height: 147.h,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              AppImage.network(
                imagePath,
                width: 36,
                height: 36,
              ),
              Text(
                name,
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Expanded _statItem(BuildContext context, String value, String name) {
    return Expanded(
      flex: 1,
      child: Container(
        height: 147.h,
        decoration: BoxDecoration(
          border: Border(
            right: BorderSide(width: 1, color: Colors.grey[300]!),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              controller.yuebao == null
                  ? ''
                  : double.parse(controller.yuebao?.value.monthProfit ?? '0.0')
                      .toStringAsFixed(2),
              style: AppTextStyles.bodyText2(
                context,
                fontSize: 20,
              ),
            ),
            Text(
              name,
              style: AppTextStyles.bodyText2(context, fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }

  Widget _counterDownWidget() {
    return Center(
      child: CircularCountDownTimer(
        duration: _duration,
        controller: _counterController,
        width: 36.w,
        height: 36.h,
        ringColor: AppColors.ffC5CBC6,
        ringGradient: null,
        fillColor: AppColors.ff4e57dd,
        fillGradient: null,
        backgroundColor: Colors.transparent,
        backgroundGradient: null,
        strokeWidth: 4.w,
        strokeCap: StrokeCap.round,
        textStyle: AppTextStyles.surface_(context, fontSize: 18),
        textFormat: CountdownTextFormat.S,
        isReverse: true,
        isReverseAnimation: true,
        isTimerTextShown: true,
        autoStart: false,
        onComplete: () {
          controller.showMoneyImg.value = true;
          _counterController.start();
        },
        onStart: () {
          delayedFunction(() {
            controller.showMoneyImg.value = false;
          }, Duration(seconds: 2));
        },
        // timeFormatterFunction: (defaultFormatterFunction, duration) {
        //   if (duration.inSeconds == 0) {
        //     return '0';
        //   } else {
        //     return Function.apply(defaultFormatterFunction, [duration]);
        //   }
        // },
      ),
    );
  }

  void delayedFunction(Function callback, Duration duration) {
    Timer(duration, () {
      callback();
    });
  }
}
