import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/YuebaoData.dart';
import 'package:fpg_flutter/data/repositories/api_yuebao.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

import 'package:fpg_flutter/data/models/AvatarModel.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';

class NewalipayTransinViewController extends GetxController {
  final AppYuebaoRepository _appYuebaoRepository = AppYuebaoRepository();
  final AppUserRepository _appUserRepository = AppUserRepository();
  final AppSystemRepository _appSystemRepository = AppSystemRepository();
  final TextEditingController unitController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  RxList<ListForProfitReport> profitData = RxList();
  RxString userBallance = ''.obs;
  RxBool isAuthenticated = false.obs;
  final storage = GetStorage();
  Rxn<UGUserModel> userInfo = Rxn();
  RxList<AvatarModel> avatarList = RxList();
  final RxString imgCaptcha = "".obs;

  var selectData = ['选择钱包', '余额', '利息宝'];
  RxBool isIniting = false.obs;

  List<IMiddleMenuItem?> seletecDropItem = [
    IMiddleMenuItem(title: '余额', id: '1', type: 'CNY'),
    IMiddleMenuItem(title: '余额', id: '1', type: 'CNY')
  ];

  ConverRateModel? currencyRate;
  int prevIndex = 0;
  List<RxBool> isColoured = [
    false.obs,
    false.obs,
    false.obs,
    false.obs,
    false.obs,
    false.obs,
  ];
  final quickArr = [100, 500, 1000, 5000, 10000, '全部金额'];
  Rx<YuebaoModel>? yuebao = YuebaoModel().obs;
  String moneyValue = '';
  RxString passText = ''.obs;
  String? from, to;
  @override
  void onInit() {
    super.onInit();
    // userBallance.value = GlobalService.to.userInfo.value?.balance ?? '';
    userBallance.value = GlobalService.to.userInfo.value?.balance ?? '';
    fetchData();
  }

  void fetchData() async {
    isIniting.value = true;

    await _appYuebaoRepository.stat().then((data) {
      isIniting.value = false;

      if (data == null) return;
      yuebao?.value = data;
    }).catchError((data) {});
  }

  void changeColorTransItem(int index) {
    isColoured[prevIndex].value = false;
    prevIndex = index;
    isColoured[index].value = true;
  }

  double calculateNewRate(double? convertRate, double? floatRate) {
    if (seletecDropItem[0]!.id == '3' ||
        seletecDropItem[0]!.id == '4' ||
        seletecDropItem[1]!.id == '3' ||
        seletecDropItem[1]!.id == '4') {
      const floatRateValue = 0;
      double newRate =
          (convertRate! * 10000 * (100 + floatRateValue)).roundToDouble();
      newRate /= 10000 * 100;

      if (newRate <= 0) {
        return 1;
      } else {
        return newRate;
      }
    } else {
      return 1;
    }
  }

  void setMoney(String newValue) {
    moneyValue = newValue;
  }

  void setPass(String newValue) {
    passText.value = newValue;
  }

  void getRateMoney() async {
    String from = seletecDropItem[0]!.id == '4'
        ? 'USD'
        : seletecDropItem[0]!.id == '5'
            ? 'TRX'
            : 'CNY';

    String to = seletecDropItem[1]!.id == '4'
        ? 'USD'
        : seletecDropItem[1]!.id == '5'
            ? 'TRX'
            : 'CNY';
    await _appSystemRepository
        .currencyRate(
            token: AppDefine.userToken?.apiSid,
            from: from,
            to: to,
            amount: '1',
            float: '1')
        .then((data) {
      if (data == null) return;
      currencyRate = data;
    }).catchError((data) {});
  }

  void transfer() async {
    if (seletecDropItem[0]!.id == '0') {
      AppToast.showDuration(msg: '请选择转出钱包');
    }
    if (seletecDropItem[1]!.id == '0') {
      AppToast.showDuration(msg: '请选择转入钱包');
    }
    await _appUserRepository
        .transfer(
            token: AppDefine.userToken?.apiSid,
            money: double.tryParse(moneyValue),
            fromType: seletecDropItem[0]!.id,
            toType: seletecDropItem[1]!.id,
            pwd: passText.value,
            rate:
                calculateNewRate(double.tryParse(currencyRate?.rate ?? '0'), 0))
        .then((data) async {
      if (data == null) return;
      await GlobalService.to.getUserInfo();
    }).catchError((e) {});
  }
}
