import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/alipayprofit_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class AlipayprofitView extends StatefulWidget {
  const AlipayprofitView({super.key});

  @override
  _AlipayprofitViewState createState() => _AlipayprofitViewState();
}

class _AlipayprofitViewState extends State<AlipayprofitView> {
  final AlipayprofitViewController controller =
      Get.put(AlipayprofitViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.ffbfa46d,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 65.h,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 28.w),
          ),
          title: Text('收益报表',
              style: AppTextStyles.surface_(context, fontSize: 28)),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Obx(() => Center(
                    child: Column(
                  children: [
                    Table(
                        border: TableBorder.all(
                            width: 0.5, color: AppColors.ffd1cfd0),
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.middle,
                        children: [
                          _createTableHeader(context),
                        ]),
                    Obx(() => controller.isIniting.value == true
                        ? const Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CustomLoadingWidget(),
                            ],
                          )
                        : Container(
                            color: AppColors.surface,
                            child: Table(
                                border:
                                    TableBorder.all(color: AppColors.ffd1cfd0),
                                defaultVerticalAlignment:
                                    TableCellVerticalAlignment.middle,
                                children: [
                                  ..._createTableContent(context),
                                ]),
                          )),
                    controller.profitData.isEmpty
                        ? Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 30).h,
                              child: Text('暂无数据',
                                  style: AppTextStyles.bodyText2(context,
                                      fontSize: 20)),
                            ),
                          )
                        : Text('')
                  ],
                )))));
  }

  TableCell _dataCell(BuildContext context, String content,
      {int fontSize = 20}) {
    return TableCell(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10).h,
        child: Text(content,
            textAlign: TextAlign.center,
            style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
      ),
    );
  }

  TableCell _headerCell(BuildContext context, String content,
      {int fontSize = 20}) {
    return TableCell(
      child: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20).h,
          child: Text(content,
              textAlign: TextAlign.center,
              style: AppTextStyles.surface_(context, fontSize: fontSize)),
        ),
      ),
    );
  }

  List<TableRow> _createTableContent(BuildContext context) {
    return controller.profitData.map((row) {
      return TableRow(children: [
        _dataCell(context, row.settleTime ?? ''),
        _dataCell(context, row.profitAmount ?? ''),
        _dataCell(context, row.balance ?? ''),
      ]);
    }).toList();
  }

  TableRow _createTableHeader(BuildContext context) {
    return TableRow(children: [
      _headerCell(context, '结算时间', fontSize: 24),
      _headerCell(context, '收益', fontSize: 24),
      _headerCell(context, '余额', fontSize: 24),
    ]);
  }
}
