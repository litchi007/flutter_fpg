import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/userModel/userTransData.dart';

class AlipayTransferRecordViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();
  RxList<UserTransList> transferLogsData = RxList();
  RxBool isIniting = false.obs;
  RxList<String> liqTypeList = [
    '余额',
    '利息宝',
  ].obs;
  Map<String, int> liqTypeNo = {
    '余额': 1,
    '利息宝': 2,
  };
  RxString rightDropdownValue = '余额'.obs;
  @override
  void onInit() {
    super.onInit();

    fetchData();
  }

  void fetchData() async {
    isIniting.value = true;
    await _appUserRepository
        .transferLogs(type: liqTypeNo[rightDropdownValue.value])
        .then((data) {
      isIniting.value = false;

      if (data == null) return;

      transferLogsData.value = data.list;
    }).catchError((data) {});
  }
}
