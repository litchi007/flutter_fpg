import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/alipay_transfer_record_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class AlipayTransferRecordView extends StatefulWidget {
  const AlipayTransferRecordView({super.key});

  @override
  _AlipayTransferRecordViewState createState() =>
      _AlipayTransferRecordViewState();
}

class _AlipayTransferRecordViewState extends State<AlipayTransferRecordView> {
  final AlipayTransferRecordViewController controller =
      Get.put(AlipayTransferRecordViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.ffbfa46d,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 65.h,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 28.w),
          ),
          title: Text('转换记录',
              style: AppTextStyles.surface_(context, fontSize: 28)),
          centerTitle: true,
          actions: [
            Obx(
              () => Container(
                  width: 150.w,
                  height: 50.h,
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 20.w),
                  decoration: BoxDecoration(
                      border: Border.all(width: 1.w, color: AppColors.surface),
                      borderRadius: BorderRadius.all(Radius.circular(4.w))),
                  child: DropdownButton(
                    dropdownColor: AppColors.surface,
                    underline: Container(
                      height: 1.h,
                      color: Colors.transparent,
                    ),
                    value: controller.rightDropdownValue.value,
                    icon: const Icon(Icons.arrow_drop_down),
                    items: controller.liqTypeList.map((var curItem) {
                      return DropdownMenuItem(
                        value: curItem,
                        child: Center(
                          child: Text(curItem,
                              textAlign: TextAlign.center,
                              style: AppTextStyles.bodyText3(context,
                                  fontSize: 20)),
                        ),
                      );
                    }).toList(),
                    selectedItemBuilder: (BuildContext context) {
                      return controller.liqTypeList.map<Widget>((String value) {
                        return Padding(
                          padding: const EdgeInsets.only(
                            top: 8.0,
                          ).w,
                          child: Text(value,
                              style:
                                  value == controller.rightDropdownValue.value
                                      ? AppTextStyles.surface_(context,
                                          fontSize: 20)
                                      : AppTextStyles.bodyText2(context,
                                          fontSize: 20)),
                        );
                      }).toList();
                    },
                    onChanged: (newValue) {
                      controller.rightDropdownValue.value = newValue.toString();
                      controller.fetchData();
                    },
                  )),
            ),
          ],
        ),
        body: SingleChildScrollView(
            child: Obx(() => Center(
                    child: Column(
                  children: [
                    Table(
                        border: TableBorder.all(color: AppColors.ffd1cfd0),
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.middle,
                        children: [
                          _createTableHeader(context),
                        ]),
                    Obx(() => controller.isIniting.value == true
                        ? const Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CustomLoadingWidget(),
                            ],
                          )
                        : Container(
                            color: AppColors.surface,
                            child: Table(
                                border:
                                    TableBorder.all(color: AppColors.ffd1cfd0),
                                defaultVerticalAlignment:
                                    TableCellVerticalAlignment.middle,
                                children: [
                                  ..._createTableContent(context),
                                ]),
                          )),
                    controller.transferLogsData.isEmpty
                        ? Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 30).h,
                              child: Text('暂无数据',
                                  style: AppTextStyles.bodyText2(context)),
                            ),
                          )
                        : const Text('')
                  ],
                )))));
  }

  TableCell _dataCell(BuildContext context, String content,
      {int fontSize = 20}) {
    return TableCell(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10).h,
        child: Text(content,
            textAlign: TextAlign.center,
            style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
      ),
    );
  }

  TableCell _headerCell(BuildContext context, String content,
      {int fontSize = 20}) {
    return TableCell(
      child: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10).h,
          child: Text(content,
              textAlign: TextAlign.center,
              style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
        ),
      ),
    );
  }

  List<TableRow> _createTableContent(BuildContext context) {
    return controller.transferLogsData.map((row) {
      return TableRow(children: [
        _dataCell(context, row.addTime),
        _dataCell(context, row.typeName),
        _dataCell(context, row.coin),
        _dataCell(context, row.settleCoin),
      ]);
    }).toList();
  }

  TableRow _createTableHeader(BuildContext context) {
    return TableRow(children: [
      _headerCell(context, '时间', fontSize: 20),
      _headerCell(context, '类型', fontSize: 20),
      _headerCell(context, '账变金额   (CNY)', fontSize: 20),
      _headerCell(context, '余额 (CNY)', fontSize: 20),
    ]);
  }
}
