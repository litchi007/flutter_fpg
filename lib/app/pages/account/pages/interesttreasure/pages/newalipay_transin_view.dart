import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/newalipay_transin_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/widgets/InputTextForTransWidget.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/YuebaoData.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/const/AlipayConst.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class NewalipayTransinView extends StatefulWidget {
  const NewalipayTransinView();

  @override
  _NewalipayTransinViewState createState() => _NewalipayTransinViewState();
}

class _NewalipayTransinViewState extends State<NewalipayTransinView> {
  final NewalipayTransinViewController controller =
      Get.put(NewalipayTransinViewController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 36.w),
          ),
          title: Text('资金转换',
              style: AppTextStyles.surface_(context, fontSize: 28)),
          centerTitle: true,
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20).w,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.ffe5c06f,
                  foregroundColor: AppColors.surface,
                  side: BorderSide(width: 1.w, color: AppColors.surface),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.w),
                  ),
                  padding: EdgeInsets.all(0.w),
                  fixedSize: Size(105.w, 35.h),
                  minimumSize: Size(105.w, 35.h),
                ),
                onPressed: () {
                  Get.toNamed(alipayTransferRecordPath);
                },
                child: Text(
                  '转换记录',
                  style: AppTextStyles.surface_(context, fontSize: 22),
                ),
              ),
            )
          ],
        ),
        body: Center(
            child: Column(children: [
          Container(
            height: 100.h,
            decoration: const BoxDecoration(color: AppColors.ffbfa46d),
            child: Obx(() => controller.isIniting.value == true
                ? const Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CustomLoadingWidget(),
                    ],
                  )
                : Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Obx(() => _headerItem(
                          context, '余额', controller.userBallance.value)),
                      Obx(
                        () => _headerItem(
                            context,
                            controller.yuebao?.value.yuebaoName ?? '',
                            double.parse(
                                    controller.yuebao?.value.balance ?? '0')
                                .toStringAsFixed(10)),
                      )
                    ],
                  )),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16).h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CategoryItemWidget(
                  controller: controller,
                  index: 0,
                ),
                CategoryItemWidget(
                  controller: controller,
                  index: 1,
                )
              ],
            ),
          ),
          SizedBox(
            height: 40.h,
          ),
          SizedBox(
            height: 72.h,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36).w,
              child: InputTextforTransWidget(
                parentController: controller,
                leftIconName: "",
                rightIconName: "icon-close-w.png",
                widgetType: 'money',
                hintext: '请输入转出的数量',
                editController: controller.unitController,
              ),
            ),
          ),
          SizedBox(
            height: 72.h,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36).w,
              child: InputTextforTransWidget(
                parentController: controller,
                leftIconName: "",
                rightIconName: "icon-close-w.png",
                widgetType: 'password',
                hintext: '请输入提款密码',
                editController: controller.passwordController,
              ),
            ),
          ),
          Obx(() => (controller.passText.value.isNotEmpty &&
                      controller.passText.value.length < 4) ==
                  true
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(32.0, 10, 0, 0).w,
                      child: Text(
                        '请输入 4 位取款密码',
                        style: AppTextStyles.error_(context, fontSize: 20),
                      ),
                    ),
                  ],
                )
              : const SizedBox()),
          Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(
                3,
                (index) => _unitTransferItem(
                    context,
                    index,
                    controller.quickArr[index].toString(),
                    controller.isColoured[index].value),
              ))),
          Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(
                3,
                (index) => _unitTransferItem(
                    context,
                    index + 3,
                    controller.quickArr[index + 3].toString(),
                    controller.isColoured[index + 3].value),
              ))),
          _confirmButton(context)
        ])));
  }

  Expanded _headerItem(BuildContext context, String title, String amount) {
    return Expanded(
        flex: 1,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0).h,
          child: Column(
            children: [
              Text(
                title,
                style: AppTextStyles.surface_(context, fontSize: 20),
              ),
              Text(
                amount,
                style: AppTextStyles.surface_(context, fontSize: 20),
              ),
            ],
          ),
        ));
  }

  SizedBox _unitTransferItem(
      BuildContext context, int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.changeColorTransItem(index);
            index < 5
                ? controller.unitController.text =
                    controller.quickArr[index].toString()
                : controller.unitController.text =
                    controller.userBallance.value;
            controller.setMoney(controller.unitController.text);
          },
          child: Container(
            width: 0.28.sw, // 29% width of the screen width
            height: 72.h,
            margin: const EdgeInsets.only(top: 16)
                .h, // 3% margin left and 10px margin bottom
            decoration: BoxDecoration(
              color: isColoured
                  ? AppColors.surface
                  : AppColors.ffeaeaea, // #eeeeee background color
              borderRadius: BorderRadius.circular(3).w,
              border: Border.all(
                color: isColoured
                    ? AppColors.ffBFA46D
                    : AppColors.ffeaeaea, // Set border color
                width: 1.0.w, // Set border width
              ), // 3px border-radius
            ),
            child: Center(
              child: Text(
                '$amount元',
                style: isColoured
                    ? AppTextStyles.ffBFA46D_(context, fontSize: 20)
                    : AppTextStyles.bodyText2(context, fontSize: 20),
                textAlign: TextAlign.center, // center align text
              ),
            ),
          )),
    );
  }

  SizedBox _confirmButton(BuildContext context) {
    return SizedBox(
      child: GestureDetector(
        onTap: () async {
          controller.transfer();
        },
        child: Container(
          width: 1.sw,
          height: 49.h,
          margin: const EdgeInsets.only(top: 16).h,
          decoration: const BoxDecoration(
              color: AppColors.ffbfa46d,
              borderRadius: BorderRadius.all(Radius.circular(3))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "确认转入",
                style: AppTextStyles.surface_(context, fontSize: 17),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CategoryItemWidget extends StatefulWidget {
  const CategoryItemWidget({
    required this.controller,
    required this.index,
  });

  final NewalipayTransinViewController controller;
  final int index;

  @override
  State<CategoryItemWidget> createState() => _CategoryItemWidgetState();
}

class _CategoryItemWidgetState extends State<CategoryItemWidget> {
  IMiddleMenuItem? selectedItem;
  final NewalipayTransinViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 250.w,
      height: 52.5.h,
      child: Row(
        children: [
          SizedBox(
            width: 90.w,
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.w,
                  color: AppColors.ffC5CBC6,
                  style: BorderStyle.solid,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    widget.index == 0 ? '转出' : '转入',
                    style: AppTextStyles.bodyText2(context, fontSize: 16),
                  ),
                  Text(
                    '钱包',
                    style: AppTextStyles.bodyText2(context, fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 160.w,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border.all(
                color: AppColors.ffC5CBC6,
                style: BorderStyle.solid,
              ),
            ),
            child: PopupMenuButton<IMiddleMenuItem>(
              color: AppColors.surface,
              onSelected: (IMiddleMenuItem value) {
                setState(() {
                  selectedItem = value;
                  if (widget.index == 0) {
                    controller.seletecDropItem[0] = value;
                  } else {
                    controller.seletecDropItem[1] = value;
                  }
                });
              },
              child: Row(
                // mainAxisSize: MainAxisSize.max,
                children: [
                  const Spacer(
                    flex: 3,
                  ),
                  Text(
                    selectedItem?.title ?? '选择钱包',
                    style: AppTextStyles.bodyText2(context, fontSize: 16),
                  ),
                  const Spacer(
                    flex: 3,
                  ),
                  const Icon(Icons.arrow_drop_down),
                  const Spacer(
                    flex: 1,
                  ),
                ],
              ),
              itemBuilder: (BuildContext context) {
                return yuebaoItemTypes.map((IMiddleMenuItem value) {
                  return PopupMenuItem<IMiddleMenuItem>(
                    value: value,
                    child: Center(
                      child: Text(
                        value.title,
                        style: AppTextStyles.bodyText2(context, fontSize: 16),
                      ),
                    ),
                  );
                }).toList();
              },
            ),
          ),
        ],
      ),
    );
  }
}
