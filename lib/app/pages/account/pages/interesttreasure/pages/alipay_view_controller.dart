import 'package:fpg_flutter/data/models/YuebaoData.dart';
import 'package:fpg_flutter/data/repositories/api_yuebao.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

import 'package:fpg_flutter/routes.dart';

import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class AlipayViewController extends GetxController {
  final AppYuebaoRepository _appYuebaoRepository = AppYuebaoRepository();
  Rx<YuebaoModel>? yuebao = YuebaoModel().obs;
  RxBool showMoneyImg = false.obs;
  RxBool isIniting = false.obs;

  @override
  void onInit() {
    super.onInit();
    if (GlobalService.to.userInfo.value?.hasFundPwd == null ||
        GlobalService.to.userInfo.value?.hasFundPwd == false) {
      if (GlobalService.isTest) {
        return;
      }

      AppToast.showDuration(msg: "请先设置取款密码");
      Future.delayed(Duration(milliseconds: 1000), () {
        Get.toNamed(safeCenterPath, arguments: [1]);
      });
    } else {
      fetchData();
    }
  }

  void fetchData() async {
    isIniting.value = true;

    await _appYuebaoRepository.stat().then((data) {
      isIniting.value = false;

      if (data == null) return;
      yuebao?.value = data;
    }).catchError((data) {});
  }

  void setShowMoney(bool show) async {
    showMoneyImg.value = show;
  }
}
