import 'package:fpg_flutter/data/models/YuebaoData.dart';
import 'package:fpg_flutter/data/repositories/api_yuebao.dart';
import 'package:get/get.dart';

class AlipayprofitViewController extends GetxController {
  final AppYuebaoRepository _appYuebaoRepository = AppYuebaoRepository();
  RxList<ListForProfitReport> profitData = RxList();
  RxBool isIniting = false.obs;
  @override
  void onInit() {
    super.onInit();

    fetchData();
  }

  void fetchData() async {
    isIniting.value = true;

    await _appYuebaoRepository.profitReport().then((data) {
      isIniting.value = false;

      if (data == null) return;
      profitData.value = data.list ?? [];
    }).catchError((data) {});
  }
}
