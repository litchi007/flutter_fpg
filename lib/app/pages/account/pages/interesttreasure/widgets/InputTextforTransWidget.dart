import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/newalipay_transin_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:flutter/services.dart';

class InputTextforTransWidget extends StatefulWidget {
  InputTextforTransWidget({
    super.key,
    required this.parentController,
    required this.leftIconName,
    required this.rightIconName,
    required this.widgetType,
    required this.editController,
    required this.hintext,
    // this.fontSize
  });
  final NewalipayTransinViewController parentController;
  final String leftIconName;
  final String rightIconName;
  final String widgetType;
  late TextEditingController editController;
  final String hintext;
  // final int? fontSize;

  @override
  _InputTextforTransWidgetState createState() =>
      _InputTextforTransWidgetState();
}

class _InputTextforTransWidgetState extends State<InputTextforTransWidget> {
  final textFieldFocusNode = FocusNode();
  bool _hasFocus_1 = false;
  bool _hasFocus_2 = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 48.h,
        margin: const EdgeInsets.only(top: 10).h,
        padding: const EdgeInsets.symmetric(vertical: 5).h,
        decoration: BoxDecoration(
            color: AppColors.surface,
            border: Border.all(
                width: 1.w,
                color: AppColors.ffC5CBC6,
                style: BorderStyle.solid),
            borderRadius: const BorderRadius.all(Radius.zero)),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          if (widget.leftIconName != '')
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10).w,
              child: AppImage.asset(widget.leftIconName,
                  width: 23.w, height: 23.h, fit: BoxFit.contain),
            ),
          (widget.widgetType == 'money')
              ? (Expanded(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      child: Focus(
                        onFocusChange: (hasFocus) {
                          setState(() {
                            _hasFocus_1 = hasFocus;
                          });
                        },
                        child: TextFormField(
                          onChanged: widget.parentController.setMoney,
                          controller: widget.editController,
                          focusNode: textFieldFocusNode,
                          cursorColor: AppColors.ff8D8B8B,
                          style: AppTextStyles.bodyText2(context, fontSize: 20),
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^-?\d+(\.\d+)?$')),
                          ],
                          decoration: InputDecoration(
                              isDense: true, // Reduces height a bit
                              contentPadding:
                                  const EdgeInsets.symmetric(horizontal: 20).w,
                              iconColor: AppColors.surface,
                              filled: true,
                              fillColor: Colors.transparent,
                              hintStyle:
                                  //  AppTextStyles.ffC5CBC6_(context,fontSize: widget.fontSize ?? 14),
                                  AppTextStyles.ffC5CBC6_(context,
                                      fontSize: 18),
                              hintText: widget.hintext,
                              border: const OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                              suffixIcon: (GestureDetector(
                                  onTap: () {
                                    widget.editController.clear();
                                  },
                                  child: _hasFocus_1
                                      ? Icon(
                                          size: 23.w,
                                          color: AppColors.ff8D8B8B,
                                          Icons.close)
                                      : const SizedBox()))),
                        ),
                      ))))
              : (Expanded(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      child: Focus(
                        onFocusChange: (hasFocus) {
                          setState(() {
                            _hasFocus_2 = hasFocus;
                          });
                        },
                        child: TextFormField(
                            onChanged: widget.parentController.setPass,
                            controller: widget.editController,
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: true,
                            focusNode: textFieldFocusNode,
                            cursorColor: AppColors.ff8D8B8B,
                            style:
                                AppTextStyles.bodyText2(context, fontSize: 20),
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(4),
                            ],
                            decoration: InputDecoration(
                                isDense: true,
                                contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 20)
                                        .w,
                                iconColor: AppColors.surface,
                                filled: true,
                                fillColor: Colors.transparent,
                                hintStyle: AppTextStyles.ffC5CBC6_(context,
                                    fontSize: 18),
                                hintText: widget.hintext,
                                border: const OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                ),
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    widget.editController.clear();
                                  },
                                  child: _hasFocus_2
                                      ? Icon(
                                          size: 23.w,
                                          color: AppColors.ff8D8B8B,
                                          Icons.close)
                                      : const SizedBox(),
                                ))),
                      )),
                ))
        ]));
  }
}
