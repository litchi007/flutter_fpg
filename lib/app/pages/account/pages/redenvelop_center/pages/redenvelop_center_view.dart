import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/redenvelop_center/const/redenvelop_const.dart';
import 'package:fpg_flutter/app/pages/account/pages/redenvelop_center/pages/redenvelop_center_view_controller.dart';
import 'package:fpg_flutter/data/models/redEnvelop/RedEnvelopModel.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class RedenvelopCenterView extends StatefulWidget {
  const RedenvelopCenterView({super.key});

  @override
  State<RedenvelopCenterView> createState() => _RedenvelopCenterViewState();
}

class _RedenvelopCenterViewState extends State<RedenvelopCenterView>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  late TabController _tabController;

  List<dynamic> payAccounts = [];
  NewDeal? curDeal;
  bool isShowBottomPanelDeposit = false;
  bool isShowBottomPanelWithDraw = false;

  RedenvelopCenterViewController controller =
      Get.put(RedenvelopCenterViewController());

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: categoryTabs.length, vsync: this);
    _tabController.addListener(_handleTabChange);
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 50),
    );

    _animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_controller);

    _animation.addListener(() {
      setState(() {});
    });
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {}
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.topCenter, children: [
      Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15).w,
              child: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.surface, size: 28.w),
                ]),
              )),
          title: Text('红包中心',
              style: AppTextStyles.surface_(context,
                  fontWeight: FontWeight.bold, fontSize: 24)),
          centerTitle: true,
          leadingWidth: 0.3.sw,
          actions: [
            Text(
              '红包纪录',
              style: AppTextStyles.surface_(
                context,
                fontSize: 22,
              ),
            )
                .onTap(() => Get.toNamed(redEnvelopRecordPath))
                .paddingOnly(right: 10.w),
          ],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(46.h),
            child: Container(
              color: AppColors.surface,
              child: Column(
                children: [
                  _createTab(context),
                ],
              ),
            ),
          ),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Obx(() => (controller.isIniting.value)
              ? CustomLoadingWidget()
              : Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      _firstTabContent(),
                      _secondTabContent(),
                    ],
                  ),
                )),
        ]),
      )
    ]);
  }

  Widget _renderItem(RedEnvelopModel item) {
    return Container(
      width: 0.9.sw,
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.ffaeaeae, width: 2.w),
      ),
      child: Column(
        // mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            child: Row(
              children: [
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '标题:  ${item.title}',
                      style: AppTextStyles.headline2(context, fontSize: 24),
                    ).paddingOnly(left: 12.w, top: 12.h, bottom: 12.h),
                    Text(
                      '限量周期: 1个',
                      style: AppTextStyles.ff666666_(context, fontSize: 20),
                    ).paddingOnly(left: 12.w, bottom: 6.h),
                    Text(
                      '红包说明:  ${item.info}',
                      style: AppTextStyles.ff666666_(context, fontSize: 20),
                    ).paddingOnly(left: 12.w, bottom: 6.h),
                  ],
                )),
                // const Spacer(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    AppImage.asset('envelope_bg.png',
                        width: 60.w, height: 60.w, fit: BoxFit.contain)
                  ],
                ),
              ],
            ),
          ),
          _button(item.id)
        ],
      ),
    ).paddingOnly(left: 12.w, top: 12.w, right: 12.w);
  }

  Widget _button(String? id) {
    return Container(
      width: 0.9.sw,
      height: 50.w,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: _tabController.index == 0
              ? AppColors.ffcf352e
              : AppColors.ffcc5c54,
          borderRadius: BorderRadius.all(Radius.circular(4.w))),
      child: GestureDetector(
        onTap: () {
          if (_tabController.index == 0) {
            controller.handleClickRedbag(id);
          }
        },
        child: Text(
          '领取红包',
          style: AppTextStyles.surface_(
            context,
            fontSize: 20,
          ),
        ),
      ),
    ).paddingOnly(bottom: 12.h);
  }

  Widget _firstTabContent() {
    return Obx(() => ListView(
        children: controller.redEnvelopData_prog
            .map(
              (item) => _renderItem(item),
            )
            .toList()));
  }

  Widget _secondTabContent() {
    return Obx(() => ListView(
        children: controller.redEnvelopData_ended
            .map(
              (item) => _renderItem(item),
            )
            .toList()));
  }

  TabBar _createTab(BuildContext context) {
    return TabBar(
      controller: _tabController,
      indicatorColor: AppColors.error,
      labelColor: AppColors.onBackground,
      unselectedLabelColor: AppColors.onBackground,
      indicatorSize: TabBarIndicatorSize.tab,
      physics: const ClampingScrollPhysics(),
      tabAlignment: TabAlignment.fill,
      isScrollable: false,
      labelStyle: AppTextStyles.surface_(context,
          fontWeight: FontWeight.bold, fontSize: 22),
      tabs: List.generate(categoryTabs.length, (index) {
        return Tab(
          text: '${categoryTabs[index]['name']}',
        );
      }),
    );
  }
}
