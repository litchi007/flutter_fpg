import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/redenvelop_center/pages/redenvelop_record_view_controller.dart';
import 'package:intl/intl.dart';
import 'package:fpg_flutter/data/models/redEnvelop/RedPackedLogModel.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class RedenvelopRecordView extends StatefulWidget {
  const RedenvelopRecordView({super.key});

  @override
  _RedenvelopRecordViewtate createState() => _RedenvelopRecordViewtate();
}

class _RedenvelopRecordViewtate extends State<RedenvelopRecordView> {
  final RedenvelopRecordViewController controller =
      Get.put(RedenvelopRecordViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 65.h,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 28.w),
          ),
          title: Text('红包纪录',
              style: AppTextStyles.surface_(context, fontSize: 24)),
          centerTitle: true,
        ),
        body: Obx(() => (controller.isIniting.value)
            ? const Center(child: CustomLoadingWidget())
            : SingleChildScrollView(
                child: Center(
                    child: Column(
                children: [
                  Obx(() => Table(
                          border: TableBorder.all(color: AppColors.ffd1cfd0),
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          columnWidths: const {
                            0: FlexColumnWidth(3),
                            1: FlexColumnWidth(4),
                            2: FlexColumnWidth(2),
                          },
                          children: [
                            _createTableHeader(context),
                            ..._createTableContent(
                                context, controller.tableData),
                          ])),
                ],
              )))));
  }

  TableCell _dataCell(BuildContext context, String content,
      {int fontSize = 17}) {
    return TableCell(
      verticalAlignment: TableCellVerticalAlignment.middle,
      child: Container(
        height: 60.h,
        alignment: Alignment.center,
        child: Text(content,
            textAlign: TextAlign.center,
            style: AppTextStyles.bodyText3(context, fontSize: fontSize)),
      ),
    );
  }

  List<TableRow> _createTableContent(
      BuildContext context, List<RedpackedLogmodel> tableData) {
    return tableData.map((item) {
      return TableRow(children: [
        _dataCell(
            context,
            DateFormat('yyyy-MM-dd HH:mm:ss').format(
                DateTime.fromMillisecondsSinceEpoch(
                    int.parse(item.createTime ?? '1723394031') * 1000))),
        _dataCell(context, item.title ?? ''),
        _dataCell(context, item.amount ?? '')
      ]);
    }).toList();
  }

  TableRow _createTableHeader(BuildContext context) {
    return TableRow(
        decoration: BoxDecoration(color: AppColors.fff0f0f0),
        children: [
          _dataCell(context, '领取時間', fontSize: 18),
          _dataCell(context, '红包活动名称', fontSize: 18),
          _dataCell(context, '领取金额', fontSize: 18),
        ]);
  }
}
