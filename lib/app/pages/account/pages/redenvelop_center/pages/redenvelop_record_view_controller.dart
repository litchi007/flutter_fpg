import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_redbag_center_repository.dart';
import 'package:fpg_flutter/data/models/redEnvelop/RedPackedLogModel.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class RedenvelopRecordViewController extends GetxController {
  RxList<RedpackedLogmodel> tableData = RxList();

  final ApiRedbagCenterRepository _appRedbagCenterRepository =
      ApiRedbagCenterRepository();
  RxBool isIniting = false.obs;

  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  void fetchData() async {
    isIniting.value = true;

    await _appRedbagCenterRepository.getRedPacketLogList().then((data) {
      isIniting.value = false;

      tableData.value = data ?? [];
    });
  }
}
