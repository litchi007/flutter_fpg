import 'package:fpg_flutter/data/models/redEnvelop/RedEnvelopModel.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:fpg_flutter/data/repositories/app_redbag_center_repository.dart';

class RedenvelopCenterViewController extends GetxController {
  final ApiRedbagCenterRepository _appRedbagCenterRepository =
      ApiRedbagCenterRepository();
  RxList<RedEnvelopModel> redEnvelopData_prog = RxList();
  RxList<RedEnvelopModel> redEnvelopData_ended = RxList();
  RxBool isIniting = false.obs;

  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  void handleClickRedbag(String? id) async {
    isIniting.value = true;

    await _appRedbagCenterRepository.getRedPacket(rpid: id).then((data) {
      isIniting.value = false;
    });
  }

  void fetchData() async {
    isIniting.value = true;

    await _appRedbagCenterRepository
        .getRedPacketList(action: 'do')
        .then((data) {
      isIniting.value = false;

      if (data == null) return;
      redEnvelopData_prog.value = data.list ?? [];
    });
    await _appRedbagCenterRepository
        .getRedPacketList(action: 'end')
        .then((data) {
      isIniting.value = false;

      if (data == null) return;
      redEnvelopData_ended.value = data.list ?? [];
    });
  }
}
