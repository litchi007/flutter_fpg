import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterynotcount/jdbetdetail_withoutclose_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class JdbetdetailWithoutcloseView extends StatefulWidget {
  const JdbetdetailWithoutcloseView({super.key});

  @override
  _JdbetdetailWithoutcloseViewState createState() =>
      _JdbetdetailWithoutcloseViewState();
}

class _JdbetdetailWithoutcloseViewState
    extends State<JdbetdetailWithoutcloseView> {
  final JdbetdetailWithoutcloseViewController controller =
      Get.put(JdbetdetailWithoutcloseViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 65.h,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 18.w),
          ),
          title: Text('即时注单',
              style: AppTextStyles.surface_(context, fontSize: 24)),
          centerTitle: true,
        ),
        body: Obx(
          () => !controller.isIniting.value
              ? const CustomLoadingWidget()
              : SingleChildScrollView(
                  child:
                      SizedBox(width: 1.sw, child: _createDataTable(context))),
        ));
  }

  DataTable _createDataTable(BuildContext context) {
    return DataTable(
        headingRowHeight: 60.h,
        columnSpacing: 0,
        horizontalMargin: 0,
        border: TableBorder.all(color: AppColors.ffd1cfd0),
        showCheckboxColumn: false,
        columns: _createColumns(context),
        rows: controller.newStateItems == null
            ? [
                const DataRow(cells: [
                  DataCell(Text('')),
                  DataCell(Text('')),
                  DataCell(Text('')),
                ])
              ]
            : _createRows(context));
  }

  List<DataColumn> _createColumns(BuildContext context) {
    return [
      DataColumn(
          label: SizedBox(
              width: 0.4.sw,
              child: Center(
                  child: Text('彩种',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.bodyText3(context, fontSize: 18))))),
      DataColumn(
          label: SizedBox(
              width: 0.3.sw,
              child: Center(
                  child: Text('注单笔数',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.bodyText3(context, fontSize: 18))))),
      DataColumn(
          label: SizedBox(
              width: 0.3.sw,
              child: Center(
                  child: Text('下注金额',
                      style: AppTextStyles.bodyText3(context, fontSize: 16))))),
    ];
  }

  List<DataRow> _createRows(BuildContext context) {
    return List<DataRow>.generate(controller.newStateItems!.length,
        (index) => DataRow(cells: _createCells(index, context)));
  }

  List<DataCell> _createCells(int index, BuildContext context) {
    return [
      DataCell(
        Center(
          child: Text(
            controller.newStateItems![index].gameName,
            textAlign: TextAlign.center,
            style: AppTextStyles.bodyText3(context, fontSize: 18),
          ),
        ),
      ),
      DataCell(
        Center(
          child: Text(
            controller.newStateItems![index].betCount.toString(),
            style: AppTextStyles.bodyText3(context, fontSize: 17),
          ),
        ),
      ),
      DataCell(
        Center(
          child: Text(
            controller.newStateItems![index].betAmount.toString(),
            style: AppTextStyles.bodyText3(context, fontSize: 17),
          ),
        ),
      ),
    ];
  }
}
