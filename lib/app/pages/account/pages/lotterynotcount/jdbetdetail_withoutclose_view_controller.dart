import 'package:fpg_flutter/data/models/HallGameModel.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/api_ticket.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';

class JdbetdetailWithoutcloseViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxString? dropdownValue;
  final ApiTicket _apiticket = ApiTicket();
  final AppGameRepository _appGameRepository = AppGameRepository();

  RxList<UGBetsRecordLongModel>? lotteryData = RxList();
  RxList<GroupGameData> lotteryGroupGameList = RxList();
  List<Lottery>? fixedArrayData;
  RxList<Lottery>? newStateItems = RxList();
  @override
  void onInit() {
    super.onInit();
    initController();
  }

  void initController() async {
    isIniting.value = false;
    await _apiticket.appHttpClient
        .getAllHistory(
      token: AppDefine.userToken?.apiSid,
    )
        .then((data) {
      fixedArrayData = listLotteryFromData(data.data?.data?.list);

      _appGameRepository.lotteryGroupGames().then((data2) {
        lotteryGroupGameList.value = data2 ?? [];
        List<Lottery> groupLotteries = createGroupLotteries(data2);
        Map<String, Lottery> groupLotteriesMap =
            createGroupLotteriesMap(groupLotteries);
        List<Lottery> uniqGroupLotteries =
            createUniqGroupLotteries(groupLotteriesMap);
        updateStateItems(uniqGroupLotteries, fixedArrayData!, groupLotteries);
      });
    });

    isIniting.value = true;
  }

  List<Lottery> listLotteryFromData(List<UGBetsRecordLongModel>? data) {
    if (data == null) {
      return [];
    }

    Map<String, Lottery> obj = {};

    data.forEach((item) {
      String id = item.gameId ?? '-1';
      String gameName = (item.gameName ?? 'noName').toString();
      int betCount = convertToInt(item.betCount) ?? 0;
      double betAmount = convertToDouble(item.betAmount) ?? 0;

      if (obj.containsKey(gameName)) {
        obj[gameName]!.updateStats(Lottery(
          id: id,
          gameName: gameName,
          betCount: betCount,
          betAmount: betAmount,
        ));
      } else {
        obj[gameName] = Lottery(
            id: id,
            gameName: gameName,
            betCount: betCount,
            betAmount: betAmount);
      }
    });

    return obj.values.toList();
  }

  List<Lottery> createGroupLotteries(List<GroupGameData>? groupGameList) {
    List<Lottery> list = [];

    if (groupGameList != null) {
      for (GroupGameData group in groupGameList) {
        List<HallGameModel2> lotteries = group.lotteries ?? [];

        for (HallGameModel2 v in lotteries) {
          if (v.enable == '1') {
            list.add(Lottery.fromMap({
              ...v.toJson(),
              'gameName': v.title,
              'betCount': 0,
              'betAmount': 0,
            }));
          }
        }
      }
    }
    return list;
  }

  List<Lottery> createUniqGroupLotteries(
      Map<String, Lottery>? groupLotteriesMap) {
    List<Lottery> uniqGroupLotteries = [];
    if (groupLotteriesMap != null) {
      uniqGroupLotteries.addAll(groupLotteriesMap.values);
    }

    return uniqGroupLotteries;
  }

  Map<String, Lottery> createGroupLotteriesMap(List<Lottery>? groupLotteries) {
    Map<String, Lottery> groupLotteriesMap = {};
    if (groupLotteries != null) {
      groupLotteriesMap = Map.fromEntries(groupLotteries.map(
          (lottery) => MapEntry(lottery.id, Lottery.fromMap(lottery.toMap()))));
    }
    return groupLotteriesMap;
  }

  List<Lottery> updateStateItems(
    List<Lottery> uniqGroupLotteries,
    List<Lottery> fixedArrayData,
    List<Lottery> groupLotteries,
  ) {
    if (uniqGroupLotteries.isNotEmpty && fixedArrayData.isNotEmpty) {
      newStateItems?.value = uniqGroupLotteries.map((lottery) {
        var target = fixedArrayData.firstWhere(
          (v) => v.id == lottery.id,
          orElse: () => lottery,
        );

        return Lottery(
          id: lottery.id,
          gameName: lottery.gameName,
          betCount: target.betCount,
          betAmount: target.betAmount,
        );
      }).toList();
    } else {
      newStateItems?.value = fixedArrayData;
    }

    newStateItems?.sort((a, b) => b.betAmount.round() - a.betAmount.round());
    return newStateItems ?? [];
  }
}

class Lottery {
  final String id;
  final String gameName;
  int betCount;
  double betAmount;

  Lottery({
    required this.id,
    required this.gameName,
    this.betCount = 0,
    this.betAmount = 0.0,
  });

  factory Lottery.fromMap(Map<String, dynamic> map) {
    return Lottery(
      id: map['id'],
      gameName: map['gameName'],
      betCount: map['betCount'] ?? 0,
      betAmount: convertToDouble(map['betAmount'].toString()) ?? 0,
    );
  }
  void updateStats(Lottery other) {
    betCount += other.betCount;
    betAmount += other.betAmount;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'gameName': gameName,
      'betCount': betCount,
      'betAmount': betAmount,
    };
  }

  @override
  String toString() {
    return 'Lottery{id: $id, gameName: $gameName, betCount: $betCount, betAmount: $betAmount }';
  }
}
