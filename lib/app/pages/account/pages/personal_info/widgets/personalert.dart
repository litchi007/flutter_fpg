import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/personal_info/personinfo_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/widgets/alertfrom_html.dart';
import 'package:get/get.dart';

class PersonAlert extends BaseAlertWidget {
  final String titleStr;

  PersonAlert(
    BuildContext context,
    this.titleStr,
  ) : super(
            context: context,
            title: const SizedBox(),
            content: const SizedBox(),
            action: const SizedBox());
  PersonInfoViewController controller = Get.find();
  final TextEditingController editController = TextEditingController();

  @override
  void showAlert() {
    title = titleWidget();
    content = contentWidget();
    action = actionWidget();
    titleColor = AppColors.ffff2f2f2;
    actionColor = AppColors.surface;
    super.dialogBuilder();
  }

  Widget titleWidget() {
    return Container(
      padding: EdgeInsets.only(left: 20.w, top: 20.w),
      color: Colors.transparent,
      child: Text(
        titleStr.contains('邮箱') ? '编辑邮箱' : '添加姓名',
        textAlign: TextAlign.center,
        style: AppTextStyles.ff858585_(context,
            fontSize: 24, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget contentWidget() {
    final String placeholder = titleStr.contains('邮箱') ? '请输入邮箱' : '真实姓名';
    // return SizedBox();
    return Padding(
      padding: EdgeInsets.only(left: 20.w, right: 20.w, bottom: 20.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (!titleStr.contains('邮箱')) // Only show the Text if mr is false
            Column(
              children: [
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: '请务必填写真实姓名', // Normal text
                        style: TextStyle(
                          fontSize: 16.sp,
                          fontWeight: FontWeight.normal,
                          color: AppColors.ff9e9e9e,
                        ),
                      ),
                      TextSpan(
                        text: '(填写后不可修改)', // Red text
                        style: TextStyle(
                          fontSize: 16.sp,
                          fontWeight: FontWeight.normal,
                          color: Colors.red, // Red color
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8.h),
              ],
            ),
          TextField(
            controller: editController,
            decoration: InputDecoration(
              hintText: placeholder,
              border: OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.ff9e9e9e,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget actionWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
            padding: EdgeInsets.only(bottom: 25.w),
            child: CustomButton(
              btnStr: '取消',
              width: 150.w,
              height: 80.w,
              backColor: AppColors.ffff2f2f2,
              onPressed: () {
                Get.back();
              },
            )),
        SizedBox(
          width: 50.w,
        ),
        Padding(
            padding: EdgeInsets.only(bottom: 25.w),
            child: CustomButton(
              btnStr: '确定',
              width: 150.w,
              height: 80.w,
              backColor: AppColors.fff08c34,
              onPressed: () {
                Get.back();
                titleStr.contains('邮箱')
                    ? controller.bindEmail(editController.text)
                    : controller.bindNickname(editController.text);
              },
            )),
      ],
    );
  }
}
