import 'package:flutter/material.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/personal_info/personinfo_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/personal_info/widgets/personalert.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:get/get.dart';

//UserInfoPage.ts
class PersonInfoView extends StatefulWidget {
  const PersonInfoView({super.key});

  @override
  State<PersonInfoView> createState() => _PersonInfoViewo();
}

class _PersonInfoViewo extends State<PersonInfoView>
    with SingleTickerProviderStateMixin {
  final PersonInfoViewController controller =
      Get.put(PersonInfoViewController());
  // String name = 'miya01';
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );

    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    // _controller.repeat(reverse: true); // Example animation configuration
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> animatedSpin() async {
    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Text('个人资料',
              style: AppTextStyles.ffffffff(context, fontSize: 25)),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // _renderAvatar(),
            UnitDataWidget(
              name: '帐号：',
              info: GlobalService.to.userInfo.value?.usr ?? '',
            ),
            Obx(() => UnitDataWidget(
                  name: '真实姓名：',
                  info: controller.nickname.value,
                  clickBtn: controller.showName.value,
                )),
            UnitDataWidget(
              name: 'QQ:',
              info: GlobalService.to.userInfo.value?.qq ?? '',
            ),
            UnitDataWidget(
              name: '手机：',
              info: GlobalService.to.userInfo.value?.phone ?? '',
            ),
            Obx(() => UnitDataWidget(
                  name: '邮箱：',
                  info: controller.email.value,
                  clickBtn: true,
                )),
            UnitDataWidget(
              name: '币别：',
              info: 'RMB',
            ),
          ],
        ),
      ),
    );
  }

  Widget _renderAvatar() {
    return Container(
      height: 100.w,
      decoration: const BoxDecoration(
        color: AppColors.ffbfa46d,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: 10.w,
          ),
          AppImage.network(
              GlobalService.to.userInfo.value?.isTest == true
                  ? AppDefine.defaultAvatar
                  : GlobalService.to.userInfo.value?.avatar ??
                      AppDefine.defaultAvatar,
              width: 85.w,
              height: 85.h,
              fit: BoxFit.contain),
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 20).w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    '${helloDate()}${GlobalService.to.userInfo.value?.usr ?? ''}',
                    style: AppTextStyles.ffffffff(context, fontSize: 20)),
                Padding(
                  padding: EdgeInsets.only(left: 1).w,
                  child: Row(
                    children: [
                      Text(
                        '头衔:',
                        style: AppTextStyles.ffffffff(
                          context,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        '${GlobalService.to.userInfo.value?.balance}',
                        style: AppTextStyles.ffcf352e(
                          context,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        'RMB',
                        style: AppTextStyles.ff000000(
                          context,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30.w, left: 10.w),
            child: Container(
              width: 15,
              height: 15,
              decoration: BoxDecoration(
                color: AppColors.ff42d1b9,
                borderRadius: BorderRadius.circular(7.5),
                border: Border.all(
                  color: AppColors.surface,
                  width: 2,
                ),
              ),
              child: GestureDetector(
                onTap: () async {
                  await animatedSpin();
                },
                child: AnimatedBuilder(
                  animation: _animation,
                  builder: (context, child) {
                    return Transform.rotate(
                      angle: _animation.value * 2.0 * 3.14,
                      child: GestureDetector(
                        onTap: () async {
                          // _controller.forward();
                          await animatedSpin();
                        },
                        child: const Icon(
                          Icons.autorenew_rounded,
                          color: AppColors.surface,
                          size: 10,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget insertLine() {
  return SizedBox(
    width: 1.sw,
    height: 1.sp,
    child: const ColoredBox(color: AppColors.bottomNavigationBarColor),
  );
}

class UnitDataWidget extends StatelessWidget {
  UnitDataWidget(
      {super.key, required this.name, required this.info, this.clickBtn});
  final String name;
  final String info;
  bool? clickBtn;
  @override
  Widget build(BuildContext context) {
    clickBtn ?? false;
    return Container(
      height: 50.h,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
              width: 1.w, color: AppColors.ffC5CBC6, style: BorderStyle.solid),
        ),
      ),
      padding: EdgeInsets.only(left: 10.w, right: 10.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            name,
            style: AppTextStyles.ff000000(
              context,
              fontSize: 18,
            ),
          ),
          SizedBox(
            child: Row(
              children: [
                Text(
                  info,
                  style: AppTextStyles.ff000000(
                    context,
                    fontSize: 18,
                  ),
                ),
                clickBtn == true
                    ? GestureDetector(
                        onTap: () {
                          PersonAlert alert = PersonAlert(context, name);
                          alert.showAlert();
                        },
                        child: SizedBox(
                          width: 40.w,
                          height: 30.w,
                          child: ColoredBox(
                            color: AppColors.ffbda671,
                            child: SizedBox(
                              child: Text(
                                name.contains('邮箱')
                                    ? info == ''
                                        ? '修改'
                                        : '绑定'
                                    : '添加',
                                style: AppTextStyles.ff000000(
                                  context,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

String helloDate() {
  DateTime now = DateTime.now();
  int curHour = now.hour;

  if (curHour >= 7 && curHour < 10) {
    return '早上好';
  } else if (curHour >= 10 && curHour < 15) {
    return '中午好';
  } else if (curHour >= 15 && curHour < 19) {
    return '下午好';
  } else if (curHour >= 19 || curHour < 7) {
    return '晚上好';
  }
  return '您好';
}

// AppToast.showToast('登录成功');
