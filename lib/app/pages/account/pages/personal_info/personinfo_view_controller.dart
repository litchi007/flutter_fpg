import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';

class PersonInfoViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxInt selectedTabIndex = 0.obs;
  RxString appLogo = ''.obs;
  RxString avatar = ''.obs;
  RxString defaultAvatar = ''.obs;
  RxString infoBalance = ''.obs;
  final AppUserRepository _appUserRepository = AppUserRepository();
  final AppUserRepository _apiUser = AppUserRepository();
  RxString email = ''.obs;
  RxString nickname = ''.obs;
  RxBool showName = false.obs;
  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    email.value = GlobalService.to.userInfo.value?.email ?? '';
    nickname.value = GlobalService.to.userInfo.value?.fullName ?? '';
    showName(GlobalService.to.userInfo.value?.fullName == '');
    isIniting(true);
    avatar.value = GlobalService.to.userInfo.value?.avatar ?? '';
    infoBalance.value = GlobalService.to.userInfo.value?.balance ?? '0';
    isIniting(false);
  }

  void bindEmail(String newemail) async {
    await _apiUser.appHttpClient
        .changeEmail(
      token: AppDefine.userToken?.apiSid,
      email: newemail,
    )
        .then((data) {
      AppToast.showDuration(msg: data.msg);
      email.value = newemail;

      if (data.code == 0) {
        GlobalService.to.getUserInfo();
      }
    });
  }

  void bindNickname(String inName) async {
    await _apiUser.appHttpClient
        .profileName(
      token: AppDefine.userToken?.apiSid,
      fullName: inName,
    )
        .then((data) {
      AppToast.showDuration(msg: data.msg);
      nickname.value = inName;
      showName(false);
      if (data.code == 0) {
        GlobalService.to.getUserInfo();
      }
    });
  }
}

// api.user.bindEmail(email).useSuccess(({ data }) => {
//               hideLoading();
//               showSuccess('修改成功');
//               UGStore.dispatch({ type: 'merge', userInfo: { email } });
//               setShowEditEmail(false);
//             });
