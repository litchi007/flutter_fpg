import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/AboutApp/about_app_view_controller.dart';
import 'package:get/get.dart';

import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class AboutAppView extends StatefulWidget {
  const AboutAppView({super.key});

  @override
  State<AboutAppView> createState() => _AboutAppViewState();
}

class _AboutAppViewState extends State<AboutAppView> {
  final AboutAppViewController controller = Get.put(AboutAppViewController());
  late final WebViewController _controller;
  var loadingPercentage = 0;
  String url = '${AppDefine.host}/mobile/#/ucenter/helpDetail/5?hideHeader=1';

  @override
  void initState() {
    super.initState();
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController webViewController =
        WebViewController.fromPlatformCreationParams(params);
    webViewController
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int value) {
            debugPrint('WebView is loading (progress : $value%)');
            controller.progress.value = value;
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
          },
          onHttpError: (HttpResponseError error) {
            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            // openDialog(request);
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      )
      ..loadRequest(Uri.parse(
          '${AppDefine.host}/mobile/#/ucenter/helpDetail/5?hideHeader=1'));
    if (webViewController.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (webViewController.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }

    _controller = webViewController;
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => controller.progress.value != 100
        ? Scaffold(
            backgroundColor: AppColors.surface,
            appBar: AppBar(
              backgroundColor: AppColors.ffbfa46d,
              automaticallyImplyLeading: false,
              titleSpacing: 0,
              leading: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ),
              title: Text(
                '关于我们',
                style: AppTextStyles.ffffffff(context, fontSize: 24),
              ),
              centerTitle: true,
            ),
            body: Obx(() => controller.progress.value != 100
                ? const CustomLoadingWidget()
                : const SizedBox()),
          )
        : Stack(alignment: Alignment.topCenter, children: [
            Scaffold(
              backgroundColor: AppColors.ffbfa46d,
              body: Column(
                children: [
                  ColoredBox(
                    color: AppColors.ffbfa46d,
                    child: SizedBox(
                      height: 50.h,
                    ),
                  ),
                  Expanded(child: WebViewWidget(controller: _controller)),
                ],
              ),
            ),
            Positioned(
              top: 65.h,
              left: 25.w,
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ),
            )
          ]));
    // );
  }
}
