import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class QdhelpDocViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxString showedItemNo = '1'.obs;
  RxBool showMenu = false.obs;
  RxDouble heightHeader = 0.0.obs;
  String xnbName = '';
  String xnbAndroidDownloadUrl = '';

  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  void fetchData() async {}

  void setShowSubmenu(bool show) {
    showMenu.value = show;
  }

  void setShowedItemNo(String path) {
    showedItemNo.value = path;
  }
}
