import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/deal.dart';
import 'package:fpg_flutter/data/repositories/app_graborder_repository.dart';

class GrabDetailViewController extends GetxController {
  final ApiGraborderRepository _appGraborderRepository =
      ApiGraborderRepository();
  Deal deal = Deal();
  String dealId = '0';
  bool dealDone = false;
  int leftTime = 0;
  bool dealing = false;
  @override
  void onInit() {
    super.onInit();
    getDetailData();
  }

// Function to update time, replace with your actual logic
  void updateTime() {
    print('Updating time...');
  }

  void getDetailData() async {
    String dealId = Get.arguments ?? '0';

    await _appGraborderRepository.detailForGrabOrder(dealId).then((data) {
      if (data == null) {
        AppToast.showDuration(msg: '服务出错');
        return;
      }
      deal = data;
      dealDone = data.status_num == 3;
      dealing = data.status_num == 6 || data.status_num == 10;
      leftTime = data.leftTime ?? 0;
    });
  }

  void setShowSubmenu(bool show) {}
  String getTimeLeft(int s) {
    int hour = s ~/ 60; // ~/ is the integer division operator in Dart
    int min = s % 60;
    return '${hour.toString().padLeft(2, '0')}:${min.toString().padLeft(2, '0')}';
  }
}
