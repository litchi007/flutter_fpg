import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/const/grab_const.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_order_main_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/components/main_list_cp.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/components/header_sub_menu.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_alert_dialog.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class DialogBoxProps {
  bool isVisible;

  DialogBoxProps({
    this.isVisible = false,
  });
}

class GrabOrderMainView extends StatefulWidget {
  const GrabOrderMainView({super.key});

  @override
  State<GrabOrderMainView> createState() => _GrabOrderMainViewState();
}

class _GrabOrderMainViewState extends State<GrabOrderMainView>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  late TabController _tabController;

  bool _isVisible = true;
  late FocusNode _focusNode;
  List<dynamic> payAccounts = [];
  NewDeal? curDeal;
  bool isShowBottomPanelDeposit = false;
  bool isShowBottomPanelWithDraw = false;

  GrabOrderMainViewController controller =
      Get.put(GrabOrderMainViewController());
  final TextEditingController minAmountController = TextEditingController();
  final TextEditingController maxAmountController = TextEditingController();
  DialogBoxProps dialogBoxData = DialogBoxProps();
  late CustomAlertDialog customAlertDialog;
  @override
  void initState() {
    super.initState();
    controller.heightHeader.value = 0;
    _tabController = TabController(length: categoryTabs.length, vsync: this);
    _tabController.addListener(_handleTabChange);
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 50),
    );

    _animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_controller);

    _animation.addListener(() {
      setState(() {});
    });

    _focusNode = FocusNode();
    _focusNode.addListener(() {
      if (!_focusNode.hasFocus) {
        _hideContainer();
      }
    });
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      controller.actionType = ActionType.values[_tabController.index];
    }
  }

  void setDialogBoxData(DialogBoxProps newData) {
    setState(() {
      dialogBoxData = newData;
    });
  }

  void hideDialogBox() {
    setDialogBoxData(DialogBoxProps(isVisible: false));
  }

  void showDialogBox() {
    setDialogBoxData(DialogBoxProps(isVisible: true));
  }

  void popAlert(String msg, VoidCallback callback) {
    customAlertDialog = CustomAlertDialog(
      context: context,
      title: msg,
      categories: [],
      titleBackgroundColor: AppColors.ffd1cfd0,
    );
    customAlertDialog.dialogBuilder([]);
  }

  bool checkQiangdanEnable() {
    return GlobalService.to.userInfo.value?.qdSwitch == 0;
  }

  void depositAction(NewDeal deal) {
    if (checkQiangdanEnable()) {
      setState(() {
        isShowBottomPanelDeposit = true;
        curDeal = deal;
      });
    } else {
      customAlertDialog = CustomAlertDialog(
        context: context,
        title: '抢单暂未开启',
        categories: [],
        titleBackgroundColor: AppColors.ffd1cfd0,
      );

      customAlertDialog.dialogBuilder([]);
    }
  }

  void withdrawAction(NewDeal deal) {
    if (checkQiangdanEnable()) {
      if (payAccounts.isNotEmpty) {
        setState(() {
          isShowBottomPanelWithDraw = true;
          curDeal = deal;
        });
      } else {
        customAlertDialog = CustomAlertDialog(
          context: context,
          title: '请先绑定提款账户',
          categories: [],
          titleBackgroundColor: AppColors.ffd1cfd0,
        );

        customAlertDialog.dialogBuilder([]);
      }
    } else {
      customAlertDialog = CustomAlertDialog(
        context: context,
        title: '抢单暂未开启',
        categories: [],
        titleBackgroundColor: AppColors.ffd1cfd0,
      );

      customAlertDialog.dialogBuilder([]);
    }
  }

  void _hideContainer() {
    setState(() {
      _isVisible = false;
      controller.showEntireBackground.value = false;
    });
    _controller.reverse();
  }

  void _showContainer() {
    setState(() {
      _isVisible = true;
      controller.showEntireBackground.value = true;
    });
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    customAlertDialog = CustomAlertDialog(
      context: context,
      title: '',
      categories: [],
      titleBackgroundColor: AppColors.ffd1cfd0,
    );
    return Stack(alignment: Alignment.topCenter, children: [
      Scaffold(
          backgroundColor: AppColors.surface,
          appBar: AppBar(
            backgroundColor: AppColors.ffe63534,
            automaticallyImplyLeading: false,
            titleSpacing: 0,
            leading: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15).w,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Row(children: [
                    Icon(Icons.arrow_back_ios,
                        color: AppColors.surface, size: 28.w),
                  ]),
                )),
            title: Text('抢单',
                style: AppTextStyles.surface_(context,
                    fontWeight: FontWeight.bold, fontSize: 24)),
            centerTitle: true,
            leadingWidth: 0.3.sw,
            actions: [
              IconButton(
                icon: const Icon(
                  Icons.more_vert,
                  color: AppColors.surface,
                ),
                onPressed: () {
                  controller.setShowSubmenu(true);
                },
              ),
            ],
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(46.h),
              child: Container(
                color: AppColors.ffe63534,
                child: Column(
                  children: [
                    _createTab(context),
                    SizedBox(
                      width: 1.sw,
                      height: 5.h,
                      child: const ColoredBox(color: AppColors.ffeaeaea),
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: Obx(
            () => (controller.isIniting.value)
                ? const CustomLoadingWidget()
                : Column(children: [
                    Expanded(
                      child: TabBarView(
                        controller: _tabController,
                        children: [
                          Obx(() => MainList(
                                prop: ListProp(
                                  loading: controller.isIniting.value,
                                  dealList: controller.dealList,
                                  depositAction: depositAction,
                                  withdrawAction: withdrawAction,
                                ),
                              )),
                          Obx(() => MainList(
                                prop: ListProp(
                                  loading: controller.isIniting.value,
                                  dealList: controller.dealList,
                                  depositAction: depositAction,
                                  withdrawAction: withdrawAction,
                                ),
                              )),
                          // Center(child: AppImage.svgByAsset('empty.svg')),
                          // Center(child: AppImage.svgByAsset('empty.svg')),
                        ],
                      ),
                    ),
                  ]),
          )),
      _filterItem(context),
      Obx(
        () => controller.showMenu.value
            ? HeaderSubmenu(onPressMenu: controller.setShowSubmenu)
            : const SizedBox(),
      ),
      Obx(() => controller.showEntireBackground.value == true
          ? GestureDetector(
              onTap: () {
                if (_isVisible) {
                  _hideContainer();
                }
              },
              child: Opacity(
                  opacity: 0.4,
                  child: Container(
                    color: AppColors.onBackground,
                    width: MediaQuery.of(context).size.width,
                  )),
            )
          : const SizedBox()),
      _sideMenu(context)
    ]);
  }

  Widget _filterItem(BuildContext context) {
    return Positioned(
      top: 110.h,
      right: 20.w,
      child: GestureDetector(
          onTap: _showContainer,
          child: AppImage.asset('filter.png',
              width: 30.w, height: 30.h, fit: BoxFit.contain)),
    );
  }

  Widget _sideMenu(BuildContext context) {
    return Positioned(
      right: (_controller.value - 1) * 0.9.sw,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(4.w),
          child: AnimatedContainer(
              duration: const Duration(milliseconds: 50),
              curve: Curves.easeInOut,
              height: 1.sh,
              width: 0.9.sw,
              color: AppColors.surface,
              child: Container(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 0.83.sw,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 35.h,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              '支付方式',
                              style: AppTextStyles.ff999999_(context,
                                  fontSize: 20),
                            )
                          ]),
                      Obx(() => Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: List.generate(
                            3,
                            (index) => _unitTransferItem(
                                context,
                                index,
                                controller.BANK_TYPE[index].toString(),
                                controller.isColoured[index].value),
                          ))),
                      Obx(
                        () => Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              _unitTransferItem(
                                  context,
                                  3,
                                  controller.BANK_TYPE[3].toString(),
                                  controller.isColoured[3].value)
                            ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16).h,
                        child: Divider(
                          height: 2.h,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 16).h,
                            child: Text(
                              '金额区间（元）',
                              style: AppTextStyles.ff999999_(context,
                                  fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            _amountItem(context, 'minAmount', '最低价',
                                minAmountController),
                            _amountItem(context, 'maxAmount', '最高价',
                                maxAmountController)
                          ]),
                      Padding(
                        padding: const EdgeInsets.only(top: 16).h,
                        child: Divider(
                          height: 2.h,
                        ),
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          _setButton(context, 0, '重置', false),
                          _setButton(context, 1, '确定', true),
                        ],
                      ),
                      SizedBox(
                        height: 30.h,
                      )
                    ],
                  ),
                ),
              ))),
    );
  }

  SizedBox _unitTransferItem(
      BuildContext context, int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.changeColorTransItem(index);
            controller.updatePayChannel(index);
          },
          child: Stack(alignment: Alignment.bottomRight, children: [
            Container(
              width: 0.25.sw,
              height: 52.h,
              margin: const EdgeInsets.only(top: 16).h,
              decoration: BoxDecoration(
                color: isColoured ? AppColors.ffFFEFEF : AppColors.ffF8F8F7,
                borderRadius: BorderRadius.circular(3).w,
              ),
              child: Center(
                child: Text(
                  '${amount}',
                  style: isColoured
                      ? AppTextStyles.ffBFA46D_(context, fontSize: 20)
                      : AppTextStyles.ff535B70_(context, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            isColoured
                ? AppImage.svgByAsset('ico_remove.svg')
                : const SizedBox(),
          ])),
    );
  }

  SizedBox _setButton(
      BuildContext context, int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            if (_isVisible) {
              _hideContainer();
            }
            if (index == 0) {
              controller.reset();
            } else {
              controller.fetchData();
            }
          },
          child: Container(
            width: 0.3.sw,
            height: 45.h,
            margin: const EdgeInsets.only(top: 16).h,
            decoration: BoxDecoration(
              color: isColoured ? AppColors.ffE44848 : AppColors.surface,
              borderRadius: BorderRadius.circular(8).w,
              border: Border.all(
                color: AppColors.ffE44848,
                width: 1.0.w,
              ),
            ),
            child: Center(
              child: Text(
                '${amount}',
                style: AppTextStyles.ff535B70_(context, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          )),
    );
  }

  Widget _amountItem(BuildContext context, String type, String hint,
      TextEditingController editController) {
    return Container(
      height: 45.h,
      width: 0.3.sw,
      margin: const EdgeInsets.only(top: 10).h,
      padding: const EdgeInsets.symmetric(vertical: 5).h,
      decoration: BoxDecoration(
        color: AppColors.ffF8F8F7,
        borderRadius: BorderRadius.circular(14).w,
      ),
      child: TextFormField(
        onChanged: (text) {
          type == 'minAmount'
              ? controller.minAmount = editController.text
              : controller.maxAmount = editController.text;
        },
        controller: editController,
        cursorColor: AppColors.ffeaeaea,
        style: AppTextStyles.ff535B70_(context, fontSize: 20),
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          isDense: true,
          contentPadding: const EdgeInsets.symmetric(horizontal: 20).w,
          iconColor: AppColors.surface,
          filled: true,
          fillColor: Colors.transparent,
          hintStyle: AppTextStyles.ff535B70_(context, fontSize: 20),
          hintText: hint,
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );
  }

  TabBar _createTab(BuildContext context) {
    return TabBar(
      controller: _tabController,
      indicatorColor: AppColors.ffffff00,
      labelColor: AppColors.ffffff00,
      unselectedLabelColor: AppColors.ffffffb3,
      indicatorSize: TabBarIndicatorSize.label,
      physics: const ClampingScrollPhysics(),
      tabAlignment: TabAlignment.start,
      isScrollable: true,
      labelStyle: AppTextStyles.surface_(context,
          fontWeight: FontWeight.bold, fontSize: 22),
      tabs: List.generate(categoryTabs.length, (index) {
        return Tab(
          text: '${categoryTabs[index]['name']}',
        );
      }),
    );
  }
}
