import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/data/models/sysConfModel/QdSupChannel.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/const/grab_const.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_create_view_controller.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';

class GrabCreateView extends StatefulWidget {
  const GrabCreateView({super.key});

  @override
  State<GrabCreateView> createState() => _GrabCreateViewState();
}

class _GrabCreateViewState extends State<GrabCreateView>
    with TickerProviderStateMixin {
  late TabController _tabController;
  List<BankInfoParam?> selectedItem = List.filled(4, null);
  GrabCreateViewController controller = Get.put(GrabCreateViewController());
  final TextEditingController _depositsController = TextEditingController();
  final TextEditingController _withdrawController = TextEditingController();
  final TextEditingController _withdrawCodeController = TextEditingController();
  @override
  void initState() {
    super.initState();
    controller.heightHeader.value = 0;
    _tabController = TabController(length: categoryTabs.length, vsync: this);
    _tabController.addListener(_handleTabChange);
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      controller.actionType = ActionType.values[_tabController.index];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffe63534,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ]),
            )),
        title: Text('发起抢单',
            style: AppTextStyles.surface_(context,
                fontWeight: FontWeight.bold, fontSize: 24)),
        centerTitle: true,
        leadingWidth: 0.3.sw,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(46.h),
          child: Container(
            color: AppColors.ffe63534,
            child: _createTab(context),
          ),
        ),
      ),
      body: Column(children: [
        Expanded(
          child: TabBarView(
            controller: _tabController,
            children: [
              _firstTabContent(context),
              _secondTabContent(context),
            ],
          ),
        ),
      ]),
    );
  }

  Widget _secondTabContent(BuildContext cotext) {
    return Column(
      children: [
        _renderInput(
            context,
            '取出金额',
            '请输入取出金额',
            _withdrawController,
            TextInputType.text,
            (text) => controller.withdrawAmount = text,
            false),
        Divider(
          height: 1.h,
          color: AppColors.ffF8F8F7,
        ),
        _renderInput(
            context,
            '取款密码',
            '请输入取款密码',
            _withdrawCodeController,
            TextInputType.visiblePassword,
            (text) => controller.withdrawCode = text,
            false),
        Container(
          color: AppColors.ffF8F8F7,
          padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10).h,
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('收款账号（至少选择一项）'),
            ],
          ),
        ),
        ...controller.qdSupChannels.map((item) => _supChannelItem(
              context,
              item,
            )),
        const Spacer(),
        _setButton(context, 0, '发布'),
        SizedBox(
          height: 10.h,
        ),
      ],
    );
  }

  Widget _supChannelItem(BuildContext context, QdSupChannel item) {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(
                width: 0.3.sw,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16).w,
                  child: Text(
                      withdrawTypes[controller.qdSupChannels.indexOf(item)]),
                )),
            Expanded(
              child: Obx(() => DropdownButton<BankInfoParam?>(
                    isExpanded: true,
                    hint: Text(
                      '未选择',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.bodyText3(context, fontSize: 20),
                    ),
                    underline: Container(
                      height: 1.h,
                      color: Colors.transparent,
                    ),
                    value: selectedItem[controller.qdSupChannels.indexOf(item)],
                    icon: const Icon(Icons.arrow_drop_down),
                    items: controller.arrAllAccountData.isEmpty
                        ? [
                            DropdownMenuItem<BankInfoParam?>(
                              value: null,
                              child: Text(
                                '',
                                textAlign: TextAlign.center,
                                style: AppTextStyles.bodyText2(
                                  context,
                                  fontSize: 20,
                                ),
                              ),
                            )
                          ]
                        : controller
                            .arrAllAccountData[
                                controller.qdSupChannels.indexOf(item)]
                            .data!
                            .map<DropdownMenuItem<BankInfoParam?>>(
                              (BankInfoParam curItem) =>
                                  DropdownMenuItem<BankInfoParam?>(
                                value: curItem,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10).w,
                                  child: Text(
                                    ' ${curItem.bankName},  ${curItem.bankCard}, ${curItem.ownerName} ',
                                    textAlign: TextAlign.center,
                                    style: AppTextStyles.bodyText2(
                                      context,
                                      fontSize: 20,
                                    ),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                    onChanged: (BankInfoParam? newValue) {
                      setState(() {
                        selectedItem[controller.qdSupChannels.indexOf(item)] =
                            newValue;
                      });
                      controller.updateAccount(
                        controller.qdSupChannels.indexOf(item),
                        convertToInt(newValue?.id ?? '-1') ?? -1,
                      );
                    },
                  )),
            ),
          ],
        ),
        Divider(
          height: 1.h,
          color: AppColors.ffF8F8F7,
        )
      ],
    );
  }

  Widget _firstTabContent(BuildContext cotext) {
    return Column(
      children: [
        _renderInput(
            context,
            '存入金额',
            '请输入存入金额',
            _depositsController,
            TextInputType.text,
            (text) => controller.depositAmount = (text),
            false),
        Divider(
          height: 1.h,
          color: AppColors.ffF8F8F7,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(16).w,
              child: Text(
                '支付方式',
                style: AppTextStyles.ff535B70_(context, fontSize: 20),
              ),
            ),
          ],
        ),
        _paymentMethodItem(context),
        Spacer(),
        _setButton(context, 0, '发布'),
        SizedBox(
          height: 10.h,
        ),
      ],
    );
  }

  Widget _paymentMethodItem(BuildContext context) {
    return SizedBox(
        width: 0.95.sw,
        child: Column(children: [
          Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(
                3,
                (index) => _unitTransferItem(
                    context,
                    index,
                    controller.BANK_TYPE[index].toString(),
                    controller.isColoured[index].value),
              ))),
          Obx(
            () => Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              _unitTransferItem(context, 3, controller.BANK_TYPE[3].toString(),
                  controller.isColoured[3].value),
            ]),
          )
        ]));
  }

  SizedBox _unitTransferItem(
      BuildContext context, int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.changeColorTransItem(index);
            controller.updatePayChannel(index);
          },
          child: Stack(alignment: Alignment.bottomRight, children: [
            Container(
              width: 0.3.sw,
              height: 36.h,
              margin: const EdgeInsets.only(top: 16).h,
              decoration: BoxDecoration(
                color: isColoured ? AppColors.ffFFEFEF : AppColors.ffF8F8F7,
                borderRadius: BorderRadius.circular(3).w,
              ),
              child: Center(
                child: Text(
                  amount,
                  style: isColoured
                      ? AppTextStyles.ffBFA46D_(context)
                      : AppTextStyles.ff535B70_(context),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            isColoured
                ? AppImage.svgByAsset('ico_remove.svg')
                : const SizedBox(),
          ])),
    );
  }

  SizedBox _setButton(BuildContext context, int index, String amount) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.create();
          },
          child: Container(
            height: 48.h,
            margin: const EdgeInsets.only(top: 16).h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4).w,
              color: AppColors.ffe63534,
            ),
            child: Center(
              child: Text(
                amount,
                style: AppTextStyles.surface_(context, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          )),
    );
  }

  Widget _renderInput(
      BuildContext context,
      String title,
      String hint,
      TextEditingController editController,
      TextInputType? keyboardType,
      void Function(String) onChanged,
      bool isObscure) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 10).h,
        child: Row(children: [
          Padding(
            padding: const EdgeInsets.all(12).w,
            child: Text(title,
                style: AppTextStyles.ff535B70_(context, fontSize: 20)),
          ),
          Container(
            height: 36.h,
            width: 0.5.sw,
            margin: const EdgeInsets.only(top: 10).h,
            padding: const EdgeInsets.symmetric(vertical: 5).h,
            decoration: const BoxDecoration(
              color: Colors.transparent,
            ),
            child: TextFormField(
              onChanged: (text) {
                onChanged(text);
              },
              keyboardType: keyboardType,
              obscureText: isObscure,
              controller: editController,
              cursorColor: AppColors.ff34650b,
              style: AppTextStyles.ff535B70_(context, fontSize: 18),
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                isDense: true,
                contentPadding: const EdgeInsets.symmetric(horizontal: 20).w,
                iconColor: AppColors.surface,
                filled: true,
                fillColor: Colors.transparent,
                hintStyle: AppTextStyles.ff535B70_(context, fontSize: 18),
                hintText: hint,
                border: const OutlineInputBorder(
                  borderSide: BorderSide.none,
                ),
              ),
            ),
          )
        ]));
  }

  TabBar _createTab(BuildContext context) {
    return TabBar(
      controller: _tabController,
      indicatorColor: AppColors.ffffff00,
      labelColor: AppColors.ffffff00,
      unselectedLabelColor: AppColors.ffffffb3,
      indicatorSize: TabBarIndicatorSize.label,
      physics: const ClampingScrollPhysics(),
      tabAlignment: TabAlignment.fill,
      isScrollable: false,
      labelStyle: AppTextStyles.surface_(context,
          fontWeight: FontWeight.bold, fontSize: 22),
      tabs: List.generate(categoryTabs.length, (index) {
        return Tab(
          text: '${createOrderTabs[index]['name']}',
        );
      }),
    );
  }
}
