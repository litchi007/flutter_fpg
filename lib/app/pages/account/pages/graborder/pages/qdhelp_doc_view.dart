import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/const/grab_const.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/qdhelp_doc_view_controller.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';

class QdhelpDocView extends StatefulWidget {
  const QdhelpDocView({super.key});

  @override
  State<QdhelpDocView> createState() => _QdhelpDocViewState();
}

class _QdhelpDocViewState extends State<QdhelpDocView>
    with TickerProviderStateMixin {
  QdhelpDocViewController controller = Get.put(QdhelpDocViewController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffe63534,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 22.w),
              ]),
            )),
        title: Text('抢单教程',
            style: AppTextStyles.surface_(context,
                fontWeight: FontWeight.bold, fontSize: 24)),
        centerTitle: true,
        leadingWidth: 0.3.sw,
      ),
      body: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0).w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ...cgList
                    .map((item) =>
                        _categoryTab(context, item.title!, item.content!))
                    .toList(),
              ],
            ),
          ),
        ),
        const VerticalDivider(
          width: 1.0,
          thickness: 1.0,
          color: AppColors.ffaeaeae,
        ),
        Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0).w,
              child: Obx(() => controller.showedItemNo.value == '1'
                  ? ListView.builder(
                      itemCount: 6,
                      itemBuilder: (context, index) {
                        if (index == 0) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '1、如何存款',
                                style: AppTextStyles.ff999999_(context,
                                    fontSize: 20),
                              ),
                              _Text(context, '如果您需要存款，“抢单”中有两种存款方式供您选择'),
                              _Text(context, '1. 通过抢单的方式存款'),
                              _image(context, 4)
                            ],
                          );
                        } else if (index == 1) {
                          return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _Text(context, '2. 通过发起订单抢购进行存款'),
                                _image(context, 5)
                              ]);
                        } else {
                          return _image(context, index + 4);
                        }
                      },
                    )
                  : controller.showedItemNo.value == '2'
                      ? ListView.builder(
                          itemCount: 4,
                          itemBuilder: (context, index) {
                            if (index == 0) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '2、如何取款',
                                    style: AppTextStyles.ff999999_(context,
                                        fontSize: 20),
                                  ),
                                  _Text(context, '如果您需要取款，“抢单”中有两种取款方式供您选择。'),
                                  _Text(context, '1. 通过抢单的方式取款'),
                                  _image(context, 9)
                                ],
                              );
                            } else if (index == 1) {
                              return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    _Text(context, '2. 通过发起抢单的方式取款'),
                                    _image(context, 10)
                                  ]);
                            } else {
                              return _image(context, index + 9);
                            }
                          },
                        )
                      : controller.showedItemNo.value == '3'
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                  Text(
                                    '3、查看抢单记录',
                                    style: AppTextStyles.ff999999_(context,
                                        fontSize: 20),
                                  ),
                                  _image(context, 16)
                                ])
                          : controller.showedItemNo.value == '4'
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                      _Text(context, 'CGpay 充值教程',
                                          fontSize: 21),
                                      Text(
                                        '4、确认存款金额',
                                        style: AppTextStyles.ff999999_(context,
                                            fontSize: 20),
                                      ),
                                      _Text(context,
                                          '在网站充值时，输入想要存入的人民币金额，并在下方选择CGpay支付，点击“开始充值”'),
                                      _imageJt(context, 'cgpay_money')
                                    ])
                              : ListView.builder(
                                  itemCount: 2,
                                  itemBuilder: (context, index) {
                                    if (index == 0) {
                                      return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            _Text(context, 'CGpay 充值教程',
                                                fontSize: 21),
                                            Text(
                                              '5、钱包付款',
                                              style: AppTextStyles.ff999999_(
                                                  context,
                                                  fontSize: 20),
                                            ),
                                            _Text(context,
                                                '页面跳转至CGpay二维码页面，系统会提示是否打开CGpay，选择打开即可'),
                                            _imageJt(context, 'cgpay_topay1')
                                          ]);
                                    } else {
                                      return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            _Text(context,
                                                '页面跳转至CGpay页面，输入CGpay的PIN码进行交易确认。交易完成后，您可以在钱包明细中看到支付详情。'),
                                            _imageJt(context, 'cgpay_topay2')
                                          ]);
                                    }
                                  })),
            ))
      ]),
    );
  }

  Widget _Text(BuildContext context, String title, {int fontSize = 20}) {
    return Padding(
      padding: const EdgeInsets.all(8.0).w,
      child: Text(title,
          style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
    );
  }

  Widget _categoryTab(BuildContext context, String title, String path) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.setShowedItemNo(path);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 14.0).h,
            child: Center(
                child: Obx(
              () => Text(title,
                  style: controller.showedItemNo.value == path
                      ? AppTextStyles.error_(context, fontSize: 20)
                      : AppTextStyles.bodyText2(context, fontSize: 20)),
            )),
          )),
    );
  }

  Widget _image(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0).h,
      child: AppImage.network(getImageByIndex(index), fit: BoxFit.fill),
    );
  }

  Widget _imageJt(BuildContext context, String path) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0).h,
      child: AppImage.network(img_jt(path), fit: BoxFit.fill),
    );
  }
}
