import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/deal.dart';
import 'package:fpg_flutter/data/repositories/app_graborder_repository.dart';

class GrabHistoryViewController extends GetxController {
  final ApiGraborderRepository _appGraborderRepository =
      ApiGraborderRepository();

  RxBool isIniting = false.obs;
  RxString showedItemNo = '1'.obs;
  RxBool showMenu = false.obs;
  RxDouble heightHeader = 0.0.obs;
  List<Record> historyData = [];

  @override
  void onInit() {
    super.onInit();

    fetchData();
  }

  void fetchData() async {
    await _appGraborderRepository.getGraborderRecords().then((data) {
      if (data == null) return;
      historyData = data.data;
    });
  }

  void setShowSubmenu(bool show) {
    showMenu.value = show;
  }

  void setShowedItemNo(String path) {
    showedItemNo.value = path;
  }
}
