import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/components/nodata_widget.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_history_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_language.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/deal.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GrabHistoryView extends StatefulWidget {
  const GrabHistoryView({super.key});

  @override
  State<GrabHistoryView> createState() => _GrabHistoryViewState();
}

class _GrabHistoryViewState extends State<GrabHistoryView>
    with TickerProviderStateMixin {
  GrabHistoryViewController controller = Get.put(GrabHistoryViewController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffe63534,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15).w,
          child: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Row(
              children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ],
            ),
          ),
        ),
        title: Text(
          '抢单记录',
          style: AppTextStyles.surface_(context,
              fontWeight: FontWeight.bold, fontSize: 24),
        ),
        centerTitle: true,
        leadingWidth: 0.3.sw,
      ),
      body: controller.historyData.isEmpty
          ? Center(child: NoDataWidget(loading: false))
          : ListView.builder(
              itemCount: controller.historyData.length,
              itemBuilder: (context, index) {
                Record item = controller.historyData[index];
                bool highlight = item.status_num == 6 || item.status_num == 10;
                String action = '存';
                String actionImg = img_static01('grab/ico_ck1', type: 'svg');
                if (item.type == 2) {
                  action = '取';
                  actionImg = img_static01('grab/ico_ck2', type: 'svg');
                }
                return _tileItem(context, item, highlight, action, actionImg);
              },
            ),
    );
  }

  Widget _tileItem(BuildContext context, Record item, bool highlight,
      String action, String actionImg) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(grabDetailPath, arguments: item.id);
      },
      child: Padding(
          padding: const EdgeInsets.all(8.0).w,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  SvgPicture.network(
                    actionImg,
                    fit: BoxFit.contain,
                    height: 20.w,
                  ),
                  _text(context, '订单号 : ${item.orderno}'),
                  const Spacer(),
                  Padding(
                    padding: const EdgeInsets.all(8.0).w,
                    child: Text(
                      item.status_str,
                      style: highlight
                          ? AppTextStyles.ffE44848_(context)
                          : AppTextStyles.ff999999_(context),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _text(context, '提单时间 : ${item.add_time}'),
                      _text(context,
                          '$action款金额 : ${currencyLogo[AppDefine.systemConfig?.currency]} ${item.amount}'),
                      _text(context,
                          '交易对象 : ${item.trade_username != '' ? item.trade_username : '-'}'),
                    ],
                  ),
                  const Spacer(),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _text(context, '>', fontSize: 30),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }

  Widget _text(BuildContext context, String title, {int fontSize = 14}) {
    return Padding(
      padding: const EdgeInsets.all(8.0).w,
      child: Text(
        title,
        style: AppTextStyles.ff999999_(context, fontSize: fontSize),
      ),
    );
  }
}
