import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/data/models/sysConfModel/QdSupChannel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/data/repositories/app_graborder_repository.dart';

class GrabCreateViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();
  final ApiGraborderRepository _appGraborderRepository =
      ApiGraborderRepository();

  RxInt index = 0.obs;
  RxBool showMenu = false.obs;
  RxDouble heightHeader = 0.0.obs;
  List<RxBool> isColoured = List.generate(4, (_) => false.obs);
  RxList<AllAccountListData> arrAllAccountData = RxList();
  RxList<BankInfoParam> arrFullBankList = RxList();
  RxBool bIsEditing = false.obs;
  RxBool showEntireBackground = false.obs;
  RxBool isIniting = false.obs;

  List<int> accounts = [];
  List<int> banks = [];
  List<QdSupChannel> qdSupChannels = [];
  String depositAmount = '';
  String withdrawAmount = '';
  String withdrawCode = '';
  String minAmount = '';
  String maxAmount = '';
  ActionType actionType = ActionType.DEPOSIT;

  final quickArr = [100, 500, 1000, 5000, 10000, '全部金额'];
  final BANK_TYPE = ['银行卡', '支付宝', '微信', '虚拟币'];

  @override
  void onInit() {
    super.onInit();
    qdSupChannels = AppDefine.systemConfig?.qdSupChannels ?? [];
    accounts = List.filled(qdSupChannels.length, 0);
    fetchData();
  }

  void changeColorTransItem(int index) {
    isColoured[index].value = isColoured[index].value ? false : true;
  }

  void create() {
    actionType == ActionType.DEPOSIT ? createDeposit() : createWithdraw();
  }

  bool checkQiangdanEnable() {
    return GlobalService.to.userInfo.value?.qdSwitch == 0;
  }

  Map<String, dynamic> convertAry2Obj(List<dynamic> array, String key) {
    Map<String, dynamic> obj = {};
    array.asMap().forEach((index, item) {
      obj['${key}[${index}]'] = item;
    });
    return obj;
  }

  void createDeposit() async {
    if (depositAmount == '') {
      AppToast.showDuration(msg: '请输入存入金额');
      return;
    }
    if (banks.isEmpty) {
      AppToast.showDuration(msg: '请选择支付方式');
      return;
    }
    if (!checkQiangdanEnable()) {
      AppToast.showDuration(msg: '抢单暂未开启');
      return;
    }
    await _appGraborderRepository
        .createDeposit(amount: depositAmount, allowBankType: banks)
        .then((data) {
      if (data.code == 200) {
        Get.toNamed(grabHistoryPath);
        AppToast.showDuration(msg: data.msg);
      } else {
        AppToast.showDuration(msg: '请求错误');
      }
    });
  }

  void createWithdraw() async {
    if (withdrawAmount == '') {
      AppToast.showDuration(msg: '请输入取出金额');
      return;
    }
    if (withdrawCode == '') {
      AppToast.showDuration(msg: '请输入取款密码');
      return;
    }
    if (banks.isEmpty) {
      AppToast.showDuration(msg: '请选择至少一项收款账号');
      return;
    }
    if (!checkQiangdanEnable()) {
      AppToast.showDuration(msg: '抢单暂未开启');
      return;
    }
    accounts = accounts.where((item) => item != -1).toList();

    await _appGraborderRepository
        .createWithdraw(
            amount: depositAmount, coinpwd: withdrawCode, payChannels: accounts)
        .then((data) {
      if (data.code == 200) {
        Get.toNamed(grabHistoryPath);
        AppToast.showDuration(msg: data.msg);
      } else {
        AppToast.showDuration(msg: '请求错误');
      }
    });
  }

  void updatePayChannel(int index) {
    isColoured[index].value == true
        ? banks.add(index + 1)
        : banks.remove(index + 1);
  }

  void updateAccount(int index, int id) {
    accounts[index] = id;
  }

  void fetchData() async {
    getBankCardData();
  }

  void setShowSubmenu(bool show) {
    showMenu.value = show;
  }

  void getBankCardData() async {
    await _appUserRepository
        .bankCard(token: AppDefine.userToken?.apiSid)
        .then((data) async {
      if (data != null) {
        arrAllAccountData.value = data.allAccountList ?? [];
      }
    });
  }
}
