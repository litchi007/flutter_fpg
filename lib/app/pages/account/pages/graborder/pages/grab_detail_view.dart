import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/const/grab_const.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_detail_view_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';

class GrabDetailView extends StatefulWidget {
  const GrabDetailView({super.key});

  @override
  State<GrabDetailView> createState() => _GrabDetailViewState();
}

class _GrabDetailViewState extends State<GrabDetailView>
    with TickerProviderStateMixin {
  GrabDetailViewController controller = Get.put(GrabDetailViewController());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var timeLeft =
        controller.deal.leftTime != null && controller.deal.leftTime! > 1
            ? controller.getTimeLeft(controller.deal.leftTime ?? 0)
            : 0;

    return Scaffold(
        backgroundColor: AppColors.ffe63534,
        appBar: AppBar(
          backgroundColor: AppColors.ffe63534,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15).w,
              child: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.surface, size: 28.w),
                ]),
              )),
          title: Text('订单详情',
              style: AppTextStyles.surface_(context,
                  fontWeight: FontWeight.bold, fontSize: 24)),
          centerTitle: true,
          leadingWidth: 0.3.sw,
        ),
        body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.w),
            child: Stack(alignment: Alignment.topRight, children: [
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4.0).w,
                        child: Text(
                          controller.deal.status_str ?? '',
                          style: AppTextStyles.surface_(context, fontSize: 24),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20.0).h,
                        child: Text(
                          '$timeLeft  ${controller.deal.status_desc}',
                          style: AppTextStyles.surface_(context, fontSize: 18),
                        ),
                      ),
                      controller.dealDone
                          ? AppImage.network(
                              img_assets(
                                  'qiangdan/success@3x'), // Replace with actual image URL
                              width: 36
                                  .w, // Assuming scale is a function returning a double
                              height: 36.h,
                            )
                          : Container(),
                    ],
                  ),
                  _dealHeader(context),
                  _dealContent(context),
                ],
              ),
              Positioned(
                  top: 168.h,
                  right: 100.w,
                  child: Container(
                    width: 18.w,
                    height: 18.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.ffe63534,
                    ),
                  )),
              Positioned(
                  top: 168.h,
                  right: 508.w,
                  child: Container(
                    width: 18.w,
                    height: 18.w,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.ffe63534,
                    ),
                  ))
            ])));
  }

  Widget _dealHeader(BuildContext context) {
    // Initial values
    String color = '#BB7672';
    String numColor = '#DD1D13';
    String bgImg = 'bg-red@3x';
    String bgColor = '#FBEDEB';

// Determine deposit or withdraw
    bool isDeposit =
        (controller.deal.type == '1' && controller.deal.sendType == 'A') ||
            (controller.deal.type == '2' && controller.deal.sendType == 'B');
    String txt1 = isDeposit ? '存入' : '取出';
    String txt2 = controller.deal.sendType == 'A' ? '抢单' : '提单';
    dynamic time = controller.deal.sendType == 'A'
        ? controller.deal.add_time
        : controller.deal.pickTime;

// Conditionally update values based on deal.status_num
    if (controller.deal.status_num == 3) {
      color = '#66A684';
      numColor = '#0E924C';
      bgImg = 'bg-green@3x';
      bgColor = '#E8F4ED';
    } else if (controller.deal.status_num == 4) {
      color = '#999';
      numColor = '#333';
      bgImg = 'bg-gray@3x';
      bgColor = '#ECECEC';
    }
    return Container(
      decoration: const BoxDecoration(
          // image: DecorationImage(image:  ExactAssetImage('assets/svg/bg_1.svg')  ),
          color: AppColors.ffF8F8F7,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(8), topRight: Radius.circular(8))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              _text(context, '$txt1金额'),
              Text(
                '${currencyLogo[AppDefine.systemConfig?.currency]}  ${controller.deal.amount != null ? controller.deal.amount : 0}',
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
            ],
          ),
          Column(
            children: [
              _text(context, '订单号'),
              Text(
                '$txt2时间 ',
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
            ],
          ),
          Column(
            children: [
              _text(context, controller.deal.orderno ?? ''),
              Text(
                '',
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _dealContent(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          // image: DecorationImage(image:  ExactAssetImage('assets/svg/bg_1.svg')  ),
          color: AppColors.surface,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(8), bottomRight: Radius.circular(8))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            children: [
              _textContent(context, '支持平台'),
              const Spacer(),
              _textContent(
                context,
                '银行卡',
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _text(BuildContext context, String title) {
    return Padding(
        padding: EdgeInsets.all(16).w,
        child:
            Text(title, style: AppTextStyles.ff535B70_(context, fontSize: 20)));
  }

  Widget _textContent(BuildContext context, String title) {
    return Padding(
        padding: EdgeInsets.all(16).w,
        child:
            Text(title, style: AppTextStyles.bodyText2(context, fontSize: 20)));
  }
}
