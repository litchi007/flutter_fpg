import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:fpg_flutter/data/repositories/app_graborder_repository.dart';

class GrabOrderMainViewController extends GetxController {
  final ApiGraborderRepository _appGraborderRepository =
      ApiGraborderRepository();
  RxBool isIniting = false.obs;
  RxList<NewDeal> dealList = RxList();
  RxInt index = 0.obs;
  RxBool showMenu = false.obs;
  RxDouble heightHeader = 0.0.obs;
  RxBool showEntireBackground = false.obs;
  final quickArr = [100, 500, 1000, 5000, 10000, '全部金额'];
  final BANK_TYPE = ['银行卡', '支付宝', '微信', '虚拟币'];
  String minAmount = '';
  String maxAmount = '';
  ActionType actionType = ActionType.DEPOSIT;

  List<RxBool> isColoured = List.generate(4, (_) => false.obs);

  List<int> payChannels = [];
  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  void depositAction(item) {
    print('Deposit action triggered for ${item['amount']}');
  }

  void withdrawAction(item) {
    print('Deposit action triggered for ${item['amount']}');
  }

  void changeColorTransItem(int index) {
    isColoured[index].value = !isColoured[index].value;
  }

  void fetchData() async {
    isIniting.value = true;
    await _appGraborderRepository
        .getList(
            type: actionType,
            payChannel: payChannels,
            amountMin: convertToInt(minAmount),
            amountMax: convertToInt(maxAmount))
        .then((data) {
      if (data == null) return;
      dealList.value = data.data;
    });
    isIniting.value = false;
  }

  void setShowSubmenu(bool show) {
    showMenu.value = show;
  }

  // part for sidepanel
  void reset() {
    minAmount = '';
    maxAmount = '';
    payChannels = [];
  }

  void updatePayChannel(int index) {
    isColoured[index].value == true
        ? payChannels.add(index + 1)
        : payChannels.remove(index + 1);
  }
}
