import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';

class NoDataWidget extends StatelessWidget {
  final bool loading;

  NoDataWidget({required this.loading});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 470.h,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AppImage.svgByAsset('empty.svg'),
          SizedBox(height: 20.h),
          Text(
            loading ? '正在努力加载中...' : '空空如也',
            style: AppTextStyles.ffC5CBC6_(context, fontSize: 20),
          ),
        ],
      ),
    );
  }
}
