import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/routes.dart';

class HeaderSubmenu extends StatelessWidget {
  final Function(bool show) onPressMenu;
  HeaderSubmenu({required this.onPressMenu});

  void handlePressMenu(String pageName) {
    onPressMenu(false);
    Get.toNamed(pageName);
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      // top: Platform.isIOS ? scale(75) : scale(42),
      top: 42.h,
      right: 20.w,
      child: Container(
        padding: const EdgeInsets.fromLTRB(12, 16, 12, 16).w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4).w,
          color: AppColors.surface,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildMenuItem(context, '发起抢单', grabCreatePath),
            _buildMenuItem(context, '抢单记录', grabHistoryPath),
            _buildMenuItem(context, '使用教程', grabCoursePath),
          ],
        ),
      ),
    );
  }

  Widget _buildMenuItem(BuildContext context, String title, String pageName) {
    return GestureDetector(
      onTap: () => handlePressMenu(pageName),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0).h,
        child: Text(
          title,
          style: AppTextStyles.bodyText2(context, fontSize: 20),
        ),
      ),
    );
  }
}
