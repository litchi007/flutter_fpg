import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/const/grab_const.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/components/nodata_widget.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/helper.dart';

class ListProp {
  final bool loading;
  final List<NewDeal> dealList;
  void Function(NewDeal) depositAction;
  void Function(NewDeal) withdrawAction;

  ListProp({
    required this.loading,
    required this.dealList,
    required this.depositAction,
    required this.withdrawAction,
  });
}

class MainList extends StatelessWidget {
  final ListProp prop;

  MainList({required this.prop});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
          child: prop.loading
              ? NoDataWidget(loading: true)
              : prop.dealList.isNotEmpty
                  ? ListView.builder(
                      itemCount: prop.dealList.length,
                      itemBuilder: (context, index) {
                        return DealItemWidget(
                          item: prop.dealList[index],
                          depositAction: prop.depositAction,
                          withdrawAction: prop.withdrawAction,
                        );
                      },
                    )
                  : NoDataWidget(loading: false),
        ));
  }
}

class DealItemWidget extends StatelessWidget {
  final NewDeal item;
  void Function(NewDeal) depositAction;
  void Function(NewDeal) withdrawAction;
  DealItemWidget({
    required this.item,
    required this.depositAction,
    required this.withdrawAction,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16).w,
      margin: EdgeInsets.only(bottom: 16).h,
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: AppColors.ffC5CBC6)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(item.ownerUsername ?? ''),
          SizedBox(height: 24.h),
          Row(
            children: [
              ...item.allowBankType!.map((bank) {
                String bankIcon =
                    (convertToInt(bank.index) ?? 0) < BANK_ICON_MAP.length
                        ? BANK_ICON_MAP[convertToInt(bank.index) ?? 0]
                        : '';
                return SvgPicture.asset(
                  img_images(bankIcon, type: 'svg'),
                  width: 16.w,
                  height: 16.h,
                );
              }),
              if (item.type == ActionType.DEPOSIT)
                Text(item.position ?? '',
                    style: AppTextStyles.ff8D8B8B_(context, fontSize: 20)),
            ],
          ),
          SizedBox(height: 10.h),
          Text(
              '${currencyLogo[AppDefine.systemConfig?.currency ?? ''] ?? ''} ${item.amount}',
              textAlign: TextAlign.center),
          SizedBox(height: 10.h),
          Row(
            children: [
              if (item.type == ActionType.DEPOSIT)
                ElevatedButton(
                  onPressed: () => depositAction.call(item),
                  style: ElevatedButton.styleFrom(
                    overlayColor: Colors.red,
                    padding:
                        EdgeInsets.symmetric(vertical: 7.h, horizontal: 24.w),
                  ),
                  child: const Text(
                    '存款',
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                  ),
                ),
              if (item.type == ActionType.WITHDRAW)
                ElevatedButton(
                  onPressed: () => withdrawAction.call(item),
                  style: ElevatedButton.styleFrom(
                    overlayColor: Colors.red,
                    padding:
                        EdgeInsets.symmetric(vertical: 7.h, horizontal: 24.w),
                  ),
                  child: const Text(
                    '提现',
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                  ),
                ),
            ],
          ),
          SizedBox(height: 16.h),
        ],
      ),
    );
  }
}
