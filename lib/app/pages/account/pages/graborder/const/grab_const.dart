import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

List<Map<String, String>> categoryTabs = ([
  {
    'id': '0',
    'name': '我要存',
  },
  {
    'id': '1',
    'name': '我要取',
  },
]);
List<String> withdrawTypes = [
  '银行卡',
  '支付宝',
  '微信',
  '虚拟币',
];

List<Map<String, String>> createOrderTabs = ([
  {
    'id': '0',
    'name': '我要存入',
  },
  {
    'id': '1',
    'name': '我要取出',
  },
]);
List<UGinviteInfoModel> cgList = [
  UGinviteInfoModel(
    title: '如何存款',
    isPress: true,
    content: '1',
  ),
  UGinviteInfoModel(
    title: '如何取款',
    isPress: false,
    content: '2',
  ),
  UGinviteInfoModel(
    title: '查看抢单记录',
    isPress: false,
    content: '3',
  ),
];

const headerColor = Color(0xFFE63633);
const themeColor = Color(0xFFE63633);

final List<String> BANK_ICON_MAP = [
  '', // Index 0 (empty string as placeholder)
  'grab/ico_card',
  'grab/ico_alipay',
  'grab/ico_wechat',
  'grab/ico_xnb',
];
/////////////////temp variable for convert react to flutter/////////////////////

NewDeal newDeal = NewDeal(
  id: '1',
  type: ActionType.WITHDRAW,
  ownerUsername: 'hidden_user',
  withdrawUsername: 'user',
  amount: '500',
  position: null, // Example of position
  allowBankType: [bankType],
);

DealListData dealListData = DealListData(
  data: [newDeal],
  total: 1,
);

dynamic transationDatailItem = {
  'owner_username': 'John Doe', // Example owner username
  'allow_bank_type': [
    {'index': 0}, // Example bank type data
    {'index': 1},
    // Add more as needed
  ],
  'type': ActionType.DEPOSIT, // Example type
  'position': 'New York', // Example position
};

var currencyLogo = {
  'USD': '\$',
  'EUR': '€',
  // Add more currencies as needed
};

class Constants {
  static double scale(double value) {
    // Implement your scale function here if needed
    return value.sp; // Placeholder implementation
  }
}

Filter filter = Filter(
  type: ActionType.DEPOSIT,
  payChannel: [1, 2], // Example of pay channels
  amountMin: 100,
  amountMax: 1000,
);

BankType bankType = BankType(
  index: '1',
  enName: 'bank',
  name: '银行卡',
);

List<NewDeal> sampleDealList = [
  NewDeal(
    id: '1',
    type: ActionType.DEPOSIT,
    ownerUsername: 'John Doe',
    withdrawUsername: 'Withdraw User 1',
    amount: '100',
    position: 'Some Position',
    allowBankType: [
      BankType(index: '1', enName: 'bank', name: '银行卡'),
      BankType(index: '2', enName: 'alipay', name: '支付宝'),
    ],
  ),
  NewDeal(
    id: '2',
    type: ActionType.WITHDRAW,
    ownerUsername: 'Jane Smith',
    withdrawUsername: 'Withdraw User 2',
    amount: '200',
    position: null,
    allowBankType: [
      BankType(index: '3', enName: 'wechat', name: '微信'),
    ],
  ),
  NewDeal(
    id: '3',
    type: ActionType.DEPOSIT,
    ownerUsername: 'John Doe',
    withdrawUsername: 'Withdraw User 1',
    amount: '100',
    position: 'Some Position',
    allowBankType: [
      BankType(index: '1', enName: 'bank', name: '银行卡'),
      BankType(index: '2', enName: 'alipay', name: '支付宝'),
    ],
  ),
  NewDeal(
    id: '4',
    type: ActionType.WITHDRAW,
    ownerUsername: 'Jane Smith',
    withdrawUsername: 'Withdraw User 2',
    amount: '200',
    position: null,
    allowBankType: [
      BankType(index: '3', enName: 'wechat', name: '微信'),
    ],
  ),
  // Add more sample data as needed
];
