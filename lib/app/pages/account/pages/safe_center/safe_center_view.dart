import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/account_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/safe_center/safe_center_view_controller.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/template/template4/mine/template4_mine.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';

class SafeCenterView extends StatefulWidget {
  const SafeCenterView({super.key});

  @override
  State<SafeCenterView> createState() => _SafeCenterViewState();
}

class _SafeCenterViewState extends State<SafeCenterView> {
  final SafeCenterViewController controller =
      Get.put(SafeCenterViewController());
  late int initialTabIndex;
  void initState() {
    super.initState();
    List<int>? args = Get.arguments;
    initialTabIndex = args?[0] ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return true; // 阻止默认返回行为
        },
        child: Scaffold(
          backgroundColor: AppColors.surface,
          appBar: AppBar(
              backgroundColor: AppColors.ffbfa46d,
              automaticallyImplyLeading: false,
              titleSpacing: 0,
              toolbarHeight: 65.h,
              leading: GestureDetector(
                onTap: () => {Get.back(), Get.back()},
                child: Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 18.w),
              ),
              title: Obx(
                () => Text(
                    controller.arrTitles[controller.selectedTabIndex.value],
                    style: AppTextStyles.surface_(context, fontSize: 24)),
              ),
              centerTitle: true,
              actions: [
                Padding(
                    padding:
                        const EdgeInsets.only(right: 10, top: 10, bottom: 10).w,
                    child: Obx(
                      () => (controller.selectedTabIndex.value != 2 ||
                              (controller.addressLength.value >= 3))
                          ? const SizedBox()
                          : Container(
                              width: 40.w,
                              height: 40.h,
                              decoration: BoxDecoration(
                                color: AppColors.ffe5c06f,
                                border: Border.all(
                                    width: 1.w, color: AppColors.surface),
                                borderRadius: BorderRadius.circular(3.w),
                              ),
                              child: Center(
                                  child: Text(
                                controller.isPlusButton.value == true
                                    ? '+'
                                    : '-',
                                style: AppTextStyles.surface_(context,
                                    fontSize: 35),
                              ))),
                    )).onTap(() {
                  controller.isPlusButton.value =
                      !controller.isPlusButton.value;
                })
              ]),
          body: DefaultTabController(
            initialIndex: initialTabIndex,
            length: controller.arrTitles.length,
            child: Column(
              children: [
                TabBar(
                  tabs: [
                    ...List.generate(controller.arrTitles.length, (index) {
                      return Tab(
                        text: controller.arrTitles[index],
                      );
                    }),
                  ],
                  labelColor: AppColors.ffff0000,
                  unselectedLabelColor: AppColors.ff5C5869,
                  indicatorColor: AppColors.ffff0000,
                  labelStyle: AppTextStyles.bodyText1(context, fontSize: 20),
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      SignInPassword(),
                      TakeMoneyPassword(),
                      JDModifyLoginPlaceCP(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

class SignInPassword extends StatelessWidget {
  SignInPassword({super.key});

  final TextEditingController oldPwdController = TextEditingController();
  final TextEditingController newPwdController = TextEditingController();
  final TextEditingController confirmNewPwdController = TextEditingController();

  final SafeCenterViewController controller = Get.find();

  String passLimit = '0';
  String passLengthMin = '6';
  String passLengthMax = '12';

  String pwdPlaceholder() {
    switch (passLimit) {
      case '1':
        return '请输入$passLengthMin到$passLengthMax位数字字母组成的密码';
      case '2':
        return '请输入$passLengthMin到$passLengthMax数字字母符号组成的密码';
      default:
        return '请输入$passLengthMin到$passLengthMax位长度的密码';
    }
  }

  void _handleChangePassword(BuildContext context) async {
    if (oldPwdController.text.isEmpty) {
      AppToast.showToast('请填写原登录密码');
    } else if (newPwdController.text.isEmpty) {
      AppToast.showToast('请输入您的新密码');
    } else if (confirmNewPwdController.text.isEmpty) {
      AppToast.showToast('请输入您的确认新密码');
    } else if (newPwdController.text.length > int.parse(passLengthMax) ||
        newPwdController.text.length < int.parse(passLengthMin)) {
      AppToast.showToast('请输入$passLengthMin到$passLengthMax位长度的密码');
    } else if (confirmNewPwdController.text.length > int.parse(passLengthMax) ||
        confirmNewPwdController.text.length < int.parse(passLengthMin)) {
      AppToast.showToast('请输入$passLengthMin到$passLengthMax位长度的密码');
    } else if (newPwdController.text == confirmNewPwdController.text) {
      _showLoading(context);
      // Simulate API call
      await controller
          .changeLoginPwd(oldPwdController.text, newPwdController.text)
          .then((data) {
        _showSuccess(context, data ?? '');
        oldPwdController.clear();
        newPwdController.clear();
        confirmNewPwdController.clear();
      });
    } else {
      _showError(context, '账号或密码错误，请重新尝试');
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.selectedTabIndex.value = 0;
    });
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 30.h),
          ),
          CustomPasswordField('请输入旧密码', oldPwdController),
          CustomPasswordField(pwdPlaceholder(), newPwdController),
          CustomPasswordField(pwdPlaceholder(), confirmNewPwdController),
          SizedBox(height: 20.h),
          ElevatedButton(
            onPressed: () => _handleChangePassword(context),
            style: ElevatedButton.styleFrom(
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
              minimumSize: Size.fromHeight(50.h),
              backgroundColor: AppColors.ffbfa46d,
            ),
            child: Text(
              '确定修改',
              style: AppTextStyles.bodyText1(context, fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}

void _showLoading(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => Center(child: CircularProgressIndicator()),
  );
}

void _showSuccess(BuildContext context, String message) {
  Navigator.of(context).pop();
  AppToast.showToast(message);
}

void _showError(BuildContext context, String message) {
  AppToast.showToast(message);
}

Widget CustomPasswordField(
    String placeholder, TextEditingController controller) {
  return Padding(
    padding: EdgeInsets.only(top: 2.h),
    child: SizedBox(
      height: 50.h,
      child: TextField(
        controller: controller,
        obscureText: true,
        enableSuggestions: false,
        autocorrect: false,
        style: TextStyle(fontSize: 20.sp),
        decoration: InputDecoration(
          hintText: placeholder,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(2),
            borderSide: BorderSide(color: Colors.black.withOpacity(0.35)),
          ),
          filled: true,
          fillColor: Colors.black.withOpacity(0.1),
        ),
      ),
    ),
  );
}

class TakeMoneyPassword extends StatelessWidget {
  TakeMoneyPassword({super.key});

  final TextEditingController oldPwdController = TextEditingController();
  final TextEditingController newPwdController = TextEditingController();
  final TextEditingController confirmNewPwdController = TextEditingController();
  SafeCenterViewController controller = Get.find();
  final AuthController _authController = Get.find();

  String switchCoinPwd = '1'; // Set this based on your logic
  bool isHBTemplate = false; // Set this based on your logic

  void _handleSubmit(BuildContext context) async {
    bool hasFundPwd = controller.userInfo?.hasFundPwd ?? false;

    if (oldPwdController.text.isEmpty) {
      AppToast.showToast(hasFundPwd ? '请填写旧取款密码' : '请填写登录密码');
    } else if (newPwdController.text.isEmpty ||
        newPwdController.text.length != 4) {
      AppToast.showToast('请输入您的4位数字提款密码');
    } else if (confirmNewPwdController.text.isEmpty ||
        confirmNewPwdController.text.length != 4) {
      AppToast.showToast('请输入您的4位数字提款密码');
    } else if (newPwdController.text != confirmNewPwdController.text) {
      AppToast.showToast('两次输入的新密码不一致');
    } else {
      _showLoading(context);

      if (hasFundPwd) {
        // Simulate API call
        await controller
            .changeFundPwd(oldPwdController.text, newPwdController.text)
            .then((data) {
          _showSuccess(context, data.msg ?? '');
          oldPwdController.clear();
          newPwdController.clear();
          confirmNewPwdController.clear();
          if (data.code == 0) {
            GlobalService.to.getUserInfo();
            Navigator.pop(context);
          }
        });
        // print("hasFundPwd");
      } else {
        if (newPwdController.text.length != 4) {
          AppToast.showToast('请输入您的4位数字提款密码');
        } else {
          await controller
              .addFundPwd(oldPwdController.text, newPwdController.text)
              .then((data) {
            _showSuccess(context, data.msg ?? '');
            oldPwdController.clear();
            newPwdController.clear();
            confirmNewPwdController.clear();
            if (data.code == 0) {
              GlobalService.to.getUserInfo();
              Navigator.pop(context);

              Get.toNamed(depositPath, arguments: [1]);
            }
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.selectedTabIndex.value = 1;
    });

    bool hasFundPwd = controller.userInfo?.hasFundPwd ?? false;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 30.h),
          ),
          CustomPasswordField(
            hasFundPwd ? '请输入旧取款密码' : '请输入登录密码',
            oldPwdController,
          ),
          CustomPasswordField(
            '请输入新取款密码',
            newPwdController,
          ),
          CustomPasswordField(
            '再次输入新密码',
            confirmNewPwdController,
          ),
          if (switchCoinPwd == '1')
            TextButton(
              onPressed: () {
                AppNavigator.toNamed(forgetPwdPath);
              },
              child: Text(
                '忘记取款密码？',
                style: AppTextStyles.bodyText1(context, fontSize: 20),
              ),
              // style: ElevatedButton.styleFrom(
              //   backgroundColor: AppColors.ffbfa46d,
              // ),
            ),
          ElevatedButton(
            onPressed: () => _handleSubmit(context),
            style: ElevatedButton.styleFrom(
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
              minimumSize: Size.fromHeight(50.h),
              backgroundColor: AppColors.ffbfa46d,
            ),
            child: Text('提交',
                style: AppTextStyles.bodyText1(context, fontSize: 20)),
          ),
        ],
      ),
    );
  }
}

class JDModifyLoginPlaceCP extends StatelessWidget {
  JDModifyLoginPlaceCP({super.key});
  SafeCenterViewController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    // _warningOverlay(context);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.selectedTabIndex.value = 2;
    });
    return Obx(() => Column(
          children: [
            Gap(20.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Obx(
                () => Column(
                  children: [
                    ...controller.addressArray.map(
                      (addr) {
                        return Padding(
                            padding: EdgeInsets.only(
                                bottom: 20.h, left: 20.w, right: 20.w),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  '常用地址: ',
                                  style: AppTextStyles.bodyText1(context,
                                      fontSize: 20),
                                ),
                                Text(
                                  '${addr.country == '0' ? '中国 ' : '国外 '} ',
                                  style: AppTextStyles.bodyText1(context,
                                      fontSize: 20),
                                ),

                                Text(
                                  '${addr.province ?? ''} ',
                                  style: AppTextStyles.bodyText1(context,
                                      fontSize: 20),
                                ),

                                Text(
                                  '${addr.city ?? ''} ',
                                  style: AppTextStyles.bodyText1(context,
                                      fontSize: 20),
                                ),
                                // const Spacer(),
                                GestureDetector(
                                  onTap: () {
                                    controller.deleteAddress(addr.id ?? '');
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10.w, vertical: 2.h),
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Text(
                                      '删除',
                                      style: AppTextStyles.bodyText1(context,
                                          fontSize: 20),
                                    ),
                                  ),
                                ),
                              ],
                            ));
                      },
                    ),
                  ],
                ),
              ),
            ),
            ..._renderSettingPanel(context, controller.isPlusButton.value),

            // showWarning Widget here
          ],
        ));
  }

  List<Widget> _renderSettingPanel(BuildContext context, bool _show) {
    return _show == true
        ? [const SizedBox()]
        : [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 120.w,
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(5.w),
                      ),
                      side: BorderSide(width: 1.w),
                    )),
                    child: Obx(
                      () => DropdownButton<String>(
                        value: controller.selectedCountry.value,
                        isExpanded: true,
                        underline: Container(),
                        items: controller.countries.map((String country) {
                          return DropdownMenuItem<String>(
                            value: country,
                            child: Text(
                              country,
                              overflow: TextOverflow.ellipsis,
                              style: AppTextStyles.bodyText1(context,
                                  fontSize: 20),
                            ),
                          );
                        }).toList(),
                        onChanged: (String? newValue) {
                          controller.updateCountryOption(
                              controller.countries.indexOf(newValue ?? ''));
                        },
                      ),
                    ),
                  ),
                  Container(
                    width: 150.w,
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(5.w),
                      ),
                      side: BorderSide(width: 1.w),
                    )),
                    child: Obx(
                      () => DropdownButton<String>(
                        value: controller.selectedProvince.value,
                        isExpanded: true,
                        underline: Container(),
                        items: controller.provinces.map((String province) {
                          return DropdownMenuItem<String>(
                            value: province,
                            child: Text(
                              province,
                              overflow: TextOverflow.ellipsis,
                              style: AppTextStyles.bodyText1(context,
                                  fontSize: 20),
                            ),
                          );
                        }).toList(),
                        onChanged: (String? newValue) {
                          controller.updateProvinceOption(
                              controller.provinces.indexOf(newValue ?? ''));
                        },
                      ),
                    ),
                  ),
                  // if (controller.countries
                  //         .indexOf(controller.selectedCountry.value) ==
                  //     0)
                  Container(
                    width: 180.w,
                    padding: EdgeInsets.symmetric(horizontal: 20.w),
                    decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(5.w),
                      ),
                      side: BorderSide(width: 1.w),
                    )),
                    child: Obx(
                      () => SizedBox(
                        width: 140.w,
                        child: DropdownButton<String>(
                          value: controller.selectedCity.value,
                          isExpanded: true,
                          underline: Container(),
                          items: controller.cities.map((String city) {
                            return DropdownMenuItem<String>(
                              value: city,
                              child: Text(
                                city,
                                overflow: TextOverflow.ellipsis,
                                style: AppTextStyles.bodyText1(context,
                                    fontSize: 20),
                              ),
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            controller.selectedCity.value = newValue ?? '';
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.h),
            Obx(() => controller.isLoading.value == true
                ? const CircularProgressIndicator()
                : Padding(
                    padding: EdgeInsets.symmetric(horizontal: 50.w),
                    child: ElevatedButton(
                      onPressed: () {
                        controller.changAddress();
                        controller.isPlusButton.value = true;
                      },
                      style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.all(20.w),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        ),
                        minimumSize: Size.fromHeight(50.h),
                        backgroundColor: AppColors.ffbfa46d,
                      ),
                      child: Text(
                        '保存',
                        style: AppTextStyles.bodyText1(context, fontSize: 20),
                      ),
                    ),
                  )),
          ];
  }

  void _warningOverlay(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      // executes after build
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.transparent,
            shape: ContinuousRectangleBorder(
              borderRadius: BorderRadius.circular(10.w),
            ),
            content: ColoredBox(
              color: AppColors.background,
              child: Padding(
                padding: EdgeInsets.all(30.w),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '⚠️为了您的账号安全，现在可以绑定常用登录地',
                      style: AppTextStyles.bodyText1(context, fontSize: 24),
                    ),
                    SizedBox(height: 20.h),
                    Text(
                      '1、绑定后，只有在常用地范围内，才能正常登录',
                      style: AppTextStyles.bodyText1(context, fontSize: 20),
                    ),
                    Text(
                      '2、可以绑定多个常用地',
                      style: AppTextStyles.bodyText1(context, fontSize: 20),
                    ),
                    Text(
                      '3、绑定后，可选择默认选项（请选择国家-请选择省-请选择市）即可自行解除绑定',
                      style: AppTextStyles.bodyText1(context, fontSize: 20),
                    ),
                    SizedBox(height: 20.h),
                    const Divider(),
                    SizedBox(
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          '我知道了',
                          style: AppTextStyles.bodyText1(context, fontSize: 20),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    });
  }
}
