// import 'package:fpg_flutter/data/models/BannersModel.dart';

import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_json.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class SafeCenterViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();
  RxBool isLoading = false.obs;
  RxBool showPlusButton = true.obs;
  RxBool isPlusButton = false.obs;
  RxInt selectedTabIndex = 0.obs;
  final List<String> arrTitles = ['登录密码', '取款密码', '常用登录地'];

  UserToken? user_token = AppDefine.userToken;
  UGUserModel? userInfo = GlobalService.to.userInfo.value;
  RxInt addressLength = 0.obs;
  RxList<Address> addressArray = [
    Address(country: '0', province: "北京", city: "北京"),
  ].obs;

  List<String> countries = ["中国", "国外"];
  RxList<String> provinces = ["北京", "上海", "广东"].obs;
  RxList<String> cities = ["北京", "上海", "广州"].obs;
  RxString selectedCountry = ''.obs;
  RxString selectedProvince = ''.obs;
  RxString selectedCity = ''.obs;
  RxBool showWarningOverlay = false.obs;

  Map<String, dynamic>? fullAddrData;
  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    await AppJSON.loadJsonObjFromAssets('json/address.json').then((data) {
      fullAddrData = data;
      updateCountryOption(0);
    });
    showWarningOverlay.value = true;
    getAddress();

    selectedCountry.value = countries.first;
    selectedProvince.value = provinces.first;
    selectedCity.value = cities.first;
  }

  void changAddress() async {
    isLoading.value = true;
    bool isDuplicated = false;
    Address newAddress = Address(
      id: (addressArray.isEmpty
              ? 1
              : (int.parse(addressArray[addressArray.length - 1].id ?? '0')) +
                  1)
          .toString(),
      province: countries.indexOf(selectedCountry.value) != 0
          ? ''
          : selectedProvince.value,
      city: countries.indexOf(selectedCountry.value) != 0
          ? ''
          : selectedCity.value,
      country: countries.indexOf(selectedCountry.value).toString(),
    );

    for (Address address in addressArray) {
      if (address.city == newAddress.city &&
          address.province == newAddress.province) {
        isDuplicated = true;
      }
    }

    if (isDuplicated) {
      AppToast.showToast('地址重复了哦');
    } else {
      addressArray.add(newAddress);
      addressLength.value = addressArray.length;
      // Call the API to change the address
      await changeAddress(newAddress).then((data) {
        AppToast.showToast(data);
      });
    }

    isLoading.value = false;
  }

  void updateCountryOption(int ind) {
    selectedCountry.value = countries.elementAt(ind);
    if (ind != 0) {
      cities.value = [];
      provinces.value = [];
      return;
    }

    if (fullAddrData != null) {
      provinces.value = [];
      for (dynamic province_data in fullAddrData?['data']) {
        provinces.add(province_data['name']);
      }
      selectedProvince.value = provinces.first;
      updateProvinceOption(0);
    }
  }

  void updateProvinceOption(int ind) {
    int cnt = 0;
    selectedProvince.value = provinces.elementAt(ind);
    if (fullAddrData != null) {
      cities.value = [];
      for (dynamic provinceData in fullAddrData?['data']) {
        if (cnt == ind) {
          for (dynamic cityData in provinceData['city']) {
            cities.add(cityData['name']);
          }
        }
        cnt++;
      }
      selectedCity.value = cities.first;
    }
  }

  Future<String?> changeLoginPwd(String oldPwd, String newPwd) async {
    String? resMsg = '';
    await _appUserRepository
        .changeLoginPwd(oldPwd, newPwd, token: user_token?.apiSid)
        .then((data) async {
      resMsg = data?.msg;
      // if (data?.code == 0) {
      //   resMsg = data?.message;
      // } else {

      // }
      AppLogger.d("changefundpwd$resMsg");
    });
    return resMsg;
  }

  Future<dynamic> changeFundPwd(String oldPwd, String newPwd) async {
    dynamic resMsg;
    await _appUserRepository
        .changeFundPwd(oldPwd, newPwd, token: user_token?.apiSid)
        .then((data) async {
      resMsg = data;

      AppLogger.d("changefundpwd$resMsg");
    });

    return resMsg;
  }

  Future<dynamic> addFundPwd(String loginPwd, String fundPwd) async {
    dynamic resMsg = '';
    await _appUserRepository
        .addFundPwd(loginPwd, fundPwd, token: user_token?.apiSid)
        .then((data) async {
      resMsg = data;
      AppLogger.d("addfundpwd$resMsg");
    });

    return resMsg;
  }

  Future<String?> getAddress() async {
    String? resMsg = '';
    await _appUserRepository
        .getAddress(token: user_token?.apiSid)
        .then((data) async {
      // resMsg = data?.message;
      addressArray.value = data ?? [];
      addressLength.value = addressArray.length;
    });

    return resMsg;
  }

  Future<String?> deleteAddress(String id) async {
    String? resMsg = '';
    await _appUserRepository
        .delAddress(token: user_token?.apiSid, id: id)
        .then((data) async {
      resMsg = data?.msg;
      getAddress();
    });

    return resMsg;
  }

  Future<String?> changeAddress(Address newAddress) async {
    String? resMsg = '';
    List<Address> newList = addressArray.map((item) => item).toList();

    await _appUserRepository
        .changeAddress(newList, token: user_token?.apiSid)
        .then((data) async {
      resMsg = data?.msg;
      getAddress();
    });
    return resMsg;
  }
}
