import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/credit/real_trans_log_view_controller.dart';
import 'package:fpg_flutter/data/models/RealGamesModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class RealTransLogView extends StatefulWidget {
  const RealTransLogView({super.key});

  @override
  State<RealTransLogView> createState() => _RealTransLogViewState();
}

class _RealTransLogViewState extends State<RealTransLogView> {
  final AuthController authController = Get.find();
  final RealTransLogViewController controller =
      Get.put(RealTransLogViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        toolbarHeight: 65.h,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title: Text('额度转换记录',
            style: AppTextStyles.surface_(context, fontSize: 24)),
        centerTitle: true,
      ),
      body: SizedBox(
          width: 1.sw,
          child: SingleChildScrollView(child: CustomDataTable(context))),
    );
  }
}

Widget CustomDataTable(BuildContext context) {
  final RealTransLogViewController controller =
      Get.put(RealTransLogViewController());

  return Obx(() => DataTable(
        headingRowHeight: 60.h,
        columnSpacing: 0,
        horizontalMargin: 0,
        showCheckboxColumn: false,
        columns: [
          _dataColumn(context, '游戏', 0.25),
          _dataColumn(context, '金额', 0.25),
          _dataColumn(context, '单号', 0.25),
          _dataColumn(context, '日期', 0.25),
        ],
        rows: controller.logList
            .map(
              (item) => DataRow(
                cells: [
                  DataCell(Center(
                      child:
                          Text(item.gameName!, textAlign: TextAlign.center))),
                  DataCell(Center(
                    child: Text(
                        '${currencyLogo[AppDefine.systemConfig?.currency ?? ''] ?? ''} ${item.amount}',
                        textAlign: TextAlign.center),
                  )),
                  DataCell(GestureDetector(
                    onTap: () {
                      controller.selectedIndex.value =
                          controller.logList.indexOf(item);
                      controller.bDialogOpened.value = true;
                      openDetailDialog(context);
                    },
                    child:
                        Center(child: Text('详情', textAlign: TextAlign.center)),
                  )),
                  DataCell(Center(
                    child: Text(item.actionTime!, textAlign: TextAlign.center),
                  )),
                ],
              ),
            )
            .toList(),
      ));
}

DataColumn _dataColumn(BuildContext context, String title, double widthRatio) {
  return DataColumn(
      label: Container(
          decoration: BoxDecoration(
              color: AppColors.fff0f0f0,
              border: Border.all(color: AppColors.ffaeaeae, width: 1.w)),
          child: SizedBox(
              width: widthRatio.sw,
              child: Center(
                  child: Text(title,
                      textAlign: TextAlign.center,
                      style:
                          AppTextStyles.bodyText3(context, fontSize: 18))))));
}

void openDetailDialog(BuildContext context) {
  final RealTransLogViewController controller = Get.find();

  Get.dialog(
    AlertDialog(
      title: Text(
        '查看详情',
        style: AppTextStyles.bodyText1(context, fontSize: 24),
        textAlign: TextAlign.center,
      ),
      content: const DetailView(), // Replace with your detail rendering logic
      actions: [
        TextButton(
          onPressed: () {
            controller.bDialogOpened.value = false;
            Navigator.pop(context);
          },
          child: Text(
            '取消',
            style: AppTextStyles.bodyText1(context, fontSize: 18),
          ),
        ),
        TextButton(
          onPressed: () {
            controller.bDialogOpened.value = false;
            Navigator.pop(context);
          },
          child: Text(
            '确定',
            style: AppTextStyles.bodyText1(context, fontSize: 18),
          ),
        ),
      ],
    ),
    barrierDismissible: true,
  );
}

class DetailView extends StatelessWidget {
  const DetailView({super.key});

  @override
  Widget build(BuildContext context) {
    final RealTransLogViewController controller =
        Get.put(RealTransLogViewController());
    TransferLogs selectedData = controller.logList[
        controller.selectedIndex.value]; // Replace with your actual model
    String gameUsername = selectedData.gameUsername ?? '';
    String gameCat = selectedData.gameCat ?? '';
    String gameName = selectedData.gameName ?? '';
    String amount = selectedData.amount ?? '';
    String billno = selectedData.billno ?? '';
    String actionTime = selectedData.actionTime ?? '';
    String isAuto = selectedData.isAuto ?? '';

    return SizedBox(
      height: 400.h,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          detailRow(context, '游戏帐号：', gameUsername),
          detailRow(context, '游戏代码：', gameCat),
          detailRow(context, '游戏名称：', gameName),
          detailRow(context, '金额：', amount),
          detailRow(context, '单号：', billno, numberOfLines: 2),
          detailRow(context, '日期：', actionTime),
          detailRow(context, '转换模式：', isAuto == '1' ? '自动' : '手动'),
        ],
      ),
    );
  }

  Widget detailRow(BuildContext context, String label, String value,
      {int numberOfLines = 1}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h),
      child: Row(
        children: [
          Text(
            label,
            style: AppTextStyles.bodyText1(context, fontSize: 18),
          ),
          Expanded(
            child: Text(
              value,
              maxLines: numberOfLines,
              style: AppTextStyles.bodyText1(context, fontSize: 18),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
