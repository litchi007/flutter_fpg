// import 'package:fpg_flutter/data/models/BannersModel.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/RealGamesModel.dart';
import 'package:fpg_flutter/data/repositories/app_real_repository.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class RealTransLogViewController extends GetxController {
  final AppRealRepository _appRealRepository = AppRealRepository();

  RxList<TransferLogs> logList = RxList();
  RxInt logTotal = 0.obs;
  RxBool bDialogOpened = false.obs;
  RxInt selectedIndex = 0.obs;

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    getTransferLogs();
  }

  void getTransferLogs() async {
    String? token = AppDefine.userToken!.apiSid;
    DateFormat dateTimeFormat = DateFormat('yyyy-MM-dd HH:mm:ss');
    DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    DateTime endDate = DateTime.now();
    DateTime startDate = DateTime(endDate.year, endDate.month - 1, endDate.day);
    if (startDate.month < 1) {
      startDate =
          DateTime(startDate.year - 1, startDate.month + 12, startDate.day);
    }
    String startTime = dateTimeFormat.format(startDate);
    String endTime = '${dateFormat.format(endDate)} 23:59:59';

    await _appRealRepository
        .transferLogs(token, startTime, endTime, page: 1, rows: 20)
        .then((data) {
      if (data?.list != null) {
        logList.value = data!.list!;
      }
    });
  }
}
