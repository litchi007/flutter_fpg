import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/configs/key_config.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/credit/real_trans_view_controller.dart';
import 'package:fpg_flutter/data/models/RealGamesModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:gap/gap.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class RealTransView extends StatefulWidget {
  const RealTransView({super.key});

  @override
  State<RealTransView> createState() => _RealTransViewState();
}

class _RealTransViewState extends State<RealTransView> {
  final AuthController authController = Get.find();
  final RealTransViewController controller = Get.put(RealTransViewController());
  final TextEditingController _txtController = TextEditingController();
  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    // controller.dropdownSources.value = ['请选择钱包'];
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        toolbarHeight: 65.h,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title:
            Text('额度转换', style: AppTextStyles.surface_(context, fontSize: 24)),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20).w,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.ffe5c06f,
                foregroundColor: AppColors.surface,
                side: BorderSide(width: 1.w, color: AppColors.surface),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3.w),
                ),
                padding: EdgeInsets.all(0.w),
                fixedSize: Size(105.w, 35.h),
                // maximumSize: Size(105.w, 35.h),
                minimumSize: Size(105.w, 35.h),
              ),
              onPressed: () {
                Get.toNamed(realtransLogPath);
                // pressRecord();
              },
              child: const Text('转换记录'),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ...List.generate(
                      realGameTypes.length,
                      (index) {
                        final navItemName = realGameTypes[index].name;
                        return GestureDetector(
                          onTap: () {
                            if (controller.selectedTabIndex.value == index) {
                              return;
                            }
                            controller.selectedTabIndex.value = index;
                            String key = realGameTypes[index].key;
                            controller.updateDropdownSources(key);
                          },
                          child: Obx(
                            () => Container(
                              margin: EdgeInsets.symmetric(horizontal: 10.w),
                              decoration:
                                  controller.selectedTabIndex.value == index
                                      ? const BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                                color: AppColors.ffff0000,
                                                width: 2.0),
                                          ),
                                        )
                                      : null,
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 10.w, right: 10.w, bottom: 10.w),
                                    child: Text(
                                      navItemName,
                                      style: TextStyle(
                                          color: controller
                                                      .selectedTabIndex.value ==
                                                  index
                                              ? AppColors.ffff0000
                                              : AppColors.ff666666,
                                          fontSize: 18.sp),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              Divider(
                color: AppColors.ff666666,
                height: 1.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 30.h, horizontal: 10.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    CustomDropdown(
                      itemIndex: 0,
                      labelText: '转出钱包',
                    ),
                    CustomDropdown(
                      itemIndex: 1,
                      labelText: '转入钱包',
                    ),
                    // CustomDropdown(),
                  ],
                ),
              ),
              Container(
                // padding: EdgeInsets.only(left: 10.w),
                // margin: EdgeInsets.all(10.w),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: AppColors.ff01d4ff,
                  borderRadius: BorderRadius.circular(8.w),
                ),
                width: 520.w,
                height: 55.h,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 85.w,
                      child: Text(
                        ' 转入金额:',
                        style: AppTextStyles.ff666666(context, fontSize: 18),
                      ),
                    ),
                    Container(
                      width: 425.w,
                      height: double.infinity,
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                        color: AppColors.background,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8.w),
                          bottomRight: Radius.circular(8.w),
                        ),
                        border: Border.all(
                          color: AppColors.ff01d4ff,
                        ),
                      ),
                      child: TextField(
                        controller: _txtController,
                        keyboardType: TextInputType.number,
                        style: TextStyle(fontSize: 18.sp),
                        onChanged: (value) => {
                          controller.money.value = _txtController.text != ''
                              ? double.parse(_txtController.text)
                              : 0
                        },
                        onTapOutside: (event) => {
                          controller.money.value = _txtController.text != ''
                              ? double.parse(_txtController.text)
                              : 0,
                        },
                        decoration: const InputDecoration(
                          hintText: '请输入金额',
                          // contentPadding: EdgeInsets.zero,
                          border: InputBorder.none,
                          isDense: true,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              CustomTransferButton(bIsAll: false, strCaption: '开始转换'),
              CustomTransferButton(bIsAll: true, strCaption: '一键提取'),
              Gap(30.h),
              CustomListView(),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomDropdown extends StatelessWidget {
  final RealTransViewController controller = Get.find();
  final int itemIndex;
  final String labelText;

  CustomDropdown({super.key, required this.itemIndex, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(left: 10.w),
      height: 55.h,
      width: 255.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.w),
        border: Border.all(
          color: AppColors.ff01d4ff,
          width: 1.0,
        ),
        color: AppColors.ff01d4ff,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 85.w,
            child: Text(
              ' $labelText:',
              style: AppTextStyles.ff666666(context, fontSize: 18),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 5.w),
            decoration: BoxDecoration(
              color: AppColors.background,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(8.w),
                bottomRight: Radius.circular(8.w),
              ),
            ),
            // constraints: BoxConstraints(minWidth: 155.w),
            width: 160.w,
            child: Obx(
              () => (DropdownButton<String>(
                icon: Container(),
                hint: Text(
                  '请选择钱包',
                  style: AppTextStyles.ff000000(context, fontSize: 18),
                ),
                style: AppTextStyles.ff252525_(
                  context,
                  fontSize: 18,
                ),
                underline: Container(),
                value: controller.dataArr.isNotEmpty
                    ? controller.dataArr
                        .firstWhere((item) =>
                            item.title == controller.transferOption[itemIndex])
                        .title
                    : '',
                onChanged: (String? value) {
                  controller.transferOption[itemIndex] = value!;
                },
                items: controller.dataArr.map<DropdownMenuItem<String>>(
                  (WalletData value) {
                    return DropdownMenuItem<String>(
                      value: value.title,
                      child: Text(
                        value.title,
                        style: AppTextStyles.ff000000(context, fontSize: 18),
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  },
                ).toList(),
              )),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomTransferButton extends StatelessWidget {
  final bool bIsAll;
  final String strCaption;
  final RealTransViewController controller = Get.find();

  CustomTransferButton(
      {super.key, required this.bIsAll, required this.strCaption});

  void _showAlert(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: Text(
                'OK',
                style: AppTextStyles.error_(context, fontSize: 18),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showLoading() {
    bIsAll
        ? controller.isLoading[1].value = true
        : controller.isLoading[0].value = true;
  }

  void _hideLoading() {
    bIsAll
        ? controller.isLoading[1].value = false
        : controller.isLoading[0].value = false;
  }

  Future<void> manualTransfer(BuildContext context) async {
    String transOut = controller.transferOption[0];
    String transIn = controller.transferOption[1];
    if (transOut == '' || transIn == '' || transOut == transIn) {
      transOut == '' || transIn == ''
          ? _showAlert(context, '请选择需要转出和转入的游戏类型')
          : _showAlert(context, '输入钱包和输出钱包不能一致');
    } else if ((AppDefine.keyConfig!.siteId.contains('t002') ||
            AppDefine.keyConfig!.siteId.contains('t024')) &&
        controller.money.value > 50.0) {
      _showAlert(context, "测试服转换金额不得超过50");
      return;
    } else {
      _showLoading();
      try {
        bool bSuccess = await controller.apiRealManualTransfer(
            transOut, transIn, controller.money.value);
        if (bSuccess == true) {
          _showAlert(context, '转入成功');
          int indexOut =
              controller.dataArr.indexWhere((item) => item.title == transOut);
          int indexIn =
              controller.dataArr.indexWhere((item) => item.title == transIn);
          if (indexOut == 0) {
            controller.updateMyPurse(false);
          } else {
            String balance = await controller.getRealBalance(indexOut);
          }
          if (indexIn == 0) {
            controller.updateMyPurse(true);
          } else {
            String balance = await controller.getRealBalance(indexIn);
          }
        } else {
          _showAlert(context, '转入失败');
        }
      } catch (e) {
        // Handle error
      } finally {
        _hideLoading();
      }
    }
  }

  Future<void> autoTransfer(BuildContext context) async {
    _showLoading();

    try {
      final transferOutData = await controller.oneKeyTransferOut();

      if (transferOutData == null) return;

      int cnt = 0;
      for (ResponseGameData game in transferOutData.games!) {
        bool bRes = await controller.quickTransferOut(game.id.toString());
        // if (bRes == true) {
        cnt++;
        if (cnt >= transferOutData.games!.length) {
          _showAlert(context, '一键提取完成');
          controller.updateUserInfo();
        }
      }
      // }
    } catch (e) {
      // Handle error
    } finally {
      _hideLoading();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20.h),
      child: SizedBox(
        height: 48.h,
        width: 520.w,
        child: Obx(() {
          bool isloading = controller.isLoading[bIsAll ? 1 : 0].value;
          return ElevatedButton(
            onPressed: () {
              if (controller.isLoading[0].value == true ||
                  controller.isLoading[1].value == true) return;
              if (bIsAll) {
                autoTransfer(context);
              } else {
                manualTransfer(context);
              }
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6.w),
                ),
                backgroundColor: AppColors.ffbfa46d,
                foregroundColor: AppColors.surface),
            child: isloading == true
                ? const CircularProgressIndicator()
                : Text(strCaption),
          );
        }),
      ),
    );
  }
}

class CustomListView extends StatelessWidget {
  final RealTransViewController controller = Get.find();
  CustomListView({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SizedBox(
        child: ListView.builder(
          shrinkWrap: true,
          physics:
              const NeverScrollableScrollPhysics(), // Prevent ListView from scrolling
          itemCount: controller.dataArr.length,
          itemBuilder: (context, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.w),
                  child: ListTile(
                    leading: AppImage.network(
                      controller.dataArr[index].pic,
                      fit: BoxFit.contain,
                      height: 30.h,
                      width: 30.w,
                    ),
                    title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            controller.dataArr[index].title,
                            style: AppTextStyles.ff000000(
                              context,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            controller.dataArr[index].balance != '*****'
                                ? "${double.parse(controller.dataArr[index].balance).toStringAsFixed(2)} RMB"
                                : '*****',
                            style: AppTextStyles.ffff0000_(
                              context,
                              fontSize: 18,
                            ),
                          )
                        ]),
                    trailing: CustomSpinner(
                      index: index,
                      key: ValueKey(controller.selectedTabIndex * 100 + index),
                    ),
                  ),
                ),
                const Divider(
                  color: AppColors.ff666666,
                  height: 0,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class CustomSpinner extends StatefulWidget {
  final int index;
  const CustomSpinner({super.key, required this.index});

  @override
  _CustomSpinnerState createState() => _CustomSpinnerState();
}

class _CustomSpinnerState extends State<CustomSpinner>
    with SingleTickerProviderStateMixin {
  final RealTransViewController realTransController = Get.find();

  late AnimationController _controller;
  late Animation<double> _animation;
  bool failed = false;
  String balance = '*****'; // defalt balance value

  @override
  void initState() {
    super.initState();

    failed = false;
    balance = '*****';

    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );

    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> animatedSpin() async {
    _controller.forward();
  }

  void checkBalance(int id) async {
    try {
      balance = await realTransController.getRealBalance(id);
    } catch (e) {
      balance = 'none';
    }
    setState(() {
      failed = balance == 'none' &&
              realTransController.checkStatusArr.elementAt(id) == true
          ? true
          : false;
    });
    // Your checkBalance logic here
  }

  @override
  Widget build(BuildContext context) {
    Widget rightContent;

    if (failed) {
      rightContent = SizedBox(
        width: 100.w,
        height: 30.w,
        child: Text(
          '加载失败',
          style: AppTextStyles.ff000000(context, fontSize: 18),
        ),
      );
      // } else if (balance != '*****') {
    } else if (realTransController.dataArr[widget.index].balance != "*****") {
      rightContent = SizedBox(
        width: 100.w,
        height: 30.h,
      ); // Empty container if balance is not '*****'
    } else if (widget.index == 0) {
      rightContent = SizedBox(
        width: 100.w,
        height: 30.h,
      );
    } else {
      rightContent = GestureDetector(
        onTap: () async {
          await animatedSpin();
          checkBalance(widget.index);
        },
        child: AnimatedBuilder(
          animation: _animation,
          builder: (context, child) {
            return Transform.rotate(
              angle: _animation.value * 2.0 * 3.14,
              child: SizedBox(
                width: 100.w,
                height: 30.h,
                child: const Icon(
                  Icons.refresh, // Replace with your desired icon
                  size: 20,
                  color: AppColors
                      .ff000000, // Replace with your desired icon color
                ),
              ),
            );
          },
        ),
      );
    }

    return SizedBox(
      width: 100.w,
      height: 30.h,
      child: Align(
        alignment: Alignment.centerRight,
        child: rightContent,
      ),
    );
  }
}
