import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/data/models/RealGamesModel.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:fpg_flutter/data/repositories/app_real_repository.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';

class RealTransViewController extends GetxController {
  RxInt selectedTabIndex = 0.obs;
  RxList<String> transferOption = [
    '我的钱包',
    '我的钱包',
  ].obs;
  RxDouble tAmount = 0.0.obs;
  final AppGameRepository _appGameRepository = AppGameRepository();
  final AppRealRepository _appRealRepository = AppRealRepository();

  RxList<RealGameData> realGamesList = RxList();
  RxList<WalletData> dataArr = RxList();
  RxList<bool> checkStatusArr =
      RxList(); //Item ? 0:default, 1:animating, 2:completed
  List<RxBool> isLoading = [false.obs, false.obs];
  RxDouble money = 0.0.obs;

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    dataArr.value = [
      WalletData(
        title: '我的钱包',
        id: '0',
        pic: img_root('images/realtrans/real0'),
        balance: GlobalService.to.userInfo.value?.balance ?? "",
      )
    ];
    checkStatusArr.value = [true];
    await _appGameRepository.realGames().then((data) {
      realGamesList.value = data ?? [];
      updateDropdownSources(realGameTypes[selectedTabIndex.value].key);
    });
  }

  void updateDropdownSources(String key) {
    transferOption = ['我的钱包', '我的钱包'].obs;
    checkStatusArr.value = [true];
    dataArr.value = [
      WalletData(
        title: '我的钱包',
        id: '0',
        pic: img_root('images/realtrans/real0'),
        balance: GlobalService.to.userInfo.value?.balance ?? '',
      )
    ];
    for (var item in realGamesList) {
      if (item.category == key) {
        WalletData newData = WalletData(
          title: item.title!,
          id: item.id!,
          pic: item.pic!,
          balance: '*****',
        );
        dataArr.add(newData);
        checkStatusArr.add(false);
      }
    }
  }

  void updateUserInfo() {
    GlobalService.to.getUserInfo();
  }

  void updateWalletDataByIndex(
      int index, String title, String id, String pic, String balance) {
    dataArr[index] = WalletData(
      title: title,
      id: id,
      pic: pic,
      balance: balance,
    );
  }

  void updateWalletBalanceByIndex(int index, String balance) {
    dataArr[index] = WalletData(
      title: dataArr[index].title,
      id: dataArr[index].id,
      pic: dataArr[index].pic,
      balance: balance,
    );
  }

  void updateMyPurse(bool isOut) {
    double curBalance =
        double.parse(GlobalService.to.userInfo.value?.balance ?? '0');
    double updateBalance =
        isOut == false ? curBalance - money.value : curBalance + money.value;
    GlobalService.to.userInfo.value?.balance = updateBalance.toString();

    dataArr[0] = WalletData(
      title: dataArr[0].title,
      id: dataArr[0].id,
      pic: dataArr[0].pic,
      balance: updateBalance.toString(),
    );
  }

  Future<String> getRealBalance(index) async {
    String? token = AppDefine.userToken!.apiSid;
    int id = int.parse(dataArr[index].id);

    await _appRealRepository.checkBalance(id, token).then((data) {
      checkStatusArr[index] = true;
      updateWalletBalanceByIndex(index, data?.balance ?? '');

      return data?.balance;
    });
    return "*****";
  }

  Future<bool> apiRealManualTransfer(
      String transOut, String transIn, double money) async {
    bool bIsSuccess = false;
    String? token = AppDefine.userToken!.apiSid;
    int transOutId =
        int.parse(dataArr.firstWhere((item) => item.title == transOut).id);
    int transInId =
        int.parse(dataArr.firstWhere((item) => item.title == transIn).id);
    await _appRealRepository
        .apiRealManualTransfer(
            token!, money, transOutId.toString(), transInId.toString())
        .then((code) async {
      if (code == 0) {
        bIsSuccess = true;
      }
    });
    return bIsSuccess;
  }

  Future<ResponseOneKeyTransferOutData?> oneKeyTransferOut() async {
    String? token = AppDefine.userToken?.apiSid;
    ResponseOneKeyTransferOutData? res;
    await _appRealRepository.oneKeyTransferOut(token).then((data) async {
      res = data;
    });
    return res;
  }

  Future<bool> quickTransferOut(String id) async {
    bool bIsSuccess = false;
    String? token = AppDefine.userToken!.apiSid;
    await _appRealRepository.quickTransferOut(token, id).then((code) async {
      bIsSuccess = (code == 0) ? true : false;
    });
    return bIsSuccess;
  }
}
