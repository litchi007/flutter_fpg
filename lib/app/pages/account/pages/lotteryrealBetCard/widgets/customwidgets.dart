import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';

// Get.toNamed(accountSubPath[index]);
TableRow unitRow(
  BuildContext context,
  List<String> data,
  int fontsize,
) {
  return TableRow(children: [
    ...List.generate(data.length, (index) {
      return unitcell(context, data.elementAt(index), fontsize);
    }),
  ]);
}

Widget unitcell(BuildContext context, String data, int fontsize) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 15.h),
    child: Text(
      data,
      textAlign: TextAlign.center,
      style: AppTextStyles.ff000000(context, fontSize: fontsize),
    ),
  );
}

class Iconbutton extends Container {
  Iconbutton(
      {super.key,
      required this.btn_name,
      required this.fontsize,
      this.onPressed});

  final String btn_name;
  final int fontsize;
  VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: AppColors.ffDDA815,
      child: GestureDetector(
        onTap: onPressed,
        child: Padding(
          padding: EdgeInsets.all(10.w),
          child: Text(
            btn_name,
            style: AppTextStyles.ffffffff(context, fontSize: fontsize),
          ),
        ),
      ),
    );
  }
}

class CustomModalDialog {
  final String title;
  final List<String> recordlist;

  final Color titleBackgroundColor;
  final BuildContext context;
  final List<String> commandPath;

  CustomModalDialog({
    required this.context,
    required this.title,
    required this.titleBackgroundColor,
    required this.recordlist,
    required this.commandPath,
  });
  // late MissionController controller = Get.find();
  Text _createTitle() {
    return Text(
      title,
      style: AppTextStyles.bodyText1(context, fontSize: 22),
      textAlign: TextAlign.center,
    );
  }

  Column createContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
            padding: EdgeInsets.only(top: 1.w, bottom: 1.w),
            child: Column(
              children: [
                ...List.generate(recordlist.length, (index) {
                  return ColoredBox(
                    color: AppColors.background,
                    child: SizedBox(
                        child: Column(
                      children: [
                        GestureDetector(
                          // onTapDown: (done) => Get.back(),
                          onTap: () {
                            Get.toNamed(commandPath.elementAt(index));
                            Get.back();
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            recordlist.elementAt(
                              index,
                            ),
                            style:
                                AppTextStyles.ffcf352e(context, fontSize: 18),
                          ),
                        ),
                        SizedBox(
                          height: 10.w,
                        ),
                        ColoredBox(
                          color: AppColors.ff9e9e9e,
                          child: SizedBox(
                            width: 1.sw,
                            height: 1.sp,
                          ),
                        ),
                      ],
                    )),
                  );
                }),
              ],
            )),
      ],
    );
  }

  Dialog createDialog() {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(2))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // Custom title with specified background color
          Container(
            width: 1.sw,
            height: 50.h,
            decoration: BoxDecoration(
                color: titleBackgroundColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(1), topRight: Radius.circular(1))),
            child: SizedBox(
              width: 1.sw,
              child: Padding(
                padding: EdgeInsets.only(top: 8.h),
                child: Column(
                  children: [
                    _createTitle(),
                    SizedBox(
                      width: 1.sw,
                      height: 1.sp,
                      child: const ColoredBox(
                        color: AppColors.ff9e9e9e,
                      ), // New color property
                    )
                  ],
                ),
              ),
            ),
          ),

          SizedBox(
            height: 20.w,
          ),
          // Content area
          SizedBox(
            width: 1.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: createContent(),
            ),
          ),
          // Actions
        ],
      ),
    );
  }

  void dialogBuilder() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (BuildContext context) {
          return Container(height: 900.w, width: 1.sw, child: createDialog());
        });
  }
}

//  showModalBottomSheet(
//           context: context,
//           builder:
//         );
