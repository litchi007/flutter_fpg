import 'package:flutter/material.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotteryrealBetCard/realbetfish_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotteryrealBetCard/widgets/customwidgets.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';

class RealBetFishView extends StatefulWidget {
  const RealBetFishView({super.key});

  @override
  State<RealBetFishView> createState() => _RealBetFish();
}

class _RealBetFish extends State<RealBetFishView> {
  final RealBetFishViewController controller =
      Get.put(RealBetFishViewController());
  late DateRangePickerController datecontroller;
  Locale currentLocale = Locale('zh');
  @override
  void initState() {
    // _controller.selectedDate=DateTime
    datecontroller = DateRangePickerController();
    controller.onReady();
    super.initState();
  }

  late CustomModalDialog modal;
  List<String> header = ['游戏', 'ID', '单号', '下注金额', '输赢', '详情'];
  List<String> dropdowndata = [
    '真人注单',
    '棋牌注单',
    '捕鱼注单',
    '电子注单',
    '电竞注单',
    '体育注单',
  ];
  String dropdownvalue = '真人注单';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title: DropdownButton(
          value: dropdownvalue,
          underline: Container(
            height: 0,
            color: Colors.transparent,
          ),
          icon: const Icon(Icons.keyboard_arrow_down),
          items: dropdowndata.map((String items) {
            return DropdownMenuItem(
              value: items,
              child: Text(items),
            );
          }).toList(),
          onChanged: (String? newValue) {
            setState(() {
              dropdownvalue = newValue!;

              controller.loadWBData(controller.dropdownList[newValue]!,
                  controller.startDate.value, controller.startDate.value);
            });
          },
        ),
        centerTitle: true,
      ),
      // actions:
      bottomNavigationBar: Container(
        height: 50.w,
        decoration: BoxDecoration(
            color: AppColors.ffbfa46d,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.w),
              topRight: Radius.circular(15.w),
            )),
        child: Padding(
          padding: EdgeInsets.all(5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Obx(
                () => Text(
                  '下注金额:' + controller.totalbetAmount.value,
                  style: AppTextStyles.ffffffff(
                    context,
                    fontSize: 20,
                  ),
                ),
              ),
              Obx(
                () => Text(
                  '有效下注:' + controller.totalwinAmount.value,
                  style: AppTextStyles.ffffffff(
                    context,
                    fontSize: 20,
                  ),
                ),
              ),
              Obx(
                () => Text(
                  '输赢金额:' + controller.totalwinAmount.value,
                  style: AppTextStyles.ffffffff(
                    context,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Container(
        color: AppColors.surface,
        child: Obx(
          () => Column(
            children: [
              Table(
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                border: TableBorder.all(color: AppColors.ff9e9e9e),
                // Allows to add a border decoration around your table
                children: [
                  TableRow(
                    children: [
                      ...List.generate(
                        header.length,
                        (index) {
                          return unitcell(context, header.elementAt(index), 18);
                        },
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                height: 60.w,
                color: AppColors.ffe3decf,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '日期',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                    CustomInputDate(
                      date: controller.startDate.value,
                      color: controller.sIconColor.value ?? Colors.black,
                      onPressed: () {
                        controller.flagShow.value = true;
                        controller.sFlag.value = true;
                        controller.eFlag.value = false;
                        controller.sIconColor.value = AppColors.ffcf352e;
                        controller.eIconColor.value = AppColors.ff000000;
                      },
                    ),
                    Text(
                      '至',
                      style: AppTextStyles.ff000000(context, fontSize: 20),
                    ),
                    CustomInputDate(
                      date: controller.endDate.value,
                      color: controller.eIconColor.value ?? Colors.black,
                      onPressed: () {
                        controller.flagShow.value = true;
                        controller.eFlag.value = true;
                        controller.sFlag.value = false;
                        controller.sIconColor.value = AppColors.ff000000;
                        controller.eIconColor.value = AppColors.ffcf352e;
                      },
                    ),
                    GestureDetector(
                      onTap: () {
                        controller.startDate.value = '----------';
                        controller.endDate.value = '----------';
                      },
                      child: SizedBox(
                        width: 35.w,
                        child: Icon(
                          Icons.close_outlined,
                          // IconData(0xe801),
                          size: 30.w,
                          color: AppColors.ff000000,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              controller.flagShow.value
                  ? Container(
                      height: 300.w,
                      child: CustomDatePicker(),
                    )
                  : const SizedBox(
                      height: 1,
                    ),
              controller.lotteryData!.length != 0
                  ? Flexible(
                      child: SingleChildScrollView(
                        child: Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          border: TableBorder.all(color: AppColors.ff9e9e9e),
                          // Allows to add a border decoration around your table
                          children: [
                            ...List.generate(
                              controller.lotteryData!.length,
                              (index) {
                                UGBetsRecordLongModel temp = controller
                                    .lotteryData!.value
                                    .elementAt(index);
                                return unitRow(
                                    context,
                                    [
                                      temp.gameName! + "\n" + temp.gameType!,
                                      temp.id!,
                                      temp.gameId!,
                                      temp.betTime!,
                                      temp.betAmount!,
                                      temp.winAmount!,
                                    ],
                                    18);
                              },
                            ),
                          ],
                        ),
                      ),
                    )
                  : const Text('暂无数据'),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomInputDate extends Container {
  CustomInputDate({super.key, required this.date, this.onPressed, this.color});
  final String? date;
  Color? color = AppColors.ff000000;
  VoidCallback? onPressed;
  double width = 200.w;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 40.w,
      decoration: BoxDecoration(
        color: AppColors.ffe3decf,
        border: Border.all(
          color: AppColors.ff9e9e9e,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 10.w,
          ),
          SizedBox(
            width: 100.w,
            child: Text(
              date ?? '',
              textAlign: TextAlign.center,
              style: AppTextStyles.ff000000(context, fontSize: 16),
            ),
          ),
          GestureDetector(
            onTap: onPressed,
            child: SizedBox(
              width: width / 3,
              child: Icon(
                Icons.calendar_month,
                size: 25.w,
                color: color,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomDatePicker extends Container {
  final RealBetFishViewController controller = Get.find();
  final DateRangePickerController datecontroller = DateRangePickerController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600.w,
      child: SfDateRangePicker(
        view: DateRangePickerView.month,
        controller: datecontroller,
        headerHeight: 20,
        backgroundColor: AppColors.surface,
        todayHighlightColor: AppColors.ffDDA815,
        selectionColor: AppColors.fffcc67e,
        monthViewSettings:
            const DateRangePickerMonthViewSettings(viewHeaderHeight: 20),
        selectionShape: DateRangePickerSelectionShape.circle,
        monthCellStyle: DateRangePickerMonthCellStyle(
          textStyle: const TextStyle(
              fontWeight: FontWeight.w400, fontSize: 15, color: Colors.black),
          todayTextStyle: const TextStyle(
              fontStyle: FontStyle.italic,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: AppColors.ff000000),
          leadingDatesDecoration: BoxDecoration(
              color: const Color(0xFFDFDFDF),
              border: Border.all(color: const Color(0xFFB6B6B6), width: 1),
              shape: BoxShape.circle),
          disabledDatesDecoration: BoxDecoration(
              color: const Color(0xFFDFDFDF).withOpacity(0.2),
              border: Border.all(color: const Color(0xFFB6B6B6), width: 1),
              shape: BoxShape.circle),
        ),
        toggleDaySelection: true,
        onSelectionChanged: (args) {
          DateTime now = DateTime.now();
          DateFormat formatter = DateFormat('yyyy-MM-dd');
          if (controller.sFlag.value) {
            controller.startDate.value =
                formatter.format(datecontroller.selectedDate ?? now);
          }
          if (controller.eFlag.value) {
            controller.endDate.value =
                formatter.format(datecontroller.selectedDate ?? now);
          }
          // datecontroller.selectedDate(now);
          controller.flagShow.value = false;
        },
      ),
    );
  }
}
