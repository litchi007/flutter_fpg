import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:fpg_flutter/data/repositories/api_ticket.dart';
import 'package:fpg_flutter/configs/app_define.dart';

//OtherRecord.tsx
class RealBetFishViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxInt selectedTabIndex = 0.obs;
  RxBool isSetData = false.obs;
  final Map<String, String> dropdownList = {
    '真人注单': 'real',
    '棋牌注单': 'card',
    '捕鱼注单': 'fish',
    '电子注单': 'game',
    '电竞注单': 'esport',
    '体育注单': 'sport',
  };
  // ApiTask
  final ApiTicket _apiTicket = ApiTicket();
  RxList<UGBetsRecordLongModel>? lotteryData = RxList();
  RxString totalbetCount = ''.obs;
  RxString totalwinAmount = ''.obs;
  RxString totalbetAmount = ''.obs;
  RxString startDate = '2024-03-05'.obs;
  RxString endDate = '2024-08-07'.obs;
  RxBool flagShow = false.obs;
  RxBool sFlag = false.obs; //start Date calender
  RxBool eFlag = false.obs;
  RxString date = ''.obs;
  Rx<Color> sIconColor = AppColors.ff000000.obs;
  Rx<Color> eIconColor = AppColors.ff000000.obs;
  RxList<UGBetsRecordLongModel>? historyData = RxList();
  RxList<IMiddleMenuItem> tabData = RxList();

  @override
  void onReady() async {
    initController();
    tabDataInit();
    super.onReady();
  }

  void initController() async {
    isIniting(true);
    initData();
    loadWBData('real', startDate.value, endDate.value);

    isIniting(false);
  }

  void loadWBData(String category, String startDate, String endDate) async {
    await _apiTicket.appHttpClient
        .history(
            token: AppDefine.userToken!.apiSid,
            category:
                category, //游戏分类：lottery=彩票，real=真人，card=棋牌，game=电子游戏，sport=体育, blockchain=区块链
            page: 1,
            rows: 999,
            startDate: startDate,
            endDate: endDate,
            timeZone: '8',
            status: 0)
        .then((data) {
      lotteryData?.clear();
      lotteryData?.value = data.data?.data?.list ?? [];
      totalwinAmount.value = data.data?.data?.totalWinAmount ?? '';
      totalbetAmount.value = data.data?.data?.totalBetAmount ?? '';
    });
  }

  void initData() {}
  void tabDataInit() {
    tabData.value = [
      IMiddleMenuItem(
        title: '真人注单',
        subTitle: null,
        icon: null,
        id: '23',
        type: 'real',
      ),
      IMiddleMenuItem(
        title: '棋牌注单',
        subTitle: null,
        icon: null,
        id: '24',
        type: 'card',
      ),
      IMiddleMenuItem(
        title: '捕鱼注单',
        subTitle: null,
        icon: null,
        id: '25',
        type: 'fish',
      ),
      IMiddleMenuItem(
        title: '电子注单',
        subTitle: null,
        icon: null,
        id: '22',
        type: 'game',
      ),
      IMiddleMenuItem(
        title: '电竞注单',
        subTitle: null,
        icon: null,
        id: '26',
        type: 'esport',
      ),
      IMiddleMenuItem(
        title: '体育注单',
        subTitle: null,
        icon: null,
        id: '27',
        type: 'sport',
      ),
      // Uncomment and adjust as needed
      // IMiddleMenuItem(
      //   title: '区块链注单',
      //   subTitle: null,
      //   icon: null,
      //   id: '9999',
      //   type: 'blockchain',
      // ),
    ];
  }
}

// navList.value = data?.navs ?? [];
class IMiddleMenuItem {
  final String title;
  final String? subTitle;
  final String? icon;
  final String id;
  final String type;

  IMiddleMenuItem({
    required this.title,
    this.subTitle,
    this.icon,
    required this.id,
    required this.type,
  });
}
