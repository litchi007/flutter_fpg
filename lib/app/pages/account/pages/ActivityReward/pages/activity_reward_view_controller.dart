import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/ApplyWinLog.dart';
import 'package:fpg_flutter/data/repositories/app_activity_repository.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:fpg_flutter/data/models/WinApplyList.dart';
import 'package:fpg_flutter/data/models/ApplyWinLogDetail.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_alert_dialog.dart';

import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker_view/multi_image_picker_view.dart';

class ActivityRewardViewController extends GetxController {
  RxBool isIniting = false.obs;
  final AppActivityRepository _activityRepository = AppActivityRepository();
  RxList<ListDataForApplyList> winApplyList = RxList();
  RxList<ListDataForWinLog> applyWinLog = RxList();
  Rx<Color> backgroundColor = AppColors.ff900909.obs;
  RxList<UGBetsRecordLongModel> newState = RxList();
  RxString? dropdownValue;
  RxBool showWinAmount = false.obs;
  RxString isApplyReason = '0'.obs;
  bool isHide = false;
  RxString amount = '0'.obs;
  RxString userComment = ''.obs;
  RxList<Map<String, String>> quickAmountArray = RxList();
  String mid = '0';
  String? messageType;
  String? type;
  String? content;
  List<String> files = [];
  final title = '查看详情';

  List<String> categories = [
    '活动名称',
    '申请日期',
    '申请金额',
    '申请原因',
    '审核结果',
    '审核说明',
  ];

  UGUserModel userInfo = UGUserModel();

  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  bool parseNumber(String input) {
    try {
      int? intValue = int.tryParse(input);
      if (intValue != null) {
        return true;
      } else {
        double? doubleValue = double.tryParse(input);
        if (doubleValue != null) {
          // depositAmount.value=
          return true;
        } else {
          amount.value = '0';
          return false;
        }
      }
    } catch (e) {
      // coinTxt.value = '0';
      amount.value = '0';
      return false;
    }
  }

  void fetchData() async {
    var item;
    _activityRepository.fetchWinApplyList().then((data) {
      if (data == null) return;
      winApplyList.value = data.list!;

      for (item in winApplyList) {
        if (item.param?.isApplyReason == '1') break;
      }
    });

    _activityRepository.fetchApplyWinLog().then((data) {
      if (data == null) return;
      applyWinLog.value = data.list!;
    });
  }

  void fetchApplyWinLogDetail(
    String? id,
    CustomAlertDialog? customAlertDialog,
  ) {
    _activityRepository.fetchApplyWinLogDetail(id).then((data) {
      if (data == null) return;
      // applyWinLog.value = data.list!;
      ApplyWinLogDetail row = data;
      customAlertDialog?.dialogBuilder(_getDataForDialog(row));
    });
  }

  bool isEmptyOrNull(String? str) {
    return str == null || str.isEmpty;
  }

  List<dynamic> _getDataForDialog(ApplyWinLogDetail row) {
    return [
      row.winName,
      row.updateTime,
      row.amount,
      row.userComment,
      row.state,
      row.adminComment,
    ];
  }

  void submit() async {
    _activityRepository
        .applyWin(
            id: mid,
            userComment: userComment.value,
            amount: amount.value,
            imgCode: '',
            files: files)
        .then((data) {
      files.clear();
      content = '';
      Get.back();
    }).catchError((data) {});
  }

  static Future<List<ImageFile>> pickImagesUsingImagePicker(
      bool allowMultiple) async {
    final picker = ImagePicker();
    final List<XFile> xFiles;
    if (allowMultiple) {
      xFiles = await picker.pickMultiImage(maxWidth: 1080, maxHeight: 1080);
    } else {
      xFiles = [];
      final xFile = await picker.pickImage(
          source: ImageSource.gallery, maxHeight: 1080, maxWidth: 1080);
      if (xFile != null) {
        xFiles.add(xFile);
      }
    }
    if (xFiles.isNotEmpty) {
      return xFiles.map<ImageFile>((e) => convertXFileToImageFile(e)).toList();
    }
    return [];
  }
}
