import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/activityReward/const/RewardConst.dart';
import 'package:fpg_flutter/app/pages/account/pages/activityReward/pages/activity_reward_view_controller.dart';
import 'package:fpg_flutter/data/models/WinApplyList.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:flutter/services.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:multi_image_picker_view/multi_image_picker_view.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_alert_dialog.dart';
import 'package:fpg_flutter/data/models/ApplyWinLog.dart';

class ActivityRewardView extends StatefulWidget {
  const ActivityRewardView({super.key});

  @override
  _ActivityRewardViewState createState() => _ActivityRewardViewState();
}

class _ActivityRewardViewState extends State<ActivityRewardView>
    with TickerProviderStateMixin {
  final ActivityRewardViewController controller =
      Get.put(ActivityRewardViewController());
  late TabController _tabController;
  final List<TextEditingController> depositsController =
      List.generate(10, (_) => TextEditingController());
  final TextEditingController inputMoneyController = TextEditingController();
  final TextEditingController inputUsercommentController =
      TextEditingController();
  final pickController = MultiImagePickerController(
      maxImages: 1,
      picker: (allowMultiple) async {
        return await ActivityRewardViewController.pickImagesUsingImagePicker(
            allowMultiple);
      });
  double itemWidth = 100;
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(_handleTabChange);
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      if (_tabController.index == 1) {
        controller.backgroundColor.value = AppColors.surface;
      } else {
        controller.backgroundColor.value = AppColors.ff900909;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    itemWidth = (MediaQuery.of(context).size.width - 60) /
        3; // Adjust item width as needed
    CustomAlertDialog customAlertDialog = CustomAlertDialog(
      context: context,
      title: controller.title,
      borderRadius: 0,
      categories: controller.categories,
      titleBackgroundColor: AppColors.error,
      action: Container(
        width: 0.775.sw,
        height: 50.h,
        decoration: const BoxDecoration(
          color: AppColors.error,
        ),
        child: SizedBox(
          width: .8.sw,
          child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(
                '关闭',
                style: AppTextStyles.bodyText1(context, fontSize: 20),
                textAlign: TextAlign.center,
              ).onTap(() {
                Get.back();
              })),
        ),
      ),
    );
    return Obx(() => Scaffold(
          backgroundColor: controller.backgroundColor.value,
          appBar: AppBar(
            backgroundColor: AppColors.ffbfa46d,
            automaticallyImplyLeading: false,
            titleSpacing: 0,
            leading: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15).w,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Row(children: [
                    Icon(Icons.arrow_back_ios,
                        color: AppColors.surface, size: 28.w),
                  ]),
                )),
            title: Text('申请彩金',
                style: AppTextStyles.surface_(context,
                    fontWeight: FontWeight.bold, fontSize: 21)),
            centerTitle: true,
            leadingWidth: 0.3.sw,
            bottom: PreferredSize(
                preferredSize: Size.fromHeight(46.h),
                child: Container(
                  color: AppColors.surface,
                  child: Column(
                    children: [
                      Center(
                        child: SizedBox(
                          width: 300.w,
                          height: 40.h,
                          child: PreferredSize(
                              preferredSize: Size(115.w, kToolbarHeight),
                              child: TabBar(
                                controller: _tabController,
                                indicatorColor: AppColors.ffff0000,
                                labelColor: AppColors.ffff0000,
                                indicatorSize: TabBarIndicatorSize.tab,
                                dividerHeight: 2.h,
                                dividerColor: AppColors.ffeaeaea,
                                tabAlignment: TabAlignment.fill,
                                labelStyle: AppTextStyles.surface_(context,
                                    fontSize: 14),
                                tabs: List.generate(tabs.length, (index) {
                                  return Tab(
                                    text: '${tabs[index]['name']}',
                                  );
                                }),
                              )),
                        ),
                      ),
                      SizedBox(
                        width: 1.sw,
                        height: 2.h,
                        child: const ColoredBox(color: AppColors.ffeaeaea),
                      ),
                      SizedBox(
                        width: 1.sw,
                        height: 15.h,
                        child: const ColoredBox(color: AppColors.surface),
                      ),
                    ],
                  ),
                )),
          ),
          body: TabBarView(
            controller: _tabController,
            children: [
              _applyReward(context),
              _applyFeedBack(context, customAlertDialog)
            ],
          ),
          // )
        ));
  }

  Widget _applyReward(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0).h,
      child: (controller.winApplyList.isEmpty)
          ? const CustomLoadingWidget()
          : Obx(() => ListView(
                  children:
                      List.generate(controller.winApplyList.length, (index) {
                return Center(
                  child: Column(
                    children: [
                      Container(
                          width: 505.w,
                          height: 232.h,
                          margin: EdgeInsets.only(bottom: 12).h,
                          padding: EdgeInsets.fromLTRB(12, 12, 12, 0).w,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [AppColors.ffde0b0b, AppColors.ff8e010d],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.3),
                                blurRadius: 3.0,
                                spreadRadius: 3.0,
                                offset: Offset(0.0, 2.0),
                              ),
                            ],
                          ),
                          child: GestureDetector(
                              onTap: () => _showApplyDialog(
                                  context,
                                  controller.winApplyList[index].param
                                      ?.winApplyContent,
                                  index
                                  // controller.winApplyList[index].param,
                                  // controller.winApplyList[index].id,
                                  ),
                              child: ApplyBonusWidgets(
                                  item: controller.winApplyList
                                      .elementAt(index))))
                    ],
                  ),
                );
              }))),
    );
  }

  Widget _applyFeedBack(
      BuildContext context, CustomAlertDialog customAlertDialog) {
    return Obx(() => controller.applyWinLog.isNotEmpty
        ? Table(
            border: TableBorder.all(
                width: 1.w,
                color: AppColors.ffC5CBC6,
                style: BorderStyle.solid),
            children: [
                  TableRow(children: [
                    _tableCell(context: context, title: '申请活动'),
                    _tableCell(context: context, title: '申请时间'),
                    _tableCell(context: context, title: '申请金额'),
                    _tableCell(context: context, title: '状态'),
                  ])
                ] +
                controller.applyWinLog.map((row) {
                  return TableRow(children: [
                    _tableCell(
                      context: context,
                      customAlertDialog: customAlertDialog,
                      row: row,
                      title: row.winName ?? '',
                    ),
                    _tableCell(
                        context: context,
                        customAlertDialog: customAlertDialog,
                        row: row,
                        title: row.updateTime ?? ''),
                    _tableCell(
                        context: context,
                        customAlertDialog: customAlertDialog,
                        row: row,
                        title: row.amount ?? ''),
                    _tableCell(
                        context: context,
                        customAlertDialog: customAlertDialog,
                        row: row,
                        title: row.state ?? ''),
                  ]);
                }).toList())
        : Column(
            children: [
              Table(
                  border: TableBorder.all(
                      width: 1.w,
                      color: AppColors.ffC5CBC6,
                      style: BorderStyle.solid),
                  children: [
                    TableRow(children: [
                      _tableCell(context: context, title: '申请活动'),
                      _tableCell(context: context, title: '申请时间'),
                      _tableCell(context: context, title: '申请金额'),
                      _tableCell(context: context, title: '状态'),
                    ]),
                  ]),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 30).h,
                  child: Text('暂无数据', style: AppTextStyles.bodyText2(context)),
                ),
              )
            ],
          ));
  }

  void _pickImage() {
    AppToast.showDuration(msg: '操作成功', duration: 1);
    pickController.pickImages();
  }

  Widget _addPickWidget(BuildContext context, double itemWidth) {
    return SizedBox(
        child: GestureDetector(
      onTap: _pickImage,
      child: Container(
          height: itemWidth,
          width: itemWidth,
          decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(
                  width: 1.w,
                  color: AppColors.ffC5CBC6,
                  style: BorderStyle.solid)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('+',
                    style: AppTextStyles.bodyText1(context, fontSize: 40)),
                Text('添加图片',
                    style: AppTextStyles.bodyText1(context, fontSize: 14))
              ])),
    ));
  }

  void submit() {
    if (controller.showWinAmount.value && !controller.isHide) {
      AppToast.showDuration(msg: '申请金额不能为空');
      return;
    }
    if (!AppConfig.isHideSQSM()) {
      if (controller.userComment.value != '') {
        AppToast.showDuration(msg: '申请说明不能为空');
        return;
      }
    }

    try {
      controller.files.clear();
      for (var i in pickController.images) {
        controller.files.add(i.path as String);
      }
      controller.submit();
    } catch (e) {
      AppLogger.e(e);
    }
  }

  Widget _tableCell({
    BuildContext? context,
    CustomAlertDialog? customAlertDialog,
    ListDataForWinLog? row,
    String? title,
  }) {
    return TableCell(
      child: SizedBox(
        height: 80.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title ?? '',
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText3(context!, fontSize: 18)),
          ],
        ),
      ).onTap(
        () {
          controller.fetchApplyWinLogDetail(row?.id, customAlertDialog);
        },
      ),
    );
  }

  void _showApplyDialog(
      BuildContext context, String? winApplyContent, int index) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return _getContent(winApplyContent ?? '', index);
        });
  }

  Widget _getContent(String? content, int index) {
    ListDataForApplyList ps = controller.winApplyList[index];
    final Map<String, dynamic> param =
        ps.param?.toJson() as Map<String, dynamic>;
    controller.mid = ps.id ?? '0';

    final List<Map<String, String>> newArr = [];
    for (final key in param.keys) {
      if (key.startsWith('quickAmount')) {
        final String str = param[key] ?? '';
        if (double.tryParse(str) != null && double.parse(str) > 0) {
          newArr.add({key.substring(11): str});
        }
      }
    }

    final List<Map<String, String>> keyArr = newArr
      ..sort((a, b) => a.keys.first.compareTo(b.keys.first));

    controller.quickAmountArray.value = keyArr;
    controller.showWinAmount.value = param['receiveNum1'] != '1';
    controller.isHide = param['receiveNum1'] == '1';
    controller.isApplyReason.value = param['isApplyReason'] ?? '0';
    return Center(
        child: Container(
      width: 0.9.sw,
      height: 0.9.sh,
      padding: EdgeInsets.all(10.w),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 0.9.sw,
            height: 80.h,
            decoration: const BoxDecoration(
                color: AppColors.surface,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: SizedBox(
              width: .9.sw,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text('申请彩金',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 28.sp)),
                ),
              ),
            ),
          ),
          Expanded(
              child: SingleChildScrollView(
                  child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('活动说明：',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 24.sp))
                  .paddingOnly(bottom: 12.h),
              HtmlWidget(
                content ?? '',
              ),
              Obx(() => (controller.showWinAmount.value == true &&
                      controller.quickAmountArray.isNotEmpty)
                  ? Text('快捷金额',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20.sp))
                      .paddingOnly(bottom: 12.h)
                  : const SizedBox()),
              Obx(
                () => (controller.showWinAmount.value == true &&
                        controller.quickAmountArray.isNotEmpty)
                    ? GridView.builder(
                        physics: const BouncingScrollPhysics(),
                        shrinkWrap: true,
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 20.h),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 8,
                          crossAxisSpacing: 10.w,
                          mainAxisSpacing: 10.w,
                          childAspectRatio: 1,
                        ),
                        itemCount: controller.quickAmountArray.length,
                        itemBuilder: (context, index) {
                          final item = controller.quickAmountArray[index];

                          return GestureDetector(
                            onTap: () => {},
                            child: SizedBox(
                                height: 50.h,
                                width: 50.w,
                                child: _amountItem(context, item.values.first)),
                          );
                        },
                      )
                    : const SizedBox(),
              ),
              ...showWinAmountWidget(),
              Obx(() => (controller.isApplyReason.value == '1')
                  ? _inputUserComment(
                      context, '申请原因', inputUsercommentController)
                  : const SizedBox()),
              Obx(() => (controller.isApplyReason.value == '1')
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0).w,
                      child: SizedBox(
                          height: 200.h,
                          width: 1.sw,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 32),
                              Expanded(
                                child: MultiImagePickerView(
                                  controller: pickController,
                                  gridDelegate:
                                      SliverGridDelegateWithMaxCrossAxisExtent(
                                    maxCrossAxisExtent: itemWidth,
                                    crossAxisSpacing: 10,
                                    mainAxisSpacing: 10,
                                    childAspectRatio: 1,
                                  ),
                                  initialWidget:
                                      _addPickWidget(context, itemWidth),
                                  addMoreButton:
                                      _addPickWidget(context, itemWidth),
                                  padding: const EdgeInsets.all(10),
                                ),
                              ),
                            ],
                          )),
                    )
                  : const SizedBox())
            ],
          ))),
          _action(context)
        ],
      ),
    ));
  }

  List<Widget> showWinAmountWidget() {
    List<Widget> temp = [];
    if (controller.showWinAmount.value) {
      temp.add(_inputMoneyWidget(context, '金额', inputMoneyController));
    }
    return temp;
  }

  Widget _amountItem(BuildContext context, String title) {
    return Container(
            padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
            width: 70.w,
            height: 50.h,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: AppColors.secondary,
                border: Border.all(width: 1.w, color: AppColors.ffC5CBC6),
                borderRadius: BorderRadius.all(Radius.circular(5.w))),
            child: Text(title))
        .onTap(() => inputMoneyController.text = title);
  }

  Widget _inputMoneyWidget(
      BuildContext context, String hint, TextEditingController editController) {
    return Container(
        padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
        width: 0.9.sw,
        height: 54.h,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
        child: Focus(
            child: TextFormField(
          onChanged: (text) {
            controller.amount.value = text;
          },
          inputFormatters: [
            FilteringTextInputFormatter.allow(RegExp(r'^-?\d+(\.\d+)?$')),
          ],
          keyboardType: TextInputType.text,
          obscureText: false,
          controller: editController,
          cursorColor: AppColors.onBackground,
          style: AppTextStyles.ff535B70_(context, fontSize: 18),
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(horizontal: 20).w,
            iconColor: AppColors.surface,
            filled: true,
            fillColor: Colors.transparent,
            hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
            hintText: hint,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.onBackground,
                width: 1.0.w,
              ),
            ),
          ),
        )));
  }

  Widget _inputUserComment(
      BuildContext context, String hint, TextEditingController editController) {
    return Container(
        padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
        width: 0.9.sw,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
        child: Focus(
            child: TextFormField(
          onChanged: (text) {
            controller.userComment.value = text;
          },
          keyboardType: TextInputType.multiline, // Allow multiline input
          maxLines: null, // No limit on the number of lines

          obscureText: false,
          controller: editController,
          cursorColor: AppColors.onBackground,
          style: AppTextStyles.ff535B70_(context, fontSize: 18),
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 20).w,
            iconColor: AppColors.surface,
            filled: true,
            fillColor: Colors.transparent,
            hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
            hintText: hint,
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppColors.onBackground,
                width: 1.0.w,
              ),
            ),
          ),
        )));
  }

  Widget _action(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        GestureDetector(
            onTapDown: (done) => Get.back(),
            child: Container(
              width: 0.42.sw,
              height: 45.h,
              decoration: const BoxDecoration(
                  color: AppColors.ffeaeaea,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "取消",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.bodyText2(context, fontSize: 15),
                  ),
                ],
              ),
            )),
        GestureDetector(
            onTapDown: (done) {
              submit();

              // if (action == 'logout') _authController.logout();
            },
            child: Container(
              width: 0.42.sw,
              height: 45.h,
              decoration: const BoxDecoration(
                  color: AppColors.ffbfa46d,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "确定",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.surface_(context, fontSize: 15),
                  ),
                ],
              ),
            ))
      ],
    );
  }
}

class ApplyBonusWidgets extends StatelessWidget {
  ApplyBonusWidgets({super.key, required this.item});
  final ListDataForApplyList item;
  @override
  Widget build(BuildContext context) {
    Param? param = item.param;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
            height: 168.h,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: param?.winApplyImage != ''
                    ? NetworkImage(param!.winApplyImage!)
                    : const NetworkImage(
                        'https://wwwstatic01.fdgdggduydaa008aadsdf008.xyz/images/winapply_default.jpg'),
                fit: BoxFit.fitWidth,
              ),
            ),
            child: Center(
              child: param?.winApplyImage == ''
                  ? Text(item.name!,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ))
                  : const Text(''),
            )),
        Padding(
          padding: const EdgeInsets.only(bottom: 10.0).h,
          child: GestureDetector(
            child: DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(4).w),
                color: AppColors.ffda2506,
              ),
              child: SizedBox(
                width: 100.w,
                height: 30.h,
                child: Text(
                  '点击申请',
                  textAlign: TextAlign.center,
                  style: AppTextStyles.surface_(context, fontSize: 17),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
