import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/feedback/pages/ksfeedbackrecord_view_controller.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class KsfeedbackrecordView extends StatefulWidget {
  const KsfeedbackrecordView({super.key});

  @override
  _KsfeedbackrecordViewState createState() => _KsfeedbackrecordViewState();
}

class _KsfeedbackrecordViewState extends State<KsfeedbackrecordView> {
  final KsfeedbackrecordViewController controller =
      Get.put(KsfeedbackrecordViewController());

  @override
  void initState() {
    super.initState();
    controller.dateSelect?.addAll(controller.getDaysArray(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 44.h,
          title: Row(children: [
            SizedBox(
                child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15).w,
                    child: GestureDetector(
                      onTap: () {
                        Get.back();
                      },
                      child: Icon(Icons.arrow_back_ios,
                          color: AppColors.surface, size: 18.w),
                    ))),
            const Spacer(),
            Text('反馈纪录', style: AppTextStyles.surface_(context, fontSize: 24)),
            const Spacer(),
            SizedBox(width: 0.1.sw),
          ]),
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Column(children: [
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0).h,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      height: 40.h,
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 1.w,
                              color: AppColors.ffC5CBC6,
                              style: BorderStyle.solid)),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          canvasColor: AppColors.surface,
                        ),
                        child: Obx(() => DropdownButton(
                              underline: Container(
                                height: 1.h,
                                color: Colors.transparent,
                              ),
                              value: controller.leftDropdownValue.value,
                              icon: const Icon(Icons.arrow_drop_down),
                              items:
                                  controller.dateSelect?.map((String curItem) {
                                return DropdownMenuItem(
                                    value: curItem,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10).w,
                                      child: Text(curItem,
                                          textAlign: TextAlign.center,
                                          style: AppTextStyles.bodyText3(
                                              context,
                                              fontSize: 18)),
                                    ));
                              }).toList(),
                              onChanged: (newValue) {
                                controller.leftDropdownValue.value =
                                    newValue.toString();
                                controller.fetchData();
                              },
                            )),
                      )),
                  Container(
                      height: 40.h,
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 1.w,
                              color: AppColors.ffC5CBC6,
                              style: BorderStyle.solid)),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          canvasColor: AppColors.surface,
                        ),
                        child: Obx(() => DropdownButton(
                              underline: Container(
                                height: 1.h,
                                color: Colors.transparent,
                              ),
                              value: controller.rightDropdownValue.value,
                              icon: const Icon(Icons.arrow_drop_down),
                              items:
                                  controller.liqTypeList.map((String curItem) {
                                return DropdownMenuItem(
                                  value: curItem,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10).w,
                                    child: Text(curItem.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppTextStyles.bodyText3(context,
                                            fontSize: 18)),
                                  ),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                controller.rightDropdownValue.value =
                                    newValue.toString();
                                controller.fetchData();
                              },
                            )),
                      )),
                ],
              )),
          Obx(() => Table(
                  border: TableBorder.all(color: AppColors.ffd1cfd0),
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  children: [
                    _createTableHeader(context),
                    ..._createTableContent(context)
                  ])),
          Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 30).h,
              child: Text('暂无点击描述可查看反馈详情数据',
                  style: AppTextStyles.bodyText2(context, fontSize: 20)),
            ),
          )
        ]))));
  }

  TableRow _createTableHeader(BuildContext context) {
    return TableRow(children: [
      _dataCell(context, '类型', fontSize: 22),
      _dataCell(context, '状态', fontSize: 22),
      _dataCell(context, '内容描述', fontSize: 22),
    ]);
  }

  List<TableRow> _createTableContent(BuildContext context) {
    return controller.tableData.map((row) {
      return TableRow(children: [
        row.type == '1'
            ? _dataCell(context, '我要投诉')
            : _dataCell(context, '提交建议'),
        row.status == '1'
            ? _dataCell(context, '已回复')
            : _dataCell(context, '待回复'),
        _dataCell(context, row.content ?? '')
      ]);
    }).toList();
  }

  TableCell _dataCell(BuildContext context, String content,
      {int fontSize = 17}) {
    return TableCell(
      child: Text(content,
          textAlign: TextAlign.center,
          style: AppTextStyles.bodyText3(context, fontSize: fontSize)),
    );
  }
}
