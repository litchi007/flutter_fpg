import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/completedtoday/pages/jddayclosedetail_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/feedback/const/FeedBack.dart';
import 'package:fpg_flutter/routes.dart';

class JdfeedbackView extends StatefulWidget {
  const JdfeedbackView({super.key});

  @override
  _JdfeedbackViewState createState() => _JdfeedbackViewState();
}

class _JdfeedbackViewState extends State<JdfeedbackView> {
  final JddayclosedetailViewController controller =
      Get.put(JddayclosedetailViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 65.h,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 18.w),
          ),
          title: Text('反馈纪录',
              style: AppTextStyles.surface_(context, fontSize: 24)),
          centerTitle: true,
        ),
        body: Column(
            children: List.generate(list.length, (index) {
          return GestureDetector(
            onTap: () {
              if (list[index].idKey == '1') {
                Get.toNamed(onlineServicePath);
              } else if (list[index].idKey == '2') {
                Get.toNamed(textFeedBackPath1);
              } else if (list[index].idKey == '3') {
                Get.toNamed(textFeedBackPath2);
              } else if (list[index].idKey == '4') {
                Get.toNamed(feedbackListPath);
              }
            },
            child: _feedbackItem(context, list[index]),
          );
        })));
  }

  Padding _feedbackItem(BuildContext context, dynamic item) {
    return Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 25).h,
              child: AppImage.network(item.imgUrl,
                  height: 40.h, width: 40.w, fit: BoxFit.contain),
            ),
            SizedBox(width: 10.w),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0).h,
                        child: Text(item.name,
                            style:
                                AppTextStyles.ff000000(context, fontSize: 15)),
                      ),
                      const Spacer(),
                      Text('›',
                          style: AppTextStyles.ff000000(context, fontSize: 20)),
                    ],
                  ),
                  SizedBox(height: 8.h),
                  Container(
                    height: 1.5.h,
                    color: AppColors.ffaeaeae, // Replace with your '#AEAEAE'
                    margin: const EdgeInsets.only(left: 11, top: 8).w,
                  ),
                  SizedBox(height: 10.h),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 70).w,
                    child: Text(item.remark,
                        style: AppTextStyles.ff757575(context, fontSize: 13)),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
