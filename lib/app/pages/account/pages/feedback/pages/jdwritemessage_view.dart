import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/feedback/pages/jdwritemessage_view_controller.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:multi_image_picker_view/multi_image_picker_view.dart';

class JdwritemessageView extends StatefulWidget {
  JdwritemessageView({super.key, required this.pageIndex});
  final int pageIndex;

  @override
  State<JdwritemessageView> createState() => _JdwritemessageView();
}

class _JdwritemessageView extends State<JdwritemessageView> {
  JdwritemessageViewController controller = JdwritemessageViewController();
  final pickController = MultiImagePickerController(
      maxImages: 3,
      picker: (allowMultiple) async {
        return await JdwritemessageViewController.pickImagesUsingImagePicker(
            allowMultiple);
      });

  void initState() {
    super.initState();
    controller.type = widget.pageIndex.toString();
    if (widget.pageIndex == 1)
      controller.messageType = '反馈类型：提交建议 ';
    else
      controller.messageType = '反馈类型：我要投诉';
  }

  @override
  Widget build(BuildContext context) {
    double itemWidth = (MediaQuery.of(context).size.width - 60) /
        3; // Adjust item width as needed

    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        toolbarHeight: 65.h,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title:
            Text('反馈纪录', style: AppTextStyles.surface_(context, fontSize: 24)),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0).h,
              child: Text(controller.messageType ?? '',
                  style: AppTextStyles.bodyText1(context, fontSize: 16)),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0).w,
              child: SizedBox(
                height: 200.h,
                child: TextField(
                  onChanged: (value) => controller.content = value,
                  style: AppTextStyles.ffC5CBC6_(context, fontSize: 14),
                  decoration: InputDecoration(
                    hintText: '请输入反馈内容',
                    hintStyle: AppTextStyles.ffC5CBC6_(context, fontSize: 14),
                    border: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.ffC5CBC6),
                    ),
                  ),
                  maxLines: 5,
                  maxLength: 200,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0).w,
              child: SizedBox(
                  height: 200.h,
                  width: 1.sw,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 32),
                      Expanded(
                        child: MultiImagePickerView(
                          controller: pickController,
                          gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: itemWidth,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10,
                            childAspectRatio: 1,
                          ),
                          initialWidget: _addPickWidget(context, itemWidth),
                          addMoreButton: _addPickWidget(context, itemWidth),
                          padding: const EdgeInsets.all(10),
                        ),
                      ),
                    ],
                  )),
            ),
            const SizedBox(height: 50),
            GestureDetector(
                onTap: () => submit(),
                child: Center(
                  child: Container(
                    width: 0.8.sw,
                    height: 49.h,
                    decoration: const BoxDecoration(
                      color: AppColors.ffbfa46d,
                      borderRadius: BorderRadius.all(
                        Radius.circular(4),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "提交",
                          style: AppTextStyles.surface_(context, fontSize: 17),
                        ),
                      ],
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }

  void _pickImage() {
    AppToast.showDuration(msg: '操作成功', duration: 1);
    pickController.pickImages();
  }

  Widget _addPickWidget(BuildContext context, double itemWidth) {
    return SizedBox(
        child: GestureDetector(
      onTap: _pickImage,
      child: Container(
          height: itemWidth,
          width: itemWidth,
          decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(
                  width: 1.w,
                  color: AppColors.ffC5CBC6,
                  style: BorderStyle.solid)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('+',
                    style: AppTextStyles.bodyText1(context, fontSize: 40)),
                Text('添加图片',
                    style: AppTextStyles.bodyText1(context, fontSize: 14))
              ])),
    ));
  }

  void submit() {
    try {
      controller.files.clear();
      for (var i in pickController.images) {
        controller.files.add(i.path as String);
      }
      controller.submit();
    } catch (e) {
      AppLogger.e(e);
    }
  }
}
