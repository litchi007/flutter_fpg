import 'package:fpg_flutter/data/models/wd/DepositRecordModel.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:intl/intl.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';

class KsfeedbackrecordViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();
  List<String>? dateSelect = ['全部日期'];
  int? type;
  String? date;
  RxList tableData = RxList();
  List<DepositRecordModel> allTableData = [];
  RxString leftDropdownValue = '全部日期'.obs;
  RxString rightDropdownValue = '全部状态'.obs;
  UGUserModel userInfo = UGUserModel();
  Map<String, String> liqTypeNo = {
    '全部状态': '全部状态',
    '待回复': '待回复',
    '已回复': '已回复',
  };
  List<dynamic>? arr;
  var liqTypeList = ['全部状态', '待回复', '已回复'];
  @override
  void onInit() {
    super.onInit();

    fetchData();
  }

  void fetchData() async {
    if (type == 0) {
      _appUserRepository
          .getRecord(
              token: AppDefine.userToken?.apiToken,
              type: 0,
              date: date,
              isReply: 0)
          .then((data) {
        if (data != null) {
          arr?.addAll(data.list);
        }
      }).catchError((data) {});
      _appUserRepository
          .getRecord(
              token: AppDefine.userToken?.apiToken,
              type: 1,
              date: date,
              isReply: 1)
          .then((data) {
        if (data != null) {
          arr?.addAll(data.list);
        }
      }).catchError((data) {});
    } else if (type == 2) {
      _appUserRepository
          .getRecord(
              token: AppDefine.userToken?.apiToken,
              type: 1,
              date: date,
              isReply: 1)
          .then((data) {
        if (data != null) {
          arr?.addAll(data.list);
        }
      }).catchError((data) {});
    } else {
      _appUserRepository
          .getRecord(
              token: AppDefine.userToken?.apiToken,
              type: 0,
              date: date,
              isReply: 0)
          .then((data) {
        if (data != null) {
          arr?.addAll(data.list);
        }
      }).catchError((data) {});
    }
    tableData.addAll(arr!);
  }

  List<String> getDaysArray(int year, int month, int date) {
    List<String> result = [];
    DateTime newDate = DateTime(year, month, date);
    while ((newDate.month == month && newDate.day >= 1) ||
        (newDate.month == month - 1 && newDate.day > date)) {
      result.add(
          DateFormat('yyyy-MM-dd').format(newDate.subtract(Duration(days: 1))));
      newDate = newDate.subtract(Duration(days: 1));
    }
    return result;
  }
}
