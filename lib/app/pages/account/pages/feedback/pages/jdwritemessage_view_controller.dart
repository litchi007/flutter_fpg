import 'package:get/get.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker_view/multi_image_picker_view.dart';

class JdwritemessageViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();

  String? messageType;
  String? type;
  String? content;
  List<String> files = [];

  void submit() async {
    _appUserRepository
        .addFeedback(
            token: AppDefine.userToken?.apiToken,
            type: type,
            pid: '',
            content: content,
            imgPaths: files)
        .then((data) {
      files.clear();
      content = '';
      Get.back();
    }).catchError((data) {});
  }

  static Future<List<ImageFile>> pickImagesUsingImagePicker(
      bool allowMultiple) async {
    final picker = ImagePicker();
    final List<XFile> xFiles;
    if (allowMultiple) {
      xFiles = await picker.pickMultiImage(maxWidth: 1080, maxHeight: 1080);
    } else {
      xFiles = [];
      final xFile = await picker.pickImage(
          source: ImageSource.gallery, maxHeight: 1080, maxWidth: 1080);
      if (xFile != null) {
        xFiles.add(xFile);
      }
    }
    if (xFiles.isNotEmpty) {
      return xFiles.map<ImageFile>((e) => convertXFileToImageFile(e)).toList();
    }
    return [];
  }
}
