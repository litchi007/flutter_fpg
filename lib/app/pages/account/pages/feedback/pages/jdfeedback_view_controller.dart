import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/api_ticket.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';

class JdfeedbackViewController extends GetxController {
  final ApiTicket _apiticket = ApiTicket();

  RxString? totalWinAmount = '0'.obs;
  RxString? totalBetAmount = '0'.obs;
  RxList<UGBetsRecordLongModel> newState = RxList();

  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  void fetchData() async {
    await _apiticket.appHttpClient
        .getAllHistory(
      token: AppDefine.userToken?.apiSid,
    )
        .then((data) {
      totalWinAmount?.value = data.data?.data?.totalWinAmount ?? '0';
      totalBetAmount?.value = data.data?.data?.totalBetAmount ?? '0';
      newState.addAll(data.data?.data?.list ?? []);
    });
  }
}
