import 'package:fpg_flutter/data/models/UserMsgList.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/api/user/user.http.dart';

class UserMessageViewController extends GetxController {
  RxBool isIniting = false.obs;

  /// 是否展开
  final isExpanded = false.obs;

  final AppUserRepository _appUserMsg = AppUserRepository();
  RxList<UserMsg> msgList = RxList();

  Future<String> readAllMsg() async {
    var res = await UserHttp.userReadMsgAll();
    return res.msg;
  }

  Future<String> deleteAllMsg() async {
    var res = await UserHttp.userDeleteMsgAll();
    if (res.code == 0) {
      msgList.value = [];
    }
    return res.msg;
  }

  void deleteItem(int index, String? msgId) async {
    var res = await UserHttp.userDeleteMsg(msgId);
    if (res.code == 0) {
      msgList.removeAt(index);
    }
  }

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    isIniting(true);
    await _appUserMsg.appHttpClient
        .msgList(
      token: AppDefine.userToken!.apiSid,
      page: 1,
      row: 20,
    )
        .then((res) {
      if (res.data != null) {
        List<UserMsg>? list = [];
        list.addAll(res.data!.list ?? []);
        list.addAll(res.data!.list ?? []);
        msgList.value = list;
      }
    });

    isIniting(false);
  }
}
