import 'package:flutter/material.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/data/models/UserMsgList.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/user_message/usermessage_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/widgets/alertfrom_html.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class UserMessageView extends StatefulWidget {
  const UserMessageView({super.key});

  @override
  State<UserMessageView> createState() => _UserMessageView();
}

class _UserMessageView extends State<UserMessageView> {
  final UserMessageViewController controller =
      Get.put(UserMessageViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
            // modal.dialogBuilder();
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 28.w),
        ),
        title: Text(
          '站内信',
          style: AppTextStyles.ffffffff(context, fontSize: 28),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
          child: Stack(
        children: [
          Column(
            children: [
              Expanded(
                  child: Obx(
                () => controller.msgList.isEmpty
                    ? Center(
                        child: Text(
                        '暂无数据',
                        style: AppTextStyles.bodyText2(context, fontSize: 20),
                      ))
                    : ListView.builder(
                        itemCount: controller.msgList.length,
                        itemBuilder: (context, index) {
                          UserMsg msg = controller.msgList.elementAt(index);
                          return Ucolumn(
                            title: msg.title ?? '',
                            time: msg.readTime ?? '',
                            index: index,
                            onPressed: () {
                              AlertFromHtml(
                                      context: context,
                                      title: msg.title ?? '',
                                      titleBackgroundColor: AppColors.surface,
                                      action: Padding(
                                        padding: const EdgeInsets.all(10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                // Get.back();
                                                controller.deleteItem(
                                                    index, msg.id);
                                                Get.back();
                                              },
                                              child: Container(
                                                padding:
                                                    EdgeInsets.only(top: 8.w),
                                                height: 40.w,
                                                width: 0.25.sw,
                                                decoration: BoxDecoration(
                                                  color: AppColors.surface,
                                                  border: Border.all(
                                                    // color: AppColors.ff9e9e9e,
                                                    width: 1.sp,
                                                  ),
                                                ),
                                                child: Text('删除',
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        AppTextStyles.ff000000(
                                                            context,
                                                            fontSize: 16)),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 0.1.sw,
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                // controller.user_msg!.value.list!
                                                //     .removeAt(2);
                                                Get.back();
                                              },
                                              child: Container(
                                                height: 40.w,
                                                width: 0.25.sw,
                                                padding:
                                                    EdgeInsets.only(top: 8.w),
                                                color: AppColors.ffbfa46d,
                                                child: Text('确定',
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        AppTextStyles.ff000000(
                                                            context,
                                                            fontSize: 16)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      htmlstring: msg.content ?? '')
                                  .dialogBuilder();
                            },
                          );
                        },
                      ),
              )),
              Obx(() {
                if (controller.isExpanded.value) {
                  return Container(
                    height: 44,
                    color: '#cbd9e1'.color(),
                    child: Row(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(width: 44),
                        Container(
                          width: 102,
                          height: 35,
                          alignment: Alignment.center,
                          color: '#3C88F7'.color(),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AppImage.network(
                                img_images('message/all_read'),
                                width: 12.w,
                                height: 12.w,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                '全部已读',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 11),
                              )
                            ],
                          ),
                        ).onTap(() async {
                          if (controller.msgList.isNotEmpty) {
                            var msg = await controller.readAllMsg();
                            ToastUtils.show(msg);
                          }
                        }),
                        SizedBox(width: 10),
                        Container(
                          width: 102,
                          height: 35,
                          alignment: Alignment.center,
                          color: '#ff0000'.color(),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AppImage.network(
                                img_images('delete_ico_white'),
                                width: 12.w,
                                height: 12.w,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                '一键删除',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 11),
                              )
                            ],
                          ),
                        ).onTap(() {
                          if (controller.msgList.isNotEmpty) {
                            showDeleteAllMsgDialog();
                          }
                        }),
                      ],
                    ),
                  );
                }
                return Container(
                  height: 10,
                  color: '#cbd9e1'.color(),
                );
              }),
            ],
          ),
          Obx(() => Positioned(
              right: 20,
              bottom: controller.isExpanded.value ? 44 : 10,
              child: AppImage.network(img_images(controller.isExpanded.value
                      ? 'message/expand_arrow_down_bg'
                      : 'message/expand_arrow_bg'))
                  .onTap(() => controller.isExpanded.value =
                      !controller.isExpanded.value)))
        ],
      )),
    );
  }

  void showDeleteAllMsgDialog() {
    SmartDialog.show(builder: (context) {
      return Container(
        width: MediaQuery.of(context).size.width - 36,
        height: 100,
        decoration: BoxDecoration(
          color: context.customTheme?.lotteryPopupItemBg,
          borderRadius: const BorderRadius.all(Radius.circular(4)),
        ),
        child: Column(
          children: [
            const SizedBox(height: 10),
            Text(
              '全部删除？',
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                const SizedBox(width: 10),
                Expanded(
                    child: Container(
                  height: 42,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    // 设置渐变色背景
                    border: Border.all(
                      color: const Color(0xffa9a9a9), // 边框颜色
                      width: 1.0, // 边框宽度
                    ),
                    borderRadius: BorderRadius.circular(4), // 设置圆角
                  ),
                  child: Text(
                    '取消',
                    style: TextStyle(
                        color: context.customTheme?.fontColor, fontSize: 16),
                  ),
                ).onTap(() => SmartDialog.dismiss())),
                const SizedBox(width: 5),
                Expanded(
                    child: Container(
                  height: 42,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [
                        '#b89b65'.color(),
                        '#b89b65'.color(),
                      ],
                    ), // 设置渐变色背景
                    borderRadius: BorderRadius.circular(4), // 设置圆角
                  ),
                  child: const Text(
                    '确定',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ).onTap(() {
                  SmartDialog.dismiss();
                  controller.deleteAllMsg();
                })),
                const SizedBox(width: 10),
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      );
    });
  }
}

class Ucolumn extends Container {
  final String? title;
  final String? time;
  VoidCallback? onPressed;
  final int index;
  Ucolumn({
    required this.title,
    required this.time,
    required this.index,
    this.onPressed,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 1.sw,
        height: 50.h,
        padding: const EdgeInsets.symmetric(horizontal: 10).w,
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    width: 1.w,
                    color: AppColors.ffC5CBC6,
                    style: BorderStyle.solid))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title!,
              style: AppTextStyles.error_(context, fontSize: 20),
            ),
            Text(
              time!,
              style: AppTextStyles.error_(context, fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
}
