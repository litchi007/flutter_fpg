import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/completedtoday/pages/jddayclosedetail_view_controller.dart';
import 'package:fpg_flutter/utils/helper.dart';

class JddayclosedetailView extends StatefulWidget {
  const JddayclosedetailView({super.key});

  @override
  _JddayclosedetailViewState createState() => _JddayclosedetailViewState();
}

class _JddayclosedetailViewState extends State<JddayclosedetailView> {
  final JddayclosedetailViewController controller =
      Get.put(JddayclosedetailViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 65.h,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 18.w),
          ),
          title: Text('今日已结',
              style: AppTextStyles.surface_(context, fontSize: 24)),
          centerTitle: true,
        ),
        bottomNavigationBar: ColoredBox(
          color: AppColors.ffbfa46d,
          child: Padding(
            padding: EdgeInsets.all(5.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Obx(
                  () => Text(
                    '下注金额: ${controller.totalBetAmount != null ? controller.totalBetAmount!.value : ''}',
                    style: AppTextStyles.ffffffff(
                      context,
                      fontSize: 20,
                    ),
                  ),
                ),
                Obx(
                  () => Text(
                    '输赢金额: ${controller.totalWinAmount != null ? controller.totalWinAmount!.value : ''}',
                    style: AppTextStyles.ffffffff(
                      context,
                      fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Column(
          children: [
            Obx(() => Table(
                    border: TableBorder.all(color: AppColors.ffd1cfd0),
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    children: [
                      _createTableHeader(context),
                      ..._createTableContent(context),
                    ]))
          ],
        ))));
  }

  TableCell _dataCell(BuildContext context, String content,
      {int fontSize = 17}) {
    return TableCell(
      verticalAlignment: TableCellVerticalAlignment.middle,
      child: Container(
        height: 80.h,
        alignment: Alignment.center,
        child: Text(content,
            textAlign: TextAlign.center,
            style: AppTextStyles.bodyText3(context, fontSize: fontSize)),
      ),
    );
  }

  List<TableRow> _createTableContent(BuildContext context) {
    return controller.newState.map((row) {
      return TableRow(children: [
        _dataCell(context, row.gameName ?? ''),
        _dataCell(context, row.id ?? ''),
        _dataCell(context,
            '${row.playGroupName ?? ''} ${row.playName ?? ''} ${row.betInfo ?? ''} @ ${((convertToDouble(row.odds) ?? 0) * 100).round() / 100}'),
        _dataCell(
            context,
            (((convertToDouble(row.betAmount) ?? 0) * 100).round() / 100)
                .toString()),
        _dataCell(context, row.lotteryNo ?? ''),
      ]);
    }).toList();
  }

  TableRow _createTableHeader(BuildContext context) {
    return TableRow(children: [
      TableCell(
          child: Row(
        children: [
          Text('期号/',
              textAlign: TextAlign.center,
              style: AppTextStyles.bodyText3(context, fontSize: 18)),
          Text('注单号',
              textAlign: TextAlign.center,
              style: AppTextStyles.error_(context, fontSize: 18)),
        ],
      )),
      _dataCell(context, '下注明细', fontSize: 18),
      _dataCell(context, '下注金额', fontSize: 18),
      _dataCell(context, '开奖结果', fontSize: 18),
      _dataCell(context, '输赢', fontSize: 18),
    ]);
  }
}
