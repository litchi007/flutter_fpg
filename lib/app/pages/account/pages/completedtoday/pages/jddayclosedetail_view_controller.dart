import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/api_ticket.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:intl/intl.dart';

class JddayclosedetailViewController extends GetxController {
  final ApiTicket _apiticket = ApiTicket();

  RxString? totalWinAmount = '0'.obs;
  RxString? totalBetAmount = '0'.obs;
  RxList<UGBetsRecordLongModel> newState = RxList();
  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  String getCurrentDate(int daysBeforeNow) {
    DateTime now = DateTime.now();

    DateTime adjustedDate = now.subtract(Duration(days: daysBeforeNow));

    String formattedCurrentDate = DateFormat('yyyy-MM-dd').format(adjustedDate);

    return formattedCurrentDate;
  }

  void fetchData() async {
    // String formattedDate = getCurrentDate();
    await _apiticket.appHttpClient
        .history(
      token: AppDefine.userToken?.apiSid,
      category: 'lottery',
      status: 5,
      startDate: getCurrentDate(0),
      endDate: getCurrentDate(0),
    )
        .then((data) {
      totalWinAmount?.value = data.data?.data?.totalWinAmount ?? '0';
      totalBetAmount?.value = data.data?.data?.totalBetAmount ?? '0';
      newState.addAll(data.data?.data?.list ?? []);
    });
  }
}
