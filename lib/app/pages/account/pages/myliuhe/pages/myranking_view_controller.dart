import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/data/models/LhcdocModel.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';

import 'package:get/get.dart';

// app_lhcdoc_repository

class MyRankingViewController extends GetxController {
  final AppLHCDocRepository _appLhcDoc = AppLHCDocRepository();
  RxInt idx = 0.obs;
  Rx<FansRanking> fansRankingData = FansRanking().obs;
  Rx<TipsRanking> tipsRankingData = TipsRanking().obs;

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    getLhcFansRankData();
    getLhcTipsRankData();
  }

  void getLhcFansRankData() async {
    await _appLhcDoc.appHttpClient
        .getLhcFansRankData(token: AppDefine.userToken!.apiSid, limit: 20)
        .then((data) {
      fansRankingData.value = data.data!;
      AppToast.showDuration(msg: data.msg, duration: 2);
    });
  }

  void getLhcTipsRankData() {
    _appLhcDoc.appHttpClient
        .getLhcTipsRankData(token: AppDefine.userToken!.apiSid, limit: 999)
        .then((data) {
      tipsRankingData.value = data.data!;
    });
  }
}
