import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/userModel/lhc_fans_list_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class MyFansViewController extends GetxController {
  RxString avatar = ''.obs;
  RxInt idx = 0.obs;
  String uid = "";
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  Rx<LhcFansListModel> posts = LhcFansListModel().obs;
  Rx<LhcFansListModel> people = LhcFansListModel().obs;
  RxBool isLoading = false.obs;
  // Map<String, Rx<UserProfileModel>> userProfile = {};
  // Map<String, Rx<LhcdocContent>> lhcdocContent = {};
  // Rx<LhcdocContent> lhcdocHistoryContent = LhcdocContent().obs;
  // RxList<LhcdocContentList> lhcdocFavContentList = RxList();
  // RxBool isLoading = false.obs;
  // RxBool isLoading2 = false.obs;

  // RxString alias = 'forum'.obs;
  // Rx<LhcdocContentList> lhcdocContentList = LhcdocContentList().obs;
  // Rx<FollowModel> followList = FollowModel().obs;

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    isLoading.value = true;
    avatar.value = GlobalService.to.userInfo.value?.avatar ?? "";
    contentFansList();
    fansList();
  }

  void contentFansList() async {
    await _appLHCDocRepository
        .contentFansList(page: '0', rows: '1000')
        .then((data) {
      posts.value = data ?? LhcFansListModel();
    });

    //.then((data) {
  }

  void fansList() async {
    await _appLHCDocRepository.fansList(page: '0', rows: '1000').then((data) {
      people.value = data ?? LhcFansListModel();
    });
    isLoading.value = false;
    //.then((data) {
  }

  // Future<void> getContentList() async {
  //   // lhcdocContent.value = LhcdocContent();
  //   isLoading.value = true;
  //   await _appLHCDocRepository
  //       .getLhcContentList(alias.value, 1, uid: uid, rows: 1000)
  //       .then((data) {
  //     if (data != null) {
  //       lhcdocContent[uid] = data.obs;
  //     }
  //     isLoading.value = false;
  //   });
  // }

  // Future getHistoryContent(String uid) async {
  //   lhcdocHistoryContent.value = LhcdocContent();
  //   String token = AppDefine.userToken?.apiSid ?? "";
  //   isLoading2.value = true;
  //   await _appLHCDocRepository
  //       .getLhcHistoryContent(uid: uid, token: token)
  //       .then((data) {
  //     if (data != null) {
  //       lhcdocHistoryContent.value = data;
  //     }
  //     isLoading2.value = false;
  //   });
  // }

  // Future<void> getFollowFavList() async {
  //   await getFavContentList();
  //   await getFollowList();
  // }

  // Future<void> getFavContentList() async {
  //   lhcdocFavContentList.value = RxList();
  //   String token = AppDefine.userToken?.apiSid ?? "";
  //   isLoading2.value = true;
  //   await _appLHCDocRepository
  //       .getFavContentList(uid: uid, token: token)
  //       .then((data) {
  //     if (data != null) {
  //       lhcdocFavContentList.value = data;
  //     }
  //     isLoading2.value = false;
  //   });
  // }
}
