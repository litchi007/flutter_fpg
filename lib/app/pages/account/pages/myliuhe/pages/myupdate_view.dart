import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/components/nodata_widget.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/components/custom.nodata.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

import 'package:get/get.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/FollowListModel.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/myliuhe_view_controller.dart';

class MyUpdateView extends StatefulWidget {
  const MyUpdateView({super.key});
  @override
  State<MyUpdateView> createState() => _MyUpdateViewState();
}

class _MyUpdateViewState extends State<MyUpdateView> {
  MyLiuheViewController controller = Get.find();
  final RxInt _tabIndex = 0.obs;

  @override
  void initState() {
    List<String>? args = Get.arguments;
    controller.uid = args?[0] ?? "";
    _tabIndex.value = int.parse(args?[1] ?? '0');
    controller.getFollowFavList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: appThemeColors?.homeBackgroundColor,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '我的关注',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        body: Obx(() => Column(
              children: [
                Container(
                    color: Colors.white,
                    padding:
                        EdgeInsets.symmetric(horizontal: 40.w, vertical: 10.h),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: ElevatedButton(
                                onPressed: () {
                                  if (_tabIndex.value == 0) return;
                                  _tabIndex.value = 0;
                                },
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10.w),
                                          bottomLeft: Radius.circular(10.w)),
                                    ),
                                    side: _tabIndex.value == 0
                                        ? BorderSide.none
                                        : BorderSide(
                                            color: Colors.black, width: 1.h),
                                    foregroundColor: _tabIndex.value == 0
                                        ? Colors.white
                                        : Colors.black,
                                    backgroundColor: _tabIndex.value == 0
                                        ? AppColors.drawMenu
                                        : Colors.white),
                                child: Text('关注专家',
                                    style: TextStyle(fontSize: 22.sp)))),
                        Expanded(
                            flex: 1,
                            child: ElevatedButton(
                                onPressed: () {
                                  if (_tabIndex.value == 1) return;
                                  _tabIndex.value = 1;
                                },
                                style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10.w),
                                          bottomRight: Radius.circular(10.w)),
                                    ),
                                    side: _tabIndex.value == 1
                                        ? BorderSide.none
                                        : BorderSide(
                                            color: Colors.black, width: 1.h),
                                    foregroundColor: _tabIndex.value == 1
                                        ? Colors.white
                                        : Colors.black,
                                    backgroundColor: _tabIndex.value == 1
                                        ? AppColors.drawMenu
                                        : Colors.white),
                                child: Text('关注帖子',
                                    style: TextStyle(fontSize: 22.sp)))),
                      ],
                    )),
                Gap(10.h),
                Expanded(
                    child: controller.isLoading2.value == true
                        ? const Center(child: CircularProgressIndicator())
                        : _tabIndex.value == 0
                            ? (controller.followList.value.list ?? []).isEmpty
                                ? Container(
                                    margin: const EdgeInsets.only(top: 100),
                                    child: const CustomNodata())
                                : ListView.separated(
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      FollowListModel item = controller
                                          .followList.value.list![index];
                                      return Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 25.w),
                                          color: Colors.white,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                children: [
                                                  item.headImg != null &&
                                                          item.headImg != ""
                                                      ? AppImage.network(
                                                          item.headImg ?? "",
                                                          width: 50.w)
                                                      : AppImage.asset(
                                                          'money-2.png',
                                                          width: 50.w),
                                                  Gap(10.w),
                                                  item.nickname != null &&
                                                          item.nickname != ""
                                                      ? Text(
                                                          item.nickname ?? "",
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .ff666666,
                                                              fontSize: 18.sp))
                                                      : Text("昵称已被禁用",
                                                          style: TextStyle(
                                                              color: AppColors
                                                                  .ff666666,
                                                              fontSize: 18.sp)),
                                                ],
                                              ),
                                              ElevatedButton(
                                                  onPressed: () async {
                                                    await controller
                                                        .followPoster(
                                                            item.posterUid ??
                                                                "",
                                                            0);
                                                    controller.getFollowList();
                                                  },
                                                  style: AppButtonStyles
                                                      .elevatedStyle(
                                                    backgroundColor:
                                                        AppColors.drawMenu,
                                                    foregroundColor:
                                                        Colors.white,
                                                    fontSize: 16,
                                                    width: 110.w,
                                                    height: 30,
                                                    padding: EdgeInsets.zero,
                                                  ),
                                                  child: const Text('取消关注'))
                                            ],
                                          )).onTap(() {
                                        Get.toNamed(userProfilePath,
                                            parameters: {
                                              'uid': item.posterUid ?? ""
                                            },
                                            preventDuplicates: false);
                                      });
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return Divider(
                                        height: 1.h,
                                      );
                                    },
                                    itemCount:
                                        (controller.followList.value.list ?? [])
                                            .length)
                            : (controller.lhcdocFavContentList).isEmpty
                                ? Container(
                                    margin: const EdgeInsets.only(top: 100),
                                    child: const CustomNodata())
                                : ListView.separated(
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      LhcdocContentList item = controller
                                          .lhcdocFavContentList[index];
                                      return Container(
                                        constraints:
                                            BoxConstraints(minHeight: 100.h),
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 25.w),
                                        color: Colors.white,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment
                                              .center, // Align to the top for multiline text
                                          children: [
                                            Expanded(
                                              child: GestureDetector(
                                                onTap: () {
                                                  // AppNavigator.toNamed(
                                                  //     myContentDatailPath,
                                                  //     arguments: [item]);
                                                  Get.toNamed(
                                                      myContentDatailPath,
                                                      arguments: [item]);
                                                },
                                                child: Text(
                                                  item.title ?? "",
                                                  style:
                                                      AppTextStyles.bodyText2(
                                                          context,
                                                          fontSize: 22),
                                                  overflow: TextOverflow
                                                      .visible, // Ensure text is visible and wraps
                                                  softWrap:
                                                      true, // Allows text to wrap to multiple lines
                                                ).paddingOnly(left: 17.w),
                                              ),
                                            ),
                                            ElevatedButton(
                                              onPressed: () async {
                                                await controller.doFavorites(
                                                    item.cid ?? "", 0);
                                                controller.getFavContentList();
                                              },
                                              style:
                                                  AppButtonStyles.elevatedStyle(
                                                backgroundColor:
                                                    AppColors.drawMenu,
                                                foregroundColor: Colors.white,
                                                fontSize: 20,
                                                width: 150.w,
                                                height: 50,
                                                padding: EdgeInsets.zero,
                                              ),
                                              child: const Text('取消关注'),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return Divider(
                                        height: 1.h,
                                      );
                                    },
                                    itemCount: (controller.lhcdocFavContentList)
                                        .length))
              ],
            )));
  }
}
