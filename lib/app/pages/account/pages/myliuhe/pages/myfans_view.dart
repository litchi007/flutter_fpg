import 'package:flutter/material.dart';
import 'package:fpg_flutter/data/models/userModel/lhc_fans_list_model.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myfans_view_controller.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:gap/gap.dart';

class MyFansView extends StatefulWidget {
  @override
  _MyFansView createState() => _MyFansView();
}

class _MyFansView extends State<MyFansView> {
  MyFansViewController controller = Get.put(MyFansViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.fffafafa,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            children: [
              Icon(Icons.arrow_back_ios, color: AppColors.ffffffff, size: 28.w)
                  .paddingOnly(left: 12.w),
              Text('返回', style: AppTextStyles.surface_(context, fontSize: 20)),
            ],
          ),
        ),
        title:
            Text('我的粉丝', style: AppTextStyles.ffffffff(context, fontSize: 24)),
        centerTitle: true,
      ),
      body: Container(
        width: 1.sw,
        decoration: BoxDecoration(
          border: Border.all(
            color: AppColors.surface, // Border color and width
            // borderRadius: BorderRadius.circular(8.0), // Optional border radius
          ),
        ),
        child: Column(
          children: [
            Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 40.w, vertical: 10.h),
                child: Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Obx(() => ElevatedButton(
                            onPressed: () {
                              controller.idx.value = 0;
                            },
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.w),
                                      bottomLeft: Radius.circular(10.w)),
                                ),
                                side: controller.idx.value == 0
                                    ? BorderSide.none
                                    : BorderSide(
                                        color: Colors.black, width: 1.h),
                                foregroundColor: controller.idx.value == 0
                                    ? Colors.white
                                    : Colors.black,
                                backgroundColor: controller.idx.value == 0
                                    ? AppColors.drawMenu
                                    : Colors.white),
                            child: Text('我的粉丝',
                                style: TextStyle(fontSize: 22.sp))))),
                    Expanded(
                        flex: 1,
                        child: Obx(() => ElevatedButton(
                            onPressed: () {
                              controller.idx.value = 1;
                            },
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10.w),
                                      bottomRight: Radius.circular(10.w)),
                                ),
                                side: controller.idx.value == 1
                                    ? BorderSide.none
                                    : BorderSide(
                                        color: Colors.black, width: 1.h),
                                foregroundColor: controller.idx.value == 1
                                    ? Colors.white
                                    : Colors.black,
                                backgroundColor: controller.idx.value == 1
                                    ? AppColors.drawMenu
                                    : Colors.white),
                            child: Text('帖子粉丝',
                                style: TextStyle(fontSize: 22.sp))))),
                  ],
                )),
            Gap(10.h),
            Obx(() => controller.isLoading.value == true
                ? Column(
                    children: [Gap(300.h), const CircularProgressIndicator()])
                : Obx(() {
                    int idx = controller.idx.value;
                    List<LhcFansModel> postsList =
                        controller.posts.value.list ?? [];
                    List<LhcFansModel> peopleList =
                        controller.people.value.list ?? [];
                    return idx == 1
                        ? postsList.isEmpty
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  AppImage.network(img_root('images/lhc/pl'),
                                      width: 200.w, height: 200.w),
                                  Text(
                                    '暂无数据',
                                    style: AppTextStyles.ff000000(context,
                                        fontSize: 20),
                                  ),
                                ],
                              )
                            : Column(
                                children: postsList.map((item) {
                                return GestureDetector(
                                  onTap: () {
                                    AppNavigator.toNamed(userProfilePath,
                                        parameters: {'uid': item.uid ?? ""});
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    height: 60,
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Color(0xFFF8F8F8))),
                                      color: Colors.white,
                                    ),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20.w),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            AppImage.network(
                                                item.headImg ??
                                                    img_assets(
                                                        'lh/defaultAvatar'),
                                                width: 75.w,
                                                height: 75.h,
                                                fit: BoxFit.contain),
                                            const SizedBox(width: 30),
                                            Text(
                                              item.nickname ?? '',
                                              style: AppTextStyles.bodyText2(
                                                  context,
                                                  fontSize: 16),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }).toList())
                        : peopleList.isEmpty
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  AppImage.network(img_root('images/lhc/pl'),
                                      width: 200.w, height: 200.w),
                                  Text(
                                    '暂无数据',
                                    style: AppTextStyles.ff000000(context,
                                        fontSize: 20),
                                  ),
                                  // Add your optional button here if needed
                                ],
                              )
                            : Column(
                                children: peopleList.map((item) {
                                return GestureDetector(
                                  onTap: () {
                                    AppNavigator.toNamed(userProfilePath,
                                        parameters: {'uid': item.uid ?? ""});
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    height: 60,
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Color(0xFFF8F8F8))),
                                      color: Colors.white,
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            AppImage.network(
                                                item.headImg ??
                                                    img_assets(
                                                        'lh/defaultAvatar'),
                                                width: 75.w,
                                                height: 75.h,
                                                fit: BoxFit.contain),
                                            const SizedBox(width: 30),
                                            Text(
                                              item.nickname ?? '',
                                              style: AppTextStyles.bodyText2(
                                                  context,
                                                  fontSize: 16),
                                            ),
                                          ],
                                        ),
                                        // Add your optional button here if needed
                                      ],
                                    ),
                                  ),
                                );
                              }).toList());
                  })),
          ],
        ),
      ),
    );
  }
}
