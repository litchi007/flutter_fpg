import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

class MyFocusViewController extends GetxController {
  RxString avatar = ''.obs;
  // RxString infoBalance = ''.obs;
  // RxString infoAmountsum = ''.obs;
  // RxString infoUsr = ''.obs;
  RxInt idx = 0.obs;

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    avatar.value = GlobalService.to.userInfo.value?.avatar ?? "";
    // infoAmountsum.value = AppDefine.userInfo!.amountSum!.toString();
    // infoBalance.value = AppDefine.userInfo!.balance!;
    // infoUsr.value = AppDefine.userInfo!.usr!;
  }
}
