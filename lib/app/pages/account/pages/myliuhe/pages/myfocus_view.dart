import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myfocus_view_controller.dart';
import 'package:get/get.dart';

class MyFocusView extends StatefulWidget {
  @override
  _MyFocusView createState() => _MyFocusView();
}

class _MyFocusView extends State<MyFocusView> {
  MyFocusViewController controller = Get.put(MyFocusViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            children: [
              Icon(Icons.arrow_back_ios, color: AppColors.ffffffff, size: 28.w)
                  .paddingOnly(left: 12.w),
              // Text('返回', style: AppTextStyles.surface_(context, fontSize: 20)),
            ],
          ),
        ),
        title:
            Text('我的动态', style: AppTextStyles.ffffffff(context, fontSize: 24)),
        centerTitle: true,
      ),
      body: Container(
        width: 1.sw,
        decoration: BoxDecoration(
          border: Border.all(
            color: AppColors.ffF5F5F5, // Border color and width
            // borderRadius: BorderRadius.circular(8.0), // Optional border radius
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppImage.network(img_root('images/lhc/search_noData'),
                width: 200.w, height: 200.w),
            Text(
              '暂时没有查到哦',
              style: AppTextStyles.ff8D8B8B_(context, fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }

  void removePost(int index) {
    // Implement your logic to remove a post from the list
  }

  void removePeople(int index) {
    // Implement your logic to remove a person from the list
  }
}
