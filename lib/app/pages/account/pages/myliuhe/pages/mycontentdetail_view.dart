// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
// import 'package:fpg_flutter/app/controllers/auth_controller.dart';
// import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
// import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
// import 'package:fpg_flutter/app/widgets/app_image.dart';
// import 'package:fpg_flutter/app/widgets/app_toast.dart';
// import 'package:fpg_flutter/app_navigator.dart';
// import 'package:fpg_flutter/configs/app_define.dart';
// import 'package:fpg_flutter/routes.dart';
// import 'package:fpg_flutter/services/global_service.dart';
// import 'package:fpg_flutter/utils/custom_icons_icons.dart';
// import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
// import 'package:fpg_flutter/utils/theme/app_colors.dart';
// import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
// import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
// import 'package:gap/gap.dart';
// import 'package:get/get.dart';
// import 'dart:math';
// import 'package:fpg_flutter/app/pages/account/pages/myliuhe/myliuhe_view_controller.dart';
// import 'package:fpg_flutter/data/models/LhcdocContentList.dart';

// class MyContentdetailView extends StatefulWidget {
//   const MyContentdetailView({super.key});

//   @override
//   State<MyContentdetailView> createState() => _MyContentdetailViewState();
// }

// class _MyContentdetailViewState extends State<MyContentdetailView>
//     with TickerProviderStateMixin {
//   final GlobalKey<ScaffoldState> _homeKey = GlobalKey(); // Create a key
//   AuthController authController = Get.find();
//   MyLiuheViewController controller = Get.find();
//   // GameCenterController gameController = Get.find();
//   final _textEditingController = TextEditingController();
//   final _scrollController = ScrollController();
//   RxInt animalFlag = 0.obs;

//   @override
//   void initState() {
//     List<LhcdocContentList>? args = Get.arguments;
//     controller.lhcdocContentList.value = args?[0] ?? LhcdocContentList();

//     controller.getUserProfile();

//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final appThemeColors = Theme.of(context).extension<AppThemeColors>();
//     return Obx(() => Scaffold(
//         key: _homeKey,
//         backgroundColor: appThemeColors?.surface,
//         appBar: AppGeneralBar(
//           backgroundColor: appThemeColors?.primary,
//           leading: GestureDetector(
//               onTap: () => Navigator.of(context).pop(),
//               child: Container(
//                 padding: EdgeInsets.only(left: 10.w),
//                 child: Row(children: [
//                   Icon(Icons.arrow_back_ios,
//                       color: AppColors.ffffffff, size: 18.w),
//                   Text('返回',
//                       style: AppTextStyles.ffffffff(context, fontSize: 18)),
//                 ]),
//               )),
//           titleWidget: const Center(
//             child: Text(
//               "论坛详情",
//               style: TextStyle(
//                 color: Colors.white,
//                 fontWeight: FontWeight.bold,
//               ),
//               overflow: TextOverflow.ellipsis,
//             ),
//           ),
//           actionWidgets: [
//             GlobalService.to.isAuthenticated.value ? _showMenu() : Container()
//           ],
//         ),
//         endDrawer: GlobalService.to.isAuthenticated.value
//             ? Container(
//                 color: Colors.white,
//                 width: Get.width * 0.5,
//                 height: Get.height,
//                 child: const HomeMenuWidget(),
//               )
//             : null,
//         body: Column(
//             mainAxisAlignment: MainAxisAlignment.start,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Container(
//                 decoration: BoxDecoration(
//                     border: Border(
//                         bottom: BorderSide(color: Colors.grey, width: 1.h))),
//                 padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
//                 child: Row(
//                   children: [
//                     if (controller.lhcdocContentList.value.headImg == null ||
//                         controller.lhcdocContentList.value.headImg == "")
//                       Container(
//                         width: 80.w,
//                         height: 80.w,
//                         decoration: const BoxDecoration(
//                             shape: BoxShape.circle,
//                             color: Color.fromARGB(255, 221, 221, 221)),
//                       ),
//                     if (controller.lhcdocContentList.value.headImg != "")
//                       GestureDetector(
//                         onTap: () {
//                           AppNavigator.back();
//                         },
//                         child: AppImage.network(
//                             controller.lhcdocContentList.value.headImg ?? "",
//                             width: 90.w,
//                             height: 90.w),
//                       ),
//                     Expanded(
//                         child: Column(
//                             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                           Text(
//                               controller.lhcdocContentList.value.nickname ?? "",
//                               style: TextStyle(
//                                   color: Colors.black,
//                                   fontSize: 22.sp,
//                                   fontWeight: FontWeight.bold)),
//                           Text(
//                             "楼主 ${controller.lhcdocContentList.value.createTime}",
//                             style: TextStyle(
//                                 color: AppColors.ff5C5869, fontSize: 18.sp),
//                             overflow: TextOverflow.ellipsis,
//                           )
//                         ])),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         GestureDetector(
//                           onTap: rewardPost,
//                           child: AppImage.asset("redBag_1.png", width: 60.w),
//                         ),
//                         SizedBox(width: 10.w),
//                         Column(
//                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           children: [
//                             ElevatedButton(
//                                 onPressed: () async {
//                                   if (checkIsTest()) return;
//                                   if (!GlobalService.to.isAuthenticated.value) {
//                                     AppNavigator.toNamed(loginPath);
//                                   }
//                                   int followFlag = (controller.lhcdocContentList
//                                                   .value.isFollow !=
//                                               null &&
//                                           controller.lhcdocContentList.value
//                                                   .isFollow ==
//                                               '1')
//                                       ? 0
//                                       : 1;
//                                   bool result = await controller.followPoster(
//                                       controller.lhcdocContentList.value.uid ??
//                                           "",
//                                       followFlag);
//                                   if (result) {
//                                     controller.lhcdocContentList.value
//                                         .isFollow = followFlag.toString();
//                                   }
//                                 },
//                                 style: AppButtonStyles.elevatedStyle(
//                                     fontSize: 18,
//                                     backgroundColor: AppColors.drawMenu,
//                                     foregroundColor: Colors.white,
//                                     width: 100,
//                                     height: 40,
//                                     padding: EdgeInsets.symmetric(
//                                         vertical: 5.w, horizontal: 10.w)),
//                                 child: Text((controller.lhcdocContentList.value
//                                                 .isFollow !=
//                                             null &&
//                                         controller.lhcdocContentList.value
//                                                 .isFollow ==
//                                             '1')
//                                     ? "关注楼主"
//                                     : "取消关注")),
//                             ElevatedButton(
//                                 onPressed: () {
//                                   AppNavigator.toNamed(historyForumPath,
//                                       arguments: [
//                                         controller
//                                                 .lhcdocContentList.value.uid ??
//                                             '0'
//                                       ]);
//                                 },
//                                 style: AppButtonStyles.elevatedStyle(
//                                     fontSize: 18,
//                                     backgroundColor: AppColors.drawMenu,
//                                     foregroundColor: Colors.white,
//                                     width: 100,
//                                     height: 40,
//                                     padding: EdgeInsets.symmetric(
//                                         vertical: 5.w, horizontal: 10.w)),
//                                 child: const Text("历史帖子"))
//                           ],
//                         )
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//               Expanded(
//                   child: Padding(
//                       padding: EdgeInsets.symmetric(
//                           vertical: 10.h, horizontal: 30.w),
//                       child: SingleChildScrollView(
//                           child: Column(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: [
//                             Text(
//                               controller.lhcdocContentList.value.title ?? "",
//                               style: TextStyle(
//                                   color: Colors.black,
//                                   fontSize: 24.sp,
//                                   fontWeight: FontWeight.bold),
//                               textAlign: TextAlign.center,
//                             ),
//                             HtmlWidget(controller
//                                     .lhcdocContentList.value.fullContent ??
//                                 ""),
//                             ...List.generate(
//                                 controller.lhcdocContentList.value.contentPic!
//                                     .length, (idx) {
//                               return AppImage.network(controller
//                                   .lhcdocContentList.value.contentPic?[idx]);
//                             }),
//                             Gap(10.h),
//                             Row(
//                                 mainAxisAlignment: MainAxisAlignment.start,
//                                 children: [
//                                   Text(
//                                       "最后更新时间：${controller.lhcdocContentList.value.createTime}",
//                                       style: TextStyle(
//                                           color: Colors.grey, fontSize: 18.sp))
//                                 ]),
//                             Gap(10.h),
//                             Row(
//                                 mainAxisAlignment: MainAxisAlignment.start,
//                                 children: [
//                                   Container(
//                                     decoration: const BoxDecoration(
//                                         border: Border(
//                                             bottom: BorderSide(
//                                                 width: 1,
//                                                 color: Colors.black))),
//                                     padding: EdgeInsets.only(bottom: 10.h),
//                                     child: Text("全部评论",
//                                         style: TextStyle(
//                                             color: Colors.black,
//                                             fontSize: 18.sp,
//                                             fontWeight: FontWeight.bold)),
//                                   )
//                                 ]),
//                             // if(controller.contentDetail.value.)
//                             AppImage.asset("pl.png", width: 400.w),
//                             Text("还没有评论，你的机会来了！"),
//                             Text("没有更多了"),
//                           ])))),
//               Container(
//                   color: AppColors.drawMenu,
//                   padding:
//                       EdgeInsets.symmetric(vertical: 5.h, horizontal: 10.w),
//                   child: IntrinsicHeight(
//                       child: Row(
//                     children: [
//                       AppImage.asset("yhj.png", width: 50.w),
//                       SizedBox(width: 5.w),
//                       Expanded(
//                           child: GestureDetector(
//                         onTap: () {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                           }
//                           AppNavigator.toNamed(postConentReply);
//                         },
//                         child: Container(
//                             height: 40.h,
//                             padding: EdgeInsets.symmetric(horizontal: 20.w),
//                             alignment: Alignment.centerLeft,
//                             decoration: BoxDecoration(
//                                 borderRadius:
//                                     BorderRadius.all(Radius.circular(20.w)),
//                                 color: Colors.white),
//                             child: const Text(
//                               '说点什么...',
//                               style: TextStyle(color: Colors.grey),
//                             )),
//                       )),
//                       SizedBox(width: 5.w),
//                       GestureDetector(
//                         onTap: () async {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                             return;
//                           }
//                           int likeFlag = (controller
//                                           .lhcdocContentList.value.isLike !=
//                                       null &&
//                                   controller.lhcdocContentList.value.isLike ==
//                                       1)
//                               ? 0
//                               : 1;
//                           bool result = await controller.likePost(
//                               controller.lhcdocContentList.value.cid ?? "",
//                               likeFlag);
//                           if (result) {
//                             controller.lhcdocContentList.value.isLike =
//                                 likeFlag;
//                           }
//                         },
//                         child: Icon(CustomIcons.thumbs_up,
//                             color: (controller.lhcdocContentList.value.isLike !=
//                                         null &&
//                                     controller.lhcdocContentList.value.isLike ==
//                                         1)
//                                 ? Colors.red
//                                 : Colors.white),
//                       ),
//                       if (controller.lhcdocContentList.value.likeNum != null)
//                         Container(
//                           alignment: Alignment.topLeft,
//                           child: Text(
//                             controller.lhcdocContentList.value.likeNum ?? "",
//                             style: const TextStyle(color: Colors.white),
//                           ),
//                         ),
//                       Gap(10.w),
//                       GestureDetector(
//                         onTap: () {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                           }
//                           AppNavigator.toNamed(postConentReply);
//                         },
//                         child: const Icon(CustomIcons.commenting_o,
//                             color: Colors.white),
//                       ),
//                       if (controller.lhcdocContentList.value.replyCount != null)
//                         Container(
//                           alignment: Alignment.topLeft,
//                           child: Text(
//                             '${controller.lhcdocContentList.value.replyCount ?? 0}',
//                             style: const TextStyle(color: Colors.white),
//                           ),
//                         ),
//                       Gap(10.w),
//                       GestureDetector(
//                         onTap: () async {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                           }
//                           int favFlag = (controller
//                                           .lhcdocContentList.value.isFav !=
//                                       null &&
//                                   controller.lhcdocContentList.value.isFav == 1)
//                               ? 0
//                               : 1;
//                           bool result = await controller.doFavorites(
//                               controller.lhcdocContentList.value.uid ?? "",
//                               favFlag);
//                           if (result) {
//                             controller.lhcdocContentList.value.isFav =
//                                 favFlag.toString();
//                           }
//                         },
//                         child: const Icon(FontAwesomeIcons.heart,
//                             color: Colors.white),
//                       ),
//                       if (controller.lhcdocContentList.value.favNum != null)
//                         Container(
//                           alignment: Alignment.topLeft,
//                           child: Text(
//                             '${controller.lhcdocContentList.value.favNum}',
//                             style: const TextStyle(color: Colors.white),
//                           ),
//                         ),
//                     ],
//                   )))
//             ])));
//   }

//   Widget _showMenu() {
//     return GestureDetector(
//         onTap: () {
//           _homeKey.currentState!.openEndDrawer();
//         },
//         child: Container(
//           padding: EdgeInsets.only(right: 10.w),
//           child:
//               AppImage.asset('menu_btn_white.png', width: 30.w, height: 30.w),
//         ));
//   }

//   void rewardPost() {
//     final random = Random();
//     double p = 3 + random.nextDouble() * 17;
//     _textEditingController.text = p.toStringAsFixed(2);
//     showDialog(
//         context: context,
//         barrierDismissible: false,
//         builder: (BuildContext context) {
//           return Dialog(
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(Radius.circular(10.w))),
//             child: SizedBox(
//                 height: 350.h,
//                 child: Stack(
//                   children: [
//                     Column(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Container(
//                             height: 60.h,
//                             width: double.infinity,
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.only(
//                                   topLeft: Radius.circular(10.w),
//                                   topRight: Radius.circular(10.w)),
//                               color: AppColors.drawMenu,
//                             ),
//                             alignment: Alignment.center,
//                             child: Text(
//                               "帖子打赏",
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: 22.sp,
//                                   fontWeight: FontWeight.bold),
//                               textAlign: TextAlign.center,
//                             )),
//                         SizedBox(height: 20.h),
//                         (controller.lhcdocContentList.value.headImg != null &&
//                                 controller.lhcdocContentList.value.headImg !=
//                                     "")
//                             ? AppImage.network(
//                                 controller.lhcdocContentList.value.headImg ??
//                                     "",
//                                 width: 80.w,
//                                 height: 80.w)
//                             : SizedBox(height: 80.w),
//                         SizedBox(height: 20.h),
//                         Container(
//                             decoration: BoxDecoration(
//                                 border: Border.all(
//                                     color: AppColors.ff5C5869, width: 1),
//                                 borderRadius:
//                                     BorderRadius.all(Radius.circular(5.w))),
//                             padding: EdgeInsets.symmetric(
//                                 vertical: 5.h, horizontal: 15.w),
//                             width: 300.w,
//                             height: 50.h,
//                             child: IntrinsicHeight(
//                                 child: Row(
//                               mainAxisAlignment: MainAxisAlignment.center,
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: [
//                                 const Text("¥",
//                                     style:
//                                         TextStyle(fontWeight: FontWeight.bold)),
//                                 SizedBox(width: 10.w),
//                                 Expanded(
//                                   child: Center(
//                                       child: TextField(
//                                     controller: _textEditingController,
//                                     scrollController: _scrollController,
//                                     style: const TextStyle(
//                                         fontSize: 16,
//                                         fontWeight: FontWeight.bold),
//                                     maxLines: 1,
//                                     decoration: InputDecoration(
//                                       contentPadding: EdgeInsets.only(
//                                           left: 10.w,
//                                           right: 10.w,
//                                           bottom: 20.h),
//                                       border: InputBorder.none,
//                                     ),
//                                   )),
//                                 ),
//                                 const VerticalDivider(
//                                     thickness: 1, color: AppColors.ff5C5869),
//                                 SizedBox(width: 7.w),
//                                 GestureDetector(
//                                     onTap: () {
//                                       p = 3 + random.nextDouble() * 17;
//                                       _textEditingController.text =
//                                           p.toStringAsFixed(2);
//                                     },
//                                     child: const Icon(CustomIcons.dice_three,
//                                         color: AppColors.ff2089dc)),
//                               ],
//                             ))),
//                         SizedBox(height: 30.h),
//                         Container(
//                           padding: EdgeInsets.symmetric(
//                               vertical: 5.h, horizontal: 10.w),
//                           alignment: Alignment.center,
//                           child: ElevatedButton(
//                               onPressed: () async {
//                                 AppToast.show();
//                                 AppToast.showToast('mycontentDetailVieew');
//                                 // var data = await gameController.tipContent(
//                                 //     controller.lhcdocContentList.value.cid ??
//                                 //         "",
//                                 //     _textEditingController.text);
//                                 // AppToast.dismiss();
//                                 // if (data.code == 0) {
//                                 //   AppToast.showSuccess(msg: '打赏成功');
//                                 //   Navigator.of(context).pop();
//                                 // }
//                               },
//                               style: AppButtonStyles.elevatedStyle(
//                                   backgroundColor: AppColors.drawMenu,
//                                   foregroundColor: Colors.white,
//                                   fontSize: 20,
//                                   width: 400,
//                                   height: 30),
//                               child: const Text("立即支付")),
//                         )
//                       ],
//                     ),
//                     Positioned(
//                         top: 0,
//                         right: 5.w,
//                         child: IconButton(
//                             onPressed: () => Navigator.of(context).pop(),
//                             icon: const Icon(Icons.close, color: Colors.white)))
//                   ],
//                 )),
//           );
//         });
//   }
// }
