import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/data/models/LhcdocModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myranking_view_controller.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';

import 'package:get/get.dart';

class MyRankingView extends StatefulWidget {
  @override
  _MyRankingView createState() => _MyRankingView();
}

class _MyRankingView extends State<MyRankingView> {
  MyRankingViewController controller = Get.put(MyRankingViewController());

  @override
  Widget build(BuildContext context) {
    Map<int, TableColumnWidth> columnWidths = const {
      0: FlexColumnWidth(1.5),
      1: FlexColumnWidth(1.5),
      2: FlexColumnWidth(1.5),
    };
    return Scaffold(
      backgroundColor: AppColors.ffF5F5F5,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 28.w),
        ),
        title: Text('排行榜',
            style: AppTextStyles.ffffffff(context,
                fontSize: 24, fontWeight: FontWeight.bold)),
        centerTitle: true,
      ),
      body: Container(
        width: 1.sw,
        decoration: BoxDecoration(
          border: Border.all(
            color: AppColors.ffF5F5F5, // Border color and width
            // borderRadius: BorderRadius.circular(8.0), // Optional border radius
          ),
        ),
        child: Obx(
          () => Column(
            children: [
              SizedBox(
                height: 10.w,
              ),
              Container(
                  width: 0.9.sw,
                  height: 48.w,
                  decoration: BoxDecoration(
                    // border: Border.all(color: Colors.grey, width: 1.5),
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  child: Column(
                    children: [
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  controller.idx.value = 0;
                                },
                                child: Container(
                                  width: 0.4.sw,
                                  height: 48.w,
                                  decoration: BoxDecoration(
                                    color: (controller.idx.value == 1
                                        ? AppColors.ffCAC2C2
                                        : AppColors.ffbfa56d),
                                    borderRadius: BorderRadius.circular(4.0),
                                  ),
                                  child: Center(
                                    child: Text(
                                      '粉丝排行',
                                      style: controller.idx.value == 1
                                          ? AppTextStyles.ff000000(context,
                                              fontSize: 20)
                                          : AppTextStyles.ffffffff(context,
                                              fontSize: 20),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 30.w,
                              ),
                              GestureDetector(
                                onTap: () {
                                  controller.idx.value = 1;
                                },
                                child: Container(
                                  width: 0.4.sw,
                                  height: 48.w,
                                  decoration: BoxDecoration(
                                    color: (controller.idx.value == 0
                                        ? AppColors.ffCAC2C2
                                        : AppColors.ffbfa56d),
                                    borderRadius: BorderRadius.circular(4.0),
                                  ),
                                  child: Center(
                                    child: Text(
                                      '打赏排行',
                                      style: controller.idx.value == 0
                                          ? AppTextStyles.ff000000(context,
                                              fontSize: 20)
                                          : AppTextStyles.ffffffff(context,
                                              fontSize: 20),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ],
                  )),
              SizedBox(
                height: 30.w,
              ),
              unitTableRow(
                  context,
                  columnWidths,
                  ['排名', '名称', controller.idx.value == 0 ? '粉丝' : '打赏'],
                  AppColors.ffF5F5F5),
              SizedBox(
                width: 1.sw,
                height: 1.sp,
                child: const ColoredBox(
                  color: AppColors.ff9e9e9e,
                ),
              ),
              if (controller.fansRankingData.value.data != null &&
                  controller.idx.value == 0)
                ...List.generate(controller.fansRankingData.value.data!.length,
                    (index) {
                  return unitTableRow(
                      context,
                      columnWidths,
                      [
                        controller.fansRankingData.value.data!
                            .elementAt(index)
                            .id
                            .toString(),
                        controller.fansRankingData.value.data!
                                .elementAt(index)
                                .username ??
                            '',
                        controller.fansRankingData.value.data!
                            .elementAt(index)
                            .fansNum
                            .toString()
                      ],
                      AppColors.ff9e9e9e);
                }),
              if (controller.tipsRankingData.value.data != null &&
                  controller.idx.value == 1)
                ...List.generate(controller.tipsRankingData.value.data!.length,
                    (index) {
                  return unitTableRow(
                      context,
                      columnWidths,
                      [
                        controller.tipsRankingData.value.data!
                            .elementAt(index)
                            .id
                            .toString(),
                        controller.tipsRankingData.value.data!
                                .elementAt(index)
                                .username ??
                            '',
                        controller.tipsRankingData.value.data!
                            .elementAt(index)
                            .totalAmount
                            .toString()
                      ],
                      AppColors.ff9e9e9e);
                }),
            ],
          ),
        ),
      ),
    );
  }
}

Table unitTableRow(
    BuildContext context,
    Map<int, TableColumnWidth>? columnWidths,
    List<String> data,
    Color? lineColor) {
  return Table(
    columnWidths: columnWidths,
    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
    border:
        TableBorder.all(color: lineColor ?? AppColors.ff9e9e9e, width: 0.5.w),
    children: [
      TableRow(
        children: [
          ...List.generate(data.length, (index) {
            return Padding(
              padding: EdgeInsets.symmetric(vertical: 10.w),
              child: Text(
                data.elementAt(index),
                textAlign: TextAlign.center,
              ),
            );
          }),
        ],
      ),
    ],
  );
}

// Table(
//         columnWidths: const {
//           0: FlexColumnWidth(1.5),
//           1: FlexColumnWidth(1.5),
//           2: FlexColumnWidth(1.5),
//         },
//         defaultVerticalAlignment: TableCellVerticalAlignment.middle,
//         border: TableBorder.all(color: AppColors.ff9e9e9e),
//         children: [
//           TableRow(
//             children: [
//               Text(
//                 '玩法: ${item.playGroupName} ${item.playName} ${item.betInfo}',
//                 style: AppTextStyles.ff000000(context, fontSize: 18),
//               ),
//               Text(
//                 '开奖号码:${kjdata(item)}',
//                 style: AppTextStyles.ff000000(context, fontSize: 18),
//               ),
//               if (item.isAllowCancel && status != 4)
//                 Container(
//                   margin: EdgeInsets.only(left: 10.w),
//                   child: ElevatedButton(
//                     child: Text(
//                       '撤单',
//                       style: AppTextStyles.ffffffff(context, fontSize: 18),
//                     ),
//                     onPressed: () {
//                       ShowAlert(context,
//                           actionStr: '确定',
//                           titleStr: '温馨提示',
//                           item: item, onPressed: () {
//                         // String orderNo = item.id;
//                         // Perform cancellation API call
//                         // api.user.cancelBet(orderNo)
//                         //   .useSuccess(({ msg }) => {
//                         //     showSuccess(msg);
//                         //     loadWBData();
//                         //   });
//                         // Placeholder for API call simulation
//                         Navigator.pop(context);
//                       }).showAlert();
//                     },
//                     style: ElevatedButton.styleFrom(
//                       backgroundColor: AppColors.ff6f96a4,
//                       // padding: EdgeInsets.symmetric(vertical: 1.h),
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(1.w),
//                       ),
//                     ),
//                   ),
//                 ),
//             ],
//           ),

//           //renderItem()
//         ],
//       ),
