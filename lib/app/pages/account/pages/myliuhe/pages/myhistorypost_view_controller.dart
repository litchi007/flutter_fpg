import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

class MyHistoryPostViewController extends GetxController {
  RxString avatar = ''.obs;
  RxInt idx = 0.obs;

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    avatar.value = GlobalService.to.userInfo.value?.avatar ?? "";
  }
}
