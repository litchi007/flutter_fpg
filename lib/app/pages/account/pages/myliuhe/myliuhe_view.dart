import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/myliuhe_view_controller.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app_navigator.dart';

class MyLiuheView extends StatefulWidget {
  const MyLiuheView({super.key});

  @override
  State<MyLiuheView> createState() => _MyLiuheViewState();
}

class _MyLiuheViewState extends State<MyLiuheView> {
  final List<List<String>> icons = [
    ['38', 'mylh_3', '优惠活动'],
    ['38', 'mylh_2', '推荐收益'],
    ['38', 'mylh_1', '个人信息'],
    ['38', 'pindao_1', '我的粉丝'],
    ['38', 'pindao_2', '我的动态'],
    ['38', 'pindao_3', '我的关注'],
    ['38', 'pindao_4', '排行榜'],
    ['38', 'pindao_5', '历史帖子'],
    ['38', 'wdfw_1', '专属客服'],
    ['38', 'wdfw_2', '站内消息'],
    ['38', 'wdfw_3', '意见反馈'],
  ];
  final MyLiuheViewController myliuheController =
      Get.put(MyLiuheViewController());

  final List<String> toolnames = ["彩票", "真人", "棋牌", '电子', '体育'];
  String name = 'miya01';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.ffC5CBC6,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 28.w),
          ),
          title: Text('我的六合',
              style: AppTextStyles.ffffffff(context, fontSize: 24)),
          centerTitle: true,
        ),
        body: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Column(
              children: [
                Container(color: Colors.white, height: 2),
                InforWidget(),
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(top: 80),
                  child: ServerTools(
                    title: '必备工具',
                    icon: icons,
                    usize: 50.w,
                    start: 0,
                    count: 3,
                    pagePath: const [promotionListPath, myrecoPath, myInfoPath],
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                ServerTools(
                  title: '我的频道',
                  icon: icons,
                  usize: 40.w,
                  start: 3,
                  count: 5,
                  pagePath: const [
                    fansPath,
                    focusPath,
                    followPath,
                    // AppNavigator.toNamed(followListPath, arguments: [uid, '0']);

                    lhcBoardPath,
                    historyForumPath,
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                ServerTools(
                  title: '我的服务',
                  icon: icons,
                  usize: 40.w,
                  start: 8,
                  count: 3,
                  pagePath: const [
                    onlineServicePath,
                    userMessagePath,
                    feedbackPath,
                  ],
                ),
                Expanded(
                    child: Container(
                  color: Colors.white,
                ))
              ],
            ),
            Positioned(
              top: 160.w,
              child: Column(
                children: [
                  Assetwidget(),
                  SizedBox(height: 10.w),
                  SizedBox(height: 10.w),
                ],
              ),
            ),
          ],
        ));
  }
}

Widget insertLine() {
  return SizedBox(
    width: 1.sw,
    height: 2.w,
    child: const ColoredBox(color: AppColors.ff8D8B8B),
  );
}

class InforWidget extends StatelessWidget {
  InforWidget({super.key});
  final MyLiuheViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1.sw,
      height: 200.w,
      // margin: EdgeInsets.only(top: 2.w),
      decoration: const BoxDecoration(
        color: AppColors.ffbfa46d,
      ),
      child: Column(
        children: [
          SizedBox(
            height: 10.w,
          ),
          Obx(
            () => Row(
              children: [
                SizedBox(width: 20.w),
                CircleAvatar(
                  backgroundImage: NetworkImage(
                    controller.avatar.value ?? '',
                  ),
                  radius: 37.5.w,
                ),
                SizedBox(
                  width: 20.w,
                ),
                Text(
                  controller.infoUsr.value,
                  style: AppTextStyles.ffffffff(context, fontSize: 24),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30.w,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                '会员等级:VIP1',
                style: AppTextStyles.fffcc67e(context, fontSize: 20),
              ),
              GestureDetector(
                onTap: () {
                  if (!GlobalService.isShowTestToast) {
                    Get.toNamed(taskPath, arguments: [3]);
                  }
                },
                child: Container(
                  width: 90.w,
                  padding: EdgeInsets.all(5.w),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40.w),
                    color: AppColors.fffcc67e,
                  ),
                  child: Text(
                    '查看详情',
                    style: AppTextStyles.ff5C5869_(context, fontSize: 18),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Assetwidget extends StatelessWidget {
  Assetwidget({super.key});
  final MyLiuheViewController controller = Get.find();

  String myassets = '我的资产';
  String accumulatedbonus = '累计彩金';
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 0.98.sw,
      height: 120.w,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(
          color: AppColors.ffb9ae9e,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(10).w,
        color: AppColors.background,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                myassets,
                style: AppTextStyles.ff000000(context,
                    fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Obx(
                () => Text(
                  controller.infoBalance.value,
                  style: AppTextStyles.ffA50606_(context),
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                accumulatedbonus,
                style: AppTextStyles.ff000000_(context,
                    fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Obx(
                () => Text(
                  controller.infoAmountsum.value,
                  style: AppTextStyles.ffA50606_(context),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class ServerTools extends StatelessWidget {
  ServerTools({
    super.key,
    required this.title,
    required this.icon,
    required this.usize,
    required this.start,
    required this.count,
    required this.pagePath,
  });
  String title = '我的资产';
  int start = 0;
  int count = 1;
  List<List<String>> icon;
  final double usize;
  List<String> pagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 134.h,
      padding: EdgeInsets.all(5.w),
      color: AppColors.background,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              height: 30.w,
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: AppTextStyles.ff000000_(context, fontSize: 20),
              )),
          const SizedBox(
            height: 20.0,
          ),
          SizedBox(
            height: 100.w,
            child: ListView(scrollDirection: Axis.horizontal, children: [
              ...List.generate(count, (index) {
                return GestureDetector(
                  onTap: () {
                    String routeName = pagePath[index];
                    if (routeName == promotionListPath) {
                      Get.toNamed(routeName, arguments: 'fromMyLiuhe');
                    } else if (routeName == followPath) {
                      AppNavigator.toNamed(followPath, arguments: [
                        GlobalService.to.userInfo.value?.uid ?? '0',
                        '0'
                      ]);
                    } else if (routeName == historyForumPath) {
                      AppNavigator.toNamed(historyForumPath, arguments: [
                        GlobalService.to.userInfo.value?.uid ?? '0'
                      ]);
                    } else {
                      /// 个人信息 和 意见反馈 试玩账号没有权限进入
                      if ((routeName == myInfoPath ||
                              routeName == feedbackPath) &&
                          GlobalService.isShowTestToast) {
                        return;
                      }

                      Get.toNamed(
                        routeName,
                      );
                    }
                  },
                  child: SizedBox(
                    width: 100.w,
                    height: 100.w,
                    child: Column(
                      children: [
                        AppImage.network(
                            img_mobileTemplate(
                              icon.elementAt(index + start).elementAt(0),
                              icon.elementAt(index + start).elementAt(1),
                            ),
                            width: usize,
                            height: usize,
                            fit: BoxFit.fill),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10,
                          ).w,
                          child: Text(
                              icon.elementAt(index + start).elementAt(2),
                              style: AppTextStyles.ff000000_(context,
                                  fontSize: 16)),
                        ),
                      ],
                    ),
                  ),
                );
              }),
            ]),
          ),
        ],
      ),
    );
  }
}
