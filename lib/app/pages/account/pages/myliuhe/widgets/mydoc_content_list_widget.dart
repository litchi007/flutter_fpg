import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/common_util.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class MydocContentListWidget extends StatefulWidget {
  MydocContentListWidget({super.key, required this.item});

  LhcdocContentList item;

  @override
  State<MydocContentListWidget> createState() => _MydocContentListWidgetState();
}

class _MydocContentListWidgetState extends State<MydocContentListWidget> {
  // final GameCenterController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    Color bgColor = Colors.transparent;
    if (widget.item.inListBackgroundColor!.isNotEmpty) {
      bgColor =
          CommonUtil.hexStringToColor(widget.item.inListBackgroundColor ?? "");
    }
    String createdTime = widget.item.createTime ?? "";
    if (createdTime != "") createdTime = DateUtil.MMddhhmm(createdTime);
    return GestureDetector(
        onTap: () {
          if (widget.item.hyperlink != null && widget.item.hyperlink != "") {
            // AppNavigator.toNamed(webAppPage, arguments: [widget.item.hyperlink, widget.item.title]);
            launchUrl(Uri.parse(widget.item.hyperlink ?? ""));
          } else {
            // controller.getContentDetail(widget.item.cid ?? "");
            // AppNavigator.toNamed(mydocContentDetailPath,
            //     arguments: ['', widget.item.alias]);
            AppNavigator.toNamed(postDetailPath, arguments: {
              'postId': widget.item.cid ?? "",
              'title': widget.item.title,
              'category': widget.item.alias,
              'from': ''
            });
          }
        },
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5.w)),
            ),
            padding: EdgeInsets.all(3.w),
            child: Column(children: [
              Container(
                color: bgColor,
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (widget.item.isHot == "1")
                        AppImage.asset("jing.png", width: 20.w, height: 20.w),
                      SizedBox(width: 4.w),
                      if (widget.item.isHot == "0")
                        AppImage.asset("jing2.png", width: 20.w, height: 20.w),
                      SizedBox(width: 4.w),
                      Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(horizontal: 3.w),
                          decoration: BoxDecoration(
                            color: AppColors.drawMenu,
                            borderRadius:
                                BorderRadius.all(Radius.circular(3.w)),
                          ),
                          child: Text(widget.item.periods ?? "",
                              style: const TextStyle(color: Colors.white))),
                      SizedBox(width: 4.w),
                      Expanded(
                          child: Text(
                        widget.item.title ?? "",
                        style: TextStyle(color: Colors.black, fontSize: 19.sp),
                        overflow: TextOverflow.ellipsis,
                      ))
                    ]),
              ),
              SizedBox(height: 5.h),
              IntrinsicHeight(
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                    SizedBox(width: 20.w),
                    AppImage.network(widget.item.headImg ?? "",
                        width: 30.w, height: 30.w),
                    SizedBox(width: 4.w),
                    Text(widget.item.nickname ?? "",
                        style: TextStyle(color: Colors.red, fontSize: 18.sp)),
                    SizedBox(width: 10.w),
                    VerticalDivider(
                      thickness: 1.w,
                      color: Colors.black,
                      indent: 4.h,
                      endIndent: 4.h,
                    ),
                    Expanded(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                          Text(createdTime,
                              style: TextStyle(
                                  color: AppColors.drawMenu, fontSize: 16.sp)),
                          SizedBox(width: 10.w),
                          Icon(CustomIcons.thumbs_up,
                              color: AppColors.drawMenu, size: 20.w),
                          SizedBox(width: 3.w),
                          Text(widget.item.likeNum ?? "",
                              style: TextStyle(
                                  color: AppColors.drawMenu, fontSize: 16.sp)),
                          SizedBox(width: 10.w),
                          Icon(CustomIcons.eye,
                              color: AppColors.drawMenu, size: 20.w),
                          SizedBox(width: 3.w),
                          Text(widget.item.viewNum ?? "",
                              style: TextStyle(
                                  color: AppColors.drawMenu, fontSize: 16.sp)),
                          SizedBox(width: 10.w),
                          Icon(CustomIcons.commenting_o,
                              color: AppColors.drawMenu, size: 20.w),
                          SizedBox(width: 3.w),
                          Text(widget.item.replyCount.toString(),
                              style: TextStyle(
                                  color: AppColors.drawMenu, fontSize: 16.sp)),
                        ]))
                  ])),
              SizedBox(height: 5.h),
              Container(height: 1, color: Colors.grey),
            ])));
  }
}
