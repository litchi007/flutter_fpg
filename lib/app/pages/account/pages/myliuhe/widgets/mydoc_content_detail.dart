// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_switch/flutter_switch.dart';
// import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
// import 'package:fpg_flutter/app/component/live_lottery/live_lottery_controller.dart';
// import 'package:fpg_flutter/app/component/live_lottery/lottery_ball.dart';
// import 'package:fpg_flutter/app/controllers/auth_controller.dart';
// import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
// import 'package:fpg_flutter/app/resource/icon_resource.dart';
// import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
// import 'package:fpg_flutter/app/widgets/app_image.dart';
// import 'package:fpg_flutter/app/widgets/app_toast.dart';
// import 'package:fpg_flutter/app_navigator.dart';
// import 'package:fpg_flutter/data/models/LhcdocContentDetail.dart';
// import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
// import 'package:fpg_flutter/routes.dart';
// import 'package:fpg_flutter/services/global_service.dart';
// import 'package:fpg_flutter/utils/custom_icons_icons.dart';
// import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
// import 'package:fpg_flutter/utils/theme/app_colors.dart';
// import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
// import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
// import 'package:gap/gap.dart';
// import 'package:get/get.dart';
// import 'dart:math';

// class MydocContentDetailView extends StatefulWidget {
//   const MydocContentDetailView({super.key});

//   @override
//   State<MydocContentDetailView> createState() => _MydocContentDetailViewState();
// }

// class _MydocContentDetailViewState extends State<MydocContentDetailView>
//   with TickerProviderStateMixin {
// final GlobalKey<ScaffoldState> _homeKey = GlobalKey(); // Create a key
// AuthController authController = Get.find();
// GameCenterController controller = Get.find();
// LiveLotteryController lcontroller = Get.find();
// final _textEditingController = TextEditingController();
// final _scrollController = ScrollController();
// RxInt animalFlag = 0.obs;
// final RxBool _isLastLotteyCheck = false.obs;
// String from = "", category = "";

// @override
// void initState() {
//   // TODO: implement initState
//   List<String?> args = Get.arguments;
//   from = args[0] ?? "";
//   category = args[1] ?? "";
//   super.initState();
// }

// @override
// Widget build(BuildContext context) {
//   final appThemeColors = Theme.of(context).extension<AppThemeColors>();
//   return SizedBox();
//   return Obx(() => Scaffold(
//       key: _homeKey,
//       backgroundColor: appThemeColors?.surface,
//       appBar: AppGeneralBar(
//         backgroundColor: appThemeColors?.primary,
//         leading: GestureDetector(
//             onTap: () => Navigator.of(context).pop(),
//             child: Container(
//               padding: EdgeInsets.only(left: 10.w),
//               child: Row(children: [
//                 Icon(Icons.arrow_back_ios,
//                     color: AppColors.ffffffff, size: 18.w),
//                 Text('返回',
//                     style: AppTextStyles.ffffffff(context, fontSize: 18)),
//               ]),
//             )),
//         titleWidget: Center(
//           child: Text(
//             from == "forum"
//                 ? "论坛详情"
//                 : controller.contentDetail.value.title ?? "",
//             style: const TextStyle(
//               color: Colors.white,
//               fontWeight: FontWeight.bold,
//             ),
//             overflow: TextOverflow.ellipsis,
//           ),
//         ),
//         actionWidgets: [
//           GlobalService.to.isAuthenticated.value ? _showMenu() : Container()
//         ],
//       ),
//       endDrawer: GlobalService.to.isAuthenticated.value
//           ? Container(
//               color: Colors.white,
//               width: Get.width * 0.5,
//               height: Get.height,
//               child: const HomeMenuWidget(),
//             )
//           : null,
//       body: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             if (controller.contentDetail.value.title == null)
//               Center(
//                   child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                     AppImage.asset("pl.png", width: 400.w),
//                     SizedBox(height: 20.h),
//                     Text("正在加载, 请稍等...", style: TextStyle(fontSize: 20.sp))
//                   ])),
//             if (controller.contentDetail.value.title != null &&
//                 controller.contentDetail.value.alias != 'rule' &&
//                 controller.contentDetail.value.alias != 'mystery')
//               Container(
//                 decoration: BoxDecoration(
//                     border: Border(
//                         bottom: BorderSide(color: Colors.grey, width: 1.h))),
//                 padding:
//                     EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
//                 child: Row(
//                   children: [
//                     if (controller.contentDetail.value.headImg == null ||
//                         controller.contentDetail.value.headImg == "")
//                       Container(
//                         width: 80.w,
//                         height: 80.w,
//                         decoration: const BoxDecoration(
//                             shape: BoxShape.circle,
//                             color: Color.fromARGB(255, 221, 221, 221)),
//                       ),
//                     if (controller.contentDetail.value.headImg != "")
//                       GestureDetector(
//                         onTap: () {
//                           AppNavigator.toNamed(userProfilePath, parameters: {
//                             'uid': controller.contentDetail.value.uid ?? ""
//                           });
//                         },
//                         child: AppImage.network(
//                             controller.contentDetail.value.headImg ?? "",
//                             width: 90.w,
//                             height: 90.w),
//                       ),
//                     Expanded(
//                         child: Column(
//                             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                           Text(controller.contentDetail.value.nickname ?? "",
//                               style: TextStyle(
//                                   color: Colors.black,
//                                   fontSize: 22.sp,
//                                   fontWeight: FontWeight.bold)),
//                           Text(
//                             "楼主 ${controller.contentDetail.value.createTime}",
//                             style: TextStyle(
//                                 color: AppColors.ff5C5869, fontSize: 18.sp),
//                             overflow: TextOverflow.ellipsis,
//                           )
//                         ])),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         GestureDetector(
//                           onTap: rewardPost,
//                           child: AppImage.asset("redBag_1.png", width: 60.w),
//                         ),
//                         SizedBox(width: 10.w),
//                         Column(
//                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           children: [
//                             ElevatedButton(
//                                 onPressed: () async {
//                                   if (checkIsTest()) return;
//                                   if (!GlobalService
//                                       .to.isAuthenticated.value) {
//                                     AppNavigator.toNamed(loginPath);
//                                   }
//                                   int followFlag = (controller.contentDetail
//                                                   .value.isFollow !=
//                                               null &&
//                                           controller.contentDetail.value
//                                                   .isFollow ==
//                                               1)
//                                       ? 0
//                                       : 1;
//                                   bool result = await controller.followPoster(
//                                       controller.contentDetail.value.uid ??
//                                           "",
//                                       followFlag);
//                                   if (result) {
//                                     await controller.updateContentDetail(
//                                         controller.contentDetail.value.id ??
//                                             "");
//                                   }
//                                 },
//                                 style: AppButtonStyles.elevatedStyle(
//                                     fontSize: 18,
//                                     backgroundColor: AppColors.drawMenu,
//                                     foregroundColor: Colors.white,
//                                     width: 100,
//                                     height: 40,
//                                     padding: EdgeInsets.symmetric(
//                                         vertical: 5.w, horizontal: 10.w)),
//                                 child: Obx(() => Text((controller
//                                                 .contentDetail
//                                                 .value
//                                                 .isFollow !=
//                                             null &&
//                                         controller.contentDetail.value
//                                                 .isFollow ==
//                                             1)
//                                     ? "取消关注"
//                                     : "关注楼主"))),
//                             ElevatedButton(
//                                 onPressed: () {
//                                   Get.back();
//                                 },
//                                 style: AppButtonStyles.elevatedStyle(
//                                     fontSize: 18,
//                                     backgroundColor: AppColors.drawMenu,
//                                     foregroundColor: Colors.white,
//                                     width: 100,
//                                     height: 40,
//                                     padding: EdgeInsets.symmetric(
//                                         vertical: 5.w, horizontal: 10.w)),
//                                 child: const Text("历史帖子"))
//                           ],
//                         )
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//             if (controller.contentDetail.value.title != null)
//               Expanded(
//                   child: Padding(
//                       padding: EdgeInsets.symmetric(
//                           vertical: 10.h, horizontal: 30.w),
//                       child: SingleChildScrollView(
//                           child: Column(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: [
//                             if (controller.contentDetail.value.baomaId!
//                                     .isNotEmpty &&
//                                 category != 'tt8Yp69n' &&
//                                 from != 'forum')
//                               _getLHCWidget(),
//                             Gap(20.h),
//                             Text(
//                               controller.contentDetail.value.title ?? "",
//                               style: TextStyle(
//                                   color: Colors.black,
//                                   fontSize: 24.sp,
//                                   fontWeight: FontWeight.bold),
//                               textAlign: TextAlign.center,
//                             ),
//                             HtmlWidget(
//                                 controller.contentDetail.value.content ?? ""),
//                             ...List.generate(
//                                 controller.contentDetail.value.contentPic!
//                                     .length, (idx) {
//                               return AppImage.network(controller
//                                   .contentDetail.value.contentPic?[idx]);
//                             }),
//                             Gap(10.h),
//                             if (controller.contentDetail.value.vote != null)
//                               Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Text("第 ",
//                                         style: TextStyle(
//                                             color: Colors.black,
//                                             fontSize: 20.sp,
//                                             fontWeight: FontWeight.bold)),
//                                     Text(
//                                         controller.contentDetail.value
//                                                     .issue !=
//                                                 null
//                                             ? controller
//                                                 .contentDetail.value.issue!
//                                                 .replaceAll("2024", "")
//                                             : "",
//                                         style: TextStyle(
//                                             color: Colors.red,
//                                             fontSize: 20.sp,
//                                             fontWeight: FontWeight.bold)),
//                                     Text(" 期投票",
//                                         style: TextStyle(
//                                             color: Colors.black,
//                                             fontSize: 20.sp,
//                                             fontWeight: FontWeight.bold)),
//                                   ]),
//                             SizedBox(height: 10.h),
//                             if (controller.contentDetail.value.vote != null)
//                               GridView.count(
//                                   primary: false,
//                                   padding: EdgeInsets.all(2.w),
//                                   shrinkWrap: true,
//                                   crossAxisSpacing: 20.w,
//                                   mainAxisSpacing: 5.h,
//                                   crossAxisCount: 2,
//                                   childAspectRatio: 7,
//                                   children: [
//                                     if (controller.contentDetail.value.vote !=
//                                         null)
//                                       ...List.generate(
//                                           controller.contentDetail.value.vote!
//                                               .length, (index) {
//                                         Vote vote = controller
//                                             .contentDetail.value.vote![index];
//                                         return SizedBox(
//                                             height: 30.h,
//                                             child: Row(children: [
//                                               Text(vote.animal ?? "",
//                                                   style: TextStyle(
//                                                       color: Colors.black,
//                                                       fontSize: 16.sp)),
//                                               SizedBox(width: 5.w),
//                                               Expanded(
//                                                   child: Container(
//                                                       padding: EdgeInsets
//                                                           .symmetric(
//                                                               vertical: 2.h,
//                                                               horizontal:
//                                                                   3.w),
//                                                       decoration:
//                                                           BoxDecoration(
//                                                         borderRadius:
//                                                             BorderRadius.all(
//                                                                 Radius
//                                                                     .circular(
//                                                                         3.w)),
//                                                         color: vote.percent ==
//                                                                 "100.00"
//                                                             ? AppColors
//                                                                 .drawMenu
//                                                             : const Color
//                                                                 .fromARGB(
//                                                                 255,
//                                                                 231,
//                                                                 231,
//                                                                 231),
//                                                       ),
//                                                       alignment: Alignment
//                                                           .centerRight,
//                                                       child: Text(
//                                                           "${vote.percent.toString().replaceAll(".00", "")}%",
//                                                           style: TextStyle(
//                                                               color: AppColors
//                                                                   .ff5C5869,
//                                                               fontSize:
//                                                                   16.sp))))
//                                             ]));
//                                       })
//                                   ]),
//                             if (controller.contentDetail.value.vote != null)
//                               ElevatedButton(
//                                   onPressed: showVoteDialog,
//                                   style: AppButtonStyles.elevatedStyle(
//                                       fontSize: 20,
//                                       backgroundColor: AppColors.drawMenu,
//                                       foregroundColor: Colors.white,
//                                       width: 250,
//                                       height: 40,
//                                       padding: EdgeInsets.symmetric(
//                                           vertical: 5.w, horizontal: 10.w)),
//                                   child: const Text("投票")),
//                             Row(
//                                 mainAxisAlignment: MainAxisAlignment.start,
//                                 children: [
//                                   Text(
//                                       "最后更新时间：${controller.contentDetail.value.createTime}",
//                                       style: TextStyle(
//                                           color: Colors.grey,
//                                           fontSize: 18.sp))
//                                 ]),
//                             Gap(10.h),
//                             Row(
//                                 mainAxisAlignment: MainAxisAlignment.start,
//                                 children: [
//                                   Container(
//                                     decoration: const BoxDecoration(
//                                         border: Border(
//                                             bottom: BorderSide(
//                                                 width: 1,
//                                                 color: Colors.black))),
//                                     padding: EdgeInsets.only(bottom: 10.h),
//                                     child: Text("全部评论",
//                                         style: TextStyle(
//                                             color: Colors.black,
//                                             fontSize: 18.sp,
//                                             fontWeight: FontWeight.bold)),
//                                   )
//                                 ]),
//                             // if(controller.contentDetail.value.)
//                             AppImage.asset("pl.png", width: 400.w),
//                             Text("还没有评论，你的机会来了！"),
//                             Text("没有更多了"),
//                           ])))),
//             if (controller.contentDetail.value.title != null)
//               Container(
//                   color: AppColors.drawMenu,
//                   padding:
//                       EdgeInsets.symmetric(vertical: 5.h, horizontal: 10.w),
//                   child: IntrinsicHeight(
//                       child: Row(
//                     children: [
//                       AppImage.asset("yhj.png", width: 50.w),
//                       SizedBox(width: 5.w),
//                       Expanded(
//                           child: GestureDetector(
//                         onTap: () {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                           }
//                           AppNavigator.toNamed(postConentReply);
//                         },
//                         child: Container(
//                             height: 40.h,
//                             padding: EdgeInsets.symmetric(horizontal: 20.w),
//                             alignment: Alignment.centerLeft,
//                             decoration: BoxDecoration(
//                                 borderRadius:
//                                     BorderRadius.all(Radius.circular(20.w)),
//                                 color: Colors.white),
//                             child: const Text(
//                               '说点什么...',
//                               style: TextStyle(color: Colors.grey),
//                             )),
//                       )),
//                       SizedBox(width: 5.w),
//                       GestureDetector(
//                         onTap: () async {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                             return;
//                           }
//                           int likeFlag = (controller
//                                           .contentDetail.value.isLike !=
//                                       null &&
//                                   controller.contentDetail.value.isLike == 1)
//                               ? 0
//                               : 1;
//                           bool result = await controller.likePost(
//                               controller.contentDetail.value.id ?? "",
//                               likeFlag);
//                           if (result) {
//                             await controller.updateContentDetail(
//                                 controller.contentDetail.value.id ?? "");
//                           }
//                         },
//                         child: Icon(CustomIcons.thumbs_up,
//                             color: (controller.contentDetail.value.isLike !=
//                                         null &&
//                                     controller.contentDetail.value.isLike ==
//                                         1)
//                                 ? Colors.red
//                                 : Colors.white),
//                       ),
//                       if (controller.contentDetail.value.likeNum != null)
//                         Container(
//                           alignment: Alignment.topLeft,
//                           child: Text(
//                             controller.contentDetail.value.likeNum ?? "",
//                             style: const TextStyle(color: Colors.white),
//                           ),
//                         ),
//                       Gap(10.w),
//                       GestureDetector(
//                         onTap: () {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                           }
//                           AppNavigator.toNamed(postConentReply);
//                         },
//                         child: const Icon(CustomIcons.commenting_o,
//                             color: Colors.white),
//                       ),
//                       if (controller.contentDetail.value.replyCount != null)
//                         Container(
//                           alignment: Alignment.topLeft,
//                           child: Text(
//                             '${controller.contentDetail.value.replyCount ?? 0}',
//                             style: const TextStyle(color: Colors.white),
//                           ),
//                         ),
//                       Gap(10.w),
//                       GestureDetector(
//                         onTap: () async {
//                           if (checkIsTest()) return;
//                           if (!GlobalService.to.isAuthenticated.value) {
//                             AppNavigator.toNamed(loginPath);
//                           }
//                           int favFlag =
//                               (controller.contentDetail.value.isFav != null &&
//                                       controller.contentDetail.value.isFav ==
//                                           1)
//                                   ? 0
//                                   : 1;
//                           bool result = await controller.doFavorites(
//                               controller.contentDetail.value.id ?? "",
//                               favFlag);
//                           if (result) {
//                             await controller.updateContentDetail(
//                                 controller.contentDetail.value.id ?? "");
//                           }
//                         },
//                         child: Icon(FontAwesomeIcons.heart,
//                             color: (controller.contentDetail.value.isFav !=
//                                         null &&
//                                     controller.contentDetail.value.isFav == 1)
//                                 ? Colors.red
//                                 : Colors.white),
//                       ),
//                       if (controller.contentDetail.value.favNum != null)
//                         Container(
//                           alignment: Alignment.topLeft,
//                           child: Text(
//                             controller.contentDetail.value.favNum.toString(),
//                             style: const TextStyle(color: Colors.white),
//                           ),
//                         ),
//                     ],
//                   )))
//           ])));
// }

// Widget _showMenu() {
//   return GestureDetector(
//       onTap: () {
//         _homeKey.currentState!.openEndDrawer();
//       },
//       child: Container(
//         padding: EdgeInsets.only(right: 10.w),
//         child:
//             AppImage.asset('menu_btn_white.png', width: 30.w, height: 30.w),
//       ));
// }

// void rewardPost() {
//   final random = Random();
//   double p = 3 + random.nextDouble() * 17;
//   _textEditingController.text = p.toStringAsFixed(2);
//   showDialog(
//       context: context,
//       barrierDismissible: false,
//       builder: (BuildContext context) {
//         return Dialog(
//           shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.all(Radius.circular(10.w))),
//           child: SizedBox(
//               height: 350.h,
//               child: Stack(
//                 children: [
//                   Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       Container(
//                           height: 60.h,
//                           width: double.infinity,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.only(
//                                 topLeft: Radius.circular(10.w),
//                                 topRight: Radius.circular(10.w)),
//                             color: AppColors.drawMenu,
//                           ),
//                           alignment: Alignment.center,
//                           child: Text(
//                             "帖子打赏",
//                             style: TextStyle(
//                                 color: Colors.white,
//                                 fontSize: 22.sp,
//                                 fontWeight: FontWeight.bold),
//                             textAlign: TextAlign.center,
//                           )),
//                       SizedBox(height: 20.h),
//                       (controller.contentDetail.value.headImg != null &&
//                               controller.contentDetail.value.headImg != "")
//                           ? AppImage.network(
//                               controller.contentDetail.value.headImg ?? "",
//                               width: 80.w,
//                               height: 80.w)
//                           : SizedBox(height: 80.w),
//                       SizedBox(height: 20.h),
//                       Container(
//                           decoration: BoxDecoration(
//                               border: Border.all(
//                                   color: AppColors.ff5C5869, width: 1),
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(5.w))),
//                           padding: EdgeInsets.symmetric(
//                               vertical: 5.h, horizontal: 15.w),
//                           width: 300.w,
//                           height: 50.h,
//                           child: IntrinsicHeight(
//                               child: Row(
//                             mainAxisAlignment: MainAxisAlignment.center,
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             children: [
//                               const Text("¥",
//                                   style:
//                                       TextStyle(fontWeight: FontWeight.bold)),
//                               SizedBox(width: 10.w),
//                               Expanded(
//                                 child: Center(
//                                     child: TextField(
//                                   controller: _textEditingController,
//                                   scrollController: _scrollController,
//                                   style: const TextStyle(
//                                       fontSize: 16,
//                                       fontWeight: FontWeight.bold),
//                                   maxLines: 1,
//                                   decoration: InputDecoration(
//                                     contentPadding: EdgeInsets.only(
//                                         left: 10.w,
//                                         right: 10.w,
//                                         bottom: 20.h),
//                                     border: InputBorder.none,
//                                   ),
//                                 )),
//                               ),
//                               const VerticalDivider(
//                                   thickness: 1, color: AppColors.ff5C5869),
//                               SizedBox(width: 7.w),
//                               GestureDetector(
//                                   onTap: () {
//                                     p = 3 + random.nextDouble() * 17;
//                                     _textEditingController.text =
//                                         p.toStringAsFixed(2);
//                                   },
//                                   child: const Icon(CustomIcons.dice_three,
//                                       color: AppColors.ff2089dc)),
//                             ],
//                           ))),
//                       SizedBox(height: 30.h),
//                       Container(
//                         padding: EdgeInsets.symmetric(
//                             vertical: 5.h, horizontal: 10.w),
//                         alignment: Alignment.center,
//                         child: ElevatedButton(
//                             onPressed: () async {
//                               AppToast.show();
//                               var data = await controller.tipContent(
//                                   controller.contentDetail.value.id ?? "",
//                                   _textEditingController.text);
//                               AppToast.dismiss();
//                               if (data.code == 0) {
//                                 AppToast.showSuccess(msg: '打赏成功');
//                                 Navigator.of(context).pop();
//                               }
//                             },
//                             style: AppButtonStyles.elevatedStyle(
//                                 backgroundColor: AppColors.drawMenu,
//                                 foregroundColor: Colors.white,
//                                 fontSize: 20,
//                                 width: 400,
//                                 height: 30),
//                             child: const Text("立即支付")),
//                       )
//                     ],
//                   ),
//                   Positioned(
//                       top: 0,
//                       right: 5.w,
//                       child: IconButton(
//                           onPressed: () => Navigator.of(context).pop(),
//                           icon: const Icon(Icons.close, color: Colors.white)))
//                 ],
//               )),
//         );
//       });
// }

// void showVoteDialog() {
//   animalFlag.value = 0;
//   showDialog(
//       context: context,
//       barrierDismissible: false,
//       builder: (BuildContext context) {
//         return Dialog(
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(Radius.circular(10.w))),
//             child: Obx(() => Container(
//                 padding: EdgeInsets.all(10.w),
//                 height: 350.h,
//                 child: Column(
//                   children: [
//                     GridView.count(
//                         primary: false,
//                         padding: EdgeInsets.all(5.w),
//                         shrinkWrap: true,
//                         crossAxisSpacing: 20.w,
//                         mainAxisSpacing: 20.h,
//                         crossAxisCount: 3,
//                         childAspectRatio: 3,
//                         children: [
//                           if (controller.contentDetail.value.vote != null)
//                             ...List.generate(
//                                 controller.contentDetail.value.vote!.length,
//                                 (index) {
//                               Vote vote =
//                                   controller.contentDetail.value.vote![index];
//                               return GestureDetector(
//                                   onTap: () =>
//                                       animalFlag.value = vote.animalFlag ?? 0,
//                                   child: Container(
//                                       height: 50.h,
//                                       alignment: Alignment.center,
//                                       decoration: BoxDecoration(
//                                           border: Border.all(
//                                               color: vote.animalFlag ==
//                                                       animalFlag.value
//                                                   ? Colors.red
//                                                   : Colors.grey,
//                                               width: 2.w),
//                                           borderRadius: BorderRadius.all(
//                                               Radius.circular(10.w))),
//                                       child: Row(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.center,
//                                           children: [
//                                             Text(vote.animal ?? "",
//                                                 style: TextStyle(
//                                                     color: Colors.red,
//                                                     fontSize: 16.sp)),
//                                             SizedBox(width: 5.w),
//                                             Text(
//                                                 "${vote.percent.toString().replaceAll(".00", "")}%",
//                                                 style: TextStyle(
//                                                     color: Colors.black,
//                                                     fontSize: 16.sp)),
//                                           ])));
//                             })
//                         ]),
//                     Gap(
//                       20.h,
//                     ),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: [
//                         ElevatedButton(
//                             onPressed: () => Navigator.of(context).pop(),
//                             style: ElevatedButton.styleFrom(
//                                 shape: RoundedRectangleBorder(
//                                     side: BorderSide(
//                                         color: Colors.grey, width: 2.w),
//                                     borderRadius:
//                                         BorderRadius.circular(10.w)),
//                                 backgroundColor: Colors.white,
//                                 foregroundColor: Colors.black,
//                                 textStyle: TextStyle(fontSize: 20.sp),
//                                 minimumSize: Size(200.w, 50.w)),
//                             child: const Text("取消")),
//                         ElevatedButton(
//                             onPressed: animalFlag.value == 0
//                                 ? null
//                                 : () {
//                                     controller
//                                         .vote(
//                                             controller
//                                                     .contentDetail.value.id ??
//                                                 "",
//                                             animalFlag.value)
//                                         .then((data) {
//                                       if (data.code == 0) {
//                                         AppToast.showSuccess(msg: data.msg);
//                                         Navigator.of(context).pop();
//                                       } else {
//                                         AppToast.showError(msg: data.msg);
//                                       }
//                                     });
//                                   },
//                             style: ElevatedButton.styleFrom(
//                                 shape: RoundedRectangleBorder(
//                                     side: BorderSide(
//                                         color: AppColors.drawMenu,
//                                         width: 2.w),
//                                     borderRadius:
//                                         BorderRadius.circular(10.w)),
//                                 backgroundColor: AppColors.drawMenu,
//                                 foregroundColor: Colors.white,
//                                 textStyle: TextStyle(fontSize: 20.sp),
//                                 minimumSize: Size(200.w, 50.w)),
//                             child: const Text("确定"))
//                       ],
//                     )
//                   ],
//                 ))));
//       });
// }

// Widget _getLHCWidget() {
//   return Container(
//       decoration: const BoxDecoration(
//         image: DecorationImage(
//           image: AssetImage('assets/images/icon_border.png'),
//           fit: BoxFit.fill, // Makes sure the image covers the whole area
//         ),
//       ),
//       padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
//       child: Column(children: [
//         Row(
//           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           children: [
//             GestureDetector(
//               onTap: () => AppNavigator.toNamed(lotteryHistoryPath,
//                   arguments: [lcontroller.lotteryNumber.value.gameId]),
//               child: Text(
//                 '历史记录>',
//                 style: TextStyle(
//                     color: const Color(0xff666666), fontSize: 15.sp),
//               ),
//             ),
//             Row(
//               children: [
//                 AppImage.network(IconResource.sound01(),
//                     width: 21.w, height: 21.w),
//                 SizedBox(width: 10.w),
//                 Row(children: [
//                   Text('第',
//                       style: TextStyle(
//                           fontSize: 20.sp,
//                           color: const Color(0xff333333),
//                           fontWeight: FontWeight.bold)),
//                   Obx(
//                     () => Text(
//                         getLotteryNoOrIssueSubstring(
//                             lcontroller.lotteryNumber.value),
//                         style: TextStyle(
//                             fontSize: 20.sp,
//                             color: Colors.red,
//                             fontWeight: FontWeight.bold)),
//                   ),
//                   Text('期开奖结果',
//                       style: TextStyle(
//                           fontSize: 20.sp,
//                           color: const Color(0xff333333),
//                           fontWeight: FontWeight.bold)),
//                 ]),
//               ],
//             ),
//             Row(children: [
//               Text('咪',
//                   style: TextStyle(
//                       color: Colors.black,
//                       fontSize: 20.sp,
//                       fontWeight: FontWeight.bold)),
//               SizedBox(width: 10.w),
//               FlutterSwitch(
//                 width: 50.h,
//                 height: 30.h,
//                 value: _isLastLotteyCheck.value,
//                 activeColor: const Color(0xff11c1f3),
//                 onToggle: (bool value) => _isLastLotteyCheck.value = value,
//               )
//             ])
//           ],
//         ),
//         Padding(
//           padding: EdgeInsets.all(10.w),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               ...List.generate(lcontroller.showlotterys.length, (index) {
//                 final item = lcontroller.showlotterys[index];
//                 return LotteryBallWidget(
//                   score: item.number ?? '',
//                   color: item.color ?? '',
//                   text: item.sx ?? '',
//                   isLastItem: index == 6,
//                   isEnableLastItem: _isLastLotteyCheck.value,
//                   onChange: (value) {
//                     _isLastLotteyCheck.value = value;
//                   },
//                 );
//               }),
//             ],
//           ),
//         ),
//       ]));
// }

// String getLotteryNoOrIssueSubstring(LotteryNumberData lotteryNum) {
//   if (lotteryNum.lhcdocLotteryNo != null &&
//       lotteryNum.lhcdocLotteryNo!.isNotEmpty) {
//     return lotteryNum.lhcdocLotteryNo!;
//   } else if (lotteryNum.issue != null &&
//       (lotteryNum.issue ?? '').length >= 3) {
//     return (lotteryNum.issue ?? '')
//         .substring((lotteryNum.issue ?? '').length - 3);
//   } else {
//     return ''; // or any default value you prefer
//   }
// }
//   }
// }
