import 'package:flutter/material.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/lotteryhistory_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/basewidgets.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class JDNotDetailListCP extends StatefulWidget {
  const JDNotDetailListCP({super.key});

  @override
  State<JDNotDetailListCP> createState() => _JDNotDetailListCP();
}

class _JDNotDetailListCP extends State<JDNotDetailListCP>
    with TickerProviderStateMixin {
  LotteryHistoryViewController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      body: Container(
        // padding: EdgeInsets.all(20.w),

        color: AppColors.surface,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Table(
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  border: TableBorder.all(color: AppColors.ff9e9e9e),
                  // Allows to add a border decoration around your table
                  children: [
                    unitRow(context, ['时间', '笔数', '下注金额', '输赢'], 18),
                  ]),
              SizedBox(
                height: 10.h,
              ),
              Obx(
                () => (controller.isIniting.value == true)
                    ? const CustomLoadingWidget()
                    : Table(
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.middle,
                        border: TableBorder.all(color: AppColors.ff9e9e9e),
                        children: [
                          ...List.generate(
                            controller.lotteryData?.length ?? 0,
                            (index) {
                              UGBetsRecordModel temp;
                              temp = controller.lotteryData!.elementAt(index);

                              return unitRow(
                                  context,
                                  [
                                    '${temp.date ?? ''}\n${temp.dayOfWeek ?? ''}',
                                    temp.betCount ?? '',
                                    temp.betAmount ?? '',
                                    temp.winLoseAmount ?? ''
                                  ],
                                  18);
                            },
                          ),
                        ],
                      ),
              ),
              SizedBox(
                height: 10.w,
              ),
              // Text(
              //   '点击日期可查看下注详情',
              //   style: AppTextStyles.ff000000(context, fontSize: 16),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
