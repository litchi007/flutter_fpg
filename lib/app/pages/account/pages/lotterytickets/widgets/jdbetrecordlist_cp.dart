import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/lotteryhistory_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/widgets/panWidget.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';

class JDBetRecordListCP extends StatefulWidget {
  JDBetRecordListCP(
      {super.key,
      this.tabLabel,
      this.setkey,
      this.changeTabCount,
      this.gameType,
      this.status,
      this.startDate});
  String? tabLabel;
  String? setkey;
  int? changeTabCount;
  String? gameType;
  int? status;
  String? startDate;

  @override
  State<JDBetRecordListCP> createState() => _JDBetRecordListCP();
}

class _JDBetRecordListCP extends State<JDBetRecordListCP>
    with TickerProviderStateMixin {
  LotteryHistoryViewController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    Map<int, TableColumnWidth> columnWidths = const {
      0: FlexColumnWidth(1.5),
      1: FlexColumnWidth(1.5),
      2: FlexColumnWidth(1.5),
    };
    return Scaffold(
      backgroundColor: AppColors.surface,
      body: Container(
        // padding: EdgeInsets.all(20.w),
        color: AppColors.surface,
        child: SingleChildScrollView(
            child: Obx(
          () => (controller.isIniting.value == true)
              ? const CustomLoadingWidget()
              : Column(
                  children: [
                    ...List.generate(
                      controller.historyData!.length,
                      (index) {
                        // return Text('dddd');
                        return Obx(
                          () => panWidget(
                              context,
                              columnWidths,
                              controller.historyData?.elementAt(index) ?? '',
                              widget.status ?? 1,
                              widget.setkey ?? ''),
                        );
                      },
                    ),
                  ],
                ),
        )),
      ),
    );
  }
}
