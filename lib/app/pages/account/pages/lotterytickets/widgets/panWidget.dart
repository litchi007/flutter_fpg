import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/widgets/alertfrom_html.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/lotteryhistory_view_controller.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:get/get.dart';

Widget panWidget(BuildContext context, Map<int, TableColumnWidth>? columnWidths,
    dynamic item, int status, String someKey) {
  return Column(
    children: [
      SizedBox(
        height: 5.h,
      ),
      unitTableRow(context, columnWidths, [
        '彩种: ${item.gameName}',
        '注单单号: ${item.id}',
        '注单金额:${double.parse(item.betAmount.toString()).toStringAsFixed(2)}',
      ]),
      unitTableRow(context, columnWidths, [
        '期数: ${item.displayNumber.isEmpty ? item.issue : item.displayNumber}',
        '赔率: ${double.parse(item.odds.toString()).toStringAsFixed(2)}',
        '${yktitle(item)}${ykdata(item)}',
      ]),
      Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        border: TableBorder.all(color: AppColors.ff9e9e9e),
        children: [
          TableRow(
            children: [
              SizedBox(
                height: 100.w,
                child: Center(
                  child: Text(
                    '玩法: ${item.playGroupName} ${item.playName} ${item.betInfo}',
                    style: AppTextStyles.ff000000(context, fontSize: 18),
                  ),
                ),
              ),
              SizedBox(
                height: 100.w,
                child: Center(
                  child: Text(
                    '开奖号码:${kjdata(item)}',
                    style: AppTextStyles.ff000000(context, fontSize: 18),
                  ),
                ),
              ),
              if (item.isAllowCancel && status != 4)
                Container(
                  height: 100.w,
                  margin: EdgeInsets.only(left: 10.w),
                  child: ElevatedButton(
                    child: Text(
                      '撤单',
                      style: AppTextStyles.ffffffff(context, fontSize: 18),
                    ),
                    onPressed: () {
                      ShowAlert(context,
                          actionStr: '确定',
                          titleStr: '温馨提示',
                          item: item, onPressed: () {
                        // String orderNo = item.id;
                        // Perform cancellation API call
                        // api.user.cancelBet(orderNo)
                        //   .useSuccess(({ msg }) => {
                        //     showSuccess(msg);
                        //     loadWBData();
                        //   });
                        // Placeholder for API call simulation
                        Navigator.pop(context);
                      }).showAlert();
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.ff6f96a4,
                      // padding: EdgeInsets.symmetric(vertical: 1.h),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(1.w),
                      ),
                    ),
                  ),
                ),
            ],
          ),

          //renderItem()
        ],
      ),
    ],
  );
}

Table unitTableRow(BuildContext context,
    Map<int, TableColumnWidth>? columnWidths, List<String> data) {
  return Table(
    columnWidths: columnWidths,
    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
    border: TableBorder.all(color: AppColors.ff9e9e9e),
    children: [
      TableRow(
        children: [
          ...List.generate(data.length, (index) {
            return SizedBox(
              height: 100.w,
              child: Center(
                child: Text(
                  data.elementAt(index),
                ),
              ),
            );
          }),
        ],
      ),
    ],
  );
}

String yktitle(dynamic item) {
  String dataStr = '';

  if (item.status == '1') {
    dataStr = '可赢金额: ';
  } else if (item.status == '2' || item.status == '3') {
    // if (appConfig.isAmountOfMoney()) {
    //   dataStr = '结算金额: ';
    // } else if (appConfig.isPrizeOfMoney()) {
    //   dataStr = '派奖: ';
    // } else {
    //   dataStr = '盈亏: ';
    // }
  } else {
    dataStr = '盈亏: ';
  }

  return dataStr;
}

String ykdata(dynamic item) {
  String dataStr;

  String currencyLogo1 =
      currencyLogo[AppDefine.systemConfig?.currency ?? ''] as String;
  if (item.status == '1') {
    dataStr =
        '${(double.parse(item.expectAmount ?? '0') > 0 ? '+' : '')}${double.parse(item.settleAmount ?? '0').toStringAsFixed(2)}${currencyLogo1}';
  } else if (item.status == '4') {
    dataStr = '--';
  } else if (item.status == '2') {
    // if (appConfig.isAmountOfMoney()) {
    //   dataStr = item.winAmount + currencyLogo1;
    // } else {
    dataStr = (double.parse(item.settleAmount ?? '0') > 0 ? '+' : '') +
        double.parse(item.settleAmount ?? '0').toStringAsFixed(2) +
        currencyLogo1;
    // }
  } else {
    dataStr = (double.parse(item.settleAmount ?? '0') > 0 ? '+' : '') +
        double.parse(item.settleAmount ?? '0').toStringAsFixed(2) +
        currencyLogo1;
  }

  return dataStr;
}

String kjdata(dynamic item) {
  String dataStr;

  if (item.status == '1') {
    dataStr = '等待开奖';
  } else if (item.status == '4') {
    dataStr = '已撤单';
  } else {
    dataStr = item.lotteryNo ?? '';
  }

  return dataStr;
}

class ShowAlert extends BaseAlertWidget {
  ShowAlert(BuildContext context,
      {this.titleStr, this.actionStr, this.item, this.onPressed})
      : super(
            context: context,
            title: const SizedBox(),
            content: const SizedBox(),
            action: const SizedBox());
  dynamic item;
  String? titleStr;
  String? actionStr;
  VoidCallback? onPressed;
  LotteryHistoryViewController controller =
      Get.put(LotteryHistoryViewController());
  @override
  Widget build(BuildContext context) {
    context = context;
    return const SizedBox();
  }

  @override
  void showAlert() {
    title = titleWidget();
    content = contentWidget();
    action = actionWidget();
    titleColor = AppColors.surface;
    actionColor = AppColors.surface;
    super.dialogBuilder();
  }

  Widget titleWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 5.w, top: 15.w),
      child: Text(
        titleStr ?? '温馨提示',
        style: AppTextStyles.ff000000(
          context,
          fontSize: 25,
        ),
      ),
    );
  }

  Widget contentWidget() {
    return Text('您要撤销注单号${item.id}订单吗？');
  }

  Widget actionWidget() {
    return Row(
      children: [
        SizedBox(
          width: 0.45.sw,
        ),
        CustomButton(
          btnStr: actionStr ?? '确认', //'确认'
          width: 100,
          textColor: AppColors.ff4caf50,
          height: 30,
          onPressed: onPressed!,
        ),
      ],
    );
  }
}
