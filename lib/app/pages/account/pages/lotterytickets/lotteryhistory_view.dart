import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/widgets/jdbetrecordlist_cp.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/lotteryhistory_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/widgets/jdnotdetaillist_cp.dart';
import 'package:get/get.dart';

// LotteryHistoryComponent

class LotteryHistoryView extends StatefulWidget {
  const LotteryHistoryView({super.key});

  @override
  State<LotteryHistoryView> createState() => _LotteryHistoryView();
}

class _LotteryHistoryView extends State<LotteryHistoryView>
    with TickerProviderStateMixin {
  late TabController _tabController;
  LotteryHistoryViewController controller =
      Get.put(LotteryHistoryViewController());

  List<String> tabNames = [
    '注单统计',
    '待开奖',
    '已中奖',
    '未中奖',
    '已撤单',
    '已结单',
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: tabNames.length, vsync: this);
    _tabController.addListener(_handleTabChange);
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      if (_tabController.index == 0) {
        controller.initController();
      } else {
        controller.loadWBData(
          'lottery',
          _tabController.index,
        );
      }
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 18.w),
        ),
        title: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Text('彩票注单',
              style: AppTextStyles.ffffffff(context, fontSize: 25)),
        ),
        centerTitle: true,
      ),
      bottomNavigationBar: ColoredBox(
        color: AppColors.ffbfa46d,
        child: Padding(
          padding: EdgeInsets.all(5.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Obx(
                () => Text(
                  '总笔数:' + controller.totalBetCount.value,
                  style: AppTextStyles.ffffffff(
                    context,
                    fontSize: 18,
                  ),
                ),
              ),
              Obx(
                () => Text(
                  '总输赢:' + controller.totalWinAmount.value,
                  style: AppTextStyles.ffffffff(
                    context,
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: 1.sw,
              height: 1.sh,
              child: Column(
                children: [
                  TabBar(
                    controller: _tabController,
                    tabs: tabNames.map((item) => Tab(text: item)).toList(),
                    isScrollable: true,
                    tabAlignment: TabAlignment.start,
                    unselectedLabelColor: AppColors.ff9e9e9e,
                    labelColor: AppColors.ffbfa46d,
                    indicatorColor: AppColors.ffbfa46d,
                  ),
                  Expanded(
                    child: SizedBox(
                      width: 1.sw,
                      child: TabBarView(
                        controller: _tabController,
                        children: tabNames.map(
                          (item) {
                            // controller.title.value = item;
                            // controller.historyData?.clear();
                            return renderTab(item);
                          },
                        ).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget renderTab(String item) {
    switch (item) {
      case '注单统计':
        return const JDNotDetailListCP();
      case '等开奖':
        return JDBetRecordListCP(
          tabLabel: '待开奖',
          setkey: '待开奖',
          changeTabCount: 1,
          gameType: 'lottery',
          status: 1,
          startDate: '2024-06-21',
        );

      // return MyWidget();
      case '已中奖':
        return JDBetRecordListCP(
          tabLabel: '已中奖',
          setkey: '已中奖',
          changeTabCount: 2,
          gameType: 'lottery',
          status: 2,
          startDate: '2024-06-21',
        );

      // return JDPromotionTabMemberCP(tabLabel: item);
      case '未中奖':
        return JDBetRecordListCP(
          tabLabel: '未中奖',
          setkey: '未中奖',
          changeTabCount: 3,
          gameType: 'lottery',
          status: 3,
          startDate: '2024-06-21',
        );

      case '已撤单':
        return JDBetRecordListCP(
          tabLabel: '已撤单',
          setkey: '已撤单',
          changeTabCount: 4,
          gameType: 'lottery',
          status: 4,
          startDate: '2024-06-21',
        );

      case '已结单':
        return JDBetRecordListCP(
          tabLabel: '已结单',
          setkey: '已结单',
          changeTabCount: 5,
          gameType: 'lottery',
          status: 5,
          startDate: '2024-06-21',
        );

      default:
        return Container();
    }
  }
}
