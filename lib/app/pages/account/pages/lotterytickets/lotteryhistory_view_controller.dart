import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:fpg_flutter/data/repositories/api_ticket.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';

// import 'package:intl/intl.dart';

// import 'package:timezone/timezone.dart' as tz;

//token :b4ywNPp384p35W54BX4Kyzyy
class LotteryHistoryViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxInt selectedTabIndex = 0.obs;
  final storage = GetStorage();

  // ApiTask
  final ApiTicket _apiTicket = ApiTicket();
  RxList<UGBetsRecordModel>? lotteryData = RxList();
  RxString totalBetCount = ''.obs;
  RxString totalWinAmount = ''.obs;

  RxList<UGBetsRecordLongModel>? historyData = RxList();
  RxString totalbetCount = ''.obs;
  RxString totalwinAmount = ''.obs;
  RxString totalbetAmount = ''.obs;
  RxString startDate = ''.obs;
  RxString endDate = ''.obs;
  RxBool flagShow = false.obs;
  RxBool sFlag = false.obs; //start Date calender
  RxBool eFlag = false.obs;
  RxString date = ''.obs;

  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  void initController() async {
    isIniting.value = true;
    _apiTicket.appHttpClient
        .lotteryStatistics(
      token: AppDefine.userToken?.apiSid,
      startDate: "2024-07-05",
      endDate: "2024-08-10",
    )
        .then((data) {
      isIniting.value = false;

      AppToast.showDuration(msg: data.msg, duration: 2);

      lotteryData?.value = data.data?.tickets ?? [];
      totalBetCount.value = data.data?.totalBetCount ?? '';
      totalWinAmount.value = data.data?.totalWinAmount ?? '';
    });
  }

  void loadWBData(String category, int status, {String? startDate}) async {
    isIniting.value = true;
    _apiTicket.appHttpClient
        .history(
      token: AppDefine.userToken?.apiSid,
      // startDate: startDate,
      page: 1,
      rows: 999,
      category:
          'lottery', //游戏分类：lottery=彩票，real=真人，card=棋牌，game=电子游戏，sport=体育, blockchain=区块链
      status: status, // 注单状态：1=待开奖，2=已中奖，3=未中奖，4=已撤单 5：已结
    )
        .then((data) {
      isIniting.value = false;

      AppToast.showDuration(msg: data.msg, duration: 1);
      historyData?.clear();
      historyData?.value = data.data?.data?.list ?? [];
      totalwinAmount.value = data.data?.data?.totalWinAmount ?? '';
      totalbetAmount.value = data.data?.data?.totalBetAmount ?? '';
    });
  }
}
