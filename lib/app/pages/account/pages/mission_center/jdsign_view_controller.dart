import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/UGSignInModel.dart';
import 'package:fpg_flutter/data/repositories/api_task.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/showsalary_alert.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/pages/account/widgets/ucolumn.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class JdsiginViewController extends GetxController {
  RxBool isIniting = false.obs;
  final RxList<RxDouble> cardOpacity =
      [1.0.obs, 1.0.obs, 1.0.obs, 1.0.obs, 1.0.obs, 1.0.obs, 1.0.obs].obs;
  RxInt selectedTabIndex = 0.obs;
  RxInt selectedIconIndex = 0.obs;
  final ApiTask _apiTask = ApiTask();
  RxBool kisCheckIn = false.obs;
  Rx<UGcheckinBonusModel> obj = UGcheckinBonusModel().obs;
  Rx<UGcheckinBonusModel> obj2 = UGcheckinBonusModel().obs;
  Rx<UGcheckinBonusModel> obj3 = UGcheckinBonusModel().obs;
  RxBool hide1 = false.obs;
  RxBool hide2 = false.obs;
  RxBool hide3 = false.obs;
  RxString bonus1 = ''.obs;
  RxString bonus2 = ''.obs;
  RxString bonus3 = ''.obs;
  Rx<UGcheckinBonusModel> checkinBounusModel1 = UGcheckinBonusModel().obs;
  Rx<UGcheckinBonusModel> checkinBounusModel2 = UGcheckinBonusModel().obs;
  Rx<UGcheckinBonusModel> checkinBounusModel3 = UGcheckinBonusModel().obs;
  Rx<UGSignInModel> checkData = UGSignInModel().obs;
  RxString msg = ''.obs;
  RxList<UGSignInHistoryModel> history = RxList();
  Rx<JDSignInHistoryVars>? historyData = JDSignInHistoryVars().obs;
  bool isOneSelected = false;
  @override
  void onReady() async {
    initController();
    checkinList();
    super.onReady();
  }

  void initController() async {
    isIniting(true);
    _apiTask.appHttpClient
        .checkinList(
      token: AppDefine.userToken?.apiSid,
    )
        .then((data) {
      checkData.value = data.data!;
    });
    isIniting(false);
  }

  Future checkinList() async {
    bool empty = false;
    // AppToast.showDuration(msg: 'Roading', duration: 3);
    await _apiTask.appHttpClient
        .checkinList(
      token: AppDefine.userToken?.apiSid,
    )
        .then((data) {
      if (data.data == null) {
        empty = true;
        return;
      }
      checkData.value = data.data!;
    });
    if (empty) return;
    obj.value = checkData.value.checkinBonus!.elementAt(0);
    obj2.value = checkData.value.checkinBonus!.elementAt(1);
    if (checkData.value.checkinBonus!.length == 3) {
      obj3.value = checkData.value.checkinBonus!.elementAt(2);
    }
    //
    if (checkData.value.checkinBonus!.length >= 2) {
      hide1.value = setState(obj.value.switch_ == 1);
      hide2.value = setState(obj2.value.switch_ == 1);
      bonus1.value = '5天礼包(${obj.value.int_})';
      bonus2.value = '7天礼包(${obj2.value.int_})';
      if (obj3.value != null) {
        hide3.value = setState(obj3.value.switch_ == 1);
        bonus3.value = '6天礼包(${obj3.value.int_})';
      }
    }

    for (var key in checkData.value.checkinList!) {
      if (key.whichDay == checkData.value.serverTime) {
        kisCheckIn.value = key.isCheckin ?? false;
      }
    }
  }

  void checkinDataWithType(BuildContext context, String type, String date) {
    _apiTask.appHttpClient
        .checkin(
      token: AppDefine.userToken?.apiSid,
      type: type,
      date: date,
    )
        .then((data) {
      msg.value = data.msg;
      checkinList();
      ShowSalaryAlert(context).showAlert();
    });
  }

  void checkinBonusData(BuildContext context, String type) {
    _apiTask.appHttpClient
        .checkinBonus(
      token: AppDefine.userToken?.apiSid,
      type: type,
    )
        .then((data) {
      checkinList();
      ShowSalaryAlert(context).showAlert();
    });
  }

  void checkinHistory(BuildContext context) async {
    await _apiTask.appHttpClient
        .checkinHistory(
      token: AppDefine.userToken?.apiSid,
    )
        .then((data) {
      historyData?.value.list = data.data!;
      historyData?.value.show = historyData?.value.show ?? false;
      historyData?.value.name = '1';
      historyData?.value.checkinMoney = checkData.value.checkinMoney;
    });
    HistoryAlert().showRecord(context);
  }

  bool setState(bool flag) {
    return flag;
  }

  String checkinState(int index) {
    String result = '';
    UGCheckinListModel? item = checkData.value.checkinList?.elementAt(index);
    if (item?.isCheckin ?? false) {
      result = '已签到';
    } else {
      DateTime whichDay = DateTime.parse(item?.whichDay ?? '');
      DateTime serverTime = DateTime.parse(checkData.value.serverTime ?? '');
      if (whichDay.isAfter(serverTime)) {
        return '签到';
      } else {
        return '补签';
      }
    }
    return result;
  }

  void itemAction(BuildContext context, int index) {
    isOneSelected = true;
    UGCheckinListModel? item = checkData.value.checkinList?.elementAt(index);
    if (item?.isCheckin ?? false) {
      return;
    }
    DateTime whichDay = DateTime.parse(item?.whichDay ?? '');
    if (whichDay.isAfter(DateTime.now())) {
      checkinDataWithType(
          context,
          '0',
          item?.whichDay ??
              ''); // Assuming this function sends data with type '0'
    } else {
      if (checkData.value.mkCheckinSwitch! && item!.isMakeup!) {
        bool foundUnchecked = false;
        for (UGCheckinListModel clm in checkData.value.checkinList!) {
          if (clm == item) {
            checkinDataWithType(
                context,
                '1',
                item.whichDay ??
                    ''); // Assuming this function sends data with type '1'
            foundUnchecked = true;
            break;
          }
          if (!clm.isCheckin!) {
            AppToast.showToast('必须从前往后补签'); // Displaying a toast message
            break;
          }
        }
        if (!foundUnchecked) {
          AppToast.showToast('补签通道已关闭'); // Displaying a toast message
        }
      } else {
        AppToast.showToast('补签通道已关闭'); // Displaying a toast message
      }
    }
  }
}

class JDSignInHistoryVars {
  List<UGSignInHistoryModel>? list;
  bool? show = false;
  String? name = '1';
  String? checkinMoney;
  JDSignInHistoryVars({this.list, this.show, this.name});
}

class HistoryAlert {
  HistoryAlert();
  JdsiginViewController controller = Get.find();

  Widget _createContent(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0).w,
        child: Text('bodyText',
            textAlign: TextAlign.center,
            style: AppTextStyles.headline2(
              context,
              fontSize: 20,
            )),
      ),
    );
  }

  Dialog _createDialog(BuildContext context) {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(4))),
      child: Container(
          decoration: BoxDecoration(
              border: Border.all(width: 3.w, color: AppColors.ff17c492),
              borderRadius: BorderRadius.all(Radius.circular(4))),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                width: 0.8.sw,
                child: titleWidget(context),
              ),
              SizedBox(
                width: 0.8.sw,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8).w,
                  child: contentWidget(context),
                ),
              ),
              SizedBox(
                height: 20.h,
              )
            ],
          )),
    );
  }

  @override
  void showRecord(BuildContext context) {
    // title = titleWidget();

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.85.sw, child: _createDialog(context));
        });
  }

  Widget titleWidget(BuildContext context) {
    return Stack(
      alignment: Alignment.topRight,
      children: [
        Positioned(
            top: 5.h,
            right: 5.w,
            child: Icon(size: 30.w, color: AppColors.ff8D8B8B, Icons.close)
                .onTap(() => Get.back())),
        Container(
            height: 110.h,
            padding: EdgeInsets.only(top: 20.w),
            child: Column(children: [
              SizedBox(
                height: 20.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Text(
                        ' 连续签到 ',
                        textAlign: TextAlign.center,
                        style: AppTextStyles.ff000000(
                          context,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        '  ${controller.checkData.value.checkinTimes.toString()} ',
                        textAlign: TextAlign.center,
                        style: AppTextStyles.ff17c492_(
                          context,
                          fontSize: 35,
                        ),
                      ),
                    ],
                  ),
                  Column(children: [
                    Text(
                      '累计积分奖励',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.ff000000(
                        context,
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      '${controller.checkData!.value.checkinMoney}',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.ff17c492_(
                        context,
                        fontSize: 35,
                      ),
                    ),
                  ])
                ],
              )
            ]))
      ],
    );
  }

  Widget contentWidget(BuildContext context) {
    return Container(
      height: 500.w,
      child: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                height: 60.h,
                color: AppColors.CLBgColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '签到日期',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.ff000000(
                        context,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      width: 1.sw / 4 - 50.w,
                    ),
                    Text(
                      '奖励',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.ff000000(
                        context,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      width: 1.sw / 4 - 30.w,
                    ),
                    Text(
                      '说明',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.ff000000(
                        context,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(width: 10.w)
                  ],
                ),
              ),
              if (controller.historyData?.value.list != null)
                ..._renderSelectedPageContent(context),
              SizedBox(
                height: 10.h,
              ),
              if (controller.historyData?.value.list != null)
                ..._renderSelectPageIcon(context),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _renderSelectedPageContent(BuildContext context) {
    List<Widget> temp = [];

    int len = controller.historyData?.value.list?.length ?? 0;
    for (int i = controller.selectedIconIndex.value * 8;
        i < (controller.selectedIconIndex.value + 1) * 8 && i < len;
        i++) {
      UGSignInHistoryModel item =
          controller.historyData?.value.list!.elementAt(i) ??
              UGSignInHistoryModel();
      temp.add(Ucolumn(data: [
        item.checkinDate ?? '',
        item.integral ?? '',
        item.remark ?? ''
      ], index: i));
    }
    return temp;
  }

  List<Widget> _renderSelectPageIcon(BuildContext context) {
    List<Widget> temp = [];
    int len = controller.historyData?.value.list?.length ?? 0;

    return [
      Center(
          child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ...List.generate(
            ((controller.historyData?.value.list?.length ?? 0) / 8).round(),
            (index) {
              return Container(
                      width: 30.w,
                      height: 30.h,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: controller.selectedIconIndex.value == index
                              ? Colors.transparent
                              : AppColors.ff17c492,
                        ),
                        color: controller.selectedIconIndex.value == index
                            ? AppColors.ff17c492
                            : Colors.transparent,
                      ),
                      child: Center(
                          child: Text(
                        '${index + 1}',
                        style: controller.selectedIconIndex.value == index
                            ? AppTextStyles.surface_(context)
                            : AppTextStyles.bodyText2(context),
                      )))
                  .onTap(() => controller.selectedIconIndex.value = index)
                  .paddingOnly(right: 10.w);
            },
          )
        ],
      ))
    ];
  }
}
