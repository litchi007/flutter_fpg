import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/hallcard.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/exchangecard.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/scorecard.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/levelcard.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/customalertdialog.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/cp/reloadbalance_cp.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:get/get.dart';
import 'package:gap/gap.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

class MissionCenterView extends StatefulWidget {
  MissionCenterView({super.key, this.initIndex});
  int? initIndex = 0;

  @override
  State<MissionCenterView> createState() => _MissionCenterView();
}

class _MissionCenterView extends State<MissionCenterView>
    with TickerProviderStateMixin {
  final MissionCenterViewController controller =
      Get.put(MissionCenterViewController());
  int? initTabIndex;
  late TabController _tabController;
  final List<String> tabs = ["任务大厅", "分数兑换", "分数账变", '等级说明'];

  @override
  void initState() {
    initTabIndex = Get.arguments != null ? Get.arguments[0] : 0;
    _tabController =
        TabController(initialIndex: initTabIndex ?? 0, length: 4, vsync: this);
    _tabController.addListener(_handleTabChange);
    super.initState();
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      controller.tabIndex.value = _tabController.index;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: GestureDetector(
          onTap: () {
            TimerManager().stopAllTimers();
            Get.back();
          },
          child:
              Icon(Icons.arrow_back_ios, color: AppColors.surface, size: 28.w),
        ),
        title: Obx(() => Text(tabs.elementAt(controller.tabIndex.value),
            style: AppTextStyles.ffffffff(context,
                fontSize: 25, fontWeight: FontWeight.bold))),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Stack(
            clipBehavior: Clip.none,
            alignment: AlignmentDirectional.topCenter,
            children: [
              InforCard(),
              Positioned(
                top: 200.w,
                child: StateCard(),
              ),
            ],
          ),
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                color: AppColors.ffd7213a,
              ),
              child: Column(
                children: [
                  TabBar(
                      onTap: (index) {
                        // controller.selectItem();
                      },
                      controller: _tabController,
                      unselectedLabelColor: AppColors.surface,
                      labelColor: (_tabController.index == 0 ||
                              _tabController.index == 1
                          ? AppColors.ffffeb3b
                          : AppColors.ffffeb3b),
                      indicatorColor: (_tabController.index == 0 ||
                              _tabController.index == 1
                          ? AppColors.ffffeb3b
                          : AppColors.ffffeb3b),
                      physics: const ClampingScrollPhysics(),
                      indicatorSize: TabBarIndicatorSize.label,
                      tabAlignment: TabAlignment.fill,
                      indicator: null,
                      tabs: [
                        ...List.generate(tabs.length, (index) {
                          return Tab(
                            child: Text(
                              tabs.elementAt(index),
                              style: TextStyle(fontSize: 20.w),
                            ),
                          );
                        }),
                      ]),
                  Expanded(
                    child: SizedBox(
                      width: 1.sw,
                      height: 100.h,
                      child: TabBarView(controller: _tabController, children: [
                        HallCard(),
                        Exchangecard(),
                        ScoreCard(),
                        LevelCard(),
                      ]),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class InforCard extends StatelessWidget {
  InforCard({super.key});
  final List<String> imgdatapath = [
    'images/center',
    'images/signup',
    'images/zh/lqfl',
  ];
  @override
  Widget build(BuildContext context) {
    CustomAlertDialog bonusalert = CustomAlertDialog(
      title: '领取俸禄',
      context: context,
      titleBackgroundColor: AppColors.surface,
    );
    MissionCenterViewController controller = Get.find();

    return Container(
      width: 1.sw,
      height: 320.w,
      margin: EdgeInsets.only(top: 2.h),
      decoration: const BoxDecoration(
        color: AppColors.ffbfa46d,
      ),
      child: Stack(
        // alignment:AlignmentDirectional.center ,
        clipBehavior: Clip.none,
        children: [
          Positioned(
              left: 20.w,
              top: 10.w,
              child: SizedBox(
                  height: 150.w,
                  width: 150.w,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Obx(
                        () => CircleAvatar(
                          backgroundImage: NetworkImage(
                            controller.avatar.value ?? '',
                          ),
                          radius: 50.w,
                        ),
                      ),
                      SizedBox(
                        height: 10.w,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20,
                        ).w,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '头衔:',
                              style: AppTextStyles.ffffffff(
                                context,
                                fontSize: 14,
                              ),
                            ),
                            Obx(
                              () => Text(
                                controller.percent.value == '100'
                                    ? '至尊'
                                    : controller.curLevelGrade.value,
                                style: AppTextStyles.ffffff8c_(
                                  context,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ))),
          Positioned(
            left: 150.w,
            top: 30.w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Text(
                      controller.infoUsr.value,
                      style: AppTextStyles.ffffffff(
                        context,
                        fontSize: 18,
                      ),
                    ),
                    Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              '#55c6ff'.color(),
                              '#91daff'.color(),
                            ],
                          ), // 设置渐变色背景
                          borderRadius: BorderRadius.circular(4), // 设置圆角
                        ),
                        child: Obx(
                          () => Text(
                            controller.curLevelGrade.value,
                            style: AppTextStyles.ffffffff(
                              context,
                              fontSize: 18,
                            ),
                          ),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20.w,
                ),
                SizedBox(
                  height: 35.w,
                  child: Row(
                    children: [
                      Obx(
                        () => ReLoadBalanceComponent(
                            title: '余额',
                            balance: controller.infoBalance.value,
                            balanceDecimal: '4',
                            titleStyle:
                                const TextStyle(color: AppColors.surface),
                            balanceStyle: AppTextStyles.ffffff8c_(
                              context,
                              fontSize: 20,
                            ),
                            onRefresh: () {
                              controller.refreshUserInfo();
                            }),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 35.w,
                  child: Row(
                    children: [
                      Text(
                        '分数: ',
                        style: AppTextStyles.ffffffff(
                          context,
                          fontSize: 20,
                        ),
                      ),
                      Obx(
                        () => Text(
                          controller.infoReward.value.toString(),
                          style: AppTextStyles.ffffffff(context, fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            right: 5.w,
            top: 10.w,
            child: SizedBox(
                width: 150.w,
                height: 200.w,
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        // Get.toNamed(accountPath);
                        Navigator.pop(context);
                      },
                      child: SvgPicture.network(
                        img_root(imgdatapath.elementAt(0), type: 'svg'),
                        fit: BoxFit.contain,
                        height: 40.w,
                      ),
                    ),
                    Gap(10.h),
                    GestureDetector(
                      onTap: () {
                        Get.toNamed(mobilePath);
                        // na
                      },
                      // SvgPicture.network
                      child: SvgPicture.network(
                        img_root(imgdatapath.elementAt(1), type: 'svg'),
                        fit: BoxFit.contain,
                        height: 40.w,
                      ),
                    ),
                    Gap(10.h),
                    GestureDetector(
                      onTap: () {
                        bonusalert.dialogBuilder();
                      },
                      child: AppImage.network(
                        'https://gsyahcot002uqmhd.aaasfjyt.com/images/zh/lqfl.png', // temperory path---
                        fit: BoxFit.contain,
                        height: 40.w,
                      ),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}

class StateCard extends StatelessWidget {
  StateCard({super.key});

  String mark = 'images/vip';
  final MissionCenterViewController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Obx(() => Padding(
              padding: EdgeInsets.only(left: 0.4.sw),
              child: controller.percent.value == '100'
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          '恭喜您已经是最高等级!',
                          style: AppTextStyles.ffffffff(context, fontSize: 16),
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          '距离下一级还差',
                          style: AppTextStyles.ffffffff(context, fontSize: 16),
                        ),
                        Obx(
                          () => Text(
                            ('${double.parse(controller.nextLevelInt.value) <= 0 ? 0.00 : (double.parse(controller.nextLevelInt.value) - double.parse(controller.taskRewardTotal.value)).toStringAsFixed(2)}'),
                            style:
                                AppTextStyles.ffffffff(context, fontSize: 16),
                          ),
                        ),
                      ],
                    ))),
          SizedBox(
            height: 10.w,
          ),
          Obx(() => Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    '成长值：',
                    style: AppTextStyles.ffffffff(context, fontSize: 16),
                  ),
                  Stack(
                    alignment: Alignment.centerLeft,
                    children: [
                      LinearPercentIndicator(
                        width: 350.0.w,
                        animation: true,
                        animationDuration: 2000,
                        lineHeight: 20.0.w,
                        percent: double.parse(controller.percent.value) / 100.0,
                        linearGradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [AppColors.fff9fd8f, AppColors.fff7b635],
                        ),
                        barRadius: const Radius.circular(10),
                      ),
                      Positioned(
                        left: double.parse(controller.percent.value) < 10
                            ? 20.w
                            : double.parse(controller.percent.value) < 20
                                ? (265.0.w *
                                        double.parse(controller.percent.value) /
                                        100.0) -
                                    10.w
                                : double.parse(controller.percent.value) < 50
                                    ? (265.0.w *
                                            double.parse(
                                                controller.percent.value) /
                                            100.0) -
                                        10.w
                                    : (280.0.w *
                                        double.parse(controller.percent.value) /
                                        100.0),
                        top: 0,
                        child: Obx(
                          () => Text(
                            // '${controller.percent.value}%',
                            '${controller.percent.value}%',
                            style: AppTextStyles.error_(context, fontSize: 16),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              )),
          SizedBox(height: 10.w),
          Padding(
            padding: EdgeInsets.only(
              left: 0.12.sw,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                    child: Row(
                  children: [
                    AppImage.network(img_root(mark, type: 'png'),
                        width: 20.w, height: 20.w, fit: BoxFit.contain),
                    Obx(
                      () => Text(controller.curLevelGrade.value,
                          style:
                              AppTextStyles.ffffff8c_(context, fontSize: 20)),
                    ),
                  ],
                )),
                SizedBox(
                  width: 200.w,
                ),
                Obx(() {
                  if (controller.percent.value == '100') {
                    return SizedBox(
                      width: 50,
                    );
                  }
                  return SizedBox(
                    child: Row(
                      children: [
                        AppImage.network(
                          img_root(mark, type: 'png'),
                          width: 20.w,
                          height: 20.w,
                        ),
                        Obx(
                          () => Text(
                            controller.nextLevelGrade.value,
                            style:
                                AppTextStyles.ffffff8c_(context, fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
