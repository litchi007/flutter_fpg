import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/MissionModel.dart';
import 'package:fpg_flutter/data/models/MissionData.dart';
import 'package:fpg_flutter/data/repositories/api_task.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/configs/pushhelper.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_notitle_dialog.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/model/SortId.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/router/router.dart';
import 'dart:async';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

import 'dart:math' as math;

class MissionCenterViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxInt tabIndex = 0.obs;
  final Map<String, int> dropdownList = {
    '全部日期': 1000,
    '最近一天': 1,
    '最近7天': 7,
    '最近一个月': 30,
  };
  RxInt selectedSortId = (0).obs;
  RxString selectedSortName = ''.obs;
  Map<String, RxBool> showDetail = {'': false.obs};
  RxString state_data = "确认兑换".obs;
  RxList<MissionCategory> allCategories = RxList();
  RxList<MissionCategory> curCategories = RxList();
  RxString dropdownValue = '全部日期'.obs;

  final ApiTask _apiTask = ApiTask();
  RxString curLevelGrade = ''.obs;
  RxString nextLevelGrade = ''.obs;
  RxString infoUsr = ''.obs;
  RxString infoBalance = ''.obs;
  RxString infoReward = ''.obs;
  RxString avatar = ''.obs;
  RxString missionBili = ''.obs;
  RxDouble selectedBili = 0.0.obs;
  RxString percent = ''.obs;
  RxString nextLevelInt = '0'.obs;
  RxString taskRewardTotal = "0".obs;
  RxList<SalaryModel> salaryList = RxList();
  Rx<NewSalaryModel> salaryListNew = NewSalaryModel().obs;
  // vip等级
  RxList<ScoreLevelData>? scoreLevelList = RxList();
  //积分账变列表
  Rx<CreditsLogModel>? changeList = CreditsLogModel().obs;

  RxList<CreditsUList> scoreList = RxList();
  RxList<Mission> hallData = RxList();
  Timer? timer;
  @override
  void onInit() async {
    super.onInit();

    initController();
    _startTimer();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void _startTimer() {
    timer = Timer.periodic(const Duration(seconds: 5), (Timer timer) {
      refreshUserInfo();
      initData();
      getTasks(periodicCall: true);
    });
    TimerManager().addTimer(timer!);
  }

  void initController() async {
    isIniting.value = true;
    initData();
    getData();
  }

  void initData() {
    dropdownValue.value = '全部日期';
    missionBili.value = AppDefine.systemConfig?.missionBili ?? '';
    curLevelGrade.value = GlobalService.to.userInfo.value?.curLevelGrade ?? '';
    nextLevelGrade.value =
        GlobalService.to.userInfo.value?.nextLevelGrade ?? '';
    taskRewardTotal.value =
        GlobalService.to.userInfo.value?.taskRewardTotal?.toString() ?? '0';
    nextLevelInt.value =
        GlobalService.to.userInfo.value?.nextLevelInt?.toString() ?? '0';
    infoUsr.value = GlobalService.to.userInfo.value?.usr ?? '';
    avatar.value = GlobalService.to.userInfo.value?.avatar ?? "";
    infoBalance.value = double.parse(
            GlobalService.to.userInfo.value?.balance?.toString() ?? '0')
        .toStringAsFixed(4);
    infoReward.value = double.parse(
            GlobalService.to.userInfo.value?.taskReward?.toString() ?? '0')
        .toStringAsFixed(4);

    calculateLevelPercent();
  }

  void createShowDetails() {
    showDetail.clear();
    _apiTask
        .categories(
      token: AppDefine.userToken?.apiSid,
    )
        .then((data) {
      allCategories.value = data ?? [];

      curCategories.clear();
      for (MissionCategory item in allCategories) {
        bool flag = false;
        for (Mission hallItem in hallData) {
          if (hallItem.sortId == item.id) {
            flag = true;
            break;
          }
        }
        if (flag) {
          curCategories.add(item);
        }
      }
      for (MissionCategory item in curCategories) {
        showDetail[item.id ?? '0'] = false.obs;
      }
      selectedSortId.value = int.parse(
          curCategories.isNotEmpty ? (curCategories[0].id ?? '0') : '0');
      selectedSortName.value =
          curCategories.isNotEmpty ? (curCategories[0].sortName ?? '') : '';
    });
  }

  void getData() async {
    getLevelandCreditLog();
    getTasks();
    getMissionBonusListNew(false);
  }

  void getLevelandCreditLog() {
    _apiTask.appHttpClient.getLevels().then((data) {
      scoreLevelList?.value = data.data ?? [];
      _apiTask.appHttpClient
          .getCreditsLog(
              token: AppDefine.userToken?.apiSid,
              time: '2024',
              page: 1,
              rows: 1000)
          .then((data) {
        changeList?.value = data.data ?? CreditsLogModel();
        selectItem();
      });
    });
  }

  void getTasks({bool? periodicCall = false}) async {
    _apiTask.appHttpClient
        .center(
            token: AppDefine.userToken?.apiSid, category: '', page: 1, rows: 20)
        .then((data) {
      hallData.value = data.data?.missions ?? [];
      _apiTask
          .categories(
        token: AppDefine.userToken?.apiSid,
      )
          .then((data) {
        allCategories.value = data ?? [];
        curCategories.clear();
        for (MissionCategory item in allCategories) {
          bool flag = false;
          for (Mission hallItem in hallData) {
            if (hallItem.sortId == item.id) {
              flag = true;
              break;
            }
          }
          if (flag) {
            curCategories.add(item);
          }
        }
        if (periodicCall == false) {
          createShowDetails();
        }
      });
    });
    isIniting.value = false;
  }

  void getMissionBonusListNew(bool isSendEvent) async {
    await _apiTask
        .getMissionBonusListNew(token: AppDefine.userToken?.apiSid)
        .then((data) {
      isIniting.value = false;

      salaryListNew.value = data ?? NewSalaryModel();
      AppLogger.d(salaryListNew.toJson());
    });
  }

  void requestTask(Mission item, BuildContext context) async {
    try {
      switch (item.status) {
        case '0':
          if (item.id?.isEmpty ?? false) return;
          _apiTask.appHttpClient
              .getTask(token: AppDefine.userToken?.apiSid, mid: item.id)
              .then((data) {
            AppToast.showDuration(msg: data.msg);
          });
          break;
        case '1':
          if (item.id?.isEmpty ?? false) return;
          _apiTask.appHttpClient
              .getReward(token: AppDefine.userToken?.apiSid, mid: item.id)
              .then((data) {
            if (data.code != 0) {
              CustomNotitleDialog(
                      context: context,
                      bodyText: data.msg,
                      titleBackgroundColor: Colors.white,
                      actionPath: '',
                      action: _actionWidget(context, gofinish, item))
                  .dialogBuilder([]);
            }
          });
          break;

        case '3':
          if (item.id?.isEmpty ?? false) return;
          _apiTask.appHttpClient
              .getReward(token: AppDefine.userToken?.apiSid, mid: item.id)
              .then((data) {
            AppToast.showDuration(msg: data.msg);
            initController();
            if (data.code == 0) {
              AppToast.showDuration(msg: data.msg);
            } else {
              handleUnfinished(item);
            }
          });
          break;
      }
    } catch (e) {
      AppToast.showDuration(msg: '$e');
    }
  }

  void gofinish(Mission item) {
    if (item.sortId == SortId.everydayTask.value) {
      if (item.missionType == '9' && item.jumpTypeId == '0') {
        List<GameModel>? lottery = getLotteryList();
        final length = lottery?.length ?? 0;
        final random = math.Random().nextInt(length);
        Get.toNamed('${AppRoutes.lottery}/${lottery?[random].gameId ?? 243}');
      } else if (item.missionType == '9') {
        Get.toNamed('${AppRoutes.lottery}/${item.jumpTypeId}');
      } else if (item.missionType == '49' && item.jumpTypeId != '0') {
        Get.toNamed('${AppRoutes.lottery}/${item.jumpTypeId}');
      } else if (item.missionType == '54') {
        return;
      }
    } else if (item.sortId == SortId.lotteryTask.value) {
      if (item.jumpTypeId != '' && (item.jumpTypeId?.contains(',') ?? false)) {
        final arr = item.jumpTypeId?.split(',') ?? [];
        final length = arr.length;
        final random = math.Random().nextInt(length);
        Get.toNamed('${AppRoutes.lottery}/${arr[random]}');
      }
      if ((item.jumpTypeId == '' || item.jumpTypeId == '0') &&
          item.missionName != '') {
        final gameName = item.missionName;

        final gameIdList =
            getLotteryList()?.where((item) => item.name == gameName).toList();
        dynamic gameId;
        if (gameIdList?.isNotEmpty ?? false) {
          gameId = gameIdList![0].gameId;
        }

        if (gameId != null) {
          Get.toNamed("${AppRoutes.lottery}/$gameId");
        } else {
          List<GameModel>? lottery = getLotteryList();
          final length = lottery?.length ?? 0;
          final random = math.Random().nextInt(length);
          Get.toNamed('${AppRoutes.lottery}/${lottery?[random].gameId ?? 243}');
        }
      } else {
        Get.toNamed('${AppRoutes.lottery}/${item.jumpTypeId}');
      }
    } else if (item.sortName == '存款任务') {
      Get.toNamed(depositPath, arguments: [0]);
    } else if (item.sortName == '棋牌任务') {
      Get.toNamed(cardListPath);
    } else if (item.sortName == '真人投注') {
      return;
    }
    // else if (item.sortId == SortId.electronicTask) {
    //   Get.toNamed(gameLobbyPath);
    // }
    else if (item.sortId == SortId.promotetask.value) {
      PushHelper.pushUserCenterType(UGUserCenterType.recommendedIncome);
    } else {
      final gameName = item.missionName;
      final gameIdList =
          getLotteryList()?.where((item) => item.name == gameName).toList();
      dynamic gameId;
      if (gameIdList?.isNotEmpty ?? false) {
        gameId = gameIdList![0].gameId;
      }
      if (gameId != null) {
        Get.toNamed(gameId);
      } else {
        List<GameModel>? lottery = getLotteryList();
        final length = lottery?.length ?? 0;
        final random = math.Random().nextInt(length);
        Get.toNamed('${AppRoutes.lottery}/${lottery?[random].gameId ?? 243}');
      }
    }
  }

  List<GameModel>? getLotteryList() {
    List<GameModel>? lotteryList = AppDefine.homeGamesIcon
        ?.firstWhere((item) =>
            item.url?.contains('lottery/list') ?? false || item.name == '彩票')
        .list;
    return lotteryList;
  }

  Widget _actionWidget(
      BuildContext context, Function(Mission item) gofinish, Mission item) {
    return GestureDetector(
        onTapDown: (done) {
          Get.back();
          gofinish(item);
        },
        child: Container(
          width: 0.75.sw,
          height: 45.h,
          decoration: const BoxDecoration(
              color: AppColors.ffbfa46d,
              borderRadius: BorderRadius.all(Radius.circular(8))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "确定",
                textAlign: TextAlign.center,
                style: AppTextStyles.headline2(
                  context,
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ));
  }

  void handleUnfinished(Mission item) {
    String actionPath;
    if (item.sortName?.contains('存款') ?? false) {
      actionPath = depositPath;
    } else if (item.sortName?.contains('推广') ?? false) {
      actionPath = myrecoPath;
    } else if (item.sortName?.contains('彩票') ?? false) {
      actionPath = "${AppRoutes.lottery}/${item.jumpTypeId}";
    } else {
      actionPath = "noWhere";
    }
    Get.back();
    Get.toNamed(actionPath);
  }

  void selectItem() {
    scoreList.clear();

    int index = dropdownList[dropdownValue.value] ?? 0;

    for (CreditsUList v in changeList?.value.data?.list ?? []) {
      DateTime recordTime = DateTime.parse(v.addTime ?? '');
      DateTime now = DateTime.now();
      if (now.difference(recordTime).inDays < index) {
        scoreList.add(v);
      }
    }
  }

  void calculateLevelPercent() {
    if (curLevelGrade == nextLevelGrade) {
      percent.value = '100';
    } else {
      double divisionResult = nextLevelInt.value != '0'
          ? (double.parse(taskRewardTotal.value)) /
              double.parse(nextLevelInt.value)
          : double.nan;

      if (divisionResult.isNaN) {
        percent.value = '0';
      } else {
        percent.value = (divisionResult * 100).toStringAsFixed(0);
      }
    }
  }

  void sendMissionBons(String bonsId) async {
    await _apiTask.sendMissionBonus(bonsId: bonsId).then((data) {
      getMissionBonusListNew(true);
    });
  }

  void requestExchange() async {
    if (selectedBili.value == 0) {
      AppToast.showDuration(msg: '请输入积分');
      return;
    }

    _apiTask
        .creditsExchange(AppDefine.userToken?.apiSid, selectedBili.value)
        .then((code) {
      if (code == 0) {
        getLevelandCreditLog();
      }
    });
  }

  void refreshUserInfo() async {
    await GlobalService.to.getPeriodicUserInfo();
    infoBalance.value = double.parse(
            GlobalService.to.userInfo.value?.balance?.toString() ?? '0')
        .toStringAsFixed(4);
    infoReward.value = double.parse(
            GlobalService.to.userInfo.value?.taskReward?.toString() ?? '0')
        .toStringAsFixed(4);
  }
}
