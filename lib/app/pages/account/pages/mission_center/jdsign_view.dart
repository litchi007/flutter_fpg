import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/jdsign_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/jdsignview_statecard.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';

class JdsiginView extends StatefulWidget {
  const JdsiginView({super.key});

  @override
  State<JdsiginView> createState() => _JdsiginView();
}

class _JdsiginView extends State<JdsiginView> with TickerProviderStateMixin {
  JdsiginViewController controller = Get.put(JdsiginViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.ff5651A0,

      // 暂无数据
      body: SingleChildScrollView(
        child: Obx(
          () => Column(
            children: [
              Stack(
                alignment: Alignment.topCenter,
                children: [
                  AppImage.network(
                      'https://gsyahcot002uqmhd.aaasfjyt.com/html/qiandao/images/zh/qiandao_banner.png',
                      width: 1.sw,
                      height: 300.h,
                      fit: BoxFit.fill),
                  Row(children: [
                    AppImage.asset('return.png',
                            width: 50.w, height: 50.w, fit: BoxFit.fill)
                        .paddingOnly(top: 30.h, left: 10.w, right: 10.w)
                        .onTap(() => Get.back()),
                    const Spacer(),
                    SvgPicture.network(
                      'https://gsyahcot002uqmhd.aaasfjyt.com/html/qiandao/images/qiandao_history.svg',
                      width: 50.w, // Adjust as needed
                    )
                        .onTap(() => controller.checkinHistory(context))
                        .paddingOnly(top: 30.h, left: 10.w, right: 10.w),
                  ]),
                ],
              ),
              Stack(
                alignment: Alignment.topCenter,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '已连续',
                        style: AppTextStyles.surface_(
                          context,
                          fontSize: 20,
                        ),
                      ),
                      Obx(
                        () => Text(
                            controller.checkData.value.checkinTimes == null
                                ? ''
                                : controller.checkData.value.checkinTimes
                                    .toString(),
                            style: AppTextStyles.ffff9211_(context,
                                fontSize: 40, fontWeight: FontWeight.bold)),
                      ),
                      Text(
                        '天签到',
                        style: AppTextStyles.surface_(
                          context,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      SizedBox(
                        height: 20.h,
                      ),
                      Center(
                          child: Text(
                        "...............",
                        style: AppTextStyles.ffff9211_(
                          context,
                          fontSize: 50,
                        ),
                      )),
                    ],
                  )
                ],
              ).paddingOnly(top: 50.h),

              // const CustomLoadingWidget()
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0).w,
                child: Panwidgets(),
              ),
              Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 40.h,
                      ),
                      controller.hide1.value
                          ? JdSignViewStateCard(
                              content1: controller.bonus1.value,
                              content2: '连续签到5天即可领取',
                              callId: '5')
                          : const SizedBox(),
                      controller.hide2.value
                          ? JdSignViewStateCard(
                              content1: controller.bonus2.value,
                              content2: '连续签到7天即可领取',
                              callId: '6')
                          : const SizedBox(),
                      controller.hide3.value
                          ? JdSignViewStateCard(
                              content1: controller.bonus3.value,
                              content2: '连续签到6天即可领取',
                              callId: '7')
                          : const SizedBox(),
                    ],
                  ),
                  Center(
                      child: Column(
                    children: [
                      SizedBox(
                        height: 17.h,
                      ),
                      SizedBox(
                        height: 50.w,
                        child: Opacity(
                            opacity: 1,
                            child: ElevatedButton(
                              onPressed: () {
                                controller.checkinDataWithType(
                                    context,
                                    '0',
                                    controller.checkData.value.serverTime ??
                                        '');
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor: AppColors.ffff5c7b,
                                // padding: EdgeInsets.symmetric(vertical: 1.h),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(35.w),
                                ),
                              ),
                              child: Obx(() => Text(
                                    controller.kisCheckIn.value
                                        ? '今日已签'
                                        : '马上签到',
                                    style: AppTextStyles.ffffffff(
                                      context,
                                      fontSize: 26,
                                    ),
                                  )),
                            )),
                      ),
                    ],
                  )),
                ],
              ),

              SizedBox(
                height: 30.h,
              ),

              // : const CustomLoadingWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
