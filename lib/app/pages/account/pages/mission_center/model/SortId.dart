enum SortId {
  everydayTask,
  depositTask,
  withdrawTask,
  lotteryTask,
  realTask,
  promotetask,
  rewardTask
}

extension SortIdExtension on SortId {
  String get value {
    switch (this) {
      case SortId.everydayTask:
        return '1';
      case SortId.depositTask:
        return '2';
      case SortId.withdrawTask:
        return '3';
      case SortId.lotteryTask:
        return '4';
      case SortId.realTask:
        return '5';
      case SortId.promotetask:
        return '6';
      case SortId.rewardTask:
        return '7';
    }
  }
}
