import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fpg_flutter/data/models/MissionModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view_controller.dart';
import 'package:get/get.dart';

class LevelCard extends StatelessWidget {
  LevelCard({super.key});
  final MissionCenterViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.surface,
      child: Column(
        children: [
          Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              border: TableBorder.all(color: AppColors.ff9e9e9e),
              // Allows to add a border decoration around your table
              children: [
                unitRow(context, '等级', '分数头衔', '成长分数', 20),
              ]),
          Obx(
            () => Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              border: TableBorder.all(color: AppColors.ff9e9e9e),
              children: [
                ...List.generate(controller.scoreLevelList!.length, (index) {
                  ScoreLevelData temp;
                  temp = controller.scoreLevelList!.elementAt(index);
                  return unitRow(context, temp.levelName!, temp.levelTitle!,
                      temp.integral!, 18);
                }),
              ],
            ),
          ),
          SizedBox(
            height: 20.w,
          ),
        ],
      ),
    );
  }
}

TableRow unitRow(BuildContext context, String data1, String data2, String data3,
    int fontsize) {
  return TableRow(children: [
    unitCell(context, data1, fontsize),
    unitCell(context, data2, fontsize),
    unitCell(context, data3, fontsize),
  ]);
}

Widget unitCell(BuildContext context, String data, int fontsize) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 15.w),
    child: Text(
      data,
      textAlign: TextAlign.center,
      style: AppTextStyles.ff666666(context, fontSize: fontsize),
    ),
  );
}
