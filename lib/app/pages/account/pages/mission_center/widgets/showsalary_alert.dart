import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
// import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/widgets/alertfrom_html.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/jdsign_view_controller.dart';
import 'package:get/get.dart';

class ShowSalaryAlert extends BaseAlertWidget {
  ShowSalaryAlert(BuildContext context, {this.titleStr, this.actionStr})
      : super(
            context: context,
            title: const SizedBox(),
            content: const SizedBox(),
            action: const SizedBox());
  JdsiginViewController controller = Get.find();
  String? titleStr;
  String? actionStr;
  @override
  Widget build(BuildContext context) {
    context = context;
    return const SizedBox();
  }

  @override
  void showAlert() {
    title = titleWidget();
    content = contentWidget();
    action = actionWidget();
    titleColor = AppColors.surface;
    actionColor = AppColors.surface;
    super.dialogBuilder();
  }

  Widget titleWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 5.w, top: 15.w),
      child: Text(
        titleStr ?? '温馨提示',
        style: AppTextStyles.ff000000(
          context,
          fontSize: 25,
        ),
      ),
    );
  }

  Widget contentWidget() {
    return Obx(
      () => (controller.msg.value == '')
          ? const CustomLoadingWidget()
          : Text(
              controller.msg.value,
              style: AppTextStyles.ff000000(
                context,
                fontSize: 25,
              ),
            ),
    );
  }

  Widget actionWidget() {
    return Row(
      children: [
        SizedBox(
          width: 0.45.sw,
        ),
        CustomButton(
          btnStr: actionStr ?? '确认', //'确认'
          backColor: AppColors.ffb9ae9e,
          width: 100,
          textColor: AppColors.ff4caf50,
          height: 30,
          onPressed: () {
            controller.isOneSelected = false;
            Get.back();
          },
        ),
      ],
    );
  }
}
