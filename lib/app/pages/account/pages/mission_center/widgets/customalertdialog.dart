import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view_controller.dart';

class CustomAlertDialog {
  final String title;

  final Color titleBackgroundColor;
  final BuildContext context;

  CustomAlertDialog({
    required this.context,
    required this.title,
    required this.titleBackgroundColor,
  });
  late MissionCenterViewController controller = Get.find();
  Text _createTitle() {
    return Text(
      title,
      style: AppTextStyles.bodyText1(context, fontSize: 22),
      textAlign: TextAlign.center,
    );
  }

  Widget _createContent() {
    return Container(
      color: AppColors.surface,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 20.w, bottom: 20.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '您当前的等级为: ',
                    style: AppTextStyles.ff5C5869_(
                      context,
                      fontSize: 20,
                    ),
                  ),
                  Obx(
                    () => Text(
                      controller.curLevelGrade.value,
                      style: AppTextStyles.ffff0000_(context,
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    ',可领取以下俸禄',
                    style: AppTextStyles.ff5C5869_(
                      context,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
            Obx(() => _createCard(
                type: '周',
                title: int.parse(controller
                            .salaryListNew.value.need?.week?.weekIntegral ??
                        '0')
                    .toString(),
                subTitle: int.parse(controller
                            .salaryListNew.value.need?.week?.weekBonsNeed ??
                        '0')
                    .toStringAsFixed(0),
                claim:
                    '${controller.salaryListNew.value.weekSetStatus ?? ''}${controller.salaryListNew.value.bons?.week?.weekBonsStatus}',
                onPress: () => controller.sendMissionBons(
                    controller.salaryListNew.value.bons?.week?.bonsId ?? ''))),
            SizedBox(
              height: 20.w,
            ),
            Obx(
              () => _createCard(
                  type: '月',
                  title: int.parse(controller
                              .salaryListNew.value.need?.month?.monthIntegral ??
                          '0')
                      .toString(),
                  subTitle: int.parse(controller
                              .salaryListNew.value.need?.month?.monthBonsNeed ??
                          '0')
                      .toStringAsFixed(0),
                  claim:
                      '${controller.salaryListNew.value.monthSetStatus ?? ''}${controller.salaryListNew.value.bons?.month?.monthBonsStatus}',
                  onPress: () => controller.sendMissionBons(
                      controller.salaryListNew.value.bons?.month?.bonsId ??
                          '')),
            )
          ]),
    );
  }

  Widget _createCard(
      {String? type,
      String? title,
      String? subTitle,
      String? claim,
      VoidCallback? onPress}) {
    String statusText = claim == '1,0'
        ? '可领取'
        : claim == '1,1'
            ? '已领取'
            : '不可领取';
    return Container(
      padding: EdgeInsets.all(10.w),
      decoration: BoxDecoration(
        color: AppColors.surface,
        border: Border.all(
          color: AppColors.ff9e9e9e,
          width: 1.w,
        ),
      ),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: 10.w,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // controller.salarylistnew.value.bons.
                Text(
                  '$type俸禄:  ${title ?? ''}',
                  style: AppTextStyles.ff5C5869_(context,
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  '上$type存款达到${subTitle ?? ''}可领取',
                  style: AppTextStyles.ff5C5869_(context, fontSize: 16),
                ),
              ],
            ),
          ),
          const Spacer(),
          Container(
            height: 30.sp,
            width: 100.w,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.w),
              color:
                  statusText == '可领取' ? AppColors.ff0277BD : AppColors.ffdcdcdd,
            ),
            child: GestureDetector(
              onTap: () {
                if (statusText != '不可领取') onPress!();
              },
              child: Text(
                statusText,
                style: AppTextStyles.ffffffff(context, fontSize: 16),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container _createActions() {
    return Container(
      width: 0.75.sw,
      height: 35.w,
      padding: EdgeInsets.only(
        left: 10.w,
        right: 10.w,
      ),
      decoration: const BoxDecoration(
        color: AppColors.ff007aff,
        borderRadius: BorderRadius.all(
          Radius.circular(1),
        ),
      ),
      child: GestureDetector(
        onTapDown: (done) => Get.back(),
        child: Text(
          textAlign: TextAlign.center,
          '关闭',
          style: AppTextStyles.ffffffff(context,
              fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Dialog _createDialog() {
    return Dialog(
      backgroundColor: titleBackgroundColor,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // Custom title with specified background color
          Container(
            width: 1.sw,
            height: 50.h,
            decoration: BoxDecoration(
                color: titleBackgroundColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            child: SizedBox(
              width: .8.sw,
              child: Padding(
                padding: EdgeInsets.only(top: 8.h),
                child: Column(
                  children: [
                    _createTitle(),
                    SizedBox(
                      width: 1.sw,
                      height: 1.sp,
                      child: const ColoredBox(
                        color: AppColors.ff9e9e9e,
                      ), // New color property
                    )
                  ],
                ),
              ),
            ),
          ),

          // Content area
          SizedBox(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: _createContent(),
            ),
          ),
          // Actions
          _createActions(),
          SizedBox(
            height: 20.h,
          )
        ],
      ),
    );
  }

  void dialogBuilder() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(width: 0.8.sw, child: _createDialog());
        });
  }
}
