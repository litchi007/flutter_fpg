import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view_controller.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/MissionData.dart';
import 'package:intl/intl.dart';

class HallCard extends StatelessWidget {
  final MissionCenterViewController controller = Get.find();

  HallCard({super.key});
  String value = '100';
  @override
  Widget build(BuildContext context) {
    return Container(
        color: AppColors.surface,
        child: Obx(() => controller.isIniting.value == true
            ? const Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CustomLoadingWidget(),
                ],
              )
            : Obx(
                () => controller.hallData.isNotEmpty
                    ? Column(
                        children: [
                          SizedBox(height: 10.w),
                          Obx(() => SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: controller
                                              .curCategories.isNotEmpty
                                          ? controller.curCategories
                                              .map((item) => _renderSortName(
                                                  context,
                                                  item.sortName ?? '',
                                                  int.parse(item.id ?? '-1'),
                                                  controller
                                                          .selectedSortId.value
                                                          .toString() ==
                                                      item.id))
                                              .toList()
                                          : [const SizedBox()])
                                  .paddingOnly(left: 10.w))),
                          SizedBox(height: 10.w),
                          Container(
                            width: 1.sw,
                            height: 120.h,
                            decoration:
                                BoxDecoration(color: AppColors.ffCCCCCC),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                    width: 0.3.sw,
                                    height: 30.h,
                                    child: AppImage.network(
                                      img_images('fangico-l'),
                                    )),
                                Text(controller.selectedSortName.value,
                                    style: AppTextStyles.surface_(context,
                                        fontSize: 30)),
                                SizedBox(
                                    width: 0.3.sw,
                                    height: 30.h,
                                    child: AppImage.network(
                                        img_images('fangico-r'))),
                              ],
                            ),
                          ),
                          Obx(() {
                            final showDetail = controller
                                    .showDetail[controller.selectedSortId.value
                                        .toString()]
                                    ?.value ??
                                false;
                            return GestureDetector(
                                onTap: () => controller
                                    .showDetail[controller.selectedSortId.value
                                        .toString()]
                                    ?.value = !showDetail,
                                child: Container(
                                  width: 1.sw,
                                  padding:
                                      const EdgeInsets.fromLTRB(30, 16, 30, 16)
                                          .w,
                                  decoration: BoxDecoration(
                                      border: Border(
                                    top: BorderSide(
                                        width: 1.w, color: AppColors.ff8D8B8B),
                                    bottom: BorderSide(
                                        width: 1.w, color: AppColors.ff8D8B8B),
                                  )),
                                  child: Row(
                                    children: [
                                      Text(controller.selectedSortName.value,
                                          style: AppTextStyles.bodyText2(
                                              context,
                                              fontSize: 30)),
                                      const Spacer(),
                                      showDetail == false
                                          ? Icon(Icons.arrow_forward_ios,
                                              color: AppColors.onBackground,
                                              size: 36.w)
                                          : Transform.rotate(
                                              angle: 3.141592 / 2,
                                              child: Icon(
                                                  Icons.arrow_forward_ios,
                                                  color: AppColors.onBackground,
                                                  size: 36.w)),
                                    ],
                                  ),
                                ));
                          }),
                          Obx(() {
                            final showDetail = controller
                                .showDetail[
                                    controller.selectedSortId.value.toString()]
                                ?.value;
                            final hallData = controller.hallData;
                            return showDetail == true
                                ? _renderSortedContent(context, hallData)
                                : const SizedBox();
                          })
                        ],
                      )
                    : const Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          '暂无数据',
                          textAlign: TextAlign.center,
                        ),
                      ),
              )));
  }

  Widget _renderSortName(
      BuildContext context, String title, int sortId, bool isSeleted) {
    return SizedBox(
        child: GestureDetector(
      onTap: () {
        controller.selectedSortId.value = sortId;
        controller.selectedSortName.value = title;
      },
      child: Container(
          alignment: AlignmentDirectional.center,
          width: 100.w,
          height: 50.w,
          padding: EdgeInsets.only(top: 10.w),
          decoration: BoxDecoration(
              color: isSeleted == true ? AppColors.ffbfa46d : AppColors.surface,
              borderRadius: BorderRadius.circular(8.w)),
          child: Text(title,
              style: isSeleted == true
                  ? AppTextStyles.ffffffff(
                      context,
                      fontSize: 20,
                    )
                  : AppTextStyles.bodyText2(
                      context,
                      fontSize: 20,
                    ))),
    ));
  }

  Widget _renderSortedContent(BuildContext context, List<Mission> data) {
    var _data = data
        .where(
            (item) => item.sortId == controller.selectedSortId.value.toString())
        .toList();

    List<Widget> temp = [];
    for (var item in _data) {
      temp.add(SizedBox(
          width: 1.sw,
          child: GestureDetector(
              onTap: () => AppToast.showDuration(msg: item.missionDesc),
              child: Container(
                child: Padding(
                    padding: const EdgeInsets.all(20).w,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        AppImage.network(img_root('images/dicon', type: 'png'),
                            width: 30.w, height: 30.w, fit: BoxFit.contain),
                        SizedBox(
                          width: 10.w,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${item.missionName}  ',
                                style: AppTextStyles.ff000000(context,
                                    fontSize: 18, fontWeight: FontWeight.bold)),
                            SizedBox(
                              height: 10.h,
                            ),
                            Text(
                              '+${double.parse(item.integral ?? '0').toStringAsFixed(4)} 截止时间:  ${DateFormat('yyyy-MM-dd').format(item.overTime)}',
                              style: AppTextStyles.ffcd9f4b_(context,
                                  fontSize: 16),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            if (item.algorithmData.now != null &&
                                item.algorithmData.now != "0")
                              LinearPercentIndicator(
                                width: 0.5.sw,
                                animation: true,
                                animationDuration: 2000,
                                lineHeight: 10.0.w,
                                trailing: Text(
                                  item.algorithmData.now ?? '',
                                  style: AppTextStyles.ff387ef5_(context,
                                      fontSize: 16),
                                ),
                                percent: item.algorithmData.percent == null
                                    ? 0
                                    : double.parse(item.algorithmData.percent!
                                            .substring(
                                                0,
                                                item.algorithmData.percent!
                                                        .length -
                                                    1)) /
                                        100,
                                linearGradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    AppColors.ffBDA386,
                                    AppColors.ffa78954
                                  ],
                                ),
                                barRadius: const Radius.circular(10),
                              ),
                          ],
                        ),
                        const Spacer(),
                        Container(
                          width: 100.w,
                          height: 46.h,
                          alignment: Alignment.centerRight,
                          decoration: BoxDecoration(
                              color: AppColors.ffbfa46d,
                              borderRadius: BorderRadius.circular(20.w)),
                          child: GestureDetector(
                            onTap: () {
                              controller.requestTask(item, context);
                            },
                            child: Center(
                              child: Text(
                                item.status == '0'
                                    ? '领任务'
                                    : item.status == '1'
                                        ? '去完成'
                                        : item.status == '2'
                                            ? "已领取"
                                            : "领奖励",
                                style: AppTextStyles.ffffffff(
                                  context,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
              ))));
    }
    return Column(
      children: temp,
    );
  }
}
