import 'package:flutter/material.dart';
import 'package:fpg_flutter/data/models/MissionModel.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view_controller.dart';
import 'package:get/get.dart';

class ScoreCard extends StatelessWidget {
  ScoreCard({super.key});
  final List<String> _list = [
    '全部日期',
    '最近一天',
    '最近7天',
    '最近一个月',
  ];
  MissionCenterViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.surface,
      child: Column(
        children: [
          Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            border: TableBorder.all(color: AppColors.ff9e9e9e),
            children: [
              TableRow(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.w),
                    child: Text(
                      '账变类型',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.ff666666(context, fontSize: 18),
                    ),
                  ),
                  Text(
                    '分数',
                    textAlign: TextAlign.center,
                    style: AppTextStyles.ff666666(context, fontSize: 16),
                  ),
                  Text(
                    '分数账变',
                    textAlign: TextAlign.center,
                    style: AppTextStyles.ff666666(context, fontSize: 16),
                  ),
                  Obx(
                    () => DropdownButton(
                      value: controller.dropdownValue.value,
                      underline: Container(
                        height: 0,
                        color: Colors.transparent,
                      ),
                      // icon: const Icon(
                      //   Icons.keyboard_arrow_down,
                      //   weight: 10.0,
                      // ),
                      style: AppTextStyles.ff666666(context, fontSize: 16),
                      items: _list.map((String item) {
                        return DropdownMenuItem(
                          value: item,
                          child: Padding(
                            padding: EdgeInsets.only(left: 15.w),
                            child: Text(item),
                          ),
                        );
                      }).toList(),
                      onChanged: (String? newValue) {
                        controller.dropdownValue.value = newValue!;
                        controller.selectItem();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Obx(
                () => controller.scoreList.isNotEmpty == true
                    ? Table(
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.middle,
                        border: TableBorder.all(color: AppColors.ff9e9e9e),
                        children: [
                          ...List.generate(controller.scoreList.length,
                              (index) {
                            CreditsUList temp;
                            temp = controller.scoreList.elementAt(index);
                            return unitRow(
                                context,
                                temp.type ?? '',
                                temp.integral ?? '',
                                temp.newInt ?? '',
                                temp.addTime ?? '',
                                18);
                          }),
                        ],
                      )
                    : const Text('暂无数据'),
              ),
            ),
          ),
          SizedBox(
            height: 20.w,
          ),
        ],
      ),
    );
  }
}

TableRow unitRow(BuildContext context, String data1, String data2, String data3,
    String data4, int fontsize) {
  return TableRow(children: [
    unitCell(context, data1, fontsize),
    unitCell(context, data2, fontsize),
    unitCell(context, data3, fontsize),
    unitCell(context, data4, fontsize),
  ]);
}

Widget unitCell(BuildContext context, String data, int fontsize) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 15.w),
    child: Text(
      data,
      textAlign: TextAlign.center,
      style: AppTextStyles.ff666666(context, fontSize: fontsize),
    ),
  );
}
