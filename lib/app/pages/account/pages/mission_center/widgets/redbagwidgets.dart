import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/jdsign_view_controller.dart';
import 'package:get/get.dart';

class RedBagWidget extends Container {
  RedBagWidget({
    super.key,
    required this.integral,
    this.checkinState,
    this.imgPath,
    this.month,
    this.week,
    this.day,
  });

  String? checkinState = ''; //1,2,3:[check, activeuncheck,uncheck]
  int? month = 7;
  int? day = 1;
  String? week = '星期一';
  String? integral;
  String? imgPath;

  String coinImage1 =
      'https://gsyahcot002uqmhd.aaasfjyt.com/html/qiandao/images/zh/money3.png';
  String coinImage2 =
      'https://gsyahcot002uqmhd.aaasfjyt.com/html/qiandao/images/zh/money.gif';
  JdsiginViewController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    String monthStr = (month! < 10) ? '0${month}' : '${month}';
    String dayStr = (day! < 10) ? '0${day}' : '${day}';
    return Column(
      children: [
        Text(
          '${monthStr}月${dayStr}日',
          style: AppTextStyles.ff000000(context, fontSize: 16),
        ),
        Text(
          week!,
          style: AppTextStyles.ff000000(context, fontSize: 16),
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.w),
            image: DecorationImage(
              image: NetworkImage(imgPath!),
              fit: BoxFit.fill,
            ),
          ),
          child: Stack(alignment: Alignment.center, children: [
            Opacity(
                opacity: (checkinState == '已签到') ? 1 : 0.7,
                child: SvgPicture.network(
                  imgPath ?? "",
                  width: 0.2.sw, // Adjust as needed
                )),
            (checkinState == '已签到')
                ? SizedBox(
                    width: 0.2.sw,
                    height: 120.h,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '+$integral',
                            textAlign: TextAlign.center,
                            style:
                                AppTextStyles.surface_(context, fontSize: 16),
                          ),
                          SizedBox(
                            height: 15.h,
                          ),
                        ]))
                : SizedBox(
                    width: 0.2.sw,
                    height: 120.h,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15.h,
                        ),
                        (checkinState != '已签到')
                            ? AppImage.network(
                                checkinState != '签到' ? coinImage2 : coinImage1,
                                width: 40.w,
                                height: 40.h,
                                fit: BoxFit.contain)
                            : const SizedBox(),
                        SizedBox(
                          height: 15.h,
                        ),
                        Text(
                          '+$integral',
                          textAlign: TextAlign.center,
                          style: AppTextStyles.surface_(context, fontSize: 16),
                        ),
                      ],
                    ),
                  )
          ]),
        ),
      ],
    );
  }
}

Widget insertLine() {
  return SizedBox(
    width: 1.sw,
    height: 1.sp,
    child: const ColoredBox(color: AppColors.ffffffff), // New color property
  );
}
