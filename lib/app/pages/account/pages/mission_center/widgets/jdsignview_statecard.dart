import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/widgets/redbagwidgets.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/jdsign_view_controller.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

import 'package:get/get.dart';

class JdSignViewStateCard extends Container {
  JdSignViewStateCard(
      {super.key,
      required this.content1,
      required this.content2,
      required this.callId});
  String title = '连续签到礼包';
  final String? content1;
  final String? content2;
  final String? callId;

  final JdsiginViewController controller = Get.find();
  String coinImage =
      'https://gsyahcot002uqmhd.aaasfjyt.com/html/qiandao/images/dou.png';
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.surface,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(10.w),
            child: Text(
              title,
              style: AppTextStyles.ff000000(context, fontSize: 26),
            ),
          ),
          insertLine(),
          Padding(
            padding: EdgeInsets.only(
              right: 5.w,
              left: 5.w,
              top: 10.w,
              bottom: 10.w,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 0.w),
                  child: Row(
                    children: [
                      AppImage.network(coinImage, width: 60.w, height: 60.w),
                      SizedBox(
                        child: Column(
                          children: [
                            Text(
                              content1 ?? '',
                              style:
                                  AppTextStyles.ff000000(context, fontSize: 26),
                            ),
                            Text(
                              content2 ?? '',
                              style: AppTextStyles.ff000000(
                                context,
                                fontSize: 24,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 50.w,
                ),
                SizedBox(
                  width: 120.w,
                  height: 50.h,
                  child: ElevatedButton(
                    child: Text(
                      '领取',
                      style: AppTextStyles.ffffffff(
                        context,
                        fontSize: 20,
                      ),
                    ),
                    onPressed: () {
                      controller.checkinBonusData(context, callId ?? '7');
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColors.ffc5c5c5,
                      // padding: EdgeInsets.symmetric(vertical: 1.h),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(1.w),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          insertLine(),
          SizedBox(
            width: 1.sw,
            height: 5.w,
          ),
        ],
      ),
    );
  }
}

class Panwidgets extends Container {
  Panwidgets({
    super.key,
  });
  JdsiginViewController controller = Get.find();

  String signedImg =
      'https://gsyahcot002uqmhd.aaasfjyt.com/html/qiandao/images/red_bag.svg';
  String unsignedImg =
      'https://gsyahcot002uqmhd.aaasfjyt.com/html/qiandao/images/red_bag2.svg';
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 0.95.sw,
      decoration: BoxDecoration(
        color: AppColors.surface,
        borderRadius: BorderRadius.circular(4.w),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2), // Shadow color
            spreadRadius: 5, // Spread radius
            blurRadius: 7, // Blur radius
            offset: Offset(0, 1.w), // Offset of the shadow
          ),
          BoxShadow(
            color: Colors.blue.withOpacity(0.3), // Another shadow color
            spreadRadius: 5,
            blurRadius: 10,
            offset: Offset(0, 2.w),
          ),
        ],
      ),
      child: Obx(
        () => controller.checkData.value.checkinList != null
            ? _renderCheckList(context)
            : Padding(
                padding: EdgeInsets.only(top: 200.w),
                child: Column(
                  children: [
                    AppImage.network(img_root('images/lhc/search_noData'),
                        width: 100.w, height: 100.w),
                    Text(
                      '暂无数据',
                      style: AppTextStyles.ff000000(context, fontSize: 16),
                    ),
                  ],
                )),
      ),
    );
  }

  Widget _renderCheckList(
    BuildContext context,
  ) {
    List<Widget> temp = [];
    int len = controller.checkData.value.checkinList == null
        ? 0
        : controller.checkData.value.checkinList!.length;

    for (int j = 0; j <= len / 4 - 1; j++) {
      temp.add(Column(children: [
        Obx(() => Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(4, (index) {
              String integral = controller.checkData.value.checkinList!
                  .elementAt(index + j * 4)
                  .integral
                  .toString();
              String dateString = controller.checkData.value.checkinList!
                  .elementAt(index + j * 4)
                  .updateTime!;
              String week = controller.checkData.value.checkinList!
                  .elementAt(index + j * 4)
                  .week!;
              DateTime dateTime = DateTime.parse(dateString);
              double cardOpacity = controller.cardOpacity[index + j * 4].value;
              return Obx(
                () => GestureDetector(
                  onTapDown: (details) {
                    if (!controller.isOneSelected) {
                      controller.cardOpacity[index + j * 4].value = 0.5;
                    }
                  },
                  onTapUp: (_) {
                    controller.cardOpacity[index + j * 4].value = 1;
                    if (!controller.isOneSelected) {
                      controller.itemAction(context, index + j * 4);
                    }
                  },
                  child: Opacity(
                    opacity: cardOpacity,
                    child: RedBagWidget(
                      imgPath: (controller.checkData.value.checkinList
                                  ?.elementAt(index + j * 4)
                                  .isCheckin ??
                              false)
                          ? signedImg
                          : unsignedImg,
                      integral: integral,
                      month: dateTime.month,
                      day: dateTime.day,
                      week: week,
                      checkinState: controller.checkinState(index + j * 4),
                    ),
                  ),
                ),
              );
            }))),
        SizedBox(
          height: 30.h,
        )
      ]));
    }

    if (len % 4 > 0) {
      int k = (len / 4).floor();
      temp.add(Column(
        children: [
          Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(len % 4, (index) {
                String integral = controller.checkData.value.checkinList!
                    .elementAt(index + k * 4)
                    .integral
                    .toString();
                String dateString = controller.checkData.value.checkinList!
                    .elementAt(index + k * 4)
                    .updateTime!;
                String week = controller.checkData.value.checkinList!
                    .elementAt(index + k * 4)
                    .week!;
                DateTime dateTime = DateTime.parse(dateString);
                double cardOpacity =
                    controller.cardOpacity[index + k * 4].value;
                return GestureDetector(
                  onTapDown: (details) {
                    controller.cardOpacity[index + k * 4].value = 0.5;
                  },
                  onTapUp: (_) {
                    controller.cardOpacity[index + k * 4].value = 1;
                    if (!controller.isOneSelected) {
                      controller.itemAction(context, index + k * 4);
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16).w,
                    child: Opacity(
                        opacity: cardOpacity,
                        child: RedBagWidget(
                          imgPath: (controller.checkData.value.checkinList
                                      ?.elementAt(index + k * 4)
                                      .isCheckin ??
                                  false)
                              ? signedImg
                              : unsignedImg,
                          integral: integral,
                          month: dateTime.month,
                          day: dateTime.day,
                          week: week,
                          checkinState: controller.checkinState(index + k * 4),
                        )),
                  ),
                );
              }))),
          SizedBox(
            height: 30.h,
          )
        ],
      ));
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: temp,
    );
  }
}
