import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view_controller.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/routes.dart';

class Exchangecard extends StatefulWidget {
  Exchangecard({super.key});

  @override
  State<Exchangecard> createState() => _Exchangecard();
}

class _Exchangecard extends State<Exchangecard> {
  double datavalue = 0;
  final getMoneyArr = ['10', '50', '100', '500', '全部兑换'];
  final MissionCenterViewController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: AppColors.surface,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 30.w,
          ),
          SizedBox(
            height: 30.w,
            child: Obx(
              () => Text(
                '${controller.missionBili.value}分数: 1元人民币',
                style: AppTextStyles.ff000000(
                  context,
                  fontSize: 18,
                ),
              ),
            ),
          ),
          SizedBox(height: 10.w),
          Container(
            // height: 50.w,

            margin: EdgeInsets.only(
              left: 20.w,
              right: 20.w,
            ),
            padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
              border: Border.all(color: AppColors.ff9e9e9e, width: 1.w),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 100.w,
                  // height: 50.w,
                  child: SpinBox(
                    value: datavalue,
                    textStyle: AppTextStyles.ffff0000_(context, fontSize: 20),
                    onChanged: (value) {
                      controller.selectedBili.value = value;
                      controller.state_data.value =
                          (value == 0) ? '确认兑换' : '${value / 10}';
                    },
                    showButtons: false,
                    max: 10000,
                    decoration: const InputDecoration(
                      // contentPadding: EdgeInsets.only(top: 0),
                      hintText: '输入分数',
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                    ),
                  ),
                ),
                AppImage.network(img_mobileTemplate('0', 'change'),
                    width: 20.w, height: 20.h, fit: BoxFit.contain),
                SizedBox(
                  width: 100.w,
                  child: Obx(() => Text(controller.state_data.value,
                      style: AppTextStyles.ffff0000_(context, fontSize: 20))),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.w),
            alignment: Alignment.center,
            width: 120.w,
            height: 50.w,
            padding: EdgeInsets.only(top: 2.w),
            decoration: const BoxDecoration(
              color: AppColors.ffcf352e,
            ),
            child: GestureDetector(
                onTap: () {
                  controller.requestExchange();
                  Get.offNamedUntil(
                      taskPath,
                      arguments: [2],
                      (route) => route.settings.name == homePath);
                },
                child: Text(
                  '确认兑换',
                  style: AppTextStyles.ffffffff(
                    context,
                    fontSize: 18,
                  ),
                )),
          ),
          SizedBox(
            height: 30.w,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: getMoneyArr.map(
                (e) {
                  return CustomButton(
                    data: e,
                    onPressed: () {
                      setState(() {
                        if (e != '全部兑换') {
                          datavalue = double.parse(e);
                          controller.selectedBili.value = datavalue;
                          controller.state_data.value = (datavalue /
                                  double.parse(controller.missionBili.value))
                              .toStringAsFixed(0);
                          setState(() {});
                        } else {
                          datavalue = double.parse(
                              controller.infoReward.value.toString());
                          controller.selectedBili.value = datavalue;
                          controller.state_data.value = (datavalue /
                                  double.parse(controller.missionBili.value))
                              .toStringAsFixed(0);
                          setState(() {});
                        }
                      });
                    },
                  );
                },
              ).toList(),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  CustomButton({super.key, required this.data, this.onPressed});
  final String data;
  VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90.w,
      height: 50.w,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(
          color: AppColors.ff9e9e9e,
          width: 1.w,
        ),
      ),
      child: Text(
        data,
        style: AppTextStyles.ff000000(
          context,
          fontSize: 16,
        ),
      ),
    ).onTap(onPressed);
  }
}
