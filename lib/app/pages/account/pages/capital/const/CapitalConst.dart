import 'package:fpg_flutter/routes.dart';

List<Map<String, String>> categoryTabs = ([
  {
    'id': '0',
    'name': '存款',
  },
  {
    'id': '1',
    'name': '取款',
  },
  {'id': '2', 'name': '存款记录'},
  {'id': '3', 'name': '取款记录'},
  {'id': '4', 'name': '资金明细'}
]);

List<String> picName = [
  'qd-icon.svg',
  'transfer.png',
  'xnbcz-icon.png',
  'online-okp.png',
  'cbrpay-online.jpg',
  'pending_order.png',
  'online-embedwallet.png',
  'online-808pay.png',
  'qiandai.png'
];

List<Map<String, String>> tileItems = [
  {'title': "抢单存款", 'subtitle': "安全快捷"}, //qd_online
  {'title': "银行转账", 'subtitle': "支持大额入款，您的首选"}, //bank_transfer
  {'title': "虚拟币充值", 'subtitle': "虚拟币充值"}, //xnb_transfer
  {'title': "okp在线支付", 'subtitle': "okp在线支付"}, //okp_online
  {'title': "C币 Pay 钱包", 'subtitle': " C币 Pay 钱包"}, //cbrpay_online
  {'title': "挂单充值", 'subtitle': "挂单充值"}, //pending_order
  {'title': "内嵌钱包", 'subtitle': "内嵌钱包支付"}, //embedwallet_online
  {'title': "808PAY钱包", 'subtitle': "808PAY钱包"}, //balingba_online
];

List<String> picNameForWithdraw = ['qd-icon.svg', 'qiandai.png'];

List<Map<String, String>> tileItemsForWithdraw = [
  {'title': "余额提款", 'subtitle': "安全快捷", 'url': ''},
  {'title': "抢单提款", 'subtitle': "支持大额入款，您的首选", 'url': grabPath},
];
