import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';

List<Map<String, String>> categoryTabs = ([
  {
    'id': '0',
    'name': '火币',
  },
  {
    'id': '1',
    'name': '币安',
  },
]);

List<UGinviteInfoModel> list = [
  UGinviteInfoModel(
    title: '概述',
    isPress: true,
    content: '1',
  ),
  UGinviteInfoModel(
    title: '安装钱包',
    isPress: false,
    content: '2',
  ),
  UGinviteInfoModel(
    title: '注册账号',
    isPress: false,
    content: '3',
  ),
  UGinviteInfoModel(
    title: '购买虚拟币',
    isPress: true,
    content: '4',
  ),
  UGinviteInfoModel(
    title: '确认存款金额',
    isPress: false,
    content: '5',
  ),
  UGinviteInfoModel(
    title: '钱包付款',
    isPress: true,
    content: '6',
  ),
  UGinviteInfoModel(
    title: '完成充值',
    isPress: false,
    content: '7',
  ),
];

List<UGinviteInfoModel> cgList = [
  UGinviteInfoModel(
    title: '安装CGpay',
    isPress: true,
    content: '1',
  ),
  UGinviteInfoModel(
    title: '注册账号',
    isPress: false,
    content: '2',
  ),
  UGinviteInfoModel(
    title: '购买虚拟币',
    isPress: false,
    content: '3',
  ),
  UGinviteInfoModel(
    title: '确定存款金额',
    isPress: true,
    content: '4',
  ),
  UGinviteInfoModel(
    title: '钱包付款',
    isPress: false,
    content: '5',
  ),
];

List<UGinviteInfoModel> okexList = [
  UGinviteInfoModel(
    title: '概述',
    isPress: true,
    content: '1',
  ),
  UGinviteInfoModel(
    title: '安装虚拟币钱包',
    isPress: false,
    content: '2',
  ),
  UGinviteInfoModel(
    title: '注册账号',
    isPress: false,
    content: '3',
  ),
  UGinviteInfoModel(
    title: '身份认证',
    isPress: true,
    content: '4',
  ),
  UGinviteInfoModel(
    title: '购买虚拟币',
    isPress: false,
    content: '5',
  ),
  UGinviteInfoModel(
    title: '确认存款金额',
    isPress: false,
    content: '6',
  ),
  UGinviteInfoModel(
    title: '钱包付款',
    isPress: true,
    content: '7',
  ),
  UGinviteInfoModel(
    title: '完成充值',
    isPress: false,
    content: '8',
  ),
];
