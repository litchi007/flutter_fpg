import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/dp/deposit_record_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/wd/withdrawal_record_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/wt/withdrawal_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/const/CapitalConst.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/pay_list_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/dl/capitaldetail_list_view.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/capital_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/wt/withdrawal_view_controller.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';

class DepositHomeView extends StatefulWidget {
  const DepositHomeView({super.key});

  @override
  _DepositHomeViewState createState() => _DepositHomeViewState();
}

class _DepositHomeViewState extends State<DepositHomeView>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  late TabController _tabController;
  DepositHomeViewController controller = Get.put(DepositHomeViewController());
  final WithdrawalViewController withdrawalController =
      Get.put(WithdrawalViewController());
  bool _reload = false;
  final AuthController authController = Get.find();
  bool firstInit = true;
  List<String> titleNames = [
    '资金管理',
    '提现',
    '存款记录',
    '取款记录',
    '资金明细',
  ];
  @override
  void initState() {
    super.initState();
    List<int>? args = Get.arguments;
    int defaultTabIndex = args?[0] ?? 0;

    controller.heightHeader.value = 0;
    _tabController = TabController(
        length: categoryTabs.length,
        vsync: this,
        initialIndex: defaultTabIndex);
    _tabController.addListener(_handleTabChange);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _handleTabChange();
    });

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.linear,
      ),
    );
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging || firstInit) {
      bool temp = false;
      if (_tabController.index == 1) {
        if (GlobalService.to.userInfo.value?.hasFundPwd == null ||
            GlobalService.to.userInfo.value?.hasFundPwd == false) {
          AppToast.showDuration(msg: "请先设置取款密码");
          Get.toNamed(safeCenterPath, arguments: [1]);
        }
        controller.heightHeader.value = 90;
        temp = true;
      } else {
        controller.heightHeader.value = 0;
      }

      controller.showAvatar.value = temp;
      controller.index.value = _tabController.index;
      withdrawalController.showBalanceWithdrawal.value = false;
      firstInit = false;
    }
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabChange);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          backgroundColor: AppColors.surface,
          appBar: AppBar(
            backgroundColor: AppColors.ffbfa46d,
            automaticallyImplyLeading: false,
            titleSpacing: 0,
            leading: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15).w,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                    // Get.offNamedUntil(
                    //     homePath, (route) => route.settings.name == homePath);
                  },
                  child: Row(children: [
                    Icon(Icons.arrow_back_ios,
                        color: AppColors.surface, size: 22.w),
                  ]),
                )),
            title: Text(titleNames[controller.index.value],
                style: AppTextStyles.surface_(context,
                    fontWeight: FontWeight.bold, fontSize: 24)),
            centerTitle: true,
            leadingWidth: 0.3.sw,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(
                  kToolbarHeight.h + controller.heightHeader.value.h),
              child: Container(
                color: AppColors.surface,
                child: Column(
                  children: [
                    if (controller.showAvatar.value) _createAvatar(context),
                    _createTab(context),
                    SizedBox(
                      width: 1.sw,
                      height: 5.h,
                      child: const ColoredBox(color: AppColors.ffeaeaea),
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: SafeArea(
              child: Column(children: [
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  PayListView(),
                  const WithdrawView(),
                  const DepositRecordView(),
                  const WithdrawalRecordView(),
                  const CapitaldetailListView(type: 'FundingDetails')
                ],
              ),
            )
          ])),
        ));
  }

  TabBar _createTab(BuildContext context) {
    return TabBar(
      controller: _tabController,
      indicatorColor: AppColors.ffff0000,
      labelColor: AppColors.ffff0000,
      unselectedLabelColor: AppColors.ff5C5869,
      indicatorSize: TabBarIndicatorSize.tab,
      physics: const ClampingScrollPhysics(),
      tabAlignment: TabAlignment.center,
      isScrollable: false,
      labelStyle: AppTextStyles.surface_(context,
          fontWeight: FontWeight.bold, fontSize: 18),
      tabs: List.generate(categoryTabs.length, (index) {
        return Tab(
          text: '${categoryTabs[index]['name']}',
        );
      }),
    );
  }

  Padding _createAvatar(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(40, 16, 16, 4).w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          AppImage.network(
              GlobalService.to.userInfo.value?.isTest == true
                  ? AppDefine.defaultAvatar
                  : GlobalService.to.userInfo.value?.avatar ??
                      AppDefine.defaultAvatar,
              width: 75.w,
              height: 75.h,
              fit: BoxFit.contain),
          SizedBox(
            height: 76.h,
            child: Padding(
              padding: const EdgeInsets.only(left: 30).w,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Spacer(
                    flex: 3,
                  ),
                  Text('${GlobalService.to.userInfo.value?.usr}',
                      style: AppTextStyles.bodyText2(context, fontSize: 18)),
                  const Spacer(
                    flex: 1,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                          controller.showAvatar.value == true
                              ? controller.showYuebao.value == true
                                  ? '${AppDefine.systemConfig?.yuebaoName}余额:'
                                  : '用户余额： '
                              : '',
                          style:
                              AppTextStyles.bodyText2(context, fontSize: 16)),
                      Text(
                        controller.infoBalance.value,
                        style: AppTextStyles.error_(context, fontSize: 16),
                      ),
                      Text(
                        'RMB',
                        style: AppTextStyles.bodyText2(context, fontSize: 16),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 14.0),
                        child: AnimatedBuilder(
                          animation: _spinAnimation,
                          builder: (context, child) {
                            return Transform.rotate(
                              angle: _spinAnimation.value * 2 * 3.14159,
                              child: Container(
                                width: 18,
                                height: 18,
                                decoration: BoxDecoration(
                                  color: AppColors.ff8acfb8,
                                  borderRadius: BorderRadius.circular(9),
                                  border: Border.all(
                                    color: AppColors.surface,
                                    width: 2,
                                  ),
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    if (!_reload) {
                                      setState(() {
                                        _reload = true;
                                      });
                                      _controller.forward(from: 0).then((_) {
                                        _controller.reset();
                                        setState(() {
                                          _reload = false;
                                        });
                                      });
                                      controller.refreshUserInfo();
                                    }
                                  },
                                  child: const Icon(
                                    Icons.autorenew_rounded,
                                    color: AppColors.surface,
                                    size: 12,
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  const Spacer(
                    flex: 3,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
