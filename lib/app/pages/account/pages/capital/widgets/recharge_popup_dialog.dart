import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/pay_list_view_controller.dart';
import 'package:fpg_flutter/services/global_service.dart';

class CustomPopUpDialog extends StatelessWidget {
  String title;
  final String content;
  List<dynamic> dataForDialog = <dynamic>[];
  final Color titleBackgroundColor;
  final BuildContext context;
  final double? borderRadius;
  // final PayListViewController controller = Get.find();
  final TextEditingController _fullNameController = TextEditingController();
  var controller;
  dynamic action;
  CustomPopUpDialog(
      {required this.context,
      required this.title,
      this.borderRadius,
      required this.content,
      required this.titleBackgroundColor,
      this.action = 'normal',
      this.controller});
  Text _createTitle() {
    return Text(
      title,
      style: AppTextStyles.headline1(context, fontSize: 24),
      textAlign: TextAlign.center,
    );
  }

  void setTitle(String title) {
    title = title;
  }

  Column _createContent() {
    if (content != 'AddingFullName') {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              content,
              style: AppTextStyles.bodyText1(context, fontSize: 20),
            )
          ]);
    } else {
      return Column(
        children: [
          Row(
            children: [
              Text(
                '请务必填写真实姓名(',
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
              Text(
                '填写后不可修改',
                style: AppTextStyles.error_(context, fontSize: 20),
              ),
              Text(
                ')',
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
            ],
          ),
          Container(
            height: 60.h,
            child: TextField(
              controller: _fullNameController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: '真实姓名',
              ),
            ),
          ),
        ],
      );
    }
  }

  List<Widget>? _createActions() {
    if (action is String) {
      return [
        GestureDetector(
            onTapDown: (done) {
              if (content != 'AddingFullName') {
                controller.dialogShowing.value = false;

                GlobalService.to.rechargePopUpAlarmShowing = false;
              } else {
                controller.withdrawalDialogShowing.value = false;
              }
            },
            child: Container(
              width: 0.35.sw,
              height: 60.h,
              decoration: BoxDecoration(
                  color: AppColors.ffF3F3F3,
                  border: Border.all(
                    color: AppColors.ffC5CBC6,
                    width: 1,
                  ),
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "取消",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.bodyText2(context, fontSize: 15),
                  ),
                ],
              ),
            )),
        SizedBox(
          width: 30.w,
        ),
        GestureDetector(
            onTapDown: (done) {
              if (content != 'AddingFullName') {
                controller.dialogShowing.value = false;
                GlobalService.to.rechargePopUpAlarmShowing = false;
              } else {
                controller.withdrawalDialogShowing.value = false;
                controller.bindRealName(_fullNameController.text);
                AppToast.show(msg: GlobalService.to.userInfo.value?.fullName);
              }
            },
            child: Container(
              width: 0.35.sw,
              height: 60.h,
              decoration: BoxDecoration(
                  color: AppColors.ffbfa46d,
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 8))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "确定",
                    textAlign: TextAlign.center,
                    style: AppTextStyles.surface_(context, fontSize: 15),
                  ),
                ],
              ),
            ))
      ];
    } else if (action is Widget) {
      return [action];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: AppColors.surface,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius ?? 8))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 0.85.sw,
            height: 60.h,
            decoration: BoxDecoration(
                color: titleBackgroundColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(borderRadius ?? 8),
                    topRight: Radius.circular(borderRadius ?? 8))),
            child: SizedBox(
              width: .8.sw,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: _createTitle(),
              ),
            ),
          ),
          SizedBox(
            width: 0.8.sw,
            child: Padding(
              padding: const EdgeInsets.all(8).w,
              child: _createContent(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: _createActions() ?? [const SizedBox()],
          ),
          if (action is String)
            SizedBox(
              height: 20.h,
            )
        ],
      ),
    );
  }
}
