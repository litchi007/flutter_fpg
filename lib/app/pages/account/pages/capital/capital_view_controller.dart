import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/helper.dart';

class DepositHomeViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxInt index = 0.obs;
  RxBool showAvatar = false.obs;
  RxBool showYuebao = false.obs;
  RxDouble heightHeader = 0.0.obs;
  RxString infoBalance = ''.obs;
  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  void refreshUserInfo() {
    GlobalService.to.getUserInfo();
    infoBalance.value =
        (convertToDouble(GlobalService.to.userInfo.value?.balance ?? '') ?? 0)
            .toStringAsFixed(4);
  }

  void fetchData() async {
    infoBalance.value =
        (convertToDouble(GlobalService.to.userInfo.value?.balance ?? '') ?? 0)
            .toStringAsFixed(4);
  }
}
