import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';

class TransNameModel {
  String? bankName;
  String? bankNameDes;
  String? payee;
  String? payeeDes;
  String? bankAccount;
  String? bankAccountDes;
  String? accountAddress;
  String? accountAddressDes;
  String? transHint;
  String? type; // 银行 还是 其它

  // Constructor
  TransNameModel({
    required this.bankName,
    required this.bankNameDes,
    required this.payee,
    required this.payeeDes,
    required this.bankAccount,
    required this.bankAccountDes,
    required this.accountAddress,
    required this.accountAddressDes,
    required this.transHint,
    required this.type,
  });

  // Factory constructor to create an instance from a Map
  factory TransNameModel.fromJson(Map<String?, dynamic> map) {
    return TransNameModel(
      bankName: map['bank_name'] as String?,
      bankNameDes: map['bank_name_des'] as String?,
      payee: map['payee'] as String?,
      payeeDes: map['payee_des'] as String?,
      bankAccount: map['bank_account'] as String?,
      bankAccountDes: map['bank_account_des'] as String?,
      accountAddress: map['account_address'] as String?,
      accountAddressDes: map['account_address_des'] as String?,
      transHint: map['trans_hint'] as String?,
      type: map['type'] as String?,
    );
  }

  // Method to convert the instance to a Map
  Map<String?, dynamic> toJson() {
    return {
      'bank_name': bankName,
      'bank_name_des': bankNameDes,
      'payee': payee,
      'payee_des': payeeDes,
      'bank_account': bankAccount,
      'bank_account_des': bankAccountDes,
      'account_address': accountAddress,
      'account_address_des': accountAddressDes,
      'trans_hint': transHint,
      'type': type,
    };
  }
}

Map<String, String> TMP = {
  'bank_name': '姓名: ',
  'payee': '账号: ',
  'bank_account': '银行名称: ',
  'account_address': '支行名称: ',
  'type': 'other',
};

Map<String, String> BNK = {
  'bank_name': '银行名称: ',
  'payee': '收款人: ',
  'bank_account': '银行账号: ',
  'account_address': '开户地址: ',
  'type': 'bank',
};

Map<String, String> UPI = {
  'payee': 'UPI账号: ',
  'type': 'upi',
};

Map<String, String> BNK_ALI = {
  'bank_name': '银行名称: ',
  'payee': '收款人: ',
  'bank_account': '银行账号: ',
  'type': 'bank',
};

Map<String, String> LB = {
  'bank_name': '银行名称: ',
  'payee': '收款人: ',
  'bank_account': '聊呗账号: ',
  'account_address': '开户地址: ',
  'type': 'bank',
};

Map<String, String> transferName(
    {PayItemType? payData,
    // The type PayChannelBean is not defined, assuming it's a custom class
    PayChannelBean? payChannelBean}) {
  Map<String, String> transInfo = {};

  switch (payData) {
    case PayItemType.ez_recharge:
    case PayItemType.bank_transfer:
    case PayItemType.zfbzyhk_transfer:
    case PayItemType.zfbzk_transfer:
    case PayItemType.ysf_transfer:
    case PayItemType.siyu_transfer:
    case PayItemType.truemoney_transfer:
    case PayItemType.pending_order:
      transInfo = {
        ...BNK,
        'trans_hint': '请填写实际转账人姓名',
      };
      break;
    case PayItemType.upi_transfer:
      transInfo = {
        ...UPI,
        'trans_hint': '请填写付款的UPI用户昵称',
      };
      break;
    case PayItemType.bankalipay_transfer:
      transInfo = {
        ...BNK_ALI,
        'trans_hint': '请填写付款的银行支付宝昵称',
      };
      break;
    case PayItemType.alipay_transfer:
    case PayItemType.alihb_online:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的支付宝真实姓名',
      };
      break;
    case PayItemType.tenpay_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的财付通用户昵称',
      };
      break;
    case PayItemType.wechat_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的微信用户昵称',
      };
      break;
    case PayItemType.quick_online:
    case PayItemType.wxzfbsm_transfer:
    case PayItemType.wxzsm_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的账号',
      };
      break;
    case PayItemType.momo_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的账号',
      };
      break;
    case PayItemType.qqpay_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的QQ钱包用户昵称',
      };
      break;
    case PayItemType.wechat_alipay_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的微信支付宝用户昵称',
      };
      break;
    case PayItemType.jdzz_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的京东钱包真实姓名',
      };
      break;
    case PayItemType.ddhb_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的钉钉真实姓名',
      };
      break;
    case PayItemType.dshb_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的多闪账号',
      };
      break;
    case PayItemType.xlsm_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款的闲聊账号',
      };
      break;
    case PayItemType.wxsm_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写微信昵称或商户单号后六位',
      };
      break;
    case PayItemType.liaobei_transfer:
      transInfo = {
        ...LB,
        'trans_hint': '请填写付款的聊呗用户昵称',
      };
      break;
    case PayItemType.viettelpay_transfer:
      transInfo = {
        ...TMP,
        'trans_hint': '请填写付款订单号后六位或viettelpay用户昵称',
      };
      break;
    default:
      transInfo = {
        'trans_hint': '未知支付类型',
      };
      break;
  }

  return transInfo;
}
