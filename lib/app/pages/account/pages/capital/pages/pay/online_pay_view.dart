import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/online_pay_view_controller.dart';
import 'package:board_datetime_picker/board_datetime_picker.dart';

import 'package:webview_flutter/webview_flutter.dart';

class OnlinePayView extends StatefulWidget {
  const OnlinePayView({super.key});

  @override
  State<OnlinePayView> createState() => _OnlinePayViewState();
}

class _OnlinePayViewState extends State<OnlinePayView>
    with SingleTickerProviderStateMixin {
  OnlinePayViewController controller = Get.put(OnlinePayViewController());
  final TextEditingController _depositsController = TextEditingController();

  final textController = BoardDateTimeTextController();
  int selectedGender = 0;
  List<String> moneyOption = [];

  DateTime? lastClickTime;

  @override
  void initState() {
    super.initState();

    controller.payData.value = Get.arguments[0];
    controller.payIndex = Get.arguments[1];
    setMoneyOption();
    controller.fetchDepositPrompt();
    controller.isColoured = List.generate(moneyOption.length, (_) => false.obs);
  }

  bool _validateInput(value) {
    final numberRegExp = RegExp(r'^-?\d+(\.\d+)?$');
    if (!numberRegExp.hasMatch(value)) {
      _depositsController.clear();
      return false;
    } else {
      return true;
    }
  }

  void setMoneyOption() {
    moneyOption = controller.payData.value.quickAmount?.split(' ') ?? [];

    if (controller.payData.value.channel2?[0] != null &&
        controller.payData.value.channel2?[0].para != null) {
      moneyOption =
          controller.payData.value.channel2?[0].para?.fixedAmount?.split(' ') ??
              [];
    }
    List<String> temp = [];
    for (var item in moneyOption) {
      if (item != '') temp.add(item);
    }
    moneyOption.clear();
    moneyOption = temp;
  }

  @override
  Widget build(BuildContext context) {
    controller.context = context;

    return Stack(children: [
      Obx(
        () => controller.isWebViewShow.value == true
            ? WebViewWidget(
                controller: controller.webViewController,
              )
            : Scaffold(
                backgroundColor: AppColors.surface,
                appBar: AppBar(
                  backgroundColor: AppColors.ffbfa46d,
                  automaticallyImplyLeading: false,
                  titleSpacing: 0,
                  leading: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15).w,
                      child: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Row(children: [
                          Icon(Icons.arrow_back_ios,
                              color: AppColors.surface, size: 28.w),
                        ]),
                      )),
                  title: Text(controller.payData.value.name ?? '',
                      style: AppTextStyles.surface_(context,
                          fontWeight: FontWeight.bold, fontSize: 24)),
                  centerTitle: true,
                  leadingWidth: 0.3.sw,
                ),
                body: Stack(alignment: Alignment.topCenter, children: [
                  ListView(children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16).h,
                      child: Center(
                        child: SizedBox(
                          width: 0.9.sw,
                          child: Column(
                            children: [
                              _amountItem('depositAmount', '请填写存款金额',
                                  _depositsController),
                              _paymentMethodItem(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  _text(controller.payData.value.prompt ?? ''),
                                ],
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                        width: 0.9.sw,
                                        child: Text(
                                          controller.transferPrompt.value,
                                          style: AppTextStyles.error_(context,
                                              fontSize: 20),
                                          overflow: TextOverflow.visible,
                                        )),
                                  ]),
                              _comboBox(),
                              _setButton(0, '开始充值', false),
                            ],
                          ),
                        ),
                      ),
                    )
                  ]),
                ]),
              ),
      )
    ]);
  }

  Widget _circleWithText(String text, double diameter) {
    return Container(
      width: diameter, // Diameter of the circle
      height: diameter, // Diameter of the circle
      decoration: const BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        // Optional: Border for better visibility
      ),
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            fontSize: 20.0.w,
            fontWeight: FontWeight.bold,
            color: AppColors.error,
          ),
        ),
      ),
    );
  }

  Widget _datetimePicker() {
    return Container(
        padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
        width: 0.9.sw,
        height: 54.h,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
        child: Stack(alignment: Alignment.centerRight, children: [
          BoardDateTimeInputField(
            controller: textController,
            initialDate: DateTime.now(),
            pickerType: DateTimePickerType.datetime,
            options: const BoardDateTimeOptions(
                languages: BoardPickerLanguages.en(),
                pickerFormat: PickerFormat.ymd),
            textStyle: AppTextStyles.bodyText1(context, fontSize: 20),
            textAlign: TextAlign.left,
            onChanged: (date) {
              print('onchanged: $date');
            },
            onFocusChange: (val, date, text) {
              print('on focus changed date: $val, $date, $text');
            },
          ),
          GestureDetector(
            onTap: () {},
            child: SizedBox(
              width: 30.w,
              child: Icon(
                Icons.calendar_month,
                size: 25.w,
              ),
            ),
          ).paddingOnly(right: 5.w),
        ]));
  }

  void _onTap() {
    final currentTime = DateTime.now();
    // 检查是否在 1 秒内重复点击
    if (lastClickTime == null ||
        currentTime.difference(lastClickTime!) > const Duration(seconds: 1)) {
      lastClickTime = currentTime;
      // 执行点击逻辑
      controller.requestPayData();
    }
  }

  SizedBox _setButton(int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: _onTap,
          child: Container(
            width: 0.9.sw,
            height: 54.h,
            margin: const EdgeInsets.only(top: 16).h,
            decoration: BoxDecoration(
              color: AppColors.ffbfa46d,
              borderRadius: BorderRadius.circular(4).w,
            ),
            child: Center(
              child: Text(
                amount,
                style: AppTextStyles.surface_(context, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          )),
    );
  }

  Widget _paymentMethodItem() {
    List<Widget> temp = [];
    int len = moneyOption.length;

    for (int j = 0; j <= len / 3 - 1; j++) {
      temp.add(Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(3, (index) {
            return _unitTransferItem(
                context,
                index + j * 3,
                moneyOption[index + j * 3],
                controller.isColoured[index + j * 3].value);
          }))));
    }

    if (len % 3 > 0) {
      int k = (len / 3).floor();
      temp.add(Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(
            len % 3,
            (index) => _unitTransferItem(
                context,
                index,
                moneyOption[index + k * 3],
                controller.isColoured[index + k * 3].value),
          ))));
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: temp,
    );
  }

  Widget _amountItem(
      String type, String hint, TextEditingController editController) {
    return Container(
      padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
      width: 0.9.sw,
      height: 54.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
      child: Obx(() => Focus(
          onFocusChange: (hasFocus) {
            setState(() {
              controller.hasFocus[type] = hasFocus;
            });
          },
          child: TextFormField(
            onChanged: (text) {
              if (_validateInput(text)) {
                type == 'depositAmount'
                    ? controller.depositAmount = text
                    : type == 'remarks'
                        ? controller.remarkContent = text
                        : controller.transferorName = text;
              }
            },
            keyboardType: type == 'withdrawCode'
                ? TextInputType.visiblePassword
                : TextInputType.text,
            obscureText: type == 'withdrawCode' ? true : false,
            controller: editController,
            cursorColor: AppColors.onBackground,
            style: AppTextStyles.ff535B70_(context, fontSize: 18),
            textAlign: TextAlign.left,
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(horizontal: 20).w,
                iconColor: AppColors.surface,
                filled: true,
                fillColor: Colors.transparent,
                hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
                hintText: hint,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: AppColors.onBackground,
                    width: 1.0.w,
                  ),
                ),
                suffixIcon: (GestureDetector(
                    onTap: () {
                      editController.clear();
                    },
                    child: controller.hasFocus[type] ?? false
                        ? Icon(
                            size: 23.w, color: AppColors.ff8D8B8B, Icons.close)
                        : const SizedBox()))),
          ))),
    );
  }

  SizedBox _unitTransferItem(
      BuildContext context, int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.changeColorTransItem(index);
            controller.depositAmount = amount;
            _depositsController.text = amount;
          },
          child: Stack(alignment: Alignment.bottomRight, children: [
            Container(
              width: 0.29.sw,
              height: 48.h,
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 16).h,
              decoration: BoxDecoration(
                color: isColoured ? AppColors.surface : AppColors.ffeeeeee,
                borderRadius: BorderRadius.circular(2).w,
                border: Border.all(
                  color: isColoured ? AppColors.fffd7328 : AppColors.ffeeeeee,
                ),
              ),
              child: Text(
                amount,
                style: isColoured
                    ? AppTextStyles.ffBFA46D_(context, fontSize: 18)
                    : AppTextStyles.ff666666_(context, fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),
          ])),
    );
  }

  Widget _text(
    String title, {
    int fontSize = 20,
  }) {
    return SizedBox(
      width: 0.7.sw,
      child: Padding(
        padding: const EdgeInsets.all(8.0).w,
        child: Text(title,
            style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
      ),
    );
  }

  Widget _textButton(
    String title, {
    int fontSize = 20,
  }) {
    return GestureDetector(
        onTap: () => controller.pressRec(),
        child: Padding(
          padding: const EdgeInsets.all(8.0).w,
          child: Text(title,
              style: AppTextStyles.error_(context, fontSize: fontSize)),
        ));
  }

  Widget _headerText(String title, {int fontSize = 20}) {
    return Padding(
      padding: const EdgeInsets.all(8.0).w,
      child: Text(title,
          style: AppTextStyles.ff999999_(context, fontSize: fontSize)),
    );
  }

  Widget _comboBox() {
    return Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1.w, color: AppColors.ffe1ebef),
        ),
        child: Padding(
            padding: const EdgeInsets.all(8).w,
            child: Column(children: [
              RadioListTile<int>(
                title: _text(
                    controller.payData.value.channel2?[0].payeeName ?? ''),
                value: 0,
                groupValue: selectedGender,
                onChanged: (int? value) {
                  setState(() {
                    selectedGender = value ?? 0;
                  });
                },
              ),
            ])));
  }

  Widget _textContent() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1.w, color: AppColors.ffe1ebef),
      ),
      child: Padding(
        padding: EdgeInsets.all(8).w,
        child: Column(
          children: [
            Row(
              children: [
                _text(
                    '银行名称: ${controller.payData.value.channel2?[0].branchAddress}'),
                const Spacer(),
                _textButton('复制')
              ],
            ),
            Row(
              children: [
                _text('收款人: ${controller.payData.value.channel2?[0].domain}'),
                const Spacer(),
                _textButton('复制')
              ],
            ),
            Row(
              children: [
                _text('银行账号: ${controller.payData.value.channel2?[0].account}'),
                const Spacer(),
                _textButton('复制')
              ],
            ),
            Row(
              children: [
                _text(
                    '开户地址: ${controller.payData.value.channel2?[0].fcomment}'),
                const Spacer(),
                _textButton('复制')
              ],
            ),
          ],
        ),
      ),
    );
  }
}
