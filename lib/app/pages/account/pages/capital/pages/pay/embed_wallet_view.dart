import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/embed_wallet_view_controller.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:fpg_flutter/configs/app_define.dart';

class EmbedWalletView extends StatefulWidget {
  const EmbedWalletView({super.key});

  @override
  State<EmbedWalletView> createState() => _EmbedWalletViewState();
}

class _EmbedWalletViewState extends State<EmbedWalletView>
    with TickerProviderStateMixin {
  EmbedWalletViewController controller = Get.put(EmbedWalletViewController());
  final TextEditingController _conversionAmountController =
      TextEditingController();
  late AnimationController controllerLeft;
  late Animation<double> spinAnimationLeft;
  late AnimationController controllerRight;
  late Animation<double> spinAnimationRight;
  bool reloadLeft = false;
  bool reloadRight = false;

  @override
  void initState() {
    super.initState();
    controller.payBigData = Get.arguments[0];

    controller.payBigData.channel?.sort(
        (a, b) => (int.parse(b.id ?? '0').compareTo(int.parse(a.id ?? '0'))));

    controllerLeft = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    spinAnimationLeft = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(controllerLeft);

    controllerRight = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    spinAnimationRight = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(controllerRight);
  }

  bool _validateInput(value) {
    final numberRegExp = RegExp(r'^-?\d+(\.\d+)?$');
    if (!numberRegExp.hasMatch(value)) {
      _conversionAmountController.clear();
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15).w,
              child: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.surface, size: 28.w),
                ]),
              )),
          title: Text('虚拟钱包转换',
              style: AppTextStyles.surface_(context,
                  fontWeight: FontWeight.bold, fontSize: 24)),
          centerTitle: true,
          leadingWidth: 0.3.sw,
        ),
        body: ListView(children: [
          Center(
              child: SizedBox(
                  width: 0.9.sw,
                  child: Column(children: [
                    SizedBox(
                      height: 16.h,
                    ),
                    _renderPaymentType(),
                    SizedBox(
                      height: 16.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Obx(() => Text(
                              '${controller.payBigData.channel?[controller.methodIndex.value].payeeName}钱包',
                              style: AppTextStyles.ff17c492_(context,
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    _renderTransferBlock(),
                    SizedBox(
                      height: 16.h,
                    ),
                    _renderWalletAddress(),
                    SizedBox(
                      height: 16.h,
                    ),
                    _renderInputMoney(),
                    SizedBox(
                      height: 50.h,
                    ),
                    _finalButtonCP('确认划转', AppColors.fffb5050, 0),
                    SizedBox(
                      height: 20.h,
                    ),
                    _finalButtonCP('前往买币', AppColors.ff17c492, 1),
                  ])))
        ]));
  }

  Widget _finalButtonCP(String title, Color color, int index) {
    return ClipRRect(
        borderRadius: BorderRadius.circular(6.w),
        child: GestureDetector(
            onTap: () {
              if (index == 0) {
                controller.requestPayData();
              } else {
                controller.buyVirtual();
              }
            },
            child: Container(
              width: 0.9.sw,
              height: 52.h,
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(6).w,
              ),
              child: Center(
                child: Text(
                  title,
                  style: AppTextStyles.surface_(context, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),
            )));
  }

  Widget _renderInputMoney() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          '金额',
          style: AppTextStyles.bodyText2(context, fontSize: 20),
        ),
        _inputAmountCP(),
        GestureDetector(
          onTap: () {
            controller.selectAllAmount();
          },
          child: _selectAllAmountCP('全部'),
        ),
      ],
    );
  }

  Widget _inputAmountCP() {
    return Container(
        padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
        width: 320.w,
        height: 36.h,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
        child: TextFormField(
          onChanged: (text) {
            if (_validateInput(text)) {
              controller.inputMoney = text;
            }
          },
          controller: _conversionAmountController,
          cursorColor: AppColors.onBackground,
          style: AppTextStyles.ff535B70_(context, fontSize: 18),
          textAlign: TextAlign.left,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(horizontal: 20).w,
            iconColor: AppColors.surface,
            filled: true,
            fillColor: Colors.transparent,
            hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
            hintText: '请输入金额',
            border: const OutlineInputBorder(
              borderSide: BorderSide.none,
            ),
          ),
        ));
  }

  Widget _selectAllAmountCP(String title) {
    return ClipRRect(
        borderRadius: BorderRadius.circular(6.w),
        child: Container(
          width: 74.w,
          height: 36.h,
          decoration: BoxDecoration(
            color: AppColors.ff17c492,
            borderRadius: BorderRadius.circular(6).w,
          ),
          child: Center(
            child: Text(
              title,
              style: AppTextStyles.bodyText2(context, fontSize: 20),
              textAlign: TextAlign.center,
            ),
          ),
        ));
  }

  Widget _renderWalletAddress() {
    return Container(
        width: 0.9.sw,
        height: 44.h,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: AppColors.a17c49212,
          borderRadius: BorderRadius.circular(6).w,
        ),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          //textcolor:  ff004c36
          AppImage.network(
            img_images('embedwallet/address'),
            width: 20.w,
            height: 20.w,
          ),
          // embedWalletData?.walletAddress

          Text(
            controller.embedWalletData,
            style: AppTextStyles.ff17c492_(context, fontSize: 20),
          ),

          const Spacer(),
          GestureDetector(
            onTap: () => AppToast.showDuration(msg: '复制成功'),
            child: AppImage.network(
              img_images('embedwallet/copy'),
              width: 20.w,
              height: 20.w,
            ),
          )
        ]));
  }

  Widget _renderTransferBlock() {
    return Container(
        width: 0.9.sw,
        height: 100.h,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: AppColors.fffafafa,
          borderRadius: BorderRadius.circular(6).w,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Obx(() => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    _balanceItem(
                        '${controller.payBigData.channel?[controller.methodIndex.value].payeeName}余额',
                        '${currencyLogo[AppDefine.systemConfig?.currency ?? ''] ?? ''} ${controller.embedWalletData}',
                        refreshWidgetLeft()),
                    Column(
                      children: [
                        Text(
                          '转入',
                          style: AppTextStyles.ff17c492_(context, fontSize: 20),
                        ),
                        AppImage.network(
                          img_images('embedwallet/transfer'),
                          width: 50.w,
                          height: 20.w,
                        ),
                      ],
                    ),
                    _balanceItem(
                        '系统余额',
                        '${currencyLogo[AppDefine.systemConfig?.currency] ?? ''} ${(convertToDouble(GlobalService.to.userInfo.value?.balance ?? '0') ?? 0).toStringAsFixed(2)}',
                        refreshWidgetRight()),
                  ],
                ))
          ],
        ));
  }

  Widget refreshWidgetLeft() {
    return SizedBox(
        child: AnimatedBuilder(
      animation: spinAnimationLeft,
      builder: (context, child) {
        return Transform.rotate(
            angle: spinAnimationLeft.value * 6 * 3.14159,
            child: GestureDetector(
              onTap: () {
                if (!reloadLeft) {
                  setState(() {
                    reloadLeft = true;
                  });

                  controllerLeft.forward(from: 0).then((_) {
                    controllerLeft.reset();
                    setState(() {
                      reloadLeft = false;
                    });
                  });

                  controller.requestEmbedWalletData();
                }
              },
              child: Icon(
                Icons.autorenew_rounded,
                color: AppColors.ffCAC2C2,
                size: 28.w,
              ),
            ));
      },
    ));
  }

  Widget refreshWidgetRight() {
    return SizedBox(
        child: AnimatedBuilder(
      animation: spinAnimationRight,
      builder: (context, child) {
        return Transform.rotate(
          angle: spinAnimationRight.value * 6 * 3.14159,
          child: GestureDetector(
            onTap: () {
              if (!reloadRight) {
                setState(() {
                  reloadRight = true;
                });

                controllerRight.forward(from: 0).then((_) {
                  controllerRight.reset();
                  setState(() {
                    reloadRight = false;
                  });
                });
                controller.requestEmbedWalletData();
              }
            },
            child: Icon(
              Icons.autorenew_rounded,
              color: AppColors.ffCAC2C2,
              size: 28.w,
            ),
          ),
        );
      },
    ));
  }

  Widget _balanceItem(String title, String amount, Widget refreshWidget) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title,
          style: AppTextStyles.textColor3_(context, fontSize: 20),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              amount,
              style: AppTextStyles.error_(context, fontSize: 20),
            ),
            refreshWidget
          ],
        ),
      ],
    );
  }

  Widget _renderPaymentType() {
    return Container(
        width: 0.9.sw,
        height: 60.h,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: AppColors.ffdfdfdf, //
          borderRadius: BorderRadius.circular(6).w,
        ),
        child: Row(
            children: controller.payBigData.channel
                    ?.map((item) => _methodButton(
                        controller
                                .payBigData
                                .channel?[controller.payBigData.channel
                                        ?.indexOf(item) ??
                                    0]
                                .payeeName ??
                            '',
                        controller.payBigData.channel?.indexOf(item) ?? 0))
                    .toList() ??
                []));
  }

  Widget _methodButton(String title, int index) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0).w,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(6.w),
          child: GestureDetector(
              onTap: () {
                controller.methodIndex.value = index;
                controller.requestEmbedWalletData();
              },
              child: Obx(() => Container(
                    width: 143.w,
                    height: 40.h,
                    decoration: BoxDecoration(
                      color: controller.payBigData.channel?[index].payeeName ==
                              controller
                                  .payBigData
                                  .channel?[controller.methodIndex.value]
                                  .payeeName
                          ? AppColors.fffb5050
                          : AppColors.surface,
                      borderRadius: BorderRadius.circular(6).w,
                    ),
                    child: Center(
                      child: Text(
                        title,
                        style: controller
                                    .payBigData.channel?[index].payeeName ==
                                controller
                                    .payBigData
                                    .channel?[controller.methodIndex.value]
                                    .payeeName
                            ? AppTextStyles.surface_(context, fontSize: 20)
                            : AppTextStyles.bodyText2(context, fontSize: 20),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )))),
    );
  }
}
