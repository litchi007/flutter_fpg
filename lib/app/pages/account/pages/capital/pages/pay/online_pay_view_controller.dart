import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
// import 'package:fpg_flutter/http/UGEncryptParams.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:http/http.dart' as http;
import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:flutter/material.dart';

import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';

class OnlinePayViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();

  RxMap<String, bool> hasFocus = {
    'depositAmount': false,
    'remarks': false,
    'transferor': false,
  }.obs;
  RxBool isWebViewShow = false.obs;
  List<RxBool> isColoured = [];
  WebViewController webViewController = WebViewController();
  Rx<PayAisleListData> payData = PayAisleListData().obs;
  List<PayAisleListData> payBigData = [];
  int payIndex = 0;
  RxString payResult = ''.obs;
  BuildContext? context;
  List<int> banks = [];
  String depositAmount = '';
  String remarkContent = '';
  String transferorName = '';
  int prevIndex = 0;
  RxString transferPrompt = ''.obs;
  String? coinPwd;
  void changeColorTransItem(int index) {
    isColoured[index].value = isColoured[index].value ? false : true;
    isColoured[prevIndex].value = false;
    prevIndex = index;
  }

  void pressRec() async {
    AppToast.showDuration(msg: '复制成功');
  }

  void fetchDepositPrompt() async {
    await _appRechargeRepository.cashier().then((data) {
      if (data == null) return;
      transferPrompt.value = data.transferPrompt ?? '';
    });
  }

  void fetchCashier() async {
    await _appRechargeRepository.cashier().then((data) {
      if (data == null) return;
      transferPrompt.value = data.transferPrompt ?? '';
      payBigData = data.payment ?? [];
      payData.value = payBigData[payIndex];
    });
  }

  bool verify() {
    double maximum = 0;
    double minimum = 0;

    maximum = double.parse(payData.value.channel2?[0].maximum ?? '0');
    minimum = double.parse(payData.value.channel2?[0].mininum ?? '0');
    if (maximum == 0 && minimum == 0) {
      return true;
    }

    if (maximum != 0 && minimum != 0) {
      if (double.parse(depositAmount) < minimum ||
          double.parse(depositAmount) > maximum) {
        AppToast.showDuration(
          msg: '请输入金额 $minimum 至 $maximum 的金额',
        );
        return false;
      }
    } else if (maximum == 0 &&
        minimum != 0 &&
        double.parse(depositAmount) < minimum) {
      AppToast.showDuration(
        msg: '最低充值金额: $minimum',
      );
      return false;
    } else if (minimum == 0 &&
        maximum != 0 &&
        double.parse(depositAmount) > maximum) {
      AppToast.showDuration(
        msg: '最高充值金额: $maximum',
      );
      return false;
    }
    return true;
  }

  void requestPayData() async {
    Map<String, String> params = {
      'payId': payData.value.channel2?[0].id ?? '',
      'gateway': payData.value.channel2?[0].para?.bankList == null ||
              payData.value.channel2?[0].para?.bankList?.isEmpty == true
          ? '0'
          : payData.value.channel2?[0].para?.bankList?[0].code ?? '',
      'money': depositAmount,
      'c': 'recharge',
      'a': 'payUrl',
      'token': AppDefine.userToken?.apiSid ?? ''
    };

    // payResult.value = await getByHttpWithParams(params) ?? '';

    // openUrlWithParams(params);
    //  Depends on RN cipherparams But it doesn't work.

    Uri baseUri =
        Uri.parse('${AppDefine.host}/wjapp/api.php?c=recharge&a=payUrl');
    Uri finalUri = baseUri.replace(queryParameters: params);
    // initWebView(finalUri);
    getByUrlLanch(finalUri);
    // Get.toNamed(webAppPage, arguments: [uri.toString(), payData.value.name ?? '']);
    // http.get(Uri.parse(data)).then((data) {});

/* 
// *******  This is dart code for copying the  part of converting the param to cipherparams  ************
 await _appRechargeRepository
        .onlinePay(
      money: depositAmount,
      payId: payData.value.channel2?[0].id,
      gateway: payData.value.channel2?[0].para?.bankList == null ||
              payData.value.channel2?[0].para?.bankList?.isEmpty == true
          ? '0'
          : payData.value.channel2?[0].para?.bankList?[0].code,
    )
        .then((data) {
      Uri uri = Uri.parse('?$data');
      //   Map<String, dynamic> params = {
      //     'payId': queryParams['payId'].toString(),
      //     'gateway': queryParams['gateway'].toString(),
      //     'money': queryParams['money'].toString(),
      //     'coinpwd': coinPwd?.toString()
      //   };

      //   Map<String, String> resultEncrypted = encryptParams(params);

      //   if (verify()) {
      //     // *******  This is dart code for copying the  part of converting the param to cipherparams  ************

      //     getByHttpWithParams(resultEncrypted);
      //     // openUrlWithParams(resultEncrypted);
      //   }
    });*/
  }

  Future<String>? getByHttpWithParams(Map<String, String> resultEncrypted) {
    Uri baseUri =
        Uri.parse('${AppDefine.host}/wjapp/api.php?c=recharge&a=payUrl');
    resultEncrypted['c'] = 'recharge';
    resultEncrypted['a'] = 'payUrl';
    resultEncrypted['token'] = AppDefine.userToken?.apiSid ?? '';

    Uri finalUri = baseUri.replace(queryParameters: resultEncrypted);
    String readableUrl = finalUri.toString();

    try {
      http.get(Uri.parse(readableUrl)).then((data) {
        AppToast.showDuration(msg: data.body);
        // AppToast.showDuration(msg: jsonResponse['msg']);
        // return jsonResponse['msg'];
      });
    } catch (e) {
      AppLogger.e('An error occurred: $e');
    }
    return null;
  }

  String buildUrl(String baseUrl, Map<String, String>? queryParams) {
    final uri = Uri.parse(baseUrl);
    final uriWithParams = uri.replace(queryParameters: queryParams);
    return uriWithParams.toString();
  }

  void openUrlWithParams(Map<String, String> encryptedParams) async {
    final baseUrl =
        '${AppDefine.host}/wjapp/api.php?c=recharge&a=payUrl'; // Replace with your base URL

    Uri uri = Uri.parse(baseUrl);

    Uri uriWithParams = uri.replace(queryParameters: encryptedParams);
    getByWebView(uriWithParams);
    // getByUrlLanch(uriWithParams);
  }

  void getByUrlLanch(Uri uriWithParams) async {
    if (await canLaunchUrl(uriWithParams)) {
      await launchUrl(uriWithParams);
    }
  }

  void getByWebView(Uri uriWithParams) {
    Get.toNamed(webAppPage,
        arguments: [uriWithParams.toString(), payData.value.name ?? '']);
  }

  void initWebView(Uri uri) {
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
            _injectJavaScript();
          },
          onHttpError: (HttpResponseError error) {
            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            // openDialog(request);
          },
        ),
      )
      // ..loadRequest(Uri.parse('${AppDefine.host}/service/member.html'));
      ..loadRequest(uri);
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }

    webViewController = controller;
    isWebViewShow.value = true;
  }

  void _injectJavaScript() async {
    String jsCode = """
    document.querySelectorAll('body *').forEach(function(node) {
      var currentSize = window.getComputedStyle(node).fontSize;
      var newSize = parseFloat(currentSize) * 2 + 'px';  // Increase font size by 50%
      node.style.fontSize = newSize;
    });
    """;
    await webViewController.runJavaScript(jsCode);
  }
}
