import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/timezone.dart';

class TransferPayViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();
  Rx<PayAisleListData> payData = PayAisleListData().obs;
  List<PayAisleListData> payBigData = [];
  int payIndex = 0;
  RxMap<String, bool> hasFocus = {
    'depositAmount': false,
    'remarks': false,
    'transferor': false,
  }.obs;
  List<RxBool> isColoured = [];
  RxString depositPrompt = ''.obs;
  List<int> banks = [];
  String depositAmount = '';
  String remarkContent = '';
  String transferorName = '';
  int prevIndex = 0;

  void updatePayChannel(int index) {
    isColoured[index].value == true
        ? banks.add(index + 1)
        : banks.remove(index + 1);
  }

  void changeColorTransItem(int index) {
    isColoured[index].value = isColoured[index].value ? false : true;
    isColoured[prevIndex].value = false;
    prevIndex = index;
  }

  void pressRec() async {
    ToastUtils.show('复制成功');
  }

  void fetchCashier() async {
    await _appRechargeRepository.cashier().then((data) {
      if (data == null) return;
      depositPrompt.value = data.depositPrompt ?? '';
      payBigData = data.payment ?? [];
      payData.value = payBigData[payIndex];
    });
  }

  void fetchDepositPrompt() async {
    await _appRechargeRepository.cashier().then((data) {
      if (data == null) return;
      depositPrompt.value = data.depositPrompt ?? '';
    });
  }

  void requestPayData() async {
    int time = (DateTime.now().millisecondsSinceEpoch / 1000)
        .round(); // Example timestamp

    String serverTime = TimeZone.getTzTimeBySecond(time);

    await _appRechargeRepository
        .transfer(
      amount: depositAmount,
      payee: payData.value.channel?[0].account,
      channel: payData.value.channel?[0].id,
      remark: remarkContent,
      payer: transferorName,
      // payer: transferorName,
      depositTime: serverTime,
    )
        .then((data) {
      AppToast.showDuration(msg: data.msg);
      if (data.code == 0) {
        Get.offNamedUntil(
            depositPath,
            arguments: [2],
            (route) => route.settings.name == homePath);
      }
      if (data == null) return;
    });
  }
}
