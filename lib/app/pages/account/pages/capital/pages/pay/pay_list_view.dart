import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/pay_list_view_controller.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/widgets/recharge_popup_dialog.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_alert_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:flutter_html/flutter_html.dart';

class PayListView extends StatefulWidget {
  const PayListView({super.key});
  @override
  PayListViewState createState() => PayListViewState();
}

class PayListViewState extends State<PayListView> {
  final PayListViewController controller = Get.put(PayListViewController());
  CustomAlertDialog? rechargePopUpAlarmDialog;

  @override
  Widget build(BuildContext context) {
    CustomAlertDialog customAlertDialog = CustomAlertDialog(
        context: context,
        title: '确认要退出该帐号？',
        categories: [],
        titleBackgroundColor: AppColors.surface,
        action: 'logout');

    return Obx(() => (controller.isIniting.value)
        ? const CustomLoadingWidget()
        : controller.payBigData.isEmpty
            ? GestureDetector(
                onTap: () {
                  customAlertDialog.dialogBuilder([]);
                },
                child: Container(
                  width: 1.sw,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 18).h,
                      child: Text(
                        '退出登录',
                        style: AppTextStyles.bodyText2(context, fontSize: 22),
                      ),
                    ),
                  ),
                ))
            : Stack(alignment: Alignment.center, children: [
                ListView.builder(
                  itemCount: controller.payBigData.length,
                  itemBuilder: (context, index) {
                    var data = controller.payBigData[index];
                    LogUtil.w(data.toJson());
                    return Column(children: [
                      GestureDetector(
                          onTap: () =>
                              controller.goToSubPageandGetIcon(index, true),
                          child: ListTile(
                            title: Row(
                              children: [
                                Text('${data.name}',
                                    style: AppTextStyles.bodyText3(context,
                                        fontSize: 22)),
                                const Spacer(),
                                (index != 0 && index != 1 && index != 6)
                                    ? GestureDetector(
                                        onTap: () => controller.goToTutorial(
                                            data.id, data.name),
                                        child: Container(
                                          width: 70.w,
                                          height: 28.h,
                                          margin: EdgeInsets.only(
                                              right: 20.w, top: 7.h),
                                          decoration: BoxDecoration(
                                              color: AppColors.ffeaeaea,
                                              borderRadius:
                                                  const BorderRadius.all(
                                                          Radius.circular(8))
                                                      .w),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Text(
                                                "充值教程",
                                                textAlign: TextAlign.center,
                                                style: AppTextStyles.headline2(
                                                    context,
                                                    fontSize: 14),
                                              ),
                                            ],
                                          ),
                                        ))
                                    : SizedBox(width: 70.w)
                              ],
                            ),
                            subtitle: Html(
                              data:
                                  "<span style='color:red;font-size:14px'> ${data.tip}</span>",
                            ),
                            leading: data.id == PayItemType.qd_online
                                ? SvgPicture.network(
                                    img_images('qd-icon', type: 'svg'),
                                    width: 45.w,
                                    height: 45.h,
                                    fit: BoxFit.contain)
                                : AppImage.network(
                                    (controller
                                        .getRechargeChannelIcon(data.idString)),
                                    width: 45.w,
                                    height: 45.h,
                                    fit: BoxFit.contain),
                          )),
                      Divider(
                        height: 2.h,
                      )
                    ]);
                  },
                ),
                Positioned(
                    // top: 65.h,
                    // left: 25.w,
                    child: controller.dialogShowing.value &&
                            GlobalService.to.rechargePopUpAlarmShowing
                        ? CustomPopUpDialog(
                            context: context,
                            title: '温馨提示',
                            content: controller.dialogContent.value,
                            titleBackgroundColor: AppColors.ffF3F3F3,
                            action: 'logout',
                            controller: controller,
                          )
                        : const SizedBox())
              ]));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
