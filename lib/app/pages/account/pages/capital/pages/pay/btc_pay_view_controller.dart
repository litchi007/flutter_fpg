import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/timezone.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/YuebaoData.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:get_storage/get_storage.dart';

class BtcPayViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();
  final AppSystemRepository appSystemRepository = AppSystemRepository();
  Rx<PayAisleListData> payData = PayAisleListData().obs;
  Rx<ConverRateModel> currencyRate = ConverRateModel().obs;
  RxString depositPrompt = ''.obs;
  List<PayAisleListData> payBigData = [];
  @override
  void onInit() {
    super.onInit();
    getCurrentRate();
  }

  Future<void> fetchCashier() async {
    await _appRechargeRepository.cashier().then((data) {
      if (data == null) return;
      depositPrompt.value = data.depositPrompt ?? '';
      payBigData = data.payment ?? [];
      payData.value = payBigData[payIndex];
    });
  }

  void fetchDepositPrompt() async {
    await _appRechargeRepository.cashier().then((data) {
      if (data == null) return;
      depositPrompt.value = data.depositPrompt ?? '';
    });
  }

  void pressRec() async {
    AppToast.showDuration(msg: '复制成功');
  }

  final BANK_TYPE = [
    '¥101',
    '¥201',
    '¥301',
    '¥401',
    '¥500',
    '¥1000',
    '¥2000',
    '¥3000',
    '¥5000',
  ];
  RxMap<String, bool> hasFocus = {
    'depositAmount': false,
    'remarks': false,
    'transferor': false,
  }.obs;
  RxDouble newRate = 0.0.obs;
  RxDouble newUsd = 0.0.obs;

  RxString coinTxt = ''.obs;
  List<RxBool> isColoured = List.generate(9, (_) => false.obs);
  List<int> banks = [];
  RxString depositAmount = ''.obs;
  RxString transferorName = ''.obs;
  RxString remarkContent = ''.obs;
  RxInt selPayChannel = 0.obs;
  int payIndex = 0;

  void updatePayChannel(int index) {
    isColoured[index].value == true
        ? banks.add(index + 1)
        : banks.remove(index + 1);
  }

  void getCurrentRate() {
    appSystemRepository
        .currencyRate(
            token: AppDefine.userToken?.apiSid,
            from: AppDefine.systemConfig?.currency,
            to: payData.value.channel?[selPayChannel.value].domain == 'TRX'
                ? 'TRX'
                : 'USD',
            amount: '1',
            float: payData.value.channel?[selPayChannel.value].branchAddress)
        .then((data) {
      if (data == null) return;
      currencyRate.value = data;
      rateMoney(double.parse(currencyRate.value.rate ?? '0'));
    });
  }
  /**
   * 计算当前的汇率
   * 汇率 * ( 1 + 浮动汇率 / 100 ) = 结果
   * 为保证精度不丢失，对数据放大 10000倍 再缩小
   */

  void rateMoney(double convertRate) {
    PayChannelBean? channel = payData.value.channel?[selPayChannel.value];
    if (channel?.domain == 'CGP' ||
        channel?.domain == 'DOY' ||
        channel?.domain == 'GOP' ||
        channel?.domain == 'FUTP') {
      newRate.value = 1;
      newUsd.value = 1;
    } else {
      // Retrieve the floating rate, with null checks
      final floatRate = double.tryParse(channel?.branchAddress ?? '0') ?? 0.0;
      newRate.value = (convertRate * 10000 * (100 + floatRate)) / (10000 * 100);

      if (newRate.value <= 0) {
        newRate.value = 1;
      }
      newUsd.value = (100 / newRate.value).roundToDouble() / 100;
    }
  }

  void changeColorTransItem(int index) {
    isColoured[index].value = isColoured[index].value ? false : true;
  }

  void requestPayData() async {
    if (depositAmount.value == '') {
      AppToast.showDuration(msg: '请输入金额');
    }
    int time = (DateTime.now().millisecondsSinceEpoch / 1000).round();

    String serverTime = TimeZone.getTzTimeBySecond(time);
    String btcMoney =
        ((double.tryParse(depositAmount.value) ?? 0) * newRate.value)
            .toStringAsFixed(2);
    _appRechargeRepository
        .transfer(
      amount: depositAmount.value,
      payee: payData.value.channel?[selPayChannel.value].account,
      channel: payData.value.channel?[selPayChannel.value].id,
      remark: remarkContent.value,
      payer:
          "$btcMoney${payData.value.channel?[selPayChannel.value].domain ?? ''}",
      depositTime: serverTime,
    )
        .then((data) {
      AppToast.showDuration(msg: data.msg);
      if (data.code == 0) {
        Get.offNamedUntil(
            depositPath,
            arguments: [2],
            (route) => route.settings.name == homePath);
      }
    });
  }

  bool parseNumber(String input) {
    try {
      int? intValue = int.tryParse(input);
      if (intValue != null) {
        return true;
      } else {
        double? doubleValue = double.tryParse(input);
        if (doubleValue != null) {
          // depositAmount.value=
          return true;
        } else {
          depositAmount.value = '0';
          return false;
        }
      }
    } catch (e) {
      // coinTxt.value = '0';
      depositAmount.value = '0';
      return false;
    }
  }
}
