import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:get/state_manager.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';

class EmbedWalletViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();
  String inputMoney = '';
  RxInt methodIndex = 0.obs;
  List<String> balanceItemTitle = ['波币内嵌支付錢包', 'VIPpay米娅錢包'];
  List<String> balanceItemSubTitle = ['波币内嵌支付余额', 'VIPpay米娅余额'];
  dynamic embedWalletData = '';
  PayAisleListData payBigData = PayAisleListData();
  RxBool isIniting = true.obs;
  String setCurSelBank = '';
  @override
  void onReady() {
    isIniting.value = true;
    super.onReady();
    // requestEmbedWalletData();
  }

  void selectAllAmount() async {
    if (embedWalletData == '') requestEmbedWalletData();
    inputMoney = embedWalletData.balance;
  }

  void requestPayData() async {
    Map<String, String> params = {
      'payId': payBigData.channel2?[0].id ?? '',
      'gateway': payBigData.channel2?[0].para?.bankList == null ||
              payBigData.channel2?[0].para?.bankList?.isEmpty == true
          ? '0'
          : payBigData.channel2?[0].para?.bankList?[0].code ?? '',
      'money': inputMoney,
      'c': 'recharge',
      'a': 'payUrl',
      'token': AppDefine.userToken?.apiSid ?? ''
    };
    Uri baseUri =
        Uri.parse('${AppDefine.host}/wjapp/api.php?c=recharge&a=payUrl');
    Uri finalUri = baseUri.replace(queryParameters: params);
    getByUrlLanch(finalUri);
  }

  void getByUrlLanch(Uri uriWithParams) async {
    if (await canLaunchUrl(uriWithParams)) {
      await launchUrl(uriWithParams);
    }
  }

  void buyVirtual() async {
    AppToast.showDuration(msg: '伺服器异常');
  }

  void requestEmbedWalletData() async {
    await _appRechargeRepository
        .payLoginCoin(payId: payBigData.channel?[methodIndex.value].id)
        .then((data) {
      isIniting.value = false;
      setCurSelBank = payBigData.channel2?[0].para?.bankList?.isEmpty == true
          ? '0'
          : payBigData.channel2?[0].para?.bankList?[0].code ?? '0';
      if (data == null) return;
      embedWalletData = data;
      // payBigData.value = data.payment!;
    });
  }
}
