import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/transfer_pay_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/models/ITransName.dart';
import 'package:board_datetime_picker/board_datetime_picker.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';

import 'package:flutter/services.dart';

class TransferPayView extends StatefulWidget {
  const TransferPayView({super.key});

  @override
  State<TransferPayView> createState() => _TransferPayViewState();
}

enum Gender { male, female, other }

class _TransferPayViewState extends State<TransferPayView>
    with SingleTickerProviderStateMixin {
  TransferPayViewController controller = Get.put(TransferPayViewController());
  final TextEditingController _depositsController = TextEditingController();
  final TextEditingController _paymentController = TextEditingController();
  final TextEditingController _transferorController = TextEditingController();
  TransNameModel? nameHint;
  final textController = BoardDateTimeTextController();
  int selectedGender = 0;
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  bool _reload = false;
  List<String> moneyOption = [];

  DateTime? lastClickTime;

  @override
  void initState() {
    super.initState();
    controller.payData.value = Get.arguments[0];
    controller.payIndex = Get.arguments[1];
    Map<String, dynamic> jsonNameHint = transferName(
        payData: controller.payData.value.id,
        payChannelBean: controller.payData.value.channel?[0]);
    nameHint = TransNameModel.fromJson(jsonNameHint);

    controller.fetchDepositPrompt();
    setMoneyOption();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_controller);
    controller.isColoured = List.generate(moneyOption.length, (_) => false.obs);
  }

  void copyToClipboard(String text) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      print('Text copied to clipboard!');
    });
  }

  void setMoneyOption() {
    moneyOption = controller.payData.value.quickAmount?.split(' ') ?? [];

    if (controller.payData.value.channel?[0] != null &&
        controller.payData.value.channel?[0].para != null) {
      moneyOption =
          controller.payData.value.channel?[0].para?.fixedAmount?.split(' ') ??
              [];
    }
    List<String> temp = [];
    for (var item in moneyOption) {
      if (item != '') temp.add(item);
    }
    moneyOption.clear();
    moneyOption = temp;
  }

  bool _validateInput(value) {
    final numberRegExp = RegExp(r'^-?\d+(\.\d+)?$');
    if (!numberRegExp.hasMatch(value)) {
      _depositsController.clear();
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ]),
            )),
        actions: [
          AnimatedBuilder(
            animation: _spinAnimation,
            builder: (context, child) {
              return Transform.rotate(
                angle: _spinAnimation.value * 12 * 3.14159,
                child: GestureDetector(
                  onTap: () {
                    if (!_reload) {
                      setState(() {
                        _reload = true;
                      });
                      _controller.forward(from: 0).then((_) {
                        _controller.reset();
                        setState(() {
                          _reload = false;
                        });
                      });
                      controller.fetchCashier();
                    }
                  },
                  child: Icon(
                    Icons.autorenew_rounded,
                    color: AppColors.surface,
                    size: 32.w,
                  ),
                ),
              );
            },
          ),
        ],
        title: Obx(() => Text(
            controller.payData.value.id == PayItemType.bank_transfer
                ? '银行转账'
                : controller.payData.value.name ?? '',
            style: AppTextStyles.surface_(context,
                fontWeight: FontWeight.bold, fontSize: 24))),
        centerTitle: true,
        leadingWidth: 0.3.sw,
      ),
      body: Stack(alignment: Alignment.topCenter, children: [
        ListView(children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16).h,
            child: Center(
              child: SizedBox(
                width: 0.9.sw,
                child: Column(
                  children: [
                    SizedBox(
                      height: 54.h,
                    ),
                    _comboBox(),
                    Obx(() => Container(
                        alignment: Alignment.centerLeft,
                        color: AppColors.ffff0000,
                        child: Row(children: [
                          _circleWithText("!", 20.w).paddingOnly(left: 10.w),
                          Text(controller.depositPrompt.value,
                              style: AppTextStyles.surface_(context,
                                  fontSize: 20)),
                        ]))),
                    _textContent(),
                    _amountItem(
                        'depositAmount', '请填写存款金额', _depositsController),
                    _amountItem('remarks', '请填写备注信息', _paymentController),
                    _paymentMethodItem(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Obx(() => _text(controller.payData.value.prompt ?? '')),
                      ],
                    ),
                    _amountItem(
                        'transferor', '请填写实际转账人姓名', _transferorController),
                    _datetimePicker(),
                    _setButton(0, '提交申请', false),
                  ],
                ),
              ),
            ),
          )
        ]),
        Positioned(
          top: 0,
          child: Container(
            width: 1.sw,
            padding: const EdgeInsets.symmetric(vertical: 12).h,
            decoration: const BoxDecoration(color: AppColors.ffbfa46d),
            child: Text(
              '汇款前先点击右上角“刷新”按钮，获取最新的入款通道',
              textAlign: TextAlign.center,
              style: AppTextStyles.surface_(context, fontSize: 20),
            ),
          ),
        )
      ]),
    );
  }

  Widget _datetimePicker() {
    return Container(
        padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
        width: 0.9.sw,
        height: 54.h,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
        child: Stack(
          alignment: Alignment.centerRight,
          children: [
            BoardDateTimeInputField(
              controller: textController,
              initialDate: DateTime.now(),
              pickerType: DateTimePickerType.datetime,
              options: const BoardDateTimeOptions(
                  languages: BoardPickerLanguages.en(),
                  pickerFormat: PickerFormat.ymd),
              textStyle: AppTextStyles.bodyText1(context, fontSize: 20),
              textAlign: TextAlign.left,
              onChanged: (date) {
                print('onchanged: $date');
              },
              onFocusChange: (val, date, text) {
                print('on focus changed date: $val, $date, $text');
              },
            ),
            GestureDetector(
              onTap: () {},
              child: SizedBox(
                width: 30.w,
                child: Icon(
                  Icons.calendar_month,
                  size: 25.w,
                ),
              ),
            ).paddingOnly(right: 5.w),
          ],
        ));
  }

  void _onTap() {
    final currentTime = DateTime.now();
    // 检查是否在 1 秒内重复点击
    if (lastClickTime == null ||
        currentTime.difference(lastClickTime!) > const Duration(seconds: 1)) {
      lastClickTime = currentTime;
      // 执行点击逻辑
      controller.requestPayData();
    }
  }

  SizedBox _setButton(int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: _onTap,
          child: Container(
            width: 0.9.sw,
            height: 54.h,
            margin: const EdgeInsets.only(top: 16).h,
            decoration: BoxDecoration(
              color: AppColors.ffbfa46d,
              borderRadius: BorderRadius.circular(4).w,
            ),
            child: Center(
              child: Text(
                amount,
                style: AppTextStyles.surface_(context, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          )),
    );
  }

  Widget _paymentMethodItem() {
    List<Widget> temp = [];
    int len = moneyOption.length;
    for (int j = 0; j <= len / 3 - 1; j++) {
      temp.add(Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(3, (index) {
            return _unitTransferItem(
                context,
                index + j * 3,
                moneyOption[index + j * 3],
                controller.isColoured[index + j * 3].value);
          }))));
    }
    if (len % 3 > 0) {
      int k = (len / 3).floor();
      temp.add(Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(
            len % 3,
            (index) => _unitTransferItem(
                context,
                index,
                moneyOption[index + k * 3],
                controller.isColoured[index + k * 3].value),
          ))));
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: temp,
    );
  }

  Widget _amountItem(
      String type, String hint, TextEditingController editController) {
    return Container(
      padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
      width: 0.9.sw,
      height: 54.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
      child: Obx(() => Focus(
          onFocusChange: (hasFocus) {
            setState(() {
              controller.hasFocus[type] = hasFocus;
            });
          },
          child: TextFormField(
            onChanged: (text) {
              if (type == 'depositAmount') {
                if (_validateInput(text)) controller.depositAmount = text;
              } else if (type == 'remarks') {
                controller.remarkContent = text;
              } else {
                controller.transferorName = text;
              }
            },
            keyboardType: type == 'withdrawCode'
                ? TextInputType.visiblePassword
                : TextInputType.text,
            obscureText: type == 'withdrawCode' ? true : false,
            controller: editController,
            cursorColor: AppColors.onBackground,
            style: AppTextStyles.ff535B70_(context, fontSize: 18),
            textAlign: TextAlign.left,
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(horizontal: 20).w,
                iconColor: AppColors.surface,
                filled: true,
                fillColor: Colors.transparent,
                hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
                hintText: hint,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: AppColors.onBackground,
                    width: 1.0.w,
                  ),
                ),
                suffixIcon: (GestureDetector(
                    onTap: () {
                      editController.clear();
                    },
                    child: controller.hasFocus[type] ?? false
                        ? Icon(
                            size: 23.w, color: AppColors.ff8D8B8B, Icons.close)
                        : const SizedBox()))),
          ))),
    );
  }

  SizedBox _unitTransferItem(
      BuildContext context, int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.changeColorTransItem(index);
            controller.depositAmount = amount;
            _depositsController.text = amount;
          },
          child: Stack(alignment: Alignment.bottomRight, children: [
            Container(
              width: 0.29.sw,
              height: 48.h,
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 16).h,
              decoration: BoxDecoration(
                color: isColoured ? AppColors.surface : AppColors.ffeeeeee,
                borderRadius: BorderRadius.circular(2).w,
                border: Border.all(
                  color: isColoured ? AppColors.fffd7328 : AppColors.ffeeeeee,
                ),
              ),
              child: Text(
                amount,
                style: isColoured
                    ? AppTextStyles.ffBFA46D_(context, fontSize: 18)
                    : AppTextStyles.ff666666_(context, fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),
          ])),
    );
  }

  Widget _text(
    String title, {
    int fontSize = 18,
  }) {
    return SizedBox(
      width: 0.7.sw,
      child: Padding(
        padding: const EdgeInsets.all(8.0).w,
        child: Text(title,
            style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
      ),
    );
  }

  Widget _textButton(String title, {int fontSize = 18, VoidCallback? onPress}) {
    return GestureDetector(
        onTap: onPress,
        child: Padding(
          padding: const EdgeInsets.all(8.0).w,
          child: Text(title,
              style: AppTextStyles.error_(context, fontSize: fontSize)),
        ));
  }

  Widget _headerText(String title, {int fontSize = 20}) {
    return Padding(
      padding: const EdgeInsets.all(8.0).w,
      child: Text(title,
          style: AppTextStyles.ff999999_(context, fontSize: fontSize)),
    );
  }

  Widget _comboBox() {
    return Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1.w, color: AppColors.ffe1ebef),
        ),
        child: Padding(
            padding: EdgeInsets.all(8).w,
            child: Column(children: [
              RadioListTile<int>(
                title: Obx(() => _text(
                    controller.payData.value.channel?[0].payeeName ?? '')),
                value: 0,
                groupValue: selectedGender,
                onChanged: (int? value) {
                  setState(() {
                    selectedGender = value ?? 0;
                  });
                },
              ),
            ])));
  }

  Widget _circleWithText(String text, double diameter) {
    return Container(
      width: diameter, // Diameter of the circle
      height: diameter, // Diameter of the circle
      decoration: const BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        // Optional: Border for better visibility
      ),
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            fontSize: 20.0.w,
            fontWeight: FontWeight.bold,
            color: AppColors.error,
          ),
        ),
      ),
    );
  }

  Widget _renderSelectedChannelItem(
      {String? title, String? copyText, bool showCopyButton = true}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(child: _text("$title  $copyText")),
        if (showCopyButton)
          Column(
              crossAxisAlignment:
                  CrossAxisAlignment.end, // Align button to the end
              children: [
                _textButton('复制', onPress: () {
                  controller.pressRec();
                  copyToClipboard(copyText ?? '');
                })
              ])
      ],
    );
  }

  Widget _textContent() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1.w, color: AppColors.ffe1ebef),
      ),
      child: Padding(
        padding: EdgeInsets.all(8).w,
        child: Column(
          children: [
            _renderSelectedChannelItem(
              title: nameHint?.bankName,
              copyText: controller.payData.value.channel?[0].address,
            ),
            _renderSelectedChannelItem(
              title: nameHint?.payee,
              copyText: controller.payData.value.channel?[0].domain,
            ),
            _renderSelectedChannelItem(
              title: nameHint?.bankAccount,
              copyText: controller.payData.value.channel?[0].account,
            ),
            _renderSelectedChannelItem(
              title: nameHint?.accountAddress,
              copyText: "${controller.payData.value.channel?[0].branchAddress}",
            ),
            Container(
              padding: EdgeInsets.only(left: 8),
              alignment: Alignment.centerLeft,
              child: Text('${controller.payData.value.channel?[0].fcomment}',
                  textAlign: TextAlign.left,
                  style: AppTextStyles.bodyText2(context, fontSize: 18)),
            )
          ],
        ),
      ),
    );
  }
}
