import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:scroll_date_picker/scroll_date_picker.dart';
import 'package:url_launcher/url_launcher.dart';

class PayListViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();
  final AppUserRepository _appUserRepository = AppUserRepository();
  RxList<PayAisleListData> payBigData = RxList();
  RxBool dialogShowing = false.obs;
  RxBool isIniting = true.obs;
  RxString dialogContent = ''.obs;
  @override
  void onInit() {
    super.onInit();
    isIniting.value = true;
    requestPayData();
  }

  void requestPayData() async {
    await _appRechargeRepository.cashier().then((data) {
      isIniting.value = false;
      if (data == null) return;
      if (data.rechargePopUpAlarmMsg != '' &&
          data.rechargePopUpAlarmSwitch == '1') {
        dialogShowing.value = true;
        dialogContent.value = data.rechargePopUpAlarmMsg ?? '';
      }
      payBigData.value = data.payment ?? [];
    });
  }

  String getRechargeChannelIcon(String? idString) {
    if (idString != null && idString.isNotEmpty) {
      return PayIcon.getIcon(idString);
    }
    return '';
  }

  String goToSubPageandGetIcon(int index, bool isEvent) {
    PayItemType? id = payBigData[index].id;
    String imgName = 'online-okp';

    if (id != null) {
      switch (id) {
        case PayItemType.alipay_online:
        case PayItemType.wechat_online:
        case PayItemType.tenpay_online:
        case PayItemType.aliyin_transfer:
        case PayItemType.bank_online:
        case PayItemType.yinlian_online:
        case PayItemType.quick_online:
        case PayItemType.huobi_online:
        case PayItemType.xnb_online:
        case PayItemType.jingdong_online:
        case PayItemType.gopay_online:
        case PayItemType.okp_online:
        case PayItemType.upay_online:
        case PayItemType.momopay_online:
        case PayItemType.zalo_online:
        case PayItemType.viettelpay_online:
        case PayItemType.topay_online:
        case PayItemType.qq_online:
        case PayItemType.abpay_online:
        case PayItemType.kdou_online:
        case PayItemType.douyin_online:
        case PayItemType.ecny_online:
        case PayItemType.vippay_online:
        case PayItemType.tgpay_online:
        case PayItemType.mpay_online:
        case PayItemType.edan_online:
        case PayItemType.obpay_online:
        case PayItemType.cbrpay_online:
        case PayItemType.bobipay_online:
        case PayItemType.jiujiubapay_online:
        case PayItemType.guantianshouyintai_online:
        case PayItemType.miaofupay_online:
        case PayItemType.cgpay_online:
        case PayItemType.ebpay_online:
        case PayItemType.fpay_online:
        case PayItemType.didishoyintai_online:
        case PayItemType.goubao_online:
        case PayItemType.wanbipay_online:
        case PayItemType.kbao_online:
        case PayItemType.jdpay_online:
        case PayItemType.balingba_online:
        case PayItemType.qiannengpay_online:
          if (isEvent) {
            Get.toNamed(onlinePayViewPath,
                arguments: [payBigData[index], index]);
          } else if (id == PayItemType.okp_online) {
            imgName = 'online-okp';
          } else if (id == PayItemType.balingba_online) {
            imgName = 'online-808pay';
          } else {
            imgName = 'cbrpay-online';
          }
        case PayItemType.alipayenvelope_transfer:
          if (isEvent) {
            Get.toNamed(transferPayViewPath,
                arguments: [payBigData[index], index]);
          } else {
            imgName = 'online-pay';
          }
        case PayItemType.yxsm_transfer:
        case PayItemType.bank_transfer:
        case PayItemType.upi_transfer:
        case PayItemType.alipay_transfer:
        case PayItemType.momo_transfer:
        case PayItemType.yunshanfu_transfer:
        case PayItemType.wxzsm_transfer:
        case PayItemType.wxsm_transfer:
        case PayItemType.ysf_transfer:
        case PayItemType.tenpay_transfer:
        case PayItemType.wechat_transfer:
        case PayItemType.qqpay_transfer:
        case PayItemType.wechat_alipay_transfer:
        case PayItemType.alihb_online:
        case PayItemType.jdzz_transfer:
        case PayItemType.ddhb_transfer:
        case PayItemType.dshb_transfer:
        case PayItemType.xlsm_transfer:
        case PayItemType.zfbzyhk_transfer:
        case PayItemType.zfbzk_transfer:
        case PayItemType.bankalipay_transfer:
        case PayItemType.wxzfbsm_transfer:
        case PayItemType.liaobei_transfer:
        case PayItemType.viettelpay_transfer:
        case PayItemType.siyu_transfer:
        case PayItemType.truemoney_transfer:
          if (isEvent) {
            Get.toNamed(transferPayViewPath,
                arguments: [payBigData[index], index]);
          } else {
            if (id == PayItemType.truemoney_transfer) {
              imgName = 'online-truemoney';
            } else if (id != PayItemType.bank_transfer) {
              imgName = 'xnbcz-icon';
            } else {
              imgName = 'transfer';
            }
          }
        case PayItemType.xnb_transfer:
          if (isEvent) {
            Get.toNamed(btcPayViewPath, arguments: [payBigData[index], index]);
          } else {
            return img_rechageChannelIcon('bank_sort_iconxnb_transfer',
                type: 'jpg');
          }
        case PayItemType.qd_online:
          if (isEvent) {
            Get.toNamed(grabPath);
          } else {
            imgName = 'qd-icon';
          }

        case PayItemType.pending_order:
          if (isEvent) {
            if (payBigData[index].channel != null &&
                payBigData[index].channel?.isNotEmpty == true) {
              processPendingOrder(payBigData[index]);
            }
          } else {
            imgName = 'pending_order';
          }
        case PayItemType.embedwallet_online:
          if (isEvent) {
            Get.toNamed(embedwalletPath, arguments: [
              payBigData[index],
              index,
            ]);
          } else {
            imgName = 'online-embedwallet';
          }
        default:
          if (isEvent) {
            Get.toNamed(depositPath);
          } else {
            imgName = 'online-pay';
          }
      }
    }
    return img_images(imgName);
  }

  void goToTutorial(PayItemType? btcType, String? pageName) {
    var host = AppDefine.host + '/views/bank/tutorialPictures/';

    List<String> images = [];
    if (btcType != null &&
        [
          PayItemType.okp_online, // 'okpay在线支付'
          PayItemType.gopay_online, // 'gopay在线支付'
          PayItemType.topay_online, // 'topay在线支付大字'
          PayItemType.kdou_online, // 'K豆钱包'
          PayItemType.abpay_online, // 'AbPay钱包'
          PayItemType.pending_order, // '快充通道'
          PayItemType.vippay_online, // 'VIPPay'
          PayItemType.tgpay_online, // 'TGPay'
          PayItemType.cbrpay_online, // 'cbrpay'
          PayItemType.obpay_online, // 'obpay'
          PayItemType.bobipay_online, // '波币钱包'
          PayItemType.mpay_online, // 'mpay在线支付'
          PayItemType.upay_online, // 'upay在线支付'
          PayItemType.jiujiubapay_online, // 'jjbpay钱包'
          PayItemType.ecny_online, // '数字人民币'
          PayItemType.agpay_online, // 'AGPay'
          PayItemType.guantianshouyintai_online, // '收银台'
          PayItemType.miaofupay_online, // '秒付充值'
          PayItemType.cgpay_online, // 'CGPay钱包'
          PayItemType.ebpay_online, // 'EBPay钱包'
          PayItemType.fpay_online, // 'FPay'
          PayItemType.embedwallet_online, // '內嵌錢包'
          PayItemType.didishoyintai_online, // 'didi收银台'
          PayItemType.zfbzk_transfer, // '支付宝转卡'
          PayItemType.goubao_online, // '购宝钱包'
          PayItemType.wanbipay_online, // 'wanbipay'
          PayItemType.kbao_online, // 'kbao'
          PayItemType.jdpay_online, // 'jdpay'
          PayItemType.balingba_online, // '八零八Pay'
          PayItemType.qiannengpay_online, // '钱能钱包'
        ].contains(btcType)) {
      switch (btcType) {
        case PayItemType.okp_online:
          images = [host + 'okpayOriginal.png'];
          break;
        case PayItemType.gopay_online:
          images = [host + 'gopayOriginal.png'];
          break;
        case PayItemType.topay_online:
          images = [host + 'topayOriginal.png'];
          break;
        case PayItemType.upay_online:
          images = [host + 'upayOriginal.png'];
          break;
        case PayItemType.abpay_online:
        case PayItemType.eden:
          images = [host + 'abpayOriginal.png'];
          break;
        case PayItemType.agpay_online:
          images = [host + 'agpayOriginal.png'];
          break;
        case PayItemType.kdou_online:
          images = [host + 'KdouPayOriginal.png'];
          break;
        case PayItemType.jiujiubapay_online:
          images = [host + '988payOriginal.png'];
          break;
        case PayItemType.pending_order:
          final siteId =
              AppDefine.siteId == 'f070' ? 'F070A' : AppDefine.siteId;
          images = [
            host + 'pendingOrder${AppDefine.siteId?.toUpperCase()}.png'
          ];
          break;
        case PayItemType.vippay_online:
          images = [host + 'vippayOriginal.png'];
          break;
        case PayItemType.tgpay_online:
          images = [host + 'tgpayOriginal.png'];
          break;
        case PayItemType.cbrpay_online:
          images = [host + 'cbrpayOriginal.png'];
          break;
        case PayItemType.fpay_online:
          images = [host + 'fpayOriginal.png'];
          break;
        case PayItemType.obpay_online:
          images = [host + 'obpayOriginal.png'];
          break;
        case PayItemType.bobipay_online:
          images = [host + 'bobipayOriginal.png'];
          break;
        case PayItemType.mpay_online:
          images = [host + 'mpayOriginal.png'];
          break;
        case PayItemType.ecny_online:
          images = [host + 'ecnyOriginal.png'];
          break;
        case PayItemType.guantianshouyintai_online:
          images = [host + 'guantianshouyintaiOriginal.png'];
          break;
        case PayItemType.miaofupay_online:
          images = [host + 'miaofupayOriginal.png'];
          break;
        case PayItemType.embedwallet_online:
          final siteIdForEmbedWallet = !AppDefine.inSites(['f028b'])
              ? AppDefine.siteId
              : AppDefine.keyConfig?.siteCode;
          images = [
            host + 'embedWallet${siteIdForEmbedWallet?.toUpperCase()}.png'
          ];
          break;
        case PayItemType.cgpay_online:
          images = [
            host + 'cgpay${AppDefine.siteId?.toUpperCase().substring(0, 4)}.png'
          ];
          break;
        case PayItemType.ebpay_online:
          images = [host + 'ebpayOriginal.png'];
          break;
        case PayItemType.didishoyintai_online:
          images = [host + 'didipayOriginal.png'];
          break;
        case PayItemType.zfbzk_transfer:
          images = [host + 'zfbzkTransferOriginal.png'];
          break;
        case PayItemType.goubao_online:
          images = [host + 'goubaopayOriginal.png'];
          break;
        case PayItemType.wanbipay_online:
          images = [host + 'wanbipayOriginal.png'];
          break;
        case PayItemType.kbao_online:
          images = [host + 'kbaopayOriginal.png'];
          break;
        case PayItemType.jdpay_online:
          images = [host + 'jdpayOriginal.png'];
          break;
        case PayItemType.xnb_online:
        case PayItemType.balingba_online:
          images = [host + '808payOriginal.png'];
          break;
        case PayItemType.qiannengpay_online:
          images = [host + 'qiannengpayOriginal_1.jpg'];
          break;
        default:
          break;
      }
    }
    Get.toNamed(captialTutorialPath, arguments: [images, pageName]);
  }

  void processPendingOrder(PayAisleListData item) async {
    await _appUserRepository.getWebAccessToken().then((data) {
      if (data.code == 0) {
        String accessToken = data.data["accessToken"];
        String url =
            "${AppDefine.host}/mobile/userrech/onlinePay.do/pay?rechId=${item.channel?[0].id ?? '0'}&accessToken=${accessToken}";

        launchUrl(Uri.parse(url));
      }
    }).catchError((error) {
      print({'getWebAccessToken error': error});
    });
  }
}
