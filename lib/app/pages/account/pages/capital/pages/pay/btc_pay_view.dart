import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/btc_pay_view_controller.dart';
import 'package:board_datetime_picker/board_datetime_picker.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/models/ITransName.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/services.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/routes.dart';

class BtcPayView extends StatefulWidget {
  const BtcPayView({super.key});

  @override
  State<BtcPayView> createState() => _BtcPayView();
}

enum Gender { male, female, other }

class _BtcPayView extends State<BtcPayView>
    with SingleTickerProviderStateMixin {
  BtcPayViewController controller = Get.put(BtcPayViewController());
  final TextEditingController _depositsController = TextEditingController();
  final TextEditingController _paymentController = TextEditingController();
  final textController = BoardDateTimeTextController();
  Gender selectedGender = Gender.male;
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  bool _reload = false;
  TransNameModel? nameHint;

  DateTime? lastClickTime;
  @override
  void initState() {
    super.initState();

    controller.payData.value = Get.arguments[0];
    controller.payIndex = Get.arguments[1];
    Map<String, dynamic> jsonNameHint = transferName(
        payData: controller.payData.value.id,
        payChannelBean: controller.payData.value.channel?[0]);
    nameHint = TransNameModel.fromJson(jsonNameHint);

    controller.fetchDepositPrompt();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_controller);
    controller.fetchDepositPrompt();

    controller.getCurrentRate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ]),
            )),
        actions: [
          AnimatedBuilder(
            animation: _spinAnimation,
            builder: (context, child) {
              return Transform.rotate(
                angle: _spinAnimation.value * 12 * 3.14159,
                child: GestureDetector(
                    onTap: () async {
                      _controller.forward();
                      await controller.fetchCashier();
                      _controller.stop();
                    },
                    child: Icon(
                      Icons.autorenew_rounded,
                      color: AppColors.surface,
                      size: 32.w,
                    )),
                // ),
              );
            },
          ),
        ],
        title: Text('虚拟币充值',
            // payBigData.id == PayItemType.xnb_transfer
            //     ? '虚拟币充值'
            //     : payBigData.name ?? '',
            style: AppTextStyles.surface_(context,
                fontWeight: FontWeight.bold, fontSize: 24)),
        centerTitle: true,
        leadingWidth: 0.3.sw,
      ),
      body: Stack(alignment: Alignment.topCenter, children: [
        ListView(children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16).h,
            child: Center(
              child: SizedBox(
                width: 0.9.sw,
                child: Column(
                  children: [
                    SizedBox(
                      height: 54.h,
                    ),
                    _comboBox(),
                    Obx(() => Container(
                        alignment: Alignment.centerLeft,
                        color: AppColors.ffff0000,
                        child: Row(children: [
                          _circleWithText("!", 20.w).paddingOnly(left: 10.w),
                          Text(controller.depositPrompt.value,
                              style: AppTextStyles.surface_(context,
                                  fontSize: 20)),
                        ]))),
                    _textContent(),
                    SizedBox(
                      height: 10.w,
                    ),
                    _amountItem(
                        'depositAmount', '请填写存款金额', _depositsController),
                    _calcuPane(),
                    _rateContainer(),
                    Obx(() {
                      final prompt = controller.payData.value.prompt;
                      if (prompt != null && prompt.isNotEmpty) {
                        return Html(data: prompt);
                      }
                      return SizedBox();
                    }),
                    // _paymentMethodItem(),
                    // _text(payBigData.prompt ?? ''),
                    _datetimePicker(),
                    _amountItem('remarks', '请填写备注信息', _paymentController),

                    _setButton(0, '提交申请', false),
                  ],
                ),
              ),
            ),
          )
        ]),
        Positioned(
          top: 0,
          child: Container(
            width: 1.sw,
            padding: const EdgeInsets.symmetric(vertical: 12).h,
            decoration: const BoxDecoration(color: AppColors.ffbfa46d),
            child: Text(
              '汇款前先点击右上角“刷新”按钮，获取最新的入款通道',
              textAlign: TextAlign.center,
              style: AppTextStyles.surface_(context, fontSize: 20),
            ),
          ),
        )
      ]),
    );
  }

  Widget _calcuPane() {
    return Container(
      padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
      width: 0.9.sw,
      height: 54.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
      child: Row(
        children: [
          SizedBox(width: 120.w, child: Text('虚拟币金额:')),
          Expanded(
            child: Obx(
              () {
                var depositAmount = controller.depositAmount.value;
                var btcMoney =
                    ((double.tryParse(controller.depositAmount.value) ?? 0) *
                            controller.newRate.value)
                        .toStringAsFixed(2);
                return Text(
                  double.tryParse(controller.depositAmount.value) == null
                      ? ''
                      : btcMoney,
                  textAlign: TextAlign.right,
                );
              },
            ),
          ),
          GestureDetector(
            onTap: () {
              String money = double.tryParse(controller.depositAmount.value) ==
                      null
                  ? ''
                  : '${((double.tryParse(controller.depositAmount.value) ?? 0) * controller.newRate.value).toStringAsFixed(2)}';
              controller.pressRec();
              copyToClipboard(money);
            },
            child: Row(
              children: [
                Text(
                  'USDT',
                  style: AppTextStyles.ff000000_(
                    context,
                    fontSize: 18,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
                  width: 100.w,
                  height: 70.h,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      border:
                          Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
                  child: Text(
                    '复制',
                    style: AppTextStyles.ffE44848_(
                      context,
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _rateContainer() {
    return Container(
      height: 50.h,
      padding: const EdgeInsets.symmetric(vertical: 12).h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            width: 1.w,
            color: AppColors.ffC5CBC6,
          )),
      child: Obx(
        () => Text('1 USDT = ${controller.newUsd.value} CNY',
            style: AppTextStyles.bodyText2(
              context,
              fontSize: 18,
            )),
      ),
    );
  }

  Widget _datetimePicker() {
    return Container(
        padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
        width: 0.9.sw,
        height: 54.h,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
        child: Stack(alignment: Alignment.centerRight, children: [
          BoardDateTimeInputField(
            controller: textController,
            initialDate: DateTime.now(),
            pickerType: DateTimePickerType.datetime,
            options: const BoardDateTimeOptions(
                languages: BoardPickerLanguages.en(),
                pickerFormat: PickerFormat.ymd),
            textStyle: AppTextStyles.bodyText1(context, fontSize: 20),
            textAlign: TextAlign.left,
            onChanged: (date) {
              print('onchanged: $date');
            },
            onFocusChange: (val, date, text) {
              print('on focus changed date: $val, $date, $text');
            },
          ),
          GestureDetector(
            onTap: () {},
            child: SizedBox(
              width: 30.w,
              child: Icon(
                Icons.calendar_month,
                size: 25.w,
              ),
            ),
          ).paddingOnly(right: 5.w),
        ]));
  }

  void _onTap() {
    final currentTime = DateTime.now();
    // 检查是否在 1 秒内重复点击
    if (lastClickTime == null ||
        currentTime.difference(lastClickTime!) > const Duration(seconds: 1)) {
      lastClickTime = currentTime;
      // 执行点击逻辑
      controller.requestPayData();
      Get.toNamed(depositPath, arguments: [2]);
    }
  }

  SizedBox _setButton(int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: _onTap,
          child: Container(
            width: 0.9.sw,
            height: 54.h,
            margin: const EdgeInsets.only(top: 16).h,
            decoration: BoxDecoration(
              color: AppColors.ffbfa46d,
              borderRadius: BorderRadius.circular(4).w,
            ),
            child: Center(
              child: Text(
                amount,
                style: AppTextStyles.surface_(context, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          )),
    );
  }

  Widget _paymentMethodItem() {
    return SizedBox(
        width: 0.9.sw,
        child: Column(children: [
          Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(
                3,
                (index) => _unitTransferItem(
                    context,
                    index,
                    controller.BANK_TYPE[index].toString(),
                    controller.isColoured[index].value),
              ))),
          Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(
                3,
                (index) => _unitTransferItem(
                    context,
                    index + 3,
                    controller.BANK_TYPE[index + 3].toString(),
                    controller.isColoured[index + 3].value),
              ))),
          Obx(() => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(
                3,
                (index) => _unitTransferItem(
                    context,
                    index + 6,
                    controller.BANK_TYPE[index + 6].toString(),
                    controller.isColoured[index + 6].value),
              ))),
        ]));
  }

  Widget _amountItem(
      String type, String hint, TextEditingController editController) {
    return Container(
      padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
      width: 0.9.sw,
      height: 54.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
      child: Obx(() => Focus(
          onFocusChange: (hasFocus) {
            setState(() {
              controller.hasFocus[type] = hasFocus;
            });
          },
          child: TextFormField(
            onChanged: (text) {
              type == 'depositAmount'
                  ? controller.depositAmount.value = text
                  : type == 'remarks'
                      ? controller.remarkContent.value = text
                      : controller.transferorName.value = text;
              // controller.parseNumber(controller.depositAmount.value);
              if (type == 'depositAmount') {
                if (controller.parseNumber(controller.depositAmount.value)) {
                  editController.text = controller.depositAmount.value;
                } else {
                  editController.text = '';
                }
              }
            },
            keyboardType: type == 'depositAmount'
                ? TextInputType.numberWithOptions(decimal: true)
                : TextInputType.text,
            obscureText: type == 'withdrawCode' ? true : false,
            controller: editController,
            cursorColor: AppColors.onBackground,
            style: AppTextStyles.ff535B70_(context, fontSize: 18),
            textAlign: TextAlign.left,
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(horizontal: 20).w,
                iconColor: AppColors.surface,
                filled: true,
                fillColor: Colors.transparent,
                hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
                hintText: hint,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: AppColors.onBackground,
                    width: 1.0.w,
                  ),
                ),
                suffixIcon: (GestureDetector(
                    onTap: () {
                      editController.clear();
                    },
                    child: controller.hasFocus[type] ?? false
                        ? Icon(
                            size: 23.w, color: AppColors.ff8D8B8B, Icons.close)
                        : const SizedBox()))),
          ))),
    );
  }

  SizedBox _unitTransferItem(
      BuildContext context, int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.changeColorTransItem(index);
            controller.updatePayChannel(index);
          },
          child: Stack(alignment: Alignment.bottomRight, children: [
            Container(
              width: 0.29.sw,
              height: 48.h,
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 16).h,
              decoration: BoxDecoration(
                color: isColoured ? AppColors.surface : AppColors.ffeeeeee,
                borderRadius: BorderRadius.circular(2).w,
                border: Border.all(
                  color: isColoured ? AppColors.fffd7328 : AppColors.ffeeeeee,
                ),
              ),
              child: Text(
                amount,
                style: isColoured
                    ? AppTextStyles.ffBFA46D_(context, fontSize: 18)
                    : AppTextStyles.ff666666_(context, fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),
          ])),
    );
  }

  Widget _text(
    String title, {
    int fontSize = 20,
  }) {
    return SizedBox(
      width: 0.7.sw,
      child: Padding(
        padding: const EdgeInsets.all(8.0).w,
        child: Text(title,
            style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
      ),
    );
  }

  Widget _textButton(String title, {int fontSize = 20, VoidCallback? onPress}) {
    return GestureDetector(
        onTap: onPress,
        child: Padding(
          padding: const EdgeInsets.all(8.0).w,
          child: Text(title,
              style: AppTextStyles.error_(context, fontSize: fontSize)),
        ));
  }

  Widget _headerText(String title, {int fontSize = 20}) {
    return Padding(
      padding: const EdgeInsets.all(8.0).w,
      child: Text(title,
          style: AppTextStyles.ff999999_(context, fontSize: fontSize)),
    );
  }

  Widget _comboBox() {
    return Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1.w, color: AppColors.ffe1ebef),
        ),
        child: Padding(
          padding: EdgeInsets.all(8).w,
          child: RadioListTile<Gender>(
            title: Text('USDT充值'),
            value: Gender.male,
            groupValue: selectedGender,
            onChanged: (Gender? value) {
              setState(() {
                selectedGender = value ?? Gender.male;
              });
            },
          ),
        ));
  }

  Widget _circleWithText(String text, double diameter) {
    return Container(
      width: diameter, // Diameter of the circle
      height: diameter, // Diameter of the circle
      decoration: const BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        // Optional: Border for better visibility
      ),
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            fontSize: 20.0.w,
            fontWeight: FontWeight.bold,
            color: AppColors.error,
          ),
        ),
      ),
    );
  }

  Widget _renderSelectedChannelItem(
      {String? title, String? copyText, bool showCopyButton = true}) {
    return Row(
      children: [
        _text("$title  $copyText"),
        const Spacer(),
        if (showCopyButton)
          _textButton('复制', onPress: () {
            if (showCopyButton) {
              controller.pressRec();
              copyToClipboard(copyText ?? '');
            }
          })
      ],
    );
  }

  void copyToClipboard(String text) {
    Clipboard.setData(ClipboardData(text: text)).then((_) {
      print('Text copied to clipboard!');
    });
  }

  Widget _textContent() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1.w, color: AppColors.ffe1ebef),
      ),
      child: Padding(
        padding: EdgeInsets.all(8).w,
        child: Column(
          children: [
            _renderSelectedChannelItem(
                title: "币种:",
                copyText: controller.payData.value.channel?[0].domain,
                showCopyButton: false),
            if (!['doy', 'trx'].contains(controller.payData.value.channel
                ?.elementAt(controller.selPayChannel.value)
                .domain
                ?.toLowerCase()))
              _renderSelectedChannelItem(
                  title: '链名称:',
                  copyText: controller.payData.value.channel?[0].address,
                  showCopyButton: false),
            _renderSelectedChannelItem(
                title: '充值地址:',
                copyText: controller.payData.value.channel?[0].account,
                showCopyButton: true),
            if (controller.payData.value.channel
                    ?.elementAt(controller.selPayChannel.value)
                    .domain ==
                'doy')
              _renderSelectedChannelItem(
                  title: '真实姓名:',
                  copyText: controller.payData.value.channel
                      ?.elementAt(controller.selPayChannel.value)
                      .payeeName,
                  showCopyButton: false),
            Container(
                height: 200.0.w,
                width: double.infinity,
                alignment: Alignment.topLeft,
                child: Row(
                  children: [
                    QrImageView(
                      data: 'TExvfk4oLkT8bDdUsgvj9YZb6we2HPu5gf',
                      version: QrVersions.auto,
                      errorCorrectionLevel: QrErrorCorrectLevel.Q,
                      size: 190,
                    ),
                    const Spacer(),
                  ],
                )),
            _renderSelectedChannelItem(
                title: '',
                copyText: controller.payData.value.channel
                        ?.elementAt(controller.selPayChannel.value)
                        .fcomment ??
                    '',
                showCopyButton: false),
          ],
        ),
      ),
    );
  }
}
