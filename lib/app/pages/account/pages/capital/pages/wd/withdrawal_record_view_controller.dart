import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/wd/WithdrawalRecordModel.dart';
import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:fpg_flutter/data/models/wd/DepositRecordModel.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class WithdrawalRecordViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();
  RxString? dropdownValue;
  RxBool isIniting = false.obs;
  // RxBool isIniting = true.obs;
  RxDouble sumAmount = 0.0.obs;

  RxList<String> liqTypeList = [
    '全部状态',
    '提现成功',
    '已失败',
    '处理中',
    '申请中',
  ].obs;
  final title = '查看详情';

  List<String> getCategories() {
    return [
      '交易编号',
      '发起时间',
      '到账时间',
      '交易类型',
      '交易金额',
      '手续费用',
      '交易状态',
      '提款方式',
      '账号',
      '持卡人',
      '备注'
    ];
  }

  RxList<WithdrawalRecordModel> tableData = RxList();

  RxString leftDropdownValue = '最近31天'.obs;
  int duration = 31;
  String endDate = '';
  String startDate = '';
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  int page = 1;

  RxString rightDropdownValue = '全部状态'.obs;
  UGUserModel userInfo = UGUserModel();
  Map<String, String> liqTypeNo = {
    '全部状态': '全部状态',
    '成功': '提现成功',
    '失败': '已失败',
    '处理中': '处理中',
    '申请中': '申请中',
  };

  var dateSelect = [
    '今天',
    '最近7天',
    '最近31天',
  ];

  @override
  void onInit() {
    super.onInit();
    setDate();
  }

  void setDate() {
    if (leftDropdownValue.value == '今天') {
      duration = 0;
    } else if (leftDropdownValue.value == '最近7天') {
      duration = 7;
    } else if (leftDropdownValue.value == '最近31天') {
      duration = 31;
    }
    endDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
    startDate = DateFormat('yyyy-MM-dd')
        .format(DateTime.now().subtract(Duration(days: duration)));
    tableData.clear();

    onRefresh();
  }

  Future getRecordData(bool isRefresh) async {
    if (isRefresh) {
      page = 1;
      sumAmount.value = 0;
      tableData.clear();
    } else {
      page++;
    }

    await _appRechargeRepository
        .getWithdrawalRecordData(
            page: page, startDate: startDate, endDate: endDate)
        .then((data) {
      if (data != null) {
        final tempList = data.list ?? [];
        tableData.addAll(tempList);
        for (var item in tempList) {
          sumAmount.value += double.parse(item.amount ?? '0');
        }
        sumAmount.value = (sumAmount.value * 100).round() / 100;

        if (tempList.length < 100) {
          refreshController.loadNoData();
        } else {
          refreshController.loadComplete();
        }
      } else {
        refreshController.refreshCompleted();
        refreshController.loadComplete();
      }
    });
  }

  Future onRefresh() async {
    getRecordData(true);
  }

  Future onLoading() async {
    getRecordData(false);
  }
}
