import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/wt/forget_pwd_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:multi_image_picker_view/multi_image_picker_view.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class ForgetPwdView extends StatefulWidget {
  const ForgetPwdView({super.key});

  @override
  State<ForgetPwdView> createState() => _ForgetPwdViewState();
}

class _ForgetPwdViewState extends State<ForgetPwdView> {
  final List<Map<String, String>> tileItems = [
    {'title': "余额提款", 'subtitle': "安全快捷", 'url': ''},
    {'title': "抢单提款", 'subtitle': "支持大额入款，您的首选", 'url': grabPath},
  ];

  final List<String> picName = ['qd-icon.svg', 'qiandai.png'];
  double itemWidth = 0.2.sw;

  final ForgetPwdViewController controller = Get.put(ForgetPwdViewController());
  final pickController = MultiImagePickerController(
      maxImages: 1,
      picker: (allowMultiple) async {
        return await ForgetPwdViewController.pickImagesUsingImagePicker(
            allowMultiple);
      });
  final pickController_2 = MultiImagePickerController(
      maxImages: 1,
      picker: (allowMultiple) async {
        return await ForgetPwdViewController.pickImagesUsingImagePicker(
            allowMultiple);
      });

  bool _validateInput(value, editController) {
    final numberRegExp = RegExp(r'^-?\d+(\.\d+)?$');
    if (!numberRegExp.hasMatch(value)) {
      editController.clear();
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.surface,
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          toolbarHeight: 65.h,
          leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios,
                color: AppColors.surface, size: 28.w),
          ),
          title: Text('忘记取款密码',
              style: AppTextStyles.surface_(context, fontSize: 24)),
          centerTitle: true,
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0).h,
            child: Column(
              children: [
                _renderInput('取款密码', '输入提款银行卡号', TextInputType.visiblePassword,
                    (text) => controller.withdrawBankCardNo = text, true),
                _renderInput('输入手机号', '输入手机号', TextInputType.visiblePassword,
                    (text) => controller.phoneNo = text, true),
                _renderInput('取款密码', '新的四位取款密码', TextInputType.visiblePassword,
                    (text) => controller.withdrawPwd = text, true),
                _renderPickImage(),
                _setButton(0, '确认提交', false),
              ],
            ),
          ),
        ));
  }

  Widget _renderPickImage() {
    return Container(
        padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
        width: 0.9.sw,
        height: itemWidth * 1.5,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: AppColors.ffF5F5F5,
            border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20.0).w,
              child: Text(
                '身份证正反面: ',
                style: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
              ),
            ),
            SizedBox(
              width: itemWidth,
              height: itemWidth,
              child: MultiImagePickerView(
                controller: pickController,
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: itemWidth,
                  crossAxisSpacing: 4.w,
                  mainAxisSpacing: 4.h,
                  childAspectRatio: 1,
                ),
                initialWidget: _addPickWidget(context, itemWidth),
                addMoreButton: _addPickWidget(context, itemWidth),
                // padding: const EdgeInsets.all(10),
              ),
            ).paddingOnly(top: 10.h, bottom: 10.h, right: 20.w),
            SizedBox(
              width: itemWidth,
              height: itemWidth,
              child: MultiImagePickerView(
                controller: pickController_2,
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: itemWidth,
                  crossAxisSpacing: 4.w,
                  mainAxisSpacing: 4.h,
                  childAspectRatio: 1,
                ),
                initialWidget: _addPickWidget_2(context, itemWidth),
                addMoreButton: _addPickWidget_2(context, itemWidth),
              ),
            ).paddingOnly(top: 10.h, bottom: 10.h),
          ],
        ));
  }

  Widget _renderInput(String title, String hint, TextInputType? keyboardType,
      void Function(String) onChanged, bool isObscure) {
    final TextEditingController editController = TextEditingController();

    return Container(
      padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
      width: 0.9.sw,
      height: 54.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: AppColors.ffF5F5F5,
          border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
      child: TextFormField(
        onChanged: (text) {
          if (_validateInput(text, editController)) {
            onChanged(text);
          }
        },
        keyboardType: keyboardType,
        obscureText: isObscure,
        controller: editController,
        cursorColor: AppColors.onBackground,
        style: AppTextStyles.ff535B70_(context, fontSize: 20),
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          isDense: true,
          iconColor: AppColors.onBackground,
          filled: true,
          fillColor: Colors.transparent,
          hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
          hintText: hint,
          border: const OutlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }

  SizedBox _setButton(int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            try {
              controller.files.clear();
              for (var i in pickController.images) {
                controller.files.add(i.path as String);
              }
            } catch (e) {
              AppLogger.e(e);
            }
            try {
              for (var i in pickController_2.images) {
                controller.files.add(i.path as String);
              }
              controller.bindPassword();
            } catch (e) {
              AppLogger.e(e);
            }
          },
          child: Container(
            width: 0.9.sw,
            height: 54.h,
            margin: const EdgeInsets.only(top: 16).h,
            decoration: BoxDecoration(
              color: AppColors.ffbfa46d,
              borderRadius: BorderRadius.circular(4).w,
            ),
            child: Center(
              child: Text(
                amount,
                style: AppTextStyles.surface_(context, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          )),
    );
  }

  void _pickImage() {
    pickController.pickImages();
  }

  void _pickImage_2() {
    pickController_2.pickImages();
  }

  Widget _addPickWidget(BuildContext context, double itemWidth) {
    return SizedBox(
        child: GestureDetector(
      onTap: _pickImage,
      child: Container(
        height: itemWidth,
        width: itemWidth,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
                width: 1.w,
                color: AppColors.ffC5CBC6,
                style: BorderStyle.solid)),
        child: Center(
          child:
              Text('+', style: AppTextStyles.bodyText1(context, fontSize: 40)),
        ),
      ),
    ));
  }

  Widget _addPickWidget_2(BuildContext context, double itemWidth) {
    return SizedBox(
        child: GestureDetector(
      onTap: _pickImage_2,
      child: Container(
        height: itemWidth,
        width: itemWidth,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
                width: 1.w,
                color: AppColors.ffC5CBC6,
                style: BorderStyle.solid)),
        child: Center(
          child:
              Text('+', style: AppTextStyles.bodyText1(context, fontSize: 40)),
        ),
      ),
    ));
  }

  Widget _text(
    String title, {
    int fontSize = 20,
  }) {
    return SizedBox(
      width: 0.9.sw,
      child: Padding(
        padding: const EdgeInsets.all(8.0).w,
        child: Text(title,
            style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
      ),
    );
  }
}
