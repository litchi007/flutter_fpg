import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/wt/withdrawal_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/data/models/sysConfModel/QdSupChannel.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/app/widgets/app_custom_dialog.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/widgets/recharge_popup_dialog.dart';

class WithdrawView extends StatefulWidget {
  const WithdrawView({super.key});

  @override
  State<WithdrawView> createState() => _WithdrawViewState();
}

class _WithdrawViewState extends State<WithdrawView> {
  final List<Map<String, String>> tileItems = [
    {'title': "余额提款", 'subtitle': "安全快捷", 'url': ''},
    {'title': "抢单提款", 'subtitle': "支持大额入款，您的首选", 'url': grabPath},
  ];

  final List<String> picName = ['qd-icon.svg', 'qiandai.png'];
  final WithdrawalViewController controller = Get.find();
  final TextEditingController _withdrawController = TextEditingController();
  BankInfoParam? selectedItem;
  final TextEditingController _withdrawCodeController = TextEditingController();
  List<String> withdrawTypes = [
    '银行卡',
    '支付宝',
    '微信',
    '虚拟币',
  ];
  DateTime? lastClickTime;

  bool _validateInput(value, editController) {
    final numberRegExp = RegExp(r'^-?\d+(\.\d+)?$');
    if (!numberRegExp.hasMatch(value)) {
      editController.clear();
      return false;
    } else {
      return true;
    }
  }

  // Widget _renderContent() {
  //   if (AppDefine.systemConfig?.switchBalanceChannel == '0' &&
  //       AppDefine.systemConfig?.switchYuebaoChannel == '0') {
  //     return _emptyView(text: '该功能暂时关闭');
  //   } else
  //     {_renderItem();}
  // }
// Widget _renderItem(){

// }
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Obx(() => AppDefine.systemConfig?.switchBalanceChannel == '0' &&
                AppDefine.systemConfig?.switchYuebaoChannel == '0' &&
                GlobalService.to.userInfo.value?.qdSwitch == null
            ? _emptyView(text: '该功能暂时关闭')
            : GlobalService.to.userInfo.value?.fullName == null ||
                    GlobalService.to.userInfo.value?.fullName == ''
                ? _emptyView(
                    type: "needname",
                    text: "您还没有完善个人信息",
                    buttonWidget: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            '完善个人信息',
                            style:
                                AppTextStyles.surface_(context, fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                        ]),
                    callBack: () =>
                        controller.withdrawalDialogShowing.value = true)
                : controller.showAddBank.value == true
                    ? _emptyView(
                        type: "addWithdrawalAccount",
                        text: "您还未绑定提款账户",
                        buttonWidget: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                '添加提款账户',
                                style: AppTextStyles.surface_(context,
                                    fontSize: 20),
                                textAlign: TextAlign.center,
                              ),
                            ]),
                        callBack: () => Get.toNamed(manageBankAccountsPath))
                    : Obx(() => controller.showBalanceWithdrawal.value
                        ? _renderToBank()
                        : ListView.builder(
                            itemCount: tileItems.length,
                            itemBuilder: (context, index) {
                              return Column(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      if (index == 0) {
                                        controller.showBalanceWithdrawal.value =
                                            true;
                                      } else {
                                        Get.toNamed(
                                            tileItems[index]['url'] ?? '');
                                      }
                                    },
                                    child: ListTile(
                                        dense: true,
                                        title: Text(
                                            '${tileItems[index]['title']}',
                                            style: AppTextStyles.bodyText3(
                                                context,
                                                fontSize: 24)),
                                        leading: index == 0
                                            ? AppImage.asset(picName[1],
                                                width: 60.w,
                                                height: 60.h,
                                                fit: BoxFit.contain)
                                            : AppImage.svgByAsset(
                                                'qd-icon.svg')),
                                  ).paddingOnly(top: 10.h, bottom: 10.h),
                                  Divider(
                                    height: 2.h,
                                  ),
                                ],
                              );
                            },
                          ))),
        Positioned(
            top: 80.h,
            // left: 25.w,
            child: Obx(() => controller.withdrawalDialogShowing.value
                ? CustomPopUpDialog(
                    context: context,
                    title: '完善个人信息',
                    content: 'AddingFullName',
                    titleBackgroundColor: AppColors.ffF3F3F3,
                    action: 'logout',
                    controller: controller,
                  )
                : const SizedBox()))
      ],
    );
  }

  Widget _renderToBank() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 24.0).h,
          child: _supChannelItem(
            context,
            controller.qdSupChannels[0],
          ),
        ),
        _text('选择免提直充：提交后不会真实到账，扣除的取款金额，将以等额的金额自动为您充值到账，并享受充值彩金优惠。'),
        _amountItem(context, 'withdraw', '请输入取出金额', _withdrawController),
        _text('单笔下限 10，上限 50000?'),
        _amountItem(
            context, 'withdrawCode', '请填写取款密码', _withdrawCodeController),
        _setButton(0, '确认提交', false),
        SizedBox(
          width: 0.9.sw,
          child: Padding(
              padding: const EdgeInsets.all(8.0).w,
              child: GestureDetector(
                onTap: () => Get.toNamed(forgetPwdPath),
                child: Text('忘记取款密码',
                    style: AppTextStyles.ff387ef5_(context, fontSize: 20)),
              )),
        ),
      ],
    );
  }

  Widget _emptyView(
      {String? type,
      String? text,
      Widget? buttonWidget,
      VoidCallback? callBack}) {
    return Center(
      child: Column(
        children: [
          const Spacer(
            flex: 1,
          ),
          if (type == 'needname')
            AppImage.asset('responsive-mess.png',
                width: 100.w, fit: BoxFit.cover),
          if (type == 'addWithdrawalAccount')
            AppImage.svgByAsset('empty.svg',
                width: 200.w, height: 100.h, fit: BoxFit.cover),
          SizedBox(
            height: 10.h,
          ),
          Text(
            text ?? '',
            style: AppTextStyles.ff000000(context, fontSize: 20),
          ),
          SizedBox(
            height: 10.h,
          ),
          GestureDetector(
              onTap: callBack,
              child: Container(
                  width: 200.w,
                  height: 60.h,
                  // margin: const EdgeInsets.only(top: 16).h,
                  padding: EdgeInsets.symmetric(horizontal: 15.w),
                  decoration: BoxDecoration(
                    color: AppColors.ffbfa46d,
                    borderRadius: BorderRadius.circular(4).w,
                  ),
                  child: buttonWidget)),
          const Spacer(
            flex: 5,
          ),
        ],
      ),
    );
  }

  Widget _amountItem(BuildContext context, String type, String hint,
      TextEditingController editController) {
    return Container(
      padding: const EdgeInsets.fromLTRB(8.0, 4, 8, 4).w,
      width: 0.9.sw,
      height: 54.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: AppColors.ffF5F5F5,
          border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
      child: TextFormField(
        onChanged: (text) {
          if (_validateInput(text, editController)) {
            type == 'withdraw'
                ? controller.withdrawAmount = text
                : controller.withdrawCode = text;
          }
        },
        keyboardType: type == 'withdrawCode'
            ? TextInputType.visiblePassword
            : TextInputType.text,
        obscureText: type == 'withdrawCode' ? true : false,
        controller: editController,
        cursorColor: AppColors.onBackground,
        style: AppTextStyles.ff535B70_(context, fontSize: 20),
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          isDense: true,
          iconColor: AppColors.onBackground,
          filled: true,
          fillColor: Colors.transparent,
          hintStyle: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
          hintText: hint,
          border: const OutlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }

  Widget _supChannelItem(BuildContext context, QdSupChannel item) {
    return Container(
      width: 0.9.sw,
      height: 54.h,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: AppColors.ffF5F5F5,
          border: Border.all(width: 1.w, color: AppColors.ffC5CBC6)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24).w,
        child: Obx(() => DropdownButton<BankInfoParam?>(
              isExpanded: true,
              hint: Text(
                '请选择银行',
                textAlign: TextAlign.center,
                style: AppTextStyles.ff8D8B8B_(context, fontSize: 20),
              ),
              underline: Container(
                height: 1.h,
                color: Colors.transparent,
              ),
              value: selectedItem,
              icon: const Icon(Icons.arrow_drop_down),
              items: _getDropdownItems(),
              onChanged: (BankInfoParam? newValue) {
                setState(() {
                  selectedItem = newValue;
                });
                controller.curBank = newValue ?? BankInfoParam();
                controller.updateAccount(
                  controller.qdSupChannels.indexOf(item),
                  convertToInt(newValue?.id ?? '-1') ?? -1,
                );
              },
            )),
      ),
    );
  }

  List<DropdownMenuItem<BankInfoParam?>> _getDropdownItems() {
    if (controller.arrAllAccountData.isEmpty) {
      return [
        DropdownMenuItem<BankInfoParam?>(
          value: null,
          child: Text(
            '',
            textAlign: TextAlign.center,
            style: AppTextStyles.bodyText2(
              context,
              fontSize: 20,
            ),
          ),
        )
      ];
    } else {
      List<DropdownMenuItem<BankInfoParam?>> listDropdownMenuItem = [];

      for (var arrAllAccountDataItem in controller.arrAllAccountData) {
        for (var curItem in arrAllAccountDataItem.data ?? []) {
          String title = controller.getTitle(curItem, arrAllAccountDataItem);

          listDropdownMenuItem.add(DropdownMenuItem<BankInfoParam?>(
            value: curItem,
            child: Padding(
              padding: const EdgeInsets.only(left: 10).w,
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(
                  context,
                  fontSize: 20,
                ),
              ),
            ),
          ));
        }
      }
      return listDropdownMenuItem;
    }
  }

  void _onTap() {
    final currentTime = DateTime.now();
    // 检查是否在 1 秒内重复点击
    if (lastClickTime == null ||
        currentTime.difference(lastClickTime!) > const Duration(seconds: 1)) {
      lastClickTime = currentTime;
      // 执行点击逻辑
      controller.confirmWithdraw(isC2c: 1);
    }
  }

  SizedBox _setButton(int index, String amount, bool isColoured) {
    return SizedBox(
      child: GestureDetector(
          onTap: _onTap,
          child: Container(
            width: 0.9.sw,
            height: 54.h,
            margin: const EdgeInsets.only(top: 16).h,
            decoration: BoxDecoration(
              color: AppColors.ffbfa46d,
              borderRadius: BorderRadius.circular(4).w,
            ),
            child: Center(
              child: Text(
                amount,
                style: AppTextStyles.surface_(context, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
          )),
    );
  }

  Widget _text(
    String title, {
    int fontSize = 20,
  }) {
    return SizedBox(
      width: 0.9.sw,
      child: Padding(
        padding: const EdgeInsets.all(8.0).w,
        child: Text(title,
            style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
      ),
    );
  }
}
