import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/sysConfModel/QdSupChannel.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:fpg_flutter/http/tools/rsa_encrypt.dart';
import 'dart:math';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/storage_util.dart';

class WithdrawalViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();
  RxBool showBalanceWithdrawal = false.obs;
  RxBool showAddBank = false.obs;
  String depositAmount = '';
  String withdrawAmount = '';
  String withdrawCode = '';

  RxBool dialogShowing = false.obs;
  RxBool withdrawalDialogShowing = false.obs;
  List<QdSupChannel> qdSupChannels = [];
  RxList<BankInfoParam> arrFullBankList = RxList();
  RxList<AllAccountListData> arrAllAccountData = RxList();
  List<int> accounts = [];
  BankInfoParam curBank = BankInfoParam();
  final AppUserRepository _appUserRepository = AppUserRepository();
  int withdrawType = 0; // 当前 余额取款 0 还是 余额宝取款 1
  @override
  void onInit() {
    super.onInit();
    qdSupChannels = AppDefine.systemConfig?.qdSupChannels ?? [];
    accounts = List.filled(qdSupChannels.length, 0);

    fetchData();
    dialogShowing.value = false;
    showBalanceWithdrawal.value =
        GlobalService.to.userInfo.value?.hasFundPwd ?? false;
  }

  void fetchData() async {
    getBankCardData();
  }

  void bindRealName(String fullName) async {
    AppToast.show();

    await _appUserRepository
        .profileName(token: AppDefine.userToken?.apiSid, fullName: fullName)
        .then((res) async {
      if (res?.code == 0) {
        GlobalService.to.userInfo.value?.fullName = fullName;
        final storage = GetStorage();
        storage.write(StorageKeys.USERINFO, GlobalService.to.userInfo.value);
      }
    });

    AppToast.dismiss();
    Get.offNamedUntil(
        depositPath,
        arguments: [1],
        (route) => route.settings.name == homePath);
  }

  void getBankCardData() async {
    await _appUserRepository
        .bankCard(token: AppDefine.userToken?.apiSid)
        .then((data) async {
      if (data != null) {
        arrAllAccountData.value = data.allAccountList ?? [];
        arrFullBankList.value = [];
        // for (AllAccountListData sData in arrAllAccountData) {
        //   arrFullBankList.addAll(sData.data ?? []);
        // }
        if (arrAllAccountData.isNotEmpty) {
          for (var item in arrAllAccountData) {
            if ((item.data?.isEmpty ?? true) ||
                ((item.data != null && item.data?.length == 1) &&
                    (item.data != null &&
                        item.data?.isNotEmpty == true &&
                        item.data![0].bankCode == 'mtzc'))) {
              if (item.isshow == true) {
                final random = Random();
                arrFullBankList.add(BankInfoParam(
                  parentTypeName: item.name,
                  bankName: '未绑定',
                  ownerName: '无',
                  bankCard: '无',
                  type: item.type?.toString(),
                  notBind: true,
                  id: item.data?[0].id ??
                      (random.nextInt(1) * 9000000000 + 1000000000).toString(),
                ));
              }
            } else {
              if (item.data?.isNotEmpty ?? false) {
                for (BankInfoParam res in item.data!) {
                  res.parentTypeName = item.name;
                  if (res.bankCode == 'mtzc') {
                    res.id = '-1';
                    res.type = '-1';
                  }
                  res.id = res.id ??
                      (Random().nextInt(9000000000) + 1000000000).toString();
                }
                arrFullBankList.addAll(item.data!
                    .map((data) => BankInfoParam(
                          parentTypeName: data.parentTypeName,
                          bankName: data.bankName,
                          ownerName: data.ownerName,
                          bankCard: data.bankCard,
                          type: data.type,
                          notBind: data.notBind,
                          id: data.id,
                          bankCode: data.bankCode,
                        ))
                    .toList());
              }
            }
          }
        }
      }
      showAddBank.value = arrFullBankList.isNotEmpty
          ? arrFullBankList.every((item) => item.bankName == '未绑定')
          : true;
    });
  }

  void updateAccount(int index, int id) {
    accounts[index] = id;
  }

  String getTitle(
      BankInfoParam item, AllAccountListData allaccountListDataItem) {
    String title = '';
    if (item.bankCode == 'mtzc') {
      title = item.bankName?.isEmpty ?? true ? '免提直充' : item.bankName!;
    } else if (item.bankName == '未绑定') {
      title = '${allaccountListDataItem.name}（${item.bankName}）';
    } else {
      switch (item.type) {
        case '-1':
          title = '${allaccountListDataItem.name}';
          break;
        case '1':
          title =
              '${allaccountListDataItem.name}（${item.bankName}，尾号${item.bankCard?.substring(item.bankCard!.length - 4)}，${item.ownerName}）';
          break;
        case '2':
          title =
              '${allaccountListDataItem.name}（${item.bankCard}，${item.ownerName}）';
          break;
        case '3':
          title =
              '${allaccountListDataItem.name}（${item.bankCard}，尾号${item.bankCard?.substring(item.bankCard!.length - 4)}，${item.ownerName}）';
          break;
        case '4':
          title =
              '${allaccountListDataItem.name}（${item.bankCard}，${item.bankName}，${item.ownerName}）';
          break;
        default:
          title = '';
          break;
      }
    }

    return title;
  }

  // 请求余额提现到银行卡

  void confirmWithdraw({int isC2c = 1}) async {
    if (!checkWithdrawTime()) {
      return;
    }
    if (withdrawAmount == '') {
      AppToast.showDuration(msg: '请输入金额');
      return;
    } else if (withdrawCode == '') {
      AppToast.showDuration(msg: '请输入密码');
      return;
    }
    // final virtual_amount = curBank.type != BankConst.BTC ? 0 : btcMoney;
    int money = int.parse(withdrawAmount);
    int virtual_amount = 0;
    int result = virtual_amount > 0 ? virtual_amount : money;
    await _appRechargeRepository
        .withdrawApply(
            id: curBank.id,
            money: money,
            pwd: md5(withdrawCode),
            virtual_amount: result,
            isC2c: isC2c)
        .then((data) async {});
  }

  bool checkWithdrawTime() {
    // Get the current date in 'YYYY/MM/DD' format
    final now = DateTime.now();
    final curDate =
        '${now.year}/${now.month.toString().padLeft(2, '0')}/${now.day.toString().padLeft(2, '0')}';

    DateTime? startTime;
    DateTime? endTime;
    final curTime =
        now.millisecondsSinceEpoch / 1000; // Current time in seconds

    if (withdrawType == 0) {
      // Check balance channel time range
      final startTimeString =
          '${curDate} ${AppDefine.systemConfig?.balanceChannelStartTime}';
      final endTimeString =
          '${curDate} ${AppDefine.systemConfig?.balanceChannelEndTime}';
      startTime = DateTime.parse(
          startTimeString.replaceAll('/', '-')); // Convert to ISO 8601 format
      endTime = DateTime.parse(endTimeString.replaceAll('/', '-'));
    } else if (withdrawType == 1) {
      // Check yuebao channel time range
      final startTimeString =
          '${curDate} ${AppDefine.systemConfig?.yuebaoChannelStartTime}';
      final endTimeString =
          '${curDate} ${AppDefine.systemConfig?.yuebaoChannelEndTime}';
      startTime = DateTime.parse(
          startTimeString.replaceAll('/', '-')); // Convert to ISO 8601 format
      endTime = DateTime.parse(endTimeString.replaceAll('/', '-'));
    }

    if (startTime == null || endTime == null) {
      // Invalid time range or missing data
      return false;
    }

    final startTimeSeconds = startTime.millisecondsSinceEpoch / 1000;
    final endTimeSeconds = endTime.millisecondsSinceEpoch / 1000;

    if (curTime > endTimeSeconds || curTime < startTimeSeconds) {
      // Show a Toast message (or any other UI feedback)
      AppToast.showDuration(
          msg: withdrawType == 0
              ? AppDefine.systemConfig?.balanceChannelPrompt
              : AppDefine.systemConfig?.yuebaoChannelPrompt);
    } else {
      return true;
    }

    return false;
  }
}
