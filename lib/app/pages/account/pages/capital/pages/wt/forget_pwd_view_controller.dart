import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/sysConfModel/QdSupChannel.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker_view/multi_image_picker_view.dart';

class ForgetPwdViewController extends GetxController {
  RxBool showBalanceWithdrawal = false.obs;
  String depositAmount = '';
  String withdrawAmount = '';
  String withdrawPwd = '';
  String withdrawBankCardNo = '';
  String phoneNo = '';
  List<QdSupChannel> qdSupChannels = [];
  RxList<BankInfoParam> arrFullBankList = RxList();
  RxList<AllAccountListData> arrAllAccountData = RxList();
  List<int> accounts = [];
  BankInfoParam curBank = BankInfoParam();
  final AppUserRepository _appUserRepository = AppUserRepository();
  int withdrawType = 0; // 当前 余额取款 0 还是 余额宝取款 1
  List<String> files = [];
  String smsNumber = '';
  void bindPassword() async {
    if (withdrawBankCardNo == '') {
      AppToast.showDuration(msg: '请填写银行卡号');
      return;
    }
    if (phoneNo == '') {
      AppToast.showDuration(msg: '请填写手机号');
      return;
    }
    if (withdrawPwd == '') {
      AppToast.showDuration(msg: '请输入您的4位数字提款密码');
      return;
    }
    String identityPath = '';
    for (int i = 0; i < files.length; i++) {
      if (i == 1) {
        identityPath += files[i];
      } else {
        identityPath += files[i] + ',';
      }
    }
    _appUserRepository
        .applyCoinPwd(
            token: AppDefine.userToken?.apiToken,
            bankNo: withdrawBankCardNo,
            coinpwd: withdrawPwd,
            mobile: phoneNo,
            smsCode: smsNumber,
            identityPathDot: identityPath)
        .then((data) {})
        .catchError((data) {});
  }

  static Future<List<ImageFile>> pickImagesUsingImagePicker(
      bool allowMultiple) async {
    final picker = ImagePicker();
    final List<XFile> xFiles;
    if (allowMultiple) {
      xFiles = await picker.pickMultiImage(maxWidth: 1080, maxHeight: 1080);
    } else {
      xFiles = [];
      final xFile = await picker.pickImage(
          source: ImageSource.gallery, maxHeight: 1080, maxWidth: 1080);
      if (xFile != null) {
        xFiles.add(xFile);
      }
    }
    if (xFiles.isNotEmpty) {
      return xFiles.map<ImageFile>((e) => convertXFileToImageFile(e)).toList();
    }
    return [];
  }
}
