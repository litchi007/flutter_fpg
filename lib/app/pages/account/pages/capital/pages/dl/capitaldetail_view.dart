import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/dl/capitaldetail_list_view_controller.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/dl/capitaldetail_list_view.dart';

class CapitalDetailListView extends StatefulWidget {
  const CapitalDetailListView({super.key});

  @override
  _CapitalDetailListViewState createState() => _CapitalDetailListViewState();
}

class _CapitalDetailListViewState extends State<CapitalDetailListView> {
  final CapitalDetaillistViewController controller =
      Get.put(CapitalDetaillistViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        toolbarHeight: 44.h,
        title: Row(children: [
          SizedBox(
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15).w,
                  child: GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Icon(Icons.arrow_back_ios,
                        color: AppColors.surface, size: 18.w),
                  ))),
          const Spacer(),
          Text('资金明细', style: AppTextStyles.surface_(context, fontSize: 24)),
          const Spacer(),
          SizedBox(width: 0.1.sw),
        ]),
      ),
      body: CapitaldetailListView(type: 'Transaction'),
    );
  }
}
