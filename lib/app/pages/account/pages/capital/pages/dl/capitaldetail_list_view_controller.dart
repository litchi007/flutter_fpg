import 'package:fpg_flutter/data/repositories/app_recharge_repository.dart';
import 'package:fpg_flutter/data/models/wd/CapitalDetailModel.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:intl/intl.dart';

class CapitalDetaillistViewController extends GetxController {
  final AppRechargeRepository _appRechargeRepository = AppRechargeRepository();
  RxBool isIniting = true.obs;

  int duration = 31;
  String endDate = '';
  String startDate = '';
  int page = 1;

  RxList<String> liqTypeList = RxList<String>();
  RxList<CapitalDetailModel> tableData = RxList();
  List<CapitalDetailModel> allTableData = [];
  RxString leftDropdownValue = '今天'.obs;
  RxString rightDropdownValue = '全部类型'.obs;
  Rx<int> selectTaskKey = 0.obs;
  RxList<String> taskKeys = ['余额', '利息宝'].obs;
  final sumAmount = '0.00'.obs;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  var dateSelect = [
    '今天',
    '最近7天',
    '最近31天',
  ];

  /// 加载状态
  final loading = true.obs;

  @override
  void onInit() {
    super.onInit();
    liqTypeList.add('全部类型');
    setDate();

    // fetchData();
  }

  void setDate() {
    if (leftDropdownValue.value == '今天') {
      duration = 0;
    } else if (leftDropdownValue.value == '最近7天') {
      duration = 7;
    } else if (leftDropdownValue.value == '最近31天') {
      duration = 31;
    }
    endDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
    startDate = DateFormat('yyyy-MM-dd')
        .format(DateTime.now().subtract(Duration(days: duration)));
    tableData.clear();

    onRefresh();
  }

  Future getRecordData(bool isRefresh) async {
    if (isRefresh) {
      page = 1;
      sumAmount.value = '0.00';
      tableData.clear();
    } else {
      page++;
    }

    await _appRechargeRepository
        .getCapitalDetailData(
            page: page,
            startDate: startDate,
            endDate: endDate,
            group: liqTypeList.indexOf(rightDropdownValue.value).toString())
        .then((data) {
      LogUtil.w(data?.toJson());
      sumAmount.value =
          double.parse(data?.sumCoin ?? '0.00').toStringAsFixed(2);
      if (data != null) {
        final tempList = data.list ?? [];
        tableData.addAll(tempList);

        if (isRefresh) {
          liqTypeList.clear();
          liqTypeList.add('全部类型');
          for (var item in data.groups!) {
            liqTypeList.add(item.name!);
          }
        }

        if (tempList.length < 20) {
          refreshController.loadNoData();
        } else {
          refreshController.loadComplete();
        }
      } else {
        refreshController.refreshCompleted();
        refreshController.loadComplete();
      }
    });
  }

  Future onRefresh() async {
    getRecordData(true);
  }

  Future onLoading() async {
    getRecordData(false);
  }
}
