import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/dl/capitaldetail_list_view_controller.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CapitaldetailListView extends StatefulWidget {
  const CapitaldetailListView({super.key, required this.type});
  final String type;
  @override
  _CapitaldetailListViewState createState() => _CapitaldetailListViewState();
}

class _CapitaldetailListViewState extends State<CapitaldetailListView> {
  final CapitalDetaillistViewController controller =
      Get.put(CapitalDetaillistViewController());

  @override
  void dispose() {
    Get.delete<CapitalDetaillistViewController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Obx(() => Column(
              children: [
                widget.type == 'FundingDetails'
                    ? Row(
                        children: controller.taskKeys.map((item) {
                          return GestureDetector(
                            onTap: () {
                              controller.selectTaskKey.value =
                                  controller.taskKeys.indexOf(item);
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.h, horizontal: 10.w),
                              width: 118.w,
                              height: 37.h,
                              decoration: BoxDecoration(
                                  color: item ==
                                          controller.taskKeys[
                                              controller.selectTaskKey.value]
                                      ? Colors.blue
                                      : null,
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(4))),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 4.0).h,
                                child: Text(
                                  item,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 18.sp,
                                    color: item ==
                                            controller.taskKeys[
                                                controller.selectTaskKey.value]
                                        ? Colors.white
                                        : Colors.black,
                                  ),
                                ),
                              ),
                            ),
                          );
                        }).toList(),
                      )
                    : SizedBox(
                        width: 10.w,
                      ),
                Table(
                    columnWidths: const {
                      0: FlexColumnWidth(3.5),
                      1: FlexColumnWidth(2),
                      2: FlexColumnWidth(4.5),
                      3: FlexColumnWidth(3),
                    },
                    border: TableBorder.all(
                        width: 1.w,
                        color: AppColors.ffC5CBC6,
                        style: BorderStyle.solid),
                    children: [
                      _firstTableRow(context),
                    ]),
                Expanded(
                    child: Obx(() => controller.tableData.isEmpty
                        ? Center(
                            child: Text(
                            '暂无数据',
                            style:
                                AppTextStyles.bodyText2(context, fontSize: 20),
                          ))
                        : SmartRefresher(
                            header: CustomHeader(
                              builder:
                                  (BuildContext context, RefreshStatus? mode) {
                                Widget body;
                                if (mode == RefreshStatus.refreshing) {
                                  body = Text("刷新中..."); // "Refreshing..."
                                } else if (mode == RefreshStatus.canRefresh) {
                                  body = Text("释放刷新"); // "Release to refresh"
                                } else if (mode == RefreshStatus.completed) {
                                  body = Text("刷新完成"); // "Refresh complete"
                                } else {
                                  body = Text("刷新失败"); // "Refresh failed"
                                }
                                return Container(
                                  height: 55,
                                  child: Center(child: body),
                                );
                              },
                            ),
                            footer: CustomFooter(
                              builder:
                                  (BuildContext context, LoadStatus? mode) {
                                Widget body;
                                if (mode == LoadStatus.loading) {
                                  body = Text(
                                      "加载中..."); // Change 'Loading' to Chinese here
                                } else if (mode == LoadStatus.failed) {
                                  body = Text("加载失败，请重试");
                                } else if (mode == LoadStatus.canLoading) {
                                  body = Text("释放立即加载更多");
                                } else {
                                  body = Text("没有更多数据");
                                }
                                return SizedBox(
                                  height: 55,
                                  child: Center(child: body),
                                );
                              },
                            ),
                            controller: controller.refreshController,
                            enablePullDown: false,
                            enablePullUp: true,
                            onRefresh: controller.onRefresh,
                            onLoading: controller.onLoading,
                            child: _renderTableContent(context),
                          ))),
                Container(
                    height: 50.h,
                    width: 1.sw,
                    color: AppColors.surface,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          '总计：',
                          textAlign: TextAlign.center,
                          style: AppTextStyles.bodyText2(context, fontSize: 20),
                        ),
                        Text(
                          '${controller.sumAmount} CNY',
                          textAlign: TextAlign.center,
                          style: AppTextStyles.bodyText2(context, fontSize: 20),
                        )
                      ],
                    ).paddingOnly(right: 20.w))
              ],
            )));
  }

  Widget _renderTableContent(BuildContext context) {
    return SingleChildScrollView(
        child: Table(
            columnWidths: const {
          0: FlexColumnWidth(3.5),
          1: FlexColumnWidth(2),
          2: FlexColumnWidth(4.5),
          3: FlexColumnWidth(3),
        },
            border: TableBorder.all(
                width: 1.w,
                color: AppColors.ffC5CBC6,
                style: BorderStyle.solid),
            children: (controller.selectTaskKey.value != 0
                ? List.generate(
                    controller.tableData.length,
                    (item) => const TableRow(children: [
                          TableCell(
                            child: SizedBox(),
                          ),
                          TableCell(
                            child: SizedBox(),
                          ),
                          TableCell(
                            child: SizedBox(),
                          ),
                          TableCell(
                            child: SizedBox(),
                          ),
                        ]))
                : controller.tableData.map((row) {
                    return TableRow(children: [
                      _tableCell(context, row.time ?? '',
                          const EdgeInsets.only(top: 15.0, bottom: 15.0).h),
                      _tableCell(context, row.changeMoney ?? '',
                          const EdgeInsets.only()),
                      _tableCell(
                          context, row.category ?? '', const EdgeInsets.only()),
                      _tableCell(
                          context, row.balance ?? '', const EdgeInsets.only()),
                    ]);
                  }).toList())));
  }

  Widget _tableCell(
    BuildContext context,
    String title,
    EdgeInsets padding,
  ) {
    return TableCell(
      verticalAlignment: TableCellVerticalAlignment.middle,
      child: Padding(
        padding: padding,
        child: Text(title,
            textAlign: TextAlign.center,
            style: AppTextStyles.bodyText3(context, fontSize: 18)),
      ),
    );
  }

  TableRow _firstTableRow(BuildContext context) {
    return TableRow(children: [
      TableCell(
        child: Padding(
          padding: const EdgeInsets.only(left: 4.0).w,
          child: DropdownButtonFormField<String>(
            decoration: const InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
              ),
            ),
            value: controller.leftDropdownValue.value,
            onChanged: (String? newValue) {
              controller.leftDropdownValue.value = newValue ?? '';
              controller.setDate();
            },
            items: controller.dateSelect
                .map<DropdownMenuItem<String>>((String curItem) {
              return DropdownMenuItem<String>(
                value: curItem,
                child: Container(
                  width: 0.12.sw,
                  child: Text(curItem,
                      textAlign: TextAlign.center,
                      style: AppTextStyles.bodyText3(context, fontSize: 14)),
                ),
              );
            }).toList(),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: const EdgeInsets.only(top: 30.0).h,
          child: Text('金额',
              textAlign: TextAlign.center,
              style: AppTextStyles.bodyText3(context, fontSize: 18)),
        ),
      ),
      TableCell(
        child: Container(
          width: 0.25.sw,
          child: Padding(
            padding: const EdgeInsets.only(left: 4.0).w,
            child: Obx(() => DropdownButtonFormField<String>(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                  ),
                  value: controller.rightDropdownValue.value,
                  onChanged: (String? newValue) {
                    controller.rightDropdownValue.value = newValue ?? '';
                    controller.onRefresh();
                  },
                  items: controller.liqTypeList
                      .map<DropdownMenuItem<String>>((String curItem) {
                    return DropdownMenuItem<String>(
                        value: curItem,
                        child: Container(
                          width: 0.2.sw,
                          child: Text(curItem,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: AppTextStyles.bodyText3(context,
                                  fontSize: 14)),
                        ));
                  }).toList(),
                )),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: const EdgeInsets.only(top: 30.0).h,
          child: Text('余额',
              textAlign: TextAlign.center,
              style: AppTextStyles.bodyText3(context, fontSize: 18)),
        ),
      )
    ]);
  }
}
