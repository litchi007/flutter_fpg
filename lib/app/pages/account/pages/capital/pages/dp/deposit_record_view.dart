import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/dp/deposit_record_view_controller.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/app/pages/account/widgets/custom_alert_dialog.dart';
import 'package:fpg_flutter/data/models/wd/DepositRecordModel.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DepositRecordView extends StatefulWidget {
  const DepositRecordView({super.key});

  @override
  _DepositRecordViewState createState() => _DepositRecordViewState();
}

class _DepositRecordViewState extends State<DepositRecordView> {
  final DepositRecordViewController controller =
      Get.put(DepositRecordViewController());
  late final CustomAlertDialog customAlertDialog;

  @override
  void initState() {
    super.initState();
    customAlertDialog = CustomAlertDialog(
      context: context,
      title: controller.title,
      categories: controller.getCategories(),
      titleBackgroundColor: AppColors.ffd1cfd0,
    );
  }

  @override
  void dispose() {
    Get.delete<DepositRecordViewController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Table(
          columnWidths: const {
            0: FlexColumnWidth(4),
            1: FlexColumnWidth(2),
            2: FlexColumnWidth(4),
            3: FlexColumnWidth(3),
          },
          border: TableBorder.all(
              width: 1.w, color: AppColors.ffC5CBC6, style: BorderStyle.solid),
          children: [
            TableRow(
              children: [
                TableCell(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 4.0).w,
                    child: DropdownButtonFormField<String>(
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                      ),
                      value: controller.leftDropdownValue.value,
                      onChanged: (String? newValue) {
                        controller.leftDropdownValue.value = newValue ?? '';
                        controller.setDate();
                      },
                      items: controller.dateSelect
                          .map<DropdownMenuItem<String>>((String curItem) {
                        return DropdownMenuItem<String>(
                          value: curItem,
                          child: Text(curItem,
                              textAlign: TextAlign.center,
                              style: AppTextStyles.bodyText3(context,
                                  fontSize: 14)),
                        );
                      }).toList(),
                    ),
                  ),
                ),
                TableCell(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 28.0).h,
                    child: Text('金额',
                        textAlign: TextAlign.center,
                        style: AppTextStyles.bodyText3(context, fontSize: 18)),
                  ),
                ),
                TableCell(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 4.0).w,
                    child: DropdownButtonFormField<String>(
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                      ),
                      value: controller.rightDropdownValue.value,
                      onChanged: (String? newValue) {
                        controller.rightDropdownValue.value = newValue ?? '';
                        controller.onRefresh();
                      },
                      items: controller.liqTypeList
                          .map<DropdownMenuItem<String>>((String curItem) {
                        return DropdownMenuItem<String>(
                          value: curItem,
                          child: Text(curItem,
                              textAlign: TextAlign.center,
                              style: AppTextStyles.bodyText3(context,
                                  fontSize: 14)),
                        );
                      }).toList(),
                    ),
                  ),
                ),
                TableCell(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 28.0).h,
                    child: Text('备注',
                        textAlign: TextAlign.center,
                        style: AppTextStyles.bodyText3(context, fontSize: 18)),
                  ),
                )
              ],
            ),
          ]),
      Expanded(
          child: Obx(
        () => (controller.tableData.isEmpty)
            ? Center(
                child: Text(
                  '暂无数据',
                  style: AppTextStyles.bodyText2(context, fontSize: 20),
                ),
              )
            : SmartRefresher(
                header: CustomHeader(
                  builder: (BuildContext context, RefreshStatus? mode) {
                    Widget body;
                    if (mode == RefreshStatus.refreshing) {
                      body = Text("刷新中..."); // "Refreshing..."
                    } else if (mode == RefreshStatus.canRefresh) {
                      body = Text("释放刷新"); // "Release to refresh"
                    } else if (mode == RefreshStatus.completed) {
                      body = Text("刷新完成"); // "Refresh complete"
                    } else {
                      body = Text("刷新失败"); // "Refresh failed"
                    }
                    return SizedBox(
                      height: 55.0,
                      child: Center(child: body),
                    );
                  },
                ),
                footer: CustomFooter(
                  builder: (BuildContext context, LoadStatus? mode) {
                    Widget body;
                    if (mode == LoadStatus.loading) {
                      body = Text("加载中..."); // Change 'Loading' to Chinese here
                    } else if (mode == LoadStatus.failed) {
                      body = Text("加载失败，请重试");
                    } else if (mode == LoadStatus.canLoading) {
                      body = Text("释放立即加载更多");
                    } else {
                      body = Text("没有更多数据");
                    }
                    return SizedBox(
                      height: 55,
                      child: Center(child: body),
                    );
                  },
                ),
                controller: controller.refreshController,
                enablePullDown: false,
                enablePullUp: true,
                cacheExtent: 50.h,
                onRefresh: controller.onRefresh,
                onLoading: controller.onLoading,
                child: _renderTableContent(context),
              ),
      )),
      Container(
          height: 50.h,
          width: 1.sw,
          color: AppColors.surface,
          alignment: Alignment.centerRight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                '总计：',
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
              Obx(() => Text(
                    controller.sumAmount.toString(),
                    textAlign: TextAlign.center,
                    style: AppTextStyles.bodyText2(context, fontSize: 20),
                  ))
            ],
          ).paddingOnly(right: 20.w))
    ]);

    return Obx(() => Column(children: [
          Table(
              columnWidths: const {
                0: FlexColumnWidth(4),
                1: FlexColumnWidth(2),
                2: FlexColumnWidth(4),
                3: FlexColumnWidth(3),
              },
              border: TableBorder.all(
                  width: 1.w,
                  color: AppColors.ffC5CBC6,
                  style: BorderStyle.solid),
              children: [
                TableRow(
                  children: [
                    TableCell(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4.0).w,
                        child: DropdownButtonFormField<String>(
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                          ),
                          value: controller.leftDropdownValue.value,
                          onChanged: (String? newValue) {
                            controller.leftDropdownValue.value = newValue ?? '';
                            controller.setDate();
                          },
                          items: controller.dateSelect
                              .map<DropdownMenuItem<String>>((String curItem) {
                            return DropdownMenuItem<String>(
                              value: curItem,
                              child: Text(curItem,
                                  textAlign: TextAlign.center,
                                  style: AppTextStyles.bodyText3(context,
                                      fontSize: 14)),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    TableCell(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 28.0).h,
                        child: Text('金额',
                            textAlign: TextAlign.center,
                            style:
                                AppTextStyles.bodyText3(context, fontSize: 18)),
                      ),
                    ),
                    TableCell(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4.0).w,
                        child: DropdownButtonFormField<String>(
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                          ),
                          value: controller.rightDropdownValue.value,
                          onChanged: (String? newValue) {
                            controller.rightDropdownValue.value =
                                newValue ?? '';
                            controller.onRefresh();
                          },
                          items: controller.liqTypeList
                              .map<DropdownMenuItem<String>>((String curItem) {
                            return DropdownMenuItem<String>(
                              value: curItem,
                              child: Text(curItem,
                                  textAlign: TextAlign.center,
                                  style: AppTextStyles.bodyText3(context,
                                      fontSize: 14)),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    TableCell(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 28.0).h,
                        child: Text('备注',
                            textAlign: TextAlign.center,
                            style:
                                AppTextStyles.bodyText3(context, fontSize: 18)),
                      ),
                    )
                  ],
                ),
              ]),
          Expanded(
              child: Stack(
            alignment: Alignment.center,
            children: [
              Obx(
                () => (controller.tableData.isEmpty)
                    ? Text(
                        '暂无数据',
                        style: AppTextStyles.bodyText2(context, fontSize: 20),
                      )
                    : SmartRefresher(
                        header: CustomHeader(
                          builder: (BuildContext context, RefreshStatus? mode) {
                            Widget body;
                            if (mode == RefreshStatus.refreshing) {
                              body = Text("刷新中..."); // "Refreshing..."
                            } else if (mode == RefreshStatus.canRefresh) {
                              body = Text("释放刷新"); // "Release to refresh"
                            } else if (mode == RefreshStatus.completed) {
                              body = Text("刷新完成"); // "Refresh complete"
                            } else {
                              body = Text("刷新失败"); // "Refresh failed"
                            }
                            return Container(
                              height: 55.0,
                              child: Center(child: body),
                            );
                          },
                        ),
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus? mode) {
                            Widget body;
                            if (mode == LoadStatus.loading) {
                              body = Text(
                                  "加载中..."); // Change 'Loading' to Chinese here
                            } else if (mode == LoadStatus.failed) {
                              body = Text("加载失败，请重试");
                            } else if (mode == LoadStatus.canLoading) {
                              body = Text("释放立即加载更多");
                            } else {
                              body = Text("没有更多数据");
                            }
                            return SizedBox(
                                height: 100.h,
                                child: Column(
                                  children: [
                                    Center(child: body),
                                    SizedBox(
                                      height: 50.h,
                                    )
                                  ],
                                ));
                          },
                        ),
                        controller: controller.refreshController,
                        enablePullDown: false,
                        enablePullUp: true,
                        cacheExtent: 50.h,
                        onRefresh: controller.onRefresh,
                        onLoading: controller.onLoading,
                        child: _renderTableContent(context),
                      ),
              ),
              Positioned(
                  right: 0.w,
                  bottom: 0.h,
                  child: Container(
                      height: 50.h,
                      width: 1.sw,
                      color: AppColors.surface,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            '总计：',
                            textAlign: TextAlign.center,
                            style:
                                AppTextStyles.bodyText2(context, fontSize: 20),
                          ),
                          Text(
                            controller.sumAmount.toString(),
                            textAlign: TextAlign.center,
                            style:
                                AppTextStyles.bodyText2(context, fontSize: 20),
                          )
                        ],
                      ).paddingOnly(right: 20.w)))
            ],
          ))
        ]));
  }

  Widget _renderTableContent(BuildContext context) {
    return SingleChildScrollView(
        child: Table(
            columnWidths: const {
          0: FlexColumnWidth(3.33),
          1: FlexColumnWidth(1.675),
          2: FlexColumnWidth(3.35),
          3: FlexColumnWidth(2.5),
          // 4: FlexColumnWidth(2),
        },
            border: TableBorder.all(
                width: 1.w,
                color: AppColors.ffC5CBC6,
                style: BorderStyle.solid),
            children: controller.tableData.map((row) {
              DateTime dateTime = DateTime.parse(row.arriveTime ?? '');
              String formattedDate =
                  DateFormat('MM/dd HH:mm:ss').format(dateTime);
              return TableRow(children: [
                _tableCell(context, formattedDate, customAlertDialog, row),
                _tableCell(context, (row.amount ?? ''), customAlertDialog, row),
                // _tableCell(context, row.category ?? '', customAlertDialog, row),
                (controller.userInfo.isTrial != '1')
                    ? _tableCell(
                        context, row.status ?? '', customAlertDialog, row)
                    : _tableCell(
                        context, row.status ?? '', customAlertDialog, row),
                (controller.userInfo.isTrial != '1')
                    ? _tableCell(
                        context, row.remark ?? '', customAlertDialog, row)
                    : _tableCell(
                        context, row.remark ?? '', customAlertDialog, row)
              ]);
            }).toList()));
  }

  Widget _tableCell(BuildContext context, String title,
      CustomAlertDialog customAlertDialog, DepositRecordModel row) {
    return GestureDetector(
      onTap: () {
        customAlertDialog.dialogBuilder(_getDataForDialog(row));
      },
      child: SizedBox(
        height: 70.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(title,
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText3(context, fontSize: 18)),
          ],
        ),
      ),
    );
  }

  List<dynamic> _getDataForDialog(DepositRecordModel row) {
    if (AppDefine.inSites('t59f-ada,c243') &&
        GlobalService.to.userInfo.value?.isTrial == '1') {
      return [
        row.orderNo,
        row.applyTime,
        row.amount,
        row.status,
        row.username,
        row.arriveTime,
        row.remark
      ];
    } else {
      return [
        row.orderNo,
        row.applyTime,
        row.category,
        row.amount,
        row.status,
        row.username,
        row.arriveTime,
        row.remark
      ];
    }
  }
}
