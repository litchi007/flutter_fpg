import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/tutorials/help_doc_view_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/const/PayListConst.dart';

class HelpDocView extends StatefulWidget {
  const HelpDocView({super.key});

  @override
  State<HelpDocView> createState() => _HelpDocViewState();
}

class _HelpDocViewState extends State<HelpDocView>
    with TickerProviderStateMixin {
  HelpDocViewController controller = Get.put(HelpDocViewController());
  late TabController _tabController;
  dynamic argumentsData;
  String pageName = '';
  List<String> images = [];
  @override
  void initState() {
    super.initState();
    argumentsData = Get.arguments;
    images = argumentsData[0];
    pageName = argumentsData[1];
    _tabController = TabController(length: categoryTabs.length, vsync: this);
    _tabController.addListener(_handleTabChange);
    controller.xnbName = '火币';
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      controller.xnbTabIndex.value = _tabController.index;
      _tabController.index == 0
          ? controller.xnbName = '火币'
          : controller.xnbName = '币安';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.surface,
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 22.w),
              ]),
            )),
        title: Text('$pageName教程',
            style: AppTextStyles.surface_(context,
                fontWeight: FontWeight.bold, fontSize: 24)),
        centerTitle: true,
        leadingWidth: 0.3.sw,
        bottom: images.isEmpty
            ? PreferredSize(
                preferredSize: Size.fromHeight(46.h),
                child: Container(
                  color: AppColors.ffbfa46d,
                  child: _createTab(),
                ),
              )
            : PreferredSize(
                preferredSize: Size.fromHeight(1.h), child: SizedBox()),
      ),
      body: images.isNotEmpty
          ? ListView(
              children: List.generate(
                  images.length, (index) => AppImage.network(images[index])),
            )
          : Column(children: [
              Expanded(
                child: TabBarView(
                    controller: _tabController, children: _xnbView()),
              ),
            ]),
    );
  }

  List<Widget> _xnbView() {
    return [
      _tabContent(),
      _tabContent(),
    ];
  }

  Widget _tabContent() {
    return Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Expanded(
        child: Padding(
          padding: const EdgeInsets.only(left: 12.0).w,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...list.map((item) => _categoryTab(item.title!, item.content!)),
            ],
          ),
        ),
      ),
      const VerticalDivider(
        width: 1.0,
        thickness: 1.0,
        color: AppColors.ffaeaeae,
      ),
      Expanded(
          flex: 2,
          child: Padding(
              padding: const EdgeInsets.all(12).w,
              child: Obx(() => _creatContent(controller
                  .showedItemNo[controller.xnbTabIndex.value].value))))
    ]);
  }

  Widget _creatContent(String index) {
    switch (index) {
      case '1':
        return ListView(
          children: [
            Text(
              '虚拟币充值教程',
              style: AppTextStyles.bodyText2(context, fontSize: 24),
            ),
            _headerText('1、概述'),
            _text('虚拟币包括BTC（比特币）、ETH（以太坊）、USDT等，是去中心化的货币，全球通用，并且具有不可篡改的特性。'),
            _text('USDT完全挂钩美元，价值较为稳定。BTC、ETH等因为交易频率很高，所以在价值上波动较大。'),
          ],
        );
      case '2':
        return ListView(
          children: [
            _headerText('2、安装火币钱包'),
            _text(
                '首先您需要一个${controller.xnbName}钱包，安卓版下载地址：${controller.xnbAndroidDownloadUrl}；iPhone下载地址请联系7*24在线客服获取ID进行下载。 '),
            _image(1)
          ],
        );
      case '3':
        return ListView(
          children: [
            _headerText('3、注册账号'),
            _image(2),
            _image(3),
          ],
        );
      case '4':
        return ListView(
          children: [
            _headerText('4、购买虚拟币'),
            _image(4),
            _image(5),
            _image(6),
            _image(7),
          ],
        );
      case '5':
        return ListView(
          children: [
            _headerText('5、确认存款金额'),
            _text('在网站充值时，输入想要存入的人民币金额，以获取相应的虚拟币金额。”'),
            _image(8),
          ],
        );
      case '6':
        return ListView(
          children: [
            _headerText('6、钱包付款'),
            _text(
                '因为虚拟币需要多个网络节点确认，所以付款后通常需要1-10分钟显示到账，您可以在${controller.xnbName}钱包的“记录查询”中查看到账状态。'),
            _image(9),
            _image(10),
            _image(11),
            _image(12),
          ],
        );
      case '7':
        return ListView(
          children: [
            _headerText('7、完成充值'),
            _text('完成付款后，返回至“虚拟币充值”页，点击“提交申请”后即可完成充值。'),
            _image(13),
          ],
        );

      default:
        return const SizedBox();
    }
  }

  Widget _text(String title, {int fontSize = 20}) {
    return Padding(
      padding: const EdgeInsets.all(8.0).w,
      child: Text(title,
          style: AppTextStyles.bodyText2(context, fontSize: fontSize)),
    );
  }

  Widget _headerText(String title, {int fontSize = 20}) {
    return Padding(
      padding: const EdgeInsets.all(8.0).w,
      child: Text(title,
          style: AppTextStyles.ff999999_(context, fontSize: fontSize)),
    );
  }

  Widget _categoryTab(String title, String path) {
    return SizedBox(
      child: GestureDetector(
          onTap: () {
            controller.setShowedItemNo(path);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 14.0).h,
            child: Center(
                child: Obx(
              () => Text(path == '2' ? '安装${controller.xnbName}钱包' : title,
                  style: controller.showedItemNo[controller.xnbTabIndex.value]
                              .value ==
                          path
                      ? AppTextStyles.error_(context, fontSize: 20)
                      : AppTextStyles.bodyText2(context, fontSize: 20)),
            )),
          )),
    );
  }

  Widget _image(int index) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0).h,
      child: AppImage.network(controller.getImage(index), fit: BoxFit.contain),
    );
  }

  Widget _imageJt(String path) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0).h,
      child: AppImage.network(img_jt(path), fit: BoxFit.fill),
    );
  }

  TabBar _createTab() {
    return TabBar(
      controller: _tabController,
      indicatorColor: AppColors.error,
      labelColor: AppColors.error,
      unselectedLabelColor: AppColors.onBackground,
      indicatorSize: TabBarIndicatorSize.tab,
      physics: const ClampingScrollPhysics(),
      tabAlignment: TabAlignment.fill,
      isScrollable: false,
      labelStyle: AppTextStyles.surface_(context,
          fontWeight: FontWeight.bold, fontSize: 22),
      tabs: List.generate(categoryTabs.length, (index) {
        return Tab(
          text: '${categoryTabs[index]['name']}',
        );
      }),
    );
  }
}
