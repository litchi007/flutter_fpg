import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:fpg_flutter/configs/app_define.dart';

class HelpDocViewController extends GetxController {
  RxBool isIniting = false.obs;
  List<RxString> showedItemNo = ['1'.obs, '1'.obs];
  RxBool showMenu = false.obs;
  RxDouble heightHeader = 0.0.obs;
  String xnbName = '';
  String xnbAndroidDownloadUrl = '';
  @override
  void onInit() {
    super.onInit();
    fetchData();
  }

  RxInt xnbTabIndex = 0.obs;

  void fetchData() async {}

  void setShowSubmenu(bool show) {
    showMenu.value = show;
  }

  void setShowedItemNo(String path) {
    showedItemNo[xnbTabIndex.value].value = path;
  }

  String getHuobiImage(int index) {
    return '${AppDefine.host}/static/images/czjc/huobi/h5/zh/huobi$index.png';
  }

  String getBianImage(int index) {
    if (index == 8) {
      return '${AppDefine.host}/static/images/czjc/bian/${index}_h5.png';
    } else {
      return '${AppDefine.host}/static/images/czjc/bian/$index.png';
    }
  }

  String getImage(int imageNo) {
    return xnbName == '火币' ? getHuobiImage(imageNo) : getBianImage(imageNo);
  }

  String calculateDownloadUrl(String xnbType) {
    return xnbType == 'huobi'
        ? 'www.huobi.me'
        : xnbType == 'bian'
            ? 'www.binance.com'
            : '';
  }
}
