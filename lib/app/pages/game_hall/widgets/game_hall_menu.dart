import 'package:flutter/material.dart';
import 'package:fpg_flutter/data/models/HallGameModel.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/page/lotteryPopup/controller/controller.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

class GameHallMenu extends GetView<LotteryPopupController> {
  const GameHallMenu({super.key});
  @override
  Widget build(BuildContext context) {
    Get.put(LotteryPopupController());
    return Scaffold(
        backgroundColor: Colors.transparent, // 设置背景透明
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                color: Colors.black.withOpacity(0.1),
              ),
            ),
            Positioned(
                top: MediaQuery.of(context).padding.top + kToolbarHeight - 15,
                right: 10,
                child: _buildList(context)),
          ],
        ));
  }

  Widget _buildList(BuildContext context) {
    List<Widget> children = [];

    children.add(_buildUnSettlementItem('即时注单', context, () {
      Get.back();
      Get.toNamed(lotterynotcountPath);
    }));

    children.add(_buildItem('今日已结', context, () {
      Get.toNamed(AppRoutes.todaySettled);
    }));

    children.add(_buildItem('下注记录', context, () {
      if (!GlobalService.isShowTestToast) {
        Get.toNamed(AppRoutes.lotteryTickets);
      }
    }));

    // children.add(_buildItem('开奖结果', context, () {
    //   Get.toNamed(AppRoutes.lotteryResult, arguments: lottery);
    // }));

    children.add(_buildItem('提现', context, () {
      Get.toNamed(depositPath, arguments: [1]);
    }));

    return Container(
      width: 110,
      decoration: BoxDecoration(
        border: Border.all(
          color: context.customTheme?.lotteryPopupDivColor ??
              Colors.transparent, // 边框颜色
          width: 1, // 边框宽度
        ),
      ),
      child: Column(
        children: children,
      ),
    );
  }

  // 即时注单
  Widget _buildUnSettlementItem(
      String title, BuildContext context, GestureTapCallback? onTap) {
    return Container(
        color: context.customTheme?.lotteryPopupItemBg,
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            Obx(() {
              return controller.lotteryData.value.unbalancedMoney != null
                  ? Text("(${controller.lotteryData.value.unbalancedMoney})",
                      style: const TextStyle(
                          color: Color(0xfffe5337),
                          fontSize: 16,
                          fontWeight: FontWeight.w600))
                  : const SizedBox(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.red)));
            }),
            const SizedBox(height: 6),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
          ],
        )).onTap(onTap);
  }

  // 其他
  Widget _buildItem(
      String title, BuildContext context, GestureTapCallback? onTap) {
    return Container(
        color: context.customTheme?.lotteryPopupItemBg,
        // height: 35,
        child: Column(
          children: [
            const SizedBox(height: 6),
            Text(
              title,
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 6),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
          ],
        )).onTap(() {
      Get.back();
      if (onTap != null) {
        onTap();
      }
    });
  }
}
