import 'package:fpg_flutter/data/models/HallGameModel.dart';
import 'package:fpg_flutter/data/repositories/api_ticket.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/get_rx.dart';

class GCWGameHallController extends GetxController {
  final AppGameRepository _appGameRepository = AppGameRepository();
  final ApiTicket _apiTicket = ApiTicket();
  final RxString unbalancedMoney = "0.0".obs;
  RxList<GroupGameData> groupGameList = RxList();

  Future initData() async {
    _appGameRepository.lotteryGroupGames(showIssueType: 1).then((data) {
      if (data == null) return;
      groupGameList.value = data;
    });

    _apiTicket.getLotteryData().then((data) {
      try {
        unbalancedMoney.value = data ?? "0.0";
      } catch (e) {
        unbalancedMoney.value = "0.0";
      }
    });
  }
}
