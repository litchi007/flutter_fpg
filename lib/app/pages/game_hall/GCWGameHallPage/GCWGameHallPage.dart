import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/pages/game_hall/GCWGameHallPage/GCWGameHallController.dart';
import 'package:fpg_flutter/app/pages/game_hall/widgets/game_hall_menu.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/page/lotteryPopup/lotteryPopup.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

class GCWGameHallPage extends StatefulWidget {
  const GCWGameHallPage({Key? key}) : super(key: key);

  @override
  _GCWGameHallPageState createState() => _GCWGameHallPageState();
}

class _GCWGameHallPageState extends State<GCWGameHallPage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  final GCWGameHallController controller = Get.put(GCWGameHallController());

  @override
  void initState() {
    controller.initData();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // controller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      key: _homeKey,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        titleWidget: Row(
          children: [
            // Obx(() => AppImage.network(
            //       controller.appLogo.value,
            //       height: 50.w,
            //       width: 250.w,
            //       fit: BoxFit.fill,
            //     )),
            GestureDetector(
              onTap: () {
                MainHomeController.to.selectedPath.value = homePath;
              },
              child: Icon(
                Icons.home,
                color: Colors.white,
                size: 40.w,
              ),
            ),
            Expanded(
              child: Center(
                child: Text(
                  '彩票大厅',
                  style: TextStyle(color: Colors.white, fontSize: 20.sp),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Get.to(() => GameHallMenu(),
                    opaque: false,
                    fullscreenDialog: true,
                    transition: Transition.noTransition);
              },
              child: Icon(
                Icons.menu_rounded,
                size: 40.w,
                color: Colors.white,
              ),
            ),
          ],
        ),
        // actionWidgets: [GlobalService.to.isAuthenticated.value? _showMenu(): Container()],
      ),
      body: Obx(
        () => ListView.builder(
            padding: EdgeInsets.only(left: 10.w, right: 10.w, top: 10.w),
            shrinkWrap: true,
            itemCount: controller.groupGameList.length,
            itemBuilder: (context, index) {
              final groupGame = controller.groupGameList[index];
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('${groupGame.name}'),
                  GridView.builder(
                      padding: EdgeInsets.only(top: 10.w, bottom: 10.w),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, // Number of columns
                        crossAxisSpacing: 10.0,
                        mainAxisSpacing: 10.0,
                        childAspectRatio: 3,
                      ),
                      itemCount: (groupGame.lotteries ?? []).length,
                      itemBuilder: (context, index1) {
                        final item = groupGame.lotteries![index1];
                        return GestureDetector(
                          onTap: () {
                            Get.toNamed('${AppRoutes.lottery}/${item.id}');
                          },
                          child: Container(
                            padding: EdgeInsets.only(left: 20.w),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20.w),
                              // border: Border.all(width: 0.5.w)
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(
                                      0.2), // Shadow color with opacity
                                  spreadRadius:
                                      1, // How much the shadow spreads
                                  blurRadius: 1, // The softness of the shadow
                                  offset: const Offset(0,
                                      1), // Horizontal and vertical offset of the shadow
                                ),
                              ],
                            ),
                            child: Row(
                              children: [
                                AppImage.network('${item.logo}',
                                    width: 60.w,
                                    height: 60.w,
                                    errorWidget: AppImage.asset(
                                      'defaultIcon.png',
                                      width: 60.w,
                                      height: 60.w,
                                    )),
                                SizedBox(
                                  width: 20.w,
                                ),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${item.title}',
                                      maxLines: 2,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                    const Text(
                                      '立即游戏',
                                      style: TextStyle(
                                          color: Color(0xffff0000),
                                          fontSize: 13),
                                    ),
                                  ],
                                )),
                              ],
                            ),
                          ),
                        );
                      }),
                ],
              );
            }),
      ),
    );
  }
}
