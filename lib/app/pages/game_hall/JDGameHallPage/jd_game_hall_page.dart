import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/pages/home/widgets/forum_game_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/new_block_widget/news_block_widget.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

class JDGameHallPage extends StatefulWidget {
  const JDGameHallPage({Key? key}) : super(key: key);

  @override
  _JDGameHallPageState createState() => _JDGameHallPageState();
}

class _JDGameHallPageState extends State<JDGameHallPage> {
  final GlobalKey<ScaffoldState> _homeKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      key: _homeKey,
      backgroundColor: appThemeColors?.homeBackgroundColor,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.homeHeaderColor,
        titleWidget: Stack(
          alignment: AlignmentDirectional.centerEnd,
          children: [
            Row(
              children: [
                AppImage.network(
                  '${AppDefine.systemConfig?.mobileLogo ?? ''}',
                  height: 50.w,
                  width: 250.w,
                  fit: BoxFit.fill,
                ),
              ],
            ),
            GestureDetector(
              onTap: () {
                _homeKey.currentState!.openEndDrawer();
              },
              child: AppImage.asset('menu_btn.png', width: 30.w, height: 30.w),
            ),
          ],
        ),
        actionWidgets: const [SizedBox()],
      ),
      endDrawer: Container(
        color: Colors.white,
        width: Get.width * 0.5,
        height: Get.height,
        child: const HomeMenuWidget(),
      ),
      body: SafeArea(
        child: SingleChildScrollView(child: ForumGameWidget()),
      ),
    );
  }
}
