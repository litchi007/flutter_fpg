import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/app/pages/home/rank_list_widget.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_notice_widget.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class HomeViewOther extends StatefulWidget {
  const HomeViewOther({super.key});

  @override
  _HomeViewOtherState createState() => _HomeViewOtherState();
}

class _HomeViewOtherState extends State<HomeViewOther> {
  final AuthController authController = Get.find();
  final HomeViewController controller = Get.put(HomeViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppGeneralBar(
        titleWidget: Row(
          children: [
            AppImage.network(
              '${controller.appLogo}',
              height: 50.w,
              width: 300.w,
              fit: BoxFit.fill,
            )
          ],
        ),
        actionWidgets: [
          // AppImage.svgByAsset(
          //   'tabbar/iconwode.svg',
          //   width: 25.w,
          //   height: 25.w,
          // ),
          GestureDetector(
            onTap: () {
              // AppNavigator.toNamed(loginPath);
            },
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xff5D93BC))),
              padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10.w),
              child: const Text(
                '登录',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),

          Gap(10.w),
          GestureDetector(
            onTap: () {
              // AppNavigator.toNamed(registerPath);
            },
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xff5D93BC))),
              padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10.w),
              child: const Text(
                '注册',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Gap(10.w)
          // Icon(
          //   Icons.menu,
          //   color: Colors.white,
          // )
        ],
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_1.jpg'),
            fit: BoxFit.cover, // Makes sure the image covers the whole area
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Obx(
                () => CarouselSlider(
                  options: CarouselOptions(
                    height: 280.w,
                    viewportFraction: 1.0,
                    enlargeCenterPage: false,
                    autoPlay: true,
                  ),
                  items: controller.banners
                      .map((item) => Center(
                              child: AppImage.network(
                            item.pic ?? "",
                            fit: BoxFit.cover,
                            height: 280.w,
                          )))
                      .toList(),
                ),
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 10.w),
                color: const Color(0xffB7CBDD),
                child: Obx(
                  () => AppNoticeWidget(
                    titleList: controller
                        .homeNoticeData.value.scroll, //controller.notice,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.w),
                padding: EdgeInsets.symmetric(vertical: 20.w),
                decoration: BoxDecoration(
                    color: const Color(0xffB7CBDD),
                    borderRadius: BorderRadius.circular(20.w)),
                child: Obx(
                  () => Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ...List.generate(controller.navList.length, (index) {
                        final navItemName = controller.navList[index].name;
                        final navItemLogo = controller.navList[index].icon;
                        return Column(
                          children: [
                            AppImage.network('$navItemLogo',
                                width: 50.w, height: 50.w),
                            Text('$navItemName'),
                          ],
                        );
                      }),
                    ],
                  ),
                ),
              ),
              Obx(
                () => CarouselSlider(
                  options: CarouselOptions(
                    height: 120.w,
                    viewportFraction: 1.0,
                    enlargeCenterPage: false,
                    autoPlay: true,
                  ),
                  items: controller.homeAdsList
                      .map((item) => Center(
                              child: AppImage.network(
                            item.image != null ? item.image! : '',
                            fit: BoxFit.cover,
                            height: 120.w,
                          )))
                      .toList(),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.w),
                padding: EdgeInsets.symmetric(vertical: 10.w),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color(0xffB7CBDD),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.w),
                    topRight: Radius.circular(20.w),
                  ),
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Obx(
                    () => Row(
                      children: [
                        ...List.generate(controller.iconList.length, (index) {
                          final navItemName = controller.iconList[index].name;
                          return GestureDetector(
                            onTap: () {
                              controller.selectedTabIndex1.value = index;
                            },
                            child: Obx(
                              () => Container(
                                margin: EdgeInsets.symmetric(horizontal: 10.w),
                                decoration:
                                    controller.selectedTabIndex1.value == index
                                        ? const BoxDecoration(
                                            border: Border(
                                              bottom: BorderSide(
                                                  color: Color(0xfff44600)),
                                            ),
                                          )
                                        : null,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 10.w,
                                          right: 10.w,
                                          bottom: 10.w),
                                      child: Text(
                                        '$navItemName',
                                        style: TextStyle(
                                            color: controller.selectedTabIndex1
                                                        .value ==
                                                    index
                                                ? const Color(0xfff44600)
                                                : const Color(0xFF000000),
                                            fontSize: 20.sp),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        })
                      ],
                    ),
                  ),
                ),
              ),
              Obx(
                () => Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, // Number of columns
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 10.0,
                      // mainAxisExtent: 110.w,
                      // childAspectRatio: 3, // Adjust the ratio to match your design
                    ),
                    itemCount: (controller.iconList.isNotEmpty
                            ? controller
                                    .iconList[
                                        controller.selectedTabIndex1.value]
                                    .list ??
                                []
                            : [])
                        .length,
                    itemBuilder: (context, index) {
                      final item = controller
                          .iconList[controller.selectedTabIndex1.value]
                          .list?[index];
                      if (item == null) return const SizedBox();
                      return GridItemWidget(item: item);
                    },
                  ),
                ),
              ),

              Obx(() => RankListWidget(
                    list: controller.rankList.value,
                  ))

              // Padding(
              //   padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       Row(
              //         children: [
              //           Icon(
              //             Icons.card_giftcard,
              //             size: 16,
              //           ),
              //           Text('优惠活动')
              //         ],
              //       ),
              //       Row(
              //         children: [Text('查看更多>>')],
              //       ),
              //     ],
              //   ),
              // ),
              // Container(
              //   margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 5.w),
              //   padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 10.w),
              //   decoration: BoxDecoration(
              //     color: Color(0xffB7CBDD),
              //     borderRadius: BorderRadius.circular(20.w),
              //   ),
              //   child: AppImage.network(
              //       'https://wwwstatic02.fdgdggduydaa008aadsdf008.xyz/upload/t002/customise/images/mb_promote_44.jpg?v=1714538540',
              //       width: double.infinity),
              // ),
              // Container(
              //   margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 5.w),
              //   padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 10.w),
              //   decoration: BoxDecoration(
              //     color: Color(0xffB7CBDD),
              //     borderRadius: BorderRadius.circular(20.w),
              //   ),
              //   child: AppImage.network(
              //       'https://wwwstatic02.fdgdggduydaa008aadsdf008.xyz/upload/t002/customise/images/mb_promote_51.jpg?v=1716555150',
              //       width: double.infinity),
              // ),
              // Container(
              //   margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 5.w),
              //   padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 10.w),
              //   decoration: BoxDecoration(
              //     color: Color(0xffB7CBDD),
              //     borderRadius: BorderRadius.circular(20.w),
              //   ),
              //   child: AppImage.network(
              //       'https://wwwstatic02.fdgdggduydaa008aadsdf008.xyz/upload/t002/customise/images/mb_promote_51.jpg?v=1716555150',
              //       width: double.infinity),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

// class GridItem {
//   final String imageUrl;
//   final String label;

//   GridItem({required this.imageUrl, required this.label});
// }

class GridItemWidget extends StatelessWidget {
  final GameModel item;

  const GridItemWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.w),
        color: const Color(0xffB7CBDD),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AppImage.network(
            '${item.icon}',
            width: 80.w,
            height: 80.w,
          ),
          SizedBox(width: 30.h),
          Text(item.name!),
        ],
      ),
    );
  }
}
