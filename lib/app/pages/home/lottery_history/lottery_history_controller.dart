import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/LotteryHistoryItemModel.dart';
import 'package:fpg_flutter/data/models/LotteryHistoryModel.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class LotteryHistoryController extends GetxController {
  final AppGameRepository _gameRepository = AppGameRepository();
  RxInt curYear = 2024.obs;
  RxString sortMode = "升序".obs;
  RxString sortCode = "默认".obs;
  String lotteryId = AppDefine.defaultLotteryId();
  Rxn<LotteryHistoryModel> lotteryHistory = Rxn();
  List<LotteryHistoryItemModel> lotteryHistoryItems = [];

  Future<void> getLotteryHistory() async {
    AppToast.show();
    await _gameRepository
        .getLotteryHistory(id: lotteryId, rows: 1000)
        .then((data) {
      AppToast.dismiss();
      if (data != null) {
        lotteryHistoryItems = data.list;
        lotteryHistory.value = LotteryHistoryModel(
            list: extractHistory(),
            redBalls: data.redBalls,
            greenBalls: data.greenBalls,
            blueBalls: data.blueBalls);
      }
    });
  }

  List<LotteryHistoryItemModel> extractHistory() {
    List<LotteryHistoryItemModel> list = lotteryHistoryItems
        .where((item) => (item.openTime ?? "").startsWith(curYear.toString()))
        .toList();
    if (sortMode.value == '升序') {
      list.sort((a, b) =>
          int.parse(b.displayNumber ?? "0") -
          int.parse(a.displayNumber ?? "0"));
    } else {
      list.sort((a, b) =>
          int.parse(a.displayNumber ?? "0") -
          int.parse(b.displayNumber ?? "0"));
    }
    return list;
  }

  void reSort() {
    lotteryHistory.value?.list = extractHistory();
  }
}
