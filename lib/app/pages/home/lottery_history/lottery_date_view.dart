import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class LotteryDateView extends StatefulWidget {
  const LotteryDateView({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LotteryDateViewState();
}

class _LotteryDateViewState extends State<LotteryDateView> {
  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: AppColors.ffff2f2f2,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '搅珠日期',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    margin:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: AppColors.drawMenu, width: 2.w),
                        borderRadius: BorderRadius.circular(20.w)),
                    height: 120.h,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("简介",
                            style: TextStyle(
                                fontSize: 22.sp, fontWeight: FontWeight.bold)),
                        Gap(5.h),
                        Text("搅珠日期对照表，可查看当月及下一个月的搅珠开奖日期",
                            style: TextStyle(fontSize: 18.sp)),
                      ],
                    ))),
            showCalendar(),
          ],
        ));
  }

  Widget showCalendar() {
    List<String> weeks = ['日', '一', '二', '三', '四', '五', '六'];
    List<int> days1 = [], days2 = [];
    DateTime curDate = DateTime.now();
    DateTime d1 = DateTime(curDate.year, curDate.month);
    DateTime n1 = DateTime(curDate.year, curDate.month + 1);
    DateTime d30 = n1.subtract(const Duration(days: 1));
    DateTime n30 = DateTime(curDate.year, curDate.month + 2)
        .subtract(const Duration(days: 1));

    for (int i = 0; i <= d1.weekday % 7 - 1; i++) {
      days1.add(i - d1.weekday + 1);
    }
    for (int i = 1; i <= d30.day; i++) {
      days1.add(i);
    }
    for (int i = d30.weekday % 7 + 1; i <= 6; i++) {
      days1.add(d30.weekday - 6 - 1);
    }

    for (int i = 0; i <= n1.weekday % 7 - 1; i++) {
      days2.add(i - n1.weekday + 1);
    }
    for (int i = 1; i <= n30.day; i++) {
      days2.add(i);
    }
    for (int i = n30.weekday % 7 + 1; i <= 6; i++) {
      days2.add(n30.weekday - 6 - 1);
    }

    RxList<int> days = days1.obs;
    RxInt y = d1.year.obs;
    RxInt m = d1.month.obs;
    RxInt d = curDate.day.obs;

    return Container(
        margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.ff666666, width: 2.w),
        ),
        child: Obx(() => Column(children: [
              Container(
                  padding: EdgeInsets.symmetric(vertical: 2.h),
                  alignment: Alignment.center,
                  color: AppColors.drawMenu,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Opacity(
                          opacity: (m.value == n1.month) ? 1 : 0,
                          child: IconButton(
                              onPressed: () {
                                days.value = days1;
                                y.value = d1.year;
                                m.value = d1.month;
                                d.value = curDate.day;
                              },
                              icon: const Icon(Icons.chevron_left_rounded,
                                  color: Colors.white))),
                      Text("${y.value}年${m.value}月",
                          style:
                              TextStyle(color: Colors.white, fontSize: 20.sp)),
                      Opacity(
                          opacity: (m.value == d1.month) ? 1 : 0,
                          child: IconButton(
                              onPressed: () {
                                days.value = days2;
                                y.value = n1.year;
                                m.value = n1.month;
                                d.value = curDate.day;
                              },
                              icon: const Icon(Icons.chevron_right_rounded,
                                  color: Colors.white))),
                    ],
                  )),
              Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 2.h, horizontal: 16.w),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: List.generate(7, (index) {
                        return Text(weeks[index],
                            style: TextStyle(
                                color: Colors.black, fontSize: 18.sp));
                      }))),
              GridView.count(
                crossAxisCount: 7,
                mainAxisSpacing: 5.h,
                crossAxisSpacing: 30.w,
                padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.w),
                primary: false,
                shrinkWrap: true,
                children: List.generate(days.length, (index) {
                  if (days[index] > 0) {
                    return GestureDetector(
                        onTap: () {
                          d.value = days[index];
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.w),
                                border: Border.all(
                                    color: days[index] == d.value
                                        ? Colors.red
                                        : AppColors.ff999999,
                                    width: days[index] == d.value ? 2.w : 1.w)),
                            child:
                                Center(child: Text(days[index].toString()))));
                  } else {
                    return Container();
                  }
                }),
              )
            ])));
  }
}
