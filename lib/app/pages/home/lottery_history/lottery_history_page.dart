import 'package:flutter/material.dart';
import 'package:flutter_popup_card/flutter_popup_card.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/lottery_history/lottery_history_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/custom_scroll_date_picker.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/LotteryHistoryItemModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class LotteryHistoryPage extends StatefulWidget {
  const LotteryHistoryPage({super.key});

  @override
  State<StatefulWidget> createState() => _LotteryHistoryPageState();
}

class _LotteryHistoryPageState extends State<LotteryHistoryPage> {
  LotteryHistoryController controller = Get.put(LotteryHistoryController());

  @override
  void initState() {
    super.initState();
    List<String?>? args = Get.arguments ?? [];
    if (args?[0] != null) {
      controller.lotteryId = args?[0] ?? AppDefine.defaultLotteryId();
    }
    controller.getLotteryHistory();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
        backgroundColor: AppColors.ffff2f2f2,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                '历史开奖',
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          actionWidgets: [
            GestureDetector(
                onTap: showBottomSheet,
                child: Container(
                  padding: EdgeInsets.only(right: 10.w),
                  child: Icon(Icons.calendar_month_rounded,
                      size: 30.w, color: Colors.white),
                ))
          ],
        ),
        body: Obx(() => Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 5.h),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: TextButton(
                          onPressed: () {},
                          child: Text("开奖记录",
                              style: TextStyle(
                                  color: appThemeColors?.primary,
                                  fontSize: 20.sp)),
                        )),
                    Expanded(
                        flex: 1,
                        child: TextButton(
                          onPressed: () =>
                              AppNavigator.toNamed(lotteryDateViewPath),
                          child: Text("开奖日期",
                              style: TextStyle(
                                  color: Colors.black, fontSize: 20.sp)),
                        ))
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("${controller.curYear.value}年历史开奖记录",
                        style: TextStyle(
                            fontSize: 20.sp, color: AppColors.ff252525)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              if (controller.sortMode.value == '降序') {
                                controller.sortMode.value = '升序';
                              } else {
                                controller.sortMode.value = '降序';
                              }
                              controller.reSort();
                            },
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size.zero,
                              backgroundColor: AppColors.ffffffff,
                              foregroundColor: Colors.black,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15.w, vertical: 8.h),
                            ),
                            child: Text(controller.sortMode.value,
                                style: TextStyle(fontSize: 18.sp))),
                        Gap(10.w),
                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15.w, vertical: 8.h),
                            decoration: BoxDecoration(
                              color: appThemeColors?.primary,
                              borderRadius: BorderRadius.circular(20.w),
                            ),
                            child: Text("五行",
                                style: TextStyle(
                                    fontSize: 18.sp, color: Colors.white))),
                        Gap(10.w),
                        ElevatedButton(
                            onPressed: _showPopup,
                            style: ElevatedButton.styleFrom(
                              minimumSize: Size.zero,
                              backgroundColor: AppColors.ffffffff,
                              foregroundColor: Colors.black,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15.w, vertical: 8.h),
                            ),
                            child: Text(controller.sortCode.value,
                                style: TextStyle(fontSize: 18.sp))),
                      ],
                    )
                  ],
                ),
                if (controller.lotteryHistory.value == null ||
                    (controller.lotteryHistory.value != null &&
                        controller.lotteryHistory.value!.list.isEmpty))
                  Center(child: AppImage.asset("pl.png")),
                if (controller.lotteryHistory.value != null &&
                    controller.lotteryHistory.value!.list.isNotEmpty)
                  Expanded(
                      child: ListView.builder(
                          itemCount:
                              controller.lotteryHistory.value?.list.length,
                          itemBuilder: (BuildContext context, int index) {
                            LotteryHistoryItemModel item =
                                controller.lotteryHistory.value!.list[index];
                            return _getLotteryHistoryItem(item);
                          })),
              ],
            ))));
  }

  Future<void> _showPopup() async {
    List<String> buttons = ['默认', '平码升序', '平码降序'];
    final result = await showPopupCard<String>(
      context: context,
      builder: (context) {
        return PopupCard(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.w),
            ),
            color: Colors.white,
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 5.h),
                constraints: BoxConstraints(maxHeight: 200.h, maxWidth: 100.w),
                height: 130.h,
                child: ListView.separated(
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                          onTap: () =>
                              Navigator.of(context).pop(buttons[index]),
                          child: Text(
                            buttons[index],
                            style: TextStyle(fontSize: 18.sp),
                            textAlign: TextAlign.center,
                          ));
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return const Divider();
                    },
                    itemCount: 3)));
      },
      offset: const Offset(-20, 145),
      alignment: Alignment.topRight,
      useSafeArea: true,
    );
    if (result != null) {
      controller.sortCode.value = result;
    }
  }

  Widget _getLotteryHistoryItem(LotteryHistoryItemModel item) {
    if (item.num != null && item.num!.isNotEmpty) {
      List<String>? nums = item.num?.split(",");
      return Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
                "第${(item.displayNumber ?? "").replaceFirst(controller.curYear.value.toString(), "")}期最新开奖结果",
                style: TextStyle(fontSize: 18.sp)),
            Text(DateUtil.dateWithCN(item.openTime ?? ""),
                style: TextStyle(color: AppColors.ff666666, fontSize: 16.sp))
          ],
        ),
        Container(
            padding: EdgeInsets.all(10.w),
            margin: EdgeInsets.only(bottom: 10.h),
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.drawMenu, width: 1.w),
                borderRadius: BorderRadius.circular(10.w)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ...List.generate(nums!.length - 1, (index) {
                  return _getBall(nums[index]);
                }),
                Text("+",
                    style: TextStyle(
                        fontSize: 30.sp, fontWeight: FontWeight.bold)),
                _getBall(nums[nums.length - 1])
              ],
            ))
      ]);
    } else {
      return Container();
    }
  }

  Widget _getBall(String num) {
    String icon = "";
    num = int.parse(num).toString();
    if (controller.lotteryHistory.value!.redBalls.contains(num)) {
      icon = "red.png";
    }
    if (controller.lotteryHistory.value!.greenBalls.contains(num)) {
      icon = "green.png";
    }
    if (controller.lotteryHistory.value!.blueBalls.contains(num)) {
      icon = "blue.png";
    }
    return Container(
        padding: EdgeInsets.only(top: 5.h, right: 3.w),
        width: 50.w,
        height: 50.w,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: ExactAssetImage("assets/images/ball/$icon"),
          fit: BoxFit.fill,
        )),
        child: Text(
          num,
          style: TextStyle(fontSize: 25.sp, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ));
  }

  void showBottomSheet() {
    DateTime selectedDate = DateTime(controller.curYear.value);
    Get.bottomSheet(
        CustomScrollDatePicker(
          selectedDate: selectedDate,
          onOkHandle: (value) {
            controller.curYear.value = int.parse(value);
            controller.reSort();
          },
        ),
        isDismissible: false);
  }
}
