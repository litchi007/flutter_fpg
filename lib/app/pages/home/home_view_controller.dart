import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/BannersData.dart';
import 'package:fpg_flutter/data/models/HallGameModel.dart';
import 'package:fpg_flutter/data/models/HomeAdsModel.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/data/models/NoticeMode.dart';
import 'package:fpg_flutter/data/models/RankUserModel.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:fpg_flutter/data/repositories/app_real_repository.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/common_util.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';

class HomeViewController extends GetxController {
  RxBool isIniting = false.obs;
  RxInt selectedTabIndex1 = 0.obs;
  RxInt selectedTabIndex2 = 0.obs;
  // List<String> banners = [];
  String notice = '';
  final AuthController _authController = Get.find();
  final AppSystemRepository _appSystemRepository = AppSystemRepository();
  final AppGameRepository _appGameRepository = AppGameRepository();
  final AppRealRepository _appRealRepository = AppRealRepository();
  RxList<SliderBanner> banners = RxList();
  RxList<HomeAdsModel> homeAdsList = RxList();
  RxList<HallGameData> lotteryGameList = RxList();
  Rx<HomeNoticeData> homeNoticeData = HomeNoticeData().obs;
  RxList<IconModel> iconList = RxList();
  RxList<NavModel> navList = RxList();
  RxList<RankUserModel> rankList = RxList();
  RxString infoBalance = ''.obs;

  RxString appLogo = ''.obs;
  final refresController = RefreshController();
  @override
  void onReady() async {
    initController();
    super.onReady();
  }

  handleRefresh() {
    initController(false);
  }

  void refreshUserInfo() {
    GlobalService.to.getUserInfo();
    infoBalance.value =
        (convertToDouble(GlobalService.to.userInfo.value?.balance ?? '') ?? 0)
            .toStringAsFixed(4);
  }

  void initController([bool status = true]) async {
    isIniting(true);
    appLogo.value = AppDefine.systemConfig?.mobileLogo ?? '';
    _appSystemRepository.homeAds().then((data) {
      homeAdsList.value = data ?? [];
    });

    _appSystemRepository.banners().then((data) {
      banners.value = data?.list ?? [];
    });

    _appGameRepository.homeGames().then((data) {
      AppDefine.homeGamesIcon = data?.icons;
      navList.value = data?.navs ?? [];
      iconList.value = data?.icons ?? [];
      refresController.refreshCompleted();
    });
    if (status) {
      _appSystemRepository.notices().then((data) {
        homeNoticeData.value = data ?? HomeNoticeData();
      });
    }

    // _appGameRepository.lotteryGames().then((data) {
    //   lotteryGameList.value = data ?? [];
    // });
    _appSystemRepository.getRankList().then((data) {
      rankList.value = data ?? [];
    });

    isIniting(false);
  }

  Future<NetBaseEntity<String?>?> manualTransfer(String toId, double money) {
    String token = GlobalService.to.userInfo.value!.token ?? "";
    return _appRealRepository.apiRealManualTransfer(token, money, "0", toId);
  }

  Future<NetBaseEntity<String?>?> gotoGame(String id, String game) async {
    String token = GlobalService.to.userInfo.value!.token ?? "";
    NetBaseEntity<String?>? data =
        await _appRealRepository.gotoGame(id: id, game: game, token: token);
    return data;
  }
}
