class LocalForumsTab {
  final String id;
  final String name;
  final String alias;
  final String label;
  final int pos;

  LocalForumsTab(
      {required this.id,
      required this.name,
      required this.alias,
      required this.label,
      required this.pos});
}
