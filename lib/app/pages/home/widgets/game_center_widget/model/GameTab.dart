class GameTab {
  final int id;
  final String name;
  final String image;

  GameTab({
    required this.id,
    required this.name,
    required this.image,
  });
}
