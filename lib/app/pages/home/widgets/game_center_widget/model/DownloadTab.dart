class DownloadTab {
  final String link;
  final String name;
  final String image;

  DownloadTab({
    required this.link,
    required this.name,
    required this.image,
  });
}
