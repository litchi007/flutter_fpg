import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/live_lottery/live_lottery_controller.dart';
import 'package:fpg_flutter/app/component/live_lottery/live_lottery_widget.dart';
import 'package:fpg_flutter/app/pages/comunity/widget/lhcdoc_content_list_widget.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/forum_game_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/model/GameTab.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_transfer_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/new_block_widget/new_block_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/favcor.dart';
import 'package:fpg_flutter/data/models/BannersData.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class GameCenterWidget extends StatefulWidget {
  const GameCenterWidget({super.key});

  @override
  State<StatefulWidget> createState() => _GameCenterWidgetState();
}

class _GameCenterWidgetState extends State<GameCenterWidget> {
  final GameCenterController controller = Get.find();
  final LiveLotteryController lcontroller = Get.find();
  final NewsBlockController newsBlockController = Get.find();
  final HomeViewController homeController = Get.put(HomeViewController());

  final bool isF062 = AppDefine.siteId?.contains('f062') ?? false;

  final List<GameTab> gameTabs = [
    GameTab(
        id: 0, name: '热门资讯', image: img_mobileTemplate('38', 'login_tab_1')),
    GameTab(
        id: 1, name: '游戏大厅', image: img_mobileTemplate('38', 'login_tab_2')),
  ];

  // final List<LocalForumsTab> localForumsTabs = [
  //   LocalForumsTab(
  //       id: '29', name: 'ugamlhc1', alias: 'APv57eA1', label: '澳门六合彩', pos: 1),
  //   LocalForumsTab(
  //       id: '30', name: 'lhc', alias: 'z6ZnuSaA', label: '香港六合彩', pos: 2),
  //   LocalForumsTab(
  //       id: '31', name: 'kl8lhc', alias: 'hd9dMzNx', label: '快乐8六合彩', pos: 3),
  // ];

  RxList<TabItem> lotteryTabs = [
    TabItem(
      label: '澳门六合彩',
      name: 'ugamlhc1',
      alias: 'APv57eA1',
    ),
    TabItem(label: '香港六合彩', name: 'lhc', alias: 'z6ZnuSaA'),
    // TabItem(label: '澳门十点半', name: 'lhc', alias: 'kOoJ93bj'),
  ].obs;

  @override
  void initState() {
    if (Favcor.currentName == 'f036c') {
      lotteryTabs.removeAt(2);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Obx(
      () => Column(
        children: [
          const ForumGameWidget(),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10.w),
            width: double.infinity,
            child: _LoginTab(mainColor: appThemeColors?.primary),
          ),
          SizedBox(
            height: 1.w,
          ),
          controller.gameTabIndex.value == 0
              ? Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.w),
                  color: Colors.white,
                  child: Column(children: [
                    const LiveLotteryWidget(),
                    SizedBox(
                      height: 10.h,
                    ),
                    if (AppConfig.hideTopBanner()) 
                      Obx(() {
                        if (homeController.homeAdsList.isNotEmpty) {
                          return CarouselSlider(
                            options: CarouselOptions(
                                height: 150.w,
                                viewportFraction: 1.0,
                                enlargeCenterPage: false,
                                autoPlay: homeController.homeAdsList.length > 1,
                                padEnds: false),
                            items: homeController.homeAdsList
                                .map(
                                  (item) => Container(
                                    margin: const EdgeInsets.all(5.0),
                                    child: ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(5.0)),
                                      child: Stack(
                                        children: <Widget>[
                                          InkResponse(
                                            onTap: () {
                                              SliderBanner s = SliderBanner(
                                                  linkCategory: item.linkCategory,
                                                  linkPosition: item.linkPosition,
                                                  pic: item.image,
                                                  realIsPopup: item.realIsPopup,
                                                  realSupportTrial:
                                                      item.realSupportTrial,
                                                  url: item.linkCustom,
                                                  linkAction: item.linkAction);
                                              onPressBanner(s);
                                            },
                                            child: AppImage.network(
                                                item.image ?? "",
                                                fit: BoxFit.fill,
                                                width: 1000.0),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                          );
                        }
                        return SizedBox();
                      }),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ...List.generate(lotteryTabs.length, (index) {
                          final TabItem item = lotteryTabs[index];
                          return Expanded(
                              flex: 1,
                              child: ElevatedButton(
                                  onPressed: () {
                                    controller.lotteryTabIndex2.value = index;
                                    int idx = lcontroller.lotteryTabs
                                        .indexWhere(
                                            (e) => e.label == item.name);
                                    if (idx > -1) {
                                      controller.lotteryTabIndex1.value = idx;
                                      final lotteryTab =
                                          lcontroller.lotteryTabs[idx];
                                      lcontroller.requestLHN(
                                          lotteryTab.gameId ?? '0',
                                          lotteryTab.name ?? '');
                                    }
                                    controller.getContentList(item.alias, 1);
                                  },
                                  style: AppButtonStyles.elevatedOnlyTopRadius(
                                      backgroundColor:
                                          controller.lotteryTabIndex2.value ==
                                                  index
                                              ? AppColors.drawMenu
                                              : Colors.white,
                                      foregroundColor:
                                          controller.lotteryTabIndex2.value ==
                                                  index
                                              ? Colors.white
                                              : Colors.black,
                                      fontSize: 20),
                                  child: Text(item.label ?? '')));
                        })
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.h), // Adjust the vertical padding here
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              int idx = controller.lotteryTabIndex2.value;
                              final lotteryTab = controller.lotteryTabs[idx];
                              AppNavigator.toNamed(newPostPath, arguments: {
                                'alias': lotteryTab.alias,
                              });
                            },
                            child: Text(
                              '立即发帖',
                              style: TextStyle(
                                fontSize: 24.sp,
                                color: const Color(0xffbfa46d),
                              ),
                            ),
                          ),
                          SizedBox(width: 8.w), // 添加一些间距
                          AppImage.network(img_mobileTemplate('38', 'send'),
                              width: 18.w, height: 18.h, fit: BoxFit.contain)
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (controller.lhcdocContent.value.list == null ||
                            controller.lhcdocContent.value.list!.isEmpty)
                          Text('本专场还没人发帖哟~',
                              style: TextStyle(
                                  fontSize: 20.sp,
                                  color: const Color(0xff21b2f2))),
                        if (controller.lhcdocContent.value.list != null)
                          ...List.generate(
                              controller.lhcdocContent.value.list!.length,
                              (index) {
                            final item =
                                controller.lhcdocContent.value.list![index];
                            return LhcdocContentListWidget(
                                item: item, index: index);
                          }),
                        // if (!AppConfig.isHidePaging())
                        getPagination(),
                      ],
                    ),
                    SizedBox(height: 10.h)
                  ]))
              : const ForumGameWidget(),
          SizedBox(
            height: 10.h,
          ),
        ],
      ),
    );
  }

  void onPressBanner(SliderBanner item) {
    LogUtil.w(item.toJson());

    if (["2", "3", "4", "5", "6", "7", "8", "15"].contains(item.linkCategory)) {
      GameModel game = GameModel(
        seriesId: item.linkCategory,
        subId: item.linkPosition,
        url: item.url,
        gameId: item.linkPosition,
        supportTrial: item.realSupportTrial,
        isPopup: item.realIsPopup,
      );

      /// 若是试玩账号 且 游戏不支持试玩, 则弹框
      if (GlobalService.isTest && item.realSupportTrial == 0) {
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => AppCommonDialog(
                onOk: () => Get.toNamed(loginPath),
                okLabel: '马上登录',
                title: '该游戏暂未开启试玩',
                msg: '请登录正式账号',
                cancelLabel: '取消'));
      } else {
        gotoGame(game);
      }
    } else if (item.linkCategory == "9") {
      if (AppDefine.systemConfig?.chatRoomSwitch == false) {
        AppToast.showToast('聊天室未开启');
        return;
      }
      AppNavigator.toNamed(chatRoomListPath);
    } else if (item.linkAction != null) {
      launchUrl(Uri.parse(item.url ?? ""));
    }
  }

  void gotoGame(GameModel gameItem) {
    if (gameItem.isPopup == null) {
      Get.toNamed('${AppRoutes.lottery}/${gameItem.gameId}');
      return;
    }
    if (gameItem.seriesId == '7' &&
        gameItem.subId == '70' &&
        gameItem.url != null) {
      launchUrl(Uri.parse(gameItem.url ?? ""));
      return;
    }
    if (gameItem.isPopup == 0) {
      if (!GlobalService.to.isAuthenticated.value) {
        AppNavigator.toNamed(loginPath);
        return;
      }
      if (AppDefine.systemConfig!.switchAutoTransfer == false) {
        GameTransfer(
                context: context,
                gameItem: GameModel(
                    gameId: gameItem.id,
                    gameCode: gameItem.gameCode,
                    name: gameItem.name))
            .showTransfer();
      } else {
        GameTransfer(
                context: context,
                gameItem: GameModel(
                    gameId: gameItem.id,
                    gameCode: gameItem.gameCode,
                    name: gameItem.name))
            .gotoGame();
      }
    } else {
      String id = "";
      if (gameItem.gameId != null) {
        id = gameItem.gameId.toString();
      } else {
        id = gameItem.id ?? "";
      }
      AppNavigator.toNamed(realGameTypesPath, arguments: [id, gameItem.name]);
    }
  }

  Widget _LoginTab({Color? mainColor}) {
    return Row(
      children: [
        ...List.generate(gameTabs.length, (index) {
          final gameTab = gameTabs[index];
          return Expanded(
            child: GestureDetector(
              onTap: () {
                controller.gameTabIndex.value = gameTab.id;
              },
              child: Obx(
                () => Container(
                  margin: EdgeInsets.symmetric(horizontal: 1.w),
                  padding: EdgeInsets.symmetric(vertical: 10.w),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.w),
                      topRight: Radius.circular(10.w),
                    ),
                    color: gameTab.id == controller.gameTabIndex.value
                        ? mainColor
                        : const Color(0xffb8b8b8),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AppImage.network(gameTab.image,
                          width: 30.w, height: 30.w),
                      Text(
                        gameTab.name,
                        style: TextStyle(color: Colors.white, fontSize: 20.sp),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ],
    );
  }

  Widget getPagination() {
    int totalPages = 0;
    int total = controller.lhcdocContent.value.total ?? 0;
    if (total > controller.perPage) {
      int d = controller.lhcdocContent.value.total! % controller.perPage;
      totalPages = total ~/ controller.perPage;
      if (totalPages > 0 && d > 0) {
        totalPages += 1;
      }
    }
    List<int> pages = [];
    for (int i = 0; i < totalPages; i++) {
      pages.add(i + 1);
    }
    if (pages.isEmpty || AppConfig.isHidePaging()) {
      return const SizedBox();
    } else {
      return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        ...List.generate(pages.length, (index) {
          if (pages[index] == controller.curPage.value) {
            return Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(4.w),
                margin: EdgeInsets.symmetric(horizontal: 4.w),
                decoration: BoxDecoration(
                    color: AppColors.drawMenu, border: Border.all(width: 1)),
                child: Text(pages[index].toString(),
                    style: TextStyle(color: Colors.white, fontSize: 18.sp)));
          } else {
            if (newsBlockController.lotteryForumTabs.length <=
                controller.lotteryTabIndex2.value) return const SizedBox();
            TabItem tab = newsBlockController
                .lotteryForumTabs[controller.lotteryTabIndex2.value];

            return GestureDetector(
                onTap: () => controller.getContentList(tab.alias, pages[index]),
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(4.w),
                    margin: EdgeInsets.symmetric(horizontal: 4.w),
                    decoration: BoxDecoration(
                        color: Colors.white, border: Border.all(width: 1)),
                    child: Text(pages[index].toString(),
                        style:
                            TextStyle(color: Colors.black, fontSize: 18.sp))));
          }
        })
      ]);
    }
  }
}
