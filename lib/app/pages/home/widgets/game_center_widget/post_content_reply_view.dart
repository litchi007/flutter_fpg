import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lhcdoc_content_detail/post_detail_controller.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';
import 'package:flutter/foundation.dart' as foundation;

class PostContentReplyView extends StatefulWidget {
  const PostContentReplyView({super.key});

  @override
  State<PostContentReplyView> createState() => _PostContentReplyViewState();
}

class _PostContentReplyViewState extends State<PostContentReplyView> {
  // GameCenterController controller = Get.find();
  final PostDetailController controller = Get.find();
  final _textEditingController = TextEditingController();
  final _scrollController = ScrollController();
  final RxBool _showOffstage = false.obs;

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      backgroundColor: appThemeColors?.surface,
      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.primary,
        leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Container(
              padding: EdgeInsets.only(left: 10.w),
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.ffffffff, size: 18.w),
                Text('取消',
                    style: AppTextStyles.ffffffff(context, fontSize: 18)),
              ]),
            )),
      ),
      body: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: [
          Column(
            children: [
              Stack(
                children: [
                  Container(
                      width: Get.width,
                      height: Get.height * 0.4,
                      decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: Colors.grey, width: 1.h)),
                      ),
                      child: TextField(
                        expands: true,
                        controller: _textEditingController,
                        scrollController: _scrollController,
                        style: const TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                        maxLines: null,
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 10.w, right: 10.w, bottom: 20.h),
                            border: InputBorder.none,
                            hintText: "留下您的评论..."),
                      )),
                  Positioned(
                    bottom: 10.h,
                    right: 10.w,
                    child: IconButton(
                        onPressed: () =>
                            _showOffstage.value = !_showOffstage.value,
                        icon: const Icon(
                          Icons.emoji_emotions,
                          color: AppColors.drawMenu,
                        )),
                  )
                ],
              ),
              SizedBox(height: 20.h),
              ElevatedButton(
                onPressed: () {
                  String content = _textEditingController.text;

                  String cid = controller.postDetail.value.id ?? "";
                  String toUid = controller.postDetail.value.uid ?? "";
                  controller
                      .postContentReply(cid, "", toUid, content)
                      .then((result) {
                    if (result) {
                      // controller.getContentDetail();
                      Navigator.of(context).pop();
                    }
                  });
                },
                style: AppButtonStyles.elevatedStyle(
                    backgroundColor: AppColors.drawMenu,
                    foregroundColor: Colors.white,
                    fontSize: 20,
                    width: 400,
                    height: 30),
                child: const Text("提交"),
              ),

              // Offstage(
              //   offstage: !_showOffstage.value,
              //   child: EmojiPicker(
              //     textEditingController: _textEditingController,
              //     scrollController: _scrollController,
              //     config: Config(
              //       height: 300.h,
              //       checkPlatformCompatibility: true,
              //       emojiViewConfig: EmojiViewConfig(
              //         // Issue: https://github.com/flutter/flutter/issues/28894
              //         emojiSizeMax: 28 *
              //             (foundation.defaultTargetPlatform ==
              //                     TargetPlatform.iOS
              //                 ? 1.2
              //                 : 1.0),
              //       ),
              //       swapCategoryAndBottomBar: true,
              //       skinToneConfig: const SkinToneConfig(),
              //       categoryViewConfig: const CategoryViewConfig(),
              //       bottomActionBarConfig: const BottomActionBarConfig(
              //           showBackspaceButton: false),
              //       searchViewConfig: const SearchViewConfig(),
              //     ),
              //   ),
              // )
            ],
          ),
          Obx(
            () => _showOffstage.value
                ? Container(
                    margin: EdgeInsets.all(8.0),
                    padding: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black38)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Wrap(
                            spacing: 10.w,
                            runSpacing: 10.w,
                            children: [
                              ...List.generate(104, (index) {
                                return GestureDetector(
                                  onTap: () {
                                    _textEditingController.text +=
                                        '[em_${index + 1}]';
                                    _showOffstage.value = false;
                                  },
                                  child: AppImage.network(
                                    'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz/images/arclist/${index + 1}.gif',
                                    width: 28.w,
                                    height: 28.h,
                                  ),
                                );
                              }),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                : const SizedBox(),
          ),
        ],
      ),
    );
  }
}
