import 'package:fpg_flutter/app/component/live_lottery/live_lottery_controller.dart';
import 'package:fpg_flutter/data/models/LhcdocContent.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';

class GameCenterController extends GetxController {
  // 預設關閉眯牌

  RxInt downloadTabIndex = 0.obs;
  RxInt gameTabIndex = 0.obs;
  RxInt lotteryTabIndex1 = 0.obs;
  RxInt lotteryTabIndex2 = 0.obs;
  Rx<LhcdocContent> lhcdocContent = LhcdocContent().obs;

  RxInt curPage = 1.obs;
  int perPage = 26;
  RxBool isLoading = false.obs;

  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  final LiveLotteryController _liveLotteryController =
      Get.put(LiveLotteryController());

  RxList<TabItem> lotteryTabs = [
    TabItem(
      label: '澳门六合彩',
      name: 'ugamlhc1',
      alias: 'APv57eA1',
    ),
    TabItem(label: '香港六合彩', name: 'lhc', alias: 'z6ZnuSaA'),
    // TabItem(
    //   label: '新澳门六合彩',
    //   name: 'ugamlhc2',
    // ),
  ].obs;

  @override
  void onReady() {}

  Future getContentList(String? alias, int page) async {
    curPage.value = page;
    if (alias != null) {
      await _appLHCDocRepository
          .getLhcContentList(
        alias: alias,
        page: page,
        rows: perPage,
      )
          .then((data) {
        if (data != null) {
          lhcdocContent.value = data;
        }
      });
    }
  }
}
