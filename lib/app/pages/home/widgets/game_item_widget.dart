import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/comunity/community_page.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_transfer_widget.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/components/custom.btn.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

// 识别 H5路由跳转到原生页面
// static h5Router = [
//   {
//     path: '/mobile/#/ucenter/index',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.我的页),
//   },
//   {
//     path: '/mobile/#/ucenter/myinfo',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.个人信息),
//   },
//   {
//     path: '/mobile/#/ucenter/myfpwd',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.安全中心),
//   },
//   {
//     path: '/mobile/#/ucenter/message',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.站内信),
//   },
//   {
//     path: '/mobile/#/ucenter/feedback',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.建议反馈),
//   },
//   {
//     path: '/mobile/#/ucenter/myreco',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.推荐收益),
//   },
//   {
//     path: '/mobile/#/ucenter/yuebao',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.利息宝),
//   },
//   {
//     path: '/mobile/#/ucenter/yuebao/in',
//     onPress: () => push(PageName.NewAlipayTransInView), // 利息宝转入转出
//   },
//   {
//     path: '/mobile/#/ucenter/yuebao/cash',
//     onPress: () => push(PageName.NewAlipayTransferRecordView), // 利息宝转入转出记录
//   },
//   {
//     path: '/mobile/#/ucenter/yuebao/settle',
//     onPress: () => push(PageName.AlipayProfitView), // 利息宝收益报表
//   },
//   {
//     path: '/mobile/#/bank/deposit',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.存款),
//   },
//   {
//     path: '/mobile/#/bank/bank',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.银行卡管理),
//   },
//   {
//     path: '/mobile/#/bank/realtrans',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.额度转换),
//   },
//   {
//     path: '/mobile/#/ucenter/task',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.任务中心), // 任务大厅
//   },
//   {
//     path: '/mobile/#/ucenter/taskExChange',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.任务中心), // 任务中心-积分兑换
//   },
//   {
//     path: '/mobile/#/ucenter/taskChange',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.任务中心), // 任务中心-积分账变
//   },
//   {
//     path: '/mobile/#/ucenter/taskLevel',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.任务中心), // 任务中心-等级
//   },
//   {
//     path: '/mobile/#/promote',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.优惠活动),
//   },
//   {
//     path: '/mobile/#/ucenter/promote',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.活动彩金),
//   },
//   {
//     path: '/mobile/#/lottery/index/70?id=1',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.聊天室),
//   },
//   {
//     path: '/mobile/#/chat/chatList',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.聊天室), // 聊天室列表
//   },
//   {
//     path: '/mobile/#/ucenter/guessing',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.全民竞猜),
//   },
//   {
//     path: '/mobile/#/ucenter/guessing/myGuess',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.全民竞猜), // 全民竞猜-我的竞猜
//   },
//   {
//     path: '/mobile/#/lottery/list',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.彩票大厅),
//   },
//   {
//     path: '/mobile/#/ucenter/lottoryTrend',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.开奖走势),
//   },
//   {
//     path: '/mobile/#/lottery/changLongBet',
//     onPress: () => PushHelper.pushUserCenterType(UGUserCenterType.长龙助手),
//   },
//   {
//     path: '/mobile/#/lottery/settled',
//     onPress: () =>
//       PushHelper.pushLinkPositionType(UGLinkPositionType.今日输赢),
//   },
//   {
//     path: '/mobile/#/lottery/week',
//     onPress: () =>
//       PushHelper.pushUserCenterType(UGUserCenterType.彩票注单记录),
//   },
//   {
//     path: '/mobile/#/lottery/realBetReal',
//     onPress: () =>
//       PushHelper.pushUserCenterType(UGUserCenterType.其他注单记录), // 真人注单
//   },
//   {
//     path: '/mobile/#/lottery/realBetFish',
//     onPress: () =>
//       PushHelper.pushUserCenterType(UGUserCenterType.其他注单记录), // 捕鱼注单
//   },
//   {
//     path: '/mobile/#/lottery/realBetEsport',
//     onPress: () =>
//       PushHelper.pushUserCenterType(UGUserCenterType.其他注单记录), // 电竞注单
//   },
//   {
//     path: '/mobile/#/lottery/notcount',
//     onPress: () => {}, // 未结注单
//   },
//   {
//     path: '/mobile/#/lottery/ugGameList',
//     onPress: () => {}, // UG注单
//   },
//   {
//     path: '/mobile/#/lottery/mystery',
//     onPress: params => push(PageName.PostListPage, params),
//   },
//   {
//     path: '/mobile/#/lottery/rule',
//     onPress: params => push(PageName.PostListPage, params),
//   },
//   {
//     path: '/mobile/#/lottery/forum',
//     onPress: params => push(PageName.PostListPage, params),
//   },
//   {
//     path: '/mobile/#/lottery/sixpic',
//     onPress: params => push(PageName.LHGalleryPage, params),
//   },
//   {
//     path: '/mobile/#/lottery/changLongBet',
//     onPress: () => push(PageName.LongDragonView),
//   },
//   {
//     path: '/Open_prize/index.php',
//     onPress: () =>
//       push(PageName.WebPage, {
//         url: AppDefine.host + '/Open_prize/index.php',
//       }),
//   },
//   {
//     path: '/mobile/#/tiaoma/index',
//     onPress: () => push(PageName.LHlittleHelperPage),
//   },
//   {
//     path: '/mobile/#/lottery/rundog',
//     onPress: params => push(PageName.PostDetailPage, params),
//   },
//   {
//     path: '/mobile/#/lottery/humorGuess',
//     onPress: params => push(PageName.PostDetailPage, params),
//   },
//   {
//     path: '/mobile/#/lottery/fourUnlike',
//     onPress: params => push(PageName.PostDetailPage, params),
//   },
//   {
//     path: 'mobile/#/bank/okp_online',
//     onPress: params => push(PageName.OnlinePayPage, params),
//   },
// ];

class GameItemWidget extends StatelessWidget {
  final GameModel gameItem;
  final AuthController authController = Get.find();
  final HomeViewController controller = Get.find();

  GameItemWidget({super.key, required this.gameItem});
  final model = [
    // 我的页
    UrlPushModel(path: '/mobile/#/ucenter/index', type: "me", url: "/user"),
    // 个人信息
    UrlPushModel(path: '/mobile/#/ucenter/myinfo', url: "/myinfo"),

    /// 安全中心
    UrlPushModel(path: '/mobile/#/ucenter/myfpwd', url: '/safe_center'),
    // 站内信
    UrlPushModel(path: '/mobile/#/ucenter/message', url: '/message'),
    // 建议反馈
    UrlPushModel(path: '/mobile/#/ucenter/feedback', url: '/feedback'),
    // 推荐收益
    UrlPushModel(
      path: '/mobile/#/ucenter/myreco',
    ),
    // 利息宝
    UrlPushModel(
      path: '/mobile/#/ucenter/yuebao',
      url: '/interestTreasure',
    ),
    // 利息宝转入转出
    UrlPushModel(path: '/mobile/#/ucenter/yuebao/in', url: '/newalipayTransIn'),

    //  利息宝转入转出记录
    UrlPushModel(
        path: '/mobile/#/ucenter/yuebao/cash', url: '/alipayTransferRecord'),
    //  利息宝收益报表
    UrlPushModel(path: '/mobile/#/ucenter/yuebao/settle', url: '/alipayprofit'),
    // 存款
    UrlPushModel(path: '/mobile/#/bank/deposit', url: '/bank'),
    // 银行卡管理
    UrlPushModel(path: '/mobile/#/bank/bank', url: '/bank/manageBankAccounts'),
    // 额度转换
    UrlPushModel(path: '/mobile/#/bank/realtrans', url: '/bank/realtrans'),
    // .任务中心), // 任务大厅
    UrlPushModel(path: '/mobile/#/ucenter/task', url: '/task'),
    // 任务中心), // 任务中心-积分兑换
    UrlPushModel(path: '/mobile/#/ucenter/taskExChange', url: '/task?tab=2'),
    // 任务中心), // 任务中心-积分账变
    UrlPushModel(path: '/mobile/#/ucenter/taskChange', url: '/task'),
    // 任务中心), // 任务中心-等级
    UrlPushModel(path: '/mobile/#/ucenter/taskLevel', url: '/task'),
    // 优惠活动
    UrlPushModel(path: '/mobile/#/promote', url: '/activity'),
    // 活动彩金
    UrlPushModel(
      path: '/mobile/#/ucenter/promote',
    ),
    // 聊天室
    UrlPushModel(path: '/mobile/#/lottery/index/70?id=1', url: '/chatRoomList'),
    // 全民竞猜
    UrlPushModel(path: '/mobile/#/ucenter/guessing', url: '/nationalQuiz'),
    // 全民竞猜
    UrlPushModel(
        path: '/mobile/#/ucenter/guessing/myGuess', url: '/nationalQuiz'),
    // 彩票大厅
    UrlPushModel(path: '/mobile/#/lottery/list', url: '/gameHall'),
    // 开奖走势
    UrlPushModel(
      path: '/mobile/#/ucenter/lottoryTrend',
    ),
    // 长龙助手
    UrlPushModel(
        path: '/mobile/#/lottery/changLongBet', url: '/longDragonTool'),
    // 今日输赢
    UrlPushModel(path: '/mobile/#/lottery/settled', url: '/todaySettled'),
    // 彩票注单记录
    UrlPushModel(path: '/mobile/#/lottery/week', url: '/lotteryTickets'),
    // 真人注单
    UrlPushModel(
      path: '/mobile/#/lottery/realBetReal',
    ),
    // 捕鱼注单
    UrlPushModel(
      path: '/mobile/#/lottery/realBetFish',
    ),
    // 电竞注单
    UrlPushModel(
      path: '/mobile/#/lottery/realBetEsport',
    ),
    // 未结注单
    UrlPushModel(path: '/mobile/#/lottery/notcount', url: '/lotteryLiveBet'),
    // UG注单
    UrlPushModel(
      path: '/mobile/#/lottery/ugGameList',
    ),
    // PostListPage
    UrlPushModel(
      path: '/mobile/#/lottery/mystery',
    ),
    // PostListPage
    UrlPushModel(
      path: '/mobile/#/lottery/rule',
    ),
    // PostListPage
    UrlPushModel(
      path: '/mobile/#/lottery/forum',
    ),
    // LHGalleryPage
    UrlPushModel(
      path: '/mobile/#/lottery/sixpic',
    ),
    // LongDragonView
    UrlPushModel(
      path: '/mobile/#/lottery/changLongBet',
    ),

    ///  url: AppDefine.host + '/Open_prize/index.php',
    UrlPushModel(path: '/Open_prize/index.php', type: "webview"),
    // LHlittleHelperPage
    UrlPushModel(
      path: '/mobile/#/tiaoma/index',
    ),
    // PostDetailPage
    UrlPushModel(
      path: '/mobile/#/lottery/rundog',
    ),
    // PostDetailPage
    UrlPushModel(
      path: '/mobile/#/lottery/humorGuess',
    ),
    // PostDetailPage
    UrlPushModel(
      path: '/mobile/#/lottery/fourUnlike',
    ),
    // OnlinePayPage
    UrlPushModel(
      path: '/mobile/#/bank/okp_online',
    ),
    UrlPushModel(path: '/mobile/#/lottery/forum?alias=rule', url: "/community"
        // url: "/postList"
        ),
  ].cast<UrlPushModel>();
  @override
  Widget build(BuildContext context) {
    return CustomButton(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.w),
        color: Colors.white, // Color(0xffB7CBDD),
      ),
      child: GestureDetector(
        onTap: () {
          if (gameItem.isMaintained == 1) {
            return;
          }
          // 图库，直接跳转图库
          final isGrally = gameItem.name?.contains("图库");
          final item = gameItem;
          // 图库
          if (((item.link as String?) ?? "").contains('/sixpic') ||
              item.name!.contains('图库')) {
            AppNavigator.toNamed(lhGalleryPath,
                arguments: [item.name, item.alias, item.id]);
            return;
          }
          LogUtil.w("图库， name: ${item.name} alias: ${item.alias} ");
          if (item.name == '美女社区' ||
              item.name == '澳门规律' ||
              item.name == '香港规律' ||
              item.name == '独家解谜语' ||
              item.name == '香港五点来料' ||
              item.name == '独家解密语' ||
              (item.link ?? "").contains('/mystery') ||
              item.alias == 'mystery' || // 香港规律
              item.alias == 'rule') {
            // 澳门规律
            //arguments: title, alias, type, category
            AppNavigator.toNamed(postListPath,
                arguments: [item.name, item.alias, 'mystery', '']);
            return;
          }

          if (item.name == '网红流照' ||
              item.name == '性感网红流照' ||
              item.name == '幽默猜测' ||
              item.name == '一句真言' ||
              item.name == '网红流照' ||
              item.name == '跑马团玄机') {
            //arguments: type2, title, category, cid, extra, from
            AppNavigator.toNamed(lhkPostDetail, arguments: [
              "",
              item.name ?? "",
              item.alias ?? "",
              item.id,
              'humorGuess',
              'gallery'
            ]);
            return;
          }
          if (item.name == '性感网红流照' ||
              item.alias == 'lQWcjMlA' ||
              item.alias == 'Qar5hNHG' ||
              item.alias == 'ZsD81C53' ||
              item.name == '澳门四不像' ||
              item.name == '澳门签牌' ||
              item.name == '澳门跑狗图' ||
              item.name == '秘典玄机' ||
              item.name == '澳门传真' ||
              item.name == '澳门高手榜' ||
              item.name == '118内幕图' ||
              item.name == '澳门AI资料' ||
              item.name == '澳门心水推荐' ||
              item.name == '香港心水推荐' ||
              item.name == '香港挂牌' ||
              item.name == '主持爆内幕' ||
              item.name == '123玄机图' ||
              item.name == '幽默视频猜测' ||
              item.name == '铁板神算' ||
              item.name == '四不像' ||
              item.name == '澳门挂牌') {
            //arguments: type2, title, category, cid, extra, from
            AppNavigator.toNamed(lhkPostDetail, arguments: [
              "",
              item.name ?? "",
              item.alias ?? "",
              "",
              'humorGuess',
              'humorGuess'
            ]);
            return;
          }
          if (item.alias == 'rwzx') {
            if (!GlobalService.to.isAuthenticated.value) {
              AppNavigator.toNamed(loginPath);
            } else {
              AppNavigator.toNamed(taskPath, arguments: [3]);
            }
            return;
          }

          LogUtil.w("url___${gameItem.link} $isGrally");
          final current = model.firstWhereOrNull((e) => e.path == gameItem.url);
          if (current != null) {
            if (current.url == "/community") {
              Get.to(CommunityPage());
            }
            if (current.url != null) {
              Get.toNamed(current.url!);
              return;
            }
          }
          if (gameItem.isPopup == null) {
            Get.toNamed('${AppRoutes.lottery}/${gameItem.gameId}');
            return;
          }
          // if (gameItem.isPopup == null) {
          //   Get.toNamed('${AppRoutes.lottery}/${gameItem.gameId}');
          //   return;
          // }
          if (gameItem.seriesId == '7' &&
              gameItem.subId == 70 &&
              gameItem.url != null) {
            launchUrl(Uri.parse(gameItem.url ?? ""));
            return;
          }

          // /// 若是试玩账号 不支持试玩,弹框
          if (GlobalService.isTest) {
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) => AppCommonDialog(
                    onOk: () => Get.toNamed(loginPath),
                    okLabel: '马上登录',
                    title: '该游戏暂未开启试玩',
                    msg: '请登录正式账号',
                    cancelLabel: '取消'));
            return;
          }

          if (gameItem.isPopup == 0) {
            print("gameItem ${GlobalService.to.isAuthenticated.value}");
            if (!GlobalService.to.isAuthenticated.value) {
              AppNavigator.toNamed(loginPath);
              return;
            }
            if (AppDefine.systemConfig!.switchAutoTransfer == false) {
              GameTransfer(context: context, gameItem: gameItem).showTransfer();
            } else {
              GameTransfer(context: context, gameItem: gameItem).gotoGame();
            }
          } else {
            String id = "";
            if (gameItem.gameId != null) {
              id = gameItem.gameId.toString();
            } else {
              id = gameItem.id ?? "";
            }
            AppNavigator.toNamed(realGameTypesPath,
                arguments: [id, gameItem.name]);
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              alignment: Alignment.center, // 中心对齐，确保文本位于图像中央
              children: [
                AppImage.network(
                  '${gameItem.icon}',
                  width: 80.w,
                  height: 80.w,
                ),
                if (gameItem.isMaintained == 1) // 判断条件
                  ...[
                  Container(
                    width: 80.w,
                    height: 80.w,
                    decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.5), // 添加50%透明色
                      borderRadius: BorderRadius.circular(40), // 圆角40
                    ),
                  ),
                  const Text(
                    '维护中',
                    style: TextStyle(
                      color: Colors.white, // 白色字体
                      fontSize: 16, // 字体大小
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ],
            ),
            SizedBox(width: 30.h),
            Text(gameItem.name!),
          ],
        ),
      ),
    );
  }
}

class UrlPushModel {
  String? url;
  String? path;
  String? type;
  UrlPushModel({this.url, this.path, this.type});

  UrlPushModel.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    path = json['path'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['url'] = url;
    data['path'] = path;
    data['type'] = type;
    return data;
  }
}
