import 'dart:async';
import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/data/models/NoticeMode.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class HomeNoticeWidget extends StatefulWidget {
  const HomeNoticeWidget({super.key});

  @override
  State<StatefulWidget> createState() => _HomeNoticeWidgetState();
}

class _HomeNoticeWidgetState extends State<HomeNoticeWidget> {
  HomeViewController controller = Get.find<HomeViewController>();
  final CarouselSliderController _controller = CarouselSliderController();
  final List<RxBool> _isShowHtml = [];
  final CarouselOptions _carouselOptions = CarouselOptions(
      height: 300.h,
      enlargeCenterPage: false,
      scrollDirection: Axis.vertical,
      viewportFraction: 0.15,
      scrollPhysics: const NeverScrollableScrollPhysics(),
      initialPage: 3,
      autoPlay: true,
      autoPlayInterval: const Duration(milliseconds: 200),
      pauseAutoPlayInFiniteScroll: false,
      pauseAutoPlayOnManualNavigate: false,
      autoPlayCurve: Curves.linear);

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.zero,
          side: BorderSide(color: Colors.grey, width: 5.w),
        ),
        child: SizedBox(
            height: 700.h,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    color: appThemeColors?.primary,
                    height: 50.h,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("平台公告",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 25.sp)),
                            ]),
                        Positioned(
                          top: 0,
                          right: 0,
                          width: 44,
                          height: 44,
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 30,
                          ).onTap(() {
                            Get.back();
                          }),
                        )
                      ],
                    )),
                Expanded(
                    child: SingleChildScrollView(
                  child: Obx(() => Column(
                        children: [
                          ...List.generate(
                              (controller.homeNoticeData.value.popup ?? [])
                                  .length, (index) {
                            Scroll item =
                                controller.homeNoticeData.value.popup![index];
                            _isShowHtml.add((index == 0).obs);
                            Widget widget1;
                            if (item.type == '3') {
                              var content = jsonDecode(item.content ?? "");
                              widget1 = Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 15.w),
                                  height: 300.h,
                                  child: Column(children: [
                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.h, horizontal: 5.w),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(5.w),
                                              topRight: Radius.circular(5.w)),
                                          color: AppColors.ff8D8B8B,
                                        ),
                                        child: _rankItemHeaderRow(
                                            "排名", "账号", "彩种", "单笔中奖")),
                                    Expanded(
                                        child: CarouselSlider(
                                      options: _carouselOptions,
                                      carouselController: _controller,
                                      items:
                                          List.generate(content.length, (idx) {
                                        return _rankItemRow(
                                            idx,
                                            (idx + 1).toString(),
                                            content[idx]["username"],
                                            content[idx]["betname"],
                                            content[idx]["bonus"]);
                                      }).toList(),
                                    ))
                                  ]));
                            } else {
                              widget1 = Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 15.w),
                                  child: HtmlWidget(item.content ?? ""));
                            }
                            return Column(
                              children: [
                                GestureDetector(
                                  onTap: () => collapseContent(index),
                                  behavior: HitTestBehavior.translucent,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 15.w),
                                        child: Row(children: [
                                          Icon(Icons.brightness_1_rounded,
                                              color: AppColors.ff1976D2,
                                              size: 20.w),
                                          Gap(10.w),
                                          Text(item.title ?? "",
                                              style: TextStyle(
                                                  fontSize: 25.sp,
                                                  fontWeight: FontWeight.bold))
                                        ]),
                                      ),
                                      IconButton(
                                          onPressed: () =>
                                              collapseContent(index),
                                          icon: Icon(
                                              _isShowHtml[index].value
                                                  ? Icons
                                                      .keyboard_arrow_up_rounded
                                                  : Icons
                                                      .keyboard_arrow_down_rounded,
                                              color: Colors.grey,
                                              size: 30.w))
                                    ],
                                  ),
                                ),
                                if (_isShowHtml[index].value) widget1,
                                const Divider()
                              ],
                            );
                          })
                        ],
                      )),
                )),
                // Container(
                //     alignment: Alignment.center,
                //     decoration: BoxDecoration(
                //         border: Border(
                //             top: BorderSide(color: Colors.grey, width: 1.w))),
                //     child: TextButton(
                //         onPressed: () => Navigator.of(context).pop(),
                //         child: Text("我知道了",
                //             style: TextStyle(
                //                 color: AppColors.textColor2,
                //                 fontSize: 20.sp,
                //                 fontWeight: FontWeight.bold))))
              ],
            )));
  }

  Widget _rankItemHeaderRow(
      String val1, String val2, String val3, String val4) {
    TextStyle style = TextStyle(color: Colors.white, fontSize: 18.sp);
    return Row(
      children: [
        _rankItemCol(val1, 50.w, style),
        Expanded(
          flex: 1,
          child: _rankItemCol(val2, 100.w, style),
        ),
        Expanded(
          flex: 1,
          child: _rankItemCol(val3, 100.w, style),
        ),
        _rankItemCol(val4, 100.w, style),
      ],
    );
  }

  Widget _rankItemRow(
      int index, String val1, String val2, String val3, String val4) {
    TextStyle style = TextStyle(color: AppColors.ff666666, fontSize: 16.sp);
    return Container(
        color: index % 2 == 0
            ? Colors.white
            : const Color.fromARGB(255, 230, 230, 230),
        child: Row(
          children: [
            _rankItemCol(val1, 50.w, style),
            Expanded(
              flex: 1,
              child: _rankItemCol(val2, 100.w, style),
            ),
            Expanded(
              flex: 1,
              child: _rankItemCol(val3, 100.w, style),
            ),
            _rankItemCol(val4, 100.w,
                TextStyle(color: AppColors.ffff0000, fontSize: 16.sp)),
          ],
        ));
  }

  Widget _rankItemCol(String val, double width, TextStyle style) {
    return Container(
        alignment: Alignment.center,
        width: width,
        child: Text(val, style: style));
  }

  void collapseContent(int index) {
    _isShowHtml[index].value = !_isShowHtml[index].value;
    for (int i = 0;
        i < (controller.homeNoticeData.value.popup ?? []).length;
        i++) {
      if (index != i) {
        _isShowHtml[i].value = false;
      }
    }
  }
}
