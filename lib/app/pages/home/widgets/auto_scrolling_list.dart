import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/LhcForum2Model.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

class AutoScrollingList extends StatefulWidget {
  final List<TopItem> items;
  final int interval;
  final int type;

  const AutoScrollingList({
    super.key,
    required this.items,
    required this.interval,
    required this.type,
  });

  @override
  State<StatefulWidget> createState() => _AutoScrollingListState();
}

class _AutoScrollingListState extends State<AutoScrollingList> {
  Timer? _timer;
  bool _startedTimer = false;
  final CarouselSliderController _controller = CarouselSliderController();
  final GameCenterController gameCenterController =
      Get.find<GameCenterController>();

  final CarouselOptions _carouselOptions = CarouselOptions(
    height: 40.h,
    enlargeCenterPage: false,
    reverse: true,
    scrollDirection: Axis.vertical,
    viewportFraction: 1,
    // scrollPhysics: const NeverScrollableScrollPhysics(),
  );

  @override
  void initState() {
    super.initState();
    // _controller = CarouselController();
    _startTimer();
  }

  @override
  void dispose() {
    _timer?.cancel();
    // _controller.dispose();
    super.dispose();
  }

  void _startTimer() {
    if (_startedTimer) return;
    _timer = Timer.periodic(Duration(seconds: widget.interval),
        (Timer timer) => _controller.previousPage());
    TimerManager().addTimer(_timer!);
    _startedTimer = true;
  }

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: _carouselOptions,
      carouselController: _controller,
      items: widget.items.map((item) {
        return GestureDetector(
          onTap: () {
            // gameCenterController.getContentDetail(item.cid ?? "");
            AppNavigator.toNamed(postDetailPath, arguments: {
              'postId': item.cid ?? "",
              'title': item.title,
              'category': 'forum',
              'from': 'forum'
            });
          },
          child: Text(
            '${item.short_lhcno} ${item.title}',
            overflow: TextOverflow.ellipsis,
          ),
        );
      }).toList(),
    );
  }
}
