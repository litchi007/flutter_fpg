import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:intl/intl.dart';
import 'package:scroll_date_picker/scroll_date_picker.dart';

class CustomScrollDatePicker extends StatefulWidget {
  CustomScrollDatePicker(
      {super.key, required this.selectedDate, this.onOkHandle});

  DateTime selectedDate;
  final ValueChanged<String>? onOkHandle;

  @override
  State<StatefulWidget> createState() => _CustomScrollDatePickerState();
}

class _CustomScrollDatePickerState extends State<CustomScrollDatePicker> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border:
                Border(top: BorderSide(color: AppColors.ff666666, width: 1.w))),
        height: 250.h,
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("取消",
                      style: TextStyle(
                          fontSize: 20.sp, color: AppColors.ff666666))),
              Text("选择年份",
                  style:
                      TextStyle(fontSize: 22.sp, fontWeight: FontWeight.bold)),
              TextButton(
                  onPressed: () {
                    widget.onOkHandle!(
                        DateFormat("yyyy").format(widget.selectedDate));
                    Navigator.of(context).pop();
                  },
                  child: Text("确定",
                      style: TextStyle(
                          fontSize: 20.sp, color: AppColors.ff666666)))
            ],
          ),
          SizedBox(
              height: 180.h,
              child: ScrollDatePicker(
                viewType: const [DatePickerViewType.year],
                selectedDate: widget.selectedDate,
                options: const DatePickerOptions(isLoop: false),
                onDateTimeChanged: (DateTime value) {
                  widget.selectedDate = value;
                },
              ))
        ]));
  }
}
