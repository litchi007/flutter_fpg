import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_activity_repository.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/data/models/activitys/ScratchListModel.dart';
import 'package:fpg_flutter/data/models/activitys/PrizeData.dart';

import 'package:flutter/material.dart';
import 'dart:io';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';

import 'package:fpg_flutter/app/pages/home/widgets/activity_component/widgets/ggl_dialog_controller.dart';

class GGLDialogController extends GetxController {
  var scratched = false.obs;
  var webMove = ''.obs;
  var scratchOff = false.obs;
  var alertVisible = true.obs;
  var currentLanCode = 'zh-cn'.obs;

  var show = false.obs;
  var showGglWin = false.obs;
  var tabIdx = 0.obs;
  Rx<ScratchListModel> scratchData = ScratchListModel().obs;
  RxList<GglViews> gglViews = RxList();
  var log = Rxn<Map<String, dynamic>>();
  RxInt number = 0.obs;
  var pressIdx = (-1).obs;
  var showToast = false.obs;
  var toastMsg = ''.obs;
  RxList<ScratchLog> scratchLog = RxList();
  final AppActivityRepository _activityRepository = AppActivityRepository();

  @override
  void onInit() {
    super.onInit();

    fetchLanguageCode();
    initGglViews();
  }

  void fetchLanguageCode() async {
    _activityRepository.scratchList().then((data) {
      scratchData.value = data ?? ScratchListModel();
      LogUtil.i(data);
    });

    getLog();
  }

  void getLog() {
    var id = scratchData.value.scratchWinList?[0].id;
    if (id != null) {
      _activityRepository.scratchLog(id).then((data) {
        scratchLog.value = data?.scratchLog ?? [];
        LogUtil.i(data);
      });
    }
  }

  void updateWebMove(String move) {
    webMove.value = move;
    scratched.value = true;
  }

  void updateScratchOff(bool value) {
    scratchOff.value = value;
  }

  void handleScratch() {
    if (scratched.value) {}
  }

  void setShow(bool value) => show.value = value;
  void setShowGglWin(bool value) => showGglWin.value = value;
  void setTabIdx(int index) => tabIdx.value = index;
  void setData(ScratchListModel newData) => scratchData.value = newData;
  void setGglViews(List<GglViews> newViews) => gglViews.value = newViews;
  void setLog(Map<String, dynamic>? newLog) => log.value = newLog;
  void setNum(int value) => number.value = value;
  void setPressIdx(int index) => pressIdx.value = index;
  void setShowToast(bool show, {String? msg}) {
    showToast.value = show;
    toastMsg.value = msg ?? '';
  }

  void setScratched(bool value) => scratched.value = value;

  static const int TOTAL_NUM = 9;

  void toggleShow(bool value) {
    show.value = value;
  }

  void initGglViews() {
    for (int i = 0; i < 9; i++) {
      gglViews.add(GglViews(false.obs, ''.obs));
    }
    getScratchList();
  }

  void updateGglViews() {
    number.value++;
    if (pressIdx.value < 0) return;
    var winData = scratchData.value.scratchWinList?[number.value];

    gglViews[pressIdx.value].selected?.value = true;
    gglViews[pressIdx.value].amount?.value = winData?.amount ?? '';
    scratched.value = false;
    getLog();
  }

  void getScratchList() {
    _activityRepository.scratchList().then((data) {
      scratchData.value = data ?? ScratchListModel();
      LogUtil.i(data);
    });
  }

  void requestScratchWin() {
    var id = scratchData.value.scratchWinList?[number.value].id;

    _activityRepository.scratchWin(id).then((data) {
      AppToast.showDuration(msg: data?.msg ?? '');
    });
  }

  void pressItem(int idx) {
    if ((scratchData.value.scratchWinList?.length ?? 0) - number.value == 0) {
      AppToast.showDuration(msg: '刮奖次数不足');
      return;
    }
    if (gglViews[idx].selected?.value == true ||
        scratchData.value.scratchWinList == null ||
        (scratchData.value.scratchWinList != null &&
            (scratchData.value.scratchWinList?.length ?? 0) <= number.value)) {
      return;
    }
    showGglWin.value = true;
  }
}

class GglViews {
  RxBool? selected;
  RxString? amount;

  GglViews(this.selected, this.amount);
}
