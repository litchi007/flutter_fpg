import 'package:flutter/material.dart';

class EggWinDialog extends StatelessWidget {
  final Map<String, dynamic> data;
  final VoidCallback onPress;

  const EggWinDialog({
    Key? key,
    required this.data,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Stack(
        children: [
          GestureDetector(
            onTap: onPress,
            child: Container(
              color: Colors.black.withOpacity(0.5),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.2,
            left: MediaQuery.of(context).size.width * 0.2,
            child: Container(
              width: 300,
              height: 300,
              child: Stack(
                children: [
                  Image.asset('assets/images/egg_open.png', fit: BoxFit.cover),
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.network(data['prizeIcon']),
                        SizedBox(height: 15),
                        Text(
                          data['prizeName'] ?? '',
                          style: TextStyle(color: Colors.red, fontSize: 16),
                        ),
                        SizedBox(height: 15),
                        Text(
                          data['prizeMsg'] ?? '',
                          style: TextStyle(color: Colors.orange, fontSize: 16),
                        ),
                        SizedBox(height: 15),
                        ElevatedButton(
                          onPressed: onPress,
                          style: ElevatedButton.styleFrom(
                            overlayColor: Colors.yellow,
                            minimumSize: Size(100, 30),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: Text(
                            '确定',
                            style: TextStyle(color: Colors.brown, fontSize: 17),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
