import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/active/active.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/components/custom.ani.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/constants/assets.dart';
import 'package:fpg_flutter/data/models/PostDetailModel.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/activity_gold_egg_log_model/activity_gold_egg_log_model.dart';
import 'package:fpg_flutter/models/golden_egg_list_model/golden_egg_list_model.dart';
import 'package:fpg_flutter/page/expertPlan/expertPlan.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class EggDialog extends StatefulWidget {
  final GoldenEggListModel? data;
  final List<ActivityGoldEggLogModel>?  history;
  const EggDialog({super.key, this.data, this.history});

  @override
  State<EggDialog> createState() => _EggDialogState();
}

class _EggDialogState extends State<EggDialog> {
  List<int> durationList = [];
  Duration repeatDuration = 0.milliseconds;
  late final eggList = [
    EggListModel(image: Assets.assetsImagesEgg1, left: 0, top: 0),
    EggListModel(image: Assets.assetsImagesEgg2, left: 130, top: 0),
    EggListModel(image: Assets.assetsImagesEgg3, left: 260, top: 0),
    EggListModel(image: Assets.assetsImagesEgg4, left: 0, top: 170),
    EggListModel(image: Assets.assetsImagesEgg5, left: 130, top: 170),
    EggListModel(image: Assets.assetsImagesEgg6, left: 260, top: 170),
  ].cast<EggListModel>();
  int active = 0;
  late Offset hammerTop =
      Offset((eggList[2].left ?? 0) + 80, (eggList[0].top ?? 0) + 140);
  int step = 1;
  int openActive = -1;

  final tabs = ["规则", "奖品", "记录"];
  int tabActive = 0;

  getData() async {}
  @override
  void initState() {
    final list = List.generate(6, (value) => (400 * value));
    repeatDuration = (6 * 300).milliseconds;
    durationList = list.map((e) => e).toList();
    getData();
    super.initState();
  }

  handleOpen(int value) async{
    LogUtil.w("$value ${eggList[value].left}");
    setState(() {
      step = 2;
      openActive = value;
      hammerTop = Offset(
          (eggList[value].left ?? 0) + 100, (eggList[value].top ?? 0) + 140);
    });
    final res = await ActiveHttp.activityGoldenEggWin(widget.data?.id ?? "");
    if (res.code != 0) {
      ToastUtils.showTip(CustomConfirmParams(title: res.msg));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        UnconstrainedBox(
          child: Container(
            // decoration: BoxDecoration(
            //     gradient: LinearGradient(
            //         begin: Alignment.topCenter,
            //         end: Alignment.bottomCenter,
            //         colors: [
            //       context.customTheme?.goldEggTabBegin ?? Colors.transparent,
            //       context.customTheme?.goldEggTabEnd ?? Colors.transparent
            //     ])),
            width: context.mediaQuerySize.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Stack(
                  children: [
                    Image.asset(Assets.assetsImagesLeft),
                    Container(
                      margin: const EdgeInsets.only(
                        top: 100,
                        left: 10,
                      ),
                      // width: context.mediaQuerySize.width - 50,
                      height: 330,
                      child: Stack(
                        children: [
                          ...eggList.mapIndexed((i, e) => Positioned(
                                left: e?.left,
                                top: e?.top,
                                child: CustomAni(
                                  index: i,
                                  isActiveFunction: (value, index) {
                                    if (value) {
                                      setState(() {
                                        active = index;
                                      });
                                    }
                                  },
                                  duration: durationList[i],
                                  repeatDuration: repeatDuration,
                                  child: (speed) => Container(
                                    height: 160,
                                    width: 120,
                                    // margin: EdgeInsets.only(left: e?.left ?? 0, top: e?.top ?? 0),
                                    child: Stack(
                                      children: [
                                        Positioned(
                                            bottom: 0,
                                            left: 10,
                                            child: Image.asset(
                                                Assets.assetsImagesBase)),
                                        Positioned(
                                            bottom: speed * 15 + 15,
                                            child: Stack(
                                              children: [
                                                Image.asset(e?.image ?? ''),
                                                Visibility(
                                                  visible: step == 2 &&
                                                      openActive == i,
                                                  // margin: EdgeInsets.only(top: 20),
                                                  child: Image.asset(
                                                      Assets.assetsImagesStep2),
                                                )
                                              ],
                                            )),
                                        Positioned(
                                            top: 20,
                                            child: Visibility(
                                                visible: active == i,
                                                child: Image.asset(
                                                  Assets.assetsImagesTips,
                                                  width: 50,
                                                ))),
                                      ],
                                    ),
                                  ),
                                ).onTap(() => handleOpen(i)),
                              ))
                        ],
                      ),
                    ),
                    // 锤子
                    Positioned(
                      top: hammerTop.dy,
                      left: hammerTop.dx,
                      child: CustomAni(
                          baseDuration: 100.milliseconds,
                          child: (speed) => Transform.rotate(
                              angle: pi * .05 * speed,
                              origin: Offset(50, 50),
                              child: Image.asset(Assets.assetsImagesHammer,
                                  width: 50))),
                    ),
                    Positioned(
                      bottom: 20,
                      width: context.mediaQuerySize.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            width: 180,
                            height: 40,
                            decoration: const BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        Assets.assetsImagesLeftTip))),
                            child: Text.rich(
                              TextSpan(text: "该局还可以砸", children: [
                                TextSpan(text: "2"),
                                TextSpan(text: "个金蛋"),
                              ]),
                              style: context.textTheme.bodyMedium?.copyWith(
                                  color: context.customTheme?.reverseFontColor),
                            ).paddingOnly(left: 10),
                          ),
                          DefaultTextStyle(
                            style: context.textTheme.bodyMedium!.copyWith(
                                color: context.customTheme?.reverseFontColor),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text("我的积分：16.28"),
                                Text("砸蛋积分：16.28"),
                                Text("免费抽奖剩余次数：16.28"),
                              ],
                            ).paddingOnly(right: 20),
                          )
                        ],
                      ),
                    ),

                    Positioned(
                      right: 10,
                      child: Image.asset(Assets.assetsImagesClose, width: 40,).onTap(() => Get.back())
                    )
                  ],
                ),

                Container(
                  padding: EdgeInsets.only(bottom: context.mediaQueryPadding.bottom),

                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                      context.customTheme?.goldEggTabBegin ?? Colors.transparent,
                      context.customTheme?.goldEggTabEnd ?? Colors.transparent
                    ])),
                  child: Column(
                    children: [
                      Container(
                        // height: 20,
                        padding: const EdgeInsets.only(top: 20),
                        width: context.mediaQuerySize.width,
                        
                        child: Column(
                          children: [
                            Container(
                              height: 50,
                              width: context.mediaQuerySize.width * .92,
                              decoration: BoxDecoration(
                                  borderRadius: 10.radius,
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        context.customTheme?.goldEggTabBegin ??
                                            Colors.transparent,
                                        context.customTheme?.goldEggTabEnd ??
                                            Colors.transparent
                                      ])),
                              child: Row(
                                children: tabs
                                    .mapIndexed((index, e) => Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius: 20.radius,
                                                gradient: index == tabActive
                                                    ? LinearGradient(
                                                        begin: Alignment.topCenter,
                                                        end: Alignment.bottomCenter,
                                                        colors: [
                                                            context.customTheme
                                                                    ?.goldEggTabSelectStart ??
                                                                Colors.transparent,
                                                            context.customTheme
                                                                    ?.goldEggTabSelectEnd ??
                                                                Colors.transparent
                                                          ])
                                                    : null),
                                            alignment: Alignment.center,
                                            child: Text(
                                              e,
                                              style: context.textTheme.bodyMedium
                                                  ?.copyWith(
                                                      color: context.customTheme
                                                          ?.reverseFontColor),
                                            ),
                                          ).onTap(() => setState(() {
                                                tabActive = index;
                                              })),
                                        ))
                                    .toList(),
                              ),
                            )
                          ],
                        ),
                      ),

                       // tab 项
                Visibility(
                    visible: tabActive == 0,
                    child: Container(
                      width: context.mediaQuerySize.width * .92,
                      alignment: Alignment.centerLeft,
                      child: Text(
                          widget.data?.param?.contentTurntable?.first ?? "",
                          style: context.textTheme.bodyMedium?.copyWith(
                              color: context.customTheme?.reverseFontColor)),
                    )),
                Visibility(
                    visible: tabActive == 1,
                    child: Container(
                      width: context.mediaQuerySize.width * .92,
                      child: Row(
                        children: (widget.data?.param?.prizeArr ?? [])
                            .mapIndexed((i, item) => Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    margin: EdgeInsets.only(right: 20, top: 20),
                                    decoration: BoxDecoration(
                                        borderRadius: 20.radius,
                                        gradient: LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            colors: [
                                              context.customTheme
                                                      ?.goldEggPirzeItemStart ??
                                                  Colors.transparent,
                                              context.customTheme
                                                      ?.goldEggPirzeItemEnd ??
                                                  Colors.transparent
                                            ])),
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 0.0),
                                          child: AppImage.network(
                                              item.prizeIcon ?? "",
                                              width: 80,
                                              height: 80),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 20),
                                          child: Text(
                                            item.prizeName ?? "",
                                            style: context.textTheme.bodyMedium
                                                ?.copyWith(
                                                    color: context.customTheme
                                                        ?.reverseFontColor),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ))
                            .toList(),
                      ),
                    )),
                Visibility(
                    visible: tabActive == 2,
                    child: DefaultTextStyle(
                      style: context.textTheme.bodyMedium!.copyWith(
                        color: context.customTheme?.goldEggHistoryText
                      ),
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        height: 200,
                        width: context.mediaQuerySize.width * .92,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                 Container(
                                  child: Text(""),
                                  width: 30,
                                ),
                                Expanded(
                                  child: Text("抽奖编号")
                                ),
                                Expanded(
                                  child: Text("抽奖记录")
                                ),
                                
                                Expanded(
                                  child: Text("中奖项目")
                                ),
                              ],
                            ),
                            Expanded(
                              child: ListView.builder(
                                padding: EdgeInsets.zero,
                                itemCount: widget.history?.length ?? 0,
                                itemBuilder: (context, index) {
                                  final item = widget.history?[index];
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 10
                                    ),
                                    child: Row(
                                      children: [
                                        Container(
                                          child: Text("${index+1}"),
                                          width: 30,
                                                          
                                        ),
                                        Expanded(
                                          child: Text("${item?.id}-${item?.prizeParam?.first.updateTime}")
                                        ),
                                        Expanded(
                                          child: Text("${item?.prizeParam?.first.prizeName}")
                                        ),
                                        
                                        Expanded(
                                          child: Text("${item?.prizeParam?.first.prizeAmount}")
                                        ),
                                      ],
                                    ),
                                  );
                                }
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
              
                    ],
                  ),
                ),

               ],
            ),
          ),
        ),
      ],
    );
  }
}

class EggListModel {
  String? image;
  double? left;
  double? top;

  EggListModel({this.image, this.left, this.top});

  EggListModel.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    left = json['left'];
    top = json['top'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['image'] = image;
    data['left'] = left;
    data['top'] = top;
    return data;
  }
}
