import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/widgets/redbagrain_start_modal.dart';

class RedBagRainModal extends StatefulWidget {
  final bool? show;
  final bool? isTest;
  final VoidCallback? onPress;
  final void Function(String)? onError;
  final dynamic redBag;
  final String? bagSkin;
  final String? uid;

  RedBagRainModal({
    this.show,
    this.isTest,
    this.onPress,
    this.onError,
    this.redBag,
    this.bagSkin,
    this.uid,
  });

  @override
  _RedBagRainModalState createState() => _RedBagRainModalState();
}

class _RedBagRainModalState extends State<RedBagRainModal> {
  bool redBagStart = false;
  bool redBagReward = false;
  bool showAlertModal = false;
  String alertMessage = '';
  String rewardAmount = '';

  void requestBag() {
    if (widget.uid == null) {
      widget.onPress?.call();
      // PushHelper.pushUserCenterType(UGUserCenterType.登录页); // Replace with actual implementation
      return;
    }
    // Example API calls (replace with actual API call implementation)
    final redBagRainGoApi =
        Future.delayed(Duration(seconds: 1), () => {'can_bonus_times': 1});
    final redBagRainBonusGet =
        Future.delayed(Duration(seconds: 1), () => {'bonus_amount': '100'});

    redBagRainGoApi.then((data) {
      if (data['can_bonus_times'] != null && data['can_bonus_times']! >= 0) {
        setState(() {
          redBagStart = true;
          redBagReward = true;
        });
        redBagRainBonusGet.then((bonusData) {
          setState(() {
            rewardAmount = bonusData['bonus_amount'] ?? '';
          });
        });
      } else if (data['addRecharge'] != null) {
        showAlert('您再存款${data['addRecharge']}即可再次参加活动！');
      }
    }).catchError((error) {
      setState(() {
        showAlertModal = true;
        alertMessage = error.toString();
      });
    });
  }

  void showAlert(String message) {
    setState(() {
      alertMessage = message;
      showAlertModal = true;
    });
    Future.delayed(Duration(seconds: 3), () {
      setState(() {
        showAlertModal = false;
      });
    });
  }

  String butName() {
    if (widget.redBag?.canGet == true) {
      return '立即开抢';
    } else if (widget.redBag?.attendedTimes != null) {
      return '已参与活动';
    } else if (widget.isTest == true || widget.uid == null) {
      return '登录抢红包';
    } else {
      return '立即开抢';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog.fullscreen(
        backgroundColor: Colors.transparent,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Container(
              width: 1.sw,
              height: 1.sh,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(img_static01('app/redBagRain_bg',
                      type: 'gif')), // Replace with actual URL
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
                top: 150.h,
                child: Text(
                  '红包雨活动',
                  style: TextStyle(
                      fontSize: 45.sp,
                      color: Colors.yellow,
                      fontWeight: FontWeight.bold),
                )),
            Center(
                child: GestureDetector(
              onTap: requestBag,
              child: Image.network(
                img_static01('app/redBagRainStart'), // Replace with actual URL
                width: 200.w,
                height: 200.h,
                fit: BoxFit.contain,
              ),
            )),
            if (redBagStart)
              RedBagRainStartModal(
                onPress: () {
                  setState(() {
                    redBagStart = !redBagStart;
                  });
                },
                requestBag: requestBag,
                rewardAmount: rewardAmount,
              ),
            if (showAlertModal)
              Positioned(
                child: AlertDialog(
                  backgroundColor: Colors.black.withOpacity(0.8),
                  content: Text(
                    alertMessage,
                    style: TextStyle(color: Colors.white),
                  ),
                  actions: [
                    TextButton(
                      onPressed: () {
                        setState(() {
                          showAlertModal = false;
                        });
                      },
                      child: Text('OK', style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
              ),
            Positioned(
              top: MediaQuery.of(context).padding.top + 10,
              right: 10,
              child: GestureDetector(
                onTap: () {
                  AppNavigator.back();
                },
                child: Image.network(
                  img_static01(
                      'app/redBagRainClose'), // Replace with actual URL
                  width: 65.w,
                  height: 65.h,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ],
        ));
  }
}

// class RedBagRainStartModal extends StatelessWidget {
//   final VoidCallback onPress;
//   final VoidCallback requestBag;
//   final String rewardAmount;

//   RedBagRainStartModal({
//     required this.onPress,
//     required this.requestBag,
//     required this.rewardAmount,
//   });

//   @override
//   Widget build(BuildContext context) {
//     return AlertDialog(
//       title: Text('红包雨开始'),
//       content: Text('奖励金额: $rewardAmount'),
//       actions: [
//         TextButton(
//           onPressed: onPress,
//           child: Text('关闭'),
//         ),
//         TextButton(
//           onPressed: requestBag,
//           child: Text('领取红包'),
//         ),
//       ],
//     );
//   }
// }

class ActivitySettingModel {
  final Data? data;
  ActivitySettingModel({this.data});
}

class Data {
  final String? redBagIntro;
  Data({this.redBagIntro});
}
