import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/RedBagDetail.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';

class RedBagModal extends StatelessWidget {
  final VoidCallback onPressed;
  final String bagSkin;
  final String? uid;
  final RedBagDetail? redBagDetail;
  final bool isTest;
  final String redBagIntro;

  RedBagModal({
    required this.onPressed,
    required this.bagSkin,
    this.uid,
    this.redBagDetail,
    this.isTest = false,
    this.redBagIntro = '',
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          AppImage.network(
            '$bagSkin',
            width: 380.w,
            height: 700.h,
            fit: BoxFit.fill,
          ),
          Positioned(
              top: 60.h,
              right: 0.w,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                      width: 40.w,
                      height: 40.w,
                      alignment: Alignment.topLeft,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        // Optional: Border for better visibility
                      )),
                  IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.red,
                      size: 30.w,
                    ),
                    onPressed: () {
                      AppNavigator.back();
                    },
                    // )
                  )
                ],
              )),
          Positioned(
              left: 60.w,
              top: 20.h,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Column(
                    children: [
                      Text.rich(
                        TextSpan(
                          text: '帐号:  ',
                          children: [
                            TextSpan(
                              text:
                                  '${!isTest && (uid ?? '').isNotEmpty ? GlobalService.to.userInfo.value?.usr ?? '' : '游客'}',
                              style: const TextStyle(
                                color: Colors.black,
                              ),
                            ),
                          ],
                          style: const TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ),
                      if (!(redBagDetail?.isHideAmount ?? false))
                        Padding(
                          padding: EdgeInsets.only(top: 28.h),
                          child: Text.rich(
                            TextSpan(
                              text: '红包余额：',
                              children: [
                                TextSpan(
                                  text:
                                      '${!isTest && uid?.isNotEmpty == true ? '${redBagDetail?.leftAmount}' : '---'}',
                                  style: const TextStyle(
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                              style: const TextStyle(
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                      if (!(redBagDetail?.isHideCount ?? false))
                        Padding(
                          padding: EdgeInsets.only(top: 28.h),
                          child: Text.rich(
                            TextSpan(
                              text: '可抢红包：',
                              children: [
                                TextSpan(
                                  text:
                                      '${isTest == false && uid?.isNotEmpty == true ? '${redBagDetail?.leftCount}' : '---'}',
                                  style: const TextStyle(
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                              style: const TextStyle(
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(top: 190.h),
              width: 180.w,
              height: 40.h,
              child: GestureDetector(
                onTap: () {
                  onPressed.call();
                  AppNavigator.back();
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 30.w),
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(2.w)),
                  child: Center(
                    child: Text(
                      _butName(),
                      style: TextStyle(color: Colors.white, fontSize: 20.sp),
                    ),
                  ),
                ),
              )),
          Container(
            margin: EdgeInsets.only(top: 280.h),
            alignment: Alignment.center,
            child: Column(children: [
              Text(
                '活动介绍',
                style: TextStyle(color: Colors.white, fontSize: 28.sp),
              ),
              // SizedBox(
              //   width: 280.w,
              //   child:
              SizedBox(
                height: 10.h,
              ),
              Container(
                  width: 340.w,
                  height: 350.h,
                  alignment: Alignment.center,
                  child: SingleChildScrollView(
                    child: Text(
                      redBagIntro,
                      style: TextStyle(color: Colors.white, fontSize: 18.sp),
                    ),
                    // ),
                  ))
            ]),
          ),
        ],
      ),
    );
  }

  String _butName() {
    if (redBagDetail?.canGet == 1) {
      return '立即开抢';
    } else if (redBagDetail?.attendedTimes != null) {
      return '已参与活动';
    } else if (isTest == true || uid == null) {
      return '登录抢红包';
    } else {
      return '立即开抢';
    }
  }
}
