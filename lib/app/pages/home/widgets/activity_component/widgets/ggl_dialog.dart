import 'package:flutter/material.dart';
import 'dart:io';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:flutter_html/flutter_html.dart';

import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

import 'package:fpg_flutter/app/pages/home/widgets/activity_component/widgets/ggl_dialog_controller.dart';

class ScratchOneDialog extends StatefulWidget {
  final int id;
  final dynamic scratchData;
  final VoidCallback onPress;
  final VoidCallback onScratch;

  ScratchOneDialog({
    required this.id,
    required this.scratchData,
    required this.onPress,
    required this.onScratch,
  });

  @override
  State<ScratchOneDialog> createState() => _ScratchOneDialog();
}

String getCaiJinText(String lanCode) {
  switch (lanCode) {
    case 'en':
      return 'Bonus';
    case 'vi':
      return 'Lì xì';
    case 'ja':
      return 'ジャックポット';
    case 'th':
      return 'แจ็คพอต';
    case 'hi':
      return 'Jackpot';
    case 'id':
      return 'जैकपोट';
    default:
      return '彩金';
  }
}

class _ScratchOneDialog extends State<ScratchOneDialog> {
  final GGLDialogController controller = Get.find();
  var bgImg = img_root('web/views/home/images/gyg/gyg_mask');
  final double textSize = Platform.isIOS ? 100 : 35;
  late final WebViewController _controller;

  void _sendTouchCoordinatesToWebView(double x, double y) {
    controller.scratched.value = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          GestureDetector(
            onTap: () => widget.onPress(),
            child: Container(
              width: 1.sw,
              height: 1.sh,
              color: Colors.transparent,
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.26,
            left: 45.w,
            child: Text(
              widget.id.toString(),
              style: TextStyle(
                fontSize: 28,
                color: Colors.red,
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.32,
            child: Obx(() => Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.28,
                  color: controller.scratched.value == true
                      ? AppColors.fff7dB88
                      : AppColors.ffb9ae9e,
                  child: GestureDetector(
                      onPanUpdate: (details) {
                        _sendTouchCoordinatesToWebView(
                            details.localPosition.dx, details.localPosition.dy);
                      },
                      child: Center(
                          child: Text(
                        '${getCaiJinText(controller.currentLanCode.value)}  ${widget.scratchData.amount}',
                        style: AppTextStyles.ffE27219_(context, fontSize: 60),
                      ))),
                )),
          ),
          Positioned(
            bottom: 200.h,
            child: Obx(() => SizedBox(
                width: 200.w,
                child: ElevatedButton(
                    onPressed: () {
                      if (controller.scratched.value) {
                        widget.onScratch();
                      } else {
                        widget.onPress();
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: controller.scratched.value
                          ? Colors.red
                          : Colors.purple,
                    ),
                    child: Text(
                      controller.scratched.value ? '确定' : '关闭',
                      style: TextStyle(fontSize: 18),
                    )))),
          ),
        ],
      ),
    );
  }
}

class GGLDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final GGLDialogController controller = Get.put(GGLDialogController());

    return Stack(alignment: Alignment.topCenter, children: [
      Column(
        children: [
          Stack(
            alignment: Alignment.topRight,
            children: [
              Container(
                  width: 0.95.sw,
                  height: 200.h,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(img_home('gyg/top')),
                      fit: BoxFit.fill,
                    ),
                  )),
              Positioned(
                right: 20.h,
                top: 40.h,
                child: Image.network(
                  img_home('gyg/close'),
                  fit: BoxFit.cover,
                  width: 30.w,
                ).onTap(() => Get.back()),
              ),
            ],
          ),
          Container(
              width: 0.95.sw,
              height: 0.6.sh,
              decoration: const BoxDecoration(
                color: Colors.purple,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          width: 150.w,
                          height: 40.h,
                          decoration: BoxDecoration(
                            color: AppColors.ff511BB8,
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.h)),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              AppImage.asset(
                                'number_icon.png',
                                fit: BoxFit.cover,
                                width: 15.w,
                                height: 15.w,
                              ),
                              Obx(() => Text(
                                    '刮奖次数： ${(controller.scratchData.value.scratchWinList?.length ?? 0) - controller.number.value}',
                                    style: AppTextStyles.ffFFF26F_(context,
                                        fontSize: 18),
                                  ).paddingOnly(right: 5.w))
                            ],
                          )).paddingOnly(top: 30.h, left: 10.w)
                    ],
                  ),
                  Obx(
                    () => GridView.builder(
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 0.0,
                        mainAxisSpacing: 0.0,
                        childAspectRatio: 1.2,
                      ),
                      itemCount: controller.gglViews.length,
                      itemBuilder: (context, index) {
                        return Stack(alignment: Alignment.topCenter, children: [
                          GestureDetector(
                              onTap: () {
                                controller.setPressIdx(index);
                                controller.pressItem(index);
                                if (controller.showGglWin.value = true &&
                                    controller.scratchData.value.scratchWinList
                                            ?.isNotEmpty ==
                                        true) {
                                  Get.dialog(ScratchOneDialog(
                                      id: controller.pressIdx.value + 1,
                                      scratchData: controller.scratchData.value
                                              .scratchWinList![
                                          controller.number.value],
                                      onPress: () {
                                        controller.setShowGglWin(false);
                                        controller.setScratched(false);
                                        Get.back();
                                      },
                                      onScratch: () {
                                        controller.setShowGglWin(false);
                                        controller.setScratched(false);
                                        controller.updateGglViews();
                                        controller.requestScratchWin();
                                        Get.back();
                                      }));
                                }
                              },
                              child: Obx(() => Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      AppImage.network(
                                        controller.gglViews[index].selected
                                                    ?.value ==
                                                false
                                            ? img_home('gyg/gyg_bg')
                                            : img_home('gyg/gyg_bgd'),
                                        width: 0.33.sw,
                                      ),
                                      if (controller.gglViews[index].selected
                                              ?.value ==
                                          true)
                                        Obx(() => Text(
                                            '${getCaiJinText(controller.currentLanCode.value)} ${controller.gglViews[index].amount}',
                                            style: AppTextStyles.ffE27219_(
                                                context,
                                                fontSize: 20)))
                                    ],
                                  ))),
                          Positioned(
                            left: 20.h,
                            top: 10.h,
                            child: Text(
                              '0${index + 1}',
                              style:
                                  AppTextStyles.error_(context, fontSize: 14),
                            ),
                          )
                        ]);
                      },
                    ),
                  ),
                ],
              )),
        ],
      ),
    ]);
  }
}
