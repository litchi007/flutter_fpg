import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/app_define.dart';

class RedBagModal extends StatelessWidget {
  final bool? show;
  final bool? isTest;
  final VoidCallback? onPress;
  final void Function(String)? onError;
  final dynamic redBag;
  final String? bagSkin;
  final String? uid;
  final ActivitySettingModel? activitySetting;

  RedBagModal({
    this.show,
    this.isTest,
    this.onPress,
    this.onError,
    this.redBag,
    this.bagSkin,
    this.uid,
    this.activitySetting,
  });

  @override
  Widget build(BuildContext context) {
    return show == true
        ? Dialog(
            backgroundColor: Colors.transparent,
            child: Stack(
              children: [
                Positioned.fill(
                  child: Container(
                    color: Colors.black.withOpacity(0.5),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.25,
                  left: MediaQuery.of(context).size.width * 0.1,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.5,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(bagSkin ?? 'default_image_url'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: 20),
                        Align(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            icon: Icon(Icons.close, color: Colors.red),
                            onPressed: () {
                              if (onPress != null) onPress!();
                            },
                          ),
                        ),
                        Text(
                          '账号：${isTest == false && uid?.isNotEmpty == true ? 'User' : '游客'}',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        Text(
                          '红包余额：${isTest == false && uid?.isNotEmpty == true ? 'Amount' : '---'}',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        Text(
                          '可抢红包：${isTest == false && uid?.isNotEmpty == true ? 'Count' : '---'}',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        Spacer(),
                        ElevatedButton(
                          onPressed: () {
                            // Implement requestBag function logic here
                          },
                          child: Text(
                            _butName(),
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          style: ElevatedButton.styleFrom(
                            overlayColor: Colors.red,
                            padding: EdgeInsets.symmetric(
                                horizontal: 40, vertical: 10),
                          ),
                        ),
                        SizedBox(height: 20),
                        if (bagSkin?.contains('red_pack_big.png') == true)
                          Expanded(
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Text('活动介绍',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 24)),
                                  Text(
                                    activitySetting?.data?.redBagIntro ?? '',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  ),
                                ],
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.02,
                  right: MediaQuery.of(context).size.width * 0.05,
                  child: IconButton(
                    icon: const Icon(Icons.close, color: Colors.red),
                    onPressed: () {
                      if (onPress != null) onPress!();
                    },
                  ),
                ),
              ],
            ),
          )
        : SizedBox.shrink();
  }

  String _butName() {
    if (redBag?.canGet == true) {
      return '立即开抢';
    } else if (redBag?.attendedTimes != null) {
      return '已参与活动';
    } else if (isTest == true || uid == null) {
      return '登录抢红包';
    } else {
      return '立即开抢';
    }
  }
}

class ActivitySettingModel {
  final Data? data;
  ActivitySettingModel({this.data});
}

class Data {
  final String? redBagIntro;
  Data({this.redBagIntro});
}
