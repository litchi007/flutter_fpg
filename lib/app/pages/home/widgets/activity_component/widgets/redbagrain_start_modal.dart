import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/activitys_component_controller.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/models/RedBagData.dart';

class RedBagRainStartModal extends StatefulWidget {
  final String rewardAmount;
  final VoidCallback? onPress;
  final VoidCallback? requestBag;

  const RedBagRainStartModal({
    Key? key,
    this.rewardAmount = '0',
    this.onPress,
    this.requestBag,
  }) : super(key: key);

  @override
  _RedBagRainStartModalState createState() => _RedBagRainStartModalState();
}

class _RedBagRainStartModalState extends State<RedBagRainStartModal>
    with SingleTickerProviderStateMixin {
  final ActivitysComponentController controller = Get.find();

  late AnimationController _animationController;

  static const int numberOfRedBags = 6;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 10),
    );

    startPreGameCountdown();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void startPreGameCountdown() {
    Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      if (controller.preGameCountdown.value > 1) {
        controller.preGameCountdown.value--;
      } else {
        controller.gameStarted.value = true;

        timer.cancel();
        startGameCountdown();
      }
    });
  }

  void startGameCountdown() {
    Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      if (controller.countdown.value > 0) {
        controller.countdown.value--;
      } else {
        controller.showRewardModal.value = true;
        controller.gameStarted.value = false;

        timer.cancel();
      }
    });

    startRedBagsAnimation();
  }

  void startRedBagsAnimation() {
    final random = Random();
    for (int i = 0; i < numberOfRedBags; i++) {
      final initialRotation = random.nextDouble() * 360;
      final translateY = Tween<double>(
              begin: -400, end: MediaQuery.of(context).size.height + 50)
          .animate(CurvedAnimation(
              parent: _animationController,
              curve: Interval(i / numberOfRedBags, 1.0)));
      final translateX = Tween<double>(
              begin:
                  random.nextDouble() * MediaQuery.of(context).size.width - 200,
              end:
                  random.nextDouble() * MediaQuery.of(context).size.width - 200)
          .animate(CurvedAnimation(
              parent: _animationController,
              curve: Interval(i / numberOfRedBags, 1.0)));

      _animationController.forward().whenComplete(() {
        controller.redBags.removeWhere((redBag) => redBag.key == i.toString());
      });

      controller.redBags.add(RedBagData(
        key: i.toString(),
        translateY: translateY,
        translateX: translateX,
        rotation: initialRotation,
        isClicked: false,
      ));
    }
  }

  void openRedBag(String? redBagKey) {
    controller.redBags.value = controller.redBags.map((redBag) {
      if (redBag.key == redBagKey) {
        return redBag.copyWith(isClicked: true);
      }
      return redBag;
    }).toList();
  }

  void restartGame() {
    widget.requestBag?.call();
    setState(() {
      controller.gameStarted.value = false;
      controller.preGameCountdown.value = 5;
      controller.countdown.value = 10;
      controller.showRewardModal.value = false;
    });
    startPreGameCountdown();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: widget.onPress,
      child: Stack(
        children: [
          Positioned.fill(
            child: Image.network(img_static01('app/redBagRainStart_bg'),
                fit: BoxFit.cover),
          ),
          Obx(() => (!controller.gameStarted.value)
              ? Center(
                  child: Obx(() => Text(
                        '${controller.preGameCountdown.value}',
                        style: TextStyle(fontSize: 60.sp, color: Colors.white),
                      )),
                )
              : const SizedBox()),
          Obx(() => Stack(
              children: controller.redBags
                  .map((redBag) => Positioned(
                        left: redBag.translateX.value,
                        top: redBag.translateY.value,
                        child: Transform.rotate(
                          angle: redBag.rotation,
                          child: GestureDetector(
                            onTap: () => openRedBag(redBag.key),
                            child: Image.network(
                              redBag.isClicked
                                  ? img_static01('app/redBag_open')
                                  : img_static01('app/redBag'),
                              width: 50.w,
                              height: 50.h,
                            ),
                          ),
                        ),
                      ))
                  .toList())),
          Positioned(
            bottom: screenHeight / 20,
            child: Column(
              children: [
                Image.network(
                  img_static01('app/countdown'),
                  width: 65.w,
                  height: 76.h,
                ),
                Obx(() => Text(
                      '${controller.countdown.value}',
                      style: TextStyle(fontSize: 16.sp, color: Colors.white),
                    )),
              ],
            ),
          ),
          Obx(() => (controller.showRewardModal.value)
              ? Positioned.fill(
                  child: GestureDetector(
                    onTap: widget.onPress,
                    child: Stack(
                      children: [
                        Image.network(
                          img_static01('app/redBagRainEndGame_bg', type: 'gif'),
                          fit: BoxFit.cover,
                        ),
                        Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Image.network(
                                img_static01('app/redBagRainPresent'),
                                width: 350.w,
                                height: 350.h,
                              ),
                              Text(
                                '再玩一次？',
                                style: TextStyle(
                                    fontSize: 18.sp, color: Colors.white),
                              ),
                              ElevatedButton(
                                onPressed: restartGame,
                                child: Image.network(
                                    img_static01('app/play_again'),
                                    width: 40),
                              ),
                              Text(
                                '恭喜您！您抢到${widget.rewardAmount}元!',
                                style: TextStyle(
                                  fontSize: 24.sp,
                                  color: Color(0xFF890000),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          top: MediaQuery.of(context).padding.top,
                          right: 0,
                          child: IconButton(
                            icon: Image.network(
                                img_static01('app/redBagRainClose'),
                                width: 65.w),
                            onPressed: widget.onPress,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : const SizedBox()),
        ],
      ),
    );
  }
}
