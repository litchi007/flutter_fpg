import 'dart:convert';

import 'package:fpg_flutter/api/active/active.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/data/models/RedBagDetail.dart';
import 'package:fpg_flutter/data/repositories/app_activity_repository.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/models/activity_gold_egg_log_model/activity_gold_egg_log_model.dart';
import 'package:fpg_flutter/models/golden_egg_list_model/golden_egg_list_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/data/models/activitys/SettingsModel.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/models/RedBagData.dart';

class ActivitysComponentController extends GetxController {
  final AppActivityRepository _activityRepository = AppActivityRepository();
  final AppSystemRepository _appSystemRepository = AppSystemRepository();

  Rxn<RedBagDetail> redBagDetail = Rxn();
  Rx<ActivitySettingsModel> activitySettings = ActivitySettingsModel().obs;
  var hide = false.obs;
  var diff = (-1).obs;
  var resetting = false.obs;
  // var init = false.obs;
  var position = Offset.zero.obs;
  RxBool show = false.obs;
  RxInt preGameCountdown = 5.obs;
  RxInt countdown = 10.obs;
  RxList<RedBagData> redBags = RxList();

  /// 红包雨显示
  final redbagRainStatus = false.obs;

  /// 刮刮乐显示
  final gglShow = false.obs;
  RxBool gameStarted = false.obs;
  RxBool showRewardModal = false.obs;
  final Rx<DateTime> pressSecond = DateTime.now().obs;

  RxBool isShowRedBagIcon = false.obs;
  RxBool isShowRedRainIcon = false.obs;
  RxBool isShowGGIcon = true.obs;
  // 砸金蛋显示
  final goldEddStatus = false.obs;
  final goldenEggList = const GoldenEggListModel().obs;
  final activityEggoldenEggLog = [].cast<ActivityGoldEggLogModel>().obs;
  // 获取个按钮图片
  Future<void> getFetchData([bool status = false]) async {
    _activityRepository.settings().then((data) {
      activitySettings.value = data ?? ActivitySettingsModel();
      LogUtil.w("首页配置____${jsonEncode(data?.toJson())}");
    });

    ///红包雨
    _activityRepository.redBagDetail().then((data) {
      redBagDetail.value = data;
      final start = DateTime.parse(data?.start ?? "").millisecondsSinceEpoch;
      final end = DateTime.parse(data?.end ?? "").millisecondsSinceEpoch;
      final now = DateTime.now().millisecondsSinceEpoch * 1000;
      final showTime =
          DateTime.parse(data?.showTime ?? "").millisecondsSinceEpoch;
      redbagRainStatus.value = now > showTime;
      LogUtil.i("红包雨 ${GlobalService.isTest}}");
      // isShowRedBagIcon.value =  now > start && now < end;
      // 1727417866507
      // 5480474572800000
    });

    /// 转盘活动
    _activityRepository.turntableList().then((data) {
      // if (data.first.start);
      // data.first.param.visitorShow
      final start = (data?.first.start ?? "").getTime();
      final end = (data?.first.end ?? "").getTime();
      final now = DateTime.now().millisecondsSinceEpoch * 1000;
      show.value =
          ((data?.first.param?.visitorShow == '0')) || !GlobalService.isTest;
      LogUtil.w("转盘活动 ${jsonEncode(data)}");
      // redBagDetail.value = data;
    });
    // _activityRepository.redBagRainDetail().then((data) {
    //   LogUtil.i(data);
    // });
    _activityRepository.goldenEggList().then((data) {
      LogUtil.i("砸金蛋___$data");
      goldenEggList.value = data?.first ?? const GoldenEggListModel();
      goldEddStatus.value =
          (data?.isNotEmpty ?? false) || !GlobalService.isTest;
      if (status) {
        getActivityEggoldenEggLog();
      }
      // if (data)
    });

    _appSystemRepository.floatAds().then((data) {
      LogUtil.i(data);
    });

    _activityRepository.scratchList().then((value) {
      LogUtil.w("value______${jsonEncode(value?.toJson())}");

      gglShow.value = value?.scratchList?.first.param?.visitorShow == "0" ||
          !GlobalService.isTest;
    });
  }

  getActivityEggoldenEggLog() async {
    final res = await ActiveHttp.activityEggoldenEggLog(goldenEggList.value.id ?? "");
    activityEggoldenEggLog.addAll(res.models ?? []);
  }

  @override
  void onInit() {
    super.onInit();
    if (GlobalService.to.token.isNotEmpty) {
      getFetchData(true);
    }
    GlobalService.to.token.listen((value) {
      if (value.isEmpty) {
        show.value = false;
        gglShow.value = false;
        redbagRainStatus.value = false;
        isShowRedBagIcon.value = false;
        goldEddStatus.value = false;
      } else {
        getFetchData(true);
      }
    });
  }

  Future getRedBag(String id) async {
    _activityRepository.getRedBag_activity(id: id).then((data) {
      // print(data.msg);
      AppToast.showToast(data.msg);
    });
  }
}
