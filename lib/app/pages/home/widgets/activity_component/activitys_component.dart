import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/redbag_activity_item.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/activitys_component_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/redbag_rain_activity_item.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/widgets/egg_dialog.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/widgets/red_bag_dialog.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/widgets/redbagrain_modal.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/widgets/ggl_dialog.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/components/custom.dargItem.dart';
import 'package:fpg_flutter/constants/assets.dart';
import 'package:fpg_flutter/data/models/RedBagDetail.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

class ActivitysComponent extends StatefulWidget {
  const ActivitysComponent({super.key});

  @override
  State<StatefulWidget> createState() => _ActivitysComponentState();
}

class _ActivitysComponentState extends State<ActivitysComponent> {
  final ActivitysComponentController controller =
      Get.put(ActivitysComponentController());

  @override
  void initState() {
    print('activitys_init');

    super.initState();
  }

  @override
  void dispose() {
    Get.delete<ActivitysComponentController>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        Obx(
          () => Visibility(
            visible: controller.isShowRedBagIcon.value,
            child: CustomDargItem(
              onTap: () {
                Get.dialog(
                  RedBagModal(
                    onPressed: () {
                      if (!GlobalService.to.isAuthenticated.value ||
                          GlobalService.to.userInfo.value?.uid == null) {
                        AppNavigator.toNamed(loginPath);
                        return;
                      }
                      controller
                          .getRedBag(controller.redBagDetail.value?.id ?? '');
                    },
                    uid: controller.redBagDetail.value?.name,
                    bagSkin: controller.activitySettings.value.redBagSkin ?? '',
                    redBagDetail: controller.redBagDetail.value,
                    redBagIntro:
                        controller.activitySettings.value.redBagIntro ?? '',
                  ),
                );
              },
              onClose: () {
                controller.isShowRedBagIcon.value =
                    !controller.isShowRedBagIcon.value;
              },
              left: context.mediaQuerySize.width - 125,
              top: 50,
              path:
                  // 'https://wwwstatic01.fdgdggduydaa008aadsdf008.xyz/upload/t002/customise/images/m_bonus_logo.jpg?v=1716556208 ',
                  controller.redBagDetail.value?.redBagLogo ??
                      (controller.activitySettings.value.redBagLogo ??
                          img_images('pig')),
              // show: controller.isShowRedBagIcon.value,
            ),
          ),
        ),

        /// 红包雨
        Obx(
          () => Visibility(
            // visible: controller.redbagRainStatus.value,
            visible: controller.redbagRainStatus.value,
            child: RedBagActivityItem(
              // edBagRain.png
              positionX: context.mediaQuerySize.width - 120,
              positionY: 490,
              logo: "redbagRain" ,
              onClose: () {
                controller.redbagRainStatus.value = false;
              },
              onPressed: () {
                Get.dialog(
                  RedBagRainModal(
                    onPress: () {
                      if (!GlobalService.to.isAuthenticated.value ||
                          GlobalService.to.userInfo.value?.uid == null) {
                        AppNavigator.toNamed(loginPath);
                        return;
                      }
                      controller
                          .getRedBag(controller.redBagDetail.value?.id ?? '');
                    },
                    uid: controller.redBagDetail.value?.name,
                    bagSkin: controller.activitySettings.value.redBagSkin ?? '',
                    redBag: controller.redBagDetail.value,
                    // redBagIntro:
                    // controller.activitySettings.value.redBagIntro ?? '',
                  ),
                );
              },
            ),
          ),
        ),

        Obx(() => Visibility(
            visible: controller.goldEddStatus.value,
            child: RedBagActivityItem(
              // isLocal: true,
              show: true,
              onClose: () {
                 controller.goldEddStatus.value = !controller.goldEddStatus.value;
              },
              logo: img_images("zh/zjd/zjd") ,
              positionX: context.mediaQuerySize.width - 120,
              positionY: 490,
              onPressed: () {
                Get.dialog(
                    useSafeArea: false,
                    EggDialog(data: controller.goldenEggList.value, history: controller.activityEggoldenEggLog));
              },
            ))),
       
        Obx(
          () => Visibility(
            visible: controller.gglShow.value,
            child: RedBagActivityItem(
              onPressed: () {
                Get.dialog(
                  GGLDialog(),
                );
              },
              onClose: () {
                controller.isShowGGIcon.value = !controller.isShowGGIcon.value;
              },
              positionX: 20.w,
              positionY: 500.h,
              logo: img_images('zh/gyg'),
              show: controller.isShowGGIcon.value,
            ),
          ),
        ),
      ],
    );
  }
}
