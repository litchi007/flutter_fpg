import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RedBagRainActivityItem extends StatefulWidget {
  final String logo;
  final bool show;
  final double positionX;
  final double positionY;
  final VoidCallback onPressed;
  final VoidCallback onClose;

  const RedBagRainActivityItem({
    super.key,
    required this.onPressed,
    required this.onClose,
    required this.positionX,
    required this.positionY,
    required this.logo,
    this.show = true,
  });

  @override
  State<RedBagRainActivityItem> createState() => _RedBagRainActivityItemState();
}

class _RedBagRainActivityItemState extends State<RedBagRainActivityItem> {
  late Offset _position = Offset(20, 20);
  bool _isDrag = false;
  bool _isClose = false;

  @override
  void initState() {
    _position = Offset(widget.positionX, widget.positionY);
    _isClose = !widget.show;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant RedBagRainActivityItem oldWidget) {
    _isClose = !widget.show;
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return !_isClose
        ? Positioned(
            top: _position.dy,
            left: _position.dx,
            child: Draggable(
              onDragStarted: () => setState(() => _isDrag = true),
              onDragEnd: (details) {
                double x = 0;
                double y = details.offset.dy;
                if (details.offset.dx > Get.width / 2) {
                  x = Get.width - 125.w;
                } else {
                  x = 10.w;
                }
                if (details.offset.dy < 80.h) {
                  y = 40.h;
                } else if (details.offset.dy > Get.height - 300.h) {
                  y = Get.height - 300.h;
                }
                setState(() {
                  _isDrag = false;
                  _position = Offset(
                    x,
                    y,
                  );
                });
              },
              feedback: getItemWidget(),
              // child: getItemWidget(),
              child: !_isDrag ? getItemWidget() : const SizedBox(),
            ),
          )
        : const SizedBox();
  }

  Widget getItemWidget() {
    return Stack(
      alignment: AlignmentDirectional.topEnd,
      children: [
        GestureDetector(
          onTap: widget.onPressed,
          child: widget.logo == 'redbagRain'
              ? AppImage.asset(
                  'redBagRain.png',
                  width: 120.w,
                  height: 120.w,
                  fit: BoxFit.contain,
                )
              : AppImage.network(
                  widget.logo,
                  width: 120.w,
                  height: 120.w,
                  fit: BoxFit.contain,
                ),
        ),
        GestureDetector(
          onTap: () {
            widget.onClose.call();
            // setState(() {
            //   _isClose = true;
            // });
          },
          child: Container(
            padding: EdgeInsets.all(3.w),
            decoration:
                const BoxDecoration(shape: BoxShape.circle, color: Colors.red),
            child: Icon(
              Icons.close,
              size: 20.w,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  // String formatDuration(int seconds) {
  //   final minutes = (seconds / 60).floor();
  //   final secs = seconds % 60;
  //   return '${minutes.toString().padLeft(2, '0')}:${secs.toString().padLeft(2, '0')}';
  // }
}
