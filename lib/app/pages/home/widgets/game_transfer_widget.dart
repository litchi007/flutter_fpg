import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';

class GameTransfer {
  final BuildContext context;
  final GameModel gameItem;

  GameTransfer({required this.context, required this.gameItem});

  TextEditingController textEditingController = TextEditingController();
  ScrollController scrollController = ScrollController();
  AuthController authController = Get.find();
  HomeViewController controller = Get.find();

  showTransfer() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.w))),
            child: SizedBox(
                height: 300.h,
                child: Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(
                                horizontal: 40.w, vertical: 10.h),
                            color: AppColors.drawMenu,
                            alignment: Alignment.center,
                            child: Text(
                              "您需要为${gameItem.name}转入金额吗?",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            )),
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15.w),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10.h),
                                TextField(
                                  controller: textEditingController,
                                  scrollController: scrollController,
                                  style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                  maxLines: 1,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 10.w,
                                        right: 10.w,
                                        bottom: 10.h,
                                        top: 2.h),
                                    isDense: true,
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.w),
                                    ),
                                    filled: true,
                                    hintStyle: TextStyle(
                                        color: Colors.grey.withOpacity(0.7)),
                                    hintText: "请输入金额(限整数)",
                                    fillColor: Colors.white70,
                                  ),
                                ),
                                Text("*站点和各游戏平台为不同账户,但金额可相互转换.",
                                    style: AppTextStyles.ff999999_(context,
                                        fontSize: 16)),
                                SizedBox(height: 10.h),
                                Row(
                                  children: [
                                    Text("快捷方式:",
                                        style: AppTextStyles.bodyText2(context,
                                            fontSize: 16)),
                                    SizedBox(width: 5.w),
                                    getChip("10", 10, const Color(0xff298dff)),
                                    SizedBox(width: 5.w),
                                    getChip(
                                        "100", 100, const Color(0xffdebb10)),
                                    SizedBox(width: 5.w),
                                    getChip(
                                        "1000", 1000, const Color(0xffdf1013)),
                                    SizedBox(width: 5.w),
                                    getChip("10000", 10000,
                                        const Color(0xffae10df)),
                                    SizedBox(width: 5.w),
                                    getChip(
                                        "全部", 11110, const Color(0xff1dc11c)),
                                  ],
                                )
                              ],
                            )),
                        const Spacer(),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    int money = 0;
                                    if (textEditingController.text.isNotEmpty)
                                      money =
                                          int.parse(textEditingController.text);
                                    if (money > 0) {
                                      controller
                                          .manualTransfer("${gameItem.gameId}",
                                              money.toDouble())
                                          .then((data) {
                                        LogUtil.w(
                                            "datatatatat____${jsonEncode(data)}");
                                        if (data?.code == 0) {
                                          AppToast.showSuccess(msg: '转入成功');
                                          Navigator.of(context).pop();
                                          gotoGame();
                                        } else {
                                          AppToast.showError(msg: data?.msg);
                                        }
                                      });
                                    } else {
                                      AppToast.showToast("请输入金额");
                                    }
                                  },
                                  style: AppButtonStyles.elevatedStyle(
                                      backgroundColor: Colors.white,
                                      foregroundColor: Colors.black,
                                      fontSize: 18,
                                      width: 200,
                                      height: 30),
                                  child: Text("需要,转入金额",
                                      style: AppTextStyles.bodyText2(context,
                                          fontSize: 18))),
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    gotoGame();
                                  },
                                  style: AppButtonStyles.elevatedStyle(
                                      backgroundColor: AppColors.drawMenu,
                                      foregroundColor: Colors.white,
                                      fontSize: 18,
                                      width: 200,
                                      height: 30),
                                  child: Text("不需要,立即游戏",
                                      style: AppTextStyles.bodyText2(context,
                                          fontSize: 18))),
                            ]).paddingOnly(bottom: 10.h)
                      ],
                    ),
                    Positioned(
                        top: -10.w,
                        right: -10.h,
                        child: IconButton(
                            onPressed: () => Navigator.of(context).pop(),
                            icon: const Icon(Icons.close, color: Colors.white)))
                  ],
                )),
          );
        });
  }

  Widget getChip(String label, int price, Color color) {
    return GestureDetector(
        onTap: () {
          String t = textEditingController.text;
          if (t.isNotEmpty) price += int.parse(t);
          String balance = GlobalService.to.userInfo.value?.balance ?? "";
          double b = double.parse(balance);
          if (b > price) textEditingController.text = price.toString();
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.w), color: color),
            child: Text(label,
                style: AppTextStyles.surface_(context, fontSize: 16))));
  }

  Future gotoGame() {
    return controller
        .gotoGame(gameItem.gameId.toString(), gameItem.gameCode ?? "-1")
        .then((data) async {
      if (data?.code == 1) {
        AppToast.showError(msg: data?.msg);
      } else {
        String? url = data?.data;
        if (gameItem.gameId == 769) {
          url =
              "https://center001.fhpro.cc/real_api.php?data=w90%2F2jJF3QySktCQUboqU5lHYRpdNjvXcU6eDWHbixGthBtNK47jD2%2BeVFLMWxg0Q7kPWQz4XfmDIB%2FdXNZ0lyzudAGOKUm0gAHltwXkL7VNTLMWg5vspDEXZUPZWzttE%2BLtBsr8IhyJFrdmfc8aPXInf6LVHi7vtuknL6bYAbO0zfxnuE%2Bxn6UhyRoezztP0qb5%2FnZRPgj55ZLSHwVeQIKwJF%2FuvNYTQezzosPT8GMoQJUObJDob2Ty3VnPDirYKVs7hM9K9KXT5MRLabfc2S855oRfBG8NzNuNWxs9s8R8tMhWeqFI%2FCIcKi6u%2F0hBvCtmEJ74E5o25ujjSRkeWPt7NGclSUebxMzgNFsG2tS0OjAlPKcq2UxtjQcL0IWOS6Qg%2FZV5OzVMynUNjQyBpyetrrmR0ParTksNnWP7fSOlKtwPnrvaj707j1nFLMJ%2FDhP07H3rrQ5vrG7N0d2268VGoFyklEVuBpxohcuBol66%2Fv%2Brnqdp7lg9f5zSfVl4";
        }
        // AppNavigator.toNamed(webAppPage, arguments: [url, gameItem.name]);
        if (url != null) {
          launchUrl(Uri.parse(url));
        }
      }
    });
  }
}
