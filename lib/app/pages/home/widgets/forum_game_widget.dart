import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_item_widget.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

class ForumGameWidget extends StatefulWidget {
  const ForumGameWidget({super.key});

  @override
  State<StatefulWidget> createState() => _ForumGameWidgetState();
}

class _ForumGameWidgetState extends State<ForumGameWidget> {
  final HomeViewController controller = Get.find<HomeViewController>();
  RxInt selectedTabIndex = 0.obs;

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 5.w),
          // padding: EdgeInsets.symmetric(vertical: 10.w),
          // width: double.infinity,
          decoration: BoxDecoration(
            color: appThemeColors?.homeBackgroundColor,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.w),
              topRight: Radius.circular(20.w),
            ),
          ),
          child: Obx(() => Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ...List.generate(controller.iconList.length, (index) {
                    final navItemName = controller.iconList[index].name;
                    return GestureDetector(
                        onTap: () => selectedTabIndex.value = index,
                        child: Obx(() => Container(
                              // margin: EdgeInsets.symmetric(horizontal: 10.w),
                              decoration: selectedTabIndex.value == index
                                  ? const BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xfff44600)),
                                      ),
                                    )
                                  : null,
                              child: Column(children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 10.w, right: 10.w, bottom: 10.w),
                                  child: Text(
                                    '$navItemName',
                                    style: TextStyle(
                                        color: selectedTabIndex.value == index
                                            ? const Color(0xfff44600)
                                            : const Color(0xFF000000),
                                        fontSize: 20.sp),
                                  ),
                                ),
                              ]),
                            )));
                  })
                ],
              )),
        ),
        Obx(
          () => Container(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            color: Colors.white,
            child: GridView.builder(
              shrinkWrap: true,
              physics: const BouncingScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: (AppConfig.displayNums()), // Converts to int
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 10.0,
              ),
              itemCount: (controller.iconList.isNotEmpty
                      ? controller.iconList[selectedTabIndex.value].list ?? []
                      : [])
                  .length,
              itemBuilder: (context, index) {
                final item =
                    controller.iconList[selectedTabIndex.value].list?[index];
                if (item == null) return const SizedBox();
                return GameItemWidget(gameItem: item);
              },
            ),
          ),
        ),
      ],
    );
  }
}
