import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/LhcForum2Model.dart';
import 'package:fpg_flutter/data/models/LhcdocCategoryModel.dart';
import 'package:fpg_flutter/data/models/ShowHomeListModel.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:get/get.dart';

class NewsBlockController extends GetxController {
  RxList<TopItem> list1 = RxList();
  RxList<TopItem> list2 = RxList();
  RxList<TabItem> lotteryForumTabs = RxList();
  RxList<ShowHomeListModel> showHomeList = <ShowHomeListModel>[].obs;
  RxBool isLoading = false.obs;

  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  final GameCenterController _gameCenterController =
      Get.put(GameCenterController());

  @override
  void onReady() {
    isLoading.value = true;
    _appLHCDocRepository.lhcForum2().then((forum) {
      // Mapping baoma_type to topList items
      forum!.topList?.forEach((item) {
        item.baomaType = forum.categoryList
            ?.firstWhere(
              (category) => category.alias == item.alias,
              orElse: () =>
                  LhcdocCategoryModel(alias: item.alias, baomaType: null),
            )
            .baomaType;
      });

      // Splitting list into two parts if length > 2
      List<TopItem> array = forum.topList!;
      if (array.length > 2) {
        List<TopItem> arr1 = array.sublist(0, (array.length / 2).ceil());
        if (arr1.length == 1) {
          arr1 = List.filled(2, arr1[0]);
        }
        list1.value = arr1;
        List<TopItem> arr2 = array.sublist((array.length / 2).ceil());
        if (arr2.length == 1) {
          arr2 = List.filled(2, arr2[0]);
        }
        list2.value = arr2;
      } else {
        // Directly set list1 if length <= 2
        list1.value = array;
      }

      Map<String, List<ShowHomeListModel>> contentShowHomeList =
          forum.contentShowHomeList!;
      if (contentShowHomeList.length > 1) {
        int i = 0;
        contentShowHomeList.forEach((k, v) {
          showHomeList.add(v[0]);
          LhcdocCategoryModel c =
              forum.categoryList!.firstWhere((e) => e.alias == k);
          lotteryForumTabs.add(TabItem(name: c.name, alias: c.alias));
          if (i == 0)
            _gameCenterController.getContentList(lotteryForumTabs[0].alias, 1);
          i++;
        });
      }
      isLoading.value = false;
    }).catchError((data) {
      isLoading.value = false;
    });
    super.onReady();
  }
}
