import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/new_block_widget/new_block_controller.dart';
import 'package:fpg_flutter/components/custom.load.string.webview.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class ShowHomeListWidget extends StatefulWidget {
  const ShowHomeListWidget({super.key});

  @override
  State<StatefulWidget> createState() => _ShowHomeListWidgetState();
}

class _ShowHomeListWidgetState extends State<ShowHomeListWidget> {
  final NewsBlockController newsBlockcontroller = Get.find();
  final GameCenterController gameCenterController = Get.find();

  // InAppWebViewSettings settings = InAppWebViewSettings(
  //     isInspectable: kDebugMode,
  //     mediaPlaybackRequiresUserGesture: false,
  //     allowsInlineMediaPlayback: true,
  //     iframeAllow: "camera; microphone",
  //     iframeAllowFullscreen: true,
  //     supportZoom: false,
  //     javaScriptEnabled: true,
  //     disableHorizontalScroll: true,
  //     disableVerticalScroll: false,
  //     transparentBackground: false,
  //     verticalScrollBarEnabled: true,
  //     cacheEnabled: true);
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Obx(
        () => Column(
          children: [
            if (newsBlockcontroller.showHomeList.isNotEmpty)
              Container(
                // constraints:
                //     const BoxConstraints(minHeight: 100, maxHeight: 1500),
                width: Get.width,
                // color: Colors.transparent,
                child: Visibility(
                  visible: gameCenterController.lotteryTabIndex2.value == 0,
                  //   child: AutoHeightWebview(
                  //     content: getHtml(
                  //         newsBlockcontroller.showHomeList[0].content ?? ''),
                  //   ),
                  // ),
                  //   child: InAppWebView(
                  //     initialSettings: settings,
                  //     gestureRecognizers: Set()
                  //       ..add(Factory<VerticalDragGestureRecognizer>(
                  //           () => VerticalDragGestureRecognizer())),
                  //     initialData: InAppWebViewInitialData(
                  //       data: getHtml(
                  //           newsBlockcontroller.showHomeList[0].content ?? ''),
                  //     ),
                  //   ),
                  // ),
                  // child: HtmlWidget(getHtml(newsBlockcontroller.showHomeList[0].content ?? '')),
                  child: CustomLoadStringWebview(
                    html: getHtml(
                        newsBlockcontroller.showHomeList[0].content ?? ''),
                  ),
                ),
              ),
            if (newsBlockcontroller.showHomeList.length > 1)
              Container(
                // constraints:
                //     const BoxConstraints(minHeight: 50, maxHeight: 1500),
                child: Visibility(
                  visible: gameCenterController.lotteryTabIndex2.value == 1,
                  //   child: AutoHeightWebview(
                  //     content: getHtml(
                  //         newsBlockcontroller.showHomeList[1].content ?? ''),
                  //   ),
                  // ),

                  //   child: InAppWebView(
                  //     initialSettings: settings,
                  //     gestureRecognizers: Set()
                  //       ..add(Factory<VerticalDragGestureRecognizer>(
                  //           () => VerticalDragGestureRecognizer())),
                  //     initialData: InAppWebViewInitialData(
                  //       data: getHtml(
                  //           newsBlockcontroller.showHomeList[1].content ?? ''),
                  //     ),
                  //   ),
                  // ),
                  child: CustomLoadStringWebview(
                    html: getHtml(
                        newsBlockcontroller.showHomeList[1].content ?? ''),
                  ),
                ),
              ),
          ],
        ),
      );
      // return Obx(
      //   () => Stack(
      //     children: [
      //       if (newsBlockcontroller.showHomeList.isNotEmpty)
      //         Container(
      //           constraints:
      //               const BoxConstraints(minHeight: 100, maxHeight: 1500),
      //           width: Get.width,
      //           color: Colors.transparent,
      //           child: Visibility(
      //             visible: gameCenterController.lotteryTabIndex2.value == 0,
      //             child: InAppWebView(
      //               initialOptions: options,
      //               gestureRecognizers: Set()
      //                 ..add(Factory<VerticalDragGestureRecognizer>(
      //                     () => VerticalDragGestureRecognizer())),
      //               initialData: InAppWebViewInitialData(
      //                 data: getHtml(
      //                     newsBlockcontroller.showHomeList[0].content ?? ''),
      //               ),
      //             ),
      //           ),
      //         ),
      //       if (newsBlockcontroller.showHomeList.length > 1)
      //         Container(
      //           constraints:
      //               const BoxConstraints(minHeight: 50, maxHeight: 1500),
      //           child: Visibility(
      //             visible: gameCenterController.lotteryTabIndex2.value == 1,
      //             child: InAppWebView(
      //               initialOptions: options,
      //               gestureRecognizers: Set()
      //                 ..add(Factory<VerticalDragGestureRecognizer>(
      //                     () => VerticalDragGestureRecognizer())),
      //               initialData: InAppWebViewInitialData(
      //   data: getHtml(
      //       newsBlockcontroller.showHomeList[1].content ?? ''),
      // ),
      //             ),
      //           ),
      //         ),
      //     ],
      //   ),
      // );
    });
  }

  String getHtml(String content) {
    // LogUtil.w("object__$content");
    double screenWidth = MediaQuery.of(context).size.width;
    double screehHeight = MediaQuery.of(context).size.height;
    String newContent = replaceFontSize(content
        .replaceAll('pt;;', 'pt;')
        .replaceAll('href="/mobile/#', 'href="' + AppDefine.host + '/mobile/#')
        .replaceAll('<img style="position:relative;top:8px;" src="',
            '<img class="square-image" src="')
        .replaceAll('font-family: 微软雅黑', '')
        .replaceAll('height:100%', 'height:auto')
        .replaceAll(RegExp(r'(<strong>\s*)+<\/strong>(\s*<\/strong>)+'), ''));
    // return """<html><head>
    //   <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>
    //   <style>table,table tr th, table tr td { border:1px solid; border-collapse: collapse; vertical-align: middle;}</style>

    //   <style>a {text-decoration: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);}</style>
    //   <style>.square-image {width:1.2em;height:1.2em;object-fit:cover;overflow:hidden;}</style>
    //   <style>p {margin:0; font-size: 14px}</style>
    //   </head><body>$content</body></html>""";
    final res = """<html><head>
      <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>
      <style>table,table tr th, table tr td { border:1px solid; border-collapse: collapse; vertical-align: middle;}</style>

      <style>a {text-decoration: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);}</style>
      <style>.square-image {width:1.2em;height:1.2em;object-fit:cover;overflow:hidden;}</style>
      <style>body {width:${screenWidth - 10};vertical-align: middle;}</style>
      <style>table {width:${screenWidth - 16} !important;}</style>
      <style>p {margin:0; font-size: 14px}  image {
  width: 100%!important;
  heigth: 200px!important;
  }</style>
      </head><body>$newContent<script>
                      function getHeihgt() {
    Toaster.postMessage(document.body.scrollHeight)
  }
                    </script></body></html>""";

    // LogUtil.w("res_____$res");
    return res;
  }

  String replaceFontSize(String input) {
    const double basic = 12.0; // Base size for scaling
    final RegExp regex = RegExp(
        r'font-size:\s*(\d+)pt;'); // Regular expression to match 'font-size' in pt

    return input.replaceAllMapped(regex, (Match match) {
      // Extract the font size number from the match
      int fontSize = int.parse(match.group(1)!);

      // Cap font size at 16 if it's greater than 20
      if (fontSize > 20) {
        fontSize = 16;
      }

      // Convert to em based on base size (12)
      double fontSizeInEm = fontSize / basic;

      // Return the new 'font-size' in em units, formatted to 2 decimal places
      return 'font-size: ${fontSizeInEm.toStringAsFixed(2)}em;';
    });
  }
}
