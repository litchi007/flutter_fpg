import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/widgets/auto_scrolling_list.dart';
import 'package:fpg_flutter/app/pages/home/widgets/new_block_widget/new_block_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

class NewsBlockWidget extends StatefulWidget {
  const NewsBlockWidget({super.key});

  @override
  State<StatefulWidget> createState() => _NewsBlockWidgetState();
}

class _NewsBlockWidgetState extends State<NewsBlockWidget> {
  final NewsBlockController controller = Get.put(NewsBlockController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    String newsUrl = img_mobileTemplate('38', 'ico_home_tt');

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.w),
      padding: EdgeInsets.symmetric(vertical: 5.w, horizontal: 5.w),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10.w)),
      child: Row(
        children: [
          AppImage.network(newsUrl, width: 66.w, height: 77.h),
          SizedBox(width: 10.w),
          Expanded(
            child: Obx(
              () => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  !controller.isLoading.value
                      ? AutoScrollingList(
                          items: controller.list1.value, interval: 3, type: 1)
                      : const SizedBox(),
                  Divider(
                    height: 1.w,
                    color: const Color(0xffdedbdb),
                  ),
                  !controller.isLoading.value
                      ? AutoScrollingList(
                          items: controller.list2.value, interval: 4, type: 2)
                      : const SizedBox(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
