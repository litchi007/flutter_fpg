import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class PopupMessageCP extends StatelessWidget {
  const PopupMessageCP({
    super.key,
    required this.content,
    required this.onReadMSG,
    required this.onClose,
  });
  final String content;
  final VoidCallback onReadMSG;
  final VoidCallback onClose;

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.zero,
          side: BorderSide(color: Colors.grey, width: 1.w),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                color: appThemeColors?.primary,
                height: 50.h,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "你有新消息",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25.sp,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: HtmlWidget(content
                    // '''
                    //         <!DOCTYPE html>
                    //         <html lang="en">
                    //         <head>
                    //           <meta charset="UTF-8">
                    //           <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    //           <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    //           // <title>Document</title>
                    //           <style>
                    //             *{
                    //               margin:0;
                    //               padding:0;
                    //               box-sizing:border-box;
                    //             }
                    //             table{
                    //               width: 100% !important;
                    //             }
                    //             img{
                    //               max-width: 100%;
                    //             }
                    //             a{
                    //               color: #007aff;
                    //             }
                    //           </style>
                    //         </head>
                    //         <body>
                    //         ${content}
                    //         </body>
                    //         <script>
                    //           // 让a标签点击后在A标签下新建一个视频标签用来播放视频
                    //           var aList = document.querySelectorAll('a');
                    //           for (var i = 0; i < aList.length; i++) {
                    //             const aTag = aList[i];
                    //             // if(!aTag.getAttribute('href').includes('.mp4')) continue
                    //             aTag.addEventListener('click', function (e) {
                    //               e.preventDefault();
                    //               const href = this.getAttribute('href')
                    //               // window.ReactNativeWebView.postMessage(JSON.stringify({type:'link',src:href}))
                    //             })
                    //           }
                    //           window.onload = function () {
                    //             window.location.hash = 1;
                    //             document.title = document.body.scrollHeight;
                    //             // window.ReactNativeWebView.postMessage(document.body.scrollHeight)
                    //             // window.webkit.messageHandlers.postSwiperData.postMessage(document.body.scrollHeight);
                    //           }
                    //           document.addEventListener("DOMContentLoaded", function () {
                    //             // window.ReactNativeWebView.postMessage(document.body.scrollHeight)
                    //             // window.webkit.messageHandlers.postSwiperData.postMessage(document.body.scrollHeight);
                    //           }, false);
                    //         </script>
                    //         </html>
                    //         ''',
                    ),
              ),
              Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        onReadMSG.call();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 10.h),
                        margin: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 10.h),
                        // color: appThemeColors?.primary,
                        decoration: BoxDecoration(
                          border: Border.all(width: 0.4),
                          borderRadius: BorderRadius.circular(10.w),
                        ),
                        child: const Center(
                          child: Text(
                            '取消',
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        onReadMSG.call();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 10.h),
                        margin: EdgeInsets.symmetric(
                          horizontal: 10.w,
                          vertical: 10.h,
                        ),
                        decoration: BoxDecoration(
                          color: appThemeColors?.primary,
                          // border: Border.all(width: 0.4),
                          borderRadius: BorderRadius.circular(10.w),
                        ),
                        child: const Center(
                          child: Text(
                            '确定',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
