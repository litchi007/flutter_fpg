import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/data/models/RankUserModel.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:gap/gap.dart';
import 'package:marqueer/marqueer.dart';

class RankListWidget extends StatelessWidget {
  final controller = MarqueerController();
  final List<RankUserModel>? list;
  RankListWidget({super.key, this.list});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start, // 标题和 Container 水平居左对齐
      children: [
        Container(
          margin: EdgeInsets.only(left: 15),
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(20.w),
          ),
          child: const Row(
            children: [
              Icon(Icons.bar_chart_sharp, color: Colors.black, size: 28),
              Text(
                '中奖排行榜', // 标题文本
                style: TextStyle(
                  fontSize: 16, // 设置字体大小
                  fontWeight: FontWeight.bold, // 设置字体粗细
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 0.w, horizontal: 5.w),
          margin: EdgeInsets.symmetric(vertical: 15.w, horizontal: 20.w),
          decoration: BoxDecoration(
              color: const Color.fromARGB(255, 255, 255, 255),
              borderRadius: BorderRadius.circular(20.w),
              border: Border.all(color: Colors.white, width: 3.w)),
          height: 285.h,
          child: Column(
            children: [
              const Row(
                children: [
                  Expanded(
                    child: Center(child: Text('用戶名称')),
                  ),
                  Expanded(
                    child: Center(child: Text('游戏名称')),
                  ),
                  Expanded(
                    child: Center(child: Text('中奖金额')),
                  ),
                ],
              ),
              Gap(20.w),
              Expanded(
                child: Marqueer.builder(
                  pps: 40,
                  autoStart: true,
                  controller: controller,
                  direction: MarqueerDirection.ttb,
                  autoStartAfter: const Duration(seconds: 2),
                  restartAfterInteractionDuration:
                      const Duration(milliseconds: 2),
                  interaction: false,
                  itemCount: list?.length,
                  itemBuilder: (context, index) {
                    final rankUser = list![index];
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Center(
                                  child: Text(
                                '${rankUser.username}',
                                style: const TextStyle(
                                    color: Color(0xffEA0000),
                                    fontSize: 14,
                                    fontWeight: FontWeight.normal),
                              )),
                            ),
                            Expanded(
                              child: Center(
                                  child: Text('${rankUser.type}',
                                      style: const TextStyle(
                                          color: Color(0xffEA0000),
                                          fontSize: 14,
                                          fontWeight: FontWeight.normal,
                                          overflow: TextOverflow.ellipsis))),
                            ),
                            Expanded(
                              child: Center(
                                  child: Text(
                                      '￥${double.parse(rankUser.coin ?? '').toStringAsFixed(2)}',
                                      style: const TextStyle(
                                          color: Color(0xffEA0000),
                                          fontSize: 14,
                                          fontWeight: FontWeight.normal))),
                            ),
                          ],
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
