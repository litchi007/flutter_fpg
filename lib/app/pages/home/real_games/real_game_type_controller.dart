import 'package:fpg_flutter/data/models/RealGameTypeModel.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:get/get.dart';

class RealGameTypeController extends GetxController {
  RxList<RealGameTypeModel> realGameList = RxList();
  RxBool isLoading = false.obs;

  final AppGameRepository _gameRepository = AppGameRepository();

  void getRealGameTypes(String id, {String? searchText}) {
    isLoading.value = true;
    realGameList.value = [];
    _gameRepository.getRealGameTypes(id: id, search: searchText).then((data) {
      if (data != null) {
        realGameList.value = data;
      }
      isLoading.value = false;
    });
  }
}
