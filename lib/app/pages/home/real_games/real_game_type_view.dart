import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/pages/home/real_games/real_game_type_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_transfer_widget.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/data/models/RealGameTypeModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class RealGameTypeView extends StatefulWidget {
  const RealGameTypeView({super.key});

  @override
  State<RealGameTypeView> createState() => _RealGameTypeViewState();
}

class _RealGameTypeViewState extends State<RealGameTypeView> {
  late String id = "";
  late String name = "";
  late String searchText = "";

  final RealGameTypeController controller = Get.put(RealGameTypeController());
  final _textEditingController = TextEditingController();
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    List<String?> args = Get.arguments;
    id = args[0] ?? "";
    name = args[1] ?? "";
    controller.getRealGameTypes(id);
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    // Uri uri = Uri.parse(url);

    return Scaffold(
        backgroundColor: appThemeColors?.homeBackgroundColor,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: Stack(alignment: Alignment.center, children: [
            Text(name, style: AppTextStyles.surface_(context, fontSize: 24))
          ]),
        ),
        body: Obx(() => Padding(
            padding: EdgeInsets.all(15.w),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('全部游戏',
                        style: TextStyle(color: Colors.blue, fontSize: 18.sp)),
                    SizedBox(width: 10.w),
                    Expanded(
                        child: TextField(
                      controller: _textEditingController,
                      scrollController: _scrollController,
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold),
                      maxLines: 1,
                      decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.w, right: 10.w, top: 8.h, bottom: 8.h),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.w)))),
                    )),
                    SizedBox(width: 10.w),
                    ElevatedButton(
                        onPressed: () {
                          String search = _textEditingController.text;
                          controller.getRealGameTypes(id, searchText: search);
                        },
                        style: AppButtonStyles.elevatedStyle(
                            backgroundColor: AppColors.drawMenu,
                            foregroundColor: Colors.white,
                            fontSize: 18,
                            height: 30,
                            width: 100,
                            padding: EdgeInsets.symmetric(vertical: 8.h)),
                        child: const Text("搜索"))
                  ],
                ),
                if (controller.isLoading.value)
                  const Expanded(
                      child: Center(child: CircularProgressIndicator())),
                if (!controller.isLoading.value)
                  Expanded(
                      child: GridView.builder(
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          itemCount: (controller.realGameList.value.isNotEmpty
                              ? controller.realGameList.value.length
                              : 0),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  crossAxisSpacing: 10.w,
                                  mainAxisSpacing: 10.h,
                                  childAspectRatio: 0.8),
                          itemBuilder: (BuildContext context, int index) {
                            RealGameTypeModel item =
                                controller.realGameList[index];
                            return Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20.w),
                                      topRight: Radius.circular(20.w)),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                        child: GestureDetector(
                                      onTap: () => gotoGame(item),
                                      child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(20.w),
                                              topRight: Radius.circular(20.w)),
                                          image: DecorationImage(
                                            image: NetworkImage('${item.pic}'),
                                            fit: BoxFit
                                                .contain, // Makes sure the image covers the whole area
                                          ),
                                        ),
                                      ),
                                    )),
                                    Text(item.name ?? "",
                                        overflow: TextOverflow.ellipsis,
                                        style: AppTextStyles.bodyText2(context,
                                            fontSize: 18)),
                                    ElevatedButton(
                                        onPressed: () => gotoGame(item),
                                        style: AppButtonStyles
                                            .elevatedFullWidthStyle(
                                          backgroundColor:
                                              const Color(0xffff8502),
                                          foregroundColor: Colors.white,
                                          radius: 30.w,
                                          fontSize: 16,
                                          height: 30.h,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 5.h),
                                        ),
                                        child: Text(
                                          "立刻游戏",
                                          style: AppTextStyles.surface_(context,
                                              fontSize: 18),
                                        ))
                                  ],
                                ));
                          }))
              ],
            ))));
  }

  void gotoGame(RealGameTypeModel item) {
    LogUtil.w(jsonEncode(item));

    if (!GlobalService.to.isAuthenticated.value) {
      AppNavigator.toNamed(loginPath);
      return;
    }

    if (GlobalService.isTest) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) => AppCommonDialog(
              onOk: () => Get.toNamed(loginPath),
              okLabel: '马上登录',
              title: '该游戏暂未开启试玩',
              msg: '请登录正式账号',
              cancelLabel: '取消'));
      return;
    }

    if (AppDefine.systemConfig!.switchAutoTransfer == false) {
      GameTransfer(
              context: context,
              gameItem: GameModel(
                  gameId: id, gameCode: item.code.toString(), name: item.name))
          .showTransfer();
    } else {
      GameTransfer(
              context: context,
              gameItem: GameModel(
                  gameId: id, gameCode: item.code.toString(), name: item.name))
          .gotoGame();
    }
  }
}
