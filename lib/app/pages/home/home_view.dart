// ignore_for_file: unnecessary_string_interpolations

import 'dart:async';
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/pages/activity/promotions_widget.dart';
import 'package:fpg_flutter/app/pages/home/home_view_controller.dart';
import 'package:fpg_flutter/app/pages/home/rank_list_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/activity_component/activitys_component.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/model/DownloadTab.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_transfer_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/home_notice_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/new_block_widget/news_block_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/new_block_widget/show_home_list_widget.dart';
import 'package:fpg_flutter/app/pages/home/widgets/popup_message_cp.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_notice_widget.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/BannersData.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/page/homeMenu/homeMenu.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with TickerProviderStateMixin {
  final AuthController authController = Get.find();
  final MainHomeController mainHomeController = Get.find();
  final HomeViewController controller = Get.put(HomeViewController());
  final GlobalService globalService = Get.find<GlobalService>();
  final AppUserRepository _appUserRepository = AppUserRepository();
  RxBool isWebViewShow = false.obs;
  bool isShowMessage = false;
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  WebViewController webViewController = WebViewController();
  ScrollController _scrollController = ScrollController();
  double _offset = 0;

  final GlobalKey<ScaffoldState> _HomeKey = GlobalKey(); // Create a key
  final List<DownloadTab> downloadTabs = [
    DownloadTab(
        name: '下载APP', image: img_mobileTemplate('38', 'xiazai'), link: '2'),
    DownloadTab(
      name: (AppDefine.siteId?.contains('f062') ?? false) ? '六合图库' : '聊天室',
      image: img_mobileTemplate('38', 'liaotian'),
      link: (AppDefine.siteId?.contains('f062') ?? false) ? '六合图库' : '聊天室',
    ),
    DownloadTab(
        name: '历史开奖', image: img_mobileTemplate('38', 'jilu'), link: '26'),
  ];

  Widget _loginButtonWidget() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        GestureDetector(
          onTap: () {
            AppNavigator.toNamed(loginPath);
          },
          child: Container(
            // decoration:
            // BoxDecoration(border: Border.all(color: Color(0xff5D93BC))),
            padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10.w),
            child: Text(
              '登录',
              style: TextStyle(color: const Color(0xffbe8716), fontSize: 16.sp),
            ),
          ),
        ),

        // Gap(10.w),
        GestureDetector(
          onTap: () {
            AppNavigator.toNamed(registerPath);
          },
          child: Container(
            // decoration:
            // BoxDecoration(border: Border.all(color: Color(0xff5D93BC))),
            padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10.w),
            child: Text(
              '注册',
              style: TextStyle(color: const Color(0xffbe8716), fontSize: 16.sp),
            ),
          ),
        ),
        // Gap(10.w),
        GestureDetector(
          onTap: guestLogin,
          child: Container(
            // decoration:
            // BoxDecoration(border: Border.all(color: Color(0xff5D93BC))),
            padding: EdgeInsets.symmetric(vertical: 3.h, horizontal: 10.w),
            child: Text(
              '试玩',
              style: TextStyle(color: const Color(0xffbe8716), fontSize: 16.sp),
            ),
          ),
        ),
        // Gap(10.w)
        // Icon(
        //   Icons.menu,
        //   color: Colors.white,
        // )
      ],
    );
  }

  Widget _showMenu() {
    return GestureDetector(
      onTap: () {
        Get.to(() => HomeMenuPage(),
            opaque: false,
            fullscreenDialog: true,
            transition: Transition.noTransition);
        // _HomeKey.currentState!.openEndDrawer();
      },
      child: AppImage.asset('menu_btn.png', width: 30.w, height: 30.w),
    );
  }

  late Timer? _timer;

  @override
  void initState() {
    /// 获取金币
    super.initState();
    GlobalService.to.fetchUserBalance();

    _timer = Timer.periodic(const Duration(seconds: 5), (callback) {
      if (!globalService.isAuthenticated.value) return;
      _appUserRepository
          .msgList(
        1,
        2,
      )
          .then((data) {
        // if (data.data != null) {
        //   userMsg.value = data.data!;
        //   msgList.value = data.data!.list!;
        // }
        var userLastMsg = data?.list?[0];
        if (userLastMsg?.isPopup == true &&
            userLastMsg?.isRead == 0 &&
            !isShowMessage) {
          isShowMessage = true;
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return PopupMessageCP(
                content: clearExHtml(text: userLastMsg?.content ?? ''),
                onClose: () {
                  isShowMessage = false;
                },
                onReadMSG: () {
                  isShowMessage = false;
                  _appUserRepository.readMsg(userLastMsg?.id ?? '');
                  globalService.getUserInfo();
                },
              );
            },
          );
        }
      });
    });
    TimerManager().addTimer(_timer!);

    controller.homeNoticeData.listen((value) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return const HomeNoticeWidget();
          });
    });

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.linear,
      ),
    );

    _scrollController.addListener(() {
      setState(() {
        _offset = _scrollController.offset; // 获取偏移量
      });
    });
  }

  @override
  void dispose() {
    _timer?.cancel();

    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      key: _HomeKey,
      backgroundColor: appThemeColors?.homeBackgroundColor,

      appBar: AppGeneralBar(
        backgroundColor: appThemeColors?.homeHeaderColor,
        titleWidget: Stack(
          alignment: AlignmentDirectional.centerEnd,
          children: [
            Row(
              children: [
                Obx(
                  () => AppImage.network(
                    '${controller.appLogo.value}',
                    height: 50.w,
                    width: 250.w,
                    fit: BoxFit.fill,
                  ),
                )
              ],
            ),
            Obx(() => GlobalService.to.isAuthenticated.value
                ? _showMenu()
                : _loginButtonWidget())
          ],
        ),
        actionWidgets: const [SizedBox()],
      ),
      // endDrawer: Obx((){
      //   if (GlobalService.to.isAuthenticated.value) {
      //     return Container(
      //       color: Colors.white,
      //       width: Get.width * 0.5,
      //       height: Get.height,
      //       child: const HomeMenuWidget(),
      //     );
      //   }
      //   return SizedBox();
      // }),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                // image: DecorationImage(
                //   image: AssetImage('assets/images/bg_1.jpg'),
                //   fit: BoxFit
                //       .cover, // Makes sure the image covers the whole area
                // ),
                ),
            child: SingleChildScrollView(
              controller: _scrollController,
              child: Column(
                children: [
                  Obx(
                    () => CarouselSlider(
                      options: CarouselOptions(
                        height: 280.w,
                        viewportFraction: 1.0,
                        enlargeCenterPage: false,
                        autoPlay: true,
                      ),
                      items: controller.banners.map((item) {
                        return GestureDetector(
                            onTap: () => onPressBanner(item),
                            child: Center(
                                child: AppImage.network(
                              item.pic ?? "",
                              fit: BoxFit.cover,
                              height: 280.w,
                            )));
                      }).toList(),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(
                        top: 8.w, bottom: 8.w, left: 5.w, right: 5.w),
                    color: Colors.white,
                    child: Obx(() => AppNoticeWidget(
                          titleList: controller.homeNoticeData.value
                              .scroll, // controller.notice,
                        )),
                  ),
                  Gap(5.h),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 70,
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w),
                    color: Colors.white,
                    child: Row(
                        children: [
                          Container(
                            width: 124,
                            height: 70,
                            alignment: Alignment.center,
                            child: Obx((){
                            if (GlobalService.to.isAuthenticated.value) {
                              return Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(GlobalService.isTest ? '游客' : GlobalService.to.userInfo.value?.usr ??
                                          '游客',
                                      style: TextStyle(
                                          color: const Color(0xff444444),
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 5,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          '¥ ${GlobalService.to.userBalance.balance ?? ''}',
                                          style: TextStyle(
                                              color: const Color(0xff666363),
                                              fontSize: 16.sp),
                                        ),
                                        SizedBox(width: 5.w),
                                        AnimatedBuilder(
                                          animation: _spinAnimation,
                                          builder: (context, child) {
                                            return Transform.rotate(
                                              angle:
                                              _spinAnimation.value * 6 * pi,
                                              child: GestureDetector(
                                                onTap: () async {
                                                  _controller.forward();
                                                  await GlobalService.to
                                                      .fetchUserBalance();
                                                  _controller.stop();
                                                },
                                                child: const Icon(
                                                  CustomIcons.arrows_cw,
                                                  size: 12,
                                                  color: Color(0xffbe8716),
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  ]);
                            }
                            return Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    '您还未登录',
                                    style: TextStyle(
                                        color: const Color(0xff444444),
                                        fontSize: 16, fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(height: 5,),
                                  Text(
                                    '登入/注册后查看',
                                    style: TextStyle(
                                        color: const Color(0xff666363),
                                        fontSize: 12),
                                  ),
                                ]);
                          }),),
                          Obx(()=>Expanded(child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (c, i) {
                              final model = controller.navList[i];
                              final subId = model.subId.toString();
                              return GestureDetector(
                                onTap: () async {
                                  // ToastUtils.show(model.toJson().toString());
                                  // return;
                                  switch (subId) {
                                    case '1':
                                      if (!GlobalService.to.isAuthenticated.value) {
                                        Get.toNamed(loginPath);
                                        return;
                                      }
                                      if (!GlobalService.isShowTestToast) {
                                        Get.toNamed(depositPath);
                                      }
                                      break;
                                    case '2':
                                      globalService.downloadApp();
                                      break;
                                    case '4':
                                      Get.toNamed(onlineServicePath);
                                      break;
                                    case '7':
                                      Get.toNamed(lotteryWebsitePath);
                                      break;
                                    case '9':
                                      Get.toNamed(promotionListPath);
                                      // mainHomeController.selectedPath.value='/activity';
                                      break;
                                    default:
                                      break;
                                  }
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    AppImage.network('${model.icon}',
                                        width: 50.w, height: 50.w),
                                    SizedBox(height: 5,),
                                    SizedBox(height: 24, child: Text(
                                      '${model.name}',
                                      style: TextStyle(
                                          color: const Color(0xffbe8716),
                                          fontSize: 12, fontWeight: FontWeight.bold),
                                    ),)
                                  ],
                                ).paddingOnly(left: 20),
                              );
                            },
                            itemCount: controller.navList.length,
                          )))
                        ],
                      ),
                    ),
                  // news block
                  if (!AppConfig.hideTopBanner()) 
                    Obx(() {
                      if (controller.homeAdsList.isNotEmpty) {
                        return CarouselSlider(
                          options: CarouselOptions(
                              height: 150.w,
                              viewportFraction: 1.0,
                              enlargeCenterPage: false,
                              autoPlay: controller.homeAdsList.length > 1,
                              padEnds: false),
                          items: controller.homeAdsList
                              .map(
                                (item) => Container(
                                  margin: const EdgeInsets.all(5.0),
                                  child: ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(5.0)),
                                    child: Stack(
                                      children: <Widget>[
                                        InkResponse(
                                          onTap: () {
                                            SliderBanner s = SliderBanner(
                                                linkCategory: item.linkCategory,
                                                linkPosition: item.linkPosition,
                                                pic: item.image,
                                                realIsPopup: item.realIsPopup,
                                                realSupportTrial:
                                                    item.realSupportTrial,
                                                url: item.linkCustom,
                                                linkAction: item.linkAction);
                                            onPressBanner(s);
                                          },
                                          child: AppImage.network(
                                              item.image ?? "",
                                              fit: BoxFit.fill,
                                              width: 1000.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        );
                      }
                      return SizedBox();
                    }),

                  if (!AppConfig.hideNewsBlock())
                    const NewsBlockWidget(),
                  

                  // game list widget
                  // const ForumGameWidget(),

                  Gap(10.h),

                  // gamecenter
                  const GameCenterWidget(),

                  downloadTabWidget(context),

                  const ShowHomeListWidget(),

                  // ForumGameWidget(),
                  const PromotionsWidget(
                    showTitle: true,
                  ),
                  // RankListWidget(
                  //   list: controller.rankList.value,
                  // ),
                  Obx(() => RankListWidget(
                        list: controller.rankList.value,
                      )),
                ],
              ),
            ),
          ),
          if (controller.isIniting.value)
            Center(
              child: Container(
                width: 200.0,
                height: 200.0,
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SpinKitDoubleBounce(color: Theme.of(context).primaryColor),
                    Container(width: 10.0),
                    // Text(GSYLocalizations.i18n(context)!.loading_text,
                    //     style: GSYConstant.middleText),
                  ],
                ),
              ),
            ),
          // if (_offset)
          Positioned(
            right: 10,
            bottom: 20,
            child: ElevatedButton(
              onPressed: () {
                print({'_offset ====': _offset});
                _scrollController.animateTo(
                  0, // 滚动到顶部的位置
                  duration: Duration(milliseconds: 500), // 动画时长
                  curve: Curves.easeInOut, // 动画曲线
                );
              },
              style: TextButton.styleFrom(
                backgroundColor: Colors.transparent, // 设置背景为透明
                shadowColor: Colors.transparent, // 去除阴影
                elevation: 0, // 去除按钮的提升效果
                padding: EdgeInsets.zero, // 去除内边距
              ),
              child: AppImage.network(
                Res.home_top,
                width: 45.w,
                height: 75.w,
                fit: BoxFit.contain,
              ),
            ),
          ),

          const ActivitysComponent(),
        ],
      ),
    );
  }

  void getByUrlLanch(Uri uriWithParams) async {
    if (await canLaunchUrl(uriWithParams)) {
      await launchUrl(uriWithParams);
    }
  }

  void initWebView(Uri uri) {
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
            _injectJavaScript();
          },
          onHttpError: (HttpResponseError error) {
            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            // openDialog(request);
          },
        ),
      )
      // ..loadRequest(Uri.parse('${AppDefine.host}/service/member.html'));
      ..loadRequest(uri);
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }

    webViewController = controller;
    isWebViewShow.value = true;
  }

  void _injectJavaScript() async {
    String jsCode = """
    document.querySelectorAll('body *').forEach(function(node) {
      var currentSize = window.getComputedStyle(node).fontSize;
      var newSize = parseFloat(currentSize) * 2 + 'px';  // Increase font size by 50%
      node.style.fontSize = newSize;
    });
    """;
    await webViewController.runJavaScript(jsCode);
  }

  Widget downloadTabWidget(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.w),
        child: Row(
          children: [
            ...List.generate(downloadTabs.length, (index) {
              final item = downloadTabs[index];
              return Expanded(
                  flex: 1,
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 3.w),
                      color: Colors.transparent,
                      child: ElevatedButton(
                          onPressed: () {
                            if (index == 0) {
                              globalService.downloadApp();
                            }
                            if (index == 1) {
                              if (GlobalService.to.isAuthenticated.value ==
                                  true) {
                                AppNavigator.toNamed(chatRoomListPath);
                              } else {
                                AppNavigator.toNamed(loginPath);
                              }
                            }
                            if (index == 2) {
                              AppDefine.inSites(['f099,t002'])
                                  ? AppNavigator.toNamed(lotteryHistoryPath,
                                      arguments: ['235'])
                                  : AppDefine.systemConfig
                                              ?.mobileTemplateCategory ==
                                          '7'
                                      ? const SizedBox()
                                      : AppNavigator.toNamed(
                                          AppRoutes.lotteryResult);
                            }
                          },
                          style: AppButtonStyles.elevatedOnlyTopRadius(
                              radius: 10,
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.black,
                              fontSize: 20,
                              height: 60),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AppImage.network(item.image,
                                  width: 50.w, height: 50.w),
                              Text(
                                item.name,
                                style: TextStyle(fontSize: 20.sp),
                              ),
                            ],
                          ))));
            })
          ],
        ));
  }

  void guestLogin() async {
    if (authController.loginStatus) {
      return;
    }
    AppToast.show();
    UGLoginModel? data = await authController.guestLogin();
    AppToast.dismiss();
    if (data == null) {
      AppToast.showToast('登录失败');
    } else {
      if (data.apiSid != null) {
        AppToast.showToast('登录成功');
        AppNavigator.offAllNamed(homePath);
      } else {
        AppToast.showToast('登录失败');
      }
    }
  }

  void onPressBanner(SliderBanner item) {
    LogUtil.w(item.toJson());

    if (["2", "3", "4", "5", "6", "7", "8", "15"].contains(item.linkCategory)) {
      GameModel game = GameModel(
        seriesId: item.linkCategory,
        subId: item.linkPosition,
        url: item.url,
        gameId: item.linkPosition,
        supportTrial: item.realSupportTrial,
        isPopup: item.realIsPopup,
      );

      /// 若是试玩账号 且 游戏不支持试玩, 则弹框
      if (GlobalService.isTest && item.realSupportTrial == 0) {
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => AppCommonDialog(
                onOk: () => Get.toNamed(loginPath),
                okLabel: '马上登录',
                title: '该游戏暂未开启试玩',
                msg: '请登录正式账号',
                cancelLabel: '取消'));
      } else {
        gotoGame(game);
      }
    } else if (item.linkCategory == "9") {
      if (AppDefine.systemConfig?.chatRoomSwitch == false) {
        AppToast.showToast('聊天室未开启');
        return;
      }
      AppNavigator.toNamed(chatRoomListPath);
    } else if (item.linkAction != null) {
      launchUrl(Uri.parse(item.url ?? ""));
    }
  }

  void gotoGame(GameModel gameItem) {
    if (gameItem.isPopup == null) {
      Get.toNamed('${AppRoutes.lottery}/${gameItem.gameId}');
      return;
    }
    if (gameItem.seriesId == '7' &&
        gameItem.subId == '70' &&
        gameItem.url != null) {
      launchUrl(Uri.parse(gameItem.url ?? ""));
      return;
    }
    if (gameItem.isPopup == 0) {
      if (!GlobalService.to.isAuthenticated.value) {
        AppNavigator.toNamed(loginPath);
        return;
      }
      if (AppDefine.systemConfig!.switchAutoTransfer == false) {
        GameTransfer(
                context: context,
                gameItem: GameModel(
                    gameId: gameItem.id,
                    gameCode: gameItem.gameCode,
                    name: gameItem.name))
            .showTransfer();
      } else {
        GameTransfer(
                context: context,
                gameItem: GameModel(
                    gameId: gameItem.id,
                    gameCode: gameItem.gameCode,
                    name: gameItem.name))
            .gotoGame();
      }
    } else {
      String id = "";
      if (gameItem.gameId != null) {
        id = gameItem.gameId.toString();
      } else {
        id = gameItem.id ?? "";
      }
      AppNavigator.toNamed(realGameTypesPath, arguments: [id, gameItem.name]);
    }
  }
}
