import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/pages/activity/promotion_list_controller.dart';
import 'package:fpg_flutter/app/pages/activity/promotions_widget.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:gap/gap.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class PromotionListPage extends StatefulWidget {
  const PromotionListPage({super.key});

  @override
  State<PromotionListPage> createState() => _PromotionListPageState();
}

class _PromotionListPageState extends State<PromotionListPage> {
  final AuthController authController = Get.find();
  final MainHomeController mainHomeController = Get.find();
  final PromotionListController controller = Get.put(PromotionListController());

  @override
  void initState() {
    String? str = Get.arguments;
    controller.fromMyLiuhe.value = str == 'fromMyLiuhe' ? true : false;
    // controller.initController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      backgroundColor: appThemeColors?.surface,
      appBar: AppGeneralBar(
        backgroundColor: AppColors.ffbfa46d,
        leading: GestureDetector(
          onTap: () => Get.toNamed(homePath),
          child: Icon(
            Icons.home,
            size: 40.w,
            color: Colors.white,
          ),
        ),
        titleWidget: const Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              '优惠活动',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      body: Container(
        // margin: const EdgeInsets.symmetric(horizontal: 15).w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8).w,
          color: appThemeColors?.surface,
        ),
        child: Column(
          children: [
            Obx(() => controller.fromMyLiuhe.value == true
                ? Container(
                    color: AppColors.surface,
                    child: Obx(() => controller.listTabIndex.isNotEmpty &&
                            controller.tabController.value != null
                        ? Column(children: [
                            TabBar(
                              tabAlignment: TabAlignment.start,
                              isScrollable: true,
                              controller: controller.tabController.value,
                              indicatorSize: TabBarIndicatorSize.label,
                              indicatorColor: AppColors.ffcf352e,
                              labelColor: AppColors.ffcf352e,
                              physics: const ClampingScrollPhysics(),
                              labelStyle: AppTextStyles.surface_(context,
                                  fontWeight: FontWeight.bold, fontSize: 22),
                              tabs: List.generate(
                                  controller.listTabIndex.length - 1, (index) {
                                return Tab(
                                    child: SizedBox(
                                  width: 160.w,
                                  child: Text(
                                    '${controller.categoryTabs[int.parse(controller.listTabIndex[index])].name}',
                                    textAlign: TextAlign.center,
                                  ),
                                ));
                              }),
                              onTap: (value) {
                                controller.filterType.value =
                                    controller.categoryTabs[value].id!;
                              },
                            ),
                            Divider(
                              color: AppColors.ffaeaeae,
                              height: 1.h,
                            ),
                          ])
                        : const SizedBox()),
                  )
                : Container(
                    color: const Color(0xff8EABC7),
                    child: Obx(() => controller.listTabIndex.isNotEmpty &&
                            controller.tabController.value != null
                        ? TabBar(
                            tabAlignment: TabAlignment.start,
                            isScrollable: true,
                            controller: controller.tabController.value,
                            indicatorSize: TabBarIndicatorSize.label,
                            indicatorColor: AppColors.ffffff00,
                            labelColor: AppColors.ffffff00,
                            physics: const ClampingScrollPhysics(),
                            labelStyle: AppTextStyles.surface_(context,
                                fontWeight: FontWeight.bold, fontSize: 22),
                            tabs: List.generate(controller.listTabIndex.length,
                                (index) {
                              return Tab(
                                text:
                                    '${controller.categoryTabs[int.parse(controller.listTabIndex[index])].name}',
                              );
                            }),
                            onTap: (value) {
                              controller.filterType.value =
                                  controller.categoryTabs[value].id!;
                            },
                          )
                        : const SizedBox()),
                  )),
            Gap(20.h),
            const Expanded(
              child: SingleChildScrollView(child: PromotionsWidget()),
            )
          ],
        ),
      ),
    );
  }
}
