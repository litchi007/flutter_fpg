import 'package:flutter/material.dart';
import 'package:fpg_flutter/data/models/UGPromoteListModel.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:get/get.dart';

class PromotionListController extends GetxController
    with GetSingleTickerProviderStateMixin {
  final AppSystemRepository _appSystemRepository = AppSystemRepository();
  RxMap<String, List<UGPromoteModel>> promotionMap = RxMap();
  RxString filterType = '0'.obs;
  RxList<Category> categoryTabs = RxList();
  Rxn<TabController> tabController = Rxn();
  RxBool showCategory = false.obs;
  RxBool fromMyLiuhe = false.obs;
  RxList<String> listTabIndex = ['0'].obs;

  @override
  void onInit() {
    super.onInit();
    initController();
  }

  void initController() async {
    categoryTabs.clear();
    categoryTabs.add(Category(id: '0', name: '全部', sort: 0));
    await _appSystemRepository.getPromotions().then((data) {
      if (data == null) return;
      showCategory.value = data.showCategory ?? false;
      categoryTabs.addAll(data.newCategories ?? []);

      promotionMap['0'] = [];
      promotionMap['0']?.addAll(data.list ?? []);
      data.newCategories?.forEach((category) {
        promotionMap[category.id!] = [];
        promotionMap[category.id!]?.addAll(
            data.list!.where((item) => item.category! == category.id!));
      });

      for (int i = 1; i < promotionMap.keys.length; i++) {
        if (promotionMap[i.toString()]?.isNotEmpty == true) {
          listTabIndex.add(i.toString());
        }
      }
      tabController.value =
          TabController(length: listTabIndex.length, vsync: this);
    });
  }
}
