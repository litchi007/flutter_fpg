import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fpg_flutter/app/pages/activity/promotion_list_controller.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/UGPromoteListModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';

class PromotionsWidget extends StatefulWidget {
  const PromotionsWidget({
    super.key,
    this.showTitle = false,
  });

  final bool showTitle;

  @override
  State<StatefulWidget> createState() => _PromotionsWidgetState();
}

class _PromotionsWidgetState extends State<PromotionsWidget> {
  final PromotionListController controller = Get.put(PromotionListController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          // if (widget.showTitle)
          controller.showCategory.value
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                      GestureDetector(
                          onTap: () {},
                          child: Row(children: [
                            Icon(
                              FontAwesomeIcons.gift,
                              size: 18.sp,
                            ),
                            SizedBox(width: 10.w),
                            Text(
                              '优惠活动',
                              style: TextStyle(
                                  fontSize: 20.sp, fontWeight: FontWeight.bold),
                            )
                          ])),
                      GestureDetector(
                          onTap: () => AppNavigator.toNamed(promotionListPath),
                          child: Row(children: [
                            Text(
                              '査看更多',
                              style: TextStyle(
                                  fontSize: 20.sp, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 10.w),
                            Icon(FontAwesomeIcons.chevronRight, size: 16.sp),
                          ]))
                    ])
              : const SizedBox(),
          if (widget.showTitle) SizedBox(height: 10.h),
          Obx(
            () => (controller.showCategory.value)
                ? Column(
                    children: List.generate(
                        (controller.promotionMap[controller.listTabIndex[
                                    int.parse(controller.filterType.value)]] ??
                                [])
                            .length, (index) {
                      final promote = controller.promotionMap[
                          controller.listTabIndex[
                              int.parse(controller.filterType.value)]]![index];
                      return GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return _getContent(promote);
                                });
                          },
                          child: AppImage.network(promote.pic ?? ''));
                    }),
                  )
                : const SizedBox(),
          ),
        ],
      ),
    );
  }

  Widget _getContent(UGPromoteModel promote) {
    return Center(
        child: Container(
      width: MediaQuery.of(context).size.width * 0.9,
      height: MediaQuery.of(context).size.height * 0.65,
      padding: EdgeInsets.all(10.w),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('优惠活动',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.sp)),
          Expanded(
              child: SingleChildScrollView(
                  child: HtmlWidget(
            promote.content ?? '',
          ))),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(),
                  style: AppButtonStyles.elevatedStyle(
                      backgroundColor: Colors.white,
                      foregroundColor: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                  child: const Text('取消')),
              ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(),
                  style: AppButtonStyles.elevatedStyle(
                      backgroundColor: AppColors.drawMenu,
                      foregroundColor: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                  child: const Text('确认'))
            ],
          )
        ],
      ),
    ));
  }
}
