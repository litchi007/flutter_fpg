import 'package:get/get.dart';

class GuessingViewController extends GetxController {
  String? title;
  String? webURL;
  bool? showBackButton;
  bool? hideNavItems;
  RxBool canGoBack = false.obs;
  RxString randomString = '1'.obs;
}
