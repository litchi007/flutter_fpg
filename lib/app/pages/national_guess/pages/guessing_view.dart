import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:fpg_flutter/app/pages/national_guess/pages/guessing_view_controller.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:math';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';

class GuessingView extends StatefulWidget {
  const GuessingView({super.key});
  @override
  State<GuessingView> createState() => _GuessingView();
}

class _GuessingView extends State<GuessingView> {
  late final WebViewController _controller;
  final GuessingViewController guessingViewController =
      Get.put(GuessingViewController());
  var loadingPercentage = 0;
  String url = '${AppDefine.host}/mobile/#/ucenter/guessing?a=1';

  String randomQuery = Random().nextDouble().toString();

  String get h5AuthScript {
    String token = AppDefine.userToken?.apiToken ?? '';
    String sessid = AppDefine.userToken?.apiSid ?? '';
    String fullName = GlobalService.to.userInfo.value?.fullName ?? '';

    return sessid.isNotEmpty
        ? """
          function setCookie(cookieName, value, expiresTime, path) {
            expiresTime = expiresTime || "Thu, 01-Jan-2030 00:00:01 GMT";
            path = path || "/";
            document.cookie = cookieName + "=" + encodeURIComponent(value) + "; expires=" + expiresTime + "; path=" + path;
          }
          function clearAllCookie() {
            var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
            if (keys) {
              for (var i = keys.length; i--;)
                document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString();
            }
          }
          clearAllCookie();
          setCookie('loginsessid', '$sessid');
          setCookie('logintoken', '$token');
          setCookie('username', '$fullName');
          """
        : '';
  }

  void _loadWebView() {
    _controller.loadRequest(Uri.parse(
        '${AppDefine.host}/mobile/#/ucenter/guessing?a=${guessingViewController.randomString.value}'));
  }

  @override
  void initState() {
    super.initState();
    randomQuery = DateTime.now().millisecondsSinceEpoch.toString();
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) async {
            await _controller.runJavaScript(h5AuthScript);
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) async {
            debugPrint('Page finished loading: $url');
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
                        Page resource error:
                          code: ${error.errorCode}
                          description: ${error.description}
                          errorType: ${error.errorType}
                          isForMainFrame: ${error.isForMainFrame}
                                  ''');
          },
          onHttpError: (HttpResponseError error) {
            Future.delayed(Duration(milliseconds: 1000), () {
              guessingViewController.randomString.value =
                  Random().nextDouble().toString();
              _loadWebView();
            });

            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            openDialog(request);
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      );

    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }
    _controller = controller;

    _loadWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        SafeArea(
            child: Column(
          children: <Widget>[
            Expanded(
              child: WebViewWidget(controller: _controller),
            ),
          ],
        )),
        Container(
          height: 110.h,
          child: AppBar(
            backgroundColor: AppColors.ffbfa46d,
            automaticallyImplyLeading: false,
            titleSpacing: 0,
            leading: GestureDetector(
              onTap: () async {
                if (await _controller.canGoBack()) {
                  _controller.goBack();
                } else {
                  final MainHomeController mainHomeController = Get.find();
                  mainHomeController.selectedPath.value = '/home';
                  Get.toNamed(homePath);
                  // Get.currentRoute
                }
              },
              child: Icon(Icons.arrow_back_ios,
                  color: AppColors.surface, size: 28.w),
            ),
            title: Text('全民竞猜',
                style: AppTextStyles.ffffffff(context,
                    fontSize: 30, fontWeight: FontWeight.bold)),
            centerTitle: true,
          ),
        ),
      ],
    );
  }

  Future<void> openDialog(HttpAuthRequest httpRequest) async {
    final TextEditingController usernameTextController =
        TextEditingController();
    final TextEditingController passwordTextController =
        TextEditingController();

    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('${httpRequest.host}: ${httpRequest.realm ?? '-'}'),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  decoration: const InputDecoration(labelText: 'Username'),
                  autofocus: true,
                  controller: usernameTextController,
                ),
                TextField(
                  decoration: const InputDecoration(labelText: 'Password'),
                  controller: passwordTextController,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            // Explicitly cancel the request on iOS as the OS does not emit new
            // requests when a previous request is pending.
            TextButton(
              onPressed: () {
                httpRequest.onCancel();
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                httpRequest.onProceed(
                  WebViewCredential(
                    user: usernameTextController.text,
                    password: passwordTextController.text,
                  ),
                );
                Navigator.of(context).pop();
              },
              child: const Text('Authenticate'),
            ),
          ],
        );
      },
    );
  }
}
