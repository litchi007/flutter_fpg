import 'dart:math';
import 'package:fpg_flutter/data/models/trendsModel/LotteryHistoryData.dart';
import 'package:fpg_flutter/models/lottery_data/lottery_data_model.dart';

// Constants
const double GRID_LEFT_HEADER_WIDTH = 150.0; // Use actual scale value if needed
const double GRID_ITEM_WIDTH = 46.0;
const double GRID_ITEM_HEIGHT = 46.0;

// Helper function for scaling (stub implementation)
double scale(double value) =>
    value; // Replace with actual scaling logic if needed

// Main function
Map<String, dynamic> getTrendDataCqsscQxcPcdd(
    String fromName, List<PlayData> data,
    [int defaultNumber = 0]) {
  final List<List<dynamic>> numberArray = [];
  final List<Map<String, double>> positionArr = [];
  List<String> header = [];

  // Determine header based on fromName
  switch (fromName) {
    case 'pcdd':
      header = ['百', '十', '个'];
      break;
    case 'cqssc':
      header = ['万', '千', '百', '十', '个'];
      break;
    case 'qxc':
      header = ['一', '二', '三', '四', '五', '六', '七'];
      break;
  }

  // Populate numberArray with initial values
  for (var i = 0; i < data.length; i++) {
    final element = data[i];
    final lottoryData = element.num?.split(',');
    numberArray.add([]);
    for (var j = 0; j < 10; j++) {
      numberArray[i].add(
          int.parse(lottoryData != null ? lottoryData[defaultNumber] : '0') == j
              ? 'seat'
              : 0);
    }
  }

  // Reverse the numberArray
  final List<List<dynamic>> reverseData = numberArray.reversed.toList();
  final List<List<dynamic>> tabData = [];

  for (var i = 0; i < reverseData.length; i++) {
    final totalData = reverseData[i];
    tabData.add([]);
    for (var j = 0; j < totalData.length; j++) {
      final sData = totalData[j];
      if (sData == 0) {
        if (i == 0) {
          tabData[i].add(1);
        } else {
          tabData[i]
              .add(tabData[i - 1][j] == 'seat' ? 1 : tabData[i - 1][j] + 1);
        }
      } else {
        tabData[i].add(sData);
      }
    }
  }

  final List<List<dynamic>> thisFinal = tabData.reversed.toList();
  final List<List<dynamic>> newTr = [];

  if (data.length > 100 && data.length == 200) {
    for (var i = data.length - 101; i >= 0; i--) {
      final element = data[i];
      final lottoryData = element.num?.split(',');
      newTr.add([]);
      for (var j = 0; j < 11; j++) {
        if (j == 0) {
          newTr[i].add(element.displayNumber ?? element.issue);
        } else {
          if (int.parse(
                  lottoryData != null ? lottoryData[defaultNumber] : '0') ==
              j - 1) {
            positionArr.add({
              'x': j * GRID_ITEM_WIDTH +
                  GRID_LEFT_HEADER_WIDTH -
                  GRID_ITEM_WIDTH / 2,
              'y': GRID_ITEM_HEIGHT * positionArr.length +
                  (GRID_ITEM_HEIGHT * 3) / 2,
            });
            newTr[i]
                .add(lottoryData != null ? lottoryData[defaultNumber] : '0');
          } else {
            newTr[i].add(thisFinal[i][j - 1]);
          }
        }
      }
    }
  } else if (data.length > 100 && data.length < 200) {
    final dValue = 200 - data.length;
    for (var i = data.length - dValue - 1; i >= 0; i--) {
      final element = data[i];
      final lottoryData = element.num?.split(',');
      newTr.add([]);
      for (var j = 0; j < 11; j++) {
        if (j == 0) {
          newTr[i].add(element.displayNumber ?? element.issue);
        } else {
          if (int.parse(
                  lottoryData != null ? lottoryData[defaultNumber] : '0') ==
              j - 1) {
            positionArr.add({
              'x': j * GRID_ITEM_WIDTH +
                  GRID_LEFT_HEADER_WIDTH -
                  GRID_ITEM_WIDTH / 2,
              'y': GRID_ITEM_HEIGHT * positionArr.length +
                  (GRID_ITEM_HEIGHT * 3) / 2,
            });
            newTr[i]
                .add(lottoryData != null ? lottoryData[defaultNumber] : '0');
          } else {
            newTr[i].add(thisFinal[i][j - 1]);
          }
        }
      }
    }
  } else {
    for (var i = data.length - 1; i >= 0; i--) {
      final element = data[i];
      final lottoryData = element.num?.split(',');
      newTr.add([]);
      for (var j = 0; j < 11; j++) {
        if (j == 0) {
          newTr[i].add(element.displayNumber ?? element.issue);
        } else {
          if (int.parse(
                  lottoryData != null ? lottoryData[defaultNumber] : '0') ==
              j - 1) {
            positionArr.add({
              'x': j * GRID_ITEM_WIDTH +
                  GRID_LEFT_HEADER_WIDTH -
                  GRID_ITEM_WIDTH / 2,
              'y': GRID_ITEM_HEIGHT * positionArr.length +
                  (GRID_ITEM_HEIGHT * 3) / 2,
            });
            newTr[i]
                .add(lottoryData != null ? lottoryData[defaultNumber] : '0');
          } else {
            newTr[i].add(thisFinal[i][j - 1]);
          }
        }
      }
    }
  }

  final maximumOmission = getMaximumOmission(newTr);
  final maximumConnection = getMaximumConnection(newTr);
  final totalTimes = getTotalTimes(newTr);
  final averageOmission = getAverageOmission(totalTimes);

  return {
    'data': newTr.reversed.toList(),
    'totalTimes': totalTimes,
    'averageOmission': averageOmission,
    'maximumOmission': maximumOmission,
    'maximumConnection': maximumConnection,
    'positionArr': positionArr,
    'header': header,
  };
}

// Helper functions
List<String> getMaximumConnection(List<List<dynamic>> newTr) {
  final List<String> maximumConnection = ['最大连出'];
  int maxCount = 0;

  for (var i = 1; i < 11; i++) {
    int count = 1;
    for (var index = 0; index < newTr.length; index++) {
      final item = newTr[index];
      if (item[i] is String) {
        for (var j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is String) {
            count++;
          } else {
            if (maxCount < count) {
              maxCount = count;
            }
            count = 1;
            break;
          }
        }
      }
    }
    maximumConnection.add(maxCount.toString());
    maxCount = 0;
  }

  return maximumConnection;
}

List<String> getTotalTimes(List<List<dynamic>> newTr) {
  final List<String> totalTimes = ['出现总次数'];

  for (var i = 1; i < 11; i++) {
    int count = 0;
    for (var item in newTr) {
      if (item[i] == (i - 1).toString()) {
        count++;
      }
    }
    totalTimes.add(count.toString());
  }

  return totalTimes;
}

List<String> getMaximumOmission(List<List<dynamic>> newTr) {
  final List<String> maximumOmission = ['最大遗漏'];
  int omission = 0;

  for (var i = 1; i < 11; i++) {
    if (newTr[0][i] is! String) {
      omission = newTr[0][i];
    }
    for (var index = 0; index < newTr.length; index++) {
      final item = newTr[index];
      if (item[i] is String) {
        for (var j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is! String) {
            if (omission < newTr[j][i]) {
              omission = newTr[j][i];
              break;
            }
          }
        }
      }
    }
    maximumOmission.add(omission.toString());
    omission = 0;
  }

  return maximumOmission;
}

List<String> getAverageOmission(List<String> totalTimes) {
  final List<String> averageOmission = ['平均遗漏'];

  for (var i = 1; i < 11; i++) {
    averageOmission
        .add((100 / (int.parse(totalTimes[i]) + 1)).round().toString());
  }

  return averageOmission;
}
