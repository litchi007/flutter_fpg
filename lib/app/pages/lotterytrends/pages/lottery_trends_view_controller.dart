import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/data/repositories/app_graborder_repository.dart';
import 'package:fpg_flutter/app/pages/changlong/models/LDItemModel.dart';

class LotteryTrendsViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();
  final ApiGraborderRepository _appGraborderRepository =
      ApiGraborderRepository();
  ActionType actionType = ActionType.DEPOSIT;
  RxInt index = 0.obs;
  RxMap<String, Map<String, Object>> selectedItem = {
    '2_1': {
      'bet': UGBetItemModel(playId: '2', playName: 'cat', odds: 0.1),
      'game': UGChanglongaideModel()
    }
  }.obs;
  var isVisible = false.obs;
  RxInt selectedButton = 0.obs;
  RxBool showMenu = false.obs;
  RxInt selectedIndex = 0.obs;
  final List<Offset> coordinates = [
    const Offset(1, 2),
    const Offset(2, 3),
    const Offset(4, 8),
    const Offset(5, 8),
    const Offset(6, 10),
    const Offset(7, 2),
    const Offset(10, 5),
    const Offset(12, 6),
    const Offset(13, 3),
    const Offset(16, 10),
  ];
  void setSelectedBet(
      {String? gameId,
      int? index,
      UGBetItemModel? bet,
      UGChanglongaideModel? game}) {
    selectedItem.value = {
      '${gameId ?? ''}_${index ?? ''}': {
        'bet': bet ?? UGBetItemModel(),
        'game': game ?? UGChanglongaideModel()
      }
    };
  }

  void setShowSubmenu(bool show) {
    showMenu.value = show;
  }
}
