import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/pages/lottery_trends_view_controller.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/widgets/TrendTableWidget.dart';

class LotteryTrendsView extends StatefulWidget {
  const LotteryTrendsView({super.key});

  @override
  State<LotteryTrendsView> createState() => _LotteryTrendsViewState();
}

class _LotteryTrendsViewState extends State<LotteryTrendsView>
    with TickerProviderStateMixin {
  late TabController _tabController;

  late AnimationController _controller;
  late Animation<double> _animation;
  LotteryTrendsViewController controller =
      Get.put(LotteryTrendsViewController());
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    );

    _animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_controller);

    _animation.addListener(() {
      setState(() {});
    });
  }

  void _showContainer() {
    _controller.forward();
  }

  void _hideContainer() {
    _controller.reverse();
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      controller.actionType = ActionType.values[_tabController.index];
    }
  }

  final tabList = ['一', '二', '三'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.ff6f96a4,
      appBar: AppBar(
        backgroundColor: AppColors.ff609AC5,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ]),
            )),
        title: Text('开奖走势',
            style: AppTextStyles.surface_(context,
                fontWeight: FontWeight.bold, fontSize: 24)),
        centerTitle: true,
        leadingWidth: 0.3.sw,
        actions: [
          IconButton(
            icon: const Icon(
              Icons.more_vert,
              color: AppColors.surface,
            ),
            onPressed: () {
              controller.setShowSubmenu(true);
            },
          ),
        ],
      ),
      body: Stack(alignment: Alignment.topCenter, children: [
        ListView(
          children: [
            SizedBox(
              height: 65.h,
            ),
            const TrendTableWidget()
          ],
        ),

        Positioned(top: 0.h, child: _renderHeaderWidget()),
        Positioned(bottom: 0.h, child: _renderBottomWidget()),
        // Positioned(
        //   top: (65 + 60).h,
        //   child: CustomPaint(
        //     painter: GridPainter(controller. coordinates),
        //     size: Size(double.infinity, double.infinity),
        //   ),
        // ),
        _bottomMenu(context)
      ]),
    );
  }

  Widget _renderHeaderWidget() {
    return Container(
        width: 1.sw,
        height: 65.h,
        color: AppColors.ff6f96a4,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: tabList
                .map((item) => _tabItem(item, tabList.indexOf(item)))
                .toList()));
  }

  Widget _tabItem(String title, int index) {
    return Obx(() => Container(
          width: 0.15.sw,
          height: 50.h,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: controller.selectedIndex.value == index
                ? AppColors.ffffaa2f
                : AppColors.ff609AC5,
            border: Border.all(color: AppColors.ff8D8B8B),
            borderRadius: BorderRadius.all(Radius.circular(8.w)),
          ),
          child: Center(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: AppTextStyles.surface_(context, fontSize: 20),
            ),
          ),
        ).onTap(() => controller.selectedIndex.value = index));
    // ).onTap(() => controller.selectedIndex.value = index);
  }

  Widget _bottomTextButton(
      {String? title,
      BorderRadius? borderRadius,
      Color? backgroundColor,
      double? height,
      double? width,
      VoidCallback? onTap}) {
    return ElevatedButton(
      onPressed: () => onTap?.call(),
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.symmetric(horizontal: 8.w),
        backgroundColor: backgroundColor,
        minimumSize: Size(width ?? 0.4.sw, height ?? 50.h),
        shape: RoundedRectangleBorder(
          borderRadius: borderRadius ?? BorderRadius.zero, // Zero border radius
        ),
        elevation: 0,
      ),
      child: Text(
        title ?? '',
        style: TextStyle(color: AppColors.surface, fontSize: 23.0.w),
      ),
    ).margin(EdgeInsets.only(right: 8.w));
  }

  Widget _bottomIconButton() {
    return Container(
        width: 50.w,
        height: 40.w,
        margin: EdgeInsets.only(right: 8.w),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: AppColors.ffE84B3E,
          borderRadius: BorderRadius.all(Radius.circular(8.w)),
        ),
        child: const Center(
            child: Icon(
          Icons.autorenew_rounded,
          color: AppColors.surface,
          size: 20,
        ))).onTap(() => {});
  }

  Widget _renderBottomWidget() {
    return Container(
      width: 1.sw,
      height: 50.h,
      color: AppColors.ff6f96a4,
      child: Row(
        children: <Widget>[
          _bottomTextButton(
            title: "选择彩种",
            backgroundColor: AppColors.ff4B9CD9,
            onTap: () => AppLogger.d('okok1'),
          ),
          const Spacer(),
          _bottomIconButton(),
          _bottomTextButton(
              title: "选择彩种",
              width: 130.w,
              height: 40.h,
              borderRadius: BorderRadius.all(Radius.circular(8.w)),
              backgroundColor: AppColors.ffE68131,
              onTap: () {
                // showDialog(
                //   context: context,
                //   builder: (context) => CustomDialog(),
                //   barrierDismissible: false,
                // );
                // dialogBuilder();
                _showContainer();
              }),
          _bottomTextButton(
            title: "去下注",
            width: 70.w,
            borderRadius: BorderRadius.all(Radius.circular(8.w)),
            height: 40.h,
            backgroundColor: AppColors.ffE84B3E,
            onTap: () => Get.toNamed(betLotteryPath, arguments: 1),
          ),
        ],
      ),
    );
  }

  Dialog createDialog() {
    return Dialog(
        backgroundColor: AppColors.surface,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child:
            Column(mainAxisSize: MainAxisSize.min, children: _dialogContent()));
  }

  List<Widget> _dialogContent() {
    List<Widget> temp = [];
    for (int j = 0; j <= 7 - 1; j++) {
      temp.add(Obx(() => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0).w,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List.generate(3, (index) {
                  return _selectButton(index + j * 3);
                })),
          )));
    }
    temp.add(Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0).w,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        ElevatedButton(
          onPressed: () {
            _hideContainer();
          },
          style: ElevatedButton.styleFrom(
            minimumSize: Size(
              0.46.sw,
              60.h,
            ),
            shape: RoundedRectangleBorder(
              side: BorderSide(
                color: AppColors.ff9D9797, // Border color
                width: 0.5.w, // Border width
              ),
              borderRadius: BorderRadius.circular(8),
            ),
          ),
          child: Text('取消'),
        ),
        ElevatedButton(
          onPressed: () {
            _hideContainer();
          },
          style: ElevatedButton.styleFrom(
            minimumSize: Size(
              0.46.sw,
              60.h,
            ),
            backgroundColor: AppColors.ff5576F0,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                color: AppColors.ff9D9797, // Border color
                width: 0.5.w, // Border width
              ),
              borderRadius: BorderRadius.circular(8),
            ),
          ),
          child: Text('确定'),
        )
      ]),
    ));
    return temp;
  }

  Widget _selectButton(int index, {double? width, double? height}) {
    return ElevatedButton(
      onPressed: () {
        controller.selectedButton.value = index;
      },
      style: ElevatedButton.styleFrom(
        minimumSize: Size(width ?? 0.32.sw, height ?? 60.h),
        backgroundColor: controller.selectedButton.value == index
            ? AppColors.ff5576F0
            : AppColors.surface,
        foregroundColor: controller.selectedButton.value == index
            ? Colors.white
            : Colors.black,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: AppColors.ff9D9797, // Border color
            width: 0.5.w, // Border width
          ),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      child: Text('${index + 1}'),
    );
  }

  Widget _bottomMenu(BuildContext context) {
    return Positioned(
      // bottom: 0.4.sh,
      bottom: (_controller.value - 0.9) * 1.sh,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(4.w),
          child: AnimatedContainer(
              duration: const Duration(milliseconds: 50),
              curve: Curves.easeInOut,
              height: 0.6.sh,
              width: 1.sw,
              color: AppColors.surface,
              child: Container(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 1.sw,
                  child: Column(children: _dialogContent()),
                ),
              ))),
    );
  }

  Widget _leftSideMenu(BuildContext context) {
    return Positioned(
      // bottom: 0.4.sh,
      right: (_controller.value) * 0.6.sw,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(4.w),
          child: AnimatedContainer(
              duration: const Duration(milliseconds: 50),
              curve: Curves.easeInOut,
              height: 1.sh,
              width: 0.6.sw,
              color: AppColors.surface,
              child: Container(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 0.6.sw,
                  height: 1.sh,
                  child: Column(children: [const SizedBox()]),
                ),
              ))),
    );
  }

  void dialogBuilder() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (BuildContext context) {
          return SizedBox(height: 0.6.sh, width: 1.sw, child: createDialog());
        });
  }
}
