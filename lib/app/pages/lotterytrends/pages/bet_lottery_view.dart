import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/changlong/const/longdragon_const.dart';
import 'package:fpg_flutter/app/pages/account/widgets/activity_reward_dialog.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_language.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/pages/bet_lottery_view_controller.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/widgets/sidemenuwidget.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/widgets/TimeWidget.dart';

import 'package:fpg_flutter/data/models/trendsModel/NextIssueData.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter/webview_flutter.dart';

class BetLotteryView extends StatefulWidget {
  const BetLotteryView({super.key});

  @override
  State<BetLotteryView> createState() => _BetLotteryViewState();
}

class _BetLotteryViewState extends State<BetLotteryView>
    with TickerProviderStateMixin {
  late TabController _tabController;
  BetLotteryViewController controller = Get.put(BetLotteryViewController());
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  final AuthController authController = Get.find();

  NextIssueData item = NextIssueData(
    preDisplayNumber: '1245623456',
    gameType: '3',
    preResult: 'bb',
    preNum: '1',
    hash: '12365471222222222444444489',
    verifUrl: 'aaaaaaaaa0',
  );

  int? lotteryId;
  bool _reload = false;
  WebViewController _webviewVontroller = WebViewController();

  var loadingPercentage = 0;
  String url = '${AppDefine.host}/service/member.html';

// const K3Code = {
//     SJ: 'SJ', //三军 Armed Forces
//     WS: 'WS', //围骰 Dice
//     DS: 'DS', //点数Score
//     CP: 'CP', //长牌
//     DP: 'DP', //短牌
//     RTH: 'RTH', //二同号
//     SBTH: 'SBTH', //三不同号
//     CBC: 'CBC', //猜不出
//     YS: 'YS', //颜色
//     DZHZ: 'DZHZ', //对子和值
//     SBTHZ: 'SBTHZ', //三不同和值
//     CM: 'CM', //猜码
//     XT: 'XT', //形态
//   };

  @override
  void initState() {
    super.initState();
    lotteryId = Get.arguments;
    _tabController = TabController(length: categoryTabs.length, vsync: this);
    _tabController.addListener(_handleTabChange);
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.linear,
      ),
    );
  }

  void showWebView() {
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController webviewVontroller =
        WebViewController.fromPlatformCreationParams(params);

    webviewVontroller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
                        Page resource error:
                          code: ${error.errorCode}
                          description: ${error.description}
                          errorType: ${error.errorType}
                          isForMainFrame: ${error.isForMainFrame}
                                  ''');
          },
          onHttpError: (HttpResponseError error) {
            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            openDialog(request);
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      )
      ..loadRequest(Uri.parse('${AppDefine.host}/service/member.html'));
    if (webviewVontroller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (webviewVontroller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }

    _webviewVontroller = webviewVontroller;
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      controller.actionType = ActionType.values[_tabController.index];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.topCenter, children: [
      Scaffold(
        backgroundColor: AppColors.ff8EAAC6,
        appBar: AppBar(
          backgroundColor: AppColors.ff609AC5,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15).w,
              child: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.surface, size: 28.w),
                  const Spacer(
                    flex: 2,
                  ),
                  Text('其它普通游戏 ',
                      style: AppTextStyles.surface_(context,
                          fontWeight: FontWeight.bold, fontSize: 24)),
                  const Icon(
                    Icons.arrow_drop_down,
                    color: AppColors.surface,
                  ),
                  const Spacer(
                    flex: 1,
                  ),
                ]),
              )),
          // title: Row(
          //   // mainAxisSize: MainAxisSize.max,
          //   children: [
          //     Text('其它普通游戏 ',
          //         style: AppTextStyles.surface_(context,
          //             fontWeight: FontWeight.bold, fontSize: 24)),
          //     const Icon(
          //       Icons.arrow_drop_down,
          //       color: AppColors.surface,
          //     ),
          //   ],
          // ),
          // centerTitle: false,
          leadingWidth: 0.5.sw,
          actions: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  '${currencyLogo[AppDefine.systemConfig?.currency]}  ${GlobalService.to.userInfo.value?.balance ?? ''}',
                  style: AppTextStyles.surface_(context, fontSize: 20),
                ),
                AnimatedBuilder(
                  animation: _spinAnimation,
                  builder: (context, child) {
                    return Transform.rotate(
                      angle: _spinAnimation.value * 2 * 3.14159,
                      child: Container(
                        width: 24.w,
                        height: 24.w,
                        decoration: const BoxDecoration(
                          color: AppColors.ff8acfb8,
                          shape: BoxShape.circle,
                        ),
                        child: GestureDetector(
                          onTap: () {
                            if (!_reload) {
                              setState(() {
                                _reload = true;
                              });
                              _fetchBalance();
                              _controller.forward(from: 0).then((_) {
                                _controller.reset();
                                setState(() {
                                  _reload = false;
                                });
                              });
                            }
                          },
                          child: Center(
                            child: Icon(
                              Icons.refresh,
                              color: AppColors.ff4caf50,
                              size: 20.w,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0).w,
                  // child: Text(
                  //   '必看说明',
                  //   style: AppTextStyles.surface_(context, fontSize: 20),
                  // ).onTap(
                  //   () => _showApplyDialog(context, htmlstring),
                  // ),
                  child: IconButton(
                    icon: const Icon(
                      Icons.menu,
                      color: AppColors.surface,
                    ),
                    onPressed: () {
                      controller.setShowSubmenu(true);
                    },
                  ),
                )
              ],
            ),
          ],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(46.h),
            child: Container(
              color: AppColors.ff609AC5,
              child: _createTab(context),
            ),
          ),
        ),
        body: Column(children: [
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                _firstTabContent(),
                _secondTabContent(),
              ],
            ),
          ),
        ]),
      ),
      Obx(
        () => controller.showMenu.value
            ? SideMenuWidget(onPressMenu: controller.setShowSubmenu)
            : const SizedBox(),
      ),
      if (_webviewVontroller == WebViewController())
        WebViewWidget(controller: _webviewVontroller),
    ]);
  }

  Future<void> openDialog(HttpAuthRequest httpRequest) async {
    final TextEditingController usernameTextController =
        TextEditingController();
    final TextEditingController passwordTextController =
        TextEditingController();

    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('${httpRequest.host}: ${httpRequest.realm ?? '-'}'),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  decoration: const InputDecoration(labelText: 'Username'),
                  autofocus: true,
                  controller: usernameTextController,
                ),
                TextField(
                  decoration: const InputDecoration(labelText: 'Password'),
                  controller: passwordTextController,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            // Explicitly cancel the request on iOS as the OS does not emit new
            // requests when a previous request is pending.
            TextButton(
              onPressed: () {
                httpRequest.onCancel();
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                httpRequest.onProceed(
                  WebViewCredential(
                    user: usernameTextController.text,
                    password: passwordTextController.text,
                  ),
                );
                Navigator.of(context).pop();
              },
              child: const Text('Authenticate'),
            ),
          ],
        );
      },
    );
  }

  Widget _firstTabContent() {
    return Stack(alignment: Alignment.topCenter, children: [
      ListView(
        children: [
          renderItemContent(),
          // const TrendTableWidget()
          TimeWidget(),
          Obx(() => controller.isHistoryShowed.value == false
              ? const SizedBox()
              : _showHistoryBets()),
          _renderLotteryContent(),
          _renderBottomWidget()
        ],
      ),
      Positioned(bottom: 0.h, child: _renderBottomWidget())
    ]);
  }

  Widget _renderLotteryContent() {
    return SizedBox(
      height: 550.h,
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                  height: double
                      .infinity, // Ensures the ListView takes up the full height
                  child: ListView(
                      children: List.generate(
                          30,
                          (index) => Container(
                                width: 100,
                                color: AppColors.ffA3BBD3,
                                child: Center(child: Text('Item $index')),
                              ))))),
          Expanded(
              flex: 3,
              child: Container(
                  height: double
                      .infinity, // Ensures the ListView takes up the full height
                  child: ListView(
                      children: List.generate(
                          30,
                          (index) => Row(
                                children: [
                                  Container(
                                    width: 0.375.sw,
                                    color: AppColors.ff8EAAC6,
                                    child: Center(child: Text(' $index')),
                                  ),
                                  Container(
                                    width: 0.375.sw,
                                    color: AppColors.ff8EAAC6,
                                    child: Center(child: Text(' ${index + 1}')),
                                  ),
                                ],
                              ))))),
        ],
      ),
    );
  }

  Widget _bottomButton(
      {String? title,
      double? width,
      double? height,
      double? fontSize,
      Color? color}) {
    return ElevatedButton(
      onPressed: () {
        // Handle press
      },
      style: ElevatedButton.styleFrom(
        backgroundColor: color ?? AppColors.ff387EF5,
        fixedSize: Size(width ?? 80.w, height ?? 80.w),
        padding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.all(Radius.circular(4.w)), // Zero border radius
        ),
        elevation: 0,
      ),
      child: Text(
        title ?? '',
        style:
            TextStyle(color: AppColors.surface, fontSize: fontSize ?? 23.0.w),
      ),
    ).paddingOnly(right: 5.w);
  }

  Widget _renderBottomWidget() {
    return Container(
        height: 150.h,
        width: 1.sw,
        padding: EdgeInsets.symmetric(horizontal: 20.0.w),
        decoration: BoxDecoration(
          color: AppColors.ff3E3F3E, // Example color
          // color: AppColors.ff013E3E3F, // Example color
          borderRadius: BorderRadius.circular(3),
        ),
        child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          '已选中',
                          style: TextStyle(
                              color: AppColors.onBackground, fontSize: 20.0.w),
                        ),
                        Text(
                          '0',
                          style: TextStyle(
                              color: AppColors.onBackground, fontSize: 20.0.w),
                        ),
                        Text(
                          '注',
                          style: TextStyle(
                              color: AppColors.onBackground, fontSize: 20.0.w),
                        ),
                        const Spacer(),
                        _bottomButton(
                            title: '筹码',
                            color: AppColors.ff17c492,
                            width: 50.w,
                            height: 30.h)
                      ],
                    ),
                    Container(
                      height: 50.h,
                      child: TextField(
                        textAlignVertical: TextAlignVertical.center,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: '输入金额',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(3),
                            borderSide: const BorderSide(
                              color: AppColors.ff9D9797,
                              width: 1,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.only(left: 20.0.w, top: 0, bottom: 0),
                        ),
                        keyboardType: TextInputType.number,
                        onChanged: (text) {
                          // Handle text change
                        },
                      ),
                    ),
                  ],
                ),
              ),
              _bottomButton(title: '下注', color: AppColors.ffcf352e),
              _bottomButton(title: '重置', color: AppColors.ff3490D3),
            ]));
  }

  Widget renderItemContent() {
    return SizedBox(
        child: Column(
      children: [
        Row(children: [
          SizedBox(
            width: 150.w,
            height: 60.h,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  '${item.preDisplayNumber}期',
                  style: AppTextStyles.surface_(context, fontSize: 20),
                ),
                DecoratedBox(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(12).w),
                      color: AppColors.ff819EB0,
                    ),
                    child: SizedBox(
                        width: 70.w,
                        height: 30.h,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '刷新',
                              style:
                                  AppTextStyles.surface_(context, fontSize: 16),
                            ),
                            Icon(
                              Icons.refresh,
                              color: AppColors.ff4caf50,
                              size: 20.w,
                            ),
                          ],
                        ))),
              ],
            ),
          ).paddingOnly(right: 10.w, top: 10.h),
          SizedBox(
            width: 0.6.sw,
            height: 60.h,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: List.generate(5, (index) => _zodiacNumber(index)),
                ),
                Row(
                  children: List.generate(8, (index) => _zodiacText('小')),
                )
              ],
            ),
          ).paddingOnly(top: 10.h),
          const Spacer(),
          SizedBox(
            height: 60.h,
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Icon(
                  Icons.arrow_drop_down,
                  color: AppColors.surface,
                ),
                Positioned(
                    bottom: 25.h,
                    child: Icon(
                      Icons.arrow_drop_up,
                      color: AppColors.surface,
                    )),
              ],
            ),
          ).onTap(() => controller.isHistoryShowed.value =
              !controller.isHistoryShowed.value),
        ]),
        Divider(
          thickness: 0.5.w,
          color: AppColors.surface,
        ),
        _hashVerifyWidget(hashNo: item.hash),
        Divider(
          thickness: 0.5.w,
          color: AppColors.surface,
        ),
      ],
    ));
  }

  Widget _prizeRecordWidget() {
    return Row(children: [
      SizedBox(
        width: 150.w,
        height: 60.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              '${item.preDisplayNumber ?? ''}期',
              style: AppTextStyles.surface_(context, fontSize: 20),
            ),
          ],
        ),
      ).paddingOnly(right: 10.w, top: 10.h),
      SizedBox(
        width: 0.6.sw,
        height: 60.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: List.generate(5, (index) => _zodiacNumber(index)),
            ),
            Row(
              children: List.generate(8, (index) => _zodiacText('小')),
            )
          ],
        ),
      ).paddingOnly(top: 10.h),
    ]);
  }

  Widget _hashVerifyWidget({String? hashNo}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
            width: 100.w,
            height: 40.h,
            child: Text(
              '哈希验证:',
              textAlign: TextAlign.center,
              style: AppTextStyles.ff4e57dd_(context, fontSize: 20),
            )).paddingOnly(top: 10.h).onTap(() => showWebView()),
        SizedBox(
          width: 150.w,
          height: 40.h,
          child: Text(
            hashNo ?? '',
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: AppTextStyles.surface_(context, fontSize: 20),
          ),
        ).paddingOnly(right: 20.w, top: 10.h).onTap(() => showWebView()),
        SizedBox(
          width: 150.w,
          height: 40.h,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(12).w),
                border: Border.all(width: 1.w)),
            child: Center(
              child: Text(
                '彩种介绍',
                textAlign: TextAlign.center,
                style: AppTextStyles.bodyText2(context, fontSize: 20),
              ),
            ),
          ),
        ).onTap(
          () => _showApplyDialog(context, htmlstring),
        )
      ],
    );
  }

  Widget _showHistoryBets() {
    return SizedBox(
      height: 0.35.sh,
      child: ListView(
        children: List.generate(10, (_) => _prizeRecordWidget()),
      ),
    );
  }

  Widget _zodiacNumber(int index) {
    return CircleAvatar(
      radius: 18.w,
      backgroundColor: AppColors.ffc5c5c5,
      child: Text(
        index.toString(),
        style: AppTextStyles.bodyText2(context, fontSize: 20),
        overflow: TextOverflow.visible,
        softWrap: true,
      ),
    );
  }

  Widget _zodiacText(String title) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 3.w),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(6).w),
        color: AppColors.ff819EB0,
      ),
      child: Center(
        child: Text(
          title,
          style: AppTextStyles.surface_(context, fontSize: 16),
        ),
      ),
    );
  }

  Widget _createTab(BuildContext context) {
    return Align(
        alignment: Alignment.centerLeft,
        child: TabBar(
          controller: _tabController,
          indicatorColor: AppColors.ff0277BD,
          labelColor: AppColors.surface,
          unselectedLabelColor: AppColors.surface,
          padding: EdgeInsets.zero,
          isScrollable: false,
          labelPadding: EdgeInsets.zero,
          indicatorPadding: EdgeInsets.zero,
          labelStyle: AppTextStyles.surface_(context,
              fontWeight: FontWeight.bold, fontSize: 22),
          tabs: List.generate(categoryTabs.length, (index) {
            return Container(
                width: 0.5.sw,
                height: 60.h,
                color: AppColors.CLBgColor,
                child: Center(
                    child: Text(
                  '${categoryTabs[index]['name']}',
                )));
          }),
        ));
  }

  Widget _secondTabContent() {
    return SizedBox();
  }

  Widget _action(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              // padding: EdgeInsets.only(top: 8.w),
              height: 50.h,
              width: 0.7.sw,
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: AppColors.ff999999, width: 2.sp)),
                  borderRadius: BorderRadius.all(Radius.circular(5.w))),
              child: Text('确认',
                      textAlign: TextAlign.center,
                      style: AppTextStyles.ff3490D3_(context, fontSize: 24))
                  .paddingOnly(top: 20.h),
            ),
            // ),
          ),
        ],
      ),
    );
  }

  void _showApplyDialog(BuildContext context, String? winApplyContent) {
    return ActivityRewardDialog(
            context: context,
            title: '必看说明',
            titleBackgroundColor: AppColors.surface,
            action: _action(context),
            htmlstring: (winApplyContent ?? ''),
            align: TextAlign.left,
            height: 0.6.sh,
            width: 0.8.sw,
            titleTextStyle: AppTextStyles.bodyText2(context, fontSize: 28))
        .dialogBuilder();
  }

  void _fetchBalance() {
    if (!GlobalService.to.isAuthenticated.value) {
      return;
    }
  }
}
