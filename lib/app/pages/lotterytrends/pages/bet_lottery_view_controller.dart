import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/data/repositories/app_graborder_repository.dart';
import 'package:fpg_flutter/app/pages/changlong/models/LDItemModel.dart';

class BetLotteryViewController extends GetxController {
  final AppUserRepository _appUserRepository = AppUserRepository();
  final ApiGraborderRepository _appGraborderRepository =
      ApiGraborderRepository();
  ActionType actionType = ActionType.DEPOSIT;
  RxBool showMenu = false.obs;
  RxInt index = 0.obs;
  RxBool isHistoryShowed = false.obs;

  RxMap<String, Map<String, Object>> selectedItem = {
    '2_1': {
      'bet': UGBetItemModel(playId: '2', playName: 'cat', odds: 0.1),
      'game': UGChanglongaideModel()
    }
  }.obs;

  void setShowSubmenu(bool show) {
    showMenu.value = show;
  }

  void setSelectedBet(
      {String? gameId,
      int? index,
      UGBetItemModel? bet,
      UGChanglongaideModel? game}) {
    selectedItem.value = {
      '${gameId ?? ''}_${index ?? ''}': {
        'bet': bet ?? UGBetItemModel(),
        'game': game ?? UGChanglongaideModel()
      }
    };
  }
}
