class Game {
  final String id;
  final String title;

  Game({required this.id, required this.title});
}
