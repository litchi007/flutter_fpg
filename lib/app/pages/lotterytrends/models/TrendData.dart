class TrendData {
  final List<String>? averageOmission;
  final List<String>? maximumConnection;
  final List<String>? maximumOmission;
  final List<String>? totalTimes;
  final List<List<dynamic>>? data;
  final List<Map<String, double>>? positionArr;
  final List<String>? header;

  // Constructor
  TrendData({
    this.averageOmission,
    this.maximumConnection,
    this.maximumOmission,
    this.totalTimes,
    this.data,
    this.positionArr,
    this.header,
  });
}
