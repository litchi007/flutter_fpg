import 'package:flutter/material.dart';

class GridPainter extends CustomPainter {
  final List<Offset> coordinates;

  GridPainter(this.coordinates);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.red
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke;

    if (coordinates.isEmpty) return;

    for (int i = 0; i < coordinates.length - 1; i++) {
      final start = coordinates[i];
      final end = coordinates[i + 1];

      final startOffset = _getOffset(start);
      final endOffset = _getOffset(end);

      canvas.drawLine(startOffset, endOffset, paint);
    }
  }

  Offset _getOffset(Offset coordinate) {
    final x = coordinate.dx * 50.0 + 25.0;
    final y = coordinate.dy * 50.0 + 25.0;
    return Offset(x, y);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
