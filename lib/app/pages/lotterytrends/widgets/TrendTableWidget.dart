import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';

import 'package:fpg_flutter/app/pages/lotterytrends/pages/lottery_trends_view_controller.dart';

class TrendTableWidget extends StatefulWidget {
  const TrendTableWidget({super.key});

  @override
  _WithdrawalRecordViewState createState() => _WithdrawalRecordViewState();
}

class _WithdrawalRecordViewState extends State<TrendTableWidget> {
  final LotteryTrendsViewController controller = Get.find();
  int _rowCnt = 20;
  int _columnCnt = 10;

  @override
  Widget build(BuildContext context) {
    List<Widget> temp = [];
    int crossAxisCount = 8;
    int len = 64;
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: SingleChildScrollView(
            scrollDirection: Axis.vertical, child: _createDataTable(context)));

    // for (int j = 0; j <= len / crossAxisCount - 1; j++) {
    //   temp.add(  Row(
    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //       children: List.generate(crossAxisCount, (index) {
    //         int row = index ~/ crossAxisCount;
    //         int col = index % crossAxisCount;
    //         bool isFirstRow = row == 0;
    //         bool isRedCircle = controller.coordinates
    //             .contains(Offset(row.toDouble(), col.toDouble()));

    //         return Container(
    //           decoration: BoxDecoration(
    //               border: Border.all(color: AppColors.CLBgColor, width: 0.5.w),
    //               shape: BoxShape.rectangle,
    //               color: (isFirstRow && col == 0)
    //                   ? Colors.blue
    //                   : AppColors.ffD6D3EC),
    //           // margin: EdgeInsets.all(2),
    //           child: isRedCircle
    //               ? CircleAvatar(
    //                   backgroundColor: AppColors.ff3490D3,
    //                   child: Text(
    //                     isFirstRow ? 'aaa' : 'bbb',
    //                     style: TextStyle(color: Colors.white),
    //                   ),
    //                 )
    //               : Center(
    //                   child: Text(
    //                     isFirstRow ? 'aaa' : 'bbb',
    //                     style: TextStyle(color: Colors.white),
    //                   ),
    //                 ),
    //         );
    //       })) );
    // }

    // return temp;

    // CustomPaint for lines
  }

  Widget _createDataTable(BuildContext context) {
    return DataTable(
      headingRowHeight: 60.h,
      columnSpacing: 0,
      horizontalMargin: 0,
      showCheckboxColumn: false,
      columns: _createColumns(context),
      rows: _createRows(context),
    );
  }

  List<DataColumn> _createColumns(BuildContext context) {
    return [
      _createDateColumn(context),
      ...List.generate(
        _columnCnt,
        (index) => _createDataColumn(context, index),
      )
    ];
  }

  DataColumn _createDateColumn(BuildContext context) {
    return DataColumn(
      label: Container(
        height: 60.h,
        width: 140.w,
        padding: EdgeInsets.symmetric(horizontal: 8.w),
        decoration: BoxDecoration(
          color: AppColors.ff96B1C1,
          border: Border.all(width: 0.5.w, color: AppColors.ffC5CBC6),
        ),
        child: Center(
          child: Text(
            '金额',
            textAlign: TextAlign.center,
            style: AppTextStyles.surface_(context, fontSize: 20),
          ),
        ),
      ),
    );
  }

  DataColumn _createDataColumn(BuildContext context, int index) {
    return DataColumn(
      label: Container(
        height: 60.h,
        width: 60.w,
        decoration: BoxDecoration(
          color: AppColors.ff96B1C1,
          border: Border.all(width: 0.5.w, color: AppColors.ffC5CBC6),
        ),
        child: Center(
          child: Text(
            index.toString(),
            textAlign: TextAlign.center,
            style: AppTextStyles.surface_(context, fontSize: 20),
          ),
        ),
      ),
    );
  }

  List<DataRow> _createRows(BuildContext context) {
    return List<DataRow>.generate(
      _rowCnt,
      (rowIndex) => DataRow(
        onSelectChanged: (done) => {},
        cells: [
          _dataCell(context, '1123456789',
              textStyle: AppTextStyles.surface_(context, fontSize: 20),
              color: AppColors.ff96B1C1),
          ...List.generate(_columnCnt, (columnIndex) {
            bool isRedCircle = controller.coordinates.contains(
                Offset(rowIndex.toDouble(), (columnIndex - 1).toDouble()));

            return _dataCell(context, '5', isRedCircle: isRedCircle);
          }),
        ],
      ),
    );
  }

  DataCell _dataCell(BuildContext context, String title,
      {TextStyle? textStyle, Color? color, bool? isRedCircle}) {
    return DataCell(
      Container(
        width: double.infinity,
        padding: isRedCircle == true
            ? EdgeInsets.zero
            : EdgeInsets.symmetric(horizontal: 8.w),
        decoration: BoxDecoration(
          color: color ?? AppColors.ffD6D3EC,
          border: Border.all(width: 0.5.w, color: AppColors.ffC5CBC6),
        ),
        child: Center(
          child: isRedCircle == true
              ? CircleAvatar(
                  radius: 25.w,
                  backgroundColor: AppColors.ff3490D3,
                  child: Text(
                    title,
                    style: textStyle ??
                        AppTextStyles.ff666666(context, fontSize: 20),
                    overflow: TextOverflow.visible,
                    softWrap: true,
                  ),
                )
              : Text(
                  title,
                  style: textStyle ??
                      AppTextStyles.ff666666(context, fontSize: 20),
                  overflow: TextOverflow.visible,
                  softWrap: true,
                ),
        ),
      ),
    );
  }
}
