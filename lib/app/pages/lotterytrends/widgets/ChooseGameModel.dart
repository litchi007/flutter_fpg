import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/models/Game.dart';

class ChooseGameModal extends StatelessWidget {
  final bool? showModal;
  final VoidCallback? onClose;
  final ValueChanged<String?>? onGameSelected;
  final List<Game>? games;
  final String? selectedGameId;

  const ChooseGameModal({
    this.showModal,
    this.onClose,
    this.onGameSelected,
    this.games,
    this.selectedGameId,
  });

  @override
  Widget build(BuildContext context) {
    // Default to false if showModal is null
    final isVisible = showModal ?? false;

    return isVisible
        ? Dialog(
            insetPadding: EdgeInsets.zero,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 120 * 3,
                  decoration: BoxDecoration(
                    color: Theme.of(context).appBarTheme.backgroundColor,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(8)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: Center(
                      child: Text(
                        '选择彩种',
                        style: TextStyle(
                          fontSize: 16,
                          color: AppColors.surface,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 340,
                  color: Colors.white,
                  child: GridView.builder(
                    padding: const EdgeInsets.all(2),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 4,
                      mainAxisSpacing: 4,
                    ),
                    itemCount: games?.length ?? 0,
                    itemBuilder: (context, index) {
                      final game = games?[index];
                      final isSelected = game?.id == selectedGameId;

                      return GestureDetector(
                        onTap: () =>
                            game != null ? onGameSelected?.call(game.id) : null,
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: isSelected ?? false
                                ? Colors.blue
                                : Colors.white,
                            border: Border.all(
                              color: isSelected ?? false
                                  ? Colors.blue
                                  : Colors.grey,
                            ),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Text(
                            game?.title ?? '',
                            style: TextStyle(
                              color: isSelected ?? false
                                  ? Colors.white
                                  : Colors.black,
                              fontSize: 13,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Container(
                  color: Colors.white,
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 6),
                  child: Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            foregroundColor: Colors.black,
                            side: BorderSide(color: Colors.grey),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                            elevation: 0,
                          ),
                          onPressed: onClose,
                          child: Text('取消'),
                        ),
                      ),
                      SizedBox(width: 4),
                      Expanded(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.blue,
                            foregroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                            elevation: 0,
                          ),
                          onPressed: () {
                            if (onClose != null) {
                              onClose!();
                            }
                            // Trigger any additional logic if needed
                          },
                          child: Text('确定'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        : SizedBox.shrink();
  }
}
