import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';

import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:fpg_flutter/configs/app_define.dart';

import 'package:fpg_flutter/data/models/trendsModel/NextIssueData.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/widgets/TimeWidgetController.dart';
import 'dart:math';

class TimeWidget extends StatefulWidget {
  const TimeWidget({super.key});

  @override
  State<TimeWidget> createState() => _TimeWidgetState();
}

class _TimeWidgetState extends State<TimeWidget> {
  WebViewController _controller = WebViewController();

  final TimeWidgetController timeWidgetController =
      Get.put(TimeWidgetController());
  var loadingPercentage = 0;
  String url = '${AppDefine.host}/mobile/#/ucenter/guessing?a=1';

  void showWebView() {
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
                        Page resource error:
                          code: ${error.errorCode}
                          description: ${error.description}
                          errorType: ${error.errorType}
                          isForMainFrame: ${error.isForMainFrame}
                                  ''');
          },
          onHttpError: (HttpResponseError error) {
            Future.delayed(Duration(milliseconds: 1000), () {
              // Generate a random number as a string
              timeWidgetController.randomString.value =
                  Random().nextDouble().toString();

              // Call a function to set the value (similar to `setQ` in React Native)
            });

            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            openDialog(request);
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        },
      )
      ..loadRequest(Uri.parse(
          '${AppDefine.host}/mobile/#/ucenter/guessing?a=${timeWidgetController.randomString}'));
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }

    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    NextIssueData item = NextIssueData(
      preDisplayNumber: '1245623456',
      gameType: '3',
      preResult: 'bb',
      preNum: '1',
      hash: '12365471222222222444444489',
      verifUrl: 'aaaaaaaaa0',
    );
    String displayCloseTime = '00 : 50';
    String displayOpenTime = '00 : 40';
    return Stack(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: 200.w,
                height: 40.h,
                child: Text(
                  '${item.preDisplayNumber}期 封盘: ',
                  textAlign: TextAlign.center,
                  style: AppTextStyles.surface_(context, fontSize: 20),
                ),
              ),
              Container(
                width: 70.w,
                height: 25.h,
                margin: EdgeInsets.symmetric(horizontal: 3.w),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(6).w),
                  color: AppColors.ff819EB0,
                ),
                child: Center(
                  child: Text(
                    displayCloseTime,
                    style: AppTextStyles.surface_(context, fontSize: 20),
                  ),
                ),
              ),
              SizedBox(
                width: 100.w,
                height: 40.h,
                child: Text(
                  '开奖: ',
                  textAlign: TextAlign.center,
                  style: AppTextStyles.surface_(context, fontSize: 20),
                ),
              ),
              Container(
                width: 70.w,
                height: 25.h,
                margin: EdgeInsets.symmetric(horizontal: 3.w),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(6).w),
                  color: AppColors.ff819EB0,
                ),
                child: Center(
                  child: Text(
                    displayOpenTime,
                    style: AppTextStyles.surface_(context, fontSize: 16),
                  ),
                ),
              ),
              const Spacer(),
              AppImage.network(
                img_images('kjw_01'),
                width: 30.w,
                height: 30.w,
              ).paddingOnly(right: 10.w).onTap(() => {showWebView()}),
            ],
          ),
          Divider(
            thickness: 0.5.w,
            color: AppColors.surface,
          ),
        ],
      ),
      if (_controller == WebViewController())
        WebViewWidget(controller: _controller),
    ]);
  }

  Future<void> openDialog(HttpAuthRequest httpRequest) async {
    final TextEditingController usernameTextController =
        TextEditingController();
    final TextEditingController passwordTextController =
        TextEditingController();

    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('${httpRequest.host}: ${httpRequest.realm ?? '-'}'),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  decoration: const InputDecoration(labelText: 'Username'),
                  autofocus: true,
                  controller: usernameTextController,
                ),
                TextField(
                  decoration: const InputDecoration(labelText: 'Password'),
                  controller: passwordTextController,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            // Explicitly cancel the request on iOS as the OS does not emit new
            // requests when a previous request is pending.
            TextButton(
              onPressed: () {
                httpRequest.onCancel();
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                httpRequest.onProceed(
                  WebViewCredential(
                    user: usernameTextController.text,
                    password: passwordTextController.text,
                  ),
                );
                Navigator.of(context).pop();
              },
              child: const Text('Authenticate'),
            ),
          ],
        );
      },
    );
  }
}
