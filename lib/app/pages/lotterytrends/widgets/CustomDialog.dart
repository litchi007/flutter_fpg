import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/component/home_menu/enum/ug_link_position_type.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/pages/lottery_trends_view_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_storage/get_storage.dart';

class CustomDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LotteryTrendsViewController controller =
        Get.find<LotteryTrendsViewController>();

    return Obx(() => AnimatedPositioned(
          bottom: controller.isVisible.value ? 0 : 0.6.sw,
          height: 0.6.sh,
          width: 1.sw,
          duration: Duration(milliseconds: 300),
          curve: Curves.easeOut,
          child: Expanded(
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        childAspectRatio: 3),
                    itemCount: 21, // 7 rows * 3 columns
                    itemBuilder: (context, index) {
                      return ElevatedButton(
                        onPressed: () {
                          controller.selectedButton.value = index;
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor:
                              controller.selectedButton.value == index
                                  ? Colors.blue
                                  : Colors.grey,
                          foregroundColor:
                              controller.selectedButton.value == index
                                  ? Colors.white
                                  : Colors.black,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                        ),
                        child: Text('${index + 1}'),
                      );
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.grey,
                            foregroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                          child: Text('Action 1'),
                        ),
                        ElevatedButton(
                          onPressed: () {},
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.grey,
                            foregroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                          child: Text('Action 2'),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
