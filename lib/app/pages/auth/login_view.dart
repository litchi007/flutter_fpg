import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/auth/widgets/InputTextWidget.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_custom_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:gap/gap.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:get_storage/get_storage.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> with WidgetsBindingObserver {
  final AuthController authController = Get.find();
  late TextEditingController _userController =
      TextEditingController(text: authController.rem?.username ?? "");
  late TextEditingController _passwordController =
      TextEditingController(text: authController.rem?.password ?? "");
  final TextEditingController _vcodeController = TextEditingController();
  final scrollController = ScrollController();
  final storage = GetStorage();

  @override
  void initState() {
    super.initState();
    authController.onDataFetch();
    WidgetsBinding.instance.addObserver(this);
    // authController.getSystemConfig();
    authController.getLoginPageRedInfo();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    scrollController.dispose();
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      scrollController.jumpTo(scrollController.position.maxScrollExtent);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.ffbfa46d,
          automaticallyImplyLeading: false,
          titleSpacing: 0,
          leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
                // Get.toNamed(jdsiginpath);
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.ffffffff, size: 18.w),
                Text('返回',
                    style: AppTextStyles.ffffffff(context, fontSize: 18)),
              ]),
            ),
          ),
          title: Text(
            '登录',
            style: AppTextStyles.ffffffff(context,
                fontSize: 22, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          leadingWidth: 0.3.sw,
        ),
        body: Container(
          height: context.mediaQuerySize.height,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(img_mobileTemplate('38', 'login-bg')),
                fit: BoxFit.cover),
          ),
          padding: EdgeInsets.symmetric(horizontal: 38.w),
          child: SingleChildScrollView(
            controller: scrollController,
            child: Column(children: [
              SizedBox(height: 65.h),
              Obx(
                () => AppImage.network(
                    authController.lhcdocLoginRegPageInfo.value.logo ??
                        img_root('upload/f099/customise/images/m_lhc2_m_logo',
                            type: 'jpg'),
                    width: 67.w,
                    height: 104.h,
                    fit: BoxFit.contain),
              ),
              // AppImage.asset('m_lhc2_m_logo.jpg',
              //     width: 67.w, height: 104.h, fit: BoxFit.contain),
              Obx(() => DisplayImage(
                  name: authController.lhcdocLoginRegPageInfo.value.intro)),
              Gap(100.h),
              InputTextWidget(
                leftIconName: "icon-user-w.png",
                //rightIconName: "icon-user-w.png",
                widgetType: 'userid',
                required: true,
                actionDo: false,
                hintext: '请输入会员账号',
                editController: _userController,
              ),
              InputTextWidget(
                leftIconName: "icon-pwd-w.png",
                //rightIconName: "icon-pwd-w.png",
                widgetType: 'password',
                actionDo: false,
                required: true,
                hintext: '请输入密码',
                editController: _passwordController,
              ),

              Obx(
                () => (authController.isShowVCode.value)
                    ? InputTextWidget(
                        widgetType: 'vcode',
                        actionDo: false,
                        required: true,
                        hintext: '图片验证码',
                        editController: _vcodeController,
                      )
                    : const SizedBox(),
              ),
              RemenberPassWidget(),
              GestureDetector(
                onTap: login,
                child: Container(
                  width: 1.sw,
                  height: 49.h,
                  decoration: const BoxDecoration(
                    color: AppColors.ffbfa46d,
                    borderRadius: BorderRadius.all(
                      Radius.circular(22),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        "登录",
                        style: AppTextStyles.surface_(context, fontSize: 17),
                      ),
                    ],
                  ),
                ),
              ),
              BottomWidget()
            ]),
          ),
        ));
  }

  void login({String? fullName}) async {
    if (authController.loginStatus) {
      return;
    }
    if (authController.isCheckedRem.value) {
      authController.onChangeRemember(
          true, _userController.text, _passwordController.text);
    } else {
      authController.onChangeRemember(false, '', '');
    }
    AppToast.show();
    try {
      authController.loginStatus = true;
      NetBaseEntity<UGLoginModel>? res = await authController.login(
          _userController.text, _passwordController.text,
          fullName: fullName, imgCode: _vcodeController.text);

      if (res?.code == 0) {
        if (res?.data?.apiSid != null) {
          GlobalService.to.updateToken(
            UserToken(
              apiSid: res?.data?.apiSid,
              apiToken: res?.data?.apiToken,
            ),
          );
          LogUtil.d("GlobalService.to.token  ${GlobalService.to.token}");
          await GlobalService.to.getUserInfo();
          AppToast.showToast(res?.msg);
          AppNavigator.offAllNamed(homePath);
        }
      } else {
        if (res?.code != 0) {
          AppToast.showToast(res?.msg);

          /// 验证码不能为空!
          if (res?.data?.needVcode == true) {
            authController.handleLoginCode();
          }
          LogUtil.w('res.code: ${res?.code}');
        }
        if (res?.data?.needFullName ?? false) {
          showDialogFullName();
        } else {
          print('${res?.msg} -- ${res?.code}');
          AppToast.showToast(res?.msg);
        }
      }
    } catch (e) {
    } finally {
      authController.loginStatus = false;
    }
  }

  void showDialogFullName() async {
    AppCustomDialog appCustomDialog = AppCustomDialog(
      title: "请输入绑定的真实姓名",
      hintText: "请输入真实姓名",
      textOk: "确定",
    );
    String? fullName = await appCustomDialog.showTextInputDialog(context);
    if (fullName != null) {
      login(fullName: fullName);
    }
  }
}

class BottomWidget extends StatelessWidget {
  BottomWidget({super.key});
  final AuthController authController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10).h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "免费试玩",
            style: AppTextStyles.surface_(context, fontSize: 14),
          ).onTap(() => guestLogin()),
          Gap(60.w),
          Text(
            "马上注册",
            style: AppTextStyles.surface_(context, fontSize: 14),
          ).onTap(() => Get.offAndToNamed(registerPath)),
        ],
      ),
    );
  }

  void guestLogin() async {
    if (authController.loginStatus) {
      return;
    }
    AppToast.show();
    try {
      authController.loginStatus = true;
      UGLoginModel? data = await authController.guestLogin();

      if (data == null) {
        AppToast.showToast('登录失败');
      } else {
        if (data.apiSid != null) {
          AppToast.showToast('登录成功');
          AppNavigator.offAllNamed(homePath);
        } else {
          AppToast.showToast('登录失败');
        }
      }
    } catch (e) {
    } finally {
      authController.loginStatus = false;
      AppToast.dismiss();
    }
  }
}

class RemenberPassWidget extends StatefulWidget {
  RemenberPassWidget({super.key});

  @override
  State<RemenberPassWidget> createState() => _RemenberPassWidgetState();
}

class _RemenberPassWidgetState extends State<RemenberPassWidget> {
  final AuthController authController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15).h,
      child: SizedBox(
        height: 27.5.h,
        child: Row(
          children: [
            Obx(() => Checkbox(
                value: authController.isCheckedRem.value,
                activeColor: AppColors.ff007aff,
                onChanged: (value) {
                  authController.isCheckedRem.value =
                      !authController.isCheckedRem.value;
                })),
            Text(
              '记住密码',
              style: AppTextStyles.ffDDA815_(context, fontSize: 13),
            ),
          ],
        ),
      ),
    );
  }
}

class DisplayImage extends StatelessWidget {
  final String? name;
  const DisplayImage({super.key, this.name});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16).w,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Spacer(),
          SizedBox(
            width: 71.5.w,
            height: 1.h,
            child: const ColoredBox(color: AppColors.ffbfa46d ?? Colors.brown),
          ),
          Text(name ?? 'T001, 创造快乐',
              style: AppTextStyles.ffbfa46d_(context, fontSize: 13)),
          SizedBox(
            width: 71.5.w,
            height: 1.h,
            child: const ColoredBox(color: AppColors.ffbfa46d ?? Colors.brown),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
