import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:get/get.dart';

class InputTextWidget extends StatefulWidget {
  InputTextWidget({
    super.key,
    this.leftIconName,
    required this.widgetType,
    required this.actionDo,
    required this.editController,
    required this.hintext,
    required this.required,
  });

  String? leftIconName;
  final String widgetType;
  final bool required;
  final bool actionDo;
  late TextEditingController editController;
  final String hintext;

  @override
  State<InputTextWidget> createState() => _InputTextWidgetState();
}

class _InputTextWidgetState extends State<InputTextWidget> {
  final AuthController authController = Get.find();

  final textFieldFocusNode = FocusNode();
  bool _obscured = true;
  bool _hasFocus = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 70.h,
        margin: EdgeInsets.only(top: 10.h),
        padding: EdgeInsets.symmetric(vertical: 5.h),
        decoration: BoxDecoration(
            color: AppColors.ffd1cfd0,
            borderRadius: BorderRadius.all(Radius.circular(27.w))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if (widget.leftIconName != null)
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.w),
                child: AppImage.asset(widget.leftIconName ?? "",
                    width: 23.w, height: 23.h, fit: BoxFit.contain),
              ),
            if (widget.widgetType == 'vcode')
              Obx(
                () => Padding(
                  padding: EdgeInsets.only(left: 20.w),
                  child: GestureDetector(
                    onTap: () => authController.getImgCaptcha(),
                    child: authController.imgCaptcha.value != ""
                        ? AppImage.memory(authController.imgCaptcha.value)
                        : SizedBox(
                            width: 50.w,
                            height: 50.w,
                            child: const CircularProgressIndicator(),
                          ),
                  ),
                ),
              ),
            (['text', 'email', 'userid', 'fullname']
                    .contains(widget.widgetType))
                ? Expanded(
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        child: Focus(
                          onFocusChange: (hasFocus) {
                            setState(() {
                              _hasFocus = hasFocus;
                            });
                          },
                          child: TextFormField(
                            controller: widget.editController,
                            focusNode: textFieldFocusNode,
                            cursorColor: AppColors.background,
                            onChanged: (value) {},
                            validator: (value) {
                              if (widget.required) {
                                if (widget.widgetType == 'userid') {
                                  return Validator.validateUserId(value ?? "");
                                } else if (widget.widgetType == 'email') {
                                  return Validator.validateEmail(value ?? "");
                                } else if (widget.widgetType == 'fullname') {
                                  return Validator.validateFullName(
                                      value ?? "");
                                } else {
                                  return Validator.validateRequired(
                                      value ?? "");
                                }
                              }
                              return null;
                            },
                            style:
                                AppTextStyles.surface_(context, fontSize: 20),
                            decoration: InputDecoration(
                                isDense: true, // Reduces height a bit
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 20.w),
                                iconColor: AppColors.surface,
                                filled: true,
                                fillColor: Colors.transparent,
                                hintStyle: AppTextStyles.surface_(context,
                                    fontSize: 20),
                                hintText: widget.hintext,
                                border: const OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                suffixIcon: (GestureDetector(
                                    onTap: () => widget.editController.clear(),
                                    child: _hasFocus
                                        ? Icon(
                                            size: 23.w,
                                            color: AppColors.ff8D8B8B,
                                            Icons.close)
                                        : const SizedBox()))),
                          ),
                        )))
                : (widget.widgetType == 'vcode')
                    ? Expanded(
                        child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10.w),
                            child: Focus(
                              onFocusChange: (hasFocus) {
                                setState(() {
                                  _hasFocus = hasFocus;
                                });
                              },
                              child: TextFormField(
                                controller: widget.editController,
                                focusNode: textFieldFocusNode,
                                cursorColor: AppColors.background,
                                onChanged: (value) {},
                                validator: (value) {
                                  if (widget.required) {
                                    return Validator.validateRequired(
                                        value ?? "");
                                  }
                                  return null;
                                },
                                style: AppTextStyles.surface_(context,
                                    fontSize: 20),
                                decoration: InputDecoration(
                                    isDense: true, // Reduces height a bit
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 20.w),
                                    iconColor: AppColors.surface,
                                    filled: true,
                                    fillColor: Colors.transparent,
                                    hintStyle: AppTextStyles.surface_(context,
                                        fontSize: 20),
                                    hintText: widget.hintext,
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    suffixIcon: (GestureDetector(
                                        onTap: () =>
                                            widget.editController.clear(),
                                        child: _hasFocus
                                            ? Icon(
                                                size: 23.w,
                                                color: AppColors.ff8D8B8B,
                                                Icons.close)
                                            : const SizedBox()))),
                              ),
                            )))
                    : Expanded(
                        child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10).w,
                        child: TextFormField(
                          controller: widget.editController,
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: _obscured,
                          focusNode: textFieldFocusNode,
                          cursorColor: AppColors.background,
                          validator: (value) {
                            if (widget.widgetType == "password") {
                              return Validator.validatePassword(value ?? "");
                            } else {
                              return Validator.validateRequired(value ?? "");
                            }
                          },
                          style: AppTextStyles.surface_(context, fontSize: 20),
                          decoration: InputDecoration(
                              isDense: true, // Reduces height a bit
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 20.w),
                              iconColor: AppColors.surface,
                              filled: true,
                              fillColor: Colors.transparent,
                              hintStyle:
                                  AppTextStyles.surface_(context, fontSize: 20),
                              hintText: widget.hintext,
                              border: const OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              suffixIcon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _obscured = !_obscured;
                                    });
                                  },
                                  child: Icon(
                                    size: 23.w,
                                    color: AppColors.ff8D8B8B,
                                    _obscured
                                        ? Icons.visibility_off_outlined
                                        : Icons.visibility_outlined,
                                  ))),
                        ),
                      ))
          ],
        ));
  }
}

class Validator {
  static String? validateUserId(String value) {
    String min = AppDefine.systemConfig?.userLengthMin ?? "6";
    String max = AppDefine.systemConfig?.userLengthMax ?? "16";
    final RegExp regex = RegExp("^.{$min,$max}\$");
    if (!regex.hasMatch(value)) {
      return '请使用$min-$max位字母或数字的组合';
    } else {
      return null;
    }
  }

  static String? validateEmail(String value) {
    Pattern pattern = r'^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+';
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(value)) {
      return '電子郵件不符';
    } else {
      return null;
    }
  }

  static String? validatePassword(String value) {
    String min = AppDefine.systemConfig?.passLengthMin ?? "6";
    String max = AppDefine.systemConfig?.passLengthMax ?? "16";
    final RegExp regex = RegExp("^.{$min,$max}\$");
    if (!regex.hasMatch(value)) {
      return '请使用$min-$max位字母或数字的组合';
    } else {
      return null;
    }
  }

  static String? validateFullName(String value) {
    if (value.length < 2) {
      return '请使用2个以上字母的组合。';
    } else {
      return null;
    }
  }

  static String? validateRequired(String value) {
    if (value.isEmpty) {
      return '此字段是必需的。';
    } else {
      return null;
    }
  }
}
