import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/pages/auth/widgets/InputTextWidget.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/routes.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({super.key});

  @override
  State<SignUpView> createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  AuthController authController = Get.find();
  final TextEditingController userController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmController = TextEditingController();
  final TextEditingController inviteController = TextEditingController();
  final TextEditingController inviterController = TextEditingController();
  final TextEditingController fundPwdController = TextEditingController();
  final TextEditingController qqController = TextEditingController();
  final TextEditingController wxController = TextEditingController();
  final TextEditingController lineController = TextEditingController();
  final TextEditingController fbController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController smsController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController vcodeController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> signup() async {
    if (_formKey.currentState!.validate()) {
      // AppToast.show();

      await authController
          .register(userController.text, passwordController.text,
              fullName: nameController.text,
              inviteCode: inviteController.text,
              inviter: inviterController.text,
              fundPwd: fundPwdController.text,
              qq: qqController.text,
              wx: wxController.text,
              line: lineController.text,
              fb: fbController.text,
              phone: phoneController.text,
              smsCode: smsController.text,
              email: emailController.text,
              imgCode: vcodeController.text)
          .then((msg) {
        // AppToast.dismiss();
        if (msg == "") {
          AppToast.showToast('注册成功');
          AppNavigator.offAndToNamed(homePath);
        } else {
          AppToast.showError(msg: msg);
        }
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (["1", "2"].contains(AppDefine.systemConfig?.regVcode)) {
      authController.getImgCaptcha();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.ffbfa46d,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        title: Row(children: [
          SizedBox(
            height: 30.h,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15).w,
              child: GestureDetector(
                onTap: () => Navigator.of(context).pop(),
                child: SizedBox(
                  width: 0.15.sw,
                  child: Row(
                    children: [
                      Icon(Icons.arrow_back_ios,
                          color: AppColors.ffffffff, size: 18.w),
                      Text('返回',
                          style: AppTextStyles.ffffffff(context, fontSize: 20))
                    ],
                  ),
                ),
              ),
            ),
          ),
          const Spacer(flex: 50),
          Text('注册',
              style: AppTextStyles.ffffffff(context,
                  fontSize: 22, fontWeight: FontWeight.bold)),
          const Spacer(flex: 50),
          SizedBox(width: 0.2.sw),
        ]),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage("assets/images/login-bg.png"),
                fit: BoxFit.cover),
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 10.h, bottom: 10.h),
                  child: AppImage.asset('m_lhc2_m_logo.jpg',
                      width: 70.w, height: 104.h, fit: BoxFit.fill),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.h),
                  child: Row(
                    children: [
                      const Spacer(),
                      SizedBox(
                        width: 71.5.w,
                        height: 2.h,
                        child: const ColoredBox(color: AppColors.ffbfa46d),
                      ),
                      Gap(5.w),
                      Text('T001, 创造快乐',
                          style: AppTextStyles.ffbfa46d_(context,
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      Gap(5.w),
                      SizedBox(
                        width: 71.5.w,
                        height: 2.h,
                        child: const ColoredBox(color: AppColors.ffbfa46d),
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
                Gap(20.h),
                //invitecode
                if (AppDefine.systemConfig?.inviteCodeSwitch == "1")
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      required: true,
                      actionDo: false,
                      hintext: '*${AppDefine.systemConfig?.inviteWord}',
                      editController: inviteController,
                    ),
                  ),
                //inviter
                if (["1", "2"].contains(AppDefine.systemConfig?.hideReco))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      required: AppDefine.systemConfig?.hideReco == "2",
                      actionDo: false,
                      hintext: '推荐人或上级代理',
                      editController: inviterController,
                    ),
                  ),
                //userid
                Container(
                  margin: EdgeInsets.only(bottom: 20.h),
                  width: 0.85.sw,
                  child: InputTextWidget(
                    widgetType: 'userid',
                    required: true,
                    actionDo: false,
                    hintext: '*请输入会员账号(6-16位字母或数字)',
                    editController: userController,
                  ),
                ),
                //password
                Container(
                  margin: EdgeInsets.only(bottom: 20.h),
                  width: 0.85.sw,
                  child: InputTextWidget(
                    widgetType: 'password',
                    actionDo: false,
                    required: true,
                    hintext: '*请输入密码 (长度不能低于6位)',
                    editController: passwordController,
                  ),
                ),
                //confirm password
                Container(
                  margin: EdgeInsets.only(bottom: 20.h),
                  width: 0.85.sw,
                  child: InputTextWidget(
                    widgetType: 'password',
                    required: true,
                    editController: confirmController,
                    actionDo: false,
                    hintext: '请确认密码',
                  ),
                ),
                if (["1", "2"].contains(AppDefine.systemConfig?.regName))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'fullname',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regName == "2",
                      hintext: '*请输入真实姓名',
                      editController: nameController,
                    ),
                  ),
                //fundpassword
                if (["1", "2"].contains(AppDefine.systemConfig?.regFundpwd))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'fundpwd',
                      required: AppDefine.systemConfig?.regFundpwd == "2",
                      editController: fundPwdController,
                      actionDo: false,
                      hintext: '取款密码',
                    ),
                  ),
                if (["1", "2"].contains(AppDefine.systemConfig?.regQq))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regQq == "2",
                      hintext: 'QQ号',
                      editController: qqController,
                    ),
                  ),
                if (["1", "2"].contains(AppDefine.systemConfig?.regWx))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regWx == "2",
                      hintext: '微信号',
                      editController: wxController,
                    ),
                  ),
                //Line
                if (["1", "2"].contains(AppDefine.systemConfig?.regLine))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regLine == "2",
                      hintext: 'Line账户',
                      editController: lineController,
                    ),
                  ),
                //facebook
                if (["1", "2"].contains(AppDefine.systemConfig?.regFb))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regFb == "2",
                      hintext: 'Facebook账号',
                      editController: fbController,
                    ),
                  ),
                //phonenumber
                if (["1", "2"].contains(AppDefine.systemConfig?.regPhone) ||
                    ["1", "2"].contains(AppDefine.systemConfig?.smsVerify))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regPhone == "2",
                      hintext: '手机号码',
                      editController: phoneController,
                    ),
                  ),
                //email
                if (["1", "2"].contains(AppDefine.systemConfig?.regEmail))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'email',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regEmail == "2",
                      hintext: '电子邮箱',
                      editController: emailController,
                    ),
                  ),
                //smsCode
                if (["1", "2"].contains(AppDefine.systemConfig?.smsVerify))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'text',
                      actionDo: false,
                      required: AppDefine.systemConfig?.smsVerify == "2",
                      hintext: '短信验证码',
                      editController: smsController,
                    ),
                  ),
                //
                if (["1", "2"].contains(AppDefine.systemConfig?.regVcode))
                  Container(
                    margin: EdgeInsets.only(bottom: 20.h),
                    width: 0.85.sw,
                    child: InputTextWidget(
                      widgetType: 'vcode',
                      actionDo: false,
                      required: AppDefine.systemConfig?.regVcode != "0",
                      hintext: '图片验证码',
                      editController: vcodeController,
                    ),
                  ),
                SizedBox(
                    width: 0.85.sw,
                    height: 65.h,
                    child: ElevatedButton(
                      style: const ButtonStyle(
                        backgroundColor:
                            WidgetStatePropertyAll<Color>(AppColors.ffBDA386),
                      ),
                      child: Text('注册',
                          style: AppTextStyles.surface_(context, fontSize: 20)),
                      onPressed: () {
                        RegExp regExpChinese = RegExp(
                          r'[\p{Script=Han}]',
                          unicode: true,
                        );
                        RegExp regExpSpecial = RegExp(
                            r'^(?=.*[0-9])(?=.*[a-zA-Z])[A-Za-z0-9]+$',
                            unicode: true);
                        RegExp regExpSpecialSymbols = RegExp(
                          r'^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#\$%\^&\*—_./,()%~])[a-zA-Z0-9!@#\$%\^&\*—_./,()%~]+$',
                        );
                        final limit = AppDefine.systemConfig?.passLimit;
                        if (nameController.text.isEmpty) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const AppCommonDialog(
                                      okLabel: '确定', msg: '用户名不能为空'));
                        } else if (passwordController.text.isEmpty) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const AppCommonDialog(
                                      okLabel: '确定', msg: '密码不能为空'));
                        } else if (passwordController.text !=
                            confirmController.text) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const AppCommonDialog(
                                      okLabel: '确定', msg: '两次密码不一致'));
                        } else if (passwordController.text.contains(' ')) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const AppCommonDialog(
                                      okLabel: '确定', msg: '密码包含空格'));
                        } else if (regExpChinese
                            .hasMatch(passwordController.text)) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const AppCommonDialog(
                                      okLabel: '确定', msg: '密码包含中文'));
                        } else if (limit == '1' &&
                            !regExpSpecial.hasMatch(passwordController.text)) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const AppCommonDialog(
                                      okLabel: '确定', msg: '密码需包含数字和字母'));
                        } else if (limit == '2' &&
                            !regExpSpecialSymbols
                                .hasMatch(passwordController.text)) {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const AppCommonDialog(
                                      okLabel: '确定', msg: '密码为数字，字母和符号'));
                        } else {
                          signup();
                        }
                      },
                    )),
                Gap(20.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      child: InkWell(
                        onTap: () => AppNavigator.toNamed(loginPath),
                        child: const Text('返回登入',
                            style:
                                TextStyle(fontSize: 14, color: Colors.black)),
                      ),
                    ),
                    Gap(50.w),
                    SizedBox(
                        child: InkWell(
                      onTap: () => guestLogin(),
                      child: const Text('立即试玩',
                          style: TextStyle(fontSize: 14, color: Colors.black)),
                    )),
                  ],
                ),
                Gap(
                  50.h,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void guestLogin() async {
    AppToast.show();
    UGLoginModel? data = await authController.guestLogin();
    AppToast.dismiss();
    if (data == null) {
      AppToast.showToast('登录失败');
    } else {
      if (data.apiSid != null) {
        AppToast.showToast('登录成功');
        AppNavigator.offAllNamed(homePath);
      } else {
        AppToast.showToast('登录失败');
      }
    }
  }
}
