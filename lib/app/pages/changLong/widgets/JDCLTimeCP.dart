import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class JDCLTimeCP extends StatefulWidget {
  final String? serverTime; // Server time (nullable)
  final String? closeTime; // Close time (nullable)

  JDCLTimeCP({this.serverTime, this.closeTime});

  @override
  _JDCLTimeCPState createState() => _JDCLTimeCPState();
}

class _JDCLTimeCPState extends State<JDCLTimeCP> {
  Timer? _timer; // Timer (nullable)
  bool _timeIsOpen = false;
  int _t = 1;

  @override
  void initState() {
    super.initState();
    _startTime();
  }

  @override
  void dispose() {
    _destroyTimer();
    super.dispose();
  }

  void _startTime() {
    if (!_timeIsOpen) {
      print('=========JDCLTimeCP定时器开启======== ========================');
      _timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
        setState(() {
          _timeIsOpen = true;
          _t++;
        });
      });
    }
  }

  void _destroyTimer() {
    _timer?.cancel();
    setState(() {
      _timeIsOpen = false;
    });
  }

  String _reloadText() {
    DateTime now = DateTime.now();

    // Handle nullable serverTime and closeTime
    if (widget.serverTime == null || widget.closeTime == null) {
      return '数据缺失'; // Return an error message if serverTime or closeTime is null
    }

    DateTime serverDateTime =
        DateFormat('yyyy-MM-dd HH:mm:ss').parse(widget.serverTime!);
    DateTime closeDateTime =
        DateFormat('yyyy-MM-dd HH:mm:ss').parse(widget.closeTime!);

    Duration curDatadiff = now.difference(serverDateTime);
    DateTime severNowTime = now.subtract(curDatadiff);

    if (severNowTime.isAfter(closeDateTime)) {
      return '已封盘';
    } else {
      Duration difference = closeDateTime.difference(severNowTime);
      int days = difference.inDays;
      int hours = difference.inHours % 24;
      int minutes = difference.inMinutes % 60;
      int seconds = difference.inSeconds % 60;

      if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0) {
        return '已封盘';
      }

      String dayStr = '$days';
      String hoursStr = hours < 10 ? '0$hours' : '$hours';
      String minutesStr = minutes < 10 ? '0$minutes' : '$minutes';
      String secondsStr = seconds < 10 ? '0$seconds' : '$seconds';

      if (days > 0) {
        return '$dayStr天$hoursStr:$minutesStr:$secondsStr';
      }
      if (hours > 0) {
        return '$hoursStr:$minutesStr:$secondsStr';
      }
      return '$minutesStr:$secondsStr';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Text(
        _reloadText(),
        style: TextStyle(fontSize: 13, color: Colors.red),
      ),
    );
  }
}
