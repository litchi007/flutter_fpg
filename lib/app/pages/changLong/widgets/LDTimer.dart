import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

// Function to format the time remaining
String showTime(int countDown, int diff) {
  int difference = countDown - diff;
  if (difference > 0) {
    final hours = (difference ~/ 3600).toString().padLeft(2, '0');
    final minutes = ((difference ~/ 60) % 60).toString().padLeft(2, '0');
    final seconds = (difference % 60).toString().padLeft(2, '0');
    return hours != '00' ? '$hours:$minutes:$seconds' : '$minutes:$seconds';
  } else {
    return '开奖中';
  }
}

class LDTimer extends StatefulWidget {
  final int closeCountdown;
  final VoidCallback onFinished;

  LDTimer({required this.closeCountdown, required this.onFinished});

  @override
  _LDTimerState createState() => _LDTimerState();
}

class _LDTimerState extends State<LDTimer> {
  int _diff = 0;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _diff++;
      });

      if (widget.closeCountdown - _diff <= 0) {
        widget.onFinished();
        _timer?.cancel();
      }
    });
    TimerManager().addTimer(_timer!);
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text(showTime(widget.closeCountdown, _diff),
            style: AppTextStyles.error_(context, fontSize: 20))
        .paddingOnly(left: 10);
  }
}
