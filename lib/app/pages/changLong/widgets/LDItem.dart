import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/data/models/changLongModel/ChangLongModel.dart';
import 'package:fpg_flutter/app/pages/changlong/pages/longdragon_view_controller.dart';
import 'package:fpg_flutter/app/pages/changlong/widgets/LDTimer.dart';
import 'package:get/get.dart';

// StatelessWidget for LDitem
class LDItem extends StatelessWidget {
  final ChangLongModel? item;
  final int? index;
  final VoidCallback? clearBets;
  final void Function(String gameId)? setSelectedBet;

  LDItem({
    this.item,
    this.index,
    this.clearBets,
    this.setSelectedBet,
  });

  LongdragonViewController controller = Get.find();

  Color getTextColor(BuildContext context) {
    // Replace with your dynamic text color
    // Use MediaQuery or a provider if needed
    return Theme.of(context).brightness == Brightness.dark
        ? Colors.white
        : Colors.black;
  }

  Color getPlayNameBg(String? playName) {
    switch (playName) {
      case '小':
        return Colors.green;
      case '大':
        return Colors.red;
      case '虎':
      case '单':
        return Colors.blue;
      case '龙':
      case '双':
        return Colors.blue;
      default:
        return Colors.blue;
    }
  }

  @override
  Widget build(BuildContext context) {
    final textColor = getTextColor(context);

    return Container(
      margin: EdgeInsets.all(8.0).w,
      padding: EdgeInsets.all(12).w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.0),
        border: Border.all(color: Colors.black.withOpacity(0.1)),
      ),
      child: Row(
        children: <Widget>[
          Container(
            height: 60.0.h,
            width: 60.0.w,
            margin: EdgeInsets.only(right: 15.0),
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.18),
                borderRadius: BorderRadius.circular(34.0),
                image: DecorationImage(
                  image: item != null && item?.logo != null
                      ? NetworkImage(item!.logo!)
                      : NetworkImage(
                          img_web(
                              'images/loading'), // Replace with actual image URL
                        ),
                  fit: BoxFit.cover,
                )),
            alignment: Alignment.center,
            child: null,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                item?.title ?? '',
                style: TextStyle(color: textColor, fontSize: 20.sp),
              ),
              Row(
                children: <Widget>[
                  Text(
                    '${item?.displayNumber ?? ''}期',
                    style: TextStyle(color: textColor, fontSize: 18.sp),
                  ),
                  LDTimer(
                      closeCountdown: item?.closeCountdown ?? 60,
                      onFinished: () => controller.isOpen.value = true)
                  // servertime part
                  //
                  // JDCLTimeCP(
                  //     serverTime: item?.serverTime ?? '00:00',
                  //     closeTime: item?.closeTime ?? '00:00')
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    color: Colors.grey,
                    padding: EdgeInsets.symmetric(horizontal: 10.0).w,
                    child: Text(
                      item?.playCateName ?? '',
                      style: TextStyle(color: Colors.white, fontSize: 16.sp),
                    ),
                  ),
                  Container(
                    color: getPlayNameBg(item?.playName),
                    padding: EdgeInsets.symmetric(horizontal: 10.0).w,
                    margin: EdgeInsets.only(left: 10.0).w,
                    child: Text(
                      item?.playName ?? '',
                      style: TextStyle(color: Colors.white, fontSize: 16.sp),
                    ),
                  ),
                  Container(
                    color: Colors.red,
                    padding: EdgeInsets.symmetric(horizontal: 10.0).w,
                    margin: EdgeInsets.only(left: 10.0).w,
                    child: Text(
                      item?.count ?? '',
                      style: TextStyle(color: Colors.white, fontSize: 16.sp),
                    ),
                  ),
                ],
              ),
            ],
          ),
          const Spacer(),
          Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Obx(() => Row(
                  mainAxisSize: MainAxisSize.min,
                  children: item?.betList?.map((bet) {
                        final isSelected =
                            controller.selectedPlayId.value == bet.playId;
                        controller.selectedGame = item ?? ChangLongModel();
                        controller.selectedBet = bet;
                        return GestureDetector(
                          onTap: () {
                            // controller.testBool.value = true;
                            if (isSelected) {
                              clearBets?.call();
                            } else {
                              if (setSelectedBet != null && item != null) {
                                setSelectedBet!(bet.playId ?? '');
                              }
                            }
                          },
                          child: Container(
                            width: 80.w,
                            height: 80.w,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            padding: EdgeInsets.all(5.0),
                            decoration: BoxDecoration(
                              color: isSelected == true
                                  ? Colors.red
                                  : Colors.white,
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(3.0),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  bet.playName ?? '',
                                  style: TextStyle(
                                      color: isSelected == true
                                          ? Colors.white
                                          : Colors.black,
                                      fontSize: 20.sp),
                                ),
                                Text(
                                  '赔 ${bet.odds}',
                                  style: TextStyle(
                                      color: isSelected == true
                                          ? Colors.white
                                          : Colors.grey,
                                      fontSize: 12.sp),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        );
                      }).toList() ??
                      [],
                )),
          ]),
        ],
      ),
    );
  }
}
