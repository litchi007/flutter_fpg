// Dummy models for demonstration purposes
class UGChanglongaideModel {
  final String? logo;
  final String? title;
  final String? displayNumber;
  final String? playCateName;
  final String? playName;
  final String? count;
  final List<UGBetItemModel>? betList;
  final int? closeCountdown;
  final String? gameId;

  UGChanglongaideModel({
    this.logo,
    this.title,
    this.displayNumber,
    this.playCateName,
    this.playName,
    this.count,
    this.betList,
    this.closeCountdown,
    this.gameId,
  });
}

class UGBetItemModel {
  final String? playId;
  final String? playName;
  final double? odds;

  UGBetItemModel({
    this.playId,
    this.playName,
    this.odds,
  });
}
