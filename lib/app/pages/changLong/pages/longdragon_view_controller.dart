import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:fpg_flutter/data/models/changLongModel/ChangLongModel.dart';
import 'package:fpg_flutter/data/models/changLongModel/LotteryRecordModel';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:intl/intl.dart';

class LongdragonViewController extends GetxController {
  final AppGameRepository _gameRepository = AppGameRepository();
  final AppUserRepository _appUserRepository = AppUserRepository();

  ActionType actionType = ActionType.DEPOSIT;
  RxInt index = 0.obs;

  RxString selectedPlayId = ''.obs;
  Rx<Color> selectedBackgroundColor = Colors.transparent.obs;
  RxBool isIniting = false.obs;
  RxList<ChangLongModel> changLongData = RxList();
  RxList<LotteryRecordModel> lotteryRecordData = RxList();
  RxInt betCount = 0.obs;
  RxString betAmount = ''.obs;
  RxBool testBool = false.obs;
  ChangLongModel selectedGame = ChangLongModel();
  BetItemModel selectedBet = BetItemModel();
  RxBool isOpen = false.obs;

  void setSelectedBet({String? gameId}) {
    selectedPlayId.value = gameId ?? '';
  }

  @override
  void onReady() {
    initController();
    super.onReady();
  }

  void initController() async {
    requestData();
  }

  DateTime? parseDateStringToDateTime(String dateString) {
    try {
      final format = DateFormat("yyyy-MM-dd HH:mm:ss");

      DateTime dateTime = format.parse(dateString);

      return dateTime;
    } catch (e) {
      AppLogger.e('Error parsing date: $e');
      return null;
    }
  }

  void betNow() async {
    if (selectedPlayId.value == '') AppToast.showDuration(msg: "请选择一注号码投注");
    if (betAmount.value == '') AppToast.showDuration(msg: "投注金额不能少于0.1元");
    Map<String, String> betBean = {
      'playId': selectedBet.playId ?? '',
      'playIds': selectedGame.gameId ?? '',
      'odds': selectedBet.odds ?? '',
      'money': double.parse(betAmount.value).toStringAsFixed(2).toString(),
    };
    _appUserRepository
        .userGameBetWithParams(
      token: AppDefine.userToken?.apiSid,
      betBean: [betBean],
      gameId: selectedGame.gameId,
      endTime: parseDateStringToDateTime(
              selectedGame.closeTime ?? "2024-08-09 12:42:55")
          .toString(),
      totalNum: '1',
      tag: '1',
      betIssue: selectedGame.issue,
      totalMoney: double.parse(betAmount.value).toStringAsFixed(2).toString(),
      activeReturnCoinRatio: '0', //拉条的退水
      // isInstant:  selectedGame. isInstant,
      isInstant: '1',
    )
        .then((data) {
      if (data == null) return;
    }).catchError((data) {});
  }

  void requestData() async {
    await _gameRepository.changlong(id: '60').then((data) {
      isIniting.value = false;

      if (data == null) return;
      changLongData.value = data;
    });

    await _gameRepository.getUserRecentBet().then((data) {
      isIniting.value = false;

      if (data == null) return;
      lotteryRecordData.value = data;
    });
  }
}
