import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/app/pages/changlong/pages/longdragon_view_controller.dart';
import 'package:fpg_flutter/app/pages/changlong/const/longdragon_const.dart';
import 'package:fpg_flutter/app/pages/changlong/widgets/LDItem.dart';
import 'package:fpg_flutter/app/pages/account/widgets/activity_reward_dialog.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/theme/app_language.dart';
import 'dart:async';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/app/pages/account/widgets/loading_widget.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

class LongdragonView extends StatefulWidget {
  const LongdragonView({super.key});

  @override
  State<LongdragonView> createState() => _LongdragonViewState();
}

class _LongdragonViewState extends State<LongdragonView>
    with TickerProviderStateMixin {
  late TabController _tabController;
  LongdragonViewController controller = Get.put(LongdragonViewController());
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  final AuthController authController = Get.find();
  TextEditingController betAmountController = TextEditingController();
  int _diff = 0;
  Timer? _timer;

  bool _reload = false;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: categoryTabs.length, vsync: this);
    _tabController.addListener(_handleTabChange);
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.linear,
      ),
    );
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        _diff++;
      });
      TimerManager().addTimer(_timer!);
      if (_diff > 0 && _diff % 30 == 0) {
        _handleReload();
      }
    });
  }

  Future<void> _handleReload() async {
    try {
      controller.requestData();

      _diff = 0;
    } catch (e) {
      // Handle any errors from the reload function
      AppLogger.e('Error during reload: $e');
    }
  }

  void _handleTabChange() {
    if (_tabController.indexIsChanging) {
      controller.actionType = ActionType.values[_tabController.index];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.ff609AC5,
      appBar: AppBar(
        backgroundColor: AppColors.ff609AC5,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        leading: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15).w,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(children: [
                Icon(Icons.arrow_back_ios,
                    color: AppColors.surface, size: 28.w),
              ]),
            )),
        title: Text('长龙助手',
            style: AppTextStyles.surface_(context,
                fontWeight: FontWeight.bold, fontSize: 24)),
        centerTitle: false,
        leadingWidth: 0.3.sw,
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                '${currencyLogo[AppDefine.systemConfig?.currency]}  ${GlobalService.to.userInfo.value?.balance ?? ''}',
                style: AppTextStyles.surface_(context, fontSize: 20),
              ),
              AnimatedBuilder(
                animation: _spinAnimation,
                builder: (context, child) {
                  return Transform.rotate(
                    angle: _spinAnimation.value * 2 * 3.14159,
                    child: Container(
                      width: 24.w,
                      height: 24.w,
                      decoration: const BoxDecoration(
                        color: AppColors.ff8acfb8,
                        shape: BoxShape.circle,
                      ),
                      child: GestureDetector(
                        onTap: () {
                          if (!_reload) {
                            setState(() {
                              _reload = true;
                            });
                            _fetchBalance();
                            _controller.forward(from: 0).then((_) {
                              _controller.reset();
                              setState(() {
                                _reload = false;
                              });
                            });
                          }
                        },
                        child: Center(
                          child: Icon(
                            Icons.refresh,
                            color: AppColors.ff4caf50,
                            size: 20.w,
                          ).onTap(() => controller.requestData()),
                        ),
                      ),
                    ),
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0).w,
                child: Text(
                  '必看说明',
                  style: AppTextStyles.surface_(context, fontSize: 20),
                ).onTap(
                  () => _showApplyDialog(context, htmlstring),
                ),
              )
            ],
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(46.h),
          child: Container(
            color: AppColors.surface,
            child: _createTab(context),
          ),
        ),
      ),
      body: Column(children: [
        Obx(() => (controller.isIniting.value)
            ? const CustomLoadingWidget()
            : Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    _firstTabContent(),
                    _secondTabContent(),
                  ],
                ),
              )),
      ]),
    );
  }

  Widget _firstTabContent() {
    return Stack(alignment: Alignment.topCenter, children: [
      Obx(() => ListView(
          padding: EdgeInsets.only(bottom: 20.0.w),
          children: controller.changLongData
              .map(
                (item) => LDItem(
                  item: item,
                  index: 1,
                  clearBets: () {
                    // inputRef = '';
                    controller.setSelectedBet();
                  },
                  // ${item. gameId}_${index}

                  setSelectedBet: (gameId) =>
                      controller.setSelectedBet(gameId: gameId),
                ),
              )
              .toList())),
      Positioned(bottom: 0.h, child: _buildBottomWidget(context))
    ]);
  }

  TabBar _createTab(BuildContext context) {
    return TabBar(
      controller: _tabController,
      indicatorColor: AppColors.ff0277BD,
      labelColor: AppColors.ff0277BD,
      unselectedLabelColor: AppColors.onBackground,
      indicatorSize: TabBarIndicatorSize.tab,
      physics: const ClampingScrollPhysics(),
      tabAlignment: TabAlignment.fill,
      isScrollable: false,
      labelStyle: AppTextStyles.surface_(context,
          fontWeight: FontWeight.bold, fontSize: 22),
      tabs: List.generate(categoryTabs.length, (index) {
        return Tab(
          text: '${categoryTabs[index]['name']}',
        );
      }),
    );
  }

  Widget _secondTabContent() {
    return Container(
      padding: EdgeInsets.all(20.0.w),
      child: Obx(() => controller.lotteryRecordData.isNotEmpty == true
          ? ListView(
              padding: EdgeInsets.only(bottom: 20.0.w),
              children: controller.lotteryRecordData.map((item) {
                return GestureDetector(
                  onTap: () {
                    // Handle navigation to the detail page
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) => DetailPage(item: item),
                    //   ),
                    // );
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 10.0.w),
                    padding: EdgeInsets.all(10.0.w),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.0.w),
                      border: Border.all(
                        color: Colors.black.withOpacity(0.2),
                        width: 2.0.w,
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '${item.title} ${currencyLogo[AppDefine.systemConfig?.currency ?? ''] ?? ''}${item.money}',
                              style:
                                  AppTextStyles.surface_(context, fontSize: 20),
                            ),
                            SizedBox(height: 10.0.w),
                            Text(
                              '${item.issue} 期',
                              style: AppTextStyles.bodyText2(context,
                                  fontSize: 20),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              item.msg ?? '',
                              style: TextStyle(
                                color: item.status == 0
                                    ? Colors.green
                                    : item.isWin == 0
                                        ? Colors.grey
                                        : Colors.red,
                              ),
                            ),
                            if (item.status == 1 && (item.isWin ?? 0) > 0)
                              SizedBox(height: 10.0.w),
                            if (item.status == 1 && (item.isWin ?? 0) > 0)
                              Text(
                                '+${currencyLogo[AppDefine.systemConfig?.currency ?? ''] ?? ''}${item.bonus}',
                                style:
                                    AppTextStyles.error_(context, fontSize: 20),
                              ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }).toList()
                ..add(
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(weekPath);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 30.0.w),
                      padding: EdgeInsets.symmetric(vertical: 20.0.w),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8.0.w),
                        border: Border(
                          bottom: BorderSide(
                            color: AppColors.ff387ef5,
                            width: 1.0.w,
                          ),
                        ),
                      ),
                      child: const Center(
                        child: Text(
                          '查看更多注单记录',
                          style: TextStyle(
                            color: AppColors.ff387ef5,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
            )
          : const Center(
              child: Text(
                '暂无投注记录',
                style: TextStyle(color: AppColors.ff8D8B8B),
              ),
            )),
    );
  }

  Widget _buildBottomWidget(BuildContext context) {
    return SizedBox(
      width: 1.sw,
      height: 50.h,
      child: Row(
        children: <Widget>[
          Expanded(
              child: Container(
            height: 100.h,
            // padding: EdgeInsets.symmetric(horizontal: 20.0.w),
            decoration: BoxDecoration(
              color: AppColors.ff3E3F3E, // Example color
              borderRadius: BorderRadius.circular(3),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                    width: 60.w,
                    child: ElevatedButton(
                        onPressed: () {
                          // Handle press
                          controller.betAmount.value = '';
                          betAmountController.text = '';
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.transparent,
                          elevation: 0,
                          padding: EdgeInsets.zero,
                        ),
                        child: Center(
                          child: Text('清空',
                              style: AppTextStyles.ffE68131_(context,
                                  fontSize: 25)),
                        ))).paddingOnly(left: 10.w),
                Obx(() => Expanded(
                    flex: 5,
                    child: Center(
                      child: Text(
                        overflow: TextOverflow.visible,
                        softWrap: true,
                        '共${controller.betCount}注 ${controller.betAmount}',
                        style: AppTextStyles.ff387ef5_(context, fontSize: 23),
                      ),
                    ))),
                Expanded(
                  flex: 2,
                  child: TextField(
                    controller: betAmountController,
                    textAlignVertical: TextAlignVertical.center,
                    onChanged: (amount) => controller.betAmount.value = amount,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: '金额',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3),
                        borderSide: const BorderSide(
                          color: AppColors.ff9D9797,
                          width: 1,
                        ),
                      ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 5.0.w),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ],
            ),
          )),
          SizedBox(
            width: 120.w,
            child: ElevatedButton(
              onPressed: () {
                controller.betNow();
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.ff387EF5,
                minimumSize: Size(100.w, 50.h),
                padding: EdgeInsets.zero,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.zero, // Zero border radius
                ),
                elevation: 0,
              ),
              child: Text(
                '马上投注',
                style: AppTextStyles.surface_(context, fontSize: 23),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showApplyDialog(BuildContext context, String? winApplyContent) {
    return ActivityRewardDialog(
            context: context,
            title: '必看说明',
            titleBackgroundColor: AppColors.ff609AC5,
            action: _action(context),
            htmlstring: (winApplyContent ?? ''),
            align: TextAlign.left,
            height: 0.6.sh,
            width: 0.8.sw,
            titleTextStyle: AppTextStyles.surface_(context, fontSize: 28))
        .dialogBuilder();
  }

  Widget _action(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              padding: EdgeInsets.only(top: 8.w),
              height: 50.h,
              width: 0.6.sw,
              decoration: BoxDecoration(
                  border: Border.all(width: 1.sp, color: AppColors.ff999999),
                  borderRadius: BorderRadius.all(Radius.circular(5.w))),
              child: Text('确定',
                  textAlign: TextAlign.center,
                  style: AppTextStyles.ff000000(context, fontSize: 20)),
            ),
          ),
        ],
      ),
    );
  }

  void _fetchBalance() {
    if (!GlobalService.to.isAuthenticated.value) {
      return;
    }
  }
}
