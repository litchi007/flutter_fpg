import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_button.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class AppCommonDialog extends StatelessWidget {
  const AppCommonDialog(
      {super.key,
      required this.okLabel,
      this.onOk,
      this.onCancel,
      this.msg,
      this.cancelLabel,
      this.title,
      this.okButtonColor});

  final VoidCallback? onOk;
  final VoidCallback? onCancel;
  final String? msg;
  final String? title;
  final String okLabel;
  final String? cancelLabel;
  final Color? okButtonColor;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: title != null ? Text(title ?? '') : null,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.w)),
      content: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          msg ?? '',
          textAlign: TextAlign.center,
        )
      ]),
      alignment: Alignment.center,
      contentTextStyle: TextStyle(
          color: Colors.black, fontSize: 20.sp, fontWeight: FontWeight.bold),
      actions: <Widget>[
        if (cancelLabel != null)
          ElevatedButton(
            style: AppButtonStyles.elevatedStyle(
                foregroundColor: Colors.black,
                backgroundColor: AppColors.fff0f0f0,
                fontSize: 18),
            onPressed: () {
              Navigator.pop(context);
              onCancel?.call();
            },
            child: Text(cancelLabel ?? '取消'),
          ),
        ElevatedButton(
          style: AppButtonStyles.elevatedStyle(
              color: Colors.white,
              backgroundColor: okButtonColor ?? AppColors.ff007aff,
              fontSize: 18),
          onPressed: () {
            Navigator.pop(context);
            onOk?.call();
          },
          child: Text(okLabel),
        )
      ],
      actionsAlignment: MainAxisAlignment.spaceEvenly,
    );
  }
}
