import 'package:flutter_easyloading/flutter_easyloading.dart';

class AppToast {
  static Future showToast(String? msg) async {
    return await EasyLoading.showToast(msg ?? "");
  }

  static Future show({String? msg}) async {
    return await EasyLoading.show(status: msg ?? "加载中");
  }

  static Future dismiss() async {
    return await EasyLoading.dismiss();
  }

  static Future showSuccess({String? msg}) async {
    return await EasyLoading.showSuccess(msg ?? "操作成功");
  }

  static Future showError({String? msg}) async {
    return await EasyLoading.showError(msg ?? "操作失败");
  }

  static Future showDuration({String? msg, int? duration}) async {
    EasyLoading.show(status: msg ?? "加载中");
    await Future.delayed(Duration(seconds: duration ?? 3));
    EasyLoading.dismiss();
  }
}
