import 'dart:async';

import 'package:flutter/widgets.dart';

class AppTimer extends StatefulWidget {
  Color? textColor;

  AppTimer({super.key});

  @override
  _AppTimerState createState() => _AppTimerState();
}

class _AppTimerState extends State<AppTimer> {
  late Timer _timer;
  String? _time;

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant AppTimer oldWidget) {
    startTimer();
    super.didUpdateWidget(oldWidget);
  }

  void startTimer() {
    stopTimer();
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        _time = '1';
      });
    });
  }

  void stopTimer() {
    _timer.cancel();
  }

  @override
  void dispose() {
    stopTimer();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      '$_time',
      style: TextStyle(color: widget.textColor, fontSize: 20),
    );
  }
}
