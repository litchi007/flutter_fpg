import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

enum AppButtonType {
  outlined,
  filled,
  opacity,
}

class AppButton extends StatelessWidget {
  const AppButton(
      {super.key,
      required this.label,
      this.isLoading = false,
      required this.onPressed,
      this.color,
      this.height,
      this.buttonType = AppButtonType.filled});

  final String label;
  final bool isLoading;
  final VoidCallback onPressed;
  final Color? color;
  final double? height;
  final AppButtonType? buttonType;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5.w, horizontal: 10.w),
        decoration: BoxDecoration(
          color: buttonType == AppButtonType.outlined ? null : color,
          border: Border.all(color: color ?? const Color(0xffffffff)),
          borderRadius: BorderRadius.circular((height ?? 50.h) / 2),
        ),
        child: Text(label),
      ),
    );
  }
}
