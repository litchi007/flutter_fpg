import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:fpg_flutter/data/models/NoticeMode.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:gap/gap.dart';
import 'package:scrolling_text/scrolling_text.dart';

class AppNoticeWidget extends StatefulWidget {
  AppNoticeWidget({super.key, this.titleList, this.onPressed});
  final List<Scroll>? titleList;
  VoidCallback? onPressed;

  @override
  State<AppNoticeWidget> createState() => _AppNoticeWidgetState();
}

class _AppNoticeWidgetState extends State<AppNoticeWidget> {
  @override
  Widget build(BuildContext context) {
    int index = 0;
    return SizedBox(
      width: double.infinity,
      height: 40.h,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.volume_up,
            size: 20,
          ),
          SizedBox(width: 10.w),
          Expanded(
            child: GestureDetector(
                onTap: () {
                  widget.onPressed?.call();
                  String content = widget.titleList?[index].content ?? "";
                  showContent(content);
                },
                child: ScrollingText(
                  text: widget.titleList?[index].title ?? "",
                  scrollAxis: Axis.horizontal,
                  scrollCurve: Curves.linear,
                  startOffset: 50.0,
                  endOffset: 50.0,
                  onFinish: () {
                    if (index < (widget.titleList ?? []).length) {
                      index++;
                    } else {
                      index = 0;
                    }
                    setState(() {});
                  },
                )),
          ),
        ],
      ),
    );
  }

  void showContent(String content) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.zero,
                side: BorderSide(color: Colors.grey, width: 1.w),
              ),
              child: SizedBox(
                  height: 650.h,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          height: 50.h,
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: Colors.grey, width: 1.w))),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("公告",
                                    style: TextStyle(
                                        color: AppColors.textColor2,
                                        fontSize: 25.sp)),
                              ])),
                      Gap(20.h),
                      Expanded(
                          child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15.w),
                                child: HtmlWidget(content))
                          ],
                        ),
                      )),
                      Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: 10.h, horizontal: 10.w),
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(
                                      color: Colors.grey, width: 1.w))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10.w),
                                    child: OutlinedButton(
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        style: OutlinedButton.styleFrom(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.w)),
                                            minimumSize: Size.fromHeight(45.h)),
                                        child: Text("取消",
                                            style: TextStyle(
                                                color: AppColors.textColor2,
                                                fontSize: 17.sp)))),
                              ),
                              Expanded(
                                  flex: 1,
                                  child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.w),
                                      child: ElevatedButton(
                                          onPressed: () =>
                                              Navigator.of(context).pop(),
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  AppColors.drawMenu,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.w)),
                                              minimumSize:
                                                  Size.fromHeight(45.h)),
                                          child: Text("确定",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 17.sp)))))
                            ],
                          ))
                    ],
                  )));
        });
  }
}
