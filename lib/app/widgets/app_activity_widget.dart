import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';

class AppActivityWidget extends StatefulWidget {
  bool isShow;
  String logo;
  VoidCallback onClose;
  VoidCallback onOpen;

  AppActivityWidget(
      {super.key,
      this.isShow = false,
      this.logo = '',
      required this.onClose,
      required this.onOpen});

  @override
  State<AppActivityWidget> createState() => _AppActivityWidgetState();
}

class _AppActivityWidgetState extends State<AppActivityWidget> {
  @override
  Widget build(BuildContext context) {
    if (!widget.isShow) return const SizedBox();

    return Stack(
      alignment: AlignmentDirectional.topEnd,
      children: [
        GestureDetector(
          onTap: widget.onOpen,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 10.w),
            child: AppImage.network(widget.logo, width: 100.w, height: 100.w),
          ),
        ),
        GestureDetector(
          onTap: widget.onClose,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5.w, horizontal: 5.w),
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
            ),
            child: Icon(
              Icons.close,
              size: 10.w,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
