import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';

class AppCustomDialog {
  AppCustomDialog({
    required this.title,
    required this.hintText,
    required this.textOk,
    this.defaultText,
  });

  String title;
  String hintText; //"输入对应房间密码"
  String textOk; //'匹配密码'
  String? defaultText;

  TextEditingController textEditController = TextEditingController();

  Future<String?> showTextInputDialog(BuildContext context) async {
    textEditController.text = defaultText ?? '';
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return Theme(
            data: ThemeData(dialogBackgroundColor: AppColors.dialog),
            child: Builder(builder: (context) {
              return AlertDialog(
                alignment: Alignment.center,
                actionsAlignment: MainAxisAlignment.center,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.w)),
                title: Center(
                    child: Text(title,
                        style: const TextStyle(color: Colors.white))),
                content: TextField(
                  controller: textEditController,
                  style: AppTextStyles.ff000000(context, fontSize: 24),
                  maxLines: 1,
                  decoration: InputDecoration(
                    hintText: hintText,
                    hintStyle: AppTextStyles.ff5C5869(context, fontSize: 24),
                    filled: true,
                    fillColor: AppColors.onPrimary,
                    contentPadding: EdgeInsets.only(
                      left: 16.w,
                      bottom: 8.w,
                      top: 8.w,
                      right: 16.w,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.w),
                    ),
                  ),
                ),
                actions: <Widget>[
                  ElevatedButton(
                    style: AppButtonStyles.elevatedStyle(
                        foregroundColor: Colors.black,
                        backgroundColor: AppColors.fff0f0f0,
                        fontSize: 18),
                    onPressed: () => Navigator.pop(context),
                    child: const Text('取消'),
                  ),
                  ElevatedButton(
                    style: AppButtonStyles.elevatedStyle(
                        color: Colors.white,
                        backgroundColor: AppColors.ff007aff,
                        fontSize: 18),
                    onPressed: () =>
                        Navigator.pop(context, textEditController.text),
                    child: Text(textOk),
                  )
                ],
              );
            }),
          );
        });
  }
}
