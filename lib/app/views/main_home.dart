import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fpg_flutter/router/router.dart';

class MainHome extends GetView<MainHomeController> {
  const MainHome({super.key});

  @override
  Widget build(BuildContext context) {
    final appThemeColor = Theme.of(context).extension<AppThemeColors>();
    return Scaffold(
      body: Obx(
        () => Center(
          child: controller.getCurrentPage(controller.selectedPath.value),
        ),
      ),
      bottomNavigationBar: Obx(
        () => BottomAppBar(
          padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 0.w),
          // height: 90.w,
          color: appThemeColor?.bottomNavigationBarColor,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ...List.generate(
                      (GlobalService.to.systemConfig.value?.mobileMenu ?? [])
                          .length, (index) {
                    final nav =
                        GlobalService.to.systemConfig.value?.mobileMenu![index];
                    return GestureDetector(
                      onTap: () async {
                        print(nav?.path);
                        if (nav?.path == '/message') {
                          AppNavigator.toNamed(userMessagePath);
                          return;
                        } else if (nav?.path == '/chess') {
                          AppNavigator.toNamed(cardListPath);
                          return;
                        } else if (nav?.path == '/user' &&
                            GlobalService.to.userInfo.value == null) {
                          AppNavigator.toNamed(loginPath);
                          return;
                        } else if (nav?.path == AppRoutes.expertPlan) {
                          if (GlobalService.to.userInfo.value != null) {
                            AppToast.show();
                            var res = await controller.getUserExpertSwitch();
                            AppToast.dismiss();
                            if (res) {
                              AppNavigator.toNamed(AppRoutes.expertPlan);
                            }
                          } else {
                            AppNavigator.toNamed(loginPath);
                          }
                          return;
                        } else if (nav?.path == chatRoomListPath) {
                          AppNavigator.toNamed(chatRoomListPath);
                          return;
                        } else if (nav?.path == "/gameHall" ||
                            nav?.path == "/lotteryList" ||
                            nav?.path == chatRoomListPath) {
                          if (!GlobalService.to.isAuthenticated.value) {
                            AppNavigator.toNamed(loginPath);
                            return;
                          }
                        } else if (nav?.path == "/lotteryRecord") {
                          AppNavigator.toNamed(AppRoutes.lotteryResult);
                          return;
                        } else if (nav?.path == "/changLong") {
                          /// 长龙助手
                          AppNavigator.toNamed(
                              GlobalService.to.userInfo.value != null
                                  ? AppRoutes.longDragonTool
                                  : loginPath);
                          return;
                        }
                        // else if (nav?.path == "/information") {
                        //   return;
                        // }

                        controller.selectedPath.value = nav?.path ?? '';
                      },
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              (nav?.iconLogo == null || nav?.iconLogo == '')
                                  ? Icon(
                                      getIconData(nav?.icon ?? ''),
                                      size: 30.w,
                                      color: controller.selectedPath.value ==
                                              nav?.path
                                          ? appThemeColor?.primary
                                          : AppColors.ff666666,
                                    )
                                  : AppImage.network(
                                      controller.selectedPath.value == nav?.path
                                          ? (nav?.selectedLogo ?? '')
                                          : (nav?.iconLogo ?? ''),
                                      width: 30.w,
                                      height: 30.w,
                                      fit: BoxFit.fill,
                                    ),
                              Text(
                                '${nav?.name}',
                                style: TextStyle(
                                    color: controller.selectedPath.value ==
                                            nav?.path
                                        ? appThemeColor?.primary
                                        : AppColors.ff666666,
                                    fontSize: 22.sp),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                          if (nav?.isHot == "1")
                            Padding(
                              padding: EdgeInsets.only(right: 10.w),
                              child: AppImage.network('${nav?.iconHot}',
                                  width: 24.w, height: 14.w, fit: BoxFit.fill),
                            )
                        ],
                      ),
                    );
                  })
                ]),
          ),
        ),
      ),
    );
  }

  IconData getIconData(String iconName) {
    switch (iconName) {
      case "iconshouye1":
        return FontAwesomeIcons.house;
      case "iconinformation":
        return FontAwesomeIcons.fileLines;
      case "iconcommunity":
        return FontAwesomeIcons.building;
      case "icondownloadApp":
        return FontAwesomeIcons.download;
      case "iconchess":
        return FontAwesomeIcons.map;
      case "iconliaotianshi":
        return FontAwesomeIcons.comment;
      case "iconwode":
        return FontAwesomeIcons.user;
    }
    return FontAwesomeIcons.house;
  }
}
