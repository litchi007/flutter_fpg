import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/widgets/app_general_bar.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/theme/app_text_styles.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

class WebAppPage extends StatefulWidget {
  const WebAppPage({super.key});

  @override
  State<StatefulWidget> createState() => _WebAppPageState();
}

class _WebAppPageState extends State<WebAppPage> {
  String? url;
  String? title;

  InAppWebViewController? webViewController;
  bool isLoading = true;

  double progress = 0; // Track loading progress

  @override
  void initState() {
    super.initState();
    List<String?> args = Get.arguments;
    url = args[0] ?? "";
    title = args[1] ?? "";
  }

  @override
  void dispose() {
    webViewController?.clearCache();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    Uri uri = Uri.parse(url ?? "");
    return Scaffold(
        backgroundColor: appThemeColors?.surface,
        appBar: AppGeneralBar(
          backgroundColor: appThemeColors?.primary,
          leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                padding: EdgeInsets.only(left: 10.w),
                child: Row(children: [
                  Icon(Icons.arrow_back_ios,
                      color: AppColors.ffffffff, size: 18.w),
                  Text('返回',
                      style: AppTextStyles.ffffffff(context, fontSize: 18)),
                ]),
              )),
          titleWidget: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                title ?? "",
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        body: WillPopScope(
            onWillPop: () async {
              // Check if the web view can go back
              if (webViewController != null &&
                  await webViewController!.canGoBack()) {
                webViewController!.goBack();
                return false; // Do not close the app, just navigate back
              }
              return true; // Close the screen if there's no history
            },
            child: Stack(
              alignment: Alignment.center,
              children: [
                InAppWebView(
                  onWebViewCreated: (webViewController) {
                    this.webViewController = webViewController;
                    // webViewController.loadUrl(urlRequest: widget.url);
                  },
                  gestureRecognizers: Set()
                    ..add(
                      Factory<VerticalDragGestureRecognizer>(
                        () => VerticalDragGestureRecognizer(),
                      ),
                    ),
                  // initialSettings: webViewSettings,
                  // initialUrlRequest: URLRequest(url: WebUri(url ?? "")),
                  initialUrlRequest: URLRequest(url: uri),
                  onTitleChanged: (c, title) {
                    /// void
                  },
                  onLoadStart: (controller, url) async {
                    setState(() {
                      isLoading = true;
                    });
                  },
                  onLoadStop: (controller, url) async {
                    setState(() {
                      isLoading = false;
                    });
                    print('Page end loading: $url');
                  },
                  onProgressChanged: (controller, progress) {
                    setState(() {
                      this.progress = progress / 100; // Update loading progress
                    });
                  },
                ),
                if (isLoading)
                  Container(
                    color: AppColors.ffffffff,
                    width: double.infinity,
                    height: double.infinity,
                    child: AppImage.asset('pl.png'),
                  ),
                if (isLoading)
                  LinearProgressIndicator(
                    value: progress,
                    backgroundColor: Colors.transparent,
                    valueColor:
                        const AlwaysStoppedAnimation<Color>(Colors.blue),
                  ),
              ],
            )));
  }
}
