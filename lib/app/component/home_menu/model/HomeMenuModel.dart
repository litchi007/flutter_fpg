import 'package:fpg_flutter/utils/helper.dart';

class HomeMenuModel {
  String? title;
  int? subId;
  String? icon;
  bool? show;
  bool? isHot;
  String? link;

  HomeMenuModel({
    this.icon,
    this.isHot,
    this.show,
    this.subId,
    this.title,
    this.link,
  });

  factory HomeMenuModel.fromJson(Map<String, dynamic> json) {
    return HomeMenuModel(
      title: json['title'],
      subId: convertToInt(json['subId']),
      icon: json['icon'],
      show: json['show'],
      isHot: json['isHot'],
      link: json['link'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'subId': subId,
      'icon': icon,
      'show': show,
      'isHot': isHot,
      'link': link,
    };
  }
}
