import 'package:fpg_flutter/app/component/home_menu/enum/ug_link_position_type.dart';
import 'package:fpg_flutter/app/component/home_menu/model/HomeMenuModel.dart';
import 'package:fpg_flutter/app/component/home_menu/model/LangModel.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:get/get.dart';

class HomeMenuController extends GetxController {
  final AppSystemRepository _appSystemRepository = AppSystemRepository();

  RxBool isRefreshBalance = false.obs;
  RxBool isInit = false.obs;

  // RxList<HomeMenuModel> homeMenus = RxList();

  List<HomeMenuModel> homeMenus = <HomeMenuModel>[
    HomeMenuModel(
        title: '未结算',
        subId: UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(4, 4, 'wjs_r'),
        show: true,
        link: AppRoutes.lotteryLiveBet), // 即时注单
    HomeMenuModel(
        title: '今日已结',
        subId: UGLinkPositionType.jinRiYiJie.value,
        icon: img_mobileTemplateByStaticServer(6, 4, 'yj_r'),
        show: true,
        link: AppRoutes.todaySettled), // 今日已结
    HomeMenuModel(
        title: '长龙助手',
        subId: UGLinkPositionType.changLongZhuShou.value,
        icon: img_mobileTemplateByStaticServer(9, 4, 'cl_r'),
        isHot: true,
        show: true,
        link: AppRoutes.longDragonTool), // 长龙助手
    HomeMenuModel(
        title: '下注记录',
        subId: 5, //UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(10, 4, 'xzjl_r'),
        show: true,
        link: AppRoutes.lotteryTickets), // 下注记录
    HomeMenuModel(
        title: '额度转换',
        subId: UGLinkPositionType.eDuZhuanHuan.value,
        icon: img_mobileTemplateByStaticServer(1, 4, 'edzh_r'),
        show: true,
        link: realtransPath), // 额度转换
    HomeMenuModel(
        title: '开奖结果',
        subId: 0, // UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(2, 4, 'kj_r'),
        show: true,
        link: AppRoutes.lotteryResult), // 开奖结果
    HomeMenuModel(
      title: '开奖走势',
      subId: 0, //UGLinkPositionType.jiShiZhuDan.value,
      icon: img_mobileTemplateByStaticServer(6, 4, 'kj_r'),
      show: false, //siteId === 'f022',
    ), // 开奖走势
    HomeMenuModel(
        title: '开奖网',
        subId: UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(2, 4, 'kj_r'),
        show: false, //siteId === 'f022',
        link: lotteryWebsitePath), // 开奖网
    HomeMenuModel(
        title: '站内信',
        subId: UGLinkPositionType.kaiJiangWang.value,
        icon: img_mobileTemplateByStaticServer(3, 4, 'icon-email'),
        show: true,
        link: userMessagePath), // 站内信
    HomeMenuModel(
        title: '在线客服',
        subId: UGLinkPositionType.zaiXianKeFu.value,
        icon: img_mobileTemplateByStaticServer(2, 4, 'kf_active'),
        show: true,
        link: onlineServicePath), // 在线客服
    HomeMenuModel(
        title: '个人中心',
        subId: 0, //UGLinkPositionType.会员中心,
        icon: img_mobileTemplateByStaticServer(3, 4, 'wd_active'),
        show: true,
        link: '/user'), // 会员中心
    HomeMenuModel(
        title: '退出登录',
        subId: UGLinkPositionType.tuiChuDengLu.value,
        icon: img_mobileTemplateByStaticServer(4, 4, 'tc_r'),
        show: true,
        link: 'logout'), // 退出登录
  ].obs;

  List<LangModel> langs = <LangModel>[
    LangModel('中文(简体)', 'zh-cn'),
    LangModel('中文(繁體)', 'zh-tw'),
    LangModel('English', 'en'),
    LangModel('Tiếng Việt', 'vi'),
    LangModel('Italia', 'it'),
    LangModel('España', 'es'),
    LangModel('日本語', 'ja'),
    LangModel('Bahasa Melayu', 'ms'),
    LangModel('ไทย', 'th'),
    LangModel('हिन्दी', 'hi'),
    LangModel('bahasa Indonesia', 'id'),
    LangModel('한국어', 'ko'),
    LangModel('português', 'pt'),
    LangModel('កម្ពុជា។', 'km'),
  ];

  RxString selectedLang = ''.obs;

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    selectedLang.value = langs[0].code;
    getRightMenus();
  }

  Future refreshBalance() async {
    isRefreshBalance.value = true;
    // _appUserRepository.
  }

  Future<void> getRightMenus() async {
    await _appSystemRepository.getMobileRightMenu().then((data) {
      // homeMenus.value = data??defaultMenus;
    });
  }
}
