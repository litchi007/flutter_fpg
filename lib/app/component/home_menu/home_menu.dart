import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/home_menu/home_menu_controller.dart';
import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/app_version.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/helper.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';

class HomeMenuWidget extends StatefulWidget {
  const HomeMenuWidget({super.key});

  @override
  _HomeMenuWidgetState createState() => _HomeMenuWidgetState();
}

class _HomeMenuWidgetState extends State<HomeMenuWidget>
    with SingleTickerProviderStateMixin {
  final HomeMenuController controller = Get.put(HomeMenuController());
  final AuthController _authController = Get.find();
  final MainHomeController _mainHomeController = Get.find();
  late AnimationController _controller;
  late Animation<double> _spinAnimation;
  bool _reload = false;
  final GlobalService globalService = Get.find();

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 3000),
    );
    _spinAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.linear,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    RxDouble balance =
        (((convertToDouble(GlobalService.to.userInfo.value?.balance ?? 0) ??
                            0) *
                        100)
                    .round() /
                100)
            .obs;
    return ListView(
      // Important: Remove any padding from the ListView.
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          decoration: const BoxDecoration(
            color: AppColors.drawMenu,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 30.h,
              ),
              Text(AppDefine.systemConfig?.webName ?? '',
                  style: TextStyle(color: Colors.white)),
              if (GlobalService.to.isAuthenticated.value)
                Text(GlobalService.to.userInfo.value?.usr ?? '',
                    style: const TextStyle(color: Colors.white)),
              const Divider(),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        const Text('余额：',
                            style: TextStyle(color: Colors.white)),
                        Text(
                            '${currencyLogo[AppDefine.systemConfig?.currency ?? ''] ?? ''} $balance',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                    AnimatedBuilder(
                      animation: _spinAnimation,
                      builder: (context, child) {
                        return Transform.rotate(
                          angle: _spinAnimation.value * 10 * 3.14159,
                          child: GestureDetector(
                            onTap: () {
                              if (!_reload) {
                                setState(() {
                                  _reload = true;
                                });

                                _controller.forward(from: 0).then((_) {
                                  _controller.reset();
                                  setState(() {
                                    _reload = false;
                                  });
                                });
                                globalService.getUserInfo();
                              }
                            },
                            child: AppImage.network(
                              img_images('kj_refresh'),
                              width: 20.w,
                              height: 20.w,
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          // padding: EdgeInsets.symmetric(vertical: 5.w),
          color: Color.fromARGB(213, 255, 255, 255),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                onPressed: () {
                  if (!GlobalService.isShowTestToast) {
                    AppNavigator.toNamed(depositPath);
                  }
                },
                style: AppButtonStyles.elevatedStyle(
                    backgroundColor: AppColors.drawMenu,
                    foregroundColor: Colors.white,
                    padding: EdgeInsets.only(top: 12.h, bottom: 12.h),
                    radius: 5.w,
                    fontSize: 18),
                child: Row(children: [
                  AppImage.asset('chonngzhi.png', width: 30.w, height: 30.w),
                  SizedBox(width: 5.w),
                  const Text('充值')
                ]),
              ),
              ElevatedButton(
                onPressed: () {
                  if (!GlobalService.isShowTestToast) {
                    AppNavigator.toNamed(depositPath, arguments: [1]);
                  }
                },
                style: AppButtonStyles.elevatedStyle(
                    backgroundColor: AppColors.drawMenu,
                    foregroundColor: Colors.white,
                    radius: 5.w,
                    padding: EdgeInsets.only(top: 12.h, bottom: 12.h),
                    fontSize: 18),
                child: Row(children: [
                  AppImage.asset('tixian.png', width: 30.w, height: 30.w),
                  SizedBox(width: 5.w),
                  const Text('提现')
                ]),
              ),
            ],
          ),
        ),
        Gap(10.w),
        ...List.generate(controller.homeMenus.length, (index) {
          final item = controller.homeMenus[index];
          if (!(item.show ?? false)) return const SizedBox();
          return InkWell(
              onTap: () {
                if (item.link == 'logout') {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) => AppCommonDialog(
                          onOk: _authController.logout,
                          okLabel: '确定',
                          msg: '确认要退出该帐号？',
                          cancelLabel: '取消'));
                  return;
                }
                if (item.link == userPath) {
                  _mainHomeController.selectedPath.value = item.link ?? '';
                  return;
                }

                LogUtil.w(item.link);

                /// 额度转换,下注记录 试玩账号进入了;
                if (item.link == realtransPath ||
                    item.link == AppRoutes.lotteryTickets) {
                  if (!GlobalService.isShowTestToast) {
                    AppNavigator.toNamed(item.link ?? '');
                  }
                } else {
                  AppNavigator.toNamed(item.link ?? '');
                }
              },
              child: Stack(
                alignment: AlignmentDirectional.topEnd,
                children: [
                  Column(
                    children: [
                      Container(
                        // color: Colors.amber,
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        child: Row(
                          children: [
                            AppImage.network(item.icon ?? '',
                                width: 30.w, height: 30.w),
                            SizedBox(
                              width: 10.w,
                            ),
                            Text(item.title ?? ''),
                            const Spacer(),
                            if (!(item.isHot ?? false))
                              Icon(
                                Icons.arrow_forward_ios,
                                size: 30.w,
                              )
                          ],
                        ),
                      ),
                      Divider(
                        indent: 50.w,
                      ),
                    ],
                  ),
                  if (item.isHot ?? false)
                    Padding(
                      padding: EdgeInsets.only(right: 10.w),
                      child: AppImage.network(img_images('hot2x'),
                          width: 40.w, height: 40.w),
                    )
                ],
              ));
        }),
        Text("""
         ${GlobalService.to.packageInfo.value.version}+${GlobalService.to.versionLast.value}
         ${Platform.isAndroid ? AppVersion.andridVersion : AppVersion.iosVersion}
        """),

        // DropdownButtonHideUnderline(
        //   child: DropdownButton2<String>(
        //     isExpanded: true,
        //     items: controller.langs.map((LangModel item) => DropdownMenuItem<String>(
        //       value: item.code,
        //       child: Text(item.label, style: AppTextStyles.ff000000(context, fontSize: 17),
        //                 overflow: TextOverflow.ellipsis),
        //       )).toList(),
        //     value: controller.selectedLang.value,
        //     onChanged: (String? value) {
        //       setState(()=>controller.selectedLang.value = value!);
        //     },
        //     buttonStyleData: ButtonStyleData(
        //       height: 50.h,
        //       width: 100.w,
        //       padding: EdgeInsets.only(left: 10.w, right: 10.w),
        //       elevation: 2,
        //     ),
        //     iconStyleData: const IconStyleData(
        //       icon: Icon(CustomIcons.chevron_down),
        //       iconSize: 14,
        //       iconEnabledColor: Colors.grey,
        //       openMenuIcon: Icon(CustomIcons.chevron_up)
        //     ),
        //     dropdownStyleData: DropdownStyleData(
        //       maxHeight: 200,
        //       width: 100.w,
        //       decoration: BoxDecoration(
        //         border: Border.all(width: 2, color: Colors.grey),
        //         borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10.w), bottomRight: Radius.circular(10.w))
        //       ),
        //       // offset: const Offset(-20, 0),
        //     ),
        //     menuItemStyleData: MenuItemStyleData(
        //       height: 35.h,
        //       padding: EdgeInsets.only(left: 10.w, right: 10.w),
        //       selectedMenuItemBuilder: (context, child) {
        //         return Container(color: AppColors.dialog, child: child);
        //       },
        //     ),
        //   ),
        // ),
        // const Row(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: [Text('1.0.0:24062722')],
        // )
      ],
    );
  }
}
