// ug_link_position_type.dart
enum UGLinkPositionType {
  ziJinGuanLi, // 资金管理
  appXiaZai, // APP下载
  liaoTianShi, // 聊天室
  zaiXianKeFu, // 在线客服
  zaiXianKeFu1, // 在线客服1
  changLongZhuShou, // 长龙助手
  tuiGuangShouYi, // 推广收益
  kaiJiangWang, // 开奖网
  liXiBao, // 利息宝
  youHuiHuoDong, // 优惠活动
  zhuDanJiLu, // 注单记录
  qqKeFu, // QQ客服
  weiXinKeFu, // 微信客服
  renWuDaTing, // 任务大厅
  zhanNeiXin, // 站内信
  qianDao, // 签到
  touSuZhongXin, // 投诉中心
  quanMinJingCai, // 全民竞猜
  huoDongCaiJin, // 活动彩金
  youXiDaTing, // 游戏大厅
  huiYuanZhongXin, // 会员中心
  chongZhi, // 充值
  tiXian, // 提现
  eDuZhuanHuan, // 额度转换
  jiShiZhuDan, // 即时注单
  jinRiShuYing, // 今日输赢
  kaiJiangJiLu, // 开奖记录
  dangQianBanBenHao, // 当前版本号
  ziJinMingXi, // 资金明细
  huiDaoDianNaoBan, // 回到电脑版
  fanHuiShouYe, // 返回首页
  tuiChuDengLu, // 退出登录
  touZhuJiLu, // 投注记录
  youXiWanFa, // 游戏玩法
  caiZhongGuiZe, // 彩种规则
  hongBaoJiLu, // 红包记录
  saoLeiJiLu, // 扫雷记录
  xiuGaiMiMa, // 修改密码
  xiuGaiTiKuanMiMa, // 修改提款密码
  hongBaoHuoDong, // 红包活动
  shiWan, // 试玩
  zhenRenDaTing, // 真人大厅
  qiPaiDaTing, // 棋牌大厅
  qiPaiDianZi, // 棋牌电子
  dianZiDaTing, // 电子大厅
  tiYuDaTing, // 体育大厅
  dianJingDaTing, // 电竞大厅
  caiPiaoDaTing, // 彩票大厅
  buYuDaTing, // 捕鱼大厅
  daZhuanPan, // 大转盘
  kaiJiangZouShi, // 开奖走势
  luZhu, // 路珠
  qiangDan, // 抢单
  liuHeZhuShou, // 六合助手
  duoBaoHuoDong, // 夺宝活动
  guanFangKaiJiangWang, // 官方开奖网
  yinDuChanPinYe, // 印度产品页
  anQuanZhongXin, // 安全中心
  tuiJian, // 推荐
  yinHangKaGuanLi, // 银行卡管理
  sheQu, // 社区
  ybo, // ybo
  zhuanJiaGenDan, // 专家跟单
  jinRiYiJie, // 今日已结
  liangMianChangLong, // 两面长龙
}

extension UGLinkPositionTypeExtension on UGLinkPositionType {
  String get chineseName {
    switch (this) {
      case UGLinkPositionType.ziJinGuanLi:
        return '资金管理';
      case UGLinkPositionType.appXiaZai:
        return 'APP下载';
      case UGLinkPositionType.liaoTianShi:
        return '聊天室';
      case UGLinkPositionType.zaiXianKeFu:
      case UGLinkPositionType.zaiXianKeFu1:
        return '在线客服';
      case UGLinkPositionType.changLongZhuShou:
        return '长龙助手';
      case UGLinkPositionType.tuiGuangShouYi:
        return '推广收益';
      case UGLinkPositionType.kaiJiangWang:
        return '开奖网';
      case UGLinkPositionType.liXiBao:
        return '利息宝';
      case UGLinkPositionType.youHuiHuoDong:
        return '优惠活动';
      case UGLinkPositionType.zhuDanJiLu:
        return '注单记录';
      case UGLinkPositionType.qqKeFu:
        return 'QQ客服';
      case UGLinkPositionType.weiXinKeFu:
        return '微信客服';
      case UGLinkPositionType.renWuDaTing:
        return '任务大厅';
      case UGLinkPositionType.zhanNeiXin:
        return '站内信';
      case UGLinkPositionType.qianDao:
        return '签到';
      case UGLinkPositionType.touSuZhongXin:
        return '投诉中心';
      case UGLinkPositionType.quanMinJingCai:
        return '全民竞猜';
      case UGLinkPositionType.huoDongCaiJin:
        return '活动彩金';
      case UGLinkPositionType.youXiDaTing:
        return '游戏大厅';
      case UGLinkPositionType.huiYuanZhongXin:
        return '会员中心';
      case UGLinkPositionType.chongZhi:
        return '充值';
      case UGLinkPositionType.tiXian:
        return '提现';
      case UGLinkPositionType.eDuZhuanHuan:
        return '额度转换';
      case UGLinkPositionType.jiShiZhuDan:
        return '即时注单';
      case UGLinkPositionType.jinRiShuYing:
        return '今日输赢';
      case UGLinkPositionType.kaiJiangJiLu:
        return '开奖记录';
      case UGLinkPositionType.dangQianBanBenHao:
        return '当前版本号';
      case UGLinkPositionType.ziJinMingXi:
        return '资金明细';
      case UGLinkPositionType.huiDaoDianNaoBan:
        return '回到电脑版';
      case UGLinkPositionType.fanHuiShouYe:
        return '返回首页';
      case UGLinkPositionType.tuiChuDengLu:
        return '退出登录';
      case UGLinkPositionType.touZhuJiLu:
        return '投注记录';
      case UGLinkPositionType.youXiWanFa:
      case UGLinkPositionType.caiZhongGuiZe:
        return '彩种规则';
      case UGLinkPositionType.hongBaoJiLu:
        return '红包记录';
      case UGLinkPositionType.saoLeiJiLu:
        return '扫雷记录';
      case UGLinkPositionType.xiuGaiMiMa:
        return '修改密码';
      case UGLinkPositionType.xiuGaiTiKuanMiMa:
        return '修改提款密码';
      case UGLinkPositionType.hongBaoHuoDong:
        return '红包活动';
      case UGLinkPositionType.shiWan:
        return '试玩';
      case UGLinkPositionType.zhenRenDaTing:
        return '真人大厅';
      case UGLinkPositionType.qiPaiDaTing:
      case UGLinkPositionType.qiPaiDianZi:
        return '棋牌大厅';
      case UGLinkPositionType.dianZiDaTing:
        return '电子大厅';
      case UGLinkPositionType.tiYuDaTing:
        return '体育大厅';
      case UGLinkPositionType.dianJingDaTing:
        return '电竞大厅';
      case UGLinkPositionType.caiPiaoDaTing:
        return '彩票大厅';
      case UGLinkPositionType.buYuDaTing:
        return '捕鱼大厅';
      case UGLinkPositionType.daZhuanPan:
        return '大转盘';
      case UGLinkPositionType.kaiJiangZouShi:
        return '开奖走势';
      case UGLinkPositionType.luZhu:
        return '路珠';
      case UGLinkPositionType.qiangDan:
        return '抢单';
      case UGLinkPositionType.liuHeZhuShou:
        return '六合助手';
      case UGLinkPositionType.duoBaoHuoDong:
        return '夺宝活动';
      case UGLinkPositionType.guanFangKaiJiangWang:
        return '官方开奖网';
      case UGLinkPositionType.yinDuChanPinYe:
        return '印度产品页';
      case UGLinkPositionType.anQuanZhongXin:
        return '安全中心';
      case UGLinkPositionType.tuiJian:
        return '推荐';
      case UGLinkPositionType.yinHangKaGuanLi:
        return '银行卡管理';
      case UGLinkPositionType.sheQu:
        return '社区';
      case UGLinkPositionType.ybo:
        return 'ybo';
      case UGLinkPositionType.zhuanJiaGenDan:
        return '专家跟单';
      case UGLinkPositionType.jinRiYiJie:
        return '今日已结';
      case UGLinkPositionType.liangMianChangLong:
        return '两面长龙';
      default:
        return '';
    }
  }

  int get value {
    switch (this) {
      case UGLinkPositionType.ziJinGuanLi:
        return 1;
      case UGLinkPositionType.appXiaZai:
        return 2;
      case UGLinkPositionType.liaoTianShi:
        return 3;
      case UGLinkPositionType.zaiXianKeFu:
      case UGLinkPositionType.zaiXianKeFu1:
        return 4;
      case UGLinkPositionType.changLongZhuShou:
        return 5;
      case UGLinkPositionType.tuiGuangShouYi:
        return 6;
      case UGLinkPositionType.kaiJiangWang:
        return 7;
      case UGLinkPositionType.liXiBao:
        return 8;
      case UGLinkPositionType.youHuiHuoDong:
        return 9;
      case UGLinkPositionType.zhuDanJiLu:
        return 10;
      case UGLinkPositionType.qqKeFu:
        return 11;
      case UGLinkPositionType.weiXinKeFu:
        return 12;
      case UGLinkPositionType.renWuDaTing:
        return 13;
      case UGLinkPositionType.zhanNeiXin:
        return 14;
      case UGLinkPositionType.qianDao:
        return 15;
      case UGLinkPositionType.touSuZhongXin:
        return 16;
      case UGLinkPositionType.quanMinJingCai:
        return 17;
      case UGLinkPositionType.huoDongCaiJin:
        return 18;
      case UGLinkPositionType.youXiDaTing:
        return 19;
      case UGLinkPositionType.huiYuanZhongXin:
        return 20;
      case UGLinkPositionType.chongZhi:
        return 21;
      case UGLinkPositionType.tiXian:
        return 22;
      case UGLinkPositionType.eDuZhuanHuan:
        return 23;
      case UGLinkPositionType.jiShiZhuDan:
        return 24;
      case UGLinkPositionType.jinRiShuYing:
        return 25;
      case UGLinkPositionType.kaiJiangJiLu:
        return 26;
      case UGLinkPositionType.dangQianBanBenHao:
        return 27;
      case UGLinkPositionType.ziJinMingXi:
        return 28;
      case UGLinkPositionType.huiDaoDianNaoBan:
        return 29;
      case UGLinkPositionType.fanHuiShouYe:
        return 30;
      case UGLinkPositionType.tuiChuDengLu:
        return 31;
      case UGLinkPositionType.touZhuJiLu:
        return 32;
      case UGLinkPositionType.youXiWanFa:
      case UGLinkPositionType.caiZhongGuiZe:
        return 33;
      case UGLinkPositionType.hongBaoJiLu:
        return 36;
      case UGLinkPositionType.saoLeiJiLu:
        return 37;
      case UGLinkPositionType.xiuGaiMiMa:
        return 38;
      case UGLinkPositionType.xiuGaiTiKuanMiMa:
        return 39;
      case UGLinkPositionType.hongBaoHuoDong:
        return 40;
      case UGLinkPositionType.shiWan:
        return 41;
      case UGLinkPositionType.zhenRenDaTing:
        return 42;
      case UGLinkPositionType.qiPaiDaTing:
      case UGLinkPositionType.qiPaiDianZi:
        return 43;
      case UGLinkPositionType.dianZiDaTing:
        return 44;
      case UGLinkPositionType.tiYuDaTing:
        return 45;
      case UGLinkPositionType.dianJingDaTing:
        return 46;
      case UGLinkPositionType.caiPiaoDaTing:
        return 47;
      case UGLinkPositionType.buYuDaTing:
        return 48;
      case UGLinkPositionType.daZhuanPan:
        return 51;
      case UGLinkPositionType.kaiJiangZouShi:
        return 54;
      case UGLinkPositionType.luZhu:
        return 55;
      case UGLinkPositionType.qiangDan:
        return 59;
      case UGLinkPositionType.liuHeZhuShou:
        return 60;
      case UGLinkPositionType.duoBaoHuoDong:
        return 70;
      case UGLinkPositionType.guanFangKaiJiangWang:
        return 100;
      case UGLinkPositionType.yinDuChanPinYe:
        return 101;
      case UGLinkPositionType.anQuanZhongXin:
        return 102;
      case UGLinkPositionType.tuiJian:
        return 103;
      case UGLinkPositionType.yinHangKaGuanLi:
        return 104;
      case UGLinkPositionType.sheQu:
        return 105;
      case UGLinkPositionType.ybo:
        return 106;
      case UGLinkPositionType.zhuanJiaGenDan:
        return 107;
      case UGLinkPositionType.jinRiYiJie:
        return 108;
      case UGLinkPositionType.liangMianChangLong:
        return 109;
      default:
        return 0;
    }
  }
}
