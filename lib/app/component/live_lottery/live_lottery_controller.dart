import 'package:fpg_flutter/app/component/live_lottery/LotteryNum.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/data/models/GameCategory.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/data/repositories/app_game_repository.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:get/get.dart';

class LiveLotteryController extends GetxController {
  final AppGameRepository _appGameRepository = AppGameRepository();
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();

  RxList<TabItem> lotteryTabs = [
    TabItem(
      label: '澳门六合彩',
      name: 'ugamlhc1',
      gameId: '235',
    ),
    TabItem(
      label: '香港六合彩',
      name: 'lhc',
      gameId: '70',
    ),
    // TabItem(label: '新澳门六合彩', name: 'ugamlhc2', gameId: '13'),
  ].obs;

  List<String> lotterTabImages = [
    img_mobileTemplate('38', 'icon_lhc_left'),
    img_mobileTemplate('38', 'icon_lhc_right'),
  ];

  List<Game> lotteryGames = [];
  Rx<LotteryNumberData> lotteryNumber = LotteryNumberData().obs;
  RxList<LotteryNum> showlotterys = RxList();
  RxInt lotteryTabIndex = 0.obs;
  RxInt lotteryCloseTime = 0.obs;

  @override
  void onReady() {
    _appGameRepository.homeRecommend().then((data) {
      if (data == null) return;
      lotteryGames = data
              .firstWhere((v) => v.category == 'lottery',
                  orElse: () => GameCategory())
              .games ??
          [];
      updateTabsWithGameIds(lotteryGames);
      requestLHN(lotteryTabs[0].gameId ?? '0', lotteryTabs[0].name ?? '');
    });

    super.onReady();
  }

  void updateTabsWithGameIds(List<Game> lotteryGames) {
    if (lotteryTabs.isNotEmpty && lotteryGames.isNotEmpty) {
      for (var tab in lotteryTabs) {
        var targetGame = lotteryGames.firstWhere(
          (game) => game.name == tab.name,
          orElse: () => Game(),
        );
        if (targetGame.id != null) {
          tab.gameId = targetGame.id;
        }
      }
    }
  }

  Future requestLHN(String gameId, String gameName) async {
    await _appLHCDocRepository
        .lotteryNumber(gameId: int.parse(gameId), gameName: gameName)
        .then((data) {
      if (data != null) {
        lotteryNumber.value = data;

        try {
          lotteryCloseTime.value = DateUtil.getCloseTimes(data.endtime ?? "0");
          List<String> lotteryNumbers = data.numbers?.split(',') ?? [];
          List<String> numColors = data.numColor?.split(',') ?? [];
          List<String> numSxs = data.numSx?.split(',') ?? [];
          List<LotteryNum> nums = lotteryNumbers.asMap().entries.map((entry) {
            int index = entry.key;
            String item = entry.value;
            return LotteryNum(
              number: item,
              color: numColors[index],
              sx: numSxs[index],
            );
          }).toList();
          showlotterys.value = nums;

          // countdown =
          //     CountdownTimerManager.addTimer(TimerKeys.diff) as CountdownTimer;
          // countdown.callback = (remainingTime) {
          //   diff.value = remainingTime <= 0 ? 0 : remainingTime;
          // };
          // countdown.start(dif);
        } catch (e) {
          AppLogger.e('requestLHN calculator error', error: e);
        }
      }
    });
  }

  var diff = 0.obs;
  var resetting = false.obs;
}
