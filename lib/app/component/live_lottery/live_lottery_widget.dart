import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:fpg_flutter/app/component/live_lottery/live_lottery_controller.dart';
import 'package:fpg_flutter/app/component/live_lottery/lottery_ball.dart';
import 'package:fpg_flutter/app/component/lottery_scratch_card.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/game_center_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/new_block_widget/new_block_controller.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';

class LiveLotteryWidget extends StatefulWidget {
  const LiveLotteryWidget({super.key});

  @override
  State<StatefulWidget> createState() => _LiveLotteryWidget();
}

class _LiveLotteryWidget extends State<LiveLotteryWidget> {
  late LiveLotteryController controller = Get.find();
  final GameCenterController pcontroller = Get.find();
  final NewsBlockController ncontroller = Get.find();
  final isEnable = ValueNotifier<bool>(
      false); // (AppDefine.systemConfig?.lhcdocMiCard ?? false)
  final RxBool _isLastLotteyCheck = false.obs;

  Timer? _timer;
  bool _isTimerRunning = false;

  void startTimer() {
    if (_isTimerRunning) return;

    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      controller.lotteryCloseTime.value--;
      if (controller.lotteryCloseTime.value < 0) {
        controller.lotteryCloseTime.value = 0;
      }
    });
    TimerManager().addTimer(_timer!);

    setState(() {
      _isTimerRunning = true;
    });
  }

  @override
  void initState() {
    super.initState();
    isEnable.addListener(() {
      _isLastLotteyCheck.value = isEnable.value;
    });
    startTimer();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          // height: 200.w,
          width: double.infinity,
          child: Stack(
            children: [
              Obx(
                () => AppImage.network(
                    controller
                        .lotterTabImages[pcontroller.lotteryTabIndex1.value],
                    fit: BoxFit.fill,
                    width: double.infinity),
              ),
              SizedBox(
                height: 40.w,
                child: Obx(() => Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ...List.generate(controller.lotteryTabs.length,
                            (index) {
                          final lotteryTab = controller.lotteryTabs[index];
                          return Expanded(
                            child: GestureDetector(
                              onTap: () {
                                pcontroller.lotteryTabIndex1.value = index;
                                int idx = ncontroller.lotteryForumTabs
                                    .indexWhere((item) =>
                                        item.name == lotteryTab.label);
                                if (idx > -1) {
                                  pcontroller.lotteryTabIndex2.value = idx;
                                  final TabItem fItem =
                                      ncontroller.lotteryForumTabs[idx];
                                  pcontroller.getContentList(fItem.alias, 1);
                                }
                                controller.requestLHN(lotteryTab.gameId ?? '0',
                                    lotteryTab.name ?? '');
                              },
                              child: Center(
                                child: Text(
                                  '${lotteryTab.label}',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 20.sp),
                                ),
                              ),
                            ),
                          );
                        }),
                      ],
                    )),
              ),
            ],
          ),
        ),
        Column(
          children: [
            Gap(60.w),
            Obx(() => _getLHCWidget()),
          ],
        ),
      ],
    );
  }

  Widget _getLHCWidget() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () => AppDefine.inSites(['f099,t002'])
                  ? AppNavigator.toNamed(lotteryHistoryPath,
                      arguments: [controller.lotteryNumber.value.gameId])
                  : AppDefine.systemConfig?.mobileTemplateCategory == '7'
                      ? const SizedBox()
                      : AppNavigator.toNamed(AppRoutes.lotteryResult,
                          arguments: controller.lotteryNumber.value.gameId),
              child: Text(
                '历史记录>',
                style:
                    TextStyle(color: const Color(0xff666666), fontSize: 15.sp),
              ),
            ),
            Row(
              children: [
                AppImage.network(IconResource.sound01(),
                    width: 21.w, height: 21.w),
                SizedBox(width: 10.w),
                Row(children: [
                  Text('第',
                      style: TextStyle(
                          fontSize: 20.sp,
                          color: const Color(0xff333333),
                          fontWeight: FontWeight.bold)),
                  Obx(
                    () => Text(
                        getLotteryNoOrIssueSubstring(
                            controller.lotteryNumber.value),
                        style: TextStyle(
                            fontSize: 20.sp,
                            color: Colors.red,
                            fontWeight: FontWeight.bold)),
                  ),
                  Text('期开奖结果',
                      style: TextStyle(
                          fontSize: 20.sp,
                          color: const Color(0xff333333),
                          fontWeight: FontWeight.bold)),
                ]),
              ],
            ),
            Row(children: [
              Text('咪',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold)),
              SizedBox(width: 10.w),
              FlutterSwitch(
                width: 50.h,
                height: 30.h,
                value: _isLastLotteyCheck.value,
                activeColor: const Color(0xff11c1f3),
                onToggle: (bool value) {
                  if (controller.showlotterys.isNotEmpty) {
                    _isLastLotteyCheck.value = value;
                  }
                },
              )
            ])
          ],
        ),
        Padding(
          padding: EdgeInsets.all(10.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ...List.generate(controller.showlotterys.length, (index) {
                final item = controller.showlotterys[index];
                return LotteryBallWidget(
                  score: item.number ?? '',
                  color: item.color ?? '',
                  text: item.sx ?? '',
                  isLastItem: index == 6,
                  isEnableLastItem: _isLastLotteyCheck.value,
                  onChange: (value) {
                    _isLastLotteyCheck.value = value;
                  },
                );
              }),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.all(10.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Text('下期开奖日期：',
                    style: TextStyle(
                        fontSize: 16.sp,
                        color: const Color(0xff333333),
                        fontWeight: FontWeight.bold)),
                Text(
                  (controller.lotteryNumber.value.endtime ?? '0000 00-00-00')
                      .split(' ')[0],
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold),
                )
              ]),
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Text('直播倒计时: ',
                    style: TextStyle(
                        fontSize: 16.sp,
                        color: const Color(0xff333333),
                        fontWeight: FontWeight.bold)),
                Text(
                  DateUtil.displayCloseTime(controller.lotteryCloseTime.value),
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold),
                )
              ])
            ],
          ),
        ),
      ],
    );
  }

  String getLotteryNoOrIssueSubstring(LotteryNumberData lotteryNum) {
    if (lotteryNum.lhcdocLotteryNo != null &&
        lotteryNum.lhcdocLotteryNo!.isNotEmpty) {
      return lotteryNum.lhcdocLotteryNo!;
    } else if (lotteryNum.issue != null &&
        (lotteryNum.issue ?? '').length >= 3) {
      return (lotteryNum.issue ?? '')
          .substring((lotteryNum.issue ?? '').length - 3);
    } else {
      return ''; // or any default value you prefer
    }
  }
}
