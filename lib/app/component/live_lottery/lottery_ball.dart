import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/component/lottery_scratch_card.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:get/get.dart';

class LotteryBallWidget extends StatelessWidget {
  final bool isLastItem;
  final String score;
  final String color;
  final String text;
  final bool isEnableLastItem;
  final ValueChanged<bool> onChange;

  const LotteryBallWidget({
    super.key,
    this.isLastItem = false,
    this.color = 'green',
    this.score = '0',
    this.text = '',
    this.isEnableLastItem = false,
    required this.onChange,
  });

  @override
  Widget build(BuildContext context) {
    if (isLastItem) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              SizedBox(
                width: 43.w,
                height: 43.w,
                child: const Center(child: Text('+')),
              ),
              const Text(''),
            ],
          ),
          isEnableLastItem
              ? Column(
                  children: [
                    GestureDetector(
                        onTap: () {
                          Get.dialog(LotteryScratchCard(
                              score: score,
                              color: color,
                              text: text,
                              onChange: (value) {
                                onChange.call(value);
                                Get.back();
                              }));
                        },
                        child: AppImage.svgByAsset('gyg.svg',
                            width: 55.w, height: 55.w)),
                    Text(
                      '',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: const Color(0xff333333),
                          fontSize: 18.sp),
                    )
                  ],
                )
              : Column(
                  children: [
                    Stack(
                      alignment: AlignmentDirectional.center,
                      children: [
                        AppImage.asset("ball/$color.png",
                            width: 55.w, height: 55.w),
                        // AppImage.network(ballImageUrlByColor(color), width: 55.w, height: 55.w),
                        Text(
                          '$score ',
                          style:
                              TextStyle(color: Colors.black, fontSize: 20.sp),
                        ),
                      ],
                    ),
                    Text(
                      text,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: const Color(0xff333333),
                          fontSize: 18.sp),
                    )
                  ],
                )
        ],
      );
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Stack(
          alignment: AlignmentDirectional.center,
          children: [
            AppImage.network(ballImageUrlByColor(color),
                width: 55.w, height: 55.w),
            Text(
              '$score ',
              style: TextStyle(color: Colors.black, fontSize: 20.sp),
            ),
          ],
        ),
        Text(
          text,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: const Color(0xff333333),
              fontSize: 18.sp),
        )
      ],
    );
  }

  String ballImageUrlByScore(String score) {
    int scoreInt = int.parse(score);
    if (BlueNum.contains(scoreInt)) {
      return IconResource.blue_ball();
    }
    if (RedNum.contains(scoreInt)) {
      return IconResource.red_ball();
    }
    if (GreenNum.contains(scoreInt)) {
      return IconResource.green_ball();
    }
    return '';
  }

  String ballImageUrlByColor(String color) {
    switch (color) {
      case 'green':
        return IconResource.green_ball();
      case 'red':
        return IconResource.red_ball();
      case 'blue':
        return IconResource.blue_ball();
      default:
        return '';
    }
  }
}
