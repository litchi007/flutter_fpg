import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fpg_flutter/app/component/live_lottery/lottery_ball.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:get/get.dart';
import 'package:scratcher/scratcher.dart';

class LotteryScratchCard extends StatelessWidget {
  const LotteryScratchCard({
    super.key,
    required this.color,
    required this.score,
    required this.text,
    required this.onChange,
  });

  final String score;
  final String color;
  final String text;
  final ValueChanged<bool> onChange;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            '特码已开出，请刮开特码体验开奖新乐趣。',
            style: TextStyle(color: Colors.white),
          ),
          const SizedBox(
            height: 30,
          ),
          const FaIcon(
            FontAwesomeIcons.handPointDown,
            size: 20,
            color: Colors.white,
          ),
          const SizedBox(
            height: 30,
          ),
          Center(
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                AppImage.asset('mipai_2.png',
                    width: 400, height: 200, fit: BoxFit.fill),
                Scratcher(
                  brushSize: 60,
                  threshold: 50,
                  // color: Colors.red,
                  image: Image.asset(
                    'assets/images/mipai_1.png',
                    width: 380,
                    height: 170,
                  ),
                  onChange: (value) {
                    print("Scratch progress: $value%");
                    if (value >= 99.80) {
                      onChange.call(false);
                    }
                  },
                  onThreshold: () => print(
                      "Threshold reached, you won!"), //AppImage.asset('mipai_2.png', width: Get.width - 100.w),
                  child: Container(
                    color: getColor(color),
                    width: 380,
                    height: 170,
                    child: Center(
                      child: LotteryBallWidget(
                        score: score,
                        color: color,
                        text: text,
                        isLastItem: false,
                        isEnableLastItem: false,
                        onChange: (valeu) {},
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          GestureDetector(
            onTap: () {
              onChange.call(true);
            },
            child: const FaIcon(
              FontAwesomeIcons.xmark,
              size: 20,
              color: Colors.white,
            ),
          ),
          const SizedBox(
            height: 90,
          ),
        ],
      ),
    );
  }

  Color getColor(String colorStr) {
    if (colorStr == 'green') {
      return Colors.green;
    } else if (colorStr == 'red') {
      return Colors.red;
    } else if (colorStr == 'blue') {
      return Colors.blue;
    }
    return Colors.red;
  }
}
