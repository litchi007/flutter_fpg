import 'dart:async';

import 'package:fpg_flutter/api/lhcdoc/lhcdoc.http.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/AvatarModel.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:fpg_flutter/data/models/UGRegisterModel.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/models/lhcdoc_login_reg_page_info_model/lhcdoc_login_reg_page_info_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/data/repositories/app_lhcdoc_repository.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';

class AuthController extends GetxController {
  var isCheckedRem = false.obs;
  final storage = GetStorage();
  final AppUserRepository _appUserRepository = AppUserRepository();
  final AppSystemRepository _appSystemRepository = AppSystemRepository();
  RxList<AvatarModel> avatarList = RxList();
  final RxString imgCaptcha = "".obs;
  RememberPassModel? rem;
  final GlobalService globalService = Get.find();
  final AppLHCDocRepository _appLHCDocRepository = AppLHCDocRepository();
  final RxString sloganImage =
      img_root('upload/f099/customise/images/m_lhc2_m_logo', type: 'jpg').obs;

  /// 是否显示登录图形验证码
  final isShowVCode = false.obs;
  final lhcdocLoginRegPageInfo = const LhcdocLoginRegPageInfoModel().obs;
  // /// 是否显示登录图形验证码
  // final isShowLoginCode = false.obs;
  bool loginStatus = false;
  getRegInfo() async {
    final res = await LhcdocHttp.lhcdocLoginRegPageInfo();
    LogUtil.w(res);
    lhcdocLoginRegPageInfo.value = res.data!;
  }

  @override
  void onInit() {
    super.onInit();

    // globalService.getUserInfo();

    getRegInfo();
    globalService.getUserInfo();
    try {
      var token = storage.read(StorageKeys.REMEMBER_DATA);

      if (token != null) {
        if (token is RememberPassModel?) {
          rem = storage.read(StorageKeys.REMEMBER_DATA);
        } else {
          rem = RememberPassModel.fromJson(
              storage.read(StorageKeys.REMEMBER_DATA));
        }

        AppLogger.d("token: ${globalService.token.value}");
        isCheckedRem.value = rem?.remember ?? false;
      }
    } catch (e) {
      print({'RememberPassModel error': e});
    }
  }

  void getLoginPageRedInfo() async {
    await _appLHCDocRepository.loginRegPageInfo().then((res) async {
      sloganImage.value = res?.logo ?? '';
    }).catchError((error) {
      LogUtil.e({'loginRegPageInfo': error});
    });
  }

  void onDataFetch() async {
    if (AppDefine.isShowImageCode) {
      isShowVCode.value = true;
      await getImgCaptcha();
    }
  }

  void onChangeRemember(
    bool remember,
    String userName,
    String password,
  ) async {
    storage.write(
        StorageKeys.REMEMBER_DATA,
        RememberPassModel(
            username: userName, password: password, remember: remember));
  }

  //注册
  Future<String> register(
    String userName,
    String password, {
    String? fullName,
    String? inviter,
    String? inviteCode,
    String? fundPwd,
    String? qq,
    String? wx,
    String? email,
    String? imgCode,
    String? smsCode,
    String? phone,
    String? fb,
    String? line,
  }) async {
    String state = "注册失败";
    NetBaseEntity<UGRegisterModel> res = await _appUserRepository.register(
      userName,
      password,
      fullName: fullName,
      inviter: inviter,
      inviteCode: inviteCode,
      fundPwd: fundPwd,
      qq: qq,
      wx: wx,
      email: email,
      imgCode: imgCode,
      smsCode: smsCode,
      phone: phone,
      fb: fb,
      line: line,
    );
    AppLogger.d(res.toJson());
    try {
      if (res.data == null) {
        return res.msg;
      }
      UGRegisterModel? data = res.data;
      UserToken? tempToken = UserToken();
      tempToken.apiSid = data?.apiSid;
      tempToken.apiToken = data?.apiToken;
      GlobalService.to.updateToken(tempToken);
      await GlobalService.to.getUserInfo();
      state = "";
    } catch (e) {
      state = e.toString();
      print({'register error': state});
    }

    return state;
  }

  Future<NetBaseEntity<UGLoginModel>?> login(
    String userName,
    String password, {
    String? fullName,
    String? imgCode,
  }) async {
    NetBaseEntity<UGLoginModel>? state;
    AppToast.show();

    await _appUserRepository
        .login(userName, password, fullName: fullName, imgCode: imgCode)
        .then((res) async {
      AppToast.dismiss();

      state = res;
    }).catchError((error) {
      LogUtil.e({'login error': error});
    });

    return state;
  }

  Future<UGLoginModel?> guestLogin() async {
    UGLoginModel? state;
    loginStatus = true;
    await _appUserRepository.guestLogin().then((data) async {
      if (data == null) {
      } else {
        if (data.apiSid != null) {
          UserToken? tempToken = UserToken();
          tempToken.apiSid = data.apiSid;
          tempToken.apiToken = data.apiToken;
          GlobalService.to.updateToken(tempToken);
          await GlobalService.to.getUserInfo();
        }
        state = data;
      }
    }).catchError((error) {
      LogUtil.w({'login error': error});
      // state = false;
    });
    loginStatus = false;
    return state;
  }

  Future<void> logout() async {
    await _appUserRepository
        .logout(token: AppDefine.userToken?.apiToken)
        .then((data) {
      if (data.code == 0) {
        GlobalService.to.removeToken();
        GlobalService.to.rechargePopUpAlarmShowing = true;

        Get.offAllNamed(homePath); // Navigate to login after logout
      }
    }).catchError((error) {
      print({'logout error': error});
    });
  }

  Future<void> getImgCaptcha() async {
    await _appSystemRepository.getImgCaptcha().then((data) {
      if (data != null) {
        imgCaptcha.value = data.base64Img!;
      }
    }).catchError((data) {
      print({'getImgCaptcha : failed'});
    });
  }

  void getAvatarList() {
    _appSystemRepository.getAvatarList().then((data) {
      avatarList.value = data!;
    });
  }

  void handleLoginCode() {
    isShowVCode.value = true;
    getImgCaptcha();
  }

  // void getSystemConfig() async {
  //   await _appSystemRepository.fetchConfig();
  //   if (AppDefine.isShowImageCode) {
  //     isShowLoginCode.value = AppDefine.isShowImageCode;
  //     getImgCaptcha();
  //   }
  // }
}
