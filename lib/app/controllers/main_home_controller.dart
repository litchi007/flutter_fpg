import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/app/pages/account/account_view.dart';
import 'package:fpg_flutter/app/pages/activity/promotion_list_page.dart';
import 'package:fpg_flutter/app/pages/game_hall/GCWGameHallPage/GCWGameHallPage.dart';
import 'package:fpg_flutter/app/pages/game_hall/JDGameHallPage/jd_game_hall_page.dart';
import 'package:fpg_flutter/app/pages/home/home_view.dart';
import 'package:fpg_flutter/app/pages/information/information_page.dart';
import 'package:fpg_flutter/app/pages/comunity/community_page.dart';
import 'package:fpg_flutter/app/pages/national_guess/pages/guessing_view.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/page/expertPlan/controller/controller.dart';
import 'package:fpg_flutter/page/expertPlan/expertPlan.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/routes.dart';

class MainHomeController extends GetxController {
  static MainHomeController get to => Get.find<MainHomeController>();
  final GlobalService globalService = Get.find();

  // @override
  // void onReady() async {
  //   super.onReady();
  // }

  RxString selectedPath = '/home'.obs;

  Widget getCurrentPage(String value) {
    switch (value) {
      case homePath:
        return const HomeView();
      case '/activity':
        return const PromotionListPage();
      case '/message':
        return const SizedBox();
      case '/community':
        return const CommunityPage();
      case '/information':
        return const InformationPage();
      case '/chess':
        return const SizedBox();
      case accountPath:
        return const AccountView();
      case AppRoutes.expertPlan:
        return GetBuilder<ExpertPlanController>(
          init: ExpertPlanController(),
          builder: (controller) {
            return const ExpertPlanPage(); // 从路由中获取页面
          },
        );
      case '/gameHall':
        return GCWGameHallPage();
      case '/lotteryList':
        return const JDGameHallPage();
      case '/nationalQuiz':
        return const GuessingView();

      default:
        return const SizedBox();
    }
  }

  // 根據config取得遊戲大廳
  Widget getGameHallPage() {
    if (!globalService.isAuthenticated.value) {
      AppNavigator.toNamed(loginPath);
      return const SizedBox();
    }
    // MEMO: 經確認特意這麼做
    // if (AppDefine.inSites(['f111', 'f123'])) {
    //   return PageName.JDGameHallPage;
    // }
    // let sysConf = UGStore.globalProps.sysConf;

    // if (AppDefine.systemConfig?.mobileGameHall == '1') {
    //   //新彩票大厅
    //   // return PageName.GameHallPage;
    // } else if (AppDefine.systemConfig?.mobileGameHall == '2') {
    //   //经典彩票大厅
    //   return PageName.FreedomHallPage;
    // } else if (sysConf?.mobileTemplateCategory === '7') {
    //   return PageName.HSCLotteryHallPage;
    // } else if (sysConf?.mobileTemplateCategory === '64') {
    //   return PageName.GCWGameHallPage;
    // }
    // return PageName.JDGameHallPage;
    return const JDGameHallPage();
    // return const GCWGameHallPage();
  }

  /// 获取专家列表是否有游戏
  Future<bool> getUserExpertSwitch() async {
    var res = await UserHttp.userExpertSwitch('1');
    var gameArr = res.data?.gameArr ?? [];
    return gameArr.isNotEmpty;
  }
}
