import 'package:fpg_flutter/models/activity_gold_egg_log_model/activity_gold_egg_log_model.dart';
import 'package:fpg_flutter/models/home_menu/home_menu_model.dart';
import 'package:fpg_flutter/models/chat_create_red_bag_model/chat_create_red_bag_model.dart';
import 'package:fpg_flutter/models/chat_get_bet_bills_model/chat_get_bet_bills_model.dart';
import 'package:fpg_flutter/models/chat_get_token_model/chat_get_token_model.dart';
import 'package:fpg_flutter/models/chat_get_token_res_model/chat_get_token_res_model.dart';
import 'package:fpg_flutter/models/chat_message_record_model/chat_message_record_model.dart';
import 'package:fpg_flutter/models/chat_red_bag_message_record_model/chat_red_bag_message_record_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_info_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_issue_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_record_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_rank_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_switch_model.dart';
import 'package:fpg_flutter/models/app_info_app_id_model/app_info_app_id_model.dart';
import 'package:fpg_flutter/models/game_chip_list_model/game_chip_list_model.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_history_model.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/grab_red_bag_model/grab_red_bag_model.dart';
import 'package:fpg_flutter/models/guest_login_model/guest_login_model.dart';
import 'package:fpg_flutter/models/lhcdoc_login_reg_page_info_model/lhcdoc_login_reg_page_info_model.dart';
import 'package:fpg_flutter/models/longDragon/long_dragon_model.dart';
import 'package:fpg_flutter/models/lotteryBetDetail/lottery_bet_detail.model.dart';
import 'package:fpg_flutter/models/lotteryBetResult/lottery_bet_result_data.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_list_model.dart';
import 'package:fpg_flutter/models/lotteryRoadData/lottery_road_data_model.dart';
import 'package:fpg_flutter/models/lotteryTickets/lottery_tickets_model.dart';
import 'package:fpg_flutter/models/lottery_data/lottery_data_model.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery_group_games_model.dart';
import 'package:fpg_flutter/models/lottery_my_bet_model/lottery_my_bet_model.dart';
import 'package:fpg_flutter/models/redEnvelope/red_envelope_list_model.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/models/system_avatar_list_model/system_avatar_list_model.dart';
import 'package:fpg_flutter/models/twoSideDragon/two_side_dragon_model.dart';
import 'package:fpg_flutter/models/user_balance_model/user_balance_model.dart';
import 'package:fpg_flutter/models/user_get_avatar_setting_model/user_get_avatar_setting_model.dart';

typedef DataFactory<T> = T Function(Json json);

/// 结构体，接口映射的数据结构
final Map<Type, DataFactory> dataModelFactories = <Type, DataFactory>{
  LotteryGroupGamesModel: LotteryGroupGamesModel.fromJson,
  GameNextIssueModel: GameNextIssueModel.fromJson,
  GameLotteryHistoryModel: GameLotteryHistoryModel.fromJson,
  GuestLoginModel: GuestLoginModel.fromJson,
  GamePlayOddsModel: GamePlayOddsModel.fromJson,
  LotteryDataModel: LotteryDataModel.fromJson,
  LotteryTicketsModel: LotteryTicketsModel.fromJson,
  LotteryHistoryListModel: LotteryHistoryListModel.fromJson,
  LotteryBetDetailModel: LotteryBetDetailModel.fromJson,
  TwoSideDragonModel: TwoSideDragonModel.fromJson,
  RedEnvelopeListModel: RedEnvelopeListModel.fromJson,
  LotteryRoadDataModel: LotteryRoadDataModel.fromJson,
  LongDragonModel: LongDragonModel.fromJson,
  LotteryMyBetModel: LotteryMyBetModel.fromJson,
  UserBalanceModel: UserBalanceModel.fromJson,
  LotteryBetResultData: LotteryBetResultData.fromJson,
  GameChipListModel: GameChipListModel.fromJson,
  ExpertPlanSwitchModel: ExpertPlanSwitchModel.fromJson,
  ExpertPlanRankModel: ExpertPlanRankModel.fromJson,
  ExpertPlanDataModel: ExpertPlanDataModel.fromJson,
  ExpertPlanFollowRecordModel: ExpertPlanFollowRecordModel.fromJson,
  ExpertPlanFollowInfoModel: ExpertPlanFollowInfoModel.fromJson,
  ExpertPlanFollowIssueModel: ExpertPlanFollowIssueModel.fromJson,
  AppInfoAppIdModel: AppInfoAppIdModel.fromJson,
  ChatGetTokenResModel: ChatGetTokenResModel.fromJson,
  ChatMessageRecordModel: ChatMessageRecordModel.fromJson,
  ChatCreateRedBagModel: ChatCreateRedBagModel.fromJson,
  ChatRedBagMessageRecordModel: ChatRedBagMessageRecordModel.fromJson,
  GrabRedBagModel: GrabRedBagModel.fromJson,
  SystemAvatarListModel: SystemAvatarListModel.fromJson,
  UserGetAvatarSettingModel: UserGetAvatarSettingModel.fromJson,
  ChatGetBetBillsModel: ChatGetBetBillsModel.fromJson,
  LhcdocLoginRegPageInfoModel: LhcdocLoginRegPageInfoModel.fromJson,
  ChatGetTokenModel: ChatGetTokenModel.fromJson,
  HomeMenuModel: HomeMenuModel.fromJson,
  ActivityGoldEggLogModel: ActivityGoldEggLogModel.fromJson
};
