import 'package:flutter/material.dart';
import '../constants/base.theme.dart';

class CustomTheme extends ThemeExtension<CustomTheme> {
  /// 边框
  Color? gray;

  /// 二级文字颜色
  Color? gray3;

  /// 基础文字颜色
  Color? fontColor;

  /// 反转文字颜色
  Color? reverseFontColor;

  /// 固定黑色文字颜色
  Color? blackFontColor;

  /// 错误
  Color? error;

  /// 底部导航背景
  Color? navbarBg;

  /// body 颜色
  Color? dark2;

  /// 彩票导航颜色
  Color? lotteryAppBar;

  /// 彩票选中文字颜色
  Color? lotteryActiveTxt;

  /// 彩票选中背景色
  Color? lotteryActiveBg;

  /// 彩票1
  Color? lottery1;

  /// 彩票2
  Color? lottery2;

  /// 彩票3
  Color? lottery3;

  /// 彩票4
  Color? lottery4;

  /// 彩票5
  Color? lottery5;

  /// 彩票6
  Color? lottery6;

  /// 彩票7
  Color? lottery7;

  /// 彩票8
  Color? lottery8;

  /// 彩票9
  Color? lottery9;

  /// 彩票10
  Color? lottery10;

  /// 机选背景
  Color? robotSelectBg;

  /// 筹码背景
  Color? chipsBg;

  /// 下注背景
  Color? betBg;

  /// 重置背景
  Color? resetBg;

  /// 禁止下注背景
  Color? chasingNumberDisableBg;

  /// 彩票弹框每个选项背景色
  Color? lotteryPopupItemBg;

  /// 彩票弹框边框
  Color? lotteryPopupBorderColor;

  /// 彩票弹框分割线
  Color? lotteryPopupDivColor;

  /// 游戏规则确定按钮渐变背景色1
  Color? ruleLinearGradientColor1;

  /// 游戏规则确定按钮渐变背景色2
  Color? ruleLinearGradientColor2;

  /// 选择渐变色
  Color? selectStart;

  /// 选择渐变色
  Color? selectEnd;

  /// lottery左侧选择
  Color? leftActive;

  /// lottery时间块背景
  Color? blockBg;

  /// lottery 背景渐变
  Color? lotteryBgStart;

  /// lottery 背景渐变
  Color? lotteryBgEnd;

  /// 主色选择
  Color? active;

  /// 彩票-左侧导航颜色
  Color? lotteryMenuBg;

  /// 彩票-阴影色
  Color? boxShadowBorder;

  /// 红包-字体
  Color? redpackTextColor;

  /// 下注-底部背景色
  Color? lottteryBetBottomBg;

  /// 砸金蛋-tab背景色开始
  Color? goldEggTabBegin;

  /// 砸金蛋-tab背景色结束
  Color? goldEggTabEnd;

  /// 砸金蛋-tab选中背景色开始
  Color? goldEggTabSelectStart;

  /// 砸金蛋-tab选中背景色结束
  Color? goldEggTabSelectEnd;

  /// 砸金蛋-奖品背景色开始
  Color? goldEggPirzeItemStart;

  /// 砸金蛋-奖品背景色结束
  Color? goldEggPirzeItemEnd;

  /// 砸金蛋-记录文字颜色
  Color? goldEggHistoryText;
  CustomTheme({
    /// 边框
    this.gray,

    /// 二级文字颜色
    this.gray3,

    /// 基础文字颜色
    this.fontColor,

    /// 反转文字颜色
    this.reverseFontColor,

    /// 固定黑色文字颜色
    this.blackFontColor,

    /// 错误
    this.error,

    /// 底部导航背景
    this.navbarBg,

    /// body 颜色
    this.dark2,

    /// 彩票导航颜色
    this.lotteryAppBar,

    /// 彩票选中文字颜色
    this.lotteryActiveTxt,

    /// 彩票选中背景色
    this.lotteryActiveBg,

    /// 彩票1
    this.lottery1,

    /// 彩票2
    this.lottery2,

    /// 彩票3
    this.lottery3,

    /// 彩票4
    this.lottery4,

    /// 彩票5
    this.lottery5,

    /// 彩票6
    this.lottery6,

    /// 彩票7
    this.lottery7,

    /// 彩票8
    this.lottery8,

    /// 彩票9
    this.lottery9,

    /// 彩票10
    this.lottery10,

    /// 机选背景
    this.robotSelectBg,

    /// 筹码背景
    this.chipsBg,

    /// 下注背景
    this.betBg,

    /// 重置背景
    this.resetBg,

    /// 禁止下注背景
    this.chasingNumberDisableBg,

    /// 彩票弹框每个选项背景色
    this.lotteryPopupItemBg,

    /// 彩票弹框边框
    this.lotteryPopupBorderColor,

    /// 彩票弹框分割线
    this.lotteryPopupDivColor,

    /// 游戏规则确定按钮渐变背景色1
    this.ruleLinearGradientColor1,

    /// 游戏规则确定按钮渐变背景色2
    this.ruleLinearGradientColor2,

    /// 选择渐变色
    this.selectStart,

    /// 选择渐变色
    this.selectEnd,

    /// lottery左侧选择
    this.leftActive,

    /// lottery时间块背景
    this.blockBg,

    /// lottery 背景渐变
    this.lotteryBgStart,

    /// lottery 背景渐变
    this.lotteryBgEnd,

    /// 主色选择
    this.active,

    /// 彩票-左侧导航颜色
    this.lotteryMenuBg,

    /// 彩票-阴影色
    this.boxShadowBorder,

    /// 红包-字体
    this.redpackTextColor,

    /// 下注-底部背景色
    this.lottteryBetBottomBg,

    /// 砸金蛋-tab背景色开始
    this.goldEggTabBegin,

    /// 砸金蛋-tab背景色结束
    this.goldEggTabEnd,

    /// 砸金蛋-tab选中背景色开始
    this.goldEggTabSelectStart,

    /// 砸金蛋-tab选中背景色结束
    this.goldEggTabSelectEnd,

    /// 砸金蛋-奖品背景色开始
    this.goldEggPirzeItemStart,

    /// 砸金蛋-奖品背景色结束
    this.goldEggPirzeItemEnd,

    /// 砸金蛋-记录文字颜色
    this.goldEggHistoryText,
  });
  @override
  ThemeExtension<CustomTheme> copyWith({
    Color? gray,
    Color? gray3,
    Color? fontColor,
    Color? reverseFontColor,
    Color? blackFontColor,
    Color? error,
    Color? navbarBg,
    Color? dark2,
    Color? lotteryAppBar,
    Color? lotteryActiveTxt,
    Color? lotteryActiveBg,
    Color? lottery1,
    Color? lottery2,
    Color? lottery3,
    Color? lottery4,
    Color? lottery5,
    Color? lottery6,
    Color? lottery7,
    Color? lottery8,
    Color? lottery9,
    Color? lottery10,
    Color? robotSelectBg,
    Color? chipsBg,
    Color? betBg,
    Color? resetBg,
    Color? chasingNumberDisableBg,
    Color? lotteryPopupItemBg,
    Color? lotteryPopupBorderColor,
    Color? lotteryPopupDivColor,
    Color? ruleLinearGradientColor1,
    Color? ruleLinearGradientColor2,
    Color? selectStart,
    Color? selectEnd,
    Color? leftActive,
    Color? blockBg,
    Color? lotteryBgStart,
    Color? lotteryBgEnd,
    Color? active,
    Color? lotteryMenuBg,
    Color? boxShadowBorder,
    Color? redpackTextColor,
    Color? lottteryBetBottomBg,
    Color? goldEggTabBegin,
    Color? goldEggTabEnd,
    Color? goldEggTabSelectStart,
    Color? goldEggTabSelectEnd,
    Color? goldEggPirzeItemStart,
    Color? goldEggPirzeItemEnd,
    Color? goldEggHistoryText,
  }) {
    return CustomTheme(
      gray: gray ?? this.gray,
      gray3: gray3 ?? this.gray3,
      fontColor: fontColor ?? this.fontColor,
      reverseFontColor: reverseFontColor ?? this.reverseFontColor,
      blackFontColor: blackFontColor ?? this.blackFontColor,
      error: error ?? this.error,
      navbarBg: navbarBg ?? this.navbarBg,
      dark2: dark2 ?? this.dark2,
      lotteryAppBar: lotteryAppBar ?? this.lotteryAppBar,
      lotteryActiveTxt: lotteryActiveTxt ?? this.lotteryActiveTxt,
      lotteryActiveBg: lotteryActiveBg ?? this.lotteryActiveBg,
      lottery1: lottery1 ?? this.lottery1,
      lottery2: lottery2 ?? this.lottery2,
      lottery3: lottery3 ?? this.lottery3,
      lottery4: lottery4 ?? this.lottery4,
      lottery5: lottery5 ?? this.lottery5,
      lottery6: lottery6 ?? this.lottery6,
      lottery7: lottery7 ?? this.lottery7,
      lottery8: lottery8 ?? this.lottery8,
      lottery9: lottery9 ?? this.lottery9,
      lottery10: lottery10 ?? this.lottery10,
      robotSelectBg: robotSelectBg ?? this.robotSelectBg,
      chipsBg: chipsBg ?? this.chipsBg,
      betBg: betBg ?? this.betBg,
      resetBg: resetBg ?? this.resetBg,
      chasingNumberDisableBg:
          chasingNumberDisableBg ?? this.chasingNumberDisableBg,
      lotteryPopupItemBg: lotteryPopupItemBg ?? this.lotteryPopupItemBg,
      lotteryPopupBorderColor:
          lotteryPopupBorderColor ?? this.lotteryPopupBorderColor,
      lotteryPopupDivColor: lotteryPopupDivColor ?? this.lotteryPopupDivColor,
      ruleLinearGradientColor1:
          ruleLinearGradientColor1 ?? this.ruleLinearGradientColor1,
      ruleLinearGradientColor2:
          ruleLinearGradientColor2 ?? this.ruleLinearGradientColor2,
      selectStart: selectStart ?? this.selectStart,
      selectEnd: selectEnd ?? this.selectEnd,
      leftActive: leftActive ?? this.leftActive,
      blockBg: blockBg ?? this.blockBg,
      lotteryBgStart: lotteryBgStart ?? this.lotteryBgStart,
      lotteryBgEnd: lotteryBgEnd ?? this.lotteryBgEnd,
      active: active ?? this.active,
      lotteryMenuBg: lotteryMenuBg ?? this.lotteryMenuBg,
      boxShadowBorder: boxShadowBorder ?? this.boxShadowBorder,
      redpackTextColor: redpackTextColor ?? this.redpackTextColor,
      lottteryBetBottomBg: lottteryBetBottomBg ?? this.lottteryBetBottomBg,
      goldEggTabBegin: goldEggTabBegin ?? this.goldEggTabBegin,
      goldEggTabEnd: goldEggTabEnd ?? this.goldEggTabEnd,
      goldEggTabSelectStart:
          goldEggTabSelectStart ?? this.goldEggTabSelectStart,
      goldEggTabSelectEnd: goldEggTabSelectEnd ?? this.goldEggTabSelectEnd,
      goldEggPirzeItemStart:
          goldEggPirzeItemStart ?? this.goldEggPirzeItemStart,
      goldEggPirzeItemEnd: goldEggPirzeItemEnd ?? this.goldEggPirzeItemEnd,
      goldEggHistoryText: goldEggHistoryText ?? this.goldEggHistoryText,
    );
  }

  @override
  ThemeExtension<CustomTheme> lerp(
      ThemeExtension<CustomTheme>? other, double t) {
    return CustomTheme();
  }

  static CustomTheme get dark => CustomTheme(
        gray: BaseTheme.borderDark,
        gray3: BaseTheme.subTitleDark,
        fontColor: BaseTheme.fontDark,
        reverseFontColor: BaseTheme.reverseFontColorDark,
        blackFontColor: BaseTheme.blackFontColorDark,
        error: BaseTheme.errorColorDark,
        navbarBg: BaseTheme.navbarBgDark,
        dark2: BaseTheme.dark2Dark,
        lotteryAppBar: BaseTheme.lotteryAppBarDark,
        lotteryActiveTxt: BaseTheme.lotteryActiveTxtDark,
        lotteryActiveBg: BaseTheme.lotteryActiveBgDark,
        lottery1: BaseTheme.lottery1Dark,
        lottery2: BaseTheme.lottery2Dark,
        lottery3: BaseTheme.lottery3Dark,
        lottery4: BaseTheme.lottery4Dark,
        lottery5: BaseTheme.lottery5Dark,
        lottery6: BaseTheme.lottery6Dark,
        lottery7: BaseTheme.lottery7Dark,
        lottery8: BaseTheme.lottery8Dark,
        lottery9: BaseTheme.lottery9Dark,
        lottery10: BaseTheme.lottery10Dark,
        robotSelectBg: BaseTheme.robotSelectBgDark,
        chipsBg: BaseTheme.chipsBgDark,
        betBg: BaseTheme.betBgDark,
        resetBg: BaseTheme.resetBgDark,
        chasingNumberDisableBg: BaseTheme.chasingNumberDisableBgDark,
        lotteryPopupItemBg: BaseTheme.lotteryPopupItemBgDark,
        lotteryPopupBorderColor: BaseTheme.lotteryPopupBorderColorDarkDark,
        lotteryPopupDivColor: BaseTheme.lotteryPopupDivColorDark,
        ruleLinearGradientColor1: BaseTheme.ruleLinearGradientColor1Dark,
        ruleLinearGradientColor2: BaseTheme.ruleLinearGradientColor2Dark,
        selectStart: BaseTheme.selectStartDark,
        selectEnd: BaseTheme.selectEndDark,
        leftActive: BaseTheme.leftActiveDark,
        blockBg: BaseTheme.blockBgDark,
        lotteryBgStart: BaseTheme.lotteryBgStartDark,
        lotteryBgEnd: BaseTheme.lotteryBgEndDark,
        active: BaseTheme.activeDark,
        lotteryMenuBg: BaseTheme.lotteryMenuBgDark,
        boxShadowBorder: BaseTheme.boxShadowBorderDark,
        redpackTextColor: BaseTheme.redpackTextColorDark,
        lottteryBetBottomBg: BaseTheme.lottteryBetBottomBgDark,
        goldEggTabBegin: BaseTheme.goldEggTabBeginDark,
        goldEggTabEnd: BaseTheme.goldEggTabEndDark,
        goldEggTabSelectStart: BaseTheme.goldEggTabSelectStartDark,
        goldEggTabSelectEnd: BaseTheme.goldEggTabSelectEndDark,
        goldEggPirzeItemStart: BaseTheme.goldEggPirzeItemStartDark,
        goldEggPirzeItemEnd: BaseTheme.goldEggPirzeItemEndDark,
        goldEggHistoryText: BaseTheme.goldEggHistoryTextDark,
      );
  static CustomTheme get light => CustomTheme(
        gray: BaseTheme.borderLight,
        gray3: BaseTheme.subTitleLight,
        fontColor: BaseTheme.fontLight,
        reverseFontColor: BaseTheme.reverseFontColorLight,
        blackFontColor: BaseTheme.blackFontColorLight,
        error: BaseTheme.errorColorLight,
        navbarBg: BaseTheme.navbarBgLight,
        dark2: BaseTheme.dark2Light,
        lotteryAppBar: BaseTheme.lotteryAppBarLight,
        lotteryActiveTxt: BaseTheme.lotteryActiveTxtLight,
        lotteryActiveBg: BaseTheme.lotteryActiveBgLight,
        lottery1: BaseTheme.lottery1Light,
        lottery2: BaseTheme.lottery2Light,
        lottery3: BaseTheme.lottery3Light,
        lottery4: BaseTheme.lottery4Light,
        lottery5: BaseTheme.lottery5Light,
        lottery6: BaseTheme.lottery6Light,
        lottery7: BaseTheme.lottery7Light,
        lottery8: BaseTheme.lottery8Light,
        lottery9: BaseTheme.lottery9Light,
        lottery10: BaseTheme.lottery10Light,
        robotSelectBg: BaseTheme.robotSelectBgLight,
        chipsBg: BaseTheme.chipsBgLight,
        betBg: BaseTheme.betBgLight,
        resetBg: BaseTheme.resetBgLight,
        chasingNumberDisableBg: BaseTheme.chasingNumberDisableBgLight,
        lotteryPopupItemBg: BaseTheme.lotteryPopupItemBgLight,
        lotteryPopupBorderColor: BaseTheme.lotteryPopupBorderColorDarkLight,
        lotteryPopupDivColor: BaseTheme.lotteryPopupDivColorLight,
        ruleLinearGradientColor1: BaseTheme.ruleLinearGradientColor1Light,
        ruleLinearGradientColor2: BaseTheme.ruleLinearGradientColor2Light,
        selectStart: BaseTheme.selectStartLight,
        selectEnd: BaseTheme.selectEndLight,
        leftActive: BaseTheme.leftActiveLight,
        blockBg: BaseTheme.blockBgLight,
        lotteryBgStart: BaseTheme.lotteryBgStartLight,
        lotteryBgEnd: BaseTheme.lotteryBgEndLight,
        active: BaseTheme.activeLight,
        lotteryMenuBg: BaseTheme.lotteryMenuBgLight,
        boxShadowBorder: BaseTheme.boxShadowBorderLight,
        redpackTextColor: BaseTheme.redpackTextColorLight,
        lottteryBetBottomBg: BaseTheme.lottteryBetBottomBgLight,
        goldEggTabBegin: BaseTheme.goldEggTabBeginLight,
        goldEggTabEnd: BaseTheme.goldEggTabEndLight,
        goldEggTabSelectStart: BaseTheme.goldEggTabSelectStartLight,
        goldEggTabSelectEnd: BaseTheme.goldEggTabSelectEndLight,
        goldEggPirzeItemStart: BaseTheme.goldEggPirzeItemStartLight,
        goldEggPirzeItemEnd: BaseTheme.goldEggPirzeItemEndLight,
        goldEggHistoryText: BaseTheme.goldEggHistoryTextLight,
      );
  static CustomTheme get orangeDark => CustomTheme(
        gray: BaseTheme.borderOrangeDark,
        gray3: BaseTheme.subTitleOrangeDark,
        fontColor: BaseTheme.fontOrangeDark,
        reverseFontColor: BaseTheme.reverseFontColorOrangeDark,
        blackFontColor: BaseTheme.blackFontColorOrangeDark,
        error: BaseTheme.errorColorOrangeDark,
        navbarBg: BaseTheme.navbarBgOrangeDark,
        dark2: BaseTheme.dark2OrangeDark,
        lotteryAppBar: BaseTheme.lotteryAppBarOrangeDark,
        lotteryActiveTxt: BaseTheme.lotteryActiveTxtOrangeDark,
        lotteryActiveBg: BaseTheme.lotteryActiveBgOrangeDark,
        lottery1: BaseTheme.lottery1OrangeDark,
        lottery2: BaseTheme.lottery2OrangeDark,
        lottery3: BaseTheme.lottery3OrangeDark,
        lottery4: BaseTheme.lottery4OrangeDark,
        lottery5: BaseTheme.lottery5OrangeDark,
        lottery6: BaseTheme.lottery6OrangeDark,
        lottery7: BaseTheme.lottery7OrangeDark,
        lottery8: BaseTheme.lottery8OrangeDark,
        lottery9: BaseTheme.lottery9OrangeDark,
        lottery10: BaseTheme.lottery10OrangeDark,
        robotSelectBg: BaseTheme.robotSelectBgOrangeDark,
        chipsBg: BaseTheme.chipsBgOrangeDark,
        betBg: BaseTheme.betBgOrangeDark,
        resetBg: BaseTheme.resetBgOrangeDark,
        chasingNumberDisableBg: BaseTheme.chasingNumberDisableBgOrangeDark,
        lotteryPopupItemBg: BaseTheme.lotteryPopupItemBgOrangeDark,
        lotteryPopupBorderColor:
            BaseTheme.lotteryPopupBorderColorDarkOrangeDark,
        lotteryPopupDivColor: BaseTheme.lotteryPopupDivColorOrangeDark,
        ruleLinearGradientColor1: BaseTheme.ruleLinearGradientColor1OrangeDark,
        ruleLinearGradientColor2: BaseTheme.ruleLinearGradientColor2OrangeDark,
        selectStart: BaseTheme.selectStartOrangeDark,
        selectEnd: BaseTheme.selectEndOrangeDark,
        leftActive: BaseTheme.leftActiveOrangeDark,
        blockBg: BaseTheme.blockBgOrangeDark,
        lotteryBgStart: BaseTheme.lotteryBgStartOrangeDark,
        lotteryBgEnd: BaseTheme.lotteryBgEndOrangeDark,
        active: BaseTheme.activeOrangeDark,
        lotteryMenuBg: BaseTheme.lotteryMenuBgOrangeDark,
        boxShadowBorder: BaseTheme.boxShadowBorderOrangeDark,
        redpackTextColor: BaseTheme.redpackTextColorOrangeDark,
        lottteryBetBottomBg: BaseTheme.lottteryBetBottomBgOrangeDark,
        goldEggTabBegin: BaseTheme.goldEggTabBeginOrangeDark,
        goldEggTabEnd: BaseTheme.goldEggTabEndOrangeDark,
        goldEggTabSelectStart: BaseTheme.goldEggTabSelectStartOrangeDark,
        goldEggTabSelectEnd: BaseTheme.goldEggTabSelectEndOrangeDark,
        goldEggPirzeItemStart: BaseTheme.goldEggPirzeItemStartOrangeDark,
        goldEggPirzeItemEnd: BaseTheme.goldEggPirzeItemEndOrangeDark,
        goldEggHistoryText: BaseTheme.goldEggHistoryTextOrangeDark,
      );
  static CustomTheme get orangeLight => CustomTheme(
        gray: BaseTheme.borderOrangeLight,
        gray3: BaseTheme.subTitleOrangeLight,
        fontColor: BaseTheme.fontOrangeLight,
        reverseFontColor: BaseTheme.reverseFontColorOrangeLight,
        blackFontColor: BaseTheme.blackFontColorOrangeLight,
        error: BaseTheme.errorColorOrangeLight,
        navbarBg: BaseTheme.navbarBgOrangeLight,
        dark2: BaseTheme.dark2OrangeLight,
        lotteryAppBar: BaseTheme.lotteryAppBarOrangeLight,
        lotteryActiveTxt: BaseTheme.lotteryActiveTxtOrangeLight,
        lotteryActiveBg: BaseTheme.lotteryActiveBgOrangeLight,
        lottery1: BaseTheme.lottery1OrangeLight,
        lottery2: BaseTheme.lottery2OrangeLight,
        lottery3: BaseTheme.lottery3OrangeLight,
        lottery4: BaseTheme.lottery4OrangeLight,
        lottery5: BaseTheme.lottery5OrangeLight,
        lottery6: BaseTheme.lottery6OrangeLight,
        lottery7: BaseTheme.lottery7OrangeLight,
        lottery8: BaseTheme.lottery8OrangeLight,
        lottery9: BaseTheme.lottery9OrangeLight,
        lottery10: BaseTheme.lottery10OrangeLight,
        robotSelectBg: BaseTheme.robotSelectBgOrangeLight,
        chipsBg: BaseTheme.chipsBgOrangeLight,
        betBg: BaseTheme.betBgOrangeLight,
        resetBg: BaseTheme.resetBgOrangeLight,
        chasingNumberDisableBg: BaseTheme.chasingNumberDisableBgOrangeLight,
        lotteryPopupItemBg: BaseTheme.lotteryPopupItemBgOrangeLight,
        lotteryPopupBorderColor:
            BaseTheme.lotteryPopupBorderColorDarkOrangeLight,
        lotteryPopupDivColor: BaseTheme.lotteryPopupDivColorOrangeLight,
        ruleLinearGradientColor1: BaseTheme.ruleLinearGradientColor1OrangeLight,
        ruleLinearGradientColor2: BaseTheme.ruleLinearGradientColor2OrangeLight,
        selectStart: BaseTheme.selectStartOrangeLight,
        selectEnd: BaseTheme.selectEndOrangeLight,
        leftActive: BaseTheme.leftActiveOrangeLight,
        blockBg: BaseTheme.blockBgOrangeLight,
        lotteryBgStart: BaseTheme.lotteryBgStartOrangeLight,
        lotteryBgEnd: BaseTheme.lotteryBgEndOrangeLight,
        active: BaseTheme.activeOrangeLight,
        lotteryMenuBg: BaseTheme.lotteryMenuBgOrangeLight,
        boxShadowBorder: BaseTheme.boxShadowBorderOrangeLight,
        redpackTextColor: BaseTheme.redpackTextColorOrangeLight,
        lottteryBetBottomBg: BaseTheme.lottteryBetBottomBgOrangeLight,
        goldEggTabBegin: BaseTheme.goldEggTabBeginOrangeLight,
        goldEggTabEnd: BaseTheme.goldEggTabEndOrangeLight,
        goldEggTabSelectStart: BaseTheme.goldEggTabSelectStartOrangeLight,
        goldEggTabSelectEnd: BaseTheme.goldEggTabSelectEndOrangeLight,
        goldEggPirzeItemStart: BaseTheme.goldEggPirzeItemStartOrangeLight,
        goldEggPirzeItemEnd: BaseTheme.goldEggPirzeItemEndOrangeLight,
        goldEggHistoryText: BaseTheme.goldEggHistoryTextOrangeLight,
      );
  static CustomTheme get theme14Dark => CustomTheme(
        gray: BaseTheme.borderTheme14Dark,
        gray3: BaseTheme.subTitleTheme14Dark,
        fontColor: BaseTheme.fontTheme14Dark,
        reverseFontColor: BaseTheme.reverseFontColorTheme14Dark,
        blackFontColor: BaseTheme.blackFontColorTheme14Dark,
        error: BaseTheme.errorColorTheme14Dark,
        navbarBg: BaseTheme.navbarBgTheme14Dark,
        dark2: BaseTheme.dark2Theme14Dark,
        lotteryAppBar: BaseTheme.lotteryAppBarTheme14Dark,
        lotteryActiveTxt: BaseTheme.lotteryActiveTxtTheme14Dark,
        lotteryActiveBg: BaseTheme.lotteryActiveBgTheme14Dark,
        lottery1: BaseTheme.lottery1Theme14Dark,
        lottery2: BaseTheme.lottery2Theme14Dark,
        lottery3: BaseTheme.lottery3Theme14Dark,
        lottery4: BaseTheme.lottery4Theme14Dark,
        lottery5: BaseTheme.lottery5Theme14Dark,
        lottery6: BaseTheme.lottery6Theme14Dark,
        lottery7: BaseTheme.lottery7Theme14Dark,
        lottery8: BaseTheme.lottery8Theme14Dark,
        lottery9: BaseTheme.lottery9Theme14Dark,
        lottery10: BaseTheme.lottery10Theme14Dark,
        robotSelectBg: BaseTheme.robotSelectBgTheme14Dark,
        chipsBg: BaseTheme.chipsBgTheme14Dark,
        betBg: BaseTheme.betBgTheme14Dark,
        resetBg: BaseTheme.resetBgTheme14Dark,
        chasingNumberDisableBg: BaseTheme.chasingNumberDisableBgTheme14Dark,
        lotteryPopupItemBg: BaseTheme.lotteryPopupItemBgTheme14Dark,
        lotteryPopupBorderColor:
            BaseTheme.lotteryPopupBorderColorDarkTheme14Dark,
        lotteryPopupDivColor: BaseTheme.lotteryPopupDivColorTheme14Dark,
        ruleLinearGradientColor1: BaseTheme.ruleLinearGradientColor1Theme14Dark,
        ruleLinearGradientColor2: BaseTheme.ruleLinearGradientColor2Theme14Dark,
        selectStart: BaseTheme.selectStartTheme14Dark,
        selectEnd: BaseTheme.selectEndTheme14Dark,
        leftActive: BaseTheme.leftActiveTheme14Dark,
        blockBg: BaseTheme.blockBgTheme14Dark,
        lotteryBgStart: BaseTheme.lotteryBgStartTheme14Dark,
        lotteryBgEnd: BaseTheme.lotteryBgEndTheme14Dark,
        active: BaseTheme.activeTheme14Dark,
        lotteryMenuBg: BaseTheme.lotteryMenuBgTheme14Dark,
        boxShadowBorder: BaseTheme.boxShadowBorderTheme14Dark,
        redpackTextColor: BaseTheme.redpackTextColorTheme14Dark,
        lottteryBetBottomBg: BaseTheme.lottteryBetBottomBgTheme14Dark,
        goldEggTabBegin: BaseTheme.goldEggTabBeginTheme14Dark,
        goldEggTabEnd: BaseTheme.goldEggTabEndTheme14Dark,
        goldEggTabSelectStart: BaseTheme.goldEggTabSelectStartTheme14Dark,
        goldEggTabSelectEnd: BaseTheme.goldEggTabSelectEndTheme14Dark,
        goldEggPirzeItemStart: BaseTheme.goldEggPirzeItemStartTheme14Dark,
        goldEggPirzeItemEnd: BaseTheme.goldEggPirzeItemEndTheme14Dark,
        goldEggHistoryText: BaseTheme.goldEggHistoryTextTheme14Dark,
      );
  static CustomTheme get theme14Light => CustomTheme(
        gray: BaseTheme.borderTheme14Light,
        gray3: BaseTheme.subTitleTheme14Light,
        fontColor: BaseTheme.fontTheme14Light,
        reverseFontColor: BaseTheme.reverseFontColorTheme14Light,
        blackFontColor: BaseTheme.blackFontColorTheme14Light,
        error: BaseTheme.errorColorTheme14Light,
        navbarBg: BaseTheme.navbarBgTheme14Light,
        dark2: BaseTheme.dark2Theme14Light,
        lotteryAppBar: BaseTheme.lotteryAppBarTheme14Light,
        lotteryActiveTxt: BaseTheme.lotteryActiveTxtTheme14Light,
        lotteryActiveBg: BaseTheme.lotteryActiveBgTheme14Light,
        lottery1: BaseTheme.lottery1Theme14Light,
        lottery2: BaseTheme.lottery2Theme14Light,
        lottery3: BaseTheme.lottery3Theme14Light,
        lottery4: BaseTheme.lottery4Theme14Light,
        lottery5: BaseTheme.lottery5Theme14Light,
        lottery6: BaseTheme.lottery6Theme14Light,
        lottery7: BaseTheme.lottery7Theme14Light,
        lottery8: BaseTheme.lottery8Theme14Light,
        lottery9: BaseTheme.lottery9Theme14Light,
        lottery10: BaseTheme.lottery10Theme14Light,
        robotSelectBg: BaseTheme.robotSelectBgTheme14Light,
        chipsBg: BaseTheme.chipsBgTheme14Light,
        betBg: BaseTheme.betBgTheme14Light,
        resetBg: BaseTheme.resetBgTheme14Light,
        chasingNumberDisableBg: BaseTheme.chasingNumberDisableBgTheme14Light,
        lotteryPopupItemBg: BaseTheme.lotteryPopupItemBgTheme14Light,
        lotteryPopupBorderColor:
            BaseTheme.lotteryPopupBorderColorDarkTheme14Light,
        lotteryPopupDivColor: BaseTheme.lotteryPopupDivColorTheme14Light,
        ruleLinearGradientColor1:
            BaseTheme.ruleLinearGradientColor1Theme14Light,
        ruleLinearGradientColor2:
            BaseTheme.ruleLinearGradientColor2Theme14Light,
        selectStart: BaseTheme.selectStartTheme14Light,
        selectEnd: BaseTheme.selectEndTheme14Light,
        leftActive: BaseTheme.leftActiveTheme14Light,
        blockBg: BaseTheme.blockBgTheme14Light,
        lotteryBgStart: BaseTheme.lotteryBgStartTheme14Light,
        lotteryBgEnd: BaseTheme.lotteryBgEndTheme14Light,
        active: BaseTheme.activeTheme14Light,
        lotteryMenuBg: BaseTheme.lotteryMenuBgTheme14Light,
        boxShadowBorder: BaseTheme.boxShadowBorderTheme14Light,
        redpackTextColor: BaseTheme.redpackTextColorTheme14Light,
        lottteryBetBottomBg: BaseTheme.lottteryBetBottomBgTheme14Light,
        goldEggTabBegin: BaseTheme.goldEggTabBeginTheme14Light,
        goldEggTabEnd: BaseTheme.goldEggTabEndTheme14Light,
        goldEggTabSelectStart: BaseTheme.goldEggTabSelectStartTheme14Light,
        goldEggTabSelectEnd: BaseTheme.goldEggTabSelectEndTheme14Light,
        goldEggPirzeItemStart: BaseTheme.goldEggPirzeItemStartTheme14Light,
        goldEggPirzeItemEnd: BaseTheme.goldEggPirzeItemEndTheme14Light,
        goldEggHistoryText: BaseTheme.goldEggHistoryTextTheme14Light,
      );
}
