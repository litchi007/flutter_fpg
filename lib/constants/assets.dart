// ignore_for_file: prefer_single_quotes
class Assets {
  Assets._();

  /// Assets for assetsImagesBallBlue
  /// assets/images/ball/blue.png
  static const String assetsImagesBallBlue = "assets/images/ball/blue.png";

  /// Assets for assetsImagesBallBlue3
  /// assets/images/ball/blue3.png
  static const String assetsImagesBallBlue3 = "assets/images/ball/blue3.png";

  /// Assets for assetsImagesBallBlue65
  /// assets/images/ball/blue65.png
  static const String assetsImagesBallBlue65 = "assets/images/ball/blue65.png";

  /// Assets for assetsImagesBallDltblue
  /// assets/images/ball/dltblue.png
  static const String assetsImagesBallDltblue =
      "assets/images/ball/dltblue.png";

  /// Assets for assetsImagesBallDltred
  /// assets/images/ball/dltred.png
  static const String assetsImagesBallDltred = "assets/images/ball/dltred.png";

  /// Assets for assetsImagesBallGreen
  /// assets/images/ball/green.png
  static const String assetsImagesBallGreen = "assets/images/ball/green.png";

  /// Assets for assetsImagesBallGreen3
  /// assets/images/ball/green3.png
  static const String assetsImagesBallGreen3 = "assets/images/ball/green3.png";

  /// Assets for assetsImagesBallGreen65
  /// assets/images/ball/green65.png
  static const String assetsImagesBallGreen65 =
      "assets/images/ball/green65.png";

  /// Assets for assetsImagesBallRed
  /// assets/images/ball/red.png
  static const String assetsImagesBallRed = "assets/images/ball/red.png";

  /// Assets for assetsImagesBallRed3
  /// assets/images/ball/red3.png
  static const String assetsImagesBallRed3 = "assets/images/ball/red3.png";

  /// Assets for assetsImagesBallRed65
  /// assets/images/ball/red65.png
  static const String assetsImagesBallRed65 = "assets/images/ball/red65.png";

  /// Assets for assetsImagesBase
  /// assets/images/base.png
  static const String assetsImagesBase = "assets/images/base.png";

  /// Assets for assetsImagesBc
  /// assets/images/bc.png
  static const String assetsImagesBc = "assets/images/bc.png";

  /// Assets for assetsImagesBetBg
  /// assets/images/bet_bg.png
  static const String assetsImagesBetBg = "assets/images/bet_bg.png";

  /// Assets for assetsImagesBg1
  /// assets/images/bg_1.jpg
  static const String assetsImagesBg1 = "assets/images/bg_1.jpg";

  /// Assets for assetsImagesBonusBg
  /// assets/images/bonus_bg.png
  static const String assetsImagesBonusBg = "assets/images/bonus_bg.png";

  /// Assets for assetsImagesCbrpayOnline
  /// assets/images/cbrpay-online.jpg
  static const String assetsImagesCbrpayOnline =
      "assets/images/cbrpay-online.jpg";

  /// Assets for assetsImagesChatHbL
  /// assets/images/chat/hb-l.png
  static const String assetsImagesChatHbL = "assets/images/chat/hb-l.png";

  /// Assets for assetsImagesChatOpenRedBag
  /// assets/images/chat/openRedBag.png
  static const String assetsImagesChatOpenRedBag =
      "assets/images/chat/openRedBag.png";

  /// Assets for assetsImagesChatUnGrab
  /// assets/images/chat/unGrab.png
  static const String assetsImagesChatUnGrab = "assets/images/chat/unGrab.png";

  /// Assets for assetsImagesChatZh1
  /// assets/images/chat/zh/1.jpg
  static const String assetsImagesChatZh1 = "assets/images/chat/zh/1.jpg";

  /// Assets for assetsImagesChatZh2
  /// assets/images/chat/zh/2.jpg
  static const String assetsImagesChatZh2 = "assets/images/chat/zh/2.jpg";

  /// Assets for assetsImagesChatZh3
  /// assets/images/chat/zh/3.jpg
  static const String assetsImagesChatZh3 = "assets/images/chat/zh/3.jpg";

  /// Assets for assetsImagesChatroomBg
  /// assets/images/chatroom-bg.jpg
  static const String assetsImagesChatroomBg = "assets/images/chatroom-bg.jpg";

  /// Assets for assetsImagesChonngzhi
  /// assets/images/chonngzhi.png
  static const String assetsImagesChonngzhi = "assets/images/chonngzhi.png";

  /// Assets for assetsImagesChoumaBg
  /// assets/images/chouma-bg.png
  static const String assetsImagesChoumaBg = "assets/images/chouma-bg.png";

  /// Assets for assetsImagesClose
  /// assets/images/close.png
  static const String assetsImagesClose = "assets/images/close.png";

  /// Assets for assetsImagesCsonline
  /// assets/images/csonline.png
  static const String assetsImagesCsonline = "assets/images/csonline.png";

  /// Assets for assetsImagesCsonlineActive
  /// assets/images/csonline_active.png
  static const String assetsImagesCsonlineActive =
      "assets/images/csonline_active.png";

  /// Assets for assetsImagesDefaultIcon
  /// assets/images/defaultIcon.png
  static const String assetsImagesDefaultIcon = "assets/images/defaultIcon.png";

  /// Assets for assetsImagesDiceColor1
  /// assets/images/dice-color/1.png
  static const String assetsImagesDiceColor1 = "assets/images/dice-color/1.png";

  /// Assets for assetsImagesDiceColor2
  /// assets/images/dice-color/2.png
  static const String assetsImagesDiceColor2 = "assets/images/dice-color/2.png";

  /// Assets for assetsImagesDiceColor3
  /// assets/images/dice-color/3.png
  static const String assetsImagesDiceColor3 = "assets/images/dice-color/3.png";

  /// Assets for assetsImagesDiceColor4
  /// assets/images/dice-color/4.png
  static const String assetsImagesDiceColor4 = "assets/images/dice-color/4.png";

  /// Assets for assetsImagesDiceColor5
  /// assets/images/dice-color/5.png
  static const String assetsImagesDiceColor5 = "assets/images/dice-color/5.png";

  /// Assets for assetsImagesDiceColor6
  /// assets/images/dice-color/6.png
  static const String assetsImagesDiceColor6 = "assets/images/dice-color/6.png";

  /// Assets for assetsImagesDou
  /// assets/images/dou.png
  static const String assetsImagesDou = "assets/images/dou.png";

  /// Assets for assetsImagesEgg1
  /// assets/images/egg1.png
  static const String assetsImagesEgg1 = "assets/images/egg1.png";

  /// Assets for assetsImagesEgg2
  /// assets/images/egg2.png
  static const String assetsImagesEgg2 = "assets/images/egg2.png";

  /// Assets for assetsImagesEgg3
  /// assets/images/egg3.png
  static const String assetsImagesEgg3 = "assets/images/egg3.png";

  /// Assets for assetsImagesEgg4
  /// assets/images/egg4.png
  static const String assetsImagesEgg4 = "assets/images/egg4.png";

  /// Assets for assetsImagesEgg5
  /// assets/images/egg5.png
  static const String assetsImagesEgg5 = "assets/images/egg5.png";

  /// Assets for assetsImagesEgg6
  /// assets/images/egg6.png
  static const String assetsImagesEgg6 = "assets/images/egg6.png";

  /// Assets for assetsImagesEggOpen
  /// assets/images/egg_open.png
  static const String assetsImagesEggOpen = "assets/images/egg_open.png";

  /// Assets for assetsImagesEnvelopeBg
  /// assets/images/envelope_bg.png
  static const String assetsImagesEnvelopeBg = "assets/images/envelope_bg.png";

  /// Assets for assetsImagesFilter
  /// assets/images/filter.png
  static const String assetsImagesFilter = "assets/images/filter.png";

  /// Assets for assetsImagesGameListjrsy
  /// assets/images/gameListjrsy.png
  static const String assetsImagesGameListjrsy =
      "assets/images/gameListjrsy.png";

  /// Assets for assetsImagesGameListlskj
  /// assets/images/gameListlskj.png
  static const String assetsImagesGameListlskj =
      "assets/images/gameListlskj.png";

  /// Assets for assetsImagesGameListyhhd
  /// assets/images/gameListyhhd.png
  static const String assetsImagesGameListyhhd =
      "assets/images/gameListyhhd.png";

  /// Assets for assetsImagesGameSelect
  /// assets/images/game-select.png
  static const String assetsImagesGameSelect = "assets/images/game-select.png";

  /// Assets for assetsImagesGift
  /// assets/images/gift.png
  static const String assetsImagesGift = "assets/images/gift.png";

  /// Assets for assetsImagesGly
  /// assets/images/gly.png
  static const String assetsImagesGly = "assets/images/gly.png";

  /// Assets for assetsImagesHammer
  /// assets/images/hammer.png
  static const String assetsImagesHammer = "assets/images/hammer.png";

  /// Assets for assetsImagesHga
  /// assets/images/hga.png
  static const String assetsImagesHga = "assets/images/hga.png";

  /// Assets for assetsImagesHomeActive
  /// assets/images/home_active.png
  static const String assetsImagesHomeActive = "assets/images/home_active.png";

  /// Assets for assetsImagesHomeMenuPlaceholder
  /// assets/images/home_menu_placeholder.png
  static const String assetsImagesHomeMenuPlaceholder =
      "assets/images/home_menu_placeholder.png";

  /// Assets for assetsImagesHz
  /// assets/images/hz.png
  static const String assetsImagesHz = "assets/images/hz.png";

  /// Assets for assetsImagesIconBorder
  /// assets/images/icon_border.png
  static const String assetsImagesIconBorder = "assets/images/icon_border.png";

  /// Assets for assetsImagesIconGuide
  /// assets/images/icon_guide.png
  static const String assetsImagesIconGuide = "assets/images/icon_guide.png";

  /// Assets for assetsImagesIconGuideActive
  /// assets/images/icon_guide_active.png
  static const String assetsImagesIconGuideActive =
      "assets/images/icon_guide_active.png";

  /// Assets for assetsImagesIconPwdW
  /// assets/images/icon-pwd-w.png
  static const String assetsImagesIconPwdW = "assets/images/icon-pwd-w.png";

  /// Assets for assetsImagesIconUserW
  /// assets/images/icon-user-w.png
  static const String assetsImagesIconUserW = "assets/images/icon-user-w.png";

  /// Assets for assetsImagesImgBanner
  /// assets/images/img_banner.png
  static const String assetsImagesImgBanner = "assets/images/img_banner.png";

  /// Assets for assetsImagesInvestedAmount
  /// assets/images/investedAmount.png
  static const String assetsImagesInvestedAmount =
      "assets/images/investedAmount.png";

  /// Assets for assetsImagesJing
  /// assets/images/jing.png
  static const String assetsImagesJing = "assets/images/jing.png";

  /// Assets for assetsImagesJing2
  /// assets/images/jing2.png
  static const String assetsImagesJing2 = "assets/images/jing2.png";

  /// Assets for assetsImagesJyjl
  /// assets/images/jyjl.png
  static const String assetsImagesJyjl = "assets/images/jyjl.png";

  /// Assets for assetsImagesKlRadbag
  /// assets/images/kl_radbag.png
  static const String assetsImagesKlRadbag = "assets/images/kl_radbag.png";

  /// Assets for assetsImagesLeft
  /// assets/images/left.png
  static const String assetsImagesLeft = "assets/images/left.png";

  /// Assets for assetsImagesLeftTip
  /// assets/images/left_tip.png
  static const String assetsImagesLeftTip = "assets/images/left_tip.png";

  /// Assets for assetsImagesLhcs
  /// assets/images/lhcs.png
  static const String assetsImagesLhcs = "assets/images/lhcs.png";

  /// Assets for assetsImagesLoading
  /// assets/images/loading.gif
  static const String assetsImagesLoading = "assets/images/loading.gif";

  /// Assets for assetsImagesLoginBg
  /// assets/images/login-bg.png
  static const String assetsImagesLoginBg = "assets/images/login-bg.png";

  /// Assets for assetsImagesLottery11x5
  /// assets/images/lottery/11x5.png
  static const String assetsImagesLottery11x5 =
      "assets/images/lottery/11x5.png";

  /// Assets for assetsImagesLotteryCar
  /// assets/images/lottery/car.png
  static const String assetsImagesLotteryCar = "assets/images/lottery/car.png";

  /// Assets for assetsImagesLotteryFt
  /// assets/images/lottery/ft.png
  static const String assetsImagesLotteryFt = "assets/images/lottery/ft.png";

  /// Assets for assetsImagesLotteryHappy
  /// assets/images/lottery/happy.png
  static const String assetsImagesLotteryHappy =
      "assets/images/lottery/happy.png";

  /// Assets for assetsImagesLotteryHot
  /// assets/images/lottery/hot.png
  static const String assetsImagesLotteryHot = "assets/images/lottery/hot.png";

  /// Assets for assetsImagesLotteryJs
  /// assets/images/lottery/js.png
  static const String assetsImagesLotteryJs = "assets/images/lottery/js.png";

  /// Assets for assetsImagesLotteryK3
  /// assets/images/lottery/k3.png
  static const String assetsImagesLotteryK3 = "assets/images/lottery/k3.png";

  /// Assets for assetsImagesLotteryLh
  /// assets/images/lottery/lh.png
  static const String assetsImagesLotteryLh = "assets/images/lottery/lh.png";

  /// Assets for assetsImagesLotteryNn
  /// assets/images/lottery/nn.png
  static const String assetsImagesLotteryNn = "assets/images/lottery/nn.png";

  /// Assets for assetsImagesLotteryOther
  /// assets/images/lottery/other.png
  static const String assetsImagesLotteryOther =
      "assets/images/lottery/other.png";

  /// Assets for assetsImagesLotteryPcdd
  /// assets/images/lottery/pcdd.png
  static const String assetsImagesLotteryPcdd =
      "assets/images/lottery/pcdd.png";

  /// Assets for assetsImagesLotteryQxc
  /// assets/images/lottery/qxc.png
  static const String assetsImagesLotteryQxc = "assets/images/lottery/qxc.png";

  /// Assets for assetsImagesLotteryShishi
  /// assets/images/lottery/shishi.png
  static const String assetsImagesLotteryShishi =
      "assets/images/lottery/shishi.png";

  /// Assets for assetsImagesLottorywinBg
  /// assets/images/lottorywin_bg.png
  static const String assetsImagesLottorywinBg =
      "assets/images/lottorywin_bg.png";

  /// Assets for assetsImagesLrhm
  /// assets/images/lrhm.png
  static const String assetsImagesLrhm = "assets/images/lrhm.png";

  /// Assets for assetsImagesLz
  /// assets/images/lz.png
  static const String assetsImagesLz = "assets/images/lz.png";

  /// Assets for assetsImagesMLhc2MLogo
  /// assets/images/m_lhc2_m_logo.jpg
  static const String assetsImagesMLhc2MLogo =
      "assets/images/m_lhc2_m_logo.jpg";

  /// Assets for assetsImagesMenuBtn
  /// assets/images/menu_btn.png
  static const String assetsImagesMenuBtn = "assets/images/menu_btn.png";

  /// Assets for assetsImagesMenuBtnWhite
  /// assets/images/menu_btn_white.png
  static const String assetsImagesMenuBtnWhite =
      "assets/images/menu_btn_white.png";

  /// Assets for assetsImagesMineRedBag
  /// assets/images/mineRedBag.png
  static const String assetsImagesMineRedBag = "assets/images/mineRedBag.png";

  /// Assets for assetsImagesMineRule1
  /// assets/images/mine-rule/1.jpg
  static const String assetsImagesMineRule1 = "assets/images/mine-rule/1.jpg";

  /// Assets for assetsImagesMineRule2
  /// assets/images/mine-rule/2.jpg
  static const String assetsImagesMineRule2 = "assets/images/mine-rule/2.jpg";

  /// Assets for assetsImagesMineRule3
  /// assets/images/mine-rule/3.jpg
  static const String assetsImagesMineRule3 = "assets/images/mine-rule/3.jpg";

  /// Assets for assetsImagesMipai1
  /// assets/images/mipai_1.png
  static const String assetsImagesMipai1 = "assets/images/mipai_1.png";

  /// Assets for assetsImagesMipai2
  /// assets/images/mipai_2.png
  static const String assetsImagesMipai2 = "assets/images/mipai_2.png";

  /// Assets for assetsImagesMoney
  /// assets/images/money.gif
  static const String assetsImagesMoney = "assets/images/money.gif";

  /// Assets for assetsImagesMoney2
  /// assets/images/money-2.png
  static const String assetsImagesMoney2 = "assets/images/money-2.png";

  /// Assets for assetsImagesMoney3
  /// assets/images/money3.png
  static const String assetsImagesMoney3 = "assets/images/money3.png";

  /// Assets for assetsImagesNumberIcon
  /// assets/images/number_icon.png
  static const String assetsImagesNumberIcon = "assets/images/number_icon.png";

  /// Assets for assetsImagesOnline808pay
  /// assets/images/online-808pay.png
  static const String assetsImagesOnline808pay =
      "assets/images/online-808pay.png";

  /// Assets for assetsImagesOnlineEmbedwallet
  /// assets/images/online-embedwallet.png
  static const String assetsImagesOnlineEmbedwallet =
      "assets/images/online-embedwallet.png";

  /// Assets for assetsImagesOnlineOkp
  /// assets/images/online-okp.png
  static const String assetsImagesOnlineOkp = "assets/images/online-okp.png";

  /// Assets for assetsImagesOpenRedBag
  /// assets/images/openRedBag.png
  static const String assetsImagesOpenRedBag = "assets/images/openRedBag.png";

  /// Assets for assetsImagesPendingOrder
  /// assets/images/pending_order.png
  static const String assetsImagesPendingOrder =
      "assets/images/pending_order.png";

  /// Assets for assetsImagesPeopleNumber
  /// assets/images/peopleNumber.png
  static const String assetsImagesPeopleNumber =
      "assets/images/peopleNumber.png";

  /// Assets for assetsImagesPl
  /// assets/images/pl.png
  static const String assetsImagesPl = "assets/images/pl.png";

  /// Assets for assetsImagesProfile
  /// assets/images/profile.png
  static const String assetsImagesProfile = "assets/images/profile.png";

  /// Assets for assetsImagesProfileActive
  /// assets/images/profile_active.png
  static const String assetsImagesProfileActive =
      "assets/images/profile_active.png";

  /// Assets for assetsImagesProfitandLossBg
  /// assets/images/profitandLoss_bg.png
  static const String assetsImagesProfitandLossBg =
      "assets/images/profitandLoss_bg.png";

  /// Assets for assetsImagesPublicIcon
  /// assets/images/publicIcon.png
  static const String assetsImagesPublicIcon = "assets/images/publicIcon.png";

  /// Assets for assetsImagesQiandai
  /// assets/images/qiandai.png
  static const String assetsImagesQiandai = "assets/images/qiandai.png";

  /// Assets for assetsImagesQiandaoBanner
  /// assets/images/qiandao_banner.png
  static const String assetsImagesQiandaoBanner =
      "assets/images/qiandao_banner.png";

  /// Assets for assetsImagesQiandaoHistory
  /// assets/images/qiandao_history.svg
  static const String assetsImagesQiandaoHistory =
      "assets/images/qiandao_history.svg";

  /// Assets for assetsImagesQiandaoReturn
  /// assets/images/qiandao_return.svg
  static const String assetsImagesQiandaoReturn =
      "assets/images/qiandao_return.svg";

  /// Assets for assetsImagesRedBag
  /// assets/images/redBag.png
  static const String assetsImagesRedBag = "assets/images/redBag.png";

  /// Assets for assetsImagesRedBag1
  /// assets/images/redBag_1.png
  static const String assetsImagesRedBag1 = "assets/images/redBag_1.png";

  /// Assets for assetsImagesRedBag2
  /// assets/images/red_bag2.svg
  static const String assetsImagesRedBag2 = "assets/images/red_bag2.svg";

  /// Assets for assetsImagesRedBagDetail
  /// assets/images/redBagDetail.png
  static const String assetsImagesRedBagDetail =
      "assets/images/redBagDetail.png";

  /// Assets for assetsImagesRedBagRain
  /// assets/images/redBagRain.png
  static const String assetsImagesRedBagRain = "assets/images/redBagRain.png";

  /// Assets for assetsImagesResponsiveMess
  /// assets/images/responsive-mess.png
  static const String assetsImagesResponsiveMess =
      "assets/images/responsive-mess.png";

  /// Assets for assetsImagesReturn
  /// assets/images/return.png
  static const String assetsImagesReturn = "assets/images/return.png";

  /// Assets for assetsImagesSao
  /// assets/images/sao.png
  static const String assetsImagesSao = "assets/images/sao.png";

  /// Assets for assetsImagesSend
  /// assets/images/send.png
  static const String assetsImagesSend = "assets/images/send.png";

  /// Assets for assetsImagesSlxxhdpi
  /// assets/images/slxxhdpi.png
  static const String assetsImagesSlxxhdpi = "assets/images/slxxhdpi.png";

  /// Assets for assetsImagesSponsor
  /// assets/images/sponsor.png
  static const String assetsImagesSponsor = "assets/images/sponsor.png";

  /// Assets for assetsImagesSponsorActive
  /// assets/images/sponsor_active.png
  static const String assetsImagesSponsorActive =
      "assets/images/sponsor_active.png";

  /// Assets for assetsImagesStatusbarBg
  /// assets/images/statusbar-bg.png
  static const String assetsImagesStatusbarBg =
      "assets/images/statusbar-bg.png";

  /// Assets for assetsImagesStep2
  /// assets/images/step2.png
  static const String assetsImagesStep2 = "assets/images/step2.png";

  /// Assets for assetsImagesStep3
  /// assets/images/step3.png
  static const String assetsImagesStep3 = "assets/images/step3.png";

  /// Assets for assetsImagesSxan
  /// assets/images/sxan.png
  static const String assetsImagesSxan = "assets/images/sxan.png";

  /// Assets for assetsImagesSxlm
  /// assets/images/sxlm.png
  static const String assetsImagesSxlm = "assets/images/sxlm.png";

  /// Assets for assetsImagesSzzx
  /// assets/images/szzx.png
  static const String assetsImagesSzzx = "assets/images/szzx.png";

  /// Assets for assetsImagesTiaoma
  /// assets/images/tiaoma.png
  static const String assetsImagesTiaoma = "assets/images/tiaoma.png";

  /// Assets for assetsImagesTips
  /// assets/images/tips.png
  static const String assetsImagesTips = "assets/images/tips.png";

  /// Assets for assetsImagesTixian
  /// assets/images/tixian.png
  static const String assetsImagesTixian = "assets/images/tixian.png";

  /// Assets for assetsImagesTmzs
  /// assets/images/tmzs.png
  static const String assetsImagesTmzs = "assets/images/tmzs.png";

  /// Assets for assetsImagesTransfer
  /// assets/images/transfer.png
  static const String assetsImagesTransfer = "assets/images/transfer.png";

  /// Assets for assetsImagesTzjl
  /// assets/images/tzjl.png
  static const String assetsImagesTzjl = "assets/images/tzjl.png";

  /// Assets for assetsImagesUnGrab
  /// assets/images/unGrab.png
  static const String assetsImagesUnGrab = "assets/images/unGrab.png";

  /// Assets for assetsImagesVipa
  /// assets/images/vipa.png
  static const String assetsImagesVipa = "assets/images/vipa.png";

  /// Assets for assetsImagesWdqb
  /// assets/images/wdqb.png
  static const String assetsImagesWdqb = "assets/images/wdqb.png";

  /// Assets for assetsImagesWinPublicMessage
  /// assets/images/winPublicMessage.gif
  static const String assetsImagesWinPublicMessage =
      "assets/images/winPublicMessage.gif";

  /// Assets for assetsImagesWjzd
  /// assets/images/wjzd.png
  static const String assetsImagesWjzd = "assets/images/wjzd.png";

  /// Assets for assetsImagesXnbczIcon
  /// assets/images/xnbcz-icon.png
  static const String assetsImagesXnbczIcon = "assets/images/xnbcz-icon.png";

  /// Assets for assetsImagesYhj
  /// assets/images/yhj.png
  static const String assetsImagesYhj = "assets/images/yhj.png";

  /// Assets for assetsImagesYhk
  /// assets/images/yhk.png
  static const String assetsImagesYhk = "assets/images/yhk.png";

  /// Assets for assetsImagesZjd
  /// assets/images/zjd.png
  static const String assetsImagesZjd = "assets/images/zjd.png";
}
