import 'dart:ui';
import '../extension/string.ext.dart';

class BaseTheme {
  static Color get borderDark => '#36373F'.color();
  static Color get borderLight => '#fff5f3'.color();
  static Color get borderOrangeDark => '#ffa500'.color();
  static Color get borderOrangeLight => '#ffa500'.color();
  static Color get borderTheme14Dark => '#36373F'.color();
  static Color get borderTheme14Light => '#fff5f3'.color();

  /// 边框
  static Color get subTitleDark => '#A5A8B4'.color();
  static Color get subTitleLight => '#A5A8B4'.color();
  static Color get subTitleOrangeDark => '#ffa500'.color();
  static Color get subTitleOrangeLight => '#ffa500'.color();
  static Color get subTitleTheme14Dark => '#A5A8B4'.color();
  static Color get subTitleTheme14Light => '#A5A8B4'.color();

  /// 二级文字颜色
  static Color get fontDark => '#ffffff'.color();
  static Color get fontLight => '#000000'.color();
  static Color get fontOrangeDark => '#ffa500'.color();
  static Color get fontOrangeLight => '#ffa500'.color();
  static Color get fontTheme14Dark => '#ffffff'.color();
  static Color get fontTheme14Light => '#000000'.color();

  /// 基础文字颜色
  static Color get reverseFontColorDark => '#000000'.color();
  static Color get reverseFontColorLight => '#ffffff'.color();
  static Color get reverseFontColorOrangeDark => '#ffa500'.color();
  static Color get reverseFontColorOrangeLight => '#ffa500'.color();
  static Color get reverseFontColorTheme14Dark => '#000000'.color();
  static Color get reverseFontColorTheme14Light => '#000000'.color();

  /// 反转文字颜色
  static Color get blackFontColorDark => '#000000'.color();
  static Color get blackFontColorLight => '#000000'.color();
  static Color get blackFontColorOrangeDark => '#ffa500'.color();
  static Color get blackFontColorOrangeLight => '#ffa500'.color();
  static Color get blackFontColorTheme14Dark => '#000000'.color();
  static Color get blackFontColorTheme14Light => '#000000'.color();

  /// 固定黑色文字颜色
  static Color get errorColorDark => '#d7213a'.color();
  static Color get errorColorLight => '#d7213a'.color();
  static Color get errorColorOrangeDark => '#d7213a'.color();
  static Color get errorColorOrangeLight => '#d7213a'.color();
  static Color get errorColorTheme14Dark => '#d7213a'.color();
  static Color get errorColorTheme14Light => '#d7213a'.color();

  /// 错误
  static Color get navbarBgDark => '#181818'.color();
  static Color get navbarBgLight => '#ffffff'.color();
  static Color get navbarBgOrangeDark => '#ffa500'.color();
  static Color get navbarBgOrangeLight => '#ffa500'.color();
  static Color get navbarBgTheme14Dark => '#181818'.color();
  static Color get navbarBgTheme14Light => '#ffffff'.color();

  /// 底部导航背景
  static Color get dark2Dark => '#202124'.color();
  static Color get dark2Light => '#f6f6f6'.color();
  static Color get dark2OrangeDark => '#ffa500'.color();
  static Color get dark2OrangeLight => '#ffa500'.color();
  static Color get dark2Theme14Dark => '#202124'.color();
  static Color get dark2Theme14Light => '#f6f6f6'.color();

  /// body 颜色
  static Color get lotteryAppBarDark => '#BFA36D'.color();
  static Color get lotteryAppBarLight => '#BFA36D'.color();
  static Color get lotteryAppBarOrangeDark => '#ffa500'.color();
  static Color get lotteryAppBarOrangeLight => '#ffa500'.color();
  static Color get lotteryAppBarTheme14Dark => '#619AC5'.color();
  static Color get lotteryAppBarTheme14Light => '#619AC5'.color();

  /// 彩票导航颜色
  static Color get lotteryActiveTxtDark => '#956A6A'.color();
  static Color get lotteryActiveTxtLight => '#956A6A'.color();
  static Color get lotteryActiveTxtOrangeDark => '#ffa500'.color();
  static Color get lotteryActiveTxtOrangeLight => '#ffa500'.color();
  static Color get lotteryActiveTxtTheme14Dark => '#956A6A'.color();
  static Color get lotteryActiveTxtTheme14Light => '#956A6A'.color();

  /// 彩票选中文字颜色
  static Color get lotteryActiveBgDark => '#D8223B'.color();
  static Color get lotteryActiveBgLight => '#D8223B'.color();
  static Color get lotteryActiveBgOrangeDark => '#ffa500'.color();
  static Color get lotteryActiveBgOrangeLight => '#ffa500'.color();
  static Color get lotteryActiveBgTheme14Dark => '#D8223B'.color();
  static Color get lotteryActiveBgTheme14Light => '#D8223B'.color();

  /// 彩票选中背景色
  static Color get lottery1Dark => '##e1d463'.color();
  static Color get lottery1Light => '##e1d463'.color();
  static Color get lottery1OrangeDark => '##e1d463'.color();
  static Color get lottery1OrangeLight => '##e1d463'.color();
  static Color get lottery1Theme14Dark => '##e1d463'.color();
  static Color get lottery1Theme14Light => '##e1d463'.color();

  /// 彩票1
  static Color get lottery2Dark => '#008bf9'.color();
  static Color get lottery2Light => '#008bf9'.color();
  static Color get lottery2OrangeDark => '#008bf9'.color();
  static Color get lottery2OrangeLight => '#008bf9'.color();
  static Color get lottery2Theme14Dark => '#008bf9'.color();
  static Color get lottery2Theme14Light => '#008bf9'.color();

  /// 彩票2
  static Color get lottery3Dark => '#4c4d51'.color();
  static Color get lottery3Light => '#4c4d51'.color();
  static Color get lottery3OrangeDark => '#4c4d51'.color();
  static Color get lottery3OrangeLight => '#4c4d51'.color();
  static Color get lottery3Theme14Dark => '#4c4d51'.color();
  static Color get lottery3Theme14Light => '#4c4d51'.color();

  /// 彩票3
  static Color get lottery4Dark => '#f47a00'.color();
  static Color get lottery4Light => '#f47a00'.color();
  static Color get lottery4OrangeDark => '#f47a00'.color();
  static Color get lottery4OrangeLight => '#f47a00'.color();
  static Color get lottery4Theme14Dark => '#f47a00'.color();
  static Color get lottery4Theme14Light => '#f47a00'.color();

  /// 彩票4
  static Color get lottery5Dark => '#63d2d2'.color();
  static Color get lottery5Light => '#63d2d2'.color();
  static Color get lottery5OrangeDark => '#63d2d2'.color();
  static Color get lottery5OrangeLight => '#63d2d2'.color();
  static Color get lottery5Theme14Dark => '#63d2d2'.color();
  static Color get lottery5Theme14Light => '#63d2d2'.color();

  /// 彩票5
  static Color get lottery6Dark => '#420aff'.color();
  static Color get lottery6Light => '#420aff'.color();
  static Color get lottery6OrangeDark => '#420aff'.color();
  static Color get lottery6OrangeLight => '#420aff'.color();
  static Color get lottery6Theme14Dark => '#420aff'.color();
  static Color get lottery6Theme14Light => '#420aff'.color();

  /// 彩票6
  static Color get lottery7Dark => '#aea6a6'.color();
  static Color get lottery7Light => '#aea6a6'.color();
  static Color get lottery7OrangeDark => '#aea6a6'.color();
  static Color get lottery7OrangeLight => '#aea6a6'.color();
  static Color get lottery7Theme14Dark => '#aea6a6'.color();
  static Color get lottery7Theme14Light => '#aea6a6'.color();

  /// 彩票7
  static Color get lottery8Dark => '#ff0400'.color();
  static Color get lottery8Light => '#ff0400'.color();
  static Color get lottery8OrangeDark => '#ff0400'.color();
  static Color get lottery8OrangeLight => '#ff0400'.color();
  static Color get lottery8Theme14Dark => '#ff0400'.color();
  static Color get lottery8Theme14Light => '#ff0400'.color();

  /// 彩票8
  static Color get lottery9Dark => '#770100'.color();
  static Color get lottery9Light => '#770100'.color();
  static Color get lottery9OrangeDark => '#770100'.color();
  static Color get lottery9OrangeLight => '#770100'.color();
  static Color get lottery9Theme14Dark => '#770100'.color();
  static Color get lottery9Theme14Light => '#770100'.color();

  /// 彩票9
  static Color get lottery10Dark => '#2bc610'.color();
  static Color get lottery10Light => '#2bc610'.color();
  static Color get lottery10OrangeDark => '#2bc610'.color();
  static Color get lottery10OrangeLight => '#2bc610'.color();
  static Color get lottery10Theme14Dark => '#2bc610'.color();
  static Color get lottery10Theme14Light => '#2bc610'.color();

  /// 彩票10
  static Color get robotSelectBgDark => '#EEA325'.color();
  static Color get robotSelectBgLight => '#EEA325'.color();
  static Color get robotSelectBgOrangeDark => '#ffa500'.color();
  static Color get robotSelectBgOrangeLight => '#ffa500'.color();
  static Color get robotSelectBgTheme14Dark => '#EEA325'.color();
  static Color get robotSelectBgTheme14Light => '#EEA325'.color();

  /// 机选背景
  static Color get chipsBgDark => '#57B95E'.color();
  static Color get chipsBgLight => '#57B95E'.color();
  static Color get chipsBgOrangeDark => '#ffa500'.color();
  static Color get chipsBgOrangeLight => '#ffa500'.color();
  static Color get chipsBgTheme14Dark => '#57B95E'.color();
  static Color get chipsBgTheme14Light => '#57B95E'.color();

  /// 筹码背景
  static Color get betBgDark => '#D35641'.color();
  static Color get betBgLight => '#D35641'.color();
  static Color get betBgOrangeDark => '#ffa500'.color();
  static Color get betBgOrangeLight => '#ffa500'.color();
  static Color get betBgTheme14Dark => '#D3573F'.color();
  static Color get betBgTheme14Light => '#D3573F'.color();

  /// 下注背景
  static Color get resetBgDark => '#4574C2'.color();
  static Color get resetBgLight => '#4574C2'.color();
  static Color get resetBgOrangeDark => '#4574C2'.color();
  static Color get resetBgOrangeLight => '#4574C2'.color();
  static Color get resetBgTheme14Dark => '#4574C2'.color();
  static Color get resetBgTheme14Light => '#4574C2'.color();

  /// 重置背景
  static Color get chasingNumberDisableBgDark => '#988665'.color();
  static Color get chasingNumberDisableBgLight => '#988665'.color();
  static Color get chasingNumberDisableBgOrangeDark => '#988665'.color();
  static Color get chasingNumberDisableBgOrangeLight => '#988665'.color();
  static Color get chasingNumberDisableBgTheme14Dark => '#988665'.color();
  static Color get chasingNumberDisableBgTheme14Light => '#988665'.color();

  /// 禁止下注背景
  static Color get lotteryPopupItemBgDark => '#000000'.color();
  static Color get lotteryPopupItemBgLight => '#ffffff'.color();
  static Color get lotteryPopupItemBgOrangeDark => '#ffa500'.color();
  static Color get lotteryPopupItemBgOrangeLight => '#ffa500'.color();
  static Color get lotteryPopupItemBgTheme14Dark => '#000000'.color();
  static Color get lotteryPopupItemBgTheme14Light => '#ffffff'.color();

  /// 彩票弹框每个选项背景色
  static Color get lotteryPopupBorderColorDarkDark => '#ffffff'.color();
  static Color get lotteryPopupBorderColorDarkLight => '#f3f3f3'.color();
  static Color get lotteryPopupBorderColorDarkOrangeDark => '#ffa500'.color();
  static Color get lotteryPopupBorderColorDarkOrangeLight => '#ffa500'.color();
  static Color get lotteryPopupBorderColorDarkTheme14Dark => '#ffffff'.color();
  static Color get lotteryPopupBorderColorDarkTheme14Light => '#f3f3f3'.color();

  /// 彩票弹框边框
  static Color get lotteryPopupDivColorDark => '#ffffff'.color();
  static Color get lotteryPopupDivColorLight => '#c7c7c7'.color();
  static Color get lotteryPopupDivColorOrangeDark => '#ffa500'.color();
  static Color get lotteryPopupDivColorOrangeLight => '#ffa500'.color();
  static Color get lotteryPopupDivColorTheme14Dark => '#ffffff'.color();
  static Color get lotteryPopupDivColorTheme14Light => '#c7c7c7'.color();

  /// 彩票弹框分割线
  static Color get ruleLinearGradientColor1Dark => '#ffffff'.color();
  static Color get ruleLinearGradientColor1Light => '#e5caae'.color();
  static Color get ruleLinearGradientColor1OrangeDark => '#ffa500'.color();
  static Color get ruleLinearGradientColor1OrangeLight => '#ffa500'.color();
  static Color get ruleLinearGradientColor1Theme14Dark => '#ffffff'.color();
  static Color get ruleLinearGradientColor1Theme14Light => '#e5caae'.color();

  /// 游戏规则确定按钮渐变背景色1
  static Color get ruleLinearGradientColor2Dark => '#ffffff'.color();
  static Color get ruleLinearGradientColor2Light => '#d3a06f'.color();
  static Color get ruleLinearGradientColor2OrangeDark => '#ffa500'.color();
  static Color get ruleLinearGradientColor2OrangeLight => '#ffa500'.color();
  static Color get ruleLinearGradientColor2Theme14Dark => '#ffffff'.color();
  static Color get ruleLinearGradientColor2Theme14Light => '#d3a06f'.color();

  /// 游戏规则确定按钮渐变背景色2
  static Color get selectStartDark => '#F14B4B'.color();
  static Color get selectStartLight => '#F14B4B'.color();
  static Color get selectStartOrangeDark => '#ffa500'.color();
  static Color get selectStartOrangeLight => '#ffa500'.color();
  static Color get selectStartTheme14Dark => '#248FCA'.color();
  static Color get selectStartTheme14Light => '#248FCA'.color();

  /// 选择渐变色
  static Color get selectEndDark => '#F14B4B'.color();
  static Color get selectEndLight => '#F14B4B'.color();
  static Color get selectEndOrangeDark => '#ffa500'.color();
  static Color get selectEndOrangeLight => '#ffa500'.color();
  static Color get selectEndTheme14Dark => '#248FCA'.color();
  static Color get selectEndTheme14Light => '#248FCA'.color();

  /// 选择渐变色
  static Color get leftActiveDark => '#F14B4B'.color();
  static Color get leftActiveLight => '#F14B4B'.color();
  static Color get leftActiveOrangeDark => '#ffa500'.color();
  static Color get leftActiveOrangeLight => '#ffa500'.color();
  static Color get leftActiveTheme14Dark => '#248FCA'.color();
  static Color get leftActiveTheme14Light => '#248FCA'.color();

  /// lottery左侧选择
  static Color get blockBgDark => '#A8BFCF'.color();
  static Color get blockBgLight => '#A8BFCF'.color();
  static Color get blockBgOrangeDark => '#ffa500'.color();
  static Color get blockBgOrangeLight => '#ffa500'.color();
  static Color get blockBgTheme14Dark => '#a6bdcc'.color();
  static Color get blockBgTheme14Light => '#a6bdcc'.color();

  /// lottery时间块背景
  static Color get lotteryBgStartDark => '#A8BFCF'.color();
  static Color get lotteryBgStartLight => '#A8BFCF'.color();
  static Color get lotteryBgStartOrangeDark => '#ffa500'.color();
  static Color get lotteryBgStartOrangeLight => '#ffa500'.color();
  static Color get lotteryBgStartTheme14Dark => '#92ACC2'.color();
  static Color get lotteryBgStartTheme14Light => '#92ACC2'.color();

  /// lottery 背景渐变
  static Color get lotteryBgEndDark => '#A8BFCF'.color();
  static Color get lotteryBgEndLight => '#A8BFCF'.color();
  static Color get lotteryBgEndOrangeDark => '#ffa500'.color();
  static Color get lotteryBgEndOrangeLight => '#ffa500'.color();
  static Color get lotteryBgEndTheme14Dark => '#83A5CD'.color();
  static Color get lotteryBgEndTheme14Light => '#83A5CD'.color();

  /// lottery 背景渐变
  static Color get activeDark => '#F14B4B'.color();
  static Color get activeLight => '#F14B4B'.color();
  static Color get activeOrangeDark => '#ffa500'.color();
  static Color get activeOrangeLight => '#ffa500'.color();
  static Color get activeTheme14Dark => '#248FCA'.color();
  static Color get activeTheme14Light => '#248FCA'.color();

  /// 主色选择
  static Color get lotteryMenuBgDark => '#f3f3f3'.color();
  static Color get lotteryMenuBgLight => '#f3f3f3'.color();
  static Color get lotteryMenuBgOrangeDark => '#f3f3f3'.color();
  static Color get lotteryMenuBgOrangeLight => '#f3f3f3'.color();
  static Color get lotteryMenuBgTheme14Dark => '#f3f3f3'.color();
  static Color get lotteryMenuBgTheme14Light => '#f3f3f3'.color();

  /// 彩票-左侧导航颜色
  static Color get boxShadowBorderDark => '#dddddd'.color();
  static Color get boxShadowBorderLight => '#dddddd'.color();
  static Color get boxShadowBorderOrangeDark => '#dddddd'.color();
  static Color get boxShadowBorderOrangeLight => '#dddddd'.color();
  static Color get boxShadowBorderTheme14Dark => '#dddddd'.color();
  static Color get boxShadowBorderTheme14Light => '#dddddd'.color();

  /// 彩票-阴影色
  static Color get redpackTextColorDark => '#EDCE9B'.color();
  static Color get redpackTextColorLight => '#EDCE9B'.color();
  static Color get redpackTextColorOrangeDark => '#EDCE9B'.color();
  static Color get redpackTextColorOrangeLight => '#EDCE9B'.color();
  static Color get redpackTextColorTheme14Dark => '#EDCE9B'.color();
  static Color get redpackTextColorTheme14Light => '#EDCE9B'.color();

  /// 红包-字体
  static Color get lottteryBetBottomBgDark => '#666666'.color();
  static Color get lottteryBetBottomBgLight => '#666666'.color();
  static Color get lottteryBetBottomBgOrangeDark => '#666666'.color();
  static Color get lottteryBetBottomBgOrangeLight => '#666666'.color();
  static Color get lottteryBetBottomBgTheme14Dark => '#666666'.color();
  static Color get lottteryBetBottomBgTheme14Light => '#666666'.color();

  /// 下注-底部背景色
  static Color get goldEggTabBeginDark => '#370C58'.color();
  static Color get goldEggTabBeginLight => '#370C58'.color();
  static Color get goldEggTabBeginOrangeDark => '#370C58'.color();
  static Color get goldEggTabBeginOrangeLight => '#370C58'.color();
  static Color get goldEggTabBeginTheme14Dark => '#370C58'.color();
  static Color get goldEggTabBeginTheme14Light => '#370C58'.color();

  /// 砸金蛋-tab背景色开始
  static Color get goldEggTabEndDark => '#4F1E9D'.color();
  static Color get goldEggTabEndLight => '#4F1E9D'.color();
  static Color get goldEggTabEndOrangeDark => '#4F1E9D'.color();
  static Color get goldEggTabEndOrangeLight => '#4F1E9D'.color();
  static Color get goldEggTabEndTheme14Dark => '#4F1E9D'.color();
  static Color get goldEggTabEndTheme14Light => '#4F1E9D'.color();

  /// 砸金蛋-tab背景色结束
  static Color get goldEggTabSelectStartDark => '#BD5EF9'.color();
  static Color get goldEggTabSelectStartLight => '#BD5EF9'.color();
  static Color get goldEggTabSelectStartOrangeDark => '#BD5EF9'.color();
  static Color get goldEggTabSelectStartOrangeLight => '#BD5EF9'.color();
  static Color get goldEggTabSelectStartTheme14Dark => '#BD5EF9'.color();
  static Color get goldEggTabSelectStartTheme14Light => '#BD5EF9'.color();

  /// 砸金蛋-tab选中背景色开始
  static Color get goldEggTabSelectEndDark => '#6015DC'.color();
  static Color get goldEggTabSelectEndLight => '#6015DC'.color();
  static Color get goldEggTabSelectEndOrangeDark => '#6015DC'.color();
  static Color get goldEggTabSelectEndOrangeLight => '#6015DC'.color();
  static Color get goldEggTabSelectEndTheme14Dark => '#6015DC'.color();
  static Color get goldEggTabSelectEndTheme14Light => '#6015DC'.color();

  /// 砸金蛋-tab选中背景色结束
  static Color get goldEggPirzeItemStartDark => '#5C1C84'.color();
  static Color get goldEggPirzeItemStartLight => '#5C1C84'.color();
  static Color get goldEggPirzeItemStartOrangeDark => '#5C1C84'.color();
  static Color get goldEggPirzeItemStartOrangeLight => '#5C1C84'.color();
  static Color get goldEggPirzeItemStartTheme14Dark => '#5C1C84'.color();
  static Color get goldEggPirzeItemStartTheme14Light => '#5C1C84'.color();

  /// 砸金蛋-奖品背景色开始
  static Color get goldEggPirzeItemEndDark => '#5725A8'.color();
  static Color get goldEggPirzeItemEndLight => '#5725A8'.color();
  static Color get goldEggPirzeItemEndOrangeDark => '#5725A8'.color();
  static Color get goldEggPirzeItemEndOrangeLight => '#5725A8'.color();
  static Color get goldEggPirzeItemEndTheme14Dark => '#5725A8'.color();
  static Color get goldEggPirzeItemEndTheme14Light => '#5725A8'.color();

  /// 砸金蛋-奖品背景色结束
  static Color get goldEggHistoryTextDark => '#b9dfff'.color();
  static Color get goldEggHistoryTextLight => '#b9dfff'.color();
  static Color get goldEggHistoryTextOrangeDark => '#b9dfff'.color();
  static Color get goldEggHistoryTextOrangeLight => '#b9dfff'.color();
  static Color get goldEggHistoryTextTheme14Dark => '#b9dfff'.color();
  static Color get goldEggHistoryTextTheme14Light => '#b9dfff'.color();

  /// 砸金蛋-记录文字颜色
}
