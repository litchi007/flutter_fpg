import 'package:fpg_flutter/app/pages/account/pages/completedtoday/pages/jddayclosedetail_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/jdsign_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myfocus_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/user_message/usermessage_view.dart';
import 'package:fpg_flutter/app/pages/auth/login_view.dart';
import 'package:fpg_flutter/app/pages/auth/signup_view.dart';
import 'package:fpg_flutter/app/pages/chat_room/chat_room_page.dart';
import 'package:fpg_flutter/app/pages/chess/series_lobby_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/comment/comment_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/helper/lh_assistant_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/helper/lh_common_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/helper/lh_lm_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/helper/lh_number_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lh_gallery_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/helper/lh_little_helper_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/long_dragon_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/old_year_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/post_list_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_content_detail.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_follow_list_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_funs_list_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/user_profile/user_profile_page.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lhcdoc_history_content_page.dart';
import 'package:fpg_flutter/app/pages/comunity/widget/lhk_post_detail.dart';
import 'package:fpg_flutter/app/pages/home/lottery_history/lottery_date_view.dart';
import 'package:fpg_flutter/app/pages/home/lottery_history/lottery_history_page.dart';
import 'package:fpg_flutter/app/pages/home/real_games/real_game_type_view.dart';
import 'package:fpg_flutter/app/pages/comunity/pages/lhcdoc_content_detail/post_detail_page.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_center_widget/post_content_reply_view.dart';
import 'package:fpg_flutter/app/views/main_home.dart';
import 'package:fpg_flutter/app/views/web_app_page.dart';
import 'package:fpg_flutter/main_bindings.dart';
import 'package:fpg_flutter/page/newPost/bind/bind.dart';
import 'package:fpg_flutter/page/newPost/newPost.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:get/get.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/dl/capitaldetail_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/capital_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/mission_center/missioncenter_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/myliuhe_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/personal_info/personInfo_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/activityReward/pages/activity_reward_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/credit/real_trans_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/credit/real_trans_log_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterytickets/lotteryhistory_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotteryrealBetCard/realbetfish_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/safe_center/safe_center_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/lotterynotcount/jdbetdetail_withoutclose_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedincome_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/RecommendedIncome/pages/jdrecommendedaddmember.dart';
import 'package:fpg_flutter/app/pages/account/pages/feedback/pages/jdfeedback_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/manage_bank_list/manage_bank_accounts_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/manage_bank_list/add_bank_account_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/feedback/pages/jdwritemessage_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/feedback/pages/ksfeedbackrecord_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/feedback/pages/onlineservice_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/alipay_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/alipayprofit_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/alipay_transfer_record_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/interesttreasure/pages/newalipay_transin_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_order_main_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/qdhelp_doc_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_history_view.dart';
import 'package:fpg_flutter/app/pages/activity/promotion_list_page.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_create_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/graborder/pages/grab_detail_view.dart';

import 'package:fpg_flutter/app/pages/account/pages/capital/pages/tutorials/help_doc_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/transfer_pay_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/online_pay_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/btc_pay_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/pay/embed_wallet_view.dart';

import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myfans_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myupdate_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myranking_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/myhistorypost_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/capital/pages/wt/forget_pwd_view.dart';
import 'package:fpg_flutter/app/pages/changlong/pages/longdragon_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/AboutApp/about_app_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/redenvelop_center/pages/redenvelop_center_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/redenvelop_center/pages/redenvelop_record_view.dart';
import 'package:fpg_flutter/app/pages/national_guess/pages/guessing_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/pages/mycontentdetail_view.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/widgets/mydoc_content_detail.dart';
import 'package:fpg_flutter/app/pages/lotterytrends/pages/bet_lottery_view.dart';

import 'package:fpg_flutter/app/pages/account/LotteryWebsiteView.dart';

//pages paths
const accountPath = '/user';
const promotePath = '/promote';
const lotterynotcountPath = '/notcount';
const profileChangePasswordPath = '/change-password';
const homePath = '/home';
const loginPath = '/login';
const registerPath = '/register';
const chatRoomListPath = '/chatRoomList';
const postDetailPath = '/PostDetailPath';
const postConentReply = '/postContentReply';
const cardListPath = '/cardList';
const capitalDetailPath = '/CapitalDetail';
const myContentDatailPath = '/myContentDetailPage';
const transactionRecordPath = '/transactionRecord';
const realtransPath = '/bank/realtrans';
const realtransLogPath = '/bank/realtranslog';
const safeCenterPath = '/safe_center';
const manageBankAccountsPath = '/bank/manageBankAccounts';
const addBankCardPath = '/bank/addBankCard';
const depositPath = '/bank';
const openbetsPath = '/openbets';
const forgetPwdPath = '/forgetPwd';
const feedbackPath = '/feedback';
const feedbackListPath = '/feedbackList';
const onlineServicePath = '/onlineService';
const myInfoPath = '/myinfo';
const myLhePath = '/mylhe';
const taskPath = '/task';
const mobilePath = '/qiandao/mobile';
const weekPath = '/lottery/week';
const realBetFishPath = '/lottery/realBetFish';
const completedtodayPath = '/lottery/settled';
const userMessagePath = '/message';
const textFeedBackPath1 = '/textfeedback1';
const textFeedBackPath2 = '/textfeedback2';
const interestTreasurePath = '/interestTreasure';
const alipayprofitPath = '/alipayprofit';
const alipayTransferRecordPath = '/alipayTransferRecord';
const newalipayTransInPath = '/newalipayTransIn';
const myrecoPath = '/myreco';
const userPath = '/user';
const grabPath = '/grab';
const grabCoursePath = '/grabCourse';
const grabHistoryPath = '/grabHistory';
const promotionListPath = '/activity';
const grabCreatePath = '/grabCreate';
const grabDetailPath = '/grabDetail';
const changLongPath = '/changLong';

const captialTutorialPath = '/captialTutorial';
const transferPayViewPath = '/transferPayViewPath';
const btcPayViewPath = '/xnb_transfer';
const onlinePayViewPath = '/onlinePayViewPath';
const embedwalletPath = '/embedwallet';

const addMemberPath = '/ucenter/addmember';
const fansPath = '/fans';
const focusPath = '/myHistory';
const followPath = '/follow';
const lhcBoardPath = '/lhcBoard';
const historyForumPath = '/historyForum';
const realGameTypesPath = '/realGameTypes';
const postListPath = '/postList';
const lhGalleryPath = '/lhGallery';
const lhkPostDetail = '/lhkPostDetail';
const oldYearPagePath = '/oldYearPage';
const lhLittleHelperPath = '/lhLittleHelperPage';
const longDragonPath = '/longDragonPage';
const lhAssistantPath = '/lhAssistantPage';
const lhCommonPath = '/lhCommonPage';
const lhNumberPath = '/lhNumberPage';
const lhLMPath = '/lhLMPage';
const lotteryHistoryPath = '/lotteryHistoryPage';
const lotteryDateViewPath = '/lotteryDateView';
const betLotteryPath = '/betLottery';
const webAppPage = '/webAppPage';
const aboutAppPath = '/aboutApp';
const redEnvelopPath = '/redEnvelopPath';
const redEnvelopRecordPath = '/redEnvelopRecordPath';
const userProfilePath = '/userProfilePage';
const userContentDetailPath = '/userContentDetailPage';
const lhcdocHistoryContentPath = '/lhcdocHistoryContentPage';
const allPeopleQuizPath = '/nationalQuiz';
const followListPath = '/followListPage';
const funsListPath = '/funsListPage';
const mycontentDetailPath = '/myContentDetail';
const commentPagePath = '/comment';
const newPostPath = '/new-post';
const lotteryWebsitePath = '/lotteryWebsite';

final List<GetPage> routes = [
  ...AppRoutes.routes,
  GetPage(
      name: homePath,
      // page: () => ActivityRewardView(),
      page: () => MainHome(),
      binding: MainBindings(),
      transition: Transition.noTransition),
  GetPage(
    name: loginPath,
    page: () => const LoginView(),
  ),
  GetPage(
    name: lotteryWebsitePath,
    page: () => LotteryWebsite(),
  ),
  GetPage(
    name: chatRoomListPath,
    page: () => const ChatRoomPage(),
  ),
  GetPage(
    name: registerPath,
    page: () => const SignUpView(),
  ),
  GetPage(
    name: cardListPath,
    page: () => const SeriesLobbyPage(),
  ),
  GetPage(
    name: capitalDetailPath,
    page: () => const CapitalDetailListView(),
  ),
  GetPage(
    name: depositPath,
    page: () => const DepositHomeView(),
  ),
  GetPage(
    name: forgetPwdPath,
    page: () => const ForgetPwdView(),
  ),
  GetPage(
    name: taskPath,
    page: () => MissionCenterView(),
  ),
  GetPage(
    name: myInfoPath,
    page: () => const PersonInfoView(),
  ),
  GetPage(
    name: myLhePath,
    page: () => const MyLiuheView(),
  ),
  GetPage(
    name: mobilePath,
    page: () => const JdsiginView(),
  ),
  GetPage(
    name: weekPath,
    page: () => const LotteryHistoryView(),
  ),
  GetPage(
    name: realBetFishPath,
    page: () => const RealBetFishView(),
  ),
  GetPage(
    name: promotePath,
    page: () => const ActivityRewardView(),
  ),
  GetPage(
    name: realtransPath,
    page: () => const RealTransView(),
  ),
  GetPage(
    name: realtransLogPath,
    page: () => const RealTransLogView(),
  ),
  GetPage(
    name: safeCenterPath,
    page: () => const SafeCenterView(),
  ),
  GetPage(
    name: lotterynotcountPath,
    page: () => const JdbetdetailWithoutcloseView(),
  ),
  GetPage(name: completedtodayPath, page: () => const JddayclosedetailView()),
  GetPage(
    name: userMessagePath,
    page: () => const UserMessageView(),
  ),
  GetPage(
    name: onlineServicePath,
    page: () => const OnlineserviceView(),
  ),
  GetPage(
    name: feedbackPath,
    page: () => const JdfeedbackView(),
  ),
  GetPage(
    name: manageBankAccountsPath,
    page: () => const ManageBankListView(),
  ),
  GetPage(
    name: addBankCardPath,
    page: () => const AddBankCardView(),
  ),
  GetPage(
    name: myrecoPath,
    page: () => const JDRecommendedIncomeView(),
  ),
  GetPage(
    name: feedbackListPath,
    page: () => const KsfeedbackrecordView(),
  ),
  GetPage(
    name: textFeedBackPath1,
    page: () => JdwritemessageView(pageIndex: 1),
  ),
  GetPage(
    name: textFeedBackPath2,
    page: () => JdwritemessageView(pageIndex: 2),
  ),
  GetPage(
    name: interestTreasurePath,
    page: () => const AlipayView(),
  ),
  GetPage(
    name: alipayprofitPath,
    page: () => const AlipayprofitView(),
  ),
  GetPage(
    name: alipayTransferRecordPath,
    page: () => const AlipayTransferRecordView(),
  ),
  GetPage(
    name: newalipayTransInPath,
    page: () => const NewalipayTransinView(),
  ),
  GetPage(
    name: grabPath,
    page: () => const GrabOrderMainView(),
  ),
  GetPage(
    name: grabCoursePath,
    page: () => const QdhelpDocView(),
  ),
  GetPage(
    name: grabHistoryPath,
    page: () => const GrabHistoryView(),
  ),
  GetPage(
    name: promotionListPath,
    page: () => const PromotionListPage(),
  ),
  GetPage(
    name: grabCreatePath,
    page: () => const GrabCreateView(),
  ),
  GetPage(
    name: grabDetailPath,
    page: () => const GrabDetailView(),
  ),
  GetPage(
    name: captialTutorialPath,
    page: () => const HelpDocView(),
  ),
  GetPage(
    name: transferPayViewPath,
    page: () => const TransferPayView(),
  ),
  GetPage(
    name: btcPayViewPath,
    page: () => const BtcPayView(),
  ),
  GetPage(
    name: onlinePayViewPath,
    page: () => const OnlinePayView(),
  ),
  GetPage(
    name: embedwalletPath,
    page: () => const EmbedWalletView(),
  ),
  GetPage(
    name: addMemberPath,
    page: () => JDRecommendedAddMember(),
  ),
  GetPage(
    name: fansPath,
    page: () => MyFansView(),
  ),
  GetPage(
    name: focusPath,
    page: () => MyFocusView(),
  ),
  GetPage(
    name: followPath,
    page: () => MyUpdateView(),
  ),
  GetPage(
    name: lhcBoardPath,
    page: () => MyRankingView(),
  ),
  GetPage(
    name: historyForumPath,
    page: () => MyHistoryPostView(),
  ),
  GetPage(name: postDetailPath, page: () => const PostDetailView()),
  GetPage(name: postConentReply, page: () => const PostContentReplyView()),
  GetPage(name: realGameTypesPath, page: () => const RealGameTypeView()),
  GetPage(
    name: postListPath,
    page: () => const PostListPage(),
  ),
  GetPage(
    name: lhGalleryPath,
    page: () => const LhGalleryPage(),
  ),
  GetPage(
    name: lhkPostDetail,
    page: () => const LhkPostDetailView(),
  ),
  GetPage(
    name: oldYearPagePath,
    page: () => const OldYearPage(),
  ),
  GetPage(
    name: lhLittleHelperPath,
    page: () => const LhLittleHelperPage(),
  ),
  GetPage(
    name: longDragonPath,
    page: () => const LongDragonPage(),
  ),
  GetPage(
    name: betLotteryPath,
    page: () => BetLotteryView(),
  ),
  GetPage(
    name: lhAssistantPath,
    page: () => const LhAssistantPage(),
  ),
  GetPage(
    name: lhCommonPath,
    page: () => const LhCommonPage(),
  ),
  GetPage(
    name: lhNumberPath,
    page: () => const LhNumberPage(),
  ),
  GetPage(
    name: lhLMPath,
    page: () => const LhLMPage(),
  ),
  GetPage(
    name: lotteryHistoryPath,
    page: () => const LotteryHistoryPage(),
  ),
  GetPage(
    name: lotteryDateViewPath,
    page: () => const LotteryDateView(),
  ),
  GetPage(
    name: webAppPage,
    page: () => const WebAppPage(),
  ),
  GetPage(
    name: aboutAppPath,
    page: () => const AboutAppView(),
  ),
  GetPage(
    name: redEnvelopPath,
    page: () => const RedenvelopCenterView(),
  ),
  GetPage(
    name: redEnvelopRecordPath,
    page: () => const RedenvelopRecordView(),
  ),
  GetPage(
    name: userProfilePath,
    page: () => const UserProfilePage(),
  ),
  GetPage(
    name: userContentDetailPath,
    page: () => const UserContentDetailView(),
  ),
  GetPage(
    name: lhcdocHistoryContentPath,
    page: () => const LhcdocHistoryContentPage(),
  ),
  GetPage(
    name: allPeopleQuizPath,
    page: () => const GuessingView(),
  ),
  GetPage(
    name: changLongPath,
    page: () => const LongdragonView(),
  ),
  GetPage(
    name: followListPath,
    page: () => const UserFollowListPage(),
  ),
  GetPage(
    name: funsListPath,
    page: () => const UserFunsListPage(),
  ),
  GetPage(
    name: commentPagePath,
    page: () => const CommentPage(),
  ),
  GetPage(
      name: newPostPath,
      page: () => const NewPostPage(),
      binding: NewPostBinding()),
];
