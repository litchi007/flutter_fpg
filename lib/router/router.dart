import 'package:fpg_flutter/middleware/auth_middleware.dart';
import 'package:fpg_flutter/page/expertPlan/bind/bind.dart';
import 'package:fpg_flutter/page/expertPlan/expertPlan.dart';
import 'package:fpg_flutter/page/home/bind/bind.dart';
import 'package:fpg_flutter/page/home/home.dart';
import 'package:fpg_flutter/page/homeMenu/bind/bind.dart';
import 'package:fpg_flutter/page/homeMenu/homeMenu.dart';
import 'package:fpg_flutter/page/longDragonTool/bind/bind.dart';
import 'package:fpg_flutter/page/longDragonTool/longDragonTool.dart';
import 'package:fpg_flutter/page/lottery/bind/bind.dart';
import 'package:fpg_flutter/page/lottery/lottery.dart';
import 'package:fpg_flutter/page/lotteryBetDetail/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryBetDetail/lotteryBetDetail.dart';
import 'package:fpg_flutter/page/lotteryBetDetailSettled/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryBetDetailSettled/lotteryBetDetailSettled.dart';
import 'package:fpg_flutter/page/lotteryLiveBet/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryLiveBet/lotteryLiveBet.dart';
import 'package:fpg_flutter/page/lotteryOrderDetail/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryOrderDetail/lotteryOrderDetail.dart';
import 'package:fpg_flutter/page/lotteryResult/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryResult/lotteryResult.dart';
import 'package:fpg_flutter/page/lotteryRoadData/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryRoadData/lotteryRoadData.dart';
import 'package:fpg_flutter/page/lotteryTickets/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryTickets/lotteryTickets.dart';
import 'package:fpg_flutter/page/lotteryTrend/bind/bind.dart';
import 'package:fpg_flutter/page/lotteryTrend/lotteryTrend.dart';
import 'package:fpg_flutter/page/redEnvelope/bind/bind.dart';
import 'package:fpg_flutter/page/redEnvelope/redEnvelope.dart';
import 'package:fpg_flutter/page/splash/bind/bind.dart';
import 'package:fpg_flutter/page/splash/splash.dart';
import 'package:fpg_flutter/page/todaySettled/bind/bind.dart';
import 'package:fpg_flutter/page/todaySettled/todaySettled.dart';
import 'package:fpg_flutter/page/twoSideDragon/bind/bind.dart';
import 'package:fpg_flutter/page/twoSideDragon/twoSideDragon.dart';
import 'package:fpg_flutter/page/waitingPrize/bind/bind.dart';
import 'package:fpg_flutter/page/waitingPrize/waitingPrize.dart';
// import 'package:fpg_flutter/template/template4/login/bind/bind.dart';
import 'package:fpg_flutter/template/template4/login/template4_login.dart';
import 'package:fpg_flutter/template/template4/mine/bind/bind.dart';
import 'package:fpg_flutter/template/template4/mine/template4_mine.dart';
import 'package:fpg_flutter/template/template4/signup/bind/bind.dart';
import 'package:fpg_flutter/template/template4/signup/template4_signup.dart';
import 'package:fpg_flutter/page/webview/bind/bind.dart';
import 'package:fpg_flutter/page/webview/webview.dart';

import 'package:get/get.dart';

class AppRoutes {
  /// 首页
  static String home = "/home";

  /// 首页菜单
  static String homeMenu = "/homeMenu";

  /// 登陆
  static String login = "/login";

  /// 闪屏页
  static String splash = "/splash";

  /// 彩票页
  static String lottery = "/lottery";

  /// 即时注单
  static String lotteryLiveBet = '/lotteryLiveBet';

  /// 等待开奖
  static String waitingPrize = '/waitingPrize';

  /// 今日已结
  static String todaySettled = '/todaySettled';

  /// 彩票注单
  static String lotteryTickets = '/lotteryTickets';

  /// 彩票下注明细
  static String lotteryBetDetail = '/lotteryBetDetail';

  /// 彩票下注明细(已结算)
  static String lotteryBetDetailSettled = '/lotteryBetDetailSettled';

  /// 开奖结果
  static String lotteryResult = '/lotteryResult';

  /// 两面长龙
  static String twoSideDragon = '/twoSideDragon';

  /// 红包扫雷记录
  static String redEnvelope = '/redEnvelope';

  /// 路珠
  static String lotteryRoadData = '/lotteryRoadData';

  /// 长龙助手
  static String longDragonTool = '/longDragonTool';

  /// 专家跟单计划
  static const String expertPlan = '/expertPlan';

  /// 彩票注单详情
  static String lotteryOrderDetail = '/lotteryOrderDetail';

  /// 走势图
  static String lotteryTrend = '/lotteryTrend';

  /// webview
  static String webview = '/webview';

  /// 模版4 登录页面
  static String template4Login = '/template4Login';

  /// 模版4 注册页面
  static String template4Signup = '/template4Signup';

  /// 模版4 我的页面
  static String template4Mine = '/template4Mine';

  static final List<GetPage> routes = [
    GetPage(
        transition: Transition.noTransition,
        name: AppRoutes.splash,
        page: () {
          return SplashPage();
        },
        binding: SplashBinding()),
    // GetPage(
    //     transition: Transition.noTransition,
    //     name: AppRoutes.home,
    //     page: () {
    //       return const HomePage();
    //     },
    //     binding: HomeBinding()),

    // GetPage(
    //     name: AppRoutes.template4Login,
    //     page: () => Template4LoginPage(),
    //     binding: Template4LoginBinding()),

    GetPage(
        name: AppRoutes.homeMenu,
        page: () => HomeMenuPage(),
        binding: HomeMenuBinding()),

    GetPage(
        name: AppRoutes.template4Signup,
        page: () => Template4SignupPage(),
        binding: Template4SignupBinding()),

    GetPage(
        name: AppRoutes.template4Mine,
        page: () => Template4MinePage(),
        binding: Template4MineBinding()),

    GetPage(
        name: "${AppRoutes.lottery}/:id",
        page: () => LotteryPage(),
        binding: LotteryBinding(),
        middlewares: [AuthMiddleWare()]),

    GetPage(
        name: AppRoutes.lotteryLiveBet,
        page: () => const LotteryLiveBetPage(),
        binding: LotteryLiveBetBinding()),
    GetPage(
        name: AppRoutes.waitingPrize,
        page: () => const WaitingPrizePage(),
        binding: WaitingPrizeBinding()),
    GetPage(
        name: AppRoutes.todaySettled,
        page: () => const TodaySettledPage(),
        binding: TodaySettledBinding()),
    GetPage(
        name: AppRoutes.lotteryTickets,
        page: () => const LotteryTicketsPage(),
        binding: LotteryTicketsBinding()),
    GetPage(
        name: AppRoutes.lotteryBetDetail,
        page: () => const LotteryBetDetailPage(),
        binding: LotteryBetDetailBinding()),

    GetPage(
        name: AppRoutes.lotteryResult,
        page: () => LotteryResultPage(),
        binding: LotteryResultBinding()),

    GetPage(
        name: AppRoutes.twoSideDragon,
        page: () => const TwoSideDragonPage(),
        binding: TwoSideDragonBinding()),

    GetPage(
        name: '${AppRoutes.redEnvelope}/:type',
        page: () => const RedEnvelopePage(),
        binding: RedEnvelopeBinding()),

    GetPage(
        name: AppRoutes.lotteryBetDetailSettled,
        page: () => LotteryBetDetailSettledPage(),
        binding: LotteryBetDetailSettledBinding()),

    GetPage(
        name: AppRoutes.lotteryRoadData,
        page: () => LotteryRoadDataPage(),
        binding: LotteryRoadDataBinding()),

    GetPage(
        name: AppRoutes.longDragonTool,
        page: () => LongDragonToolPage(),
        binding: LongDragonToolBinding()),

    GetPage(
        name: AppRoutes.lotteryOrderDetail,
        page: () => LotteryOrderDetailPage(),
        binding: LotteryOrderDetailBinding()),
    GetPage(
        name: "${AppRoutes.lotteryTrend}/:id",
        page: () => LotteryTrendPage(),
        binding: LotteryTrendBinding()),

    GetPage(
        name: AppRoutes.expertPlan,
        page: () => const ExpertPlanPage(),
        binding: ExpertPlanBinding()),

    GetPage(
        name: AppRoutes.webview,
        page: () => const WebviewPage(),
        binding: WebviewBinding()),
  ];
}
