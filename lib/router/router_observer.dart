import 'package:flutter/material.dart';
import 'package:fpg_flutter/page/lotteryLiveBet/controller/controller.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/router_report.dart';

class GetXRouterObserver extends NavigatorObserver {
  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    RouterReportManager.reportCurrentRoute(route);
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) async {
    RouterReportManager.reportRouteDispose(route);

    /// 等待开奖页面返回 即时注单页面需要刷新
    if (route.settings.name == AppRoutes.waitingPrize &&
        previousRoute?.settings.name == AppRoutes.lotteryLiveBet) {
      LotteryLiveBetController.to.requestRefresh();
    }
  }
}
