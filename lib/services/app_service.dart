import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/configs/favcor.dart';
import 'package:fpg_flutter/http/UGEncryptParams.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/crypto_util.dart' as crypto_util;
// import 'package:restart_app/restart_app.dart';

class AppServices {
  static Future init() async {
    await crypto_util.CryptoUtil.initRsa();
    await crypto_util.CryptoUtil.initAes();
    // print("加密++++ ${await crypto_util.CryptoUtil.encrypt3DES_ECB("123")}");
    // AppConfig.httpUrl = Favcor.current.requestUrl ?? "";
    final List<ConnectivityResult> connectivityResult =
        await (Connectivity().checkConnectivity());

    // This condition is for demo purposes only to explain every connection type.
    // Use conditions which work for your requirements.
    if (connectivityResult.contains(ConnectivityResult.mobile)) {
      print("network_ mobile");
      // Mobile network available.
    } else if (connectivityResult.contains(ConnectivityResult.wifi)) {
      print("network_ wifi");

      // Wi-fi is available.
      // Note for Android:
      // When both mobile and Wi-Fi are turned on system will return Wi-Fi only as active network type
    } else if (connectivityResult.contains(ConnectivityResult.ethernet)) {
      print("network_ ethernet");

      // Ethernet connection available.
    } else if (connectivityResult.contains(ConnectivityResult.vpn)) {
      print("network_ vpn");

      // Vpn connection active.
      // Note for iOS and macOS:
      // There is no separate network interface type for [vpn].
      // It returns [other] on any device (also simulator)
    } else if (connectivityResult.contains(ConnectivityResult.bluetooth)) {
      print("network_ bluetooth");

      // Bluetooth connection available.
    } else if (connectivityResult.contains(ConnectivityResult.other)) {
      print("network_ other");

      // Connected to a network which is not in the above mentioned networks.
    } else if (connectivityResult.contains(ConnectivityResult.none)) {
      print("network_ none");
      // No available network types
    }
    // Connectivity()
    Connectivity().onConnectivityChanged.listen((state) {
      if ((state.contains(ConnectivityResult.wifi) ||
              state.contains(ConnectivityResult.mobile) ||
              state.contains(ConnectivityResult.other)) &&
          connectivityResult.contains(ConnectivityResult.none)) {
        // Restart.restartApp();
        // TODO:尝试解决启动页灰屏问题
        GlobalService.to.fetchAppInfo().then((res) {
          AppNavigator.offAllNamed(homePath);
        });
      }
    });
  }
}
