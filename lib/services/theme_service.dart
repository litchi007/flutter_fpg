import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fpg_flutter/constants/base.theme.dart';
import 'package:fpg_flutter/constants/theme.custom.dart';
import 'package:fpg_flutter/generated/locales.g.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:fpg_flutter/utils/theme/app_theme.dart';
import 'package:fpg_flutter/utils/theme/app_theme_colors.dart';
import 'package:get/get.dart';

const themeLocal = {
  "ThemeMode.light": ThemeMode.light,
  "ThemeMode.dark": ThemeMode.dark,
  "ThemeMode.system": ThemeMode.system
};

enum ThemeState {
  dark(
    ThemeMode.dark,
    LocaleKeys.nightModal,
  ),
  light(ThemeMode.light, LocaleKeys.dayModal),
  paying(ThemeMode.system, LocaleKeys.followingSystem);

  const ThemeState(this.number, this.value);

  final ThemeMode number;

  final String value;

  static ThemeState getType(ThemeMode number) =>
      ThemeState.values.firstWhere((activity) => activity.number == number);

  static String getValue(ThemeMode number) => ThemeState.values
      .firstWhere((activity) => activity.number == number)
      .value;
}

class ThemeObj {
  /// 在这里配置后台的颜色对象
  static Map<String, String?> themeCode = {
    "火山橙": "orange",
    "默认": null, // 根据导航条颜色自动获取1
    "经典1蓝": "theme1",
    "宝石红": "theme2",
    "白曜": "theme3",
    "金星黑": "theme4",
    "ta88": "theme5",
    "凯时": "theme6",
    "乐橙": "theme7",
    "乐FUN": "theme8",
    "综合体育": "theme9",
    "六合论坛1默认": "theme10",
    "六合论坛1香槟色": "theme11",
    "六合论坛1蓝色": "theme12",
    "六合论坛1绿色": "theme13",
    "六合论坛2": "theme14",
    "六合论坛4": "theme15",
    "六合厅": "theme16",
    "六合厅资料": "theme17",
    "尊龙": "theme18",
    "利来": "theme19",
    "威尼斯": "theme20",
    "亚博": "theme21",
    "AE": "theme22",
    "DOY钱包": "theme23",
    "BET365": "theme24",
    "新世纪": "theme25",
    "BET0默认": "theme26",
    "BET1棕色": "theme27",
    "蔚蓝": "theme28",
    "钴蓝": "theme29",
    "聊天APP": "theme30",
    "黑金": "theme31",
    "摩登红": "theme32",
    "精品黑": "theme33",
    "越南": "theme34",
    "青绿": "theme35",
    "炫白": "theme36",
    "sky藍": "theme37",
    "天空蓝": "theme38",
    "vb68风格1蓝背景1蓝": "theme39",
    "vb68风格1蓝背景2红": "theme40",
    "vb68风格1蓝背景3金": "theme41",
    "vb68风格1蓝背景4绿": "theme42",
    "vb68风格1蓝背景5褐": "theme43",
    "vb68风格1蓝背景6淡蓝": "theme44",
    "vb68风格1蓝背景7深蓝": "theme45",
    "vb68风格1蓝背景8紫": "theme46",
    "vb68风格1蓝背景9深红": "theme47",
    "vb68风格1蓝背景10橘黄": "theme48",
    "vb68风格1蓝背景11橘红": "theme49",
    "vb68风格1蓝背景12星空蓝": "theme50",
    "vb68风格1蓝背景13紫": "theme51",
    "vb68风格1蓝背景14粉": "theme52",
    "vb68风格1蓝背景15淡蓝": "theme53",
    "vb68风格1蓝背景16深紫": "theme54",
    "vb68风格1蓝背景17金黄": "theme55",
    "vb68风格1蓝背景18天空灰": "theme56",
    "vb68风格1蓝背景19忧郁蓝": "theme57",
    "vb68风格1蓝背景20科技绿": "theme58",
    "vb68风格1蓝背景21白": "theme59",
    "vb68风格2粉背景1蓝": "theme60",
    "vb68风格2粉背景2红": "theme61",
    "vb68风格2粉背景3金": "theme62",
    "vb68风格2粉背景4绿": "theme63",
    "vb68风格2粉背景5褐": "theme64",
    "vb68风格2粉背景6淡蓝": "theme65",
    "vb68风格2粉背景7深蓝": "theme66",
    "vb68风格2粉背景8紫": "theme67",
    "vb68风格2粉背景9深红": "theme68",
    "vb68风格2粉背景10橘黄": "theme69",
    "vb68风格2粉背景11橘红": "theme70",
    "vb68风格2粉背景12星空蓝": "theme71",
    "vb68风格2粉背景13紫": "theme72",
    "vb68风格2粉背景14粉": "theme73",
    "vb68风格2粉背景15淡蓝": "theme74",
    "vb68风格2粉背景16深紫": "theme75",
    "vb68风格2粉背景17金黄": "theme76",
    "vb68风格2粉背景18天空灰": "theme77",
    "vb68风格2粉背景19忧郁蓝": "theme78",
    "vb68风格2粉背景20科技绿": "theme79",
    "vb68风格2粉背景21白": "theme80",
    "vb68风格3红背景1蓝": "theme81",
    "vb68风格3红背景2红": "theme82",
    "vb68风格3红背景3金": "theme83",
    "vb68风格3红背景4绿": "theme84",
    "vb68风格3红背景5褐": "theme85",
    "vb68风格3红背景6淡蓝": "theme86",
    "vb68风格3红背景7深蓝": "theme87",
    "vb68风格3红背景8紫": "theme88",
    "vb68风格3红背景9深红": "theme89",
    "vb68风格3红背景10橘黄": "theme90",
    "vb68风格3红背景11橘红": "theme91",
    "vb68风格3红背景12星空蓝": "theme92",
    "vb68风格3红背景13紫": "theme93",
    "vb68风格3红背景14粉": "theme94",
    "vb68风格3红背景15淡蓝": "theme95",
    "vb68风格3红背景16深紫": "theme96",
    "vb68风格3红背景17金黄": "theme97",
    "vb68风格3红背景18天空灰": "theme98",
    "vb68风格3红背景19忧郁蓝": "theme99",
    "vb68风格3红背景20科技绿": "theme100",
    "vb68风格3红背景21白": "theme101",
    "TG0默认": "theme102",
    "kikimall": "theme103",
    "简约0蓝": "theme104",
    "简约1红": "theme105",
    "简约2黑": "theme106",
    "新年红": "theme107",
    "红包0默认": "theme108",
    "红包1白": "theme109",
    "红包2红": "theme110",
    "红包3紫": "theme111",
    "红包4蓝": "theme112",
    "大发红": "theme113",
    "92Lottery": "theme114",
    "杏运蓝": "theme115",
    "club白": "theme116",
    "新濠蓝": "theme117",
    "V8天蓝": "theme118",
    "bet11": "theme119",
    "百胜": "theme120",
    "超凡": "theme121",
    "体育": "theme122",
    "星空": "theme123",
    "欧宝蓝": "theme124",
    "kingbet": "theme125",
    "荣耀黑": "theme126",
    "购彩网": "theme127",
    "六合图库模板": "theme128",
    "六合广告": "theme129",
    "红白版": "theme130",
    "石榴红": "theme131"
  };

  static Map<String, Map<String, CustomTheme>> obj = {
    "orange": {
      "light": CustomTheme.orangeLight,
      "dark": CustomTheme.orangeDark
    },
    "theme14": {
      "light": CustomTheme.theme14Light,
      "dark": CustomTheme.theme14Dark
    },
  };

  static Map<String, CustomTheme>? getCurrentTheme(String name) {
    return obj[themeCode[name]];
  }
}

class ThemeService extends GetxService {
  static ThemeService get to => Get.find<ThemeService>();

  // TODO: 因为重写了 ThemeData, 所以需要全部重新配色
  ThemeData light = ThemeData(
      useMaterial3: false,
      extensions: [CustomTheme.light],
      // 测试使用
      scaffoldBackgroundColor: BaseTheme.dark2Light,
      iconTheme: IconThemeData(color: BaseTheme.fontLight),
      primaryColor: Colors.black,
      // textSelectionTheme: TextSelectionThemeData(cursorColor: BaseTheme.errorColorLight),
      textTheme: TextTheme(
        bodyMedium: TextStyle(color: BaseTheme.fontLight),
        bodyLarge: TextStyle(color: BaseTheme.fontLight),
        bodySmall: TextStyle(color: BaseTheme.fontLight),
        titleLarge: TextStyle(color: BaseTheme.fontLight),
      ),
      appBarTheme: AppBarTheme(
          // systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: BaseTheme.navbarBgLight),
      colorScheme: ColorScheme.light(primary: BaseTheme.fontLight),
      cardColor: BaseTheme.subTitleLight,
      checkboxTheme: const CheckboxThemeData(),
      radioTheme: RadioThemeData(
          fillColor: MaterialStateProperty.all(BaseTheme.fontLight)));

  ThemeData dark = ThemeData(
      useMaterial3: false,
      extensions: [CustomTheme.dark],
      scaffoldBackgroundColor: BaseTheme.dark2Dark,
      iconTheme: IconThemeData(color: BaseTheme.fontDark),
      primaryColor: Colors.white,
      // textSelectionTheme: TextSelectionThemeData(cursorColor: BaseTheme.errorColorDark),
      textTheme: TextTheme(
        bodyMedium: TextStyle(color: BaseTheme.fontDark),
        bodyLarge: TextStyle(color: BaseTheme.fontDark),
        bodySmall: TextStyle(color: BaseTheme.fontDark),
        titleLarge: TextStyle(color: BaseTheme.fontDark),
      ),
      appBarTheme: AppBarTheme(
          // systemOverlayStyle: SystemUiOverlayStyle.dark,
          backgroundColor: BaseTheme.navbarBgDark),
      colorScheme: ColorScheme.dark(primary: BaseTheme.fontDark),
      cardColor: BaseTheme.subTitleDark,
      checkboxTheme: CheckboxThemeData(
          // fillColor: MaterialStateProperty.all(BaseTheme.errorColorDark),
          // overlayColor: MaterialStateProperty.all(Colors.transparent),
          ),
      radioTheme: RadioThemeData(
          fillColor: MaterialStateProperty.all(BaseTheme.fontDark)));

  ThemeMode mode = ThemeMode.system;

  // 0 -> 夜间 1 -> 日间
  final model = (ThemeMode.system).obs;

  SystemUiOverlayStyle get systemOverlay => model.value == ThemeMode.dark
      ? SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Colors.transparent,
          systemNavigationBarColor: Colors.black,
          statusBarIconBrightness: Brightness.light)
      : SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Colors.transparent,
          systemNavigationBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark);

  final String theme = "";
  // final String theme = "";

  updateStatusBar() {
    // #兼容 web，非 web 直接打开
    if (!kIsWeb) {
      LogUtil.w("mode____$mode");
      SystemUiOverlayStyle uiStyle = SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Colors.transparent,
          systemNavigationBarColor: Colors.white,
          statusBarIconBrightness: Brightness.light);

      if (mode == ThemeMode.dark) {
        uiStyle = SystemUiOverlayStyle.light.copyWith(
            statusBarColor: Colors.transparent,
            systemNavigationBarColor: Colors.black,
            statusBarIconBrightness: Brightness.light);
      }

      /// 系统主题时，判断一下主题
      if (mode == ThemeMode.system) {
        if (Get.isDarkMode) {
          uiStyle = SystemUiOverlayStyle.light.copyWith(
              statusBarColor: Colors.transparent,
              systemNavigationBarColor: Colors.black,
              statusBarIconBrightness: Brightness.light);
        }
        LogUtil.w("是否是深色布局___${Get.isDarkMode}");
      } else {
        uiStyle = SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Colors.transparent,
            systemNavigationBarColor: Colors.white,
            statusBarIconBrightness: Brightness.light);
      }
      SystemChrome.setSystemUIOverlayStyle(uiStyle);
    }
  }

  handleSetTheme(ThemeMode value) {
    // final current = ThemeState.getType(value);
    StorageUtils().save(StorageKeys.theme, "$value");
    mode = value;
    model.value = value;
    Get.changeThemeMode(mode);
    updateStatusBar();
  }

  // 动态修改主题 - 主题配置了不同的颜色时
  // 使用时机，进去启动页时在使用
  updateAppTheme() {
    final current = ThemeObj.getCurrentTheme(theme);
    LogUtil.w("current____$current");
    if (current != null) {
      // if (Get.isDarkMode) {
      //   dark = dark.copyWith(extensions: [current['dark']!]);
      //    light = light.copyWith(extensions: [current['light']!]);
      // } else {
      //   light = light.copyWith(extensions: [current['light']!]);
      // }
      dark = dark.copyWith(extensions: [current['dark']!]);
      light = light.copyWith(extensions: [current['light']!]);
      AppTheme().updateTheme(current);
      // Get.forceAppUpdate();
    }
  }

  @override
  void onInit() {
    final currentTheme = StorageUtils().read(StorageKeys.theme);
    LogUtil.w("当前主题___${currentTheme} ${dark.iconTheme.color}");

    /// 测试数据
    updateAppTheme();
    if (currentTheme != null) {
      mode = themeLocal[currentTheme] ?? ThemeMode.system;
      model.value = themeLocal[currentTheme] ?? ThemeMode.system;
      updateStatusBar();
    }

    super.onInit();
  }
}
