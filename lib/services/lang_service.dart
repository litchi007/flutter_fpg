import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:get/get.dart';

const localMap = {
  "zh_CN": Locale("zh", "CN"),
  "en_US": Locale("en", "US"),
  "zh_TW": Locale("zh", "TW")
};

enum LangState {
  cn(Locale("zh", "CN"), "简体中文", "zh"),
  us(Locale("en", "US"), "English", "en"),
  system(Locale("en", "US"), "跟随系统", "en"),
  tw(Locale("zh", "TW"), "繁体中文", "tw");

  const LangState(this.number, this.value, this.langCode);

  final Locale number;

  final String value;
  final String langCode;
  static Locale getLocal(Locale number) => LangState.values
      .firstWhere((activity) => activity.number == number)
      .number;

  static String getValue(Locale number) => LangState.values
      .firstWhere((activity) => activity.number == number)
      .value;

  static String getLangCode(Locale code) {
    return LangState.values
        .firstWhere((activity) => activity.number == code)
        .langCode;
  }

  static Locale getStrValue(String code) => LangState.values
      .firstWhere((activity) => activity.langCode == code)
      .number;
}

class LangService extends GetxService {
  static LangService get to => Get.find<LangService>();

  Locale lang = LangState.getLocal(const Locale("zh", "CN"));

  final langCode = (LangState.getValue(const Locale("zh", "CN"))).obs;

  handleSetLang([Locale? value]) {
    Locale _local =
        value ?? LangState.getStrValue(Get.deviceLocale?.languageCode ?? "zh");
    LogUtil.w(Get.deviceLocale?.languageCode);
    lang = _local;
    langCode.value = LangState.getValue(_local);
    LogUtil.w("语言切换 $value");
    Get.updateLocale(lang);
    Get.forceAppUpdate();
    // Get.updateLocale(Locale("zh", "TW"));
    StorageUtils().save(StorageKeys.lang, "$value");
  }

  @override
  void onInit() {
    final currentLang = StorageUtils().read(StorageKeys.lang);
    LogUtil.w("langs____${localMap[currentLang]} $currentLang");
    if (currentLang != null) {
      lang = localMap[currentLang] ?? const Locale("en", "US");
      langCode.value = LangState.getValue(lang);
    }
    super.onInit();
  }
}
