import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:fpg_flutter/api/app/app.dart';
import 'package:fpg_flutter/api/system/system.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/http/api_interface/app_info_http_client.dart';
import 'package:fpg_flutter/http/api_interface/http_core.dart';
import 'package:fpg_flutter/main.dart';
import 'package:fpg_flutter/models/app_info_app_id_model/app_info_app_id_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/guest_login_model/guest_login_model.dart';
import 'package:fpg_flutter/models/user_balance_model/user_balance_model.dart';
import 'package:fpg_flutter/utils/common_util.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/hot_upgrade_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:restart_app/restart_app.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class GlobalService extends GetxService {
  static String appInfoKey = '@appInfo';
  static GlobalService get to => Get.find<GlobalService>();
  final appUserRepository = AppUserRepository().obs;
  late AppInfoHttpClient appInfoHttp;
  bool rechargePopUpAlarmShowing = true;
  final storage = GetStorage();

  /// 是否开启性能监控
  final performanceStatus = false.obs;
  RxString token = "".obs;
  final userLogin = (const GuestLoginModel()).obs;
  Rx<AppInfoAppIdModel> appInfo = const AppInfoAppIdModel().obs;
  Rxn<UGUserModel> userInfo = Rxn();
  Rxn<UGSysConfModel> systemConfig = Rxn();

  /// 机选随机数
  final robotRandom = (UuidUtil.to.v4()).obs;

  /// 删除的选中的投注项
  final deleteGroup = (const PlayGroup()).obs;

  /// 用户金额
  final _userBalance = const UserBalanceModel().obs;
  UserBalanceModel get userBalance => _userBalance.value;
  set userBalance(UserBalanceModel value) {
    _userBalance.value = value;
  }

  /// 彩票页面当前选择的彩种
  final currentLotteryId = "".obs;

  /// 删除所有选项随机数
  final deleteAllRobotRndom = (UuidUtil.to.v4()).obs;
  final packageInfo =
      (PackageInfo(appName: "", packageName: '', version: '', buildNumber: ''))
          .obs;
  final versionLast = 0.obs;

  RxBool isAuthenticated = false.obs;

  Future<void> handlePerformance() async {
    performanceStatus.value = !performanceStatus.value;
    await StorageUtils().save(
        StorageKeys.performanceStatus, jsonEncode(performanceStatus.value));

    ToastUtils.show("性能图层已${performanceStatus.value ? '打开' : '关闭'},请重启APP");
    // Restart.restartApp(webOrigin: AppRoutes.home);
    // exit(0);
  }

  changeUserDetail(GuestLoginModel value) {
    userLogin.value = value;
  }

  handleDeleteAllRobotRndom() {
    deleteAllRobotRndom.value = (UuidUtil.to.v4());
  }

  changeToken(String apiSid, String apiToken) {
    token.value = apiSid;
    userLogin.value = GuestLoginModel(apiSid: apiSid, apiToken: apiToken);
  }

  changeDeleteGroup(PlayGroup value) {
    deleteGroup.value = value;
  }

  changeAppInfo(AppInfoAppIdModel model) async {
    appInfo.value = model;
    // 设置请求基础路径
    LogUtil.w("请求基础路径__$model");
    try {
      String domain = await _testSpeed(model);
      initToken();
      AppDefine.host = domain;
      AppDefine.domains = model.packageDomains ?? [];
      // init appUserrep
      appUserRepository.value = AppUserRepository();
      getUserInfo();
    } catch (e) {
      LogUtil.e(tag: '_setAppDefine error', e);
    }
  }

  getAppInfo() async {
    final info = await PackageInfo.fromPlatform();
    packageInfo.value = info;
    versionLast.value = (await shorebirdCodePush.currentPatchNumber()) ?? 0;

    print("当前版本${versionLast.value}");
  }

  Future<void> fetchAppInfo() async {
    try {
      AppInfoAppIdModel? data = await _fetchRemoteAppInfo();
      if (data != null) {
        await changeAppInfo(data);
        LogUtil.i('appInfo: got remote');
      } else {
        LogUtil.i('appInfo: remote fail');
      }
    } catch (e) {
      LogUtil.e('_fetchRemoteAppInfo error');
    }
  }

  Future<AppInfoAppIdModel?> _fetchLocalAppInfo() async {
    try {
      return storage.read(appInfoKey);
    } catch (_) {
      return null;
    }
  }

  Future<AppInfoAppIdModel?> _fetchRemoteAppInfo() async {
    final keyConfig = AppDefine.keyConfig;
    if (keyConfig == null) return null;
    try {
      final response = await appInfoHttp.getAppInfo(appId: keyConfig.appId);
      return response.data;
    } catch (e) {
      return null;
    }
  }

  Future<GlobalService> init() async {
    appInfoHttp =
        AppInfoHttpClient(AppHttpCore(baseUrl: AppDefine.appInfoDomain).dio);
    getAppInfo();
    await fetchAppInfo();
    Future.microtask(() {
      EventUtils.sleep(3000.milliseconds).then((value) {
        HotUpgradeUtils.currentPatchNumber();
      });
      try {
        performanceStatus.value = (jsonDecode(
            StorageUtils().read(StorageKeys.performanceStatus) ??
                jsonEncode(false)));

        LogUtil.w(
            "是否开启性能图层 ${(StorageUtils().read(StorageKeys.performanceStatus))}");
      } catch (e) {}
    });
    return this;
  }

  getConfig() {
    AppSystemRepository().fetchConfig();
  }

  late var homeRightMenus = [];
  Future<void> getRightMenus() async {
    var res = await SystemHttp.systemMobileRight();
    if (res.models != null && res.models!.isNotEmpty) {
      homeRightMenus = res.models!;
      storage.write(StorageKeys.RIGHTMENU, res.models);
    } else {
      homeRightMenus = storage.read(StorageKeys.RIGHTMENU);
    }
    LogUtil.w('homeRightMenus:$homeRightMenus');
  }

  @override
  void onReady() {
    // init();
    getConfig();
    Get.forceAppUpdate();

    /// 提前加载首页右侧导航栏数据以及缓存数据
    getRightMenus();
    super.onReady();
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  void downloadApp() async {
    if (AppDefine.systemConfig?.appDownloadUrl != null) {
      final Uri url = Uri.parse(AppDefine.systemConfig?.appDownloadUrl ?? "");
      await launchUrl(url);
    }
  }

  Future<String> _testSpeed(AppInfoAppIdModel info) async {
    // Get all domains, remove duplicates and filter out invalid ones
    // List<String> domains = ([
    //   ...info.packageDomains,
    //   info.siteUrl,
    // ]
    //     .where((domain) => CommonUtil.isValidUrl(domain))
    //     .map((domain) => CommonUtil.removeLastSlash(domain))).toList();

    // If no domains, use site_url
    // if (domains.isEmpty) {
    //   return CommonUtil.removeLastSlash(info.siteUrl);
    // }

    // Speed test timeout set to 2.5 seconds
    // NetworkingOptions options = NetworkingOptions(
    //   timeout: 2500,
    //   abort: true,
    // );

    // List<Future<MResponseBase>> promises = domains.map((domain) async {
    //   return await Networking.get('$domain$_serverTime', options: options);
    // }).toList();

    // List<MResponseBase> responses = await Future.wait(promises);

    // if (responses.isNotEmpty && responses[0] != null) {
    //   if (responses[0] is MResponseError) {
    //     // setError((responses[0] as ResponseError).message);
    //   }
    // }

    // responses.forEach((response) {
    //   response.url = response.url.replaceAll(_serverTime, '');
    // });

    // Keep non-error and code 0 results, sort by used time, extract URL
    // List<String> results = (responses
    //         .where((response) =>
    //             response is! MResponseError &&
    //             json.encode(response).contains('"code":0,'))
    //         .toList()
    //       ..sort((a, b) => a.used.compareTo(b.used))
    //       ..map((response) => response.url).toList())
    //     .cast<String>();

    // ugLog('results: ${json.encode(results)}');

    // Log errors
    // responses
    //     .where((response) => !results.contains(response.url))
    //     .forEach((error) {
    //   // ugLog('error: ${json.encode(error)}');
    //   // Logger.error(json.encode(error), Logger.callerTag('AppContextProvider', '_testSpeed'));
    // });

    // Return the first result, or use site_url if no results
    // return results.isNotEmpty
    //     ? results[0]
    //     :
    return CommonUtil.removeLastSlash((info.packageDomains ?? []).isNotEmpty
        ? info.packageDomains![0] != ""
            ? info.packageDomains![0]
            : info.siteUrl ?? ''
        : info.siteUrl ?? '');
  }

  Future<void> getUserInfo() async {
    LogUtil.w('token:------${token.value}');
    await appUserRepository.value.getUserInfo(token: token.value).then((data) {
      LogUtil.i(jsonEncode(data));
      if (data != null) {
        userInfo.value = data;
        storage.write(StorageKeys.USERINFO, data);
        isAuthenticated.value = true;
      } else {
        removeToken();
        LogUtil.i('remove Token');
      }
    }).catchError((data) {
      removeToken();
      LogUtil.i('catch error remove Token');
    });
  }

  Future<void> getPeriodicUserInfo() async {
    await appUserRepository.value.getUserInfo(token: token.value).then((data) {
      if (data != null) {
        userInfo.value = data;
      }
    }).catchError((data) {
      LogUtil.i('getPeriodicUserInfo error  ');
    });
  }

  /// 获取用户金币
  Future<void> fetchUserBalance() async {
    if (token.isNotEmpty) {
      final res = await UserHttp.userBalance();
      if (res.data != null) {
        userBalance = res.data!;
      }
    }
  }

  void initToken() {
    try {
      var token = storage.read(StorageKeys.USERTOKEN);
      if (token != null) {
        AppDefine.userToken =
            UserToken.fromJson(storage.read(StorageKeys.USERTOKEN));
        print('${AppDefine.userToken?.toJson()}');
        changeToken(AppDefine.userToken?.apiSid ?? '',
            AppDefine.userToken?.apiToken ?? '');
      }
    } catch (e) {
      print({'userToken error': e});
    }
  }

  void removeToken() {
    storage.write(StorageKeys.USERTOKEN, null);
    AppDefine.userToken = null;
    changeToken('', '');
    storage.write(StorageKeys.USERINFO, null);
    userInfo.value = null;
    isAuthenticated.value = false;
  }

  void updateToken(UserToken? newtoken) {
    changeToken(newtoken?.apiSid ?? '', newtoken?.apiToken ?? '');
    storage.write(StorageKeys.USERTOKEN, newtoken);
    AppDefine.userToken = newtoken;
  }

  /// 是否是试玩账号
  static bool get isTest => to.userInfo.value?.isTest ?? false;

  /// 是否显示试玩提示
  static bool get isShowTestToast {
    if (isTest) {
      ToastUtils.show('试玩账号无权访问，请先注册');
    }
    return isTest;
  }
}
