library http;

import 'package:fpg_flutter/http/dio_utils.dart';
import 'package:fpg_flutter/http/service.dart';

var http = DioUtils.getInstance();

final httpService = ApiService(dio: http);
