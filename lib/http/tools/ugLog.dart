const bool B_DEBUG = true; // Set to true for debug, false for production

// Log function
void ugLog(List<dynamic> s) {
  if (B_DEBUG) {
    print('[Log] ${s.join(' ')}');
  }
}

// Error log function
void ugError(List<dynamic> s) {
  if (B_DEBUG) {
    print('[Error] ${s.join(' ')}');
  }
}
