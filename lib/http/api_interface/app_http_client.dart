import 'package:dio/dio.dart';
import 'package:fpg_flutter/app/component/home_menu/model/HomeMenuModel.dart';
import 'package:fpg_flutter/data/models/AvatarModel.dart';
import 'package:fpg_flutter/data/models/CommentModel.dart';
import 'package:fpg_flutter/data/models/LhcdocContent.dart';
import 'package:fpg_flutter/data/models/LhlDataModel.dart';
import 'package:fpg_flutter/data/models/PostDetailModel.dart';
import 'package:fpg_flutter/data/models/LhcdocContentList.dart';
import 'package:fpg_flutter/data/models/BannersData.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatMessageModel.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatTokenModel.dart';
import 'package:fpg_flutter/data/models/ColdHotModel.dart';
import 'package:fpg_flutter/data/models/FollowModel.dart';
import 'package:fpg_flutter/data/models/GameCategory.dart';
import 'package:fpg_flutter/data/models/HallGameModel.dart';
import 'package:fpg_flutter/data/models/HomeAdsModel.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/data/models/ImgCaptchaModel.dart';
import 'package:fpg_flutter/data/models/LhcForum2Model.dart';
import 'package:fpg_flutter/data/models/LhcNoListModel.dart';
import 'package:fpg_flutter/data/models/LhcdocCategoryModel.dart';
import 'package:fpg_flutter/data/models/LotteryHistoryModel.dart';
import 'package:fpg_flutter/data/models/LotteryNumberData.dart';
import 'package:fpg_flutter/data/models/NextIssueData.dart';
import 'package:fpg_flutter/data/models/NoticeMode.dart';
import 'package:fpg_flutter/data/models/QueryAssisModel.dart';
import 'package:fpg_flutter/data/models/RankUserModel.dart';
import 'package:fpg_flutter/data/models/RealGameTypeModel.dart';
import 'package:fpg_flutter/data/models/RedBagDetail.dart';
import 'package:fpg_flutter/data/models/RedBagGrabModel.dart';
import 'package:fpg_flutter/data/models/TkListModel.dart';
import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:fpg_flutter/data/models/UGPromoteListModel.dart';
import 'package:fpg_flutter/data/models/UGRegisterModel.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/data/models/UserBalance.dart';
import 'package:fpg_flutter/data/models/UserMsgList.dart';
import 'package:fpg_flutter/data/models/UserProfileModel.dart';
import 'package:fpg_flutter/data/models/activitys/ScratchListModel.dart';
import 'package:fpg_flutter/data/models/system_config_model/system_config_model.dart';
import 'package:fpg_flutter/data/models/userModel/lhc_fans_list_model.dart';
import 'package:fpg_flutter/data/models/wd/PayAisleModel.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:fpg_flutter/data/models/activitys/PrizeData.dart';
import 'package:fpg_flutter/data/models/wd/CapitalDetailModel.dart';
import 'package:fpg_flutter/data/models/wd/WithdrawalRecordModel.dart';
import 'package:fpg_flutter/data/models/wd/DepositRecordModel.dart';
import 'package:fpg_flutter/data/models/ApplyWinLog.dart';
import 'package:fpg_flutter/data/models/WinApplyList.dart';
import 'package:fpg_flutter/data/models/chatModel/ChatUserInfoResModel.dart';
import 'package:fpg_flutter/data/models/MissionModel.dart';
import 'package:fpg_flutter/data/models/FeedbackData.dart';
import 'package:fpg_flutter/data/models/UGBetsRecordModel.dart';
import 'package:fpg_flutter/data/models/UGinviteInfoModel.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/BankCardDataModel.dart';
import 'package:fpg_flutter/data/models/BankListDetailModel.dart';
import 'package:fpg_flutter/data/models/userModel/userTransData.dart';
import 'package:fpg_flutter/data/models/UGSignInModel.dart';
import 'package:fpg_flutter/models/golden_egg_list_model/golden_egg_list_model.dart';
import 'package:fpg_flutter/models/turntable_list_model/turntable_list_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:retrofit/retrofit.dart';
import 'package:fpg_flutter/data/models/RealGamesModel.dart';
import 'package:fpg_flutter/data/models/YuebaoData.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/deal.dart';
import 'package:fpg_flutter/data/models/LhcdocModel.dart';
import 'package:fpg_flutter/data/models/grabOrderModel/list.dart';
import 'package:fpg_flutter/data/models/MissionData.dart';
import 'package:fpg_flutter/data/models/changLongModel/ChangLongModel.dart';
import 'package:fpg_flutter/data/models/changLongModel/LotteryRecordModel';
import 'package:fpg_flutter/data/models/redEnvelop/RedEnvelopModel.dart';
import 'package:fpg_flutter/data/models/redEnvelop/RedPackedLogModel.dart';
import 'package:fpg_flutter/data/models/ApplyWinLogDetail.dart';
import 'package:fpg_flutter/data/models/activitys/SettingsModel.dart';

part 'app_http_client.g.dart';

@RestApi()
abstract class AppHttpClient {
  factory AppHttpClient(Dio dio, {String baseUrl}) = _AppHttpClient;

  // api system
  @GET('/wjapp/api.php?c=system&a=homeAds')
  Future<NetBaseListEntity<HomeAdsModel>> getHomeAds();

  @GET('/wjapp/api.php?c=system&a=banners')
  Future<NetBaseEntity<BannersData>> getBanners();

  // 获取系统配置信息
  @GET('/wjapp/api.php?c=system&a=config')
  Future<NetBaseEntity<UGSysConfModel>> getSystemConfig();

  // 获取首页优惠活动
  @GET('/wjapp/api.php?c=system&a=promotions')
  Future<NetBaseEntity<UGPromoteListModel>> getPromotions();

  // 获取首页浮窗
  @GET('/wjapp/api.php?c=system&a=floatAds')
  Future<NetBaseEntity<dynamic>> floatAds();

  // api notice
  @GET('/wjapp/api.php?c=notice&a=latest')
  Future<NetBaseEntity<HomeNoticeData>> getLatestNotices();

  // api activity

  // 红包详情
  @GET('/wjapp/api.php?c=activity&a=redBagDetail')
  Future<NetBaseEntity<RedBagDetail>> redBagDetail();

  @GET('/wjapp/api.php?c=activity&a=settings')
  Future<NetBaseEntity<ActivitySettingsModel>> settings();

  @GET('/wjapp/api.php?c=activity&a=turntableList')
  Future<NetBaseListEntity<TurntableListModel>> turntableList();

  // 红包雨详情
  @GET('/wjapp/api.php?c=activity&a=redBagRainDetail')
  Future<NetBaseEntity<dynamic>> redBagRainDetail({
    @Query('token') String? token,
  });

  // 获取首页砸金蛋活动
  @GET('/wjapp/api.php?c=activity&a=goldenEggList')
  Future<NetBaseListEntity<GoldenEggListModel>> goldenEggList();

  // 获取首页砸金蛋活动
  @GET('/wjapp/api.php?c=activity&a=scratchList')
  Future<NetBaseEntity<ScratchListModel>> scratchList({
    @Query('token') String? token,
  });
  @GET('/wjapp/api.php?c=activity&a=scratchLog')
  Future<NetBaseEntity<PrizeData>> scratchLog({
    @Query('token') String? token,
    @Query('activityId') String? activityId,
  });

  @POST('/wjapp/api.php?c=activity&a=scratchWin')
  Future<NetBaseEntity<dynamic>> scratchWin({
    @Field('token') String? token,
    @Field('scratchId') String? scratchId,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=loginRegPageInfo')
  Future<NetBaseEntity<LoginRedPageInfoModel>> loginRegPageInfo();

  // api game

  // 获取首页游戏列表
  @GET('/wjapp/api.php?c=game&a=homeGames')
  Future<NetBaseEntity<HomeGamesData>> getHomeGames({
    @Query('token') String? token,
  });

  // 获取游戏大厅数据
  @GET('/wjapp/api.php?c=game&a=homeRecommend')
  Future<NetBaseListEntity<GameCategory>> getHomeRecommend();

  // 获取彩票大厅数据
  @GET('/wjapp/api.php?c=game&a=lotteryGames')
  Future<NetBaseListEntity<HallGameData>> getLotteryGames();

  // 获取彩票大厅数据
  @GET('/wjapp/api.php?c=game&a=lotteryGroupGames')
  Future<NetBaseListEntity<GroupGameData>> getLotteryGroupGames({
    @Query('showIssueType') int? showIssueType = 0,
  });

  // 筹码数据
  @GET('/wjapp/api.php?c=game&a=chipList')
  Future<NetBaseEntity<dynamic>> getChipList();

  @GET('/wjapp/api.php?c=game&a=lotteryHistory')
  Future<NetBaseEntity<LotteryHistoryModel>> getLotteryHistory({
    @Query('id') String? id,
    @Query('date') String? date,
    @Query('rows') int? rows,
  });

  // rankingList

  @GET('/mobile/game/getRanklist')
  Future<NetBaseListEntity<RankUserModel>> getRanklist();

  @GET('/wjapp/api.php?c=system&a=avatarList')
  Future<NetBaseListEntity<AvatarModel>> getAvatarList();

  @GET('/wjapp/api.php?c=secure&a=imgCaptcha')
  Future<NetBaseEntity<ImgCaptchaModel>> getImgCaptcha(
      {@Query('accessToken') String? accessToken});

  // api user

  // 获取用户信息（我的页）
  @GET('/wjapp/api.php?c=user&a=info')
  Future<NetBaseEntity<UGUserModel>> getUserInfoAPI({
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=user&a=myFeedback')
  Future<NetBaseEntity<FeedbackDataList>> getRecord({
    @Query('token') String? token,
    @Query('type') int? type,
    @Query('date') String? date,
    @Query('isReply') int? isReply,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  //
  @POST('/wjapp/api.php?c=user&a=uploadFeedback')
  Future<NetBaseEntity<bool>> uploadFeedback(
      @Queries() Map<String, String>? queries);
  //
  @POST('/wjapp/api.php?c=user&a=addFeedback')
  Future<NetBaseEntity<dynamic>> addFeedback({
    @Field('token') String? token,
    @Field('type') String? type,
    @Field('pid') String? pid,
    @Field('content') String? content,
    @Field('imgPaths') List<String>? imgPaths,
  });
  @POST('/wjapp/api.php?c=activity&a=applyWin')
  Future<NetBaseEntity<dynamic>> applyWin({
    @Field('token') String? token,
    @Field('userComment') String? userComment,
    @Field('id') String? id,
    @Field('amount') String? amount,
    @Field('imgCode') String? imgCode,
    @Field('files') List<String>? files,
  });
  // 获取用户余额
  @GET('/wjapp/api.php?c=user&a=balance')
  Future<NetBaseEntity<UserBalance>> getBalance();

  // 获取站内消息列表
  @GET('/wjapp/api.php?c=user&a=msgList')
  Future<NetBaseEntity<UserMsgList>> msgList({
    @Query('token') String? token,
    @Query('page') int? page,
    @Query('row') int? row,
  });

  @POST('/wjapp/api.php?c=user&a=transfer')
  Future<NetBaseEntity<dynamic>> transfer({
    @Field('token') String? token,
    @Field('money') double? money,
    @Field('fromType') String? fromType,
    @Field('toType') String? toType,
    @Field('pwd') String? pwd,
    @Field('rate') double? rate,
  });

  // 登录试玩账号
  @POST('/wjapp/api.php?c=user&a=guestLogin')
  Future<NetBaseEntity<UGLoginModel>> guestLogin({
    @Field('usr') String? usr,
    @Field('pwd') String? pwd,
  });

  //注册
  @POST('/wjapp/api.php?c=user&a=reg')
  Future<NetBaseEntity<UGRegisterModel>> register({
    @Field('usr') String? usr,
    @Field('pwd') String? pwd,
    @Field('device') String? device,
    @Field('fullName') String? fullName,
    @Field('inviter') String? inviter,
    @Field('inviteCode') String? inviteCode,
    @Field('fundPwd') String? fundPwd,
    @Field('wx') String? wx,
    @Field('qq') String? qq,
    @Field('email') String? email,
    @Field('imgCode') String? imgCode,
    @Field('accessToken') String? accessToken,
    @Field('smsCode') String? smsCode,
    @Field('phone') String? phone,
    @Field('fb') String? fb,
    @Field('line') String? line,
  });

  // 登录
  @POST('/wjapp/api.php?c=user&a=login')
  Future<NetBaseEntity<UGLoginModel>> login({
    @Field('token') String? token,
    @Field('oauthSms') String? oauthSms,
    @Field('usr') String? usr,
    @Field('pwd') String? pwd,
    @Field('fullName') String? fullName,
    @Field('ggCode') String? ggCode,
    @Field('imgCode') String? imgCode,
    @Field('device') String? device,
    @Field('accessToken') String? accessToken,
    @Field('slideCode') int? channel,
  });

  @POST('/wjapp/api.php?c=user&a=logout')
  Future<NetBaseEntity<dynamic>> logout({
    @Field('token') String? token,
    @Field('device') String? device,
  });

  // 修改登录密码
  @POST('/wjapp/api.php?c=user&a=changeLoginPwd')
  Future<NetBaseEntity<String>> changeLoginPwd({
    @Field('token') String? token,
    @Field('old_pwd') String? oldPwd,
    @Field('new_pwd') String? newPwd,
  });

  // 修改取款密码
  @POST('/wjapp/api.php?c=user&a=changeFundPwd')
  Future<NetBaseEntity<String>> changeFundPwd({
    @Field('token') String? token,
    @Field('old_pwd') String? oldPwd,
    @Field('new_pwd') String? newPwd,
  });

  // 修改取款密码
  @POST('/wjapp/api.php?c=user&a=addFundPwd')
  Future<NetBaseEntity<String>> addFundPwd({
    @Field('token') String? token,
    @Field('login_pwd') String? loginPwd,
    @Field('fund_pwd') String? fundPwd,
  });

  // 更新常用登录地点（待完善）
  @POST('/wjapp/api.php?c=user&a=changeAddress')
  Future<NetBaseEntity<String>> changeAddress({
    @Field('token') String? token,
    @Field('address') List<Address>? address,
  });

  // 常用登录地点列表
  @GET('/wjapp/api.php?c=user&a=address')
  Future<NetBaseListEntity<Address>> getAddress({
    @Query('token') String? token,
  });

  // 删除常用登录地点
  @POST('/wjapp/api.php?c=user&a=delAddress')
  Future<NetBaseEntity<String>> delAddress({
    @Field('token') String? token,
    @Field('id') String? id,
  });
  // 获取头像配置
  @POST('/wjapp/api.php?c=user&a=getAvatarSetting')
  Future<NetBaseEntity<AvatarSettingModel>> getAvatarSetting({
    @Field('token') String? token,
  });

  @POST('/wjapp/api.php?c=user&a=updateAvatar')
  Future<NetBaseEntity<dynamic>> updateUserAvatar(
      {@Field('token') String? token,
      @Field('publicAvatarId') String? publicAvatarId});

  // api LHC

  // 获取首页六合栏目列表
  @GET('/wjapp/api.php?c=lhcdoc&a=categoryList')
  Future<NetBaseListEntity<LhcdocCategoryModel>> getCategoryList(
      {@Query('token') String? token});

  // 六合论坛2信息
  @GET('/wjapp/api.php?c=lhcdoc&a=lhcForum2')
  Future<NetBaseEntity<LhcForum2Model>> getLhcForum2();

  // 当前开奖信息
  @GET('/wjapp/api.php?c=lhcdoc&a=lotteryNumber')
  Future<NetBaseEntity<LotteryNumberData>> getLotteryNumber({
    @Query('gameId') int? gameId,
    @Query('gameName') String? gameName,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=contentList')
  Future<NetBaseEntity<LhcdocContent>> getLhcContentList({
    @Query('key') String? key,
    @Query('alias') String? alias,
    @Query('page') int? page,
    @Query('rows') int? rows,
    @Query('uid') String? uid,
    @Query('pos') int? pos,
    @Query('model') String? model,
    // @Query('sort') String? sort,
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=historyContent')
  Future<NetBaseEntity<LhcdocContent>> getLhcHistoryContent({
    @Query('token') String? token,
    @Query('page') int? page,
    @Query('rows') int? rows,
    @Query('uid') String? uid,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=favContentList')
  Future<NetBaseListEntity<LhcdocContentList>> getFavContentList({
    @Query('token') String? token,
    @Query('page') int? page,
    @Query('rows') int? rows,
    @Query('uid') String? uid,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=followList')
  Future<NetBaseEntity<FollowModel>> getFollowList({
    @Query('token') String? token,
    @Query('page') int? page,
    @Query('rows') int? rows,
    @Query('uid') String? uid,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=contentDetail')
  Future<NetBaseEntity<PostDetailModel>> getLhcContentDetail({
    @Query('id') String? id,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=doFavorites')
  Future<NetBaseListEntity<dynamic>> doFavorites({
    @Query('id') String? id,
    @Query('favFlag') int? favFlag,
    @Query('type') int? type,
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=likePost')
  Future<NetBaseListEntity<dynamic>> likePost({
    @Query('rid') String? rid,
    @Query('likeFlag') int? likeFlag,
    @Query('type') int? type,
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=followPoster')
  Future<NetBaseListEntity<dynamic>> followPoster({
    @Query('posterUid') String? posterUid,
    @Query('followFlag') int? followFlag,
    @Query('token') String? token,
  });

  @POST('/wjapp/api.php?c=lhcdoc&a=postContentReply')
  Future<NetBaseEntity<dynamic>> postContentReply({
    @Field('token') String? token,
    @Field('cid') String? cid,
    @Field('rid') String? rid,
    @Field('toUid') String? toUid,
    @Field('content') String? content,
  });

  @POST('/wjapp/api.php?c=lhcdoc&a=vote')
  Future<NetBaseEntity<dynamic>> vote({
    @Field('token') String? token,
    @Field('cid') String? cid,
    @Field('animalId') int? animalId,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=getLhcFansRankData')
  Future<NetBaseEntity<FansRanking>> getLhcFansRankData({
    @Query('token') String? token,
    @Query('limit') int? limit,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=getLhcTipsRankData')
  Future<NetBaseEntity<TipsRanking>> getLhcTipsRankData({
    @Query('token') String? token,
    @Query('limit') int? limit,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=tkList')
  Future<NetBaseEntity<TkListModel>> getTkList({
    @Query('token') String? token,
    @Query('alias') String? alias,
    @Query('sort') String? sort,
    @Query('colorType') String? colorType,
    @Query('showFav') String? showFav,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=lhcNoList')
  Future<NetBaseEntity<LhcNoListModel>> getLhcNoList({
    @Query('token') String? token,
    @Query('type') String? type,
    @Query('type2') String? type2,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=coldHot')
  Future<NetBaseListEntity<ColdHotModel>> getColdHot({
    @Query('limit') int? limit,
    @Query('lhcType') String? lhcType,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=lhlDetail')
  Future<NetBaseEntity<dynamic>> getlhlItem();

  @GET('/wjapp/api.php?c=lhcdoc&a=queryAssis')
  Future<NetBaseEntity<QueryAssisModel>> getQueryAssis();

  @GET('/wjapp/api.php?c=lhcdoc&a=getUserInfo')
  Future<NetBaseEntity<UserProfileModel>> getUserInfo({
    @Query('uid') String? uid,
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=contentFansList')
  Future<NetBaseEntity<LhcFansListModel>> contentFansList({
    @Query('uid') String? uid,
    @Query('token') String? token,
    @Query('rows') String? rows,
    @Query('page') String? page,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=fansList')
  Future<NetBaseEntity<LhcFansListModel>> fansList({
    @Query('uid') String? uid,
    @Query('token') String? token,
    @Query('rows') String? rows,
    @Query('page') String? page,
  });

  @POST('/wjapp/api.php?c=lhcdoc&a=tipContent')
  Future<NetBaseEntity<dynamic>> tipContent({
    @Field('token') String? token,
    @Field('cid') String? cid,
    @Field('amount') String? amount,
  });

  @GET('/wjapp/api.php?c=lhcdoc&a=contentReplyList')
  Future<NetBaseListEntity<CommentModel>> contentReplyList({
    @Query('token') String? token,
    @Query('contentId') String? contentId,
    @Query('replyPId') String? replyPId,
    @Query('page') int? page = 1,
    @Query('rows') int? rows = 20,
  });

  // api chat
  @POST('/wjapp/api.php?c=chat&a=getToken')
  Future<NetBaseEntity<ChatTokenModel>> getChatToken(
      {@Field('token') String? token,
      @Field('t') String? t,
      @Field('roomId') String? roomId});

  @POST('/wjapp/api.php?c=chat&a=updateNickname')
  Future<NetBaseEntity<dynamic>> updateNickname(
      {@Field('token') String? token,
      @Field('text') String? text,
      @Field('roomId') String? roomId});

  @POST('/wjapp/api.php?c=chat&a=messageRecord')
  Future<NetBaseListEntity<ChatMessageModel>> getMessageRecord({
    @Field('token') String? token,
    @Field('sendUid') String? sendUid,
    @Field('chat_type') int? chatType,
    @Field('messageCode') String? messageCode,
    @Field('roomId') String? roomId,
    @Field('number') int? num,
  });

  @POST('/wjapp/api.php?c=chat&a=userInfo')
  Future<NetBaseEntity<ChatUserInfoResModel>> getUserInfoById({
    @Field('token') String? token,
    @Field('uid') String? uid,
  });

  @POST('/wjapp/api.php?c=chat&a=banned')
  Future<NetBaseEntity<ChatUserInfoResModel>> banned({
    @Field('token') String? token,
    @Field('uid') String? uid,
    @Field('operate') int? operate,
  });

  @POST('/wjapp/api.php?c=chat&a=operateNickname')
  Future<NetBaseEntity<ChatUserInfoResModel>> operateNickname({
    @Field('token') String? token,
    @Field('uid') String? uid,
    @Field('roomId') String? roomId,
  });

  @POST('/wjapp/api.php?c=chat&a=bannedIp')
  Future<NetBaseEntity<ChatUserInfoResModel>> bannedIp({
    @Field('token') String? token,
    @Field('uid') String? uid,
    @Field('operate') int? operate,
    @Field('ip') String? ip,
  });
  @POST('/wjapp/api.php?c=chat&a=getMemberInfo')
  Future<NetBaseEntity<ChatUserInfoResModel>> getMemberInfoById({
    @Field('token') String? token,
    @Field('uid') String? uid,
  });

  @POST('/wjapp/api.php?c=chat&a=getBetBills')
  Future<NetBaseEntity<dynamic>> getBetBills({
    @Field('token') String? token,
  });

  @POST('/wjapp/api.php?c=chat&a=createRedBag')
  Future<NetBaseEntity<dynamic>> createRedBag({
    @Field('token') String? token,
    @Field('genre') int? genre,
    @Field('amount') String? amount,
    @Field('roomId') String? roomId,
    @Field('title') String? title,
    @Field('quantity') int? quantity,
    @Field('mineNumber') String? mineNumber,
    @Field('coinPwd') String? coinPwd,
  });

  @POST('/wjapp/api.php?c=chat&a=redBag')
  Future<NetBaseEntity<RedBagGrabModel>> getRedBag({
    @Field('token') String? token,
    @Field('redBagId') String? redBagId,
  });

  @GET('/wjapp/api.php?c=game&a=nextIssue')
  Future<NetBaseEntity<NextIssueData>> getNextIssue({@Query('id') String? id});

  @GET('/wjapp/api.php?c=user&a=fundLogs')
  Future<NetBaseEntity<CapitalDetailListModel>> getCapitalDetailData({
    @Query('token') String? token,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') String? page,
    @Query('rows') String? rows,
    @Query('group') String? group,
  });

  @GET('/wjapp/api.php?c=recharge&a=logs')
  Future<NetBaseEntity<DepositRecordListModel>> getDepositRecordData({
    @Query('token') String? token,
    @Query('page') String? page,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('rows') String? rows,
  });

  @GET('/wjapp/api.php?c=withdraw&a=logs')
  Future<NetBaseEntity<WithdrawalRecordListModel>> getWithdrawalRecordData(
      {@Query('token') String? token,
      @Query('startDate') String? startDate,
      @Query('endDate') String? endDate,
      @Query('page') String? page,
      @Query('rows') String? rows});

  @POST('/wjapp/api.php?c=withdraw&a=apply')
  Future<NetBaseEntity<dynamic>> withdrawApply({
    @Field('token') String? token,
    @Field('id') String? id,
    @Field('money') int? money,
    @Field('pwd') String? pwd,
    @Field('virtual_amount') int? virtual_amount,
    @Field('isC2c') int? isC2c,
  });

  @GET('/wjapp/api.php?c=recharge&a=cashier')
  Future<NetBaseEntity<PayAisleData>> cashier({@Query('token') String? token});

  @GET('/wjapp/api.php?c=recharge&a=payLoginCoin')
  Future<NetBaseEntity<dynamic>> payLoginCoin({
    @Query('token') String? token,
    @Query('payId') String? payId,
  });

  @POST('/wjapp/api.php?c=recharge&a=transfer')
  Future<NetBaseEntity<dynamic>> transferForPay({
    @Field('token') String? token,
    @Field('channel') String? channel,
    @Field('payee') String? payee,
    @Field('payer') String? payer,
    @Field('amount') String? amount,
    @Field('remark') String? remark,
    @Field('depositTime') String? depositTime,
    @Field('upiAccount') String? upiAccount,
  });

  @POST('/wjapp/api.php?c=recharge&a=onlinePay')
  Future<String> onlinePay({
    @Field('token') String? token,
    @Field('payId') String? payId,
    @Field('money') String? money,
    @Field('gateway') String? gateway,
  });

  @GET('/wjapp/api.php?c=ticket&a=lotteryStatistics')
  Future<NetBaseEntity<UGBetsRecordListModel>> lotteryStatistics({
    @Query('token') String? token,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
  });

  @GET('/wjapp/api.php?c=ticket&a=history')
  Future<NetBaseEntity<UGBetsRecordDataLongModel>> gethistory({
    @Query('token') String? token,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
  });
  @GET('/wjapp/api.php?c=ticket&a=history')
  Future<NetBaseEntity<UGBetsRecordDataLongModel>> history({
    @Query('token') String? token,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('category') String? category,
    @Query('status') int? status,
    @Query('gameId') String? gameId,
    @Query('page') int? page,
    @Query('rows') int? rows,
    @Query('timeZone') String? timeZone,
  });

  @GET('/wjapp/api.php?c=ticket&a=history')
  Future<NetBaseEntity<UGBetsRecordDataLongModel>> getAllHistory({
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=ticket&a=lotteryData')
  Future<NetBaseEntity<dynamic>> getLotteryData({
    @Query('token') String? token,
  });

  @POST('/wjapp/api.php?c=task&a=sendMissionBonus')
  Future<NetBaseEntity<Null>> sendMissionBonus({
    @Field('bonsId') String? bonsId,
  });
  @POST('/wjapp/api.php?c=task&a=checkinBonus')
  Future<NetBaseEntity<String>> checkinBonus({
    @Field('token') String? token,
    @Field('type') String? type,
  });
  @POST('/wjapp/api.php?c=task&a=checkin')
  Future<NetBaseEntity<String>> checkin({
    @Field('token') String? token,
    @Field('type') String? type,
    @Field('date') String? date,
  });
  @GET('/wjapp/api.php?c=task&a=getMissionBonusListNew')
  Future<NetBaseEntity<NewSalaryModel>> getMissionBonusListNew(
      {@Query('token') String? token});
  // 任务大厅
  @GET('/wjapp/api.php?c=task&a=center')
  Future<NetBaseEntity<MissionModel>> center({
    @Query('token') String? token,
    @Query('category') String? category,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=sportsBetStat')
  Future<NetBaseEntity<M_RealBetStat>> sportsBetStat({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });

  @GET('/wjapp/api.php?c=task&a=categories')
  Future<NetBaseListEntity<MissionCategory>> categories({
    @Query('token') String? token,
  });
  @POST('/wjapp/api.php?c=task&a=get')
  Future<NetBaseEntity<MissionModel>> getTask({
    @Field('token') String? token,
    @Field('mid') String? mid,
  });
  @POST('/wjapp/api.php?c=task&a=reward')
  Future<NetBaseEntity<MissionModel>> getReward({
    @Field('token') String? token,
    @Field('mid') String? mid,
  });
  @POST('/wjapp/api.php?c=task&a=creditsExchange')
  Future<NetBaseEntity<String>> creditsExchange({
    @Field('token') String? token,
    @Field('money') double? money,
  });
  @GET('/wjapp/api.php?c=task&a=getMissionBonusList')
  Future<NetBaseListEntity<SalaryModel>> getMissionBonusList(
      {@Query('token') String? token});
  @GET('/wjapp/api.php?c=task&a=levels')
  Future<NetBaseListEntity<ScoreLevelData>> getLevels();
  @GET('/wjapp/api.php?c=task&a=creditsLog')
  Future<NetBaseEntity<CreditsLogModel>> getCreditsLog({
    @Query('token') String? token,
    @Query('time') String? time,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=task&a=checkinList')
  Future<NetBaseEntity<UGSignInModel>> checkinList({
    @Query('token') String? token,
  });
  @GET('/wjapp/api.php?c=task&a=checkinHistory')
  Future<NetBaseListEntity<UGSignInHistoryModel>> checkinHistory({
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=activity&a=winApplyList')
  Future<NetBaseEntity<WinApplyList>> fetchWinApplyList(
      {@Query('token') String? token});

  @GET('/wjapp/api.php?c=activity&a=applyWinLog')
  Future<NetBaseEntity<ApplyWinLog>> fetchApplyWinLog(
      {@Query('token') String? token});

  @GET('/wjapp/api.php?c=activity&a=applyWinLogDetail')
  Future<NetBaseEntity<ApplyWinLogDetail>> applyWinLogDetail({
    @Query('token') String? token,
    @Query('id') String? id,
  });

// Api by cezar
  //获取真人游戏列表
  @GET('/wjapp/api.php?c=game&a=realGames')
  Future<NetBaseListEntity<RealGameData>> getRealGames();

  @GET('/wjapp/api.php?c=game&a=changlong')
  Future<NetBaseListEntity<ChangLongModel>> getChanglong(
      {@Query('id') String? id});

  @GET('/wjapp/api.php?c=report&a=getUserRecentBet')
  Future<NetBaseListEntity<LotteryRecordModel>> getUserRecentBet({
    @Query('token') String? token,
    @Query('tag') int? tag,
  });

  // // 检查真人游戏是否存在余额未转出
  // @GET('/wjapp/api.php?c=real&a=checkTransferStatus')
  // // Future<NetBaseListEntity<RealGameData>> checkTransferStatus({
  // //   @Query('token') String? token,
  // //   @Query('startDate') DateTime? startDate,
  // // });
  // // 游戏中的余额自动转出
  // @POST('/wjapp/api.php?c=real&a=autoTransferOut')
  // // Future<NetBaseListEntity<RealGameData>> autoTransferOut({
  // //   @Query('token') String? token,
  // //   @Query('startDate') DateTime? startDate,
  // // });

  // 手动额度转换
  @POST('/wjapp/api.php?c=real&a=manualTransfer')
  Future<NetBaseEntity<String>> manualTransfer({
    @Field('token') String? token,
    @Field('fromId') String? fromId,
    @Field('toId') String? toId,
    @Field('money') String? money,
  });

  // 手动额度转换记录
  @GET('/wjapp/api.php?c=real&a=transferLogs')
  Future<NetBaseEntity<ResponseTransferLogs>> transferLogs({
    @Query('token') String? token,
    @Query('startTime') String? startTime,
    @Query('endTime') String? endTime,
    @Query('page') String? page,
    @Query('rows') String? rows,
  });

  // 额度一键转出，第一步：获取需要转出的真人ID
  @POST('/wjapp/api.php?c=real&a=oneKeyTransferOut')
  Future<NetBaseEntity<ResponseOneKeyTransferOutData>> oneKeyTransferOut({
    @Field('token') String? token,
  });

  // 额度一键转出，第二步：根据真人ID并发请求单游戏快速转出
  @POST('/wjapp/api.php?c=real&a=quickTransferOut')
  Future<NetBaseEntity<String>> quickTransferOut({
    @Field('token') String? token,
    @Field('id') String? id,
  });

  // 真人余额查询
  @GET('/wjapp/api.php?c=real&a=checkBalance')
  Future<NetBaseEntity<ResponseBalance>> checkBalance({
    @Query('token') String? token,
    @Query('id') String? id,
  });
  @GET('/wjapp/api.php?c=team&a=inviteInfo')
  Future<NetBaseEntity<UGinviteInfoModel>> inviteInfo({
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=team&a=inviteCodeList')
  Future<NetBaseEntity<InviteCodeList>> inviteCodeList({
    @Query('token') String? token,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=inviteList')
  Future<NetBaseEntity<InviteListModel>> inviteList({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=withdrawStat')
  Future<NetBaseEntity<M_WithdrawStatData>> withdrawStat({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=betStat')
  Future<NetBaseEntity<M_BetStat>> betStat({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=betList')
  Future<NetBaseEntity<M_BetList>> betList({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=realBetStat')
  Future<NetBaseEntity<M_RealBetStat>> realBetStat({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=realBetList')
  Future<NetBaseEntity<M_RealBetList>> realBetList({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=depositStat')
  Future<NetBaseEntity<M_DepositStatData>> depositStat({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('startDate') String? startDate,
    @Query('endDate') String? endDate,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=inviteDomain')
  Future<NetBaseEntity<M_InviteDomainList>> inviteDomain({
    @Query('token') String? token,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=depositList')
  Future<NetBaseEntity<M_DepositListtData>> depositList({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=team&a=withdrawList')
  Future<NetBaseEntity<M_WithdrawListData>> withdrawList({
    @Query('token') String? token,
    @Query('level') int? level,
    @Query('page') int? page,
    @Query('rows') int? rows,
  });
  @GET('/wjapp/api.php?c=language&a=getConfigs')
  Future<NetBaseEntity<LanguageData>> getconfigs({
    @Query('token') String? token,
  });

  @POST('/wjapp/api.php?c=team&a=updateInviteCodeStatus')
  Future<NetBaseEntity<Null>> updateInviteCodeStatus({
    @Field('String') String? token,
    @Field('id') int? id,
    @Field('status') int? status,
  });
  @POST('/wjapp/api.php?c=team&a=createInviteCode')
  Future<String> createInviteCode({
    @Field('String') String? token,
    @Field('length') int? length,
    @Field('count') int? count,
    @Field('user_type') int? user_type,
    @Field('randomCheck') int? randomCheck,
  });
  @POST('/wjapp/api.php?c=team&a=addMember')
  Future<NetBaseEntity<String>> addMember({
    @Field('token') String? token,
    @Field('usr') String? username,
    @Field('pwd') String? pwd,
    @Field('pwd2') String? pwd2,
    @Field('fullName') String? fullName,
    @Field('phone') String? phone,
    @Field('type') int? type = 0,
  });
  @POST('/wjapp/api.php?c=team&a=guestBet')
  Future<NetBaseEntity<String>> guestBet({
    @Field('token') String? token,
    @Field('betBean') List<Map<String, String>>? betBean,
    @Field('gameId') String? gameId,
    @Field('endTime') String? endTime,
    @Field('totalNum') String? totalNum,
    @Field('tag') String? tag,
    @Field('betIssue') String? betIssue,
    @Field('totalMoney') String? totalMoney,
    @Field('activeReturnCoinRatio') String? activeReturnCoinRatio,
    @Field('isInstant') String? isInstant,
  });
  @POST('/wjapp/api.php?c=team&a=bet')
  Future<NetBaseEntity<String>> bet({
    @Field('token') String? token,
    @Field('betBean') List<Map<String, String>>? betBean,
    @Field('gameId') String? gameId,
    @Field('endTime') String? endTime,
    @Field('totalNum') String? totalNum,
    @Field('tag') String? tag,
    @Field('betIssue') String? betIssue,
    @Field('totalMoney') String? totalMoney,
    @Field('activeReturnCoinRatio') String? activeReturnCoinRatio,
    @Field('isInstant') String? isInstant,
  });
  @POST('/wjapp/api.php?c=team&a=instantBet')
  Future<NetBaseEntity<String>> instantBet({
    @Field('token') String? token,
    @Field('betBean') List<Map<String, String>>? betBean,
    @Field('gameId') String? gameId,
    @Field('endTime') String? endTime,
    @Field('totalNum') String? totalNum,
    @Field('tag') String? tag,
    @Field('betIssue') String? betIssue,
    @Field('totalMoney') String? totalMoney,
    @Field('activeReturnCoinRatio') String? activeReturnCoinRatio,
    @Field('isInstant') String? isInstant,
  });

  @GET('/wjapp/api.php?c=yuebao&a=stat')
  Future<NetBaseEntity<YuebaoModel>> stat({
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=yuebao&a=profitReport')
  Future<NetBaseEntity<ProfitReportData>> profitReport({
    @Query('token') String? token,
  });

  @GET('/wjapp/api.php?c=user&a=transferLogs')
  Future<NetBaseEntity<UserTransData>> userTransferLogs({
    @Query('token') String? token,
    @Query('type') int? type,
  });

  @GET('/wjapp/api.php?c=system&a=currencyRate')
  Future<NetBaseEntity<ConverRateModel>> currencyRate({
    @Query('token') String? token,
    @Query('from') String? from,
    @Query('to') String? to,
    @Query('amount') String? amount,
    @Query('float') String? float,
  });

  @GET('/wjapp/api.php?c=system&a=mobileRight')
  Future<NetBaseListEntity<HomeMenuModel>> getMobileRightMenu();

  @GET('/mobile/qd/getRecords')
  Future<NetBaseEntity<RecordsData>> getGraborderRecords({
    @Query('token') String? token,
  });

  @GET('/mobile/qd/detail')
  Future<NetBaseEntity<Deal>> detailForGrabOrder({
    @Query('token') String? token,
    @Query('dealId') String? dealId,
  });

  @GET('/mobile/qd/getList')
  Future<NetBaseEntity<DealListData>> getList({
    @Query('token') String? token,
    @Query('type') ActionType? type,
    @Query('payChannel') List<int>? payChannel,
    @Query('amountMin') int? amountMin,
    @Query('amountMax') int? amountMax,
  });

  @POST('/mobile/qd/createDeposit')
  Future<NetBaseEntity<dynamic>> createDeposit({
    @Field('token') String? token,
    @Field('amount') String? amount,
    @Field('allow_bank_type') List<int>? allowBankType,
  });

  @POST('/mobile/qd/createWithdraw')
  Future<NetBaseEntity<dynamic>> createWithdraw({
    @Field('token') String? token,
    @Field('amount') String? amount,
    @Field('coinpwd') String? coinpwd,
    @Field('pay_channels') List<int>? payChannels,
  });

  // 我的提款账户列表
  @GET('/wjapp/api.php?c=user&a=bankCard')
  Future<NetBaseEntity<ManageBankCardData>> bankCard({
    @Query('token') String? token,
  });

  // 设置真实姓名
  @POST('/wjapp/api.php?c=user&a=profileName')
  Future<NetBaseEntity<String>> profileName({
    @Field('token') String? token,
    @Field('fullName') String? fullName,
  });
  //绑定邮箱
  @POST('/wjapp/api.php?c=user&a=changeEmail')
  Future<NetBaseEntity<String>> changeEmail({
    @Field('token') String? token,
    @Field('email') String? email,
  });
  // 忘记密码
  @POST('/wjapp/api.php?c=user&a=applyCoinPwd')
  Future<NetBaseEntity<dynamic>> applyCoinPwd({
    @Field('token') String? token,
    @Field('bankNo') String? bankNo,
    @Field('coinpwd') String? coinpwd,
    @Field('mobile') String? mobile,
    @Field('smsCode') String? smsCode,
    @Field('identityPathDot') String? identityPathDot,
  });

  // 提款渠道列表
  @GET('/wjapp/api.php?c=system&a=bankList')
  Future<NetBaseListEntity<BankDetailListData>> BankList({
    @Query('type') String? type,
    @Query('status') String? status,
  });

  // 绑定提款账户
  @POST('/wjapp/api.php?c=user&a=bindBank')
  Future<NetBaseEntity<String>> bindBank({
    @Field('token') String? token,
    @Field('type') String? type,
    @Field('bank_id') String? bankId,
    @Field('bank_addr') String? bankAddr,
    @Field('bank_card') String? bankCard,
    @Field('qrcode') String? qrcode,
    @Field('owner_name') String? ownerName,
    @Field('pwd') String? pwd,
  });

  // 编辑提款账户
  @POST('/wjapp/api.php?c=user&a=editBank')
  Future<NetBaseEntity<String>> editBank({
    @Field('token') String? token,
    @Field('id') String? id,
    @Field('bank_id') String? bankId,
    @Field('bank_addr') String? bankAddr,
    @Field('bank_card') String? bankCard,
    @Field('qrcode') String? qrcode,
    @Field('owner_name') String? ownerName,
    @Field('pwd') String? pwd,
  });

  // 获取三方游戏跳转URL
  @GET('/wjapp/api.php?c=real&a=gotoGame')
  Future<NetBaseEntity<String>> gotoGame({
    @Query('token') String? token,
    @Query('id') String? id,
    @Query('game') String? game, // 游戏ID、子游戏ID
  });

  @GET('/wjapp/api.php?c=game&a=realGameTypes')
  Future<NetBaseListEntity<RealGameTypeModel>> getRealGameTypes({
    @Query('id') String? id,
    @Query('search_text') String? search, // 游戏ID、子游戏ID
  });

  @GET('/wjapp/api.php?c=redPacket&a=getRedPacketList')
  Future<NetBaseEntity<RedEnvelopListModel>> getRedPacketList({
    @Query('token') String? token,
    @Query('action') String? action, // 游戏ID、子游戏ID
  });

  @GET('/wjapp/api.php?c=redPacket&a=getRedPacket')
  Future<NetBaseEntity<String>> getRedPacket({
    @Query('token') String? token,
    @Query('rpid') String? rpid, // 游戏ID、子游戏ID
  });

  @GET('/wjapp/api.php?c=redPacket&a=getRedPacketLogList')
  Future<NetBaseListEntity<RedpackedLogmodel>> getRedPacketLogList({
    @Query('token') String? token,
    @Query('rpid') String? rpid, // 游戏ID、子游戏ID
  });

  @POST('/wjapp/api.php?c=user&a=readMsg')
  Future<NetBaseEntity<dynamic>> readMsg({
    @Field('token') String? token,
    @Field('id') String? id, //
  });

  @POST('/wjapp/api.php?c=user&a=grantWebAccessToken')
  Future<NetBaseEntity<dynamic>> grantWebAccessToken({
    @Field('token') String? token,
  });

  @POST('/wjapp/api.php?c=lhcdoc&a=setNickname')
  Future<NetBaseEntity<dynamic>> setNickname({
    @Field('token') String? token,
    @Field('nickname') String? nickname,
  });

  @POST('/wjapp/api.php?c=activity&a=getRedBag')
  Future<NetBaseEntity<dynamic>> getRedBag_activity({
    @Field('token') String? token,
    @Field('id') String? id,
  });
}
