import 'package:fpg_flutter/http/api_interface/json_bean_factory.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

class NetBaseEntity<T> {
  late int code;
  late String msg;
  bool? nicknameFlag;
  bool? success;
  T? data;

  bool get isSuccess {
    return code == 200;
  }

  bool get noAuth {
    return code == 14010;
  }

  NetBaseEntity.fromJson(dynamic json) {
    code = json['code'];
    msg = json['msg'] ?? '';
    nicknameFlag = json['nicknameFlag'];
    success = json['success'];
    // if (code == 1 || code == 0) {
    if (json['data'] != null) {
      try {
        if (T == String) {
          data = json['data'].toString() as T?;
        } else {
          data = JsonBeanFactory.fromJson<T>(json['data']);
        }
      } catch (e) {
        AppLogger.e(e);
      }
      // }
    } else if (noAuth) {}
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = code;
    map['msg'] = msg;
    map['data'] = data;
    return map;
  }
}

class NetBaseListEntity<T> {
  late int code;
  late String msg;
  late String type;
  late int enable;
  List<T>? data;

  bool get isSuccess {
    return code == 200;
  }

  NetBaseListEntity.fromJson(dynamic json) {
    code = json['code'] ?? -1;
    msg = json['msg'] ?? "";
    type = json['type'] ?? '0';
    enable = json['enable'] ?? 0;
    if (code == 0 || code == 1 || enable == 1) {
      if (json['data'] != null) {
        data = [];
        try {
          var list = json['data'];
          if (json['data'] is! List && (json['data']['list'] != null)) {
            //有时返回结果 ['list']有列表
            if (json['data']['list'] is List) {
              list = json['data']['list'];
            }
          }

          for (var element in (list as List)) {
            data!.add(JsonBeanFactory.fromJson<T>(element));
          }
        } catch (e) {
          code = 400;
          // message = "数据解析错误:${e.toString()}";
        }
      }
    }
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = code;
    map['msg'] = msg;
    map['data'] = data;
    return map;
  }
}
