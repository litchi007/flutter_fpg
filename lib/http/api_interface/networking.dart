import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class NetworkingOptions {
  bool needLog;
  Map<String, String> headers;
  String tag;
  int timeout;
  bool abort;

  NetworkingOptions({
    this.needLog = false,
    this.headers = const {'Accept-Language': 'zh-cn'},
    this.tag = '',
    this.timeout = Networking.defaultTimeout,
    this.abort = false,
  });
}

class Networking {
  static const int defaultTimeout = 10000; // 10 seconds
  static const int longTimeout = 30000; // 30 seconds

  static Future<MResponseBase> get(
    String url, {
    RequestBase? request,
    NetworkingOptions? options,
  }) async {
    if (request != null) {
      url = '$url${request.path}?${request.queries}';
    }
    options ??= NetworkingOptions();
    if (options.tag.isEmpty) {
      options.tag = Logger.callerTag('Networking', 'get');
    }

    var controller = options.abort ? http.Client() : null;
    final timeout = Duration(milliseconds: options.timeout);

    final requestFuture = http.get(
      Uri.parse(url),
      headers: options.headers,
    );

    return await _request(
      url,
      requestFuture,
      options,
      timeout,
      controller,
    );
  }

  static Future<MResponseBase> post(
    String url, {
    RequestBase? request,
    NetworkingOptions? options,
  }) async {
    if (request != null) {
      url = '$url${request.path}';
    }
    options ??= NetworkingOptions();
    if (options.tag.isEmpty) {
      options.tag = Logger.callerTag('Networking', 'post');
    }

    var controller = options.abort ? http.Client() : null;
    final timeout = Duration(milliseconds: options.timeout);

    final requestFuture = http.post(
      Uri.parse(url),
      headers: options.headers,
      body: request?.body != null ? json.encode(request!.body) : null,
    );

    return await _request(
      url,
      requestFuture,
      options,
      timeout,
      controller,
    );
  }

  static Future<MResponseBase> _request(
    String url,
    Future<http.Response> requestFuture,
    NetworkingOptions options,
    Duration timeout,
    http.Client? controller,
  ) async {
    final stopwatch = Stopwatch()..start();

    try {
      final response = await requestFuture.timeout(timeout);
      stopwatch.stop();

      final data = json.decode(response.body);
      if (options.needLog) {
        Logger.info(
          json.encode({
            ...response.headers,
            'url': url,
            'timeInterval': '${stopwatch.elapsedMilliseconds} ms',
          }),
          options.tag,
        );
      }

      return MResponseBase(
        data: data,
        used: stopwatch.elapsedMilliseconds,
        url: url,
      );
    } catch (error) {
      stopwatch.stop();
      if (options.needLog) {
        Logger.error(
          json.encode({
            'error': error.toString(),
            'url': url,
            'timeInterval': '${stopwatch.elapsedMilliseconds} ms',
          }),
          options.tag,
        );
      }

      final responseError = MResponseError(
        url: url,
        used: stopwatch.elapsedMilliseconds,
        message: error.toString(),
        payload: error,
      );
      return responseError;
    } finally {
      controller?.close();
    }
  }
}

class RequestBase {
  String path;
  String queries;
  Map<String, dynamic>? body;

  RequestBase({
    required this.path,
    required this.queries,
    this.body,
  });
}

class MResponseBase {
  dynamic data;
  int used;
  String url;

  MResponseBase({
    required this.data,
    required this.used,
    required this.url,
  });
}

class MResponseError extends MResponseBase {
  String message;
  dynamic payload;

  MResponseError({
    required super.url,
    required super.used,
    required this.message,
    this.payload,
  }) : super(data: null);
}

class Logger {
  static String callerTag(String className, String methodName) {
    return '$className.$methodName';
  }

  static void info(String message, String tag) {
    print('$tag: $message');
  }

  static void error(String message, String tag) {
    print('$tag: $message');
  }
}

Future<T> retryPromise<T>(Future<T> Function() fn,
    {int retriesLeft = 3, int interval = 200}) async {
  //ugLog('Start calling ${fn.toString()}(retriesLeft: $retriesLeft)');
  try {
    return await fn();
  } catch (error) {
    if (retriesLeft == 1) {
      // ugLog('Maximum retries exceeded of ${fn.toString()}');
      rethrow;
    } else {
      await Future.delayed(Duration(milliseconds: interval));
      // ugLog('Start retrying ${fn.toString()}(retriesLeft: $retriesLeft)');
      return retryPromise(fn, retriesLeft: retriesLeft - 1, interval: interval);
    }
  }
}
