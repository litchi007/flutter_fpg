import 'package:dio/dio.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/log_util.dart';

class AppInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers.addAll({"Accept-Language": 'zh'});
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    try {
      if (response.data.runtimeType == String) {
        // var json = jsonDecode(response.data);
        // if (json['code'] == 666) {
        //   handler.reject(DioException(
        //       requestOptions: response.requestOptions, response: response));
        // }
      }
    } catch (e) {
      AppLogger.e(e);
    }
    return handler.next(response);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    return handler.next(err);
  }
}
