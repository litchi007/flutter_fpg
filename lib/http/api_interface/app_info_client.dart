import 'package:dio/dio.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/crypto_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';

class AppInfoInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (AppDefine.userToken?.apiSid != null) {
      options.headers.addAll({"auth-token": AppDefine.userToken?.apiSid});
    }
    final queryParameters = options.queryParameters;
    LogUtil.w("params___ ${options.data}");
    Map<String, dynamic>? signParams = queryParameters;
    final cryptoData = CryptoUtil.encryptRsa(queryParameters);
    signParams = cryptoData;
    options.queryParameters = cryptoData;
    // if (options.)
    if (options.path.contains("user&a=info")) {
      LogUtil.w("queryParameters_______$queryParameters $cryptoData");
    }
    if (options.data != null) {
      options.data =
          CryptoUtil.encryptRsa(options.data as Map<String, dynamic>);
    }
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    LogUtil.w('response.realUri: ${response.realUri}');
    try {
      if (response.data.runtimeType == String) {
        // var json = jsonDecode(response.data);
        // if (json['code'] == 666) {
        //   handler.reject(DioException(
        //       requestOptions: response.requestOptions, response: response));
        // }
      }
    } catch (e) {
      AppLogger.e(e);
    }
    return handler.next(response);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    return handler.next(err);
  }
}
