import 'package:dio/dio.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/http/api_interface/app_info_client.dart';
import 'package:fpg_flutter/http/api_interface/app_interceptor.dart';

class AppHttpCore {
  late Dio dio;
  late String baseUrl;

  factory AppHttpCore({required String baseUrl}) {
    var instance = AppHttpCore._internal(baseUrl);
    return instance;
  }

  // Private constructor
  AppHttpCore._internal(String mbaseUrl) {
    baseUrl = mbaseUrl;
    initDio();
  }

  void initDio() {
    BaseOptions baseOptions = BaseOptions(
      baseUrl: baseUrl,
      // connectTimeout : Duration(seconds: 300),
      // receiveTimeout  : Duration(seconds: 300),
      // sendTimeout: Duration(seconds: 5)
    );

    dio = Dio(baseOptions);
    if (baseUrl == AppDefine.host) {
      dio.interceptors.add(AppInfoInterceptor());
    } else {
      dio.interceptors.add(AppInterceptor());
    }

    // dio.interceptors.add(
    //   LogInterceptor(
    //       request: false,
    //       requestHeader: false,
    //       requestBody: true,
    //       responseHeader: false,
    //       responseBody: true),
    // );
  }
}
