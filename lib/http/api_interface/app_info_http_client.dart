import 'package:dio/dio.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:fpg_flutter/models/app_info_app_id_model/app_info_app_id_model.dart';
import 'package:retrofit/retrofit.dart';

part 'app_info_http_client.g.dart';

@RestApi()
abstract class AppInfoHttpClient {
  factory AppInfoHttpClient(Dio dio, {String baseUrl}) = _AppInfoHttpClient;

  @GET('/api.php?m=app_info&app_id={appId}')
  Future<NetBaseEntity<AppInfoAppIdModel>> getAppInfo({
    @Path('appId') String? appId,
  });
}
