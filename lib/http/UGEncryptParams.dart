import 'package:encrypt/encrypt.dart' as encrypt;
import 'dart:math';
import 'package:fpg_flutter/configs/app_define.dart';

import 'dart:convert';

import 'package:basic_utils/basic_utils.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

final defaultKey = encrypt.Key.fromUtf8('4c43c361235a4ac05b91eb5fa95');

const pemString = '-----BEGIN PUBLIC KEY-----\n'
    'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0CZTn+50HHM0QkziEunofDfIG77buLuRwItL8My9EYAyuLSW1qkLgqta2z2bIedx7Ro6enOZ0PZNFnqsztltGctwTwAVQDGoB+kpqUi5gs5jRTcoRkytgaLs7xZey45H0c2Hof4W+rcdHR/xc7C0hT5fBNqEDjBmGvoLlYpHag/p4m7h+JgpWHmKGWg7ijHMPWJQSFD1JPnP7upQlTJ8BKl24em6n2lSyH8qkoJKoEzUfQ7HricpF4S6MVCm36BSfkz35Oy4La7WxDrwW8KDs3ahKHM4uifgDlupZ+nV/dgzCQWDi5lNiQlvWR0xKsjwwrnXTdHPnMYDX8NdDTvTcQIDAQAB\n'
    '-----END PUBLIC KEY-----\n';

final List<String> selectChar = [
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z'
];

String generateRandomKey() {
  final random = Random();
  String deskey = ''; // Initialize an empty string for the key
  for (int i = 0; i < 32; i++) {
    int charIndex =
        random.nextInt(selectChar.length); // Generate a random index
    deskey +=
        selectChar[charIndex]; // Append a random character to the key string
  }
  return deskey;
}

String generateRandoLastKey() {
  final random = Random();
  return selectChar[random.nextInt(selectChar.length)];
}

String encrypt3DES(String plainText, {encrypt.Key? key}) {
  final encrypter = encrypt.Encrypter(
      encrypt.AES(key ?? defaultKey, mode: encrypt.AESMode.ecb));
  final encrypted = encrypter.encrypt(plainText);
  return encrypted.base64;
}

String decrypt3DES(String encryptedText, {encrypt.Key? key}) {
  final encrypter = encrypt.Encrypter(
      encrypt.AES(key ?? defaultKey, mode: encrypt.AESMode.ecb));
  final decrypted = encrypter.decrypt64(encryptedText);
  return decrypted;
}

String encryptRSA(String str) {
  var pub = CryptoUtils.rsaPublicKeyFromPem(pemString);
  final encrypter = encrypt.Encrypter(encrypt.RSA(publicKey: pub));
  String bs64AesKey = base64.encode(utf8.encode(str));
  final encryptedAesBytes = encrypter.encrypt(bs64AesKey);

  return encryptedAesBytes.base64;
}

Map<String, String> encryptParams(Map<String, dynamic> params) {
  final token = AppDefine.userToken?.apiSid;
  params['token'] = token;
  params['checkSign'] = 1;

  final randomKey = generateRandomKey();
  String sign = '';

  try {
    sign = encryptRSA('iOS_$defaultKey');
  } catch (e) {
    sign = '';
  }
  if (sign.isEmpty) {
    sign = AppDefine.lastSign;
  }

  final encryptedParams = params.map((key, value) {
    return MapEntry(key,
        encrypt3DES(value.toString(), key: encrypt.Key.fromUtf8(randomKey)));
  });

  encryptedParams['sign'] = sign;
  return encryptedParams;
}
