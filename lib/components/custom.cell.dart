import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomCell extends StatelessWidget {
  final String? label;
  final Widget? value;
  final bool? showBorder;
  final double? right;
  final bool? open;
  const CustomCell(
      {super.key,
      this.label,
      this.value,
      this.showBorder,
      this.right,
      this.open});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // color: context.customTheme?.navbarBg,
      child: Container(
        // margin: const EdgeInsets.symmetric(horizontal: 10),
        padding: const EdgeInsets.symmetric(vertical: 15),
        decoration: BoxDecoration(
            border: Border(
                bottom: (showBorder ?? true)
                    ? BorderSide(
                        width: .5,
                        color: context.theme.cardColor.withOpacity(.2))
                    : BorderSide.none)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              label ?? "",
              style: context.textTheme.titleMedium?.copyWith(),
            ).paddingSymmetric(horizontal: 10),
            Padding(
              padding: EdgeInsets.only(right: right ?? 0),
              child: Row(
                children: [
                  value ?? const SizedBox(),
                  AnimatedRotation(
                    turns: (open ?? false) ? 0.25 : 0,
                    duration: 300.milliseconds,
                    child: Icon(
                      Icons.keyboard_arrow_right_rounded,
                      color: context.textTheme.bodyMedium?.color,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
