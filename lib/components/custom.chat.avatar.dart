import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/chat/chat.http.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/user_get_avatar_setting_model/public_avatar_list.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class CustomChatAvatar extends StatefulWidget {
  final List<PublicAvatarList>? list;
  const CustomChatAvatar({super.key, this.list});

  @override
  State<CustomChatAvatar> createState() => _CustomChatAvatarState();
}

class _CustomChatAvatarState extends State<CustomChatAvatar> {
  late PublicAvatarList? avatar = widget.list?.first;

  handleClose() {
    Get.back();
  }

  handleSubmit() async {
    final res = await ChatHttp.chatUpdateAvatar(avatar?.id ?? "");
    ToastUtils.show(res.msg);
    Get.back();
  }

  handleSelect(PublicAvatarList value) {
    setState(() {
      avatar = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      color: context.customTheme?.navbarBg,
      child: Column(
        children: [
          // 头像预览
          AppImage.network(avatar?.url ?? "", width: 80, height: 80)
              .paddingOnly(top: 20),
          const Text("头像预览").paddingSymmetric(vertical: 20),
          Expanded(
            child: Row(
              children: [
                Icon(
                  Icons.arrow_back_ios_new,
                  color: context.theme.cardColor,
                  size: 40,
                ),
                Expanded(
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: widget.list?.length ?? 0,
                      itemBuilder: (context, index) {
                        final item = widget.list?[index];
                        return Container(
                            margin: const EdgeInsets.only(right: 10),
                            child: AppImage.network(item?.url ?? "",
                                    width: 80, height: 80)
                                .onTap(() => handleSelect(item!)));
                      }),
                ),
                Icon(Icons.arrow_forward_ios_rounded,
                    color: context.theme.cardColor, size: 40),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: context.customTheme?.error,
                      borderRadius: 4.radius),
                  child: Text(
                    "保存头像",
                    style: context.textTheme.bodyMedium?.copyWith(
                        color: context.customTheme?.reverseFontColor,
                        fontWeight: FontWeight.bold),
                  ),
                ).onTap(handleSubmit),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  decoration: BoxDecoration(
                      color: context.theme.cardColor, borderRadius: 4.radius),
                  child: Text("取消",
                      style: context.textTheme.bodyMedium?.copyWith(
                          color: context.customTheme?.reverseFontColor,
                          fontWeight: FontWeight.bold)),
                ).onTap(handleClose),
              )
            ],
          ).paddingOnly(bottom: context.mediaQueryPadding.bottom)
        ],
      ),
    );
  }
}
