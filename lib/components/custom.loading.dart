import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:get/get.dart';

class CustomLoading extends StatefulWidget {
  final bool? status;
  final Size? size;
  final Widget? child;
  const CustomLoading({super.key, this.child, this.status, this.size});

  @override
  State<CustomLoading> createState() => _CustomLoadingState();
}

class _CustomLoadingState extends State<CustomLoading> {
  Timer? timer;
  late bool status = widget.status ?? false;
  handleTime() {
    /// 网络延迟后取消加载状态
    timer = Timer(AppConfig.connectTimeout.milliseconds, () {
      setState(() {
        status = false;
      });
    });
  }

  @override
  void didUpdateWidget(CustomLoading oldWidget) {
    if (oldWidget.status != widget.status) {
      setState(() {
        status = widget.status ?? false;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    handleTime();
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.size?.width ?? context.mediaQuerySize.width,
      height: widget.size?.height ?? context.mediaQuerySize.height,
      child: status
          ? const Center(child: CupertinoActivityIndicator())
          : widget.child,
    );
  }
}
