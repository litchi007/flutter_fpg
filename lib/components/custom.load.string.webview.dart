import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class CustomLoadStringWebview extends StatefulWidget {
  final String? html;
  const CustomLoadStringWebview({super.key, this.html});

  @override
  State<CustomLoadStringWebview> createState() =>
      _CustomLoadStringWebviewState();
}

class _CustomLoadStringWebviewState extends State<CustomLoadStringWebview> {
  final isAndroid = Platform.isAndroid;
  late final String kNavigationExamplePage = widget.html ??
      '''
  <!DOCTYPE html><html>
  <head>
    <title>Navigation Delegate Example</title>
    <script src="https://cdn.bootcdn.net/ajax/libs/vConsole/3.15.1/vconsole.min.js"></script>
  </head>
  <style>
  body {
  height: 200px;
  overflow-y: scroll;
  background: blue;
  }
  image {
  width: 100%!important;
  }
  </style>
  <body>
  <p>
  The navigation delegate is set to block navigation to the youtube website.
  </p>
  <ul>
  <ul><a href="https://www.youtube.com/">https://www.youtube.com/</a></ul>
  <ul><a href="https://www.google.com/">https://www.google.com/</a></ul>
  </ul>

  <script>
  // new VConsole();
  // alert(1);
  // window.onload = function () {
  //   Toaster.postMessage(document.documentElement.scrollHeight)
  // } 

  function getHeihgt() {
    Toaster.postMessage(document.body.scrollHeight)
  }
  </script>
  </body>
  </html>
  ''';

  late final WebViewController _controller;
  String height = "100";
  @override
  void initState() {
    // #docregion platform_features
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    // #enddocregion platform_features

    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
            controller.runJavaScript("window.getHeihgt()");
            // debugPrint();
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
Page resource error:
  code: ${error.errorCode}
  description: ${error.description}
  errorType: ${error.errorType}
  isForMainFrame: ${error.isForMainFrame}
          ''');
          },
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              debugPrint('blocking navigation to ${request.url}');
              return NavigationDecision.prevent;
            }
            debugPrint('allowing navigation to ${request.url}');
            return NavigationDecision.navigate;
          },
          onHttpError: (HttpResponseError error) {
            debugPrint('Error occurred on page: ${error.response?.statusCode}');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {
            // openDialog(request);
          },
        ),
      )
      ..addJavaScriptChannel(
        'Toaster',
        onMessageReceived: (JavaScriptMessage message) {
          setState(() {
            height = message.message;
          });
          // ScaffoldMessenger.of(context).showSnackBar(
          //   SnackBar(content: Text(message.message)),
          // );
        },
      )
      ..loadHtmlString(kNavigationExamplePage);

    // setBackgroundColor is not currently supported on macOS.
    if (kIsWeb || !Platform.isMacOS) {
      // controller.setBackgroundColor(const Color(0x80000000));
    }

    // #docregion platform_features
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }
    // #enddocregion platform_features

    _controller = controller;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: SizedBox(
          height: isAndroid ? null : double.parse(height) + 40,
          child: isAndroid
              ? HtmlWidget(widget.html ?? "")
              : WebViewWidget(controller: _controller)),
    );
  }
}
