import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomAni extends StatefulWidget {
  final Widget Function(double) child;
  final double? begin;
  final double? end;
  final int? duration;
  final Duration? repeatDuration;
  final int? index;
  final Function(bool, int)? isActiveFunction;
  final bool? isRepeat;
  final Duration? baseDuration;
  const CustomAni(
      {super.key,
      required this.child,
      this.begin,
      this.end,
      this.duration,
      this.repeatDuration,
      this.isActiveFunction,
      this.index,
      this.isRepeat,
      this.baseDuration});

  @override
  State<CustomAni> createState() => _CustomAniState();
}

class _CustomAniState extends State<CustomAni> with TickerProviderStateMixin {
  late final controller = AnimationController(
      vsync: this,
      duration: widget.baseDuration ?? const Duration(milliseconds: 400))
    ..addStatusListener(listener);
  late final animated = Tween<double>(
    begin: widget.begin ?? 0,
    end: widget.end ?? 1,
  ).animate(controller);
  Timer? timer;
  listener(AnimationStatus value) {
    if ((widget?.isRepeat ?? false)) {
      return;
    }
    switch (value) {
      case AnimationStatus.completed:
        widget.isActiveFunction?.call(false, widget.index ?? 0);
        controller.reverse();
        break;
      case AnimationStatus.dismissed:
        if (mounted) {
          if (timer?.isActive ?? false) {
            timer?.cancel();
          }
          timer = Timer(widget.repeatDuration ?? (2 * 300).milliseconds, () {
            widget.isActiveFunction?.call(true, widget.index ?? 0);
            controller.forward();
          });
        }

        break;
      default:
    }
  }

  run() {
    if (!(widget?.isRepeat ?? false)) {
      timer = Timer((widget.duration ?? 0).milliseconds, () {
        widget.isActiveFunction?.call(true, widget.index ?? 0);
        controller.forward();
      });
    } else {
      controller.repeat();
    }
  }

  @override
  void initState() {
    run();
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    controller.removeStatusListener(listener);
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animated,
        builder: (context, child) {
          return widget.child(animated.value);
        });
  }
}
