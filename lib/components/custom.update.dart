import 'package:flutter/material.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/main.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:restart_app/restart_app.dart';

class CustomUpdateApp extends StatefulWidget {
  const CustomUpdateApp({super.key});

  @override
  State<CustomUpdateApp> createState() => _CustomUpdateAppState();
}

class _CustomUpdateAppState extends State<CustomUpdateApp> {
  int step = 0;
  @override
  Widget build(BuildContext context) {
    return CustomConfirm(
      params: CustomConfirmParams(
          showCancel: step != 2,
          cancel: () {
            ToastUtils.closeAll();
          },
          submit: () async {
            if (step == 2) {
              // 更新完成，重启app
              // ToastUtils.closeAll();
              Restart.restartApp();
              return;
            }
            try {
              setState(() {
                step = 1;
              });
              await shorebirdCodePush.downloadUpdateIfAvailable();
              setState(() {
                step = 2;
              });
            } catch (e) {
              LogUtil.e(e);
            } finally {}
          },
          showTitle: false,
          contentChild: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: [
                RichText(
                  text: TextSpan(
                      text: step == 0
                          ? "您当前版本非最新版本, 建议更新版本获得更佳的游戏体验,或请"
                          : step == 1
                              ? "更新中,请稍后"
                              : "更新成功, 请重启APP查看",
                      style: context.textTheme.titleMedium
                          ?.copyWith(fontWeight: FontWeight.w600, height: 1.6),
                      children: step == 0
                          ? [
                              TextSpan(
                                text: "联系客服",
                                style: Get.context!.textTheme.titleMedium
                                    ?.copyWith(
                                        fontWeight: FontWeight.w600,
                                        height: 1.6,
                                        decoration: TextDecoration.underline),
                              )
                            ]
                          : []),
                ),
              ],
            ),
          )),
    );
  }
}
