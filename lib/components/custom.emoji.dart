import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/page/newPost/newPost.dart';
import 'package:get/get.dart';

class CustomEmoji extends StatefulWidget {
  final Function(String)? onChange;
  const CustomEmoji({super.key, this.onChange});

  @override
  State<CustomEmoji> createState() => _CustomEmojiState();
}

class _CustomEmojiState extends State<CustomEmoji> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: context.mediaQueryPadding.bottom),
      // color: Colors.blue[50],
      height: 400,
      color: context.theme.scaffoldBackgroundColor,
      child: GridView.builder(
        padding: const EdgeInsets.all(10),
        itemCount: LotteryConfig.emojiList.length,
        itemBuilder: (c, index) {
          final item = LotteryConfig.emojiList[index];
          return GestureDetector(
            onTap: () {
              widget.onChange?.call("[em_$item]");
            },
            child: Container(
              color: Colors.transparent,
              child:
                  AppImage.network("${LotteryConfig.emojiBaseUrl}/$item.gif"),
            ),
          );
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 8,
          // mainAxisSpacing: 5, crossAxisSpacing: 5
        ),
      ),
    );
  }
}
