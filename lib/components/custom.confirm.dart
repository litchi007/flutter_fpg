import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class CustomConfirmParams {
  String? title;
  String? content;
  String? submitText;
  String? cancelText;
  Function? submit;
  Function? cancel;
  Widget? contentChild;
  bool? clickMaskDismiss;
  bool? backDismiss;
  bool? isCloseAll;
  bool? showBottom;
  bool? showSubmit;
  Function()? onDismiss;
  bool? showTitle;
  Widget? child;
  bool? showCancel;
  CustomConfirmParams(
      {this.title,
      this.content,
      this.submitText,
      this.cancelText,
      this.submit,
      this.cancel,
      this.contentChild,
      this.backDismiss,
      this.clickMaskDismiss,
      this.isCloseAll,
      this.showBottom,
      this.onDismiss,
      this.showTitle,
      this.child,
      this.showCancel,
      this.showSubmit});

  CustomConfirmParams.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    content = json['content'];
    submitText = json['submitText'];
    cancelText = json['cancelText'];
    submit = json['submit'];
    cancel = json['cancel'];
    contentChild = json['contentChild'];
    backDismiss = json['backDismiss'];
    clickMaskDismiss = json['clickMaskDismiss'];
    isCloseAll = json['isCloseAll'];
    showBottom = json['showBottom'];
    onDismiss = json['onDismiss'];
    showTitle = json['showTitle'];
    showCancel = json['showCancel'];
    child = json['child'];
    showSubmit = json['showSubmit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['content'] = content;
    data['submitText'] = submitText;
    data['cancelText'] = cancelText;
    data['submit'] = submit;
    data['cancel'] = cancel;
    data['contentChild'] = contentChild;
    data['backDismiss'] = backDismiss;
    data['clickMaskDismiss'] = clickMaskDismiss;
    data['isCloseAll'] = isCloseAll;
    data['showBottom'] = showBottom;
    data['onDismiss'] = onDismiss;
    data['showTitle'] = showTitle;
    data['child'] = child;
    data['showCancel'] = showCancel;
    data['showSubmit'] = showSubmit;
    return data;
  }
}

class CustomConfirm extends StatefulWidget {
  final CustomConfirmParams? params;
  final String? tag;
  final bool? showBottom;
  const CustomConfirm({super.key, this.params, this.tag, this.showBottom});

  @override
  State<CustomConfirm> createState() => _CustomConfirmState();
}

class _CustomConfirmState extends State<CustomConfirm> {
  late final params = widget.params;
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: UnconstrainedBox(
        child: ClipRRect(
          borderRadius: 12.radius,
          child: Container(
            width: context.mediaQuerySize.width * .9,
            // padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 12),
            decoration: BoxDecoration(
                color: context.customTheme?.navbarBg, borderRadius: 12.radius),
            child: Column(
              children: [
                params?.showTitle == null || (params?.showTitle ?? false)
                    ? Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 20, horizontal: 12),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: context.theme.cardColor
                                        .withOpacity(.2)))),
                        width: double.infinity,
                        // color: context.customTheme?.leftActive,
                        child: Text(
                          params?.title ?? "提示",
                          style: context.textTheme.bodyMedium?.copyWith(
                              fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                      )
                    : const SizedBox(),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 12, right: 12, bottom: 12),
                  child: Column(
                    children: [
                      widget.params?.contentChild ??
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12),
                            child: Text(
                              params?.content ?? "确定此次操作？",
                              textAlign: TextAlign.center,
                            ),
                          ),
                      Visibility(
                        visible: params?.showBottom ?? true,
                        child: Row(
                          children: [
                            (widget.params?.showCancel ?? true)
                                ? Expanded(
                                    child: Container(
                                      margin: const EdgeInsets.only(right: 6),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 9),
                                      decoration: BoxDecoration(
                                          borderRadius: 12.radius,
                                          color: "#f1f1f1".color()),
                                      alignment: Alignment.center,
                                      child: Text(
                                        params?.cancelText ?? "取消",
                                        style: context.textTheme.bodyMedium
                                            ?.copyWith(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: context.customTheme?.gray3,
                                        ),
                                      ),
                                    ).onTap(() async {
                                      params?.cancel?.call();
                                      ToastUtils.closeAll();
                                    }),
                                  )
                                : const SizedBox(),
                            (widget.params?.showSubmit ?? true)
                                ? Expanded(
                                    child: Container(
                                      margin: const EdgeInsets.only(left: 6),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 9),
                                      decoration: BoxDecoration(
                                        borderRadius: 12.radius,
                                        color: context.customTheme?.resetBg,
                                      ),
                                      alignment: Alignment.center,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Visibility(
                                              visible: loading,
                                              child: Container(
                                                  margin: const EdgeInsets.only(
                                                      right: 10),
                                                  width: 16,
                                                  height: 16,
                                                  child:
                                                      CircularProgressIndicator(
                                                    strokeWidth: 3,
                                                    color: context.customTheme
                                                        ?.reverseFontColor,
                                                  ))),
                                          Text(
                                            params?.submitText ?? "确定",
                                            style: context.textTheme.bodyMedium
                                                ?.copyWith(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color:
                                                  context.customTheme?.navbarBg,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ).onTap(() async {
                                      if (loading) {
                                        return;
                                      }
                                      setState(() {
                                        loading = true;
                                      });
                                      await params?.submit?.call();
                                      setState(() {
                                        loading = false;
                                      });
                                    }),
                                  )
                                : const SizedBox(),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
