import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class CustomWebview extends StatefulWidget {
  final String? webUri;
  final Function(double)? onProgress;
  const CustomWebview({super.key, this.webUri, this.onProgress});

  @override
  State<CustomWebview> createState() => _CustomWebviewState();
}

class _CustomWebviewState extends State<CustomWebview> {
  double currentProgress = 0;

  late final controller = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.unrestricted)
    ..setNavigationDelegate(
      NavigationDelegate(
        onProgress: (int progress) {
          // Update loading bar.
        },
        onPageStarted: (String url) {},
        onPageFinished: (String url) {},
        onHttpError: (HttpResponseError error) {},
        onWebResourceError: (WebResourceError error) {},
        onNavigationRequest: (NavigationRequest request) {
          if (request.url.startsWith('https://www.youtube.com/')) {
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
      ),
    )
    ..loadRequest(Uri.parse(widget.webUri ?? ""))
    ..setBackgroundColor(Colors.transparent)
    ..setNavigationDelegate(NavigationDelegate(onProgress: (int _progress) {
      setState(() {
        currentProgress = _progress / 100;
      });
    }));
  // final options = InAppWebViewSettings(
  //   transparentBackground: true,
  //   disableDefaultErrorPage: true
  // );

  @override
  void initState() {
    print("webUri___${widget.webUri}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Opacity(
          opacity: 1,
          child: WebViewWidget(
            controller: controller,
          ),
          // child: InAppWebView(

          //   initialSettings: options,
          //   keepAlive: InAppWebViewKeepAlive(),
          //   initialUrlRequest:  URLRequest(url: WebUri(widget.webUri ?? "")),
          //   onProgressChanged: (webviewController, progress) async {
          //     final _progress = double.parse(progress.toStringAsFixed(2));
          //     widget.onProgress?.call(
          //       _progress
          //     );
          //     // webviewController.
          //     setState(() {
          //       currentProgress = _progress;
          //     });
          //   },
          //   onWebViewCreated:
          //       (InAppWebViewController webviewController) {

          //     // webviewController.addJavaScriptHandler(
          //     //     handlerName: WebviewMethods.close,
          //     //     callback: (arguments) {});
          //     // webviewController?.evaluateJavascript(source: '');

          //     /// [example] 示例
          //     /// window.flutter_inappwebview.callHandler("paySuccess", {
          //     ///  id: "xxx"
          //     /// })
          //   },
          //   onLoadStop: (webviewController, url) async {

          //   }),
        ),
        if (currentProgress < 1)
          const Center(
            child: CupertinoActivityIndicator(),
          ),
      ],
    );
  }
}
