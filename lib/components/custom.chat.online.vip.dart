import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/socket_util.dart';
import 'package:get/get.dart';

class CustomChatOnlineVip extends StatefulWidget {
  final String? roomId;
  final Function(SocketGetUserModel)? search;
  final SocketGetUserResModel? data;
  const CustomChatOnlineVip({super.key, this.roomId, this.search, this.data});

  @override
  State<CustomChatOnlineVip> createState() => CustomChatOnlineVipState();
}

class CustomChatOnlineVipState extends State<CustomChatOnlineVip> {
  final list = [
    // DropdownMenuItem(child: Text('北京')),
    // "用户名", "昵称", "uid",
    BaseMapModel(key: "用户名", value: "username"),
    BaseMapModel(key: "昵称", value: "nickname"),
    BaseMapModel(key: "uid", value: "uid"),
  ].cast<BaseMapModel>();
  late SocketGetUserResModel user = widget.data ?? SocketGetUserResModel();
  late BaseMapModel dropdownValue = list.first;

  final textController = TextEditingController();

  updateUser(SocketGetUserResModel data) {
    setState(() {
      user = data;
    });
  }

  handleSearch() {
    widget.search?.call(SocketUtil.getUser(
        condition: dropdownValue.value, value: textController.text));
  }

  @override
  void didUpdateWidget(CustomChatOnlineVip oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.mediaQuerySize.width,
      padding: const EdgeInsets.only(top: 200),
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              UnconstrainedBox(
                child: Container(
                  color: context.customTheme?.reverseFontColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: 150,
                          height: 50,
                          child: CupertinoTextField(
                            controller: textController,
                            placeholder: "搜索在线会员",
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: 0, color: Colors.transparent)),
                          )),
                      Container(
                          width: 120,
                          // color: Colors.red,
                          child: CustomDropdown(
                            // hintText: "111",
                            excludeSelected: false,
                            items: list.map((e) => e.key).toList(),
                            initialItem: list[0].key,
                            onChanged: (value) {
                              // log('changing value to: $value');
                              setState(() {
                                dropdownValue =
                                    list.firstWhere((e) => e.key == value);
                              });
                            },
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                      margin: const EdgeInsets.only(left: 10),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 10),
                      decoration: BoxDecoration(
                          color: context.customTheme?.lottery2,
                          borderRadius: 10.radius),
                      child: Text("搜索",
                          style: context.textTheme.bodyMedium?.copyWith(
                              color: context.customTheme?.reverseFontColor)))
                  .onTap(handleSearch),
            ],
          ),
          Visibility(
              visible: user.username != null,
              child: Container(
                width: 340,
                color: Colors.black.withOpacity(.6),
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppImage.network(user.avator ?? "", width: 50, height: 50),
                    Text(user.username ?? "",
                            style: context.textTheme.bodyMedium?.copyWith(
                                color: context.customTheme?.reverseFontColor))
                        .paddingOnly(left: 10)
                  ],
                ),
              ))
        ],
      ),
    );
  }
}

class BaseMapModel {
  String? key;
  String? value;

  BaseMapModel({this.key, this.value});

  BaseMapModel.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['key'] = key;
    data['value'] = value;
    return data;
  }
}
