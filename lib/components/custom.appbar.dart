import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/theme_service.dart';
import 'package:get/get.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Widget? child;
  final double? height;
  final Widget? leading;
  final Widget? title;
  final Widget? actions;
  final String? header;
  final Color? background;
  final Color? textColor;
  final Color? iconColor;
  final Widget? customChild;
  final double? actionMaxWidth;
  final Function()? backCallback;
  const CustomAppBar(
      {super.key,
      this.child,
      this.height,
      this.actions,
      this.leading,
      this.title,
      this.header,
      this.background,
      this.textColor,
      this.iconColor,
      this.customChild,
      this.backCallback,
      this.actionMaxWidth});

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();

  @override
  Size get preferredSize => Size(double.infinity, height ?? 100);
}

class _CustomAppBarState extends State<CustomAppBar> {
  setBringer() {
    ThemeService.to.updateStatusBar();
  }

  @override
  void initState() {
    setBringer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child ??
        ColoredBox(
          color: widget.background ??
              context.theme.appBarTheme.backgroundColor ??
              Colors.transparent,
          child: SafeArea(
            bottom: false,
            child: SizedBox(
              // color: Colors.red,
              width: double.infinity,
              height: widget.height ?? 44,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      constraints:
                          const BoxConstraints(minWidth: 60, maxWidth: 60),
                      child: widget.leading ??
                          Icon(
                            Icons.arrow_back_ios_new,
                            color: widget.iconColor ??
                                context.textTheme.bodyMedium?.color,
                            size: 20,
                          ).onTap(() {
                            if (widget.backCallback != null) {
                              widget.backCallback?.call();
                            } else {
                              Get.back();
                            }
                          })),
                  Expanded(
                    child: widget.customChild ??
                        Row(
                          children: [
                            Expanded(
                                child: Container(
                                    alignment: Alignment.center,
                                    width: double.infinity,
                                    child: widget.title ??
                                        Text(
                                          widget.header ?? "",
                                          style: context.textTheme.bodyMedium
                                              ?.copyWith(
                                                  fontWeight: FontWeight.bold,
                                                  color: widget.textColor,
                                                  fontSize: 18,
                                                  overflow:
                                                      TextOverflow.ellipsis),
                                        ))),
                            Container(
                              constraints: BoxConstraints(
                                  minWidth: 60,
                                  minHeight: 0,
                                  maxWidth: widget.actionMaxWidth ?? 60,
                                  maxHeight: 50),
                              child: widget.actions,
                            )
                          ],
                        ),
                  ),
                ],
              ),
            ),
          ),
        );
  }
}
