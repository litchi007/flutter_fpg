import 'package:draggable_float_widget/draggable_float_widget.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';

class CustomDargItem extends StatefulWidget {
  final String? path;
  final bool? isLocal;
  final Function? onClose;
  final double? left;
  final double? top;
  final Function()? onTap;
  const CustomDargItem(
      {super.key,
      this.path,
      this.isLocal,
      this.onClose,
      this.left,
      this.top,
      this.onTap});

  @override
  State<CustomDargItem> createState() => _CustomDargitemSIate();
}

class _CustomDargitemSIate extends State<CustomDargItem> {
  @override
  Widget build(BuildContext context) {
    return DraggableFloatWidget(
      width: 100,
      height: 100,
      onTap: widget.onTap,
      config: DraggableFloatWidgetBaseConfig(
        borderLeft: widget.left ?? 0,
        borderTop: widget.top ?? 0,
      ),
      child: SizedBox(
        width: 100,
        height: 100,
        child: Stack(
          children: [
            (widget.isLocal ?? false)
                ? Image.asset(widget.path ?? "")
                : AppImage.network(widget.path ?? ""),
            Positioned(
              right: 0,
              child: GestureDetector(
                onTap: () {
                  widget.onClose?.call();
                  // setState(() {
                  //   _isClose = true;
                  // });
                },
                child: Container(
                  padding: const EdgeInsets.all(3),
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Colors.red),
                  child: const Icon(
                    Icons.close,
                    size: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
