import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/chat/chat.http.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/constants/assets.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/chat_message_record_model/chat_message_record_model.dart';
import 'package:fpg_flutter/models/chat_red_bag_model/red_bag.dart';
import 'package:fpg_flutter/models/grab_red_bag_model/grab_red_bag_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class CustomRedpackOpen extends StatefulWidget {
  final RedBag data;
  final ChatMessageRecordModel details;
  const CustomRedpackOpen(
      {super.key, required this.data, required this.details});

  @override
  State<CustomRedpackOpen> createState() => _CustomRedpackOpenState();
}

class _CustomRedpackOpenState extends State<CustomRedpackOpen> {
  late final data = widget.data;
  late final details = widget.details;
  GrabRedBagModel redbackDetail = const GrabRedBagModel();

  handleOpen(RedBag data) async {
    final res = await ChatHttp.chatGrabRedBag(ChatGrabRedBagParams(
      redBagId: data.id,
    ));
    LogUtil.w("抢红包____${res.code}");
    if (res.code != 0) {
      ToastUtils.show(res.msg, false);
      return;
    }
    setState(() {
      redbackDetail = res.data!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: 10.radius,
      child: Container(
        width: context.mediaQuerySize.width * .7,
        height: 400,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(Assets.assetsImagesUnGrab),
                fit: BoxFit.fill)),
        child: Stack(
          children: [
            SizedBox(
              width: double.infinity,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AppImage.network(details.avatar ?? "", width: 60, height: 60),
                  Text(details.username ?? "",
                          style: context.textTheme.bodyLarge?.copyWith(
                              color: context.customTheme?.redpackTextColor))
                      .paddingOnly(top: 20),
                  Text('发了一个红包，金额随机',
                          style: context.textTheme.bodyLarge?.copyWith(
                              color: context.customTheme?.redpackTextColor))
                      .paddingOnly(top: 10),
                  Visibility(
                    visible: redbackDetail.miniRedBagAmount == null,
                    child: Text(data.title ?? "",
                            style: context.textTheme.bodyLarge?.copyWith(
                                color: context.customTheme?.redpackTextColor))
                        .paddingOnly(top: 10),
                  ),
                  Visibility(
                    visible: redbackDetail.miniRedBagAmount != null,
                    child: Container(
                      width: context.mediaQuerySize.width * .7,
                      alignment: Alignment.center,
                      height: 80,
                      child: Text(redbackDetail.miniRedBagAmount ?? "",
                          style: context.textTheme.bodyLarge?.copyWith(
                              color: context.customTheme?.redpackTextColor,
                              fontSize: 30)),
                    ),
                  )
                ],
              ),
            ).paddingOnly(top: 40),
            Positioned(
                top: 210,
                left: 100,
                child: Visibility(
                  visible: redbackDetail.miniRedBagAmount == null,
                  child: Image.asset(
                    Assets.assetsImagesOpenRedBag,
                    width: 80,
                    height: 80,
                  ).onTap(() => handleOpen(data)),
                )),
            Positioned(
                bottom: 20,
                child: Visibility(
                    visible: redbackDetail.miniRedBagAmount != null,
                    child: Container(
                        alignment: Alignment.center,
                        width: context.mediaQuerySize.width * .7,
                        child: Text("看看大家的手气>>",
                            style: context.textTheme.bodyLarge?.copyWith(
                              color: context.customTheme?.redpackTextColor,
                              // fontSize:
                            )))))
          ],
        ),
      ),
    );
  }
}
