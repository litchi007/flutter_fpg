import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/event_util.dart';

class CustomButton extends StatefulWidget {
  final Widget? child;
  final Future Function()? onTap;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final Decoration? decoration;
  final AlignmentGeometry? alignment;
  final Size? loadingSize;
  final Color? color;
  const CustomButton(
      {super.key,
      this.child,
      this.onTap,
      this.margin,
      this.padding,
      this.decoration,
      this.alignment,
      this.loadingSize,
      this.color});

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: loading,
      child: Container(
        margin: widget.margin,
        padding: widget.padding,
        alignment: widget.alignment,
        decoration: widget.decoration,
        child: loading
            ? SizedBox(
                width: (widget.loadingSize?.width ?? 50) / 2,
                height: (widget.loadingSize?.height ?? 50) / 2,
                child: CircularProgressIndicator(
                  strokeWidth: 3,
                  color: widget.color,
                ),
              )
            : widget.child,
      ).onTap(() async {
        setState(() {
          loading = true;
        });
        await EventUtils.sleep(500.milliseconds);
        await widget.onTap?.call();
        setState(() {
          loading = false;
        });
      }),
    );
  }
}
