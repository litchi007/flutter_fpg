import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:chat_bottom_container/chat_bottom_container.dart';
import 'package:dartx/dartx.dart';
import 'package:draggable_float_widget/draggable_float_widget.dart';
import 'package:extended_text/extended_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart'
    hide ImageSource;
import 'package:fpg_flutter/api/chat/chat.http.dart';
import 'package:fpg_flutter/api/system/system.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/pages/chat_room/chat_room_controller.dart';
import 'package:fpg_flutter/app/pages/chat_room/socket/chat_web_socket_api.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/action_button_widget.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/expanding_action_button.dart';
import 'package:fpg_flutter/app/pages/chat_room/widget/triangle_painter.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/components/custom.chat.avatar.dart';
import 'package:fpg_flutter/components/custom.chat.list.dart';
import 'package:fpg_flutter/components/custom.chat.online.vip.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/components/custom.redpack.dart';
import 'package:fpg_flutter/components/custom.redpack.open.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/constants/assets.dart';
import 'package:fpg_flutter/data/models/NextIssueData.dart';
import 'package:fpg_flutter/data/models/SendMsgTextModel.dart';
import 'package:fpg_flutter/data/repositories/app_bet_chat_repository.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/chat_get_token_model/chat_ary.dart';
import 'package:fpg_flutter/models/chat_get_token_model/chat_get_token_model.dart';
import 'package:fpg_flutter/models/chat_get_token_res_model/chat_get_token_res_model.dart';
import 'package:fpg_flutter/models/chat_message_record_model/bet_url.dart';
import 'package:fpg_flutter/models/chat_message_record_model/chat_message_record_model.dart';
import 'package:fpg_flutter/models/chat_red_bag_message_record_model/chat_red_bag_message_record_model.dart';
import 'package:fpg_flutter/models/chat_red_bag_model/red_bag.dart';
import 'package:fpg_flutter/models/grab_red_bag_model/grab_red_bag_model.dart';
import 'package:fpg_flutter/models/system_avatar_list_model/system_avatar_list_model.dart';
import 'package:fpg_flutter/models/user_get_avatar_setting_model/public_avatar_list.dart';
import 'package:fpg_flutter/models/user_get_avatar_setting_model/user_get_avatar_setting_model.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/custom_icons_icons.dart';
import 'package:fpg_flutter/utils/date_util.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/socket_util.dart';
import 'package:fpg_flutter/utils/text_utils.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

enum PanelType {
  none,
  keyboard,
  emoji,
  tool,
}

class ChatArea extends StatefulWidget {
  final String roomId;
  final double? safeAreaBottom;
  final ChatAry? data;
  final bool showAppBar;

  final ChatKeyboardChangeKeyboardPanelHeight? changeKeyboardPanelHeight;

  final Function(ChatBottomPanelContainerController)? onControllerCreated;

  const ChatArea({
    super.key,
    required this.roomId,
    this.safeAreaBottom,
    this.data,
    this.showAppBar = true,
    this.changeKeyboardPanelHeight,
    this.onControllerCreated,
  });

  @override
  State<ChatArea> createState() => _ChatAreaState();
}

class _ChatAreaState extends State<ChatArea>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  final customChatListViewController =
      CustomChatListViewController<ChatMessageRecordModel>([]);
  late final scrollController = ScrollController();
  double opacity = 0;

  Color panelBgColor = const Color(0xFFF5F5F5);
  final ImagePicker picker = ImagePicker();
  final FocusNode inputFocusNode = FocusNode();
  final TextEditingController textController = TextEditingController();
  final controller = ChatBottomPanelContainerController<PanelType>();
  late final ChatWebSocketApi socket;
  PanelType currentPanelType = PanelType.none;
  bool _showActionButtons = false;
  late bool readOnly = widget.data?.isTalk == 0 ? true : false;
  late final AnimationController _animationController;
  late final Animation<double> _expandAnimation;
  List<NextIssueData> nextIssueData = [];
  List<int?> closeTimes = [];
  final double _fabDistance = 180.w;
  Timer? timer;
  late final pageController = PageController()..addListener(listener);
  final List<ExpandingActionButton> _actionButtons = [];
  final right = ValueNotifier<double>(0);
  ChatGetTokenModel chatToken = const ChatGetTokenModel();
  List<ChatMessageRecordModel> messageList = [];
  List<PublicAvatarList> avatarList = [];
  final nickNameController = TextEditingController();
  SocketGetUserResModel searchUserData = SocketGetUserResModel();
  bool isPrivateStatus = false;
  final customChatOnlineVipKey = GlobalKey<CustomChatOnlineVipState>();
  late final tools = [
    CarouselItem(
        id: 0,
        label: "表情",
        iconData: CustomIcons.smile,
        onPressed: () {
          updatePanelType(PanelType.emoji);
        }),
    CarouselItem(
        id: 1,
        label: "图片",
        iconData: CustomIcons.picture,
        onPressed: () async {
          final XFile? response =
              await picker.pickImage(source: ImageSource.gallery);
          if (response != null) {
            final File file = File(response.path);
            final bytes = file.readAsBytesSync();
            String base64Image =
                "data:image/png;base64, ${base64Encode(bytes)}";
            var data = SendMsgTextModel(
                code: "R0002",
                dataType: "image",
                chatType: 0,
                roomId: widget.roomId,
                username: GlobalService.to.userInfo.value?.usr,
                level: GlobalService.to.userInfo.value?.curLevelInt.toString(),
                ip: GlobalService.to.userInfo.value?.clientIp,
                t: DateTime.now().millisecondsSinceEpoch.toString(),
                token: AppDefine.userToken?.apiSid,
                msg: base64Image);
            socket.send(jsonEncode(data.toJson()));
          }
        }),
    CarouselItem(
        id: 2,
        label: "扫雷规则",
        iconData: CustomIcons.exclamation_circle,
        onPressed: () {
          ToastUtils.showWidget(CustomConfirmParams(
              showTitle: false,
              // showBottom: false,
              showCancel: true,
              child: ClipRRect(
                borderRadius: 12.radius,
                child: SizedBox(
                  height: Get.context!.mediaQuerySize.height * .85,
                  width: Get.context!.mediaQuerySize.width * .9,
                  child: ListView(
                    physics: const ClampingScrollPhysics(),
                    children: [
                      Assets.assetsImagesMineRule1,
                      Assets.assetsImagesMineRule2,
                      Assets.assetsImagesMineRule3
                    ].map((e) {
                      return Image.asset(e, fit: BoxFit.fitWidth);
                    }).toList(),
                  ),
                ),
              )));
        }),
    CarouselItem(
        id: 3,
        label: "在线客服",
        iconData: CustomIcons.user_1,
        onPressed: () => AppNavigator.toNamed(onlineServicePath)),
    CarouselItem(
        id: 4,
        label: "发红包",
        iconAsset: AppImage.asset("redBag.png", height: 40.w),
        onPressed: () {
          if (GlobalService.isTest) {
            ToastUtils.show('您没有权限“发红包”，登录正式账号');
            return;
          }
          // redBagPanelIndex = 0;
          // showRedbagPanel.value = true;
          ToastUtils.showWidget(CustomConfirmParams(
              child: SizedBox(
                  height: Get.context!.mediaQuerySize.height * .85,
                  width: Get.context!.mediaQuerySize.width * .92,
                  child: CustomRedpack(
                    tabIndex: 0,
                    chatRoom: widget.data,
                    onSend: sendRedPack,
                  ))));
        }),
    CarouselItem(
        id: 5,
        label: "红包扫雷",
        iconAsset: AppImage.asset("mineRedBag.png", height: 40.w),
        onPressed: () {
          if (GlobalService.isTest) {
            ToastUtils.show('您没有权限“发红包”，登录正式账号');
            return;
          }
          // redBagPanelIndex = 1;
          // showRedbagPanel.value = true;
          ToastUtils.showWidget(CustomConfirmParams(
              child: SizedBox(
                  height: Get.context!.mediaQuerySize.height * .85,
                  width: Get.context!.mediaQuerySize.width * .92,
                  child: CustomRedpack(
                    tabIndex: 2,
                    chatRoom: widget.data,
                    onSend: sendRedPack,
                  ))));
        }),
    CarouselItem(
        id: 6,
        label: "存款",
        iconData: CustomIcons.credit_card,
        onPressed: () {
          if (!GlobalService.isShowTestToast) {
            AppNavigator.toNamed(depositPath);
          }
        }),
    CarouselItem(
        id: 7,
        label: "取款",
        iconData: CustomIcons.yen_sign,
        onPressed: () {
          if (!GlobalService.isShowTestToast) {
            AppNavigator.toNamed(depositPath, arguments: [1]);
          }
        }),
    CarouselItem(
        id: 8,
        label: "利息宝",
        iconData: CustomIcons.gg,
        onPressed: () => AppNavigator.toNamed(interestTreasurePath)),
    CarouselItem(
        id: 9,
        label: "每日签到",
        iconData: CustomIcons.star,
        onPressed: () {
          if (!GlobalService.isShowTestToast) {
            AppNavigator.toNamed(mobilePath);
          }
        }),
    CarouselItem(
        id: 10,
        label: "任务大厅",
        iconData: CustomIcons.tasks,
        onPressed: () {
          if (!GlobalService.isShowTestToast) {
            AppNavigator.toNamed(taskPath);
          }
        }),
    CarouselItem(
        id: 11,
        label: "会员中心",
        iconData: CustomIcons.user,
        onPressed: () {
          AppNavigator.back();
          Get.find<MainHomeController>().selectedPath.value = userPath;
        }),
    CarouselItem(
        id: 12,
        label: "额度转换",
        iconData: CustomIcons.exchange_alt,
        onPressed: () {
          if (!GlobalService.isShowTestToast) {
            AppNavigator.toNamed(realtransPath);
          }
        }),
    CarouselItem(
        id: 13,
        label: "异常反馈",
        iconData: CustomIcons.exclamation_circle,
        onPressed: () {
          if (!GlobalService.isShowTestToast) {
            AppNavigator.toNamed(feedbackPath);
          }
        }),
    CarouselItem(
        id: 14,
        label: "口令红包",
        iconAsset: AppImage.asset("kl_radbag.png", height: 40.w),
        onPressed: () {
          if (GlobalService.isTest) {
            ToastUtils.show('您没有权限“发红包”，登录正式账号');
            return;
          }
          // redBagPanelIndex = 2;
          // showRedbagPanel.value = true;
          ToastUtils.showWidget(CustomConfirmParams(
              child: SizedBox(
                  height: Get.context!.mediaQuerySize.height * .85,
                  width: Get.context!.mediaQuerySize.width * .92,
                  child: CustomRedpack(
                    tabIndex: 1,
                    chatRoom: widget.data,
                    onSend: sendRedPack,
                  ))));
        })
  ];

  late StreamController<OperateEvent> eventStreamController =
      StreamController.broadcast();
  listener() {
    right.value = pageController.page!;
  }

  sendRedPack(CreateRedBagParams params) async {
    final data = ChatCreateRedBagParams.fromJson(
      params.toJson(),
    );
    data.coinPwd = "123".md5;
    data.roomId = widget.roomId;
    final res = await ChatHttp.chatCreateRedBag(data);
    ToastUtils.show(res.msg, res.code == 0);
    // socket.send(params.toJson());
  }

  socketListener(value) {
    final json = (jsonDecode(value));
    if (json['betUrl'] != null) {
      // json['betUrl'] = (json['betUrl']);
    }
    LogUtil.d("socket___$value");

    try {
      ChatMessageRecordModel data = const ChatMessageRecordModel();

      if (json['code'] == "-1") {
        ToastUtils.show(json['msg'], false);
        return;
      }
      if (!['0022'].contains(json['code'])) {
        data = ChatMessageRecordModel.fromJson(json).copyWith(
            extfield: jsonEncode(json['redBag']),
            avatar: json['avatar'] ?? json['avator']);
        if (data.uid != null) {
          setState(() {
            messageList.add(data);
          });
          customChatListViewController.insertToBottom(data);
        }
      } else if (json['code'] == "0022") {
        // 用户列表
        setState(() {
          searchUserData = SocketGetUserResModel.fromJson(json);
        });
        customChatOnlineVipKey.currentState
            ?.updateUser(SocketGetUserResModel.fromJson(json));
      }
    } catch (e) {
      LogUtil.e("error___$e");
    }
  }

  Future<void> updateChatToken() async {
    ChatHttp.chatGetToken(ChatGetTokenParams(
            token: AppDefine.userToken?.apiSid, roomId: widget.roomId))
        .then((data) {
      if (data.data != null) {
        setState(() {
          chatToken = data.data!;
        });
      }
      // pageTitle.value = data?.roomName ?? "";
      // chatRoomList.value = data?.chatAry ?? [];
      // chatOnlineMemberCount.value = data?.chatOnlineMemberCount ?? 0;
    });
  }

  getData() async {
    final res = await ChatHttp.chatMessageRecord(ChatMessageRecordParams(
      chatType: "0",
      roomId: widget.roomId,
      number: "20",
    ));
    messageList.addAll(res.models ?? []);
    customChatListViewController.insertAllToBottom(res.models ?? []);
    Future.delayed(300.milliseconds, () {
      scrollController.scrollToBottom();

      setState(() {
        opacity = 1;
      });
    });
  }

  Widget getBallItem(NextIssueData item, String preNum, String preResult) {
    String iconName = 'red';
    bool isRed = item.redBalls?.contains(preNum) ?? false;
    bool isGreen = item.greenBalls?.contains(preNum) ?? false;
    bool isBlue = item.blueBalls?.contains(preNum) ?? false;
    if (isRed) iconName = 'red';
    if (isGreen) iconName = 'green';
    if (isBlue) iconName = 'blue';
    String ballPath = 'assets/images/ball/$iconName.png';
    Widget ball = Container(
        width: 35.w,
        height: 35.w,
        padding: EdgeInsets.only(top: 5.w, left: 4.w),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ballPath),
            fit: BoxFit.fill, // Makes sure the image covers the whole area
          ),
        ),
        child: Text(preNum,
            style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold)));

    return Container(
        margin: EdgeInsets.all(3.w),
        child: Column(
          children: [
            ball,
            Gap(7),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 4.w),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.all(Radius.circular(5.w))),
                child: Text(preResult))
          ],
        ));
  }

  Widget getNextIssueBanner() {
    return AnimatedSize(
      duration: 300.milliseconds,
      child: SizedBox(
        height: nextIssueData.isEmpty ? 0 : 120,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.only(left: 20),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [...getNextIssueItems()]),
        ),
      ),
    );
  }

  Widget lotteryWidget(
      NextIssueData item, List<String> preNum, List<String> preResult) {
    // print("lefngth__${preNum.length}");
    if (item.gameType == 'lhc') {
      return Row(
        children: [
          ...List.generate(preNum.length - 1, (index) {
            return getBallItem(item, preNum[index], preResult[index]);
          }),
          const Text('+'),
          getBallItem(
              item, preNum[preNum.length - 1], preResult[preResult.length - 1])
        ],
      );
    } else if (item.gameType == 'cqssc' ||
        item.gameType == 'gd11x5' ||
        item.gameType == 'pk10' ||
        item.gameType == 'xyft' ||
        item.gameType == 'pk10nn') {
      return Row(
        children: [
          ...List.generate(preNum.length, (index) {
            BoxDecoration decoration;
            if (item.gameType == 'cqssc' || item.gameType == 'gd11x5') {
              decoration = BoxDecoration(
                borderRadius: BorderRadius.circular(30.w),
                color: AppColors.ff900909,
              );
            } else {
              decoration = BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5.w)),
                color: Color(SquareColors[int.parse(preNum[index]) % 10]),
              );
            }
            return Container(
                margin: EdgeInsets.all(3.w),
                child: Column(
                  children: [
                    Container(
                        width: 35.w,
                        height: 35.w,
                        alignment: Alignment.center,
                        decoration: decoration,
                        child: Text(int.parse(preNum[index]).toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold))),
                    Gap(7.w),
                    if (index < preResult.length)
                      Container(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.w))),
                          child: Text(preResult[index])),
                    if (index >= preResult.length)
                      SizedBox(
                        width: 35.w,
                        height: 27.w,
                      )
                  ],
                ));
          }),
        ],
      );
    } else if (item.gameType == 'jsk3') {
      return Row(
        children: [
          ...List.generate(preNum.length, (index) {
            int idx = int.parse(preNum[index]) % 7;
            if (idx == 0) {
              idx = 1;
            }
            return Container(
                margin: EdgeInsets.all(3.w),
                child: Column(
                  children: [
                    AppImage.asset('dice-color/$idx.png', width: 35.w),
                    Gap(7.w),
                    if (index < preResult.length)
                      Container(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.w))),
                          child: Text(preResult[index])),
                    if (index >= preResult.length)
                      Container(
                        width: 35.w,
                        height: 27.w,
                      )
                  ],
                ));
          }),
        ],
      );
    } else {
      return Row(
        children: [
          ...List.generate(preNum.length, (index) {
            return Container(
                margin: EdgeInsets.all(3.w),
                child: Column(
                  children: [
                    Text(preNum[index],
                        style: TextStyle(
                            color: AppColors.textColor4, fontSize: 18.sp)),
                    Gap(7.w),
                    if (index < preResult.length)
                      Container(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.w))),
                          child: Text(preResult[index])),
                    if (index >= preResult.length)
                      SizedBox(
                        width: 35.w,
                        height: 27.w,
                      )
                  ],
                ));
          }),
        ],
      );
    }
  }

  List<Widget> getNextIssueItems() {
    List<Widget> list = [];
    for (int i = 0; i < nextIssueData.length; i++) {
      NextIssueData item = nextIssueData[i];
      List<String> preNum = item.preNum?.split(',') ?? [];
      List<String> preResult = item.preResult?.split(',') ?? [];
      double width = 360;
      if (preNum.length > 7) {
        width = 530;
      }
      // print("nextIssueData.length${item.preNum}__$preNum");

      if (item.preNum != "" && preNum.isNotEmpty) {
        list.add(Container(
            width: width,
            height: 160,
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(right: 12),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      width: 80,
                      padding: EdgeInsets.only(right: 12),
                      child: AppImage.network(item.logo ?? '')),
                  Expanded(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(item.title ?? '',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              Text('第${item.preDisplayNumber ?? ''}期')
                            ]),
                        lotteryWidget(item, preNum, preResult),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Row(children: [
                                const Text('距离第'),
                                Text(item.displayNumber ?? '',
                                    style: const TextStyle(color: Colors.red))
                              ]),
                              // Text("${closeTimes}"),
                              Row(children: [
                                const Text('期截止还有'),
                                Text(
                                    DateUtil.displayCloseTime(
                                        closeTimes[i] ?? 0),
                                    style: const TextStyle(color: Colors.red))
                              ])
                            ])
                      ]))
                ])));
      }
    }
    return list;
  }

  getOpenData() async {
    // if (widget.data?.cronDataTopType != null) {
    final cronTypes = widget.data?.cronDataTopType?.split(",") ?? [];
    LogUtil.w("cronTypes___$cronTypes");
    nextIssueData = [];
    closeTimes = [];
    if (timer?.isActive ?? false) {
      timer?.cancel();
    }
    if (cronTypes.isNotEmpty) {
      for (int i = 0; i < cronTypes.length; i++) {
        await __getNextIssueData(cronTypes[i], i);
      }
    }
    timer = Timer.periodic(1000.milliseconds, (value) {
      List<int?> list = closeTimes;
      // closeTimes.map((e) => e-1).toList();
      for (var i = 0; i < closeTimes.length; i++) {
        final item = closeTimes[i] ?? 0;
        if (item <= 0) {
          __getNextIssueData(cronTypes[i], i);
        } else {
          list.add((closeTimes[i] ?? 1) - 1);
        }
      }
      setState(() {
        closeTimes = list;
      });
    });
  }

  Future<void> __getNextIssueData(String id, int index) async {
    var data = await AppBetChatRepository().getNextIssueData(id: id);
    if (data != null) {
      int index = nextIssueData.indexWhere((item) => item.id == data.id);
      if (index == -1) {
        setState(() {
          nextIssueData.add(data);
          closeTimes.add(DateUtil.getCloseTimes(data.curCloseTime ?? "0"));
        });
      }
    }
  }

  bool updateInputView({
    required bool isReadOnly,
  }) {
    if (readOnly != isReadOnly) {
      readOnly = isReadOnly;
      // You can just refresh the input view.
      setState(() {});
      return true;
    }
    return false;
  }

  updatePanelType(PanelType type) async {
    print("updatePanelType111 $type");
    final isSwitchToKeyboard = PanelType.keyboard == type;
    final isSwitchToEmojiPanel = PanelType.emoji == type;
    bool isUpdated = false;
    switch (type) {
      case PanelType.keyboard:
        updateInputView(isReadOnly: false);
        break;
      case PanelType.emoji:
        isUpdated = updateInputView(isReadOnly: true);
        break;
      default:
        break;
    }

    updatePanelTypeFunc() {
      controller.updatePanelType(
        isSwitchToKeyboard
            ? ChatBottomPanelType.keyboard
            : ChatBottomPanelType.other,
        data: type,
        forceHandleFocus: isSwitchToEmojiPanel
            ? ChatBottomHandleFocus.requestFocus
            : ChatBottomHandleFocus.none,
      );
    }

    if (isUpdated) {
      // Waiting for the input view to update.
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        updatePanelTypeFunc();
      });
    } else {
      updatePanelTypeFunc();
    }
  }

  Widget _buildInputView() {
    return Container(
      // height: 50,
      // color: Colors.white,
      padding: EdgeInsets.only(
          // bottom: context.mediaQueryPadding.bottom
          ),
      child: Row(
        children: [
          const SizedBox(width: 15),
          Expanded(
            child: Listener(
              onPointerUp: (event) {
                // Currently it may be emojiPanel.
                if (readOnly) {
                  updatePanelType(PanelType.keyboard);
                }
              },
              child: TextField(
                focusNode: inputFocusNode,
                readOnly: widget.data?.isTalk == 0 ? true : readOnly,
                showCursor: true,
                controller: textController,
                decoration: InputDecoration(hintText: widget.data?.placeholder),
              ),
            ),
          ),
          GestureDetector(
            child: const Icon(Icons.send_rounded, size: 30),
            onTap: () {
              if (widget.data?.isTalk == 0) {
                updatePanelType(PanelType.none);
                return;
              }
              if (textController.text.trim().isEmpty) {
                return;
              }
              var data = SendMsgTextModel(
                  code: "R0002",
                  dataType: "text",
                  chatType: 0,
                  roomId: widget.roomId,
                  username: GlobalService.to.userInfo.value?.usr,
                  level:
                      GlobalService.to.userInfo.value?.curLevelInt.toString(),
                  ip: GlobalService.to.userInfo.value?.clientIp,
                  t: DateTime.now().millisecondsSinceEpoch.toString(),
                  token: AppDefine.userToken?.apiSid,
                  msg: textController.text);

              if (data.chatType == 1) {
                data.chatUid = chatToken.uid ?? "";
                data.chatName = chatToken.nickname ?? "";
              }
              LogUtil.w(data);
              socket.send(jsonEncode(data.toJson()));
              textController.text = "";
              // 发送消息
              // updatePanelType(
              //   PanelType.emoji == currentPanelType
              //       ? PanelType.keyboard
              //       : PanelType.emoji,
              // );
            },
          ),
          GestureDetector(
            child: const Icon(Icons.add, size: 30),
            onTap: () {
              // if (GlobalService.to.userInfo.value?.isTest??false) {
              //   ToastUtils.show("请登录正式账号");
              //   return;
              // }
              updatePanelType(
                PanelType.tool,
              );
            },
          ),
          const SizedBox(width: 15),
        ],
      ),
    );
  }

  Widget _buildEmojiPickerPanel() {
    // If the keyboard height has been recorded, priority is given to setting
    // the height to the keyboard height.
    double height = 300;
    final keyboardHeight = controller.keyboardHeight;
    if (keyboardHeight != 0) {
      if (widget.changeKeyboardPanelHeight != null) {
        height = widget.changeKeyboardPanelHeight!.call(keyboardHeight);
      } else {
        height = keyboardHeight;
      }
    }

    return Container(
      padding: EdgeInsets.zero,
      height: height,
      color: Colors.blue[50],
      child: GridView.builder(
        padding: const EdgeInsets.all(10),
        itemCount: LotteryConfig.emojiList.length,
        itemBuilder: (c, index) {
          final item = LotteryConfig.emojiList[index];
          return GestureDetector(
            onTap: () {
              // final imageUrl = item;
              // RegExp regExp = RegExp(r'/([^/]+)\.gif$');
              // RegExpMatch? match = regExp.firstMatch(imageUrl) ;

              // if (match != null) {
              //   String? lastPart = match.group(1);
              //   print('最尾部的部分：$lastPart');
              //   textController.text =
              //     "${textController.text}[em_$lastPart]";
              // } else {
              //   print('未找到匹配的部分');
              // }
              textController.text = "${textController.text}[em_$item]";
            },
            child: Container(
              color: Colors.transparent,
              child:
                  AppImage.network("${LotteryConfig.emojiBaseUrl}/$item.gif"),
            ),
          );
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 8,
          // mainAxisSpacing: 5, crossAxisSpacing: 5
        ),
      ),
    );
  }

  Widget _buildToolPanel() {
    return Container(
      height: 300,
      color: context.customTheme?.lotteryMenuBg,
      // color: Colors.red[50],
      child: Column(
        children: [
          Expanded(
            child: PageView(
              controller: pageController,
              children: [
                GridView(
                  padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10),
                  children: tools.sublist(0, 8).map((e) {
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: 20.radius,
                          color: context.customTheme?.reverseFontColor),
                      width: 75,
                      height: 75,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          e.iconData != null
                              ? Icon(
                                  e.iconData,
                                  size: 30,
                                )
                              : SizedBox(
                                  width: 30, height: 30, child: e.iconAsset),
                          Text(e.label).paddingOnly(top: 10)
                        ],
                      ).onTap(e.onPressed),
                    );
                  }).toList(),
                ),
                GridView(
                  padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10),
                  children: tools.sublist(8).map((e) {
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: 20.radius,
                          color: context.customTheme?.reverseFontColor),
                      width: 75,
                      height: 75,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          e.iconData != null
                              ? Icon(
                                  e.iconData,
                                  size: 30,
                                )
                              : SizedBox(
                                  width: 30, height: 30, child: e.iconAsset),
                          Text(e.label).paddingOnly(top: 10)
                        ],
                      ),
                    ).onTap(e.onPressed);
                  }).toList(),
                )
              ],
            ),
          ),
          ValueListenableBuilder(
              valueListenable: right,
              builder: (context, state, child) {
                return Container(
                  width: 100,
                  padding: EdgeInsets.only(
                      left: 30, bottom: context.mediaQueryPadding.bottom),
                  // color: Colors.red,
                  child: Stack(
                    children: [
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(
                            2,
                            (index) => Container(
                                  // color: context.customTheme.,
                                  width: 8, height: 8,
                                  margin: const EdgeInsets.only(right: 4),
                                  decoration: BoxDecoration(
                                      borderRadius: 20.radius,
                                      border: Border.all(
                                        width: 1,
                                      )),
                                )),
                      ),
                      Positioned(
                          bottom: 0,
                          left: 12 * state,
                          child: Container(
                            width: 8,
                            height: 8,
                            decoration: BoxDecoration(
                                borderRadius: 20.radius,
                                color: context.textTheme.bodyMedium?.color),
                          )),
                    ],
                  ),
                );
              })
        ],
      ),
    );
  }

  // 选择头像组件
  handleShowAvatarList() {
    _toggleActionButtons();
    Get.bottomSheet(CustomChatAvatar(
      list: avatarList,
    ));
  }

  /// 修改昵称
  handleNickName() {
    _toggleActionButtons();
    ToastUtils.confirm(CustomConfirmParams(
        submit: () async {
          final res = await ChatHttp.chatUpdateNickname(
              roomId: widget.roomId, text: nickNameController.text);
          if (res.code == 0) {
            ToastUtils.show(res.msg);
            nickNameController.text = "";
          }
        },
        title: "更改昵称",
        contentChild: Container(
          padding: const EdgeInsets.only(bottom: 20),
          child: TextField(
            controller: nickNameController,
          ),
        )));
  }

  handleRankList() {}

  handleShareBetBills() async {
    _toggleActionButtons();
    final res = await ChatHttp.chatGetBetBills();
    if (res.data == null) {
      ToastUtils.show(res.msg);
      return;
    }
    LogUtil.w("rawData____${res.rawData.runtimeType}");
    final betUrl = jsonEncode(res.rawData);
    final msg =
        "投注金额：${res.data?.data?.totalBetMoney}，奖金：${res.data?.data?.totalBonus}，盈亏：${res.data?.data?.totalResultMoney}，跟投人数：${res.data?.data?.userCount}，跟投金额：${res.data?.data?.totalAmount}";
    final data = SendMsgTextModel(
        code: "R0002",
        dataType: "text",
        chatType: 0,
        roomId: widget.roomId,
        username: GlobalService.to.userInfo.value?.usr,
        level: GlobalService.to.userInfo.value?.curLevelInt.toString(),
        ip: GlobalService.to.userInfo.value?.clientIp,
        t: DateTime.now().millisecondsSinceEpoch.toString(),
        token: AppDefine.userToken?.apiSid,
        betUrl: betUrl,
        shareBillFlag: true,
        betFollowFlag: false,
        msg: msg);
    // print("json_____${jsonEncode(data.toJson())}");
    socket.send(jsonEncode({...data.toJson(), "betUrl": res.rawData}));
  }

  /// 打开在线会员弹窗
  handleOnlineVip() {
    _toggleActionButtons();
    ToastUtils.showWidget(CustomConfirmParams(
        onDismiss: () {
          setState(() {
            searchUserData = SocketGetUserResModel();
          });
        },
        child: CustomChatOnlineVip(
          key: customChatOnlineVipKey,
          roomId: widget.roomId,
          data: searchUserData,
          search: (value) {
            socket.send(jsonEncode(value.toJson()));
          },
        )));
  }

  handleChat() {
    _toggleActionButtons();
    setState(() {
      isPrivateStatus = !isPrivateStatus;
    });
  }

  List<ActionButton> getActionButtons() {
    return <ActionButton>[
      ActionButton(
          id: 1,
          label: '分享战绩',
          iconData: CustomIcons.share_square,
          onPressed: handleShareBetBills),
      ActionButton(
          id: 2,
          label: isPrivateStatus ? '聊天室' : '私聊',
          iconData: CustomIcons.comment_dots,
          onPressed: handleChat),
      ActionButton(
          id: 3,
          label: '清屏',
          iconData: CustomIcons.window_maximize,
          onPressed: () {
            customChatListViewController.removeAll();
            _toggleActionButtons();
          }),
      ActionButton(
          id: 4,
          label: '扫雷规则',
          iconData: CustomIcons.file_alt,
          onPressed: () {
            ToastUtils.showWidget(CustomConfirmParams(
                showTitle: false,
                // showBottom: false,
                showCancel: true,
                child: ClipRRect(
                  borderRadius: 12.radius,
                  child: SizedBox(
                    height: Get.context!.mediaQuerySize.height * .85,
                    width: Get.context!.mediaQuerySize.width * .9,
                    child: ListView(
                      physics: const ClampingScrollPhysics(),
                      children: [
                        Assets.assetsImagesMineRule1,
                        Assets.assetsImagesMineRule2,
                        Assets.assetsImagesMineRule3
                      ].map((e) {
                        return Image.asset(e, fit: BoxFit.fitWidth);
                      }).toList(),
                    ),
                  ),
                )));
          }),
      ActionButton(
          id: 5,
          label: '排行榜',
          iconData: CustomIcons.anchor,
          onPressed: handleRankList
          // () => AppNavigator.toNamed(lhcBoardPath)
          ),
      ActionButton(
          id: 6,
          label: '昵称',
          iconData: CustomIcons.github,
          onPressed: handleNickName),
      ActionButton(
          id: 7,
          label: '头像',
          iconData: CustomIcons.user_circle,
          onPressed: handleShowAvatarList),
      ActionButton(
          id: 8,
          label: '在线会员',
          iconData: CustomIcons.users,
          onPressed: handleOnlineVip)
    ];
  }

  Widget _buildPanelContainer() {
    return ChatBottomPanelContainer<PanelType>(
      controller: controller,
      inputFocusNode: inputFocusNode,
      otherPanelWidget: (type) {
        // print("ChatBottomPanelContainer___$type");
        if (type == null) return const SizedBox.shrink();
        switch (type) {
          case PanelType.emoji:
            return _buildEmojiPickerPanel();
          case PanelType.tool:
            return _buildToolPanel();
          default:
            return Container(
              padding:
                  EdgeInsets.only(bottom: context.mediaQueryPadding.bottom),
            );
        }
      },
      onPanelTypeChange: (panelType, data) {
        debugPrint('panelType: $panelType');
        switch (panelType) {
          case ChatBottomPanelType.none:
            currentPanelType = PanelType.none;
            break;
          case ChatBottomPanelType.keyboard:
            currentPanelType = PanelType.keyboard;
            break;
          case ChatBottomPanelType.other:
            if (data == null) return;
            switch (data) {
              case PanelType.emoji:
                currentPanelType = PanelType.emoji;
                break;
              case PanelType.tool:
                currentPanelType = PanelType.tool;
                break;
              default:
                currentPanelType = PanelType.none;
                break;
            }
            break;
        }
      },
      changeKeyboardPanelHeight: widget.changeKeyboardPanelHeight,
      panelBgColor: panelBgColor,
      safeAreaBottom: widget.safeAreaBottom,
    );
  }

  List<ExpandingActionButton> _buildExpandingActionButtons() {
    List<ActionButton> actionButtons0 = getActionButtons();
    _actionButtons.clear();
    final count = actionButtons0.length;
    final step = 360.0 / count;
    for (var i = 0, angleInDegrees = 0.0;
        i < count;
        i++, angleInDegrees += step) {
      _actionButtons.add(
        ExpandingActionButton(
            directionInDegrees: angleInDegrees,
            maxDistance: _fabDistance,
            progress: _expandAnimation,
            isClose: false,
            child: actionButtons0[i]),
      );
    }
    _actionButtons.add(
      ExpandingActionButton(
          directionInDegrees: 0,
          maxDistance: _fabDistance,
          progress: _expandAnimation,
          isClose: true,
          child: ActionButton(
              id: 0,
              iconData: Icons.close,
              color: AppColors.ffC5CBC6,
              iconSize: 50.w,
              onPressed: _toggleActionButtons)),
    );
    return _actionButtons;
  }

  void _toggleActionButtons() {
    _showActionButtons = !_showActionButtons;
    setState(() {
      if (_showActionButtons) {
        _animationController.forward();
      } else {
        _animationController.reverse();
      }
    });
  }

  handleHShowMenu() {
    // print(222);
    _toggleActionButtons();
  }

  @override
  void didUpdateWidget(ChatArea oldWidget) {
    if (widget.data?.cronDataTopType != oldWidget.data?.cronDataTopType) {
      getOpenData();
    }
    if (widget.roomId != oldWidget.roomId && widget.roomId != "") {
      getData();
      getOpenData();
      updateChatToken();
    }
    super.didUpdateWidget(oldWidget);
  }

  getAvatarList() async {
    final res = await UserHttp.userGetAvatarSetting();
    setState(() {
      avatarList = res.data!.publicAvatarList!;
    });
  }

  @override
  void initState() {
    socket = ChatWebSocketApi(roomId: widget.roomId, onListen: socketListener);
    getAvatarList();
    // socket
    _animationController = AnimationController(
      value: 0,
      duration: 300.milliseconds,
      reverseDuration: 300.milliseconds,
      vsync: this,
    );
    _expandAnimation = CurvedAnimation(
      curve: Curves.fastOutSlowIn,
      reverseCurve: Curves.easeOutQuad,
      parent: _animationController,
    );
    getData();
    updateChatToken();
    getOpenData();
    super.initState();
  }

  /// 开红包
  openRedbag(RedBag data, ChatMessageRecordModel details) {
    ToastUtils.showWidget(CustomConfirmParams(
        isCloseAll: true,
        showTitle: false,
        // showBottom: false,
        showCancel: true,
        child: CustomRedpackOpen(
          data: data,
          details: details,
        )));
  }

  @override
  void dispose() {
    scrollController.dispose();

    timer?.cancel();
    pageController.removeListener(listener);
    socket.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      // behavior: HitTestBehavior.opaque,
      onTap: () {
        print("1111");
        FocusScope.of(context).requestFocus(FocusNode());
        updatePanelType(PanelType.none);
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            CustomLoading(
              status: opacity == 0,
              child: Opacity(
                opacity: opacity,
                child: Stack(
                  children: [
                    Column(
                      children: [
                        getNextIssueBanner(),
                        Expanded(
                            child: NotificationListener(
                          onNotification: (notification) {
                            if (notification is ScrollStartNotification) {
                              eventStreamController
                                  .add(OperateEvent.OPERATE_HIDE);
                            } else if (notification is ScrollEndNotification) {
                              eventStreamController
                                  .add(OperateEvent.OPERATE_SHOW);
                            }
                            return true;
                          },
                          child: CustomChatListView(
                            controller: customChatListViewController,
                            isScroll: true,
                            enabledTopLoad: true,
                            scrollController: scrollController,
                            onScrollToTopLoad: () async {
                              await Future.delayed(1000.milliseconds);
                              return false;
                              // controller.page++;
                              // await controller.load();
                              // return controller.isLoad.value;
                            },
                            itemBuilder: (BuildContext context, int index,
                                int position, data) {
                              final item = data as ChatMessageRecordModel;
                              final linegradient = EventUtils.getLinearGradient(
                                  item.levelIconRight ?? "");
                              return Visibility(
                                visible: item.msg != null ||
                                    (item.extfield?.isNotEmpty ?? false),
                                child: Container(
                                  alignment: Alignment.topCenter,
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10, top: 10),
                                  child: Row(
                                    // crossAxisAlignment: CrossAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,

                                    /// 自己在右侧 用 end，对方在左侧用 start
                                    mainAxisAlignment: item.uid ==
                                            GlobalService.to.userInfo.value?.uid
                                        ? MainAxisAlignment.end
                                        : MainAxisAlignment.start,

                                    children: [
                                      Visibility(
                                        visible: item.uid !=
                                            GlobalService
                                                .to.userInfo.value?.uid,
                                        child: ClipRRect(
                                                borderRadius: 40.radius,
                                                // radius: 100,
                                                child: AppImage.network(
                                                    item.avatar ??
                                                        item.avator ??
                                                        "",
                                                    width: 40,
                                                    height: 40))
                                            .onTap(() {}),
                                      ),
                                      Column(
                                        mainAxisAlignment: item.uid ==
                                                GlobalService
                                                    .to.userInfo.value?.uid
                                            ? MainAxisAlignment.end
                                            : MainAxisAlignment.start,
                                        crossAxisAlignment: item.uid ==
                                                GlobalService
                                                    .to.userInfo.value?.uid
                                            ? CrossAxisAlignment.end
                                            : CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(item.username ?? ""),
                                              Container(
                                                  margin: const EdgeInsets.only(
                                                      left: 5),
                                                  decoration: BoxDecoration(
                                                      gradient: linegradient),
                                                  child: HtmlWidget(
                                                    """
                                                      <html>
                                                      ${item.levelIconLeft}${item.levelIconRight}
                                                      </html>
                                                    """,
                                                  )),
                                              // 时间
                                              Text(item.time ?? "")
                                                  .paddingOnly(left: 10),
                                            ],
                                          ).paddingOnly(left: 20),
                                          // Text("11111111122222 ${item.extfield?? ""}"),
                                          item.dataType == "redBag"
                                              ? CustomRedbag(
                                                  data: item.extfield ?? "",
                                                  openRedbag: (v) =>
                                                      openRedbag(v, item),
                                                )
                                              : Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    // Transform.rotate(
                                                    //   angle: 0,
                                                    //   child: const CustomPaint(
                                                    //     painter: TrianglePainter(
                                                    //       strokeColor:
                                                    //           AppColors.ff2089dc,
                                                    //       strokeWidth: 0,
                                                    //       paintingStyle:
                                                    //           PaintingStyle.fill,
                                                    //     ),
                                                    //     child: SizedBox(
                                                    //       height: 15,
                                                    //       width: 15,
                                                    //     ),
                                                    //   ).paddingOnly(top: 20),
                                                    // ),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              5.radius,
                                                          color: AppColors
                                                              .ff2089dc),
                                                      margin:
                                                          const EdgeInsets.only(
                                                              right: 10,
                                                              top: 10),
                                                      padding:
                                                          const EdgeInsets.all(
                                                              10),
                                                      child: ConstrainedBox(
                                                          constraints:
                                                              BoxConstraints(
                                                            minWidth: 0,
                                                            maxWidth: context
                                                                    .mediaQuerySize
                                                                    .width *
                                                                .6,
                                                            minHeight: 0,
                                                          ),
                                                          child: item.betUrl !=
                                                                  null
                                                              ? CustomBet(
                                                                  value: item
                                                                      .betUrl)
                                                              : CustomText(
                                                                  value:
                                                                      item.msg,
                                                                )),
                                                    ),
                                                  ],
                                                ),
                                        ],
                                      ).paddingOnly(
                                        left: 10,
                                      ),
                                      Visibility(
                                        visible: item.uid ==
                                            GlobalService
                                                .to.userInfo.value?.uid,
                                        child: ClipRRect(
                                                borderRadius: 40.radius,
                                                // radius: 100,
                                                child: AppImage.network(
                                                    item.avatar ??
                                                        item.avator ??
                                                        "",
                                                    width: 40,
                                                    height: 40))
                                            .onTap(() {}),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ).onTap(() {
                            updatePanelType(PanelType.none);
                          }),
                        )),
                        // 输入框视图
                        _buildInputView(),
                        _buildPanelContainer(),
                      ],
                    ),
                    DraggableFloatWidget(
                      eventStreamController: eventStreamController,
                      config: const DraggableFloatWidgetBaseConfig(
                        isFullScreen: false,
                        initPositionYInTop: false,
                        initPositionYMarginBorder: 50,
                        borderBottom: 50 + defaultBorderWidth,
                      ),
                      onTap: handleHShowMenu,
                      child: Container(
                        decoration: BoxDecoration(
                            color: context.textTheme.bodyMedium?.color
                                ?.withOpacity(.3),
                            borderRadius: 40.radius),
                        child: Image.asset("assets/images/menu_btn_white.png"),
                      ),
                    ),
                    ..._buildExpandingActionButtons(),
                    Positioned(
                        top: 15,
                        right: 20,
                        child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 2.h, horizontal: 10.w),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: context.theme.cardColor.withOpacity(.7)),
                            child: Text(
                                "当前在线: ${chatToken.chatOnlineMemberCount}",
                                style: context.textTheme.bodyMedium?.copyWith(
                                    color: context
                                        .customTheme?.reverseFontColor))))
                  ],
                ),
              ),
            ),

            // Positioned(
            //   child: Container(
            //     color: Colors.black.withOpacity(.5),
            //     child: CustomChatOnlineVip(
            //       roomId: widget.roomId,
            //       data: searchUserData,
            //       search: (value) {
            //         socket.send(jsonEncode(value.toJson()));
            //       },
            //     ),
            //   ),
            // )
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class CustomText extends StatelessWidget {
  final String? value;
  final TextStyle? style;
  final TextOverflow? overflow;
  const CustomText({super.key, this.value, this.style, this.overflow});

  @override
  Widget build(BuildContext context) {
    return SelectionArea(
      child: ExtendedText(
        """${value?.replaceAll(r'\n', '\n')}""",
        style: style,
        overflow: overflow,
        specialTextSpanBuilder: MySpecialTextSpanBuilder(),
      ),
    );
  }
}

class ExtendedTextField {}

class CustomBet extends StatelessWidget {
  final BetUrl? value;
  const CustomBet({super.key, this.value});

  @override
  Widget build(BuildContext context) {
    return Wrap(
      runAlignment: WrapAlignment.spaceBetween,
      alignment: WrapAlignment.spaceBetween,
      // spacing: 8,
      children: [
        SizedBox(
          width: 75,
          height: 75,
          child: Stack(
            children: [
              Image.asset(
                Assets.assetsImagesBetBg,
                width: 75,
                height: 75,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(value?.data?.totalBetMoney ?? ""),
                ],
              ).marginOnly(top: 12),
            ],
          ),
        ),
        SizedBox(
          width: 75,
          height: 75,
          child: Stack(
            children: [
              Image.asset(Assets.assetsImagesBonusBg, width: 80, height: 80),
              // Text(value?.data?.totalBonus ?? ""),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(value?.data?.totalBonus ?? ""),
                ],
              ).marginOnly(top: 12),
            ],
          ),
        ),
        SizedBox(
          width: 75,
          height: 75,
          child: Stack(
            children: [
              Image.asset(Assets.assetsImagesProfitandLossBg,
                  width: 80, height: 80),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(value?.data?.totalResultMoney ?? ""),
                ],
              ).marginOnly(top: 12),
            ],
          ),
        ),
        SizedBox(
          width: 75,
          height: 75,
          child: Stack(
            children: [
              Image.asset(
                Assets.assetsImagesPeopleNumber,
                width: 75,
                height: 75,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(value?.data?.userCount ?? ""),
                ],
              ).marginOnly(top: 12),
            ],
          ),
        ),
        SizedBox(
          width: 75,
          height: 75,
          child: Stack(
            children: [
              Image.asset(Assets.assetsImagesInvestedAmount,
                  width: 80, height: 80),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(value?.data?.totalAmount ?? ""),
                ],
              ).marginOnly(top: 12),
            ],
          ),
        )
      ],
    );
  }
}

class CustomRedbag extends StatelessWidget {
  final String data;
  final Function(RedBag)? openRedbag;
  const CustomRedbag({super.key, required this.data, this.openRedbag});

  @override
  Widget build(BuildContext context) {
    final item = RedBag.fromJson(jsonDecode(data));
    return Container(
      height: 100,
      margin: const EdgeInsets.only(top: 10),
      width: context.mediaQuerySize.width * .7,
      decoration: const BoxDecoration(
          image: DecorationImage(
        fit: BoxFit.fill,
        image: AssetImage(Assets.assetsImagesChatHbL),
      )),
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.title ?? "",
                  style: context.textTheme.titleMedium
                      ?.copyWith(color: context.customTheme?.reverseFontColor)),
              Text("点击领取",
                      style: context.textTheme.bodyMedium?.copyWith(
                          color: context.customTheme?.reverseFontColor))
                  .paddingOnly(top: 5)
            ],
          ).marginOnly(left: 80, top: 10),
          Positioned(
              left: 15,
              bottom: 2,
              child: Text(item.genre == "1"
                  ? '普通红包'
                  : item.genre == "2"
                      ? "扫雷红包"
                      : "口令红包"))
        ],
      ),
    ).onTap(() => openRedbag?.call(item));
  }
}
