import 'package:flutter/material.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class CustomToastTip extends StatefulWidget {
  final CustomConfirmParams? params;
  const CustomToastTip({super.key, this.params});

  @override
  State<CustomToastTip> createState() => _CustomToastTipState();
}

class _CustomToastTipState extends State<CustomToastTip> {
  @override
  Widget build(BuildContext context) {
    return UnconstrainedBox(
      child: Container(
        width: context.mediaQuerySize.width * .9,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        decoration: BoxDecoration(
            color: context.theme.scaffoldBackgroundColor,
            borderRadius: 10.radius),
        child: Column(
          children: [
            Text(widget.params?.title ?? "",
                style: context.textTheme.titleMedium?.copyWith(fontSize: 20)),
            Container(
              margin: const EdgeInsets.only(top: 20),
              padding: const EdgeInsets.symmetric(vertical: 10),
              width: context.mediaQuerySize.width * .8,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: context.customTheme?.lotteryAppBar,
                  borderRadius: 10.radius),
              child: Text("确定",
                  style: context.textTheme.titleMedium?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: context.customTheme?.reverseFontColor)),
            ).onTap(() {
              if (widget.params?.submit == null) {
                ToastUtils.closeAll();
                return;
              }
              widget.params?.submit?.call();
            })
          ],
        ),
      ),
    );
  }
}
