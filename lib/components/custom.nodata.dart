import 'package:flutter/material.dart';
import 'package:fpg_flutter/constants/assets.dart';

class CustomNodata extends StatelessWidget {
  final String? name;
  const CustomNodata({super.key, this.name});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          Assets.assetsImagesPl,
          width: 300,
        ),
        Text(name ?? "暂无数据")
      ],
    );
  }
}
