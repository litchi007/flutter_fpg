import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/models/lottery_trend_data/lottery_trend_data.dart';
import 'package:fpg_flutter/page/lotteryTrend/controller/get_trend_data.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class LotteryTrendController extends GetxController {
  static LotteryTrendController get to => Get.find<LotteryTrendController>();

  /// 游戏Id
  final gameId = ''.obs;

  /// 游戏名称
  final gameName = ''.obs;

  /// 游戏类型
  final gameType = ''.obs;

  /// 选中号码
  final defaultNumber = 0.obs;

  final loading = true.obs;

  /// 开奖历史数据
  late List<ListModel.LotteryList> dataList;

  /// 走势图数据
  final trendData = TrendData().obs;

  Future getHistoryData(bool isRefresh) async {
    if (isRefresh) {
      loading.value = true;
    }

    final res = await GameHttp.gameLotteryHistory(
        GameLotteryHistoryParams(id: gameId.value, rows: '100'));

    dataList = (res.data?.list ?? []);
    if (dataList.isNotEmpty) {
      var data = getTrendData(defaultNumber.value, gameType.value, dataList);
      trendData.value = data;
    }

    if (isRefresh) {
      loading.value = false;
    }
  }

  refreshData() {
    getHistoryData(false);
  }

  handleSelectedNumber(int number) {
    defaultNumber.value = number;
    var data = getTrendData(defaultNumber.value, gameType.value, dataList);
    trendData.value = data;
  }

  handleSelectedLottery(Lottery lottery) {
    defaultNumber.value = 0;
    gameName.value = lottery.shortName ?? '';
    gameId.value = lottery.id ?? '';
    gameType.value = lottery.gameType ?? '';
    trendData.value = TrendData();
    getHistoryData(true);
  }

  /// 获取列表
  Future getGameLotteryGroupGames() async {
    final res = await GameHttp.gameLotteryGroupGames();

    if (res.models?.isNotEmpty ?? false) {
      List<Lottery> lotteries = [];
      res.models?.forEach((m) {
        lotteries.addAll(m.lotteries?.where((l) => l.enable == '1') ?? []);
      });

      gameId.value = lotteries.first.id ?? '';
      gameName.value = lotteries.first.shortName ?? '';
      gameType.value = lotteries.first.gameType ?? '';
      getHistoryData(true);
    }
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null && arguments is Lottery) {
      gameName.value = arguments.shortName ?? '';
      gameId.value = arguments.id ?? '';
      gameType.value = arguments.gameType ?? '';
    }

    super.onInit();
  }

  @override
  void onReady() {
    if (gameId.value.isEmpty) {
      getGameLotteryGroupGames();
    } else {
      getHistoryData(true);
    }
    super.onReady();
  }
}
