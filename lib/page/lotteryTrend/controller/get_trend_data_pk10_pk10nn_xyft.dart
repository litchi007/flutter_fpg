import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/models/lottery_trend_data/lottery_trend_data.dart';
import 'package:get/get.dart';
import 'dart:math';

TrendData getTrendDataPk10Pk10nnXyft(
    String fromName, List<ListModel.LotteryList> thisData,
    [int defaultNumber = 0]) {
  List<List<dynamic>> numberArray = [];
  List<Map<String, double>> positionArr = [];
  List<String> header = [
    '冠',
    '亚',
    '三',
    '四',
    '五',
    '六',
    '七',
    '八',
    '九',
    '十',
  ];

  if (fromName != 'xyft') {
    header.add('冠亚和');
  }

  bool isSumUp = defaultNumber == 10;

  for (int i = 0; i < thisData.length; i++) {
    var element = thisData[i];
    var lotteryData = (element.num ?? '').split(',');
    numberArray.add(List.filled(isSumUp ? 18 : lotteryData.length, 0));
    if (isSumUp) {
      for (var j = 0; j < 18; j++) {
        if (int.parse(lotteryData[0]) + int.parse(lotteryData[1]) == j + 3) {
          numberArray[i][j] = 'seat';
        } else {
          numberArray[i][j] = 0;
        }
      }
    } else {
      for (var j = 0; j < lotteryData.length; j++) {
        if (int.parse(lotteryData[defaultNumber]) == j + 1) {
          numberArray[i][j] = 'seat';
        } else {
          numberArray[i][j] = 0;
        }
      }
    }
  }

  List<List<dynamic>> reverseData = numberArray.reversed.toList();
  List<List<dynamic>> tabData = [];

  for (var i = 0; i < reverseData.length; i++) {
    var totalData = reverseData[i];
    tabData.add(List.filled(totalData.length, 0));
    for (var j = 0; j < totalData.length; j++) {
      var sData = totalData[j];
      if (sData == 0) {
        if (i == 0) {
          tabData[i][j] = 1;
        } else {
          tabData[i][j] =
              (tabData[i - 1][j] == 'seat' ? 1 : tabData[i - 1][j] + 1);
        }
      } else {
        tabData[i][j] = sData;
      }
    }
  }

  List<List<dynamic>> finalData = tabData.reversed.toList();
  List<List<dynamic>> newTr = [];

  for (var i = 0; i < thisData.length; i++) {
    newTr.add(List.filled(isSumUp ? 18 : 11, 0));
  }

  /// 计算宽高
  var width = 28.0;
  if (!isSumUp) {
    final screenWidth = Get.context!.mediaQuerySize.width;
    width = (screenWidth - 120) / (newTr[0].length - 1);
  }
  const height = 28.0;

  for (var i = thisData.length - 1; i >= 0; i--) {
    var element = thisData[i];
    var lotteryData = (element.num ?? '').split(',');
    for (var j = 0; j < (isSumUp ? 18 : 11); j++) {
      if (j == 0) {
        newTr[i][j] = element.displayNumber ?? element.issue;
      } else {
        if (isSumUp) {
          if (int.parse(lotteryData[0]) + int.parse(lotteryData[1]) == j + 2) {
            positionArr.add({
              'x': j * width + 120 - width / 2,
              'y': height * (positionArr.length) + (height * 3) / 2
            });
            newTr[i][j] =
                (int.parse(lotteryData[0]) + int.parse(lotteryData[1]))
                    .toString()
                    .padLeft(2, '0');
          } else {
            newTr[i][j] = finalData[i][j - 1];
          }
        } else {
          if (int.parse(lotteryData[defaultNumber]) == j) {
            positionArr.add({
              'x': j * width + 120 - width / 2,
              'y': height * (positionArr.length) + (height * 3) / 2
            });
            newTr[i][j] = lotteryData[defaultNumber];
          } else {
            newTr[i][j] = finalData[i][j - 1];
          }
        }
      }
    }
  }

  var maximumOmission = getMaximumOmission(newTr, isSumUp);
  var maximumConnection = getMaximumConnection(newTr, isSumUp);
  var totalTimes = getTotalTimes(newTr, isSumUp);
  var averageOmission = getAverageOmission(totalTimes, isSumUp);

  return TrendData(
      data: newTr.reversed.toList(),
      totalTimes: totalTimes,
      averageOmission: averageOmission,
      maximumOmission: maximumOmission,
      maximumConnection: maximumConnection,
      positionArr: positionArr,
      header: header);
}

List<String> getMaximumConnection(List<List<dynamic>> newTr, bool isSumUp) {
  List<String> maximumConnection = ['最大连出'];
  int maxCount = 0;

  for (int i = 1; i < (isSumUp ? 18 : 11); i++) {
    int count = 1;
    for (int index = 0; index < newTr.length; index++) {
      if (newTr[index][i] is String) {
        for (int j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is String) {
            count++;
          } else {
            maxCount = max(maxCount, count);
            count = 1;
            break;
          }
        }
      }
    }
    maximumConnection.add(maxCount.toString());
    maxCount = 0;
  }

  return maximumConnection;
}

List<String> getTotalTimes(List<List<dynamic>> newTr, bool isSumUp) {
  List<String> totalTimes = ['出现总次数'];

  if (isSumUp) {
    for (var i = 1; i < 18; i++) {
      int count = 0;
      for (var item in newTr) {
        String num = (i + 2).toString().padLeft(2, '0');
        if (item[i] == num) {
          count++;
        }
      }
      totalTimes.add(count.toString());
    }
  } else {
    for (var i = 1; i < 11; i++) {
      int count = 0;
      for (var item in newTr) {
        String num = i.toString().padLeft(2, '0');
        if (item[i] == num) {
          count++;
        }
      }
      totalTimes.add(count.toString());
    }
  }

  return totalTimes;
}

List<String> getMaximumOmission(List<List<dynamic>> newTr, bool isSumUp) {
  List<String> maximumOmission = ['最大遗漏'];
  int omission = 0;

  for (var i = 1; i < (isSumUp ? 18 : 11); i++) {
    if (newTr[0][i] is! String) {
      omission = newTr[0][i];
    }
    for (int index = 0; index < newTr.length; index++) {
      if (newTr[index][i] is String) {
        for (int j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is! String) {
            if (omission < newTr[j][i]) {
              omission = newTr[j][i];
              break;
            }
          }
        }
      }
    }
    maximumOmission.add(omission.toString());
    omission = 0;
  }

  return maximumOmission;
}

List<String> getAverageOmission(List<String> totalTimes, bool isSumUp) {
  List<String> averageOmission = ['平均遗漏'];

  for (var i = 1; i < (isSumUp ? 18 : 11); i++) {
    int totalCount = int.parse(totalTimes[i]);
    averageOmission.add((100 / (totalCount + 1)).round().toString());
  }

  return averageOmission;
}
