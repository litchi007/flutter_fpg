// Define the functions for each case
import 'package:fpg_flutter/page/lotteryTrend/controller/get_trend_data_pk10_pk10nn_xyft.dart';
import 'package:fpg_flutter/page/lotteryTrend/controller/get_trend_data_cqssc_qxc_pcdd.dart';
import 'package:fpg_flutter/page/lotteryTrend/controller/get_trend_data_gdkl10_xync.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/page/lotteryTrend/controller/get_trend_data_gd11x5.dart';
import 'package:fpg_flutter/page/lotteryTrend/controller/get_trend_data_jsk3.dart';
import 'package:fpg_flutter/page/lotteryTrend/controller/get_trend_data_lhc.dart';
import 'package:fpg_flutter/models/lottery_trend_data/lottery_trend_data.dart';

TrendData getTrendData(
    int defaultNumber, String fromName, List<ListModel.LotteryList> data) {
  print('↓↓↓↓↓↓↓↓ from_name ↓↓↓↓↓↓↓↓');
  print('from_name =======> $fromName');
  print('↑↑↑↑↑↑↑↑ from_name ↑↑↑↑↑↑↑↑');

  switch (fromName) {
    case 'pcdd':
    case 'cqssc':
    case 'qxc':
      return getTrendData_cqssc_qxc_pcdd(fromName, data, defaultNumber);
    case 'xyft':
    case 'pk10':
    case 'pk10nn':
      return getTrendDataPk10Pk10nnXyft(fromName, data, defaultNumber);
    case 'jsk3':
      return getTrendDataJsk3(data, defaultNumber);
    case 'gd11x5':
      return getTrendDataGd11x5(data, defaultNumber);
    case 'gdkl10':
    case 'xync':
      return getTrendData_gdkl10_xync(data, defaultNumber);
    case 'lhc':
      return getTrendDataLhc(data, defaultNumber);
    default:
      throw ArgumentError('Invalid fromName: $fromName');
  }
}
