import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/models/lottery_trend_data/lottery_trend_data.dart';
import 'package:get/get.dart';
import 'dart:math';

TrendData getTrendDataGd11x5(List<ListModel.LotteryList> data,
    [int defaultNumber = 0]) {
  List<List<dynamic>> numberArray = [];
  List<Map<String, double>> positionArr = [];
  List<String> header = ['一', '二', '三', '四', '五'];

  for (var i = 0; i < data.length; i++) {
    var element = data[i];
    var lotteryData = (element.num ?? '').split(',');
    numberArray.add(List.filled(12, 0));
    for (int j = 0; j < 12; j++) {
      if (int.parse(lotteryData[defaultNumber]) == j + 1) {
        numberArray[i][j] = 'seat'; //开奖号码占位
      } else {
        numberArray[i][j] = 0; //遗漏
      }
    }
  }

  var reverseData = numberArray.reversed.toList();
  List<List<dynamic>> tabData = [];

  for (var i = 0; i < reverseData.length; i++) {
    var totalData = reverseData[i];
    tabData.add(List.filled(12, 0));
    for (var j = 0; j < 12; j++) {
      var sData = totalData[j];
      if (sData == 0) {
        if (i == 0) {
          tabData[i][j] = 1;
        } else {
          if (tabData[i - 1][j] == 'seat') {
            tabData[i][j] = 1;
          } else {
            tabData[i][j] = tabData[i - 1][j] + 1;
          }
        }
      } else {
        tabData[i][j] = sData;
      }
    }
  }

  var thisFinal = tabData.reversed.toList();
  List<List<dynamic>> newTr = [];

  for (var i = 0; i < data.length; i++) {
    newTr.add(List.filled(12, 0));
  }

  /// 计算宽高
  final screenWidth = Get.context!.mediaQuerySize.width;
  const width = 28;
  const height = 28;

  for (var i = data.length - 1; i >= 0; i--) {
    var element = data[i];
    var lotteryData = (element.num ?? '').split(',');
    for (var j = 0; j < 12; j++) {
      if (j == 0) {
        newTr[i][j] = element.displayNumber ?? element.issue;
      } else {
        if (int.parse(lotteryData[defaultNumber]) == j) {
          positionArr.add({
            'x': j * width + 120 - width / 2,
            'y': height * positionArr.length + (height * 3) / 2,
          });
          newTr[i][j] = lotteryData[defaultNumber];
        } else {
          newTr[i][j] = thisFinal[i][j - 1];
        }
      }
    }
  }

  var maximumOmission = getMaximumOmission(newTr);
  var maximumConnection = getMaximumConnection(newTr);
  var totalTimes = getTotalTimes(newTr);
  var averageOmission = getAverageOmission(totalTimes);

  return TrendData(
      data: newTr.reversed.toList(),
      totalTimes: totalTimes,
      averageOmission: averageOmission,
      maximumOmission: maximumOmission,
      maximumConnection: maximumConnection,
      positionArr: positionArr,
      header: header);
}

List<String> getMaximumConnection(List<List<dynamic>> newTr) {
  var maximumConnection = ['最大连出'];
  int maxCount = 0;

  for (var i = 1; i < 12; i++) {
    int count = 1;
    for (var index = 0; index < newTr.length; index++) {
      if (newTr[index][i] is String) {
        for (var j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is String) {
            count++;
          } else {
            maxCount = max(maxCount, count);
            count = 1;
            break;
          }
        }
      }
    }
    maximumConnection.add(maxCount.toString());
    maxCount = 0;
  }

  return maximumConnection;
}

List<String> getTotalTimes(List<List<dynamic>> newTr) {
  var totalTimes = ['出现总次数'];

  for (var i = 1; i < 12; i++) {
    int count = 0;
    for (var item in newTr) {
      var num = (i < 10) ? '0$i' : i.toString();
      if (item[i] == num) {
        count++;
      }
    }
    totalTimes.add(count.toString());
  }

  return totalTimes;
}

List<String> getMaximumOmission(List<List<dynamic>> newTr) {
  var maximumOmission = ['最大遗漏'];
  int omission = 0;

  for (var i = 1; i < 12; i++) {
    if (newTr[0][i] is! String) {
      omission = newTr[0][i];
    }
    for (var index = 0; index < newTr.length; index++) {
      if (newTr[index][i] is String) {
        for (var j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is! String) {
            if (omission < newTr[j][i]) {
              omission = newTr[j][i];
              break;
            }
          }
        }
      }
    }
    maximumOmission.add(omission.toString());
    omission = 0;
  }

  return maximumOmission;
}

List<String> getAverageOmission(List<String> totalTimes) {
  var averageOmission = ['平均遗漏'];

  for (var i = 1; i < 12; i++) {
    averageOmission
        .add((100 / (int.parse(totalTimes[i]) + 1)).round().toString());
  }

  return averageOmission;
}
