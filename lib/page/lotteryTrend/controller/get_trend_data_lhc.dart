import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/models/lottery_trend_data/lottery_trend_data.dart';
import 'dart:math';

TrendData getTrendDataLhc(List<ListModel.LotteryList> thisData,
    [int defaultNumber = 0]) {
  List<List<dynamic>> numberArray = [];
  List<List<dynamic>> tabData = [];
  List<List<dynamic>> newTr = [];

  // Populate numberArray
  for (var i = 0; i < thisData.length; i++) {
    var element = thisData[i];
    var lotteryData = (element.num ?? '').split(',');
    numberArray.add(List.filled(50, 0));

    for (var j = 0; j < 50; j++) {
      var colNumber = j + 1;
      var colNumberStr = colNumber < 10 ? '0$colNumber' : '$colNumber';
      var thisNumberIndex = lotteryData.indexOf(colNumberStr);
      if (thisNumberIndex > -1 && thisNumberIndex != 6) {
        numberArray[i][j] = 'seat';
      } else if (thisNumberIndex == 6) {
        numberArray[i][j] = 'redseat';
      } else {
        numberArray[i][j] = 0;
      }
    }
  }

  var reversedData = numberArray.reversed.toList();

  for (var i = 0; i < reversedData.length; i++) {
    var totalData = reversedData[i];
    tabData.add(List.filled(50, 0));
    for (var j = 0; j < totalData.length; j++) {
      var sData = totalData[j];

      if (sData == 0) {
        if (i == 0) {
          tabData[i][j] = 1;
        } else {
          if (tabData[i - 1][j] == 'seat' || tabData[i - 1][j] == 'redseat') {
            tabData[i][j] = 1;
          } else {
            tabData[i][j] = tabData[i - 1][j] + 1;
          }
        }
      } else {
        tabData[i][j] = sData;
      }
    }
  }

  var finalData = tabData.reversed.toList();

  for (var i = 0; i < thisData.length; i++) {
    newTr.add(List.filled(50, 0));
  }

  for (var i = thisData.length - 1; i >= 0; i--) {
    var element = thisData[i];
    for (var j = 0; j < 50; j++) {
      if (j == 0) {
        newTr[i][j] = element.displayNumber ?? element.issue;
      } else {
        var previousValue = finalData[i][j - 1];
        if (previousValue == 'seat' || previousValue == 'redseat') {
          newTr[i][j] = previousValue == 'redseat' ? 'red$j' : '$j';
        } else {
          newTr[i][j] = previousValue;
        }
      }
    }
  }

  var maximumOmission = getMaximumOmission(newTr);
  var maximumConnection = getMaximumConnection(newTr);
  var totalTimes = getTotalTimes(newTr);
  var averageOmission = getAverageOmission(totalTimes);

  return TrendData(
      averageOmission: averageOmission,
      maximumConnection: maximumConnection,
      maximumOmission: maximumOmission,
      totalTimes: totalTimes,
      data: newTr.reversed.toList());
}

// Helper functions
List<String> getMaximumConnection(List<List<dynamic>> newTr) {
  var maximumConnection = ['最大连出'];
  var maxCount = 0;

  for (var i = 1; i < 50; i++) {
    var count = 1;

    for (var index = 0; index < newTr.length; index++) {
      if (newTr[index][i] is String) {
        for (var j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is String) {
            count++;
          } else {
            maxCount = max(maxCount, count);
            count = 1;
            break;
          }
        }
      }
    }

    maximumConnection.add(maxCount.toString());
    maxCount = 0;
  }

  return maximumConnection;
}

List<String> getTotalTimes(List<List<dynamic>> newTr) {
  var totalTimes = ['出现总次数'];

  for (var i = 1; i < 50; i++) {
    var count = 0;

    for (var item in newTr) {
      var num = i.toString();
      var rednum = 'red$i';
      if (item[i] == rednum || item[i] == num) {
        count++;
      }
    }

    totalTimes.add(count.toString());
  }

  return totalTimes;
}

List<String> getMaximumOmission(List<List<dynamic>> newTr) {
  var maximumOmission = ['最大遗漏'];
  var omission = 0;

  for (var i = 1; i < 50; i++) {
    if (newTr[0][i] is! String) {
      omission = newTr[0][i];
    }

    for (var index = 0; index < newTr.length; index++) {
      if (newTr[index][i] is String) {
        for (var j = index + 1; j < newTr.length; j++) {
          if (newTr[j][i] is! String) {
            if (omission < newTr[j][i]) {
              omission = newTr[j][i];
              break;
            }
          }
        }
      }
    }

    maximumOmission.add(omission.toString());
    omission = 0;
  }

  return maximumOmission;
}

List<String> getAverageOmission(List<String> totalTimes) {
  var averageOmission = ['平均遗漏'];

  for (var i = 1; i < 50; i++) {
    averageOmission
        .add(((100 / (int.parse(totalTimes[i]) + 1)).round()).toString());
  }

  return averageOmission;
}
