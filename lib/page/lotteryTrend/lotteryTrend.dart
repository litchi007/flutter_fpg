import 'package:dartx/dartx.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/page/lotteryGameList/lotteryGameList.dart';
import 'package:fpg_flutter/page/lotteryTrend/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class LotteryTrendPage extends GetView<LotteryTrendController> {
  LotteryTrendPage({super.key});
  final tags = Get.parameters["id"];
  @override
  String? get tag => tags;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: const Color(0xffb89b65),
            header: ('开奖走势')),
        body: SafeArea(
          child: Obx(() {
            if (controller.loading.value) {
              return Container(
                  color: Colors.white,
                  child: Center(child: Text('数据加载中, 请稍等...')));
            }

            return Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      physics: const ClampingScrollPhysics(),
                      child: _getHeaderAndScrollView()),
                ),
                const SizedBox(
                  height: 10,
                ),
                _getTrendTool()
              ],
            );
          }),
        ));
  }

  Widget _getHeaderAndScrollView() {
    return Obx(() {
      return controller.trendData.value.header != null
          ? Column(
              children: [
                Obx(() => Wrap(
                      spacing: 10,
                      alignment: WrapAlignment.spaceBetween,
                      runSpacing: 10,
                      children: (controller.trendData.value.header ?? [])
                          .mapIndexed((i, e) {
                        return Container(
                          width: 66,
                          height: 35,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: controller.defaultNumber.value == i
                                  ? '#f39b67'.color()
                                  : Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              border: Border.all(
                                  color: '#BDBDBD'.color(), width: 1)),
                          child: Text(e,
                              style: TextStyle(
                                  color: controller.defaultNumber.value == i
                                      ? Colors.white
                                      : '#999999'.color())),
                        ).onTap(() {
                          controller.handleSelectedNumber(i);
                        });
                      }).toList(),
                    ).paddingOnly(top: 10, bottom: 10)),
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    physics: const ClampingScrollPhysics(),
                    child: _getLinePainter())
              ],
            )
          : SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              physics: const ClampingScrollPhysics(),
              child: _getLinePainter());
    });
  }

  Widget _getLinePainter() {
    return Obx(() {
      if (controller.trendData.value.positionArr != null) {
        return Stack(
          children: [
            Column(
              children: _getChildrenWidget(),
            ),
            Obx(() => CustomPaint(
                  painter:
                      LinePainter(controller.trendData.value.positionArr ?? []),
                ))
          ],
        );
      } else {
        return Column(
          children: _getChildrenWidget(),
        );
      }
    });
  }

  List<Widget> _getChildrenWidget() {
    List<Widget> children = [];
    children.add(Obx(
      () => Row(
        children:
            (controller.trendData.value.data?[0] ?? []).mapIndexed((j, item) {
          return Container(
            width: j == 0 ? 120 : _getTtemWidth(),
            height: controller.gameType.value == 'lhc' ? _getTtemWidth() : 28,
            decoration: BoxDecoration(
                color: '#C1ADAD'.color(),
                border: Border.all(width: 0.5, color: '#CCCCCC'.color())),
            alignment: Alignment.center,
            child: Text(
              j == 0 ? '期数' : _getHeaderIndex(controller.gameType.value, j),
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          );
        }).toList(),
      ),
    ));
    children.add(Obx(() => Column(
            children:
                (controller.trendData.value.data ?? []).mapIndexed((i, row) {
          return Row(
            children: (controller.trendData.value.data?[i] ?? [])
                .mapIndexed((j, item) {
              return Container(
                width: j == 0 ? 120 : _getTtemWidth(),
                height:
                    controller.gameType.value == 'lhc' ? _getTtemWidth() : 28,
                decoration: BoxDecoration(
                    color: j == 0 ? '#C1ADAD'.color() : '#D4D4ED'.color(),
                    border: Border.all(width: 0.5, color: '#CCCCCC'.color())),
                alignment: Alignment.center,
                child: _getContentChild(j, item),
              );
            }).toList(),
          );
        }).toList())));
    children.add(Obx(
      () => Row(
        children:
            (controller.trendData.value.totalTimes ?? []).mapIndexed((j, item) {
          return Container(
            width: j == 0 ? 120 : _getTtemWidth(),
            height: controller.gameType.value == 'lhc' ? _getTtemWidth() : 28,
            decoration: BoxDecoration(
                color: '#C1ADAD'.color(),
                border: Border.all(width: 0.5, color: '#CCCCCC'.color())),
            alignment: Alignment.center,
            child: Text(
              item,
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          );
        }).toList(),
      ),
    ));
    children.add(Obx(() => Row(
          children: (controller.trendData.value.averageOmission ?? [])
              .mapIndexed((j, item) {
            return Container(
              width: j == 0 ? 120 : _getTtemWidth(),
              height: controller.gameType.value == 'lhc' ? _getTtemWidth() : 28,
              decoration: BoxDecoration(
                  color: '#C1ADAD'.color(),
                  border: Border.all(width: 0.5, color: '#CCCCCC'.color())),
              alignment: Alignment.center,
              child: Text(
                item,
                style: TextStyle(color: Colors.white, fontSize: 13),
              ),
            );
          }).toList(),
        )));
    children.add(Obx(
      () => Row(
        children: (controller.trendData.value.maximumOmission ?? [])
            .mapIndexed((j, item) {
          return Container(
            width: j == 0 ? 120 : _getTtemWidth(),
            height: controller.gameType.value == 'lhc' ? _getTtemWidth() : 28,
            decoration: BoxDecoration(
                color: '#C1ADAD'.color(),
                border: Border.all(width: 0.5, color: '#CCCCCC'.color())),
            alignment: Alignment.center,
            child: Text(
              item,
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          );
        }).toList(),
      ),
    ));
    children.add(Obx(
      () => Row(
        children: (controller.trendData.value.maximumConnection ?? [])
            .mapIndexed((j, item) {
          return Row(children: [
            Container(
              width: j == 0 ? 120 : _getTtemWidth(),
              height: controller.gameType.value == 'lhc' ? _getTtemWidth() : 28,
              decoration: BoxDecoration(
                  color: '#C1ADAD'.color(),
                  border: Border.all(width: 0.5, color: '#CCCCCC'.color())),
              alignment: Alignment.center,
              child: Text(
                item,
                style: TextStyle(color: Colors.white, fontSize: 13),
              ),
            ),
          ]);
        }).toList(),
      ),
    ));
    return children;
  }

  Widget _getContentChild(int j, dynamic item) {
    if (j != 0 && item is String && item != 'seat') {
      return Container(
        width: 28,
        height: 28,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: _getBackgroundCoolor(j, item),
          borderRadius:
              controller.gameType.value == 'lhc' ? 0.radius : 14.radius,
        ),
        child: Text(
          _getText(j, item),
          style: TextStyle(color: Colors.white, fontSize: 13),
        ),
      );
    } else {
      return Text(_getText(j, item),
          style: TextStyle(
              color: j == 0 ? Colors.white : '#AAAAAA'.color(), fontSize: 13));
    }
  }

  /// 获取标题头
  String _getHeaderIndex(String gameType, int index) {
    switch (gameType) {
      case 'gdkl10':
      case 'xync':
      case 'xyft':
      case 'pk10':
      case 'pk10nn':
      case 'jsk3':
      case 'gd11x5':
        return (controller.defaultNumber == 10 ? index + 2 : index)
            .toString()
            .padLeft(2, '0');
      case 'pcdd':
      case 'cqssc':
      case 'qxc':
        return (index - 1).toString().padLeft(2, '0');
      case 'lhc':
        return index.toString().padLeft(2, '0');
    }
    return '';
  }

  /// 获取背景色
  Color _getBackgroundCoolor(int index, dynamic item) {
    if (index == 0) {
      return '#C1ADAD'.color();
    } else {
      if (item is String) {
        if (item.contains('red')) {
          return '#FF2600'.color();
        } else {
          return '#1993D6'.color();
        }
      } else {
        return '#D4D4ED'.color();
      }
    }
  }

  /// 获取文本
  String _getText(int index, dynamic item) {
    if (index != 0) {
      if (item is String) {
        if (item.contains('red')) {
          return item.replaceAll('red', '').toString().padLeft(2, '0');
        } else {
          return item.toString().padLeft(2, '0');
        }
      } else {
        return item.toString();
      }
    }

    return item;
  }

  double _getTtemWidth() {
    if (controller.trendData.value.data?[0] == null) {
      return 46;
    }

    final length = controller.trendData.value.data![0].length - 1;

    if (length <= 6) {
      final screenWidth = Get.context!.mediaQuerySize.width;
      return (screenWidth - 120) / length;
    }

    return 28;
  }

  /// 底部工具栏
  Widget _getTrendTool() {
    return Container(
      height: 44,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4), // 阴影颜色
            spreadRadius: 1, // 阴影扩散程度
            blurRadius: 1, // 阴影模糊程度
            offset: const Offset(0, -2), // 阴影偏移量
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            color: '#D7213A'.color(),
            width: 148,
            height: 44,
            alignment: Alignment.center,
            child: Obx(() => Text(
                  controller.gameName.value,
                  style: TextStyle(color: '#FFFFFF'.color(), fontSize: 14),
                )),
          ).onTap(() {
            showGameList();
          }),
          const Spacer(),
          Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.0),
                color: '#E74D39'.color(), // 输入框背景颜色
              ),
              child: Icon(
                Icons.refresh,
                color: Colors.white,
              )).onTap(() async {
            await controller.refreshData();
            ToastUtils.show('刷新成功');
          }),
          const SizedBox(width: 5),
          Container(
              width: 80,
              height: 30,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.0),
                color: '#E77D21'.color(), // 输入框背景颜色
              ),
              child: Text(
                '选择彩种',
                style: TextStyle(color: Colors.white, fontSize: 14),
              )).onTap(() {
            showGameList();
          }),
          const SizedBox(width: 5),
          Container(
            width: 66,
            height: 30,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2.0),
              color: '#E74D39'.color(),
            ),
            child: Text(
              '去下注',
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
          ).onTap(() {
            Get.offAndToNamed(
                '${AppRoutes.lottery}/${controller.gameId.value}');
          }),
          const SizedBox(width: 5),
        ],
      ),
    );
  }

  showGameList() async {
    var result = await Get.to(() => const LotteryGameListPage(),
        opaque: false,
        fullscreenDialog: true,
        transition: Transition.noTransition);

    if (result != null && result is Lottery) {
      controller.handleSelectedLottery(result);
    }
  }
}

/// 划线
class LinePainter extends CustomPainter {
  final List<Map<String, double>> points;

  LinePainter(this.points);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = '#409fdc'.color()
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 1.0;

    for (int i = 1; i < points.length; i++) {
      Offset p1 = Offset(points[i - 1]['x']!, points[i - 1]['y']!);
      Offset p2 = Offset(points[i]['x']!, points[i]['y']!);
      canvas.drawLine(p1, p2, paint);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
