import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryTrendBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["id"];
    Get.put<LotteryTrendController>(LotteryTrendController(), tag: tag);
  }
}
