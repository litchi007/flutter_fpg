import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:get/get.dart';

class GameRuleController extends GetxController {
  static GameRuleController get to => Get.find<GameRuleController>();

  /// 游戏id
  String gameId = '';

  /// 游戏名称
  String gameName = '';

  /// 游戏规则
  final gameRule = ''.obs;

  /// 获取彩票游戏规则
  Future getGameLotteryRule() async {
    final res = await GameHttp.gameLotteryRule(gameId);
    if (res.data != null && res.data!.isNotEmpty) {
      gameRule.value = res.data!;
    }
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null && arguments is Map) {
      gameName = arguments["gameName"];
      gameId = arguments["gameId"];
    }
    super.onInit();
  }

  @override
  void onReady() {
    /// 获取彩票游戏规则
    getGameLotteryRule();
    super.onReady();
  }
}
