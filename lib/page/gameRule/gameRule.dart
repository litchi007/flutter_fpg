import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/page/gameRule/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GameRulePage extends GetView<GameRuleController> {
  /// 游戏
  final Lottery lottery;

  const GameRulePage(this.lottery, {super.key});

  @override
  Widget build(BuildContext context) {
    // 初始化
    Get.put(GameRuleController());
    controller.gameId = lottery.id ?? '';
    controller.gameName = lottery.shortName ?? '';
    return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.1), // 设置背景透明
        body: SafeArea(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 18),
                child: Container(
                  width: MediaQuery.of(context).size.width - 36,
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  decoration: BoxDecoration(
                    color: context.customTheme?.lotteryPopupItemBg,
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      Text(
                        controller.gameName,
                        style: TextStyle(
                            color: context.customTheme?.fontColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(height: 10),
                      Container(
                        height: 1,
                        color: context.customTheme?.lotteryPopupDivColor,
                      ),
                      Expanded(
                          child: SingleChildScrollView(
                              padding: const EdgeInsets.all(16.0),
                              child: Obx(() =>
                                  HtmlWidget(controller.gameRule.value)))),
                      const SizedBox(height: 10),
                      Row(
                        children: [
                          const SizedBox(width: 10),
                          Expanded(
                              child: Container(
                            height: 42,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              // 设置渐变色背景
                              border: Border.all(
                                color: const Color(0xffa9a9a9), // 边框颜色
                                width: 1.0, // 边框宽度
                              ),
                              borderRadius: BorderRadius.circular(4), // 设置圆角
                            ),
                            child: Text(
                              '取消',
                              style: TextStyle(
                                  color: context.customTheme?.fontColor,
                                  fontSize: 16),
                            ),
                          ).onTap(() => Get.back())),
                          const SizedBox(width: 5),
                          Expanded(
                            child: Container(
                              height: 42,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    context.customTheme
                                            ?.ruleLinearGradientColor1 ??
                                        Colors.white,
                                    context.customTheme
                                            ?.ruleLinearGradientColor2 ??
                                        Colors.black
                                  ],
                                ), // 设置渐变色背景
                                borderRadius: BorderRadius.circular(4), // 设置圆角
                              ),
                              child: const Text(
                                '确定',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ).onTap(() => Get.back()),
                          ),
                          const SizedBox(width: 10),
                        ],
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                ))));
  }
}
