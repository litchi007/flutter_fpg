import 'dart:async';
import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/api/ticket/ticket.http.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_model.dart';
import 'package:fpg_flutter/models/lotteryLiveBet/lottery_live_bet_model.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery_group_games_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/get.dart';

class LotteryLiveBetController extends GetxController {
  static LotteryLiveBetController get to =>
      Get.find<LotteryLiveBetController>();

  /// 页码
  int page = 1;
  var data = [].cast<LotteryGroupGamesModel>();
  var betList = [].cast<LotteryLiveBetModel>().obs;

  RefreshController refreshController = RefreshController();

  final loading = true.obs;

  Future onRefresh() async {
    // await getGameLotteryGroupGames(true);
    await getTicketHistory(true);

    refreshController.refreshCompleted();
  }

  void requestRefresh() {
    loading.value = true;
    onRefresh();
  }

  /// 获取列表
  Future getGameLotteryGroupGames(bool isRefresh) async {
    final res = await GameHttp.gameLotteryGroupGames();
    if (res.models?.isNotEmpty ?? false) {
      getTicketHistory(isRefresh);
      // LogUtil.w(res);
      // data = res.models!;
    }
  }

  List<LotteryLiveBetModel> handleHistoryData(List<LotteryHistoryModel> data) {
    Map<String, LotteryLiveBetModel> obj = {};
    for (var historyModel in data) {
      String gameId = historyModel.gameId!;
      if (obj.containsKey(gameId)) {
        LotteryLiveBetModel item = obj[gameId]!;
        obj[gameId] = LotteryLiveBetModel(
          gameId: gameId,
          gameName: historyModel.gameName ?? '',
          betCount: item.betCount +
              (int.tryParse(historyModel.betCount.toString()) ?? 0),
          betAmount:
              item.betAmount + double.parse(historyModel.betAmount ?? '0.00'),
        );
      } else {
        obj[gameId] = LotteryLiveBetModel(
            gameId: gameId,
            gameName: historyModel.gameName ?? '',
            betCount: int.tryParse(historyModel.betCount.toString()) ?? 0,
            betAmount: double.parse(historyModel.betAmount ?? '0.00'));
      }
    }

    return obj.values.toList();
  }

  /// 获取及时注单数和投注金额
  Future getTicketHistory(bool isRefresh) async {
    if (isRefresh) {
      page = 1;
    } else {
      page++;
    }

    final res = await TicketHttp.ticketHistory(
      page: page,
      status: 1,
    );

    var resultList = res.data?.list ?? [];
    if (resultList.isNotEmpty) {
      betList.value = handleHistoryData(resultList);
    }

    loading.value = false;
  }

  @override
  void onReady() {
    /// 获取注单统计
    // getGameLotteryGroupGames(true);
    getTicketHistory(true);

    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
