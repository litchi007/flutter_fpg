import 'dart:math';

import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lotteryLiveBet/lottery_live_bet_model.dart';
import 'package:fpg_flutter/page/lotteryLiveBet/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LotteryLiveBetPage extends GetView<LotteryLiveBetController> {
  const LotteryLiveBetPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: "即时注单"),
        body: SafeArea(
            child: Column(
          children: [
            _buildHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context))
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35,
            child: Text("彩种", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("注单笔数", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("下注金额", style: context.textTheme.bodySmall),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(() => CustomLoading(
          status: controller.loading.value,
          child: SmartRefresher(
            header: CustomHeader(
              builder: (BuildContext context, RefreshStatus? mode) {
                Widget body;
                if (mode == RefreshStatus.refreshing) {
                  body = Text("刷新中..."); // "Refreshing..."
                } else if (mode == RefreshStatus.canRefresh) {
                  body = Text("释放刷新"); // "Release to refresh"
                } else if (mode == RefreshStatus.completed) {
                  body = Text("刷新完成"); // "Refresh complete"
                } else {
                  body = Text("刷新失败"); // "Refresh failed"
                }
                return Container(
                  height: 55.0,
                  child: Center(child: body),
                );
              },
            ),
            controller: controller.refreshController,
            onRefresh: controller.onRefresh,
            footer: CustomFooter(
              builder: (BuildContext context, LoadStatus? mode) {
                Widget body;
                if (mode == LoadStatus.loading) {
                  body = Text("加载中..."); // Change 'Loading' to Chinese here
                } else if (mode == LoadStatus.failed) {
                  body = Text("加载失败，请重试");
                } else if (mode == LoadStatus.canLoading) {
                  body = Text("释放立即加载更多");
                } else {
                  body = Text("没有更多数据");
                }
                return Container(
                  height: 55.0,
                  child: Center(child: body),
                );
              },
            ),
            child: ListView.builder(
              itemBuilder: (c, i) =>
                  _buildBodyItemWidget(context, controller.betList[i]),
              itemCount: controller.betList.length,
            ),
          ),
        ));
  }

  Widget _buildBodyItemWidget(BuildContext context, LotteryLiveBetModel model) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 35,
                child: Text(model.gameName, style: context.textTheme.bodySmall),
              ),
            ),
            Container(
              width: 0.5,
              height: 35,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    alignment: Alignment.center,
                    height: 35, // 设置高度作为示例
                    child: Text(
                      '${model.betCount}',
                      style: TextStyle(
                        fontSize: 12,
                        decoration: TextDecoration.underline,
                        decorationColor:
                            context.customTheme?.fontColor, // 可选：指定下划线颜色
                        decorationThickness: 2, // 可选：指定下划线粗细
                        decorationStyle:
                            TextDecorationStyle.solid, // 可选：指定下划线样式,
                      ),
                    ))),
            Container(
              width: 0.5,
              height: 35,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 35, // 设置高度作为示例
                child: Text(model.betAmount.toStringAsFixed(2),
                    style: context.textTheme.bodySmall),
              ),
            ),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    ).onTap(() {
      Get.toNamed(AppRoutes.waitingPrize,
          arguments: {"gameName": model.gameName, "gameId": model.gameId});
    });
  }
}
