import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryLiveBetBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<LotteryLiveBetController>(() => LotteryLiveBetController());
  }
}
