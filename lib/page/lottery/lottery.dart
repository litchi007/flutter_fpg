import 'dart:math';
import 'package:dartx/dartx.dart';
import 'package:fpg_flutter/api/chat/chat.http.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.cell.dart';
import 'package:fpg_flutter/components/custom.chat.dart';
import 'package:fpg_flutter/components/custom.chat.list.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/components/custom.webview.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/constants/assets.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/page/lottery/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/page/lotteryPopup/lotteryPopup.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class LotteryPage extends GetView<LotteryController> {
  LotteryPage({super.key});
  final tags = Get.parameters["id"];
  @override
  String? get tag => tags;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
          resizeToAvoidBottomInset: controller.selectTab.value == 0,
          key: controller.scffoldKey.value,
          drawer: Drawer(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  color: context.customTheme?.lotteryAppBar,
                  // color: '#BFA36D'.color(),
                  padding: const EdgeInsets.only(bottom: 10),
                  child: SafeArea(
                    bottom: false,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "返回首页",
                          style: context.textTheme.titleMedium?.copyWith(),
                        ),
                      ],
                    ),
                  ),
                ).onTap(controller.handleBackHome),
                Expanded(
                  child: Obx(
                    () => ListView.builder(
                        padding: EdgeInsets.zero,
                        itemCount: controller.data.length,
                        itemBuilder: (context, index) {
                          final item = controller.data[index];
                          return Column(
                            children: [
                              Obx(
                                () => CustomCell(
                                  label: item.name,
                                  open: index == controller.openIndex.value,
                                ).onTap(
                                    () => controller.handleOpen(index, item)),
                              ),
                              // 子类
                              AnimatedSize(
                                duration: 300.milliseconds,
                                alignment: Alignment.topCenter,
                                child: Obx(
                                  () => SizedBox(
                                    height: index != controller.openIndex.value
                                        ? 0
                                        : null,
                                    child: GridView(
                                      padding: EdgeInsets.zero,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              childAspectRatio: 3.2),
                                      children: (item.lotteries ?? [])
                                          .mapIndexed((i, e) {
                                        return Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              color: controller
                                                          .secondActive.value ==
                                                      e.id
                                                  ? context.customTheme
                                                      ?.lotteryAppBar
                                                  : null,
                                              border: Border(
                                                bottom: BorderSide(
                                                    width: 1,
                                                    color: context
                                                        .theme.cardColor
                                                        .withOpacity(.2)),
                                                right: BorderSide(
                                                    width: 1,
                                                    color: context
                                                        .theme.cardColor
                                                        .withOpacity(.2)),
                                              )),
                                          child: Text("${e.title}",
                                              style: context
                                                  .textTheme.bodyMedium
                                                  ?.copyWith(
                                                      color: controller
                                                                  .secondActive
                                                                  .value ==
                                                              e.id
                                                          ? context.customTheme
                                                              ?.reverseFontColor
                                                          : null)),
                                        ).onTap(() => controller
                                            .handleSecondActive(e.id ?? "", e));
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          );
                        }),
                  ),
                ),
              ],
            ),
          ),
          appBar: CustomAppBar(
            // backCallback: () {
            //   // Get.offNamedUntil(homePath, (route) => route.isFirst);
            //   Get.back();
            // },
            background: context.customTheme?.lotteryAppBar,
            iconColor: context.customTheme?.reverseFontColor,
            leading: Icon(
              Icons.swap_horiz,
              color:
                  context.customTheme?.reverseFontColor ?? Colors.transparent,
            ).onTap(controller.openDraw),
            customChild: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Obx(() => Text(
                          controller.select.value.title ?? "",
                          style: context.textTheme.titleMedium?.copyWith(
                              color: context.customTheme?.reverseFontColor),
                        )),
                    Icon(
                      Icons.arrow_drop_down,
                      color: context.customTheme?.reverseFontColor,
                    )
                  ],
                ).onTap(controller.openDraw),
                Row(
                  children: [
                    Obx(
                      () => Text(
                        controller.userBalanceData.value.balance ?? "0",
                        style: context.textTheme.titleMedium?.copyWith(
                            color: context.customTheme?.reverseFontColor),
                      ),
                    ),
                    AnimatedBuilder(
                        animation: controller.animation,
                        builder: (context, child) {
                          return Transform.rotate(
                              angle: pi * controller.animation.value,
                              child: Container(
                                  width: 30,
                                  height: 30,
                                  child: SizedBox(
                                    width: 20,
                                    height: 20,
                                    child: Stack(
                                      children: [
                                        Positioned(
                                            left: -28.2,
                                            width: 145,
                                            height: 30,
                                            child: Image.asset(
                                                Assets.assetsImagesPublicIcon))
                                      ],
                                    ),
                                  ))).paddingOnly(left: 10).onTap(
                              controller.handleRefresh);
                        }),
                    Icon(Icons.menu_outlined,
                            color: context.customTheme?.reverseFontColor)
                        .paddingOnly(left: 10, right: 10)
                        .onTap(() {
                      Get.to(() => LotteryPopupPage(controller.select.value),
                          opaque: false,
                          fullscreenDialog: true,
                          transition: Transition.noTransition);
                    })
                  ],
                ),
              ],
            ),
          ),
          body: Obx(
            () => CustomLoading(
                status: controller.loading.value,
                child: Container(
                  // color: Colors.red,
                  // decoration: BoxDecoration(
                  //   gradient: LinearGradient(
                  //     begin: Alignment.topCenter,
                  //     end: Alignment.bottomCenter,
                  //     colors: [
                  //       context.customTheme?.lotteryBgStart ?? Colors.transparent,
                  //       context.customTheme?.lotteryBgEnd ?? Colors.transparent
                  //     ]
                  //   )
                  // ),
                  child: Column(
                    children: [
                      Container(
                        color: context.customTheme?.lotteryMenuBg,
                        child: Column(
                          children: [
                            TabBar(
                              isScrollable: false,
                              labelPadding: EdgeInsets.zero,
                              indicatorColor: Colors.transparent,
                              controller: controller.tabController,
                              dividerColor: Colors.transparent,
                              indicator: BoxDecoration(
                                  color: context.customTheme?.leftActive,
                                  borderRadius: 4.radius),
                              unselectedLabelColor:
                                  context.textTheme.bodyLarge?.color,
                              labelStyle: context.textTheme.bodyLarge?.copyWith(
                                  fontWeight: FontWeight.bold,
                                  color: context.customTheme?.reverseFontColor),
                              onTap: (value) {
                                if (controller.selectTab.value == 1 &&
                                    value == 1) {
                                  controller.handleShowRoom();
                                }
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                              },
                              tabs: controller.tabs
                                  .mapIndexed(
                                    (i, e) => Container(
                                      // decoration: BoxDecoration(
                                      //     color: controller.selectTab.value == i
                                      //         ? context.customTheme?.leftActive
                                      //         : null),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10),
                                      alignment: Alignment.center,
                                      child: Text(
                                        e,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  )
                                  .toList(),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                            controller: controller.tabController,
                            children: [
                              BettingArea(
                                controller: controller,
                              ),
                              ChatArea(
                                roomId:
                                    controller.selectRoom.value.roomId ?? "",
                                data: controller.selectRoom.value,
                              )
                            ]),
                      )
                    ],
                  ),
                )),
          )).onTap(() {
        FocusScope.of(context).requestFocus(FocusNode());
      }),
    );
  }
}

class BettingArea extends StatefulWidget {
  final LotteryController controller;
  const BettingArea({super.key, required this.controller});

  @override
  State<BettingArea> createState() => _BettingAreaState();
}

class _BettingAreaState extends State<BettingArea>
    with AutomaticKeepAliveClientMixin {
  late final controller = widget.controller;

  Widget _buildNumWidget(BuildContext context, String result) {
    final numResult = result.split(",");
    // 是否是六合彩
    final isLhc = controller.select.value.gameType?.contains("lhc") ?? false;
    // 江苏快三
    final isJsk3 = controller.select.value.gameType == "jsk3";
    // 大乐透
    final isDlt = controller.select.value.gameType == "dlt";
    final start = isLhc ? numResult.sublist(0, 6) : numResult;
    final end = isLhc ? numResult.sublist(6) : [];
    // 是否是时时彩
    final isSsc = controller.selectSub.value.name == "时时彩" ||
        ['fc3d'].contains(controller.select.value.gameType);

    /// 开奖号码
    return Container(
      // color: Colors.blue,
      // height: 35,
      child: Wrap(
        spacing: 2,
        runSpacing: 5,
        children: [
          ...start.mapIndexed((index, value) {
            return Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  // margin: const EdgeInsets.only(right: 2),
                  width: 27,
                  height: 27,
                  decoration: BoxDecoration(
                      image: isSsc
                          ? null
                          : isLhc
                              ? DecorationImage(
                                  image: LotteryConfig.getBallBg(
                                      Play(name: value),
                                      controller.playOdds.value))
                              : isJsk3
                                  ? DecorationImage(
                                      image: AssetImage(
                                          LotteryConfig.getQuick3Img(value)))
                                  : isDlt
                                      ? DecorationImage(
                                          image: AssetImage(
                                              LotteryConfig.dltBalls(index)))
                                      : null,
                      color: (!isLhc && !isJsk3 && !isDlt)
                          ? isSsc
                              ? context.customTheme?.leftActive
                              : [
                                  'bjkl8'
                                ].contains(controller.select.value.gameType)
                                  ? context.customTheme?.error
                                  : controller.ballColor[int.tryParse(value)]
                          : null,
                      borderRadius: [isLhc, isJsk3, isDlt].contains(true)
                          ? null
                          : 12.radius),
                  child: Text("${int.tryParse(value)}",
                      style: context.textTheme.bodyMedium?.copyWith(
                          fontSize: 10,
                          // color: isLhc ? null : null
                          color: isLhc
                              ? null
                              : context.customTheme?.reverseFontColor)),
                ),
              ],
            );
          }),
          end.isNotEmpty
              ? Container(
                  alignment: Alignment.center,
                  width: 20,
                  height: 20,
                  child: const Text("+"))
              : const SizedBox(),
          ...end.map((value) {
            return Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(right: 2),
                  width: 27,
                  height: 27,
                  decoration: BoxDecoration(
                      image: isLhc
                          ? DecorationImage(
                              image: LotteryConfig.getBallBg(
                                  Play(name: value), controller.playOdds.value))
                          : null,
                      color: (!isLhc && !isJsk3 && !isDlt)
                          ? controller.ballColor[int.tryParse(value)]
                          : null,
                      borderRadius: 12.radius),
                  child: Text("${int.tryParse(value)}",
                      style: context.textTheme.bodyMedium?.copyWith(
                          fontSize: 10,
                          color: isLhc
                              ? null
                              : context.customTheme?.reverseFontColor)),
                ),
              ],
            );
          })
        ],
      ),
    );
  }

  Widget _buildTextWidget(BuildContext context, String result) {
    final numResult = result.split(",");
    // 是否是六合彩
    final isLhc = controller.select.value.gameType?.contains("lhc") ?? false;
    final start = isLhc ? numResult.sublist(0, 6) : numResult;
    final end = isLhc ? numResult.sublist(6) : [];
    return Container(
      // color: Colors.red,
      height: 30,
      margin: EdgeInsets.only(left: 5),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          ...start.map((value) {
            return ConstrainedBox(
              constraints: const BoxConstraints(
                  minWidth: 20, maxWidth: 29, maxHeight: 30, minHeight: 30),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 1,
                        color: context.textTheme.bodyMedium?.color ??
                            Colors.transparent),
                    borderRadius: 5.radius,
                    color: context.customTheme?.blockBg?.withOpacity(.1)),
                margin: const EdgeInsets.only(right: 8),
                // padding: const EdgeInsets.symmetric(horizontal: 4),
                width: 30,
                height: 30,
                child: Text(
                  value,
                  softWrap: false,
                  style: context.textTheme.bodySmall?.copyWith(fontSize: 10),
                ),
              ),
            );
          }),
          end.isNotEmpty
              ? Container(
                  alignment: Alignment.center,
                  width: 16,
                  height: 16,
                  child: const Text("+"))
              : const SizedBox(),
          ...end.map((value) {
            return ConstrainedBox(
              constraints: const BoxConstraints(minWidth: 20),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(
                        width: 1,
                        color: context.textTheme.bodyMedium?.color ??
                            Colors.transparent),
                    borderRadius: 5.radius),
                margin: const EdgeInsets.only(right: 2),
                padding: const EdgeInsets.symmetric(horizontal: 4),
                // width: 25,
                height: 20,
                child: Text(
                  value,
                  style: context.textTheme.bodySmall?.copyWith(
                      fontSize: 10, color: context.textTheme.bodyMedium?.color),
                ),
              ),
            );
          }),
        ]).paddingOnly(top: 10),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Obx(
      () => Stack(
        children: [
          Column(
            children: [
              Container(
                color: context.customTheme?.lotteryMenuBg,
                child: Column(
                  children: [
                    AnimatedSize(
                      duration: 500.milliseconds,
                      child: SizedBox(
                        height: controller.videoStatus.value ? 200 : 0,
                        child: Stack(
                          children: [
                            Obx(
                              () => controller.videoStatus.value
                                  ? CustomWebview(
                                      webUri: controller.videoUrl.value,
                                      onProgress: controller.handleProgress,
                                    )
                                  : const SizedBox(),
                            ),
                            Container(
                              width: double.infinity,
                              // color: Colors.red,
                              alignment: Alignment.centerLeft,
                              height: 5,
                              child: UnconstrainedBox(
                                child: AnimatedSize(
                                  duration: 50.milliseconds,
                                  child: Obx(
                                    () => Container(
                                        decoration: BoxDecoration(
                                            color:
                                                context.customTheme?.leftActive,
                                            borderRadius: 3.radius),
                                        height: 5,
                                        width: context.mediaQuerySize.width *
                                            0.01 *
                                            controller.progress.value),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                                right: 10,
                                top: 10,
                                child: Container(
                                  padding: const EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      borderRadius: 20.radius,
                                      color: context.customTheme?.error
                                          ?.withOpacity(.5)),
                                  child: const Icon(Icons.close_outlined)
                                      .onTap(controller.handleCloseVideo),
                                ))
                          ],
                        ),
                      ),
                    ),

                    /// 开奖信息
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Container(
                                    // color: Colors.red,
                                    alignment: Alignment.center,
                                    height: 30,
                                    child: Text(
                                        "${controller.getNextIsu.value.preIssue ?? ""}期",
                                        style: context.textTheme.bodyMedium
                                            ?.copyWith(fontSize: 12)),
                                  ),

                                  /// 刷新
                                  SizedBox(
                                    height: 40,
                                    child: Row(
                                      children: [
                                        const Text("刷新"),
                                        AnimatedBuilder(
                                            animation: controller.isuAnimation,
                                            builder: (context, child) {
                                              return Container(
                                                margin: const EdgeInsets.only(
                                                    left: 10),
                                                child: Transform.rotate(
                                                    origin: Offset.zero,
                                                    angle: pi *
                                                        controller
                                                            .isuAnimation.value,
                                                    child: SizedBox(
                                                        // color: Colors.red,
                                                        width: 30,
                                                        height: 30,
                                                        child: Stack(
                                                          children: [
                                                            Positioned(
                                                                top: 0,
                                                                left: -55,
                                                                width: 140,
                                                                height: 30,
                                                                child: Image
                                                                    .asset(Assets
                                                                        .assetsImagesPublicIcon))
                                                          ],
                                                        ))),
                                              );
                                              // const Icon(
                                              //   Icons.refresh,
                                              //   size: 18,
                                              // )).paddingOnly(left: 10);
                                            }),
                                      ],
                                    )
                                        .paddingOnly(top: 10)
                                        .onTap(controller.handleIsuRefresh),
                                  ),
                                ],
                              ),

                              /// 上期开奖结果
                              Expanded(
                                child: Builder(builder: (context) {
                                  return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        /// 开奖号码
                                        _buildNumWidget(
                                            context,
                                            (controller
                                                    .getNextIsu.value.preNum ??
                                                "")),
                                        // 开奖文字隐藏探讨
                                        ['bjkl8'].contains(controller
                                                .select.value.gameType)
                                            ? const SizedBox()
                                            : _buildTextWidget(
                                                context,
                                                (controller.getNextIsu.value
                                                        .preResult ??
                                                    ""))
                                      ]).paddingOnly(left: 5);
                                }),
                              ),
                            ],
                          ),
                        ),

                        /// 龙和展示近期开奖结果
                        Column(
                          children: [
                            Container(
                              width: 20,
                              height: 20,
                              alignment: Alignment.topCenter,
                              child: const Stack(
                                alignment: Alignment.center,
                                children: [
                                  Positioned(
                                      top: -3,
                                      child: Icon(Icons.arrow_drop_up)),
                                  Positioned(
                                      top: 3,
                                      child: Icon(Icons.arrow_drop_down))
                                ],
                              ),
                            ).onTap(controller.handleHistory),
                            // 长龙-路珠
                            Visibility(
                                visible: !(controller.select.value.gameType
                                        ?.contains("lhc") ??
                                    false),
                                child: Container(
                                  margin: const EdgeInsets.only(top: 18),
                                  width: 30,
                                  height: 30,
                                  child: Stack(
                                    children: [
                                      Positioned(
                                          left: 10,
                                          width: 100,
                                          height: 40,
                                          child: Image.asset(
                                              Assets.assetsImagesPublicIcon))
                                    ],
                                  ),
                                )
                                // const Text("龙").paddingOnly(top: 10)
                                ).onTap(controller.showLong)
                          ],
                        )
                      ],
                    ).paddingOnly(top: 10, left: 10, right: 10),
                    const Divider(),
                    // 哈希验证
                    (controller.getNextIsu.value.hash ?? "").isNotEmpty
                        ? Column(
                            children: [
                              Row(
                                children: [
                                  Text("哈希验证:",
                                          style: context.textTheme.bodyMedium
                                              ?.copyWith(
                                                  color: context
                                                      .customTheme?.lottery6))
                                      .onTap(controller.handleOpenHashWebview),
                                  Text("...${(controller.getNextIsu.value.hash ?? "").slice((controller.getNextIsu.value.hash ?? '').length > 16 ? (controller.getNextIsu.value.hash ?? '').length - 16 : 0)}")
                                      .paddingOnly(left: 10),
                                  Container(
                                    margin: const EdgeInsets.only(left: 10),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 1,
                                            color: context.textTheme.bodyLarge
                                                    ?.color ??
                                                Colors.transparent),
                                        borderRadius: 5.radius),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 4),
                                    child: Text("彩种介绍")
                                        .onTap(controller.openHashDesc),
                                  )
                                ],
                              ).paddingSymmetric(horizontal: 10),
                              const Divider(),
                            ],
                          )
                        : const SizedBox(),

                    /// 历史开奖记录
                    AnimatedSize(
                      duration: const Duration(milliseconds: 200),
                      alignment: Alignment.topCenter,
                      curve: Curves.easeIn,
                      child: SizedBox(
                        height: !controller.historyStatus.value ? 0 : 300,
                        width: double.infinity,
                        child: (controller.history.value.list?.isNotEmpty ??
                                false)
                            ? ListView.builder(
                                itemCount:
                                    ((controller.history.value.list ?? [])
                                            .sublist(1))
                                        .length,
                                itemBuilder: (context, index) {
                                  final item =
                                      controller.history.value.list?[index + 1];
                                  return Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text("${item?.issue}期",
                                              style: context
                                                  .textTheme.bodyMedium
                                                  ?.copyWith(fontSize: 12)),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                /// 开奖号码
                                                _buildNumWidget(
                                                    context, (item?.num ?? "")),

                                                /// 开奖文字
                                                // SizedBox(
                                                //     width: 300,
                                                //     child: _buildTextWidget(context,
                                                //         item?.result ?? ''))
                                              ],
                                            ).paddingOnly(left: 10),
                                          ),
                                        ],
                                      ),
                                      const Divider()
                                    ],
                                  ).paddingOnly(top: 10, left: 10, right: 10);
                                })
                            : const SizedBox(),
                      ),
                    ),
                    // 当前期数
                    controller.getNextIsu.value.curIssue != null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Text(
                                      "${controller.getNextIsu.value.curIssue ?? ""}期",
                                      style: context.textTheme.bodySmall
                                          ?.copyWith(
                                              fontSize: 14,
                                              color: context.textTheme
                                                  .bodyMedium?.color)),

                                  /// 封盘
                                  Text.rich(TextSpan(
                                      text: "封盘：",
                                      style: context.textTheme.bodySmall
                                          ?.copyWith(
                                              fontSize: 14,
                                              color: context
                                                  .textTheme.bodyMedium?.color),
                                      children: [
                                        WidgetSpan(
                                            child: Container(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          // decoration: BoxDecoration(
                                          //   borderRadius: 5.radius,
                                          //   color: context.customTheme?.blockBg
                                          //       ?.withOpacity(.2),
                                          // ),
                                          // height: 20,
                                          alignment: Alignment.center,
                                          // width: 20,
                                          child: Text(
                                              controller.closeingTimeNumber.value <=
                                                      0
                                                  ? '已封盘'
                                                  : controller
                                                      .closeingTimeNumber.value
                                                      .toHMS(),
                                              style: context
                                                  .textTheme.bodyMedium
                                                  ?.copyWith(
                                                      color: context
                                                          .customTheme?.error,
                                                      fontSize: 14)),
                                        ))
                                      ])).paddingOnly(
                                    left: 10,
                                  ),

                                  /// 开奖
                                  Text.rich(TextSpan(
                                      text: "开奖：",
                                      style: context.textTheme.bodySmall
                                          ?.copyWith(
                                              fontSize: 14,
                                              color: context
                                                  .textTheme.bodyMedium?.color),
                                      children: [
                                        WidgetSpan(
                                            child: Container(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          // decoration: BoxDecoration(
                                          //   borderRadius: 5.radius,
                                          //   color: context.customTheme?.blockBg
                                          //       ?.withOpacity(.2),
                                          // ),
                                          // height: 20,

                                          alignment: Alignment.center,
                                          // width: 20,
                                          child: Text(
                                              controller.openTimeNumber.value
                                                  .toHMS(),
                                              style: context
                                                  .textTheme.bodyMedium
                                                  ?.copyWith(
                                                      color: context
                                                          .customTheme?.error,
                                                      fontSize: 14)),
                                        ))
                                      ])).paddingOnly(left: 10),
                                ],
                              ),
                              Row(
                                children: [
                                  // Text( controller.select.value.gameType ?? ""),
                                  // 右侧打开视频
                                  // !(controller.select.value.gameType?.contains("lhc") ?? false)
                                  LotteryConfig.showVideo(
                                          id: controller.getNextIsu.value.id ??
                                              "",
                                          gameType: controller
                                                  .getNextIsu.value.gameType ??
                                              "")
                                      ? SizedBox(
                                          child: SizedBox(
                                              width: 20,
                                              height: 20,
                                              child: Stack(
                                                children: [
                                                  Positioned(
                                                      left: -78,
                                                      width: 97,
                                                      height: 20,
                                                      child: Image.asset(Assets
                                                          .assetsImagesPublicIcon))
                                                ],
                                              )).onTap(controller.handleVideo))
                                      : const SizedBox(),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: controller.select.value.gameType !=
                                            "lhc"
                                        ? SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: Stack(
                                              children: [
                                                Positioned(
                                                    left: -58,
                                                    width: 97,
                                                    height: 20,
                                                    child: Image.asset(Assets
                                                        .assetsImagesPublicIcon))
                                              ],
                                            ).onTap(
                                                controller.handleOpenWebview))
                                        : const SizedBox(),
                                  )
                                ],
                              )
                            ],
                          ).paddingOnly(top: 5, left: 10, right: 10, bottom: 5)
                        : const SizedBox(),
                  ],
                ),
              ),

              // 玩法
              Expanded(
                child: CustomLoading(
                  status: controller.oddStatus.value,
                  child: Row(
                    children: [
                      Container(
                          width: context.mediaQuerySize.width * .3,
                          decoration: BoxDecoration(
                              color: context.customTheme?.lotteryMenuBg,
                              border: Border(
                                  right: BorderSide(
                                      width: .5,
                                      color: context.theme.cardColor
                                          .withOpacity(.2)))),
                          child: Scrollbar(
                            controller: controller.scrollController,
                            child: ListView.builder(
                                padding: EdgeInsets.only(bottom: 100),
                                controller: controller.scrollController,
                                itemCount:
                                    controller.playOdds.value.playOdds?.length,
                                itemBuilder: (context, index) {
                                  final item = controller
                                      .playOdds.value.playOdds?[index];
                                  return Container(
                                          alignment: Alignment.center,
                                          margin:
                                              const EdgeInsets.only(bottom: 4),
                                          decoration: BoxDecoration(
                                              color: context.customTheme
                                                  ?.reverseFontColor,
                                              border: Border.all(
                                                width: 1,
                                                color: (controller
                                                                .leftMenuActive
                                                                .value ==
                                                            index
                                                        ? context.customTheme
                                                            ?.leftActive
                                                        : Colors.transparent) ??
                                                    Colors.transparent,
                                              )),
                                          // color:
                                          //     controller.leftMenuActive.value == index
                                          //         ? context.customTheme?.leftActive
                                          //         : null,
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Text(item?.name ?? ""))
                                      .onTap(() => controller
                                          .handleLeftMednuActive(index, item!));
                                }),
                          )),
                      Expanded(
                          child: Container(
                              color: context.customTheme?.reverseFontColor,
                              child: controller.lhcSpecialWidget(
                                  controller.leftMenuActiveDetail.value))),
                    ],
                  ),
                ),
              ),

              // 底部下注部分
              Container(
                width: context.mediaQuerySize.width,
                color: context.customTheme?.lottteryBetBottomBg,
                height: 80 + context.mediaQueryPadding.bottom,
                padding: EdgeInsets.only(
                    // bottom: context.mediaQueryPadding.bottom,
                    // left: 15,
                    // right: 15,
                    ),
                child: Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: context.mediaQueryPadding.bottom,
                          top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 4),
                                decoration: BoxDecoration(
                                    color: controller.chasingNumber.isEmpty
                                        ? context
                                            .customTheme?.chasingNumberDisableBg
                                        : context.customTheme?.robotSelectBg,
                                    borderRadius: 5.radius),
                                child: Text("追号",
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            color: context.customTheme
                                                ?.reverseFontColor)),
                              ).onTap(() => controller.handleSubmit(true)),
                              Container(
                                margin: const EdgeInsets.only(top: 5),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 4),
                                decoration: BoxDecoration(
                                    color: context.customTheme?.robotSelectBg,
                                    borderRadius: 5.radius),
                                child: Text("机选",
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            color: context.customTheme
                                                ?.reverseFontColor)),
                              ).onTap(controller.handleRobotSelect),
                            ],
                          ),

                          // 下注，筹码，输入框
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Obx(
                                        () => Text(
                                          "已选中${[
                                            "DWDZXSFS",
                                            "DWDZXLFS",
                                            "LMA"
                                          ].contains(controller.selectType.value) ? controller.notes : controller.selectPlay.length}注",
                                          style: context.textTheme.bodyMedium
                                              ?.copyWith(
                                                  color: context.customTheme
                                                      ?.reverseFontColor),
                                        ),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 4),
                                        decoration: BoxDecoration(
                                            color: context.customTheme?.chipsBg,
                                            borderRadius: 5.radius),
                                        child: Text("筹码",
                                            style: context.textTheme.bodyMedium
                                                ?.copyWith(
                                                    color: context.customTheme
                                                        ?.reverseFontColor)),
                                      ).onTap(controller.handleOpenChip)
                                    ],
                                  ),
                                  SizedBox(
                                      height: 40,
                                      child: TextField(
                                        controller: controller.textController,
                                        keyboardType: TextInputType.number,
                                        focusNode: controller.focusNode,
                                        style: context.textTheme.bodyMedium
                                            ?.copyWith(
                                                color: context.customTheme
                                                    ?.reverseFontColor),
                                        decoration: InputDecoration(
                                          contentPadding:
                                              const EdgeInsets.only(left: 10),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: context.customTheme
                                                          ?.reverseFontColor ??
                                                      Colors.transparent)),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: context.customTheme
                                                          ?.reverseFontColor ??
                                                      Colors.transparent)),
                                        ),
                                      )),
                                ],
                              ),
                            ),
                          ),

                          Row(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 60,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                    color: context.customTheme?.betBg,
                                    borderRadius: 5.radius),
                                child: Text("下注",
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            color: context.customTheme
                                                ?.reverseFontColor)),
                              ).onTap(controller.handleSubmit),
                              Container(
                                margin: const EdgeInsets.only(left: 10),
                                alignment: Alignment.center,
                                height: 60,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                    color: context.customTheme?.resetBg,
                                    borderRadius: 5.radius),
                                child: Text("重置",
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            color: context.customTheme
                                                ?.reverseFontColor)),
                              ).onTap(controller.handleReset),
                            ],
                          ).paddingOnly(left: 10)
                        ],
                      ),
                    ),
                    Positioned(
                        bottom: 0,
                        child: Obx(
                          () => controller.closeingTimeNumber.value <= 0 ||
                                  controller.getNextIsu.value.isSeal == "1"
                              ? Container(
                                  width: context.mediaQuerySize.width,
                                  height:
                                      100 + context.mediaQueryPadding.bottom,
                                  padding: EdgeInsets.only(top: 10),
                                  color: context.customTheme?.error
                                      ?.withOpacity(.8),
                                  // color: Colors.red,
                                  alignment: Alignment.center,
                                  child: Text('已封盘',
                                      style: context.textTheme.titleMedium
                                          ?.copyWith(
                                              color: context.customTheme
                                                  ?.reverseFontColor,
                                              fontWeight: FontWeight.w700)),
                                )
                              : const SizedBox(),
                        ))
                  ],
                ),
              )
            ],
          ),
          Positioned(
            bottom: context.mediaQueryPadding.bottom + 80,
            width: controller.withdrawalStatus.value
                ? context.mediaQuerySize.width
                : null,
            // height: 100,
            child: SizedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // 筹码分布
                  controller.chipsList.isNotEmpty
                      ? AnimatedOpacity(
                          opacity: controller.chipStatus.value ? 1 : 0,
                          duration: 300.milliseconds,
                          child: SizedBox(
                            width: controller.chipStatus.value
                                ? context.mediaQuerySize.width
                                : 0,
                            height: controller.chipStatus.value ? null : 0,
                            child: Row(children: [
                              // 绘制前4个
                              ...List.generate(
                                  4,
                                  (index) => Expanded(
                                          child: SizedBox(
                                        height: 48,
                                        child: Stack(
                                          children: [
                                            Positioned(
                                                top: double.parse(
                                                    (-51 * index).toString()),
                                                left: 0,
                                                child: Image.asset(
                                                  Assets.assetsImagesChoumaBg,
                                                  fit: BoxFit.fitWidth,
                                                  width: 48,
                                                )),
                                            Container(
                                              alignment: Alignment.center,
                                              child: Text(
                                                "${controller.chipsList[index]}",
                                                style: context
                                                    .textTheme.bodyMedium
                                                    ?.copyWith(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: context
                                                            .customTheme
                                                            ?.reverseFontColor),
                                              ),
                                            )
                                          ],
                                        ),
                                      ).onTap(() => controller
                                              .handleSelectChips(index)))),
                              // 绘制后三个
                              ...List.generate(
                                  3,
                                  (index) => Expanded(
                                          child: SizedBox(
                                        height: 48,
                                        child: Stack(
                                          children: [
                                            Positioned(
                                                top: double.parse(
                                                    (-51 * (index + 5))
                                                        .toString()),
                                                left: 0,
                                                child: Image.asset(
                                                  Assets.assetsImagesChoumaBg,
                                                  fit: BoxFit.fitWidth,
                                                  width: 48,
                                                )),
                                            Container(
                                              alignment: Alignment.center,
                                              child: Text(
                                                controller.chipsList[index + 4]
                                                    .formatNum(),
                                                style: context
                                                    .textTheme.bodyMedium
                                                    ?.copyWith(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: context
                                                            .customTheme
                                                            ?.reverseFontColor),
                                              ),
                                            )
                                          ],
                                        ),
                                      ).onTap(() => controller
                                              .handleSelectChips(index + 4)))),
                              // 清楚
                              Expanded(
                                  child: SizedBox(
                                height: 50,
                                child: Stack(
                                  children: [
                                    Positioned(
                                        top: -51 * 4,
                                        left: 0,
                                        child: Image.asset(
                                          Assets.assetsImagesChoumaBg,
                                          fit: BoxFit.fitWidth,
                                          width: 48,
                                        )),
                                    Container(
                                      alignment: Alignment.center,
                                      // child: Text("清除"),
                                    )
                                  ],
                                ),
                              ).onTap(() => controller.handleSelectChips(-1)))
                            ]),
                          ),
                        )
                      : const SizedBox(),
                  // 退水
                  Container(
                    // width: 30,
                    padding:
                        const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                    color: Colors.black.withOpacity(.8),
                    // color: Colors.red,
                    height: 50,
                    // width: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(controller.withdrawalStatus.value ? "收起" : "展开",
                                style: context.textTheme.bodyMedium?.copyWith(
                                    color: Colors.white.withOpacity(.8)))
                            .onTap(controller.handleWithdrawalStatus),
                        AnimatedSize(
                          duration: 300.milliseconds,
                          child: SizedBox(
                            width: controller.withdrawalStatus.value ? null : 0,
                            height:
                                controller.withdrawalStatus.value ? null : 0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text("退水: ${controller.withdrawalProgress.value.toStringAsFixed(2)}%",
                                        style: context.textTheme.bodyMedium
                                            ?.copyWith(color: Colors.white))
                                    .paddingOnly(right: 4),
                                Container(
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: 40.radius),
                                        child: const Icon(
                                            Icons.horizontal_rule_rounded))
                                    .onTap(
                                        () => controller.handleSliderValue(-1)),
                                Container(
                                  margin:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  height: 30,
                                  // width: 80,
                                  child: SliderTheme(
                                    data: const SliderThemeData(
                                      trackHeight: 2, // 轨道高度
                                      thumbShape: RoundSliderThumbShape(
                                          //  滑块形状，可以自定义
                                          enabledThumbRadius: 10 // 滑块大小
                                          ),
                                      overlayShape: RoundSliderOverlayShape(
                                        // 滑块外圈形状，可以自定义
                                        overlayRadius: 10, // 滑块外圈大小
                                      ),
                                    ),
                                    child: Slider(
                                        min: 0,
                                        max: 10,
                                        // activeColor: Colors.red,
                                        value:
                                            controller.withdrawalProgress.value,
                                        onChanged: (value) {
                                          controller.withdrawalProgress.value =
                                              value;
                                          controller.handleSliderValue(
                                              value, true);
                                        }),
                                  ),
                                ),
                                Container(
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: 40.radius),
                                        child: const Icon(Icons.add))
                                    .onTap(
                                        () => controller.handleSliderValue(1))
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
