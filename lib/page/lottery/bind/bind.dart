import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["id"];
    Get.put<LotteryController>(LotteryController(), tag: tag);
  }
}
