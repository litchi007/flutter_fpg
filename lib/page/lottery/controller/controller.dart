import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fpg_flutter/api/chat/chat.http.dart';
import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/configs/key_config.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/chat_get_token_model/chat_ary.dart';
import 'package:fpg_flutter/models/chat_get_token_model/chat_get_token_model.dart';
import 'package:fpg_flutter/models/chat_get_token_res_model/chat_get_token_res_model.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_history_model.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery_group_games_model.dart';
import 'package:fpg_flutter/models/user_balance_model/user_balance_model.dart';
import 'package:fpg_flutter/page/lottery/components/11_out_of_5_play.dart';
import 'package:fpg_flutter/page/lottery/components/champion_play.dart';
import 'package:fpg_flutter/page/lottery/components/combine_resemble_play.dart';
import 'package:fpg_flutter/page/lottery/components/connect_code_play.dart';
import 'package:fpg_flutter/page/lottery/components/connect_similar_play.dart';
import 'package:fpg_flutter/page/lottery/components/direct_election_play.dart';
import 'package:fpg_flutter/page/lottery/components/five_element_play.dart';
import 'package:fpg_flutter/page/lottery/components/flat_special_1_similar.dart';
import 'package:fpg_flutter/page/lottery/components/gd11x5_position_play.dart';
import 'package:fpg_flutter/page/lottery/components/just_special_play.dart';
import 'package:fpg_flutter/page/lottery/components/long_component.dart';
import 'package:fpg_flutter/page/lottery/components/office_play.dart';
import 'package:fpg_flutter/page/lottery/components/optional_play.dart';
import 'package:fpg_flutter/page/lottery/components/out_of_choice_play.dart';
import 'package:fpg_flutter/page/lottery/components/quick_3/quick_3_2_same_num_play.dart';
import 'package:fpg_flutter/page/lottery/components/quick_3/quick_3_dice_play.dart';
import 'package:fpg_flutter/page/lottery/components/quick_3/quick_3_three_army_play.dart';
import 'package:fpg_flutter/page/lottery/components/second_face_play.dart';
import 'package:fpg_flutter/page/lottery/components/special_code_play.dart';
import 'package:fpg_flutter/page/lottery/components/taste_play.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/time_lottery_five_element_play.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_position_gallbladder.dart';
import 'package:fpg_flutter/page/lottery/components/title_foot_play.dart';
import 'package:fpg_flutter/page/lottery/components/welfare_3d/second_font_play.dart';
import 'package:fpg_flutter/page/lottery/components/welfare_3d/welfare_3d_dwd_play.dart';
import 'package:fpg_flutter/page/webview/controller/controller.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

class LotteryController extends SuperController
    with GetTickerProviderStateMixin {
  final data = [].cast<LotteryGroupGamesModel>().obs;
  final scffoldKey = GlobalKey<ScaffoldState>().obs;
  late final refreshController = AnimationController(
    duration: const Duration(seconds: 10),
    vsync: this,
  );
  late final isuRefreshController = AnimationController(
    duration: const Duration(seconds: 10),
    vsync: this,
  );
  late final animation = Tween(begin: 0.0, end: 10).animate(refreshController);
  late final isuAnimation =
      Tween(begin: 0.0, end: 10).animate(isuRefreshController);
  final loading = true.obs;
  final openIndex = 0.obs;
  final selectSub = (const LotteryGroupGamesModel()).obs;
  final secondActive = "".obs;
  final refreshStatus = false.obs;
  final getNextIsu = (const GameNextIssueModel()).obs;
  Timer? closingTimer;
  final closeingTimeNumber = (0).obs;
  final openTimeNumber = 0.obs;
  final scrollController = ScrollController();
  final focusNode = FocusNode();

  /// 用户余额信息
  final userBalanceData = (const UserBalanceModel()).obs;

  /// 退水显示
  final withdrawalStatus = true.obs;

  /// 退水进度
  final withdrawalProgress = (0.0).obs;

  /// 退水初始数据
  GamePlayOddsModel defaultWithdrawalData = const GamePlayOddsModel();

  /// 选择的赛事
  final select = (const Lottery()).obs;

  /// 聊天室选择
  final chatData = (const ChatGetTokenModel()).obs;

  /// 聊天室选择
  final selectRoom = (const ChatAry()).obs;

  /// tab
  final tabs = ["投注区", "澳门永利高手心水..."].obs;
  late final tabController = TabController(length: tabs.length, vsync: this)
    ..addListener(tabListener);

  /// 选择的tab
  final selectTab = 0.obs;

  /// 玩法
  final playOdds = (const GamePlayOddsModel()).obs;

  /// 赔率详情
  final details = (const GamePlayOddsModel()).obs;

  /// 开奖颜色
  final ballColor = {
    1: Get.context?.customTheme?.lottery1,
    2: Get.context?.customTheme?.lottery2,
    3: Get.context?.customTheme?.lottery3,
    4: Get.context?.customTheme?.lottery4,
    5: Get.context?.customTheme?.lottery5,
    6: Get.context?.customTheme?.lottery6,
    7: Get.context?.customTheme?.lottery7,
    8: Get.context?.customTheme?.lottery8,
    9: Get.context?.customTheme?.lottery9,
    10: Get.context?.customTheme?.lottery10,
  };
  // 选择的注
  final selectNote = [].cast<Play>().obs;

  /// 组选复式显示注数
  final notes = 0.obs;

  /// 筹码列表
  final chipsList = [].cast<int>().obs;

  /// 筹码展示状态
  final chipStatus = false.obs;

  /// 选择的侧边类型 如两面（LM）
  final selectType = "".obs;

  /// 选择的数据
  final selectPlay = [].cast<PlayGroup?>().obs;

  /// 追号数据
  final chasingNumber = [].cast<PlayGroup?>().obs;

  /// 选择的数量
  final selectLength = 0.obs;

  /// 连码计算过后的组合
  final lmaResult = [].cast<List<int>>().obs;

  Timer? deleteTimer;
  final playTypeSelect = <String, List<Play>>{};

  /// 控制器数组
  final textControllerList = [].cast<TextEditingController>().obs;

  /// 视频url
  final videoUrl = "".obs;

  final videoStatus = false.obs;

  final progress = (0.0).obs;

  final oddStatus = true.obs;

  /// 专家跟单开关
  final expoertSwitch = false.obs;

  /// 批量修改金额输入框
  late final changeMoneyTextController = TextEditingController()
    ..addListener(changeMoneyListener);
  late Map<String, Widget> playTypeComponent = {
    "LM": Obx(() => SecondFacePlay(
          data:
              playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "LM"),
          onChange: (list, data) {
            selectType.value = "LM";
            playTypeSelect["LM"] = list;
            selectPlay.value = data;
          },
        )), // 两面
    "GFWF": Obx(() => OfficePlay(
          data: playOdds.value.playOdds
              ?.firstWhereOrNull((e) => e.code == "GFWF"),
          onChange: (list, data) {
            playTypeSelect["LM"] = list;
            selectType.value = "GFWF";
            selectPlay.value = data;
          },
        )), // 官方玩法
    "HE": Obx(
      () => ChampionPlay(
          data:
              playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "HE"),
          onChange: (list, data) {
            playTypeSelect["HE"] = list;
            selectType.value = "HE";
            selectPlay.value = data;
          }),
      key: ValueKey("HE_${select.value.id ?? lotteryId}"),
    ), // 冠亚和
    "Q1": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q1"),
            onChange: (list, data) {
              playTypeSelect["Q1"] = list;
              selectType.value = "Q1";
              selectPlay.value = data;
            }),
        key: ValueKey("Q1_${select.value.id ?? lotteryId}")), // 冠军
    "Q2": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q2"),
            onChange: (list, data) {
              playTypeSelect["Q2"] = list;
              selectType.value = "Q2";
              selectPlay.value = data;
            }),
        key: ValueKey("Q2_${select.value.id ?? lotteryId}")), // 亚军
    "1-5": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "1-5"),
            onChange: (list, data) {
              playTypeSelect["1-5"] = list;
              selectType.value = "1-5";
              selectPlay.value = data;
            }),
        key: ValueKey("1-5_${select.value.id ?? lotteryId}")), // 1-5名
    "6-10": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "6-10"),
            onChange: (list, data) {
              playTypeSelect["6-10"] = list;
              selectType.value = "6-10";
              selectPlay.value = data;
            }),
        key: ValueKey("6-10_${select.value.id ?? lotteryId}")), // 6-10名
    "Q3": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q3"),
            onChange: (list, data) {
              playTypeSelect["Q3"] = list;
              selectType.value = "Q3";
              selectPlay.value = data;
            }),
        key: ValueKey("Q3_${select.value.id ?? lotteryId}")), // 第三名
    "Q4": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q4"),
            onChange: (list, data) {
              playTypeSelect["Q4"] = list;
              selectType.value = "Q4";
              selectPlay.value = data;
            }),
        key: ValueKey("Q4_${select.value.id ?? lotteryId}")), // 第四名
    "Q5": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q5"),
            onChange: (list, data) {
              playTypeSelect["Q5"] = list;
              selectType.value = "Q5";
              selectPlay.value = data;
            }),
        key: ValueKey("Q5_${select.value.id ?? lotteryId}")), // 第五名
    "Q6": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q6"),
            onChange: (list, data) {
              playTypeSelect["Q6"] = list;
              selectType.value = "Q6";
              selectPlay.value = data;
            }),
        key: ValueKey("Q6_${select.value.id ?? lotteryId}")), // 第六名
    "Q7": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q7"),
            onChange: (list, data) {
              playTypeSelect["Q7"] = list;
              selectType.value = "Q7";
              selectPlay.value = data;
            }),
        key: ValueKey("Q7_${select.value.id ?? lotteryId}")), // 第七名
    "Q8": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q8"),
            onChange: (list, data) {
              playTypeSelect["Q8"] = list;
              selectType.value = "Q8";
              selectPlay.value = data;
            }),
        key: ValueKey("Q8_${select.value.id ?? lotteryId}")), // 第八名
    "Q9": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q9"),
            onChange: (list, data) {
              playTypeSelect["Q9"] = list;
              selectType.value = "Q9";
              selectPlay.value = data;
            }),
        key: ValueKey("Q9_${select.value.id ?? lotteryId}")), // 第九名
    "Q10": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "Q10"),
            onChange: (list, data) {
              playTypeSelect["Q10"] = list;
              selectType.value = "Q10";
              selectPlay.value = data;
            }),
        key: ValueKey("Q10_${select.value.id ?? lotteryId}")), // 第十名
    "ALL": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ALL"),
            onChange: (list, data) {
              playTypeSelect["ALL"] = list;
              selectType.value = "ALL";
              selectPlay.value = data;
            }),
        key: ValueKey("ALL_${select.value.id ?? lotteryId}")), // 时时彩 _ 1-5球
    "QZH": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "QZH"),
            onChange: (list, data) {
              playTypeSelect["QZH"] = list;
              selectType.value = "QZH";
              selectPlay.value = data;
            }),
        key: ValueKey("QZH_${select.value.id ?? lotteryId}")), // 时时彩 _ 前中后
    "DN": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "DN"),
            onChange: (list, data) {
              playTypeSelect["DN"] = list;
              selectType.value = "DN";
              selectPlay.value = data;
            }),
        key: ValueKey("Dn_${select.value.id ?? lotteryId}")), // 时时彩 _ 斗牛
    "SH": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "SH"),
            onChange: (list, data) {
              playTypeSelect["SH"] = list;
              selectType.value = "SH";
              selectPlay.value = data;
            }),
        key: ValueKey("SH_${select.value.id ?? lotteryId}")), // 时时彩 _ 梭哈
    "QS": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "QS"),
            onChange: (list, data) {
              playTypeSelect["QS"] = list;
              selectType.value = "QS";
              selectPlay.value = data;
            }),
        key: ValueKey("QS_${select.value.id ?? lotteryId}")), // 前三
    "WX": Obx(
        () => TimerLotteryFiveElementPlay(
            type: "WX",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "WX"),
            onChange: (list, data) {
              playTypeSelect["WX"] = list;
              selectType.value = "WX";
              selectPlay.value = data;
            }),
        key: ValueKey("WX_${select.value.id ?? lotteryId}")), // 五行
    "ZM": Obx(
        () => ChampionPlay(
            type: "ZM",
            details: playOdds.value,
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZM"),
            onChange: (list, data) {
              playTypeSelect["ZM"] = list;
              selectType.value = "ZM";
              selectPlay.value = data;
            }),
        key: ValueKey("ZM_${select.value.id ?? lotteryId}")), // 正码
    "ZM1-6": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZM1-6"),
            onChange: (list, data) {
              playTypeSelect["ZM1-6"] = list;
              selectType.value = "ZM1-6";
              selectPlay.value = data;
            }),
        key: ValueKey("ZM1-6_${select.value.id ?? lotteryId}")), // 正码1-6
    "ZT": Obx(
        () => JustSpecialPlay(
            details: playOdds.value,
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZT"),
            onChange: (list, data) {
              playTypeSelect["ZT"] = list;
              selectType.value = "ZT";
              selectPlay.value = data ?? [];
            }),
        key: ValueKey("ZT_${select.value.id ?? lotteryId}")), // 正特
    "LMA": Obx(
        () => ConnectCodePlay(
            details: playOdds.value,
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "LMA"),
            onChange: (list, data) {
              playTypeSelect["LMA"] = list;
              selectType.value = "LMA";
              selectPlay.value = data;
              lmaResult.clear();
              final result = (data.first.plays?.first.name ?? "")
                  .split(",")
                  .map((e) => int.parse(e))
                  .toList();
              lmaResult.addAll(LotteryConfig.threeBthGenRandom(result));
              notes.value = lmaResult.length;
              LogUtil.w(lmaResult.value);
            }),
        key: ValueKey("LMA_${select.value.id ?? lotteryId}")), // 连码
    "SB": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "SB"),
            onChange: (list, data) {
              playTypeSelect["SB"] = list;
              selectType.value = "SB";
              selectPlay.value = data;
            }),
        key: ValueKey("SB_${select.value.id ?? lotteryId}")), // 色波
    "YX": Obx(
        () => FlatSpecial1Similar(
            details: playOdds.value,
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "YX"),
            onChange: (list, data) {
              playTypeSelect["YX"] = list;
              selectType.value = "YX";
              selectPlay.value = data;
            }),
        key: ValueKey("YX_${select.value.id ?? lotteryId}")), // 平特一肖

    "WS": Obx(
        () => FlatSpecial1Similar(
            details: playOdds.value,
            type: "WS",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "WS"),
            onChange: (list, data) {
              playTypeSelect["WS"] = list;
              selectType.value = "WS";
              selectPlay.value = data;
            }),
        key: ValueKey("WS_${select.value.id ?? lotteryId}")), // 平特尾数
    "TWS": Obx(
        () => TitleFootPlay(
            details: playOdds.value,
            type: "TWS",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "TWS"),
            onChange: (list, data) {
              selectPlay.value = data;
              playTypeSelect["TWS"] = list;
              selectType.value = "TWS";
              LogUtil.w("data_length___${data.length}");
            }),
        key: ValueKey("TWS_${select.value.id ?? lotteryId}")), // 头/尾数
    "ZOX": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZOX"),
            onChange: (list, data) {
              playTypeSelect["ZOX"] = list;
              selectType.value = "ZOX";
              selectPlay.value = data;
            }),
        key: ValueKey("ZOX_${select.value.id ?? lotteryId}")), // 总肖
    "TX": Obx(
        () => FlatSpecial1Similar(
            details: playOdds.value,
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "TX"),
            onChange: (list, data) {
              playTypeSelect["TX"] = list;
              selectType.value = "TX";
              selectPlay.value = data;
            }),
        key: ValueKey("TX_${select.value.id ?? lotteryId}")), // 特肖
    "LX": Obx(
        () => ConnectSimilarPlay(
            details: playOdds.value,
            type: "LX",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "LX"),
            onChange: (list, data) {
              playTypeSelect["LX"] = list;
              selectType.value = "LX";
              selectPlay.value = data;
            }),
        key: ValueKey("LX_${select.value.id ?? lotteryId}")), // 连肖
    "HX": Obx(
        () => CombineResemblePlay(
            details: playOdds.value,
            type: "HX",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "HX"),
            onChange: (list, data) {
              playTypeSelect["HX"] = list;
              selectType.value = "HX";
              selectPlay.value = data;
            }),
        key: ValueKey("HX_${select.value.id ?? lotteryId}")), // 合肖
    "LW": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "LW"),
            onChange: (list, data) {
              playTypeSelect["LW"] = list;
              selectType.value = "LW";
              selectPlay.value = data;
            }),
        key: ValueKey("LW_${select.value.id ?? lotteryId}")), // TODO:连尾-未开发
    "ZX": Obx(
        () => ConnectSimilarPlay(
            details: playOdds.value,
            type: "ZX",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZX"),
            onChange: (list, data) {
              playTypeSelect["ZX"] = list;
              selectType.value = "ZX";
              selectPlay.value = data;
            }),
        key: ValueKey("ZX_${select.value.id ?? lotteryId}")), // TODO:正肖-未开发
    "ZXBZ": Obx(
        () => OutOfChoicePlay(
            details: playOdds.value,
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZXBZ"),
            onChange: (list, data) {
              playTypeSelect["ZXBZ"] = list;
              selectType.value = "ZXBZ";
              selectPlay.value = data;
            }),
        key: ValueKey("ZXBC_${select.value.id ?? lotteryId}")), // TODO:自选不中
    "LHD": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "LHD"),
            onChange: (list, data) {
              playTypeSelect["LHD"] = list;
              selectType.value = "LHD";
              selectPlay.value = data;
            }),
        key: ValueKey("LHD_${select.value.id ?? lotteryId}")), // 龙虎斗
    "YZDW": Obx(
      () => TimeLottery1PositionPlay(
          type: "YZDW",
          data: playOdds.value.playOdds
              ?.firstWhereOrNull((e) => e.code == "YZDW"),
          onChange: (list, data) {
            playTypeSelect["YZDW"] = list;
            selectType.value = "YZDW";
            selectPlay.value = data;
          }),
      key: ValueKey("YZDW_${select.value.id ?? lotteryId}"),
    ), // 一字定位
    "EZDW": Obx(
        () => TimeLottery1PositionPlay(
            type: "EZDW",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "EZDW"),
            onChange: (list, data) {
              playTypeSelect["EZDW"] = list;
              selectType.value = "EZDW";
              selectPlay.value = data;
            }),
        key: ValueKey("EZDW_${select.value.id ?? lotteryId}")), // 二字定位
    "SZDW": Obx(
        () => TimeLottery1PositionPlay(
            type: "SZDW",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "SZDW"),
            onChange: (list, data) {
              playTypeSelect["SZDW"] = list;
              selectType.value = "SZDW";
              selectPlay.value = data;
            }),
        key: ValueKey("SZDW_${select.value.id ?? lotteryId}")), // 三字定位
    "SIZDW": Obx(
        () => TimeLottery1PositionPlay(
            type: "SIZDW",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "SIZDW"),
            onChange: (list, data) {
              playTypeSelect["SIZDW"] = list;
              selectType.value = "SIZDW";
              selectPlay.value = data;
            }),
        key: ValueKey("SIZDW_${select.value.id ?? lotteryId}")), // 四字定位
    "BDW": Obx(
        () => TimeLottery1PositionPlay(
            type: "BDW",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "BDW"),
            onChange: (list, data) {
              playTypeSelect["BDW"] = list;
              selectType.value = "BDW";
              selectPlay.value = data;
            }),
        key: ValueKey("BDW_${select.value.id ?? lotteryId}")), // 四字定位
    "DWD": Obx(
        () => TimeLotteryPositionGallbladder(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "DWD"),
            onChange: (list, data) {
              playTypeSelect["DWD"] = list;
              selectType.value = "DWD";
              selectPlay.value = data;
            }),
        key: ValueKey("DWD_${select.value.id ?? lotteryId}")), // 定位胆
    "TM": Obx(
      () => SpecialCodePlay(
          details: playOdds.value,
          data:
              playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "TM"),
          onChange: (list, data) {
            playTypeSelect["TM"] = list;
            selectType.value = "TM";
            selectPlay.value = data;
          }),
      key: ValueKey("TM_${select.value.id ?? lotteryId}"),
    ), // 特码
    "SJ": Obx(
      () => Quick3ThreeArmyPlay(
          details: playOdds.value,
          data:
              playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "SJ"),
          onChange: (list, data) {
            playTypeSelect["SJ"] = list;
            selectType.value = "SJ";
            selectPlay.value = data;
          }),
      key: ValueKey("SJ_${select.value.id ?? lotteryId}"),
    ), // 三军
    "DS": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "DS",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "DS"),
            onChange: (list, data) {
              playTypeSelect["DS"] = list;
              selectType.value = "DS";
              selectPlay.value = data;
            }),
        key: ValueKey("DS_${select.value.id ?? lotteryId}")), // 点数
    "CP": Obx(() => Quick3DicePlay(
        data: playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "CP"),
        details: playOdds.value,
        onChange: (list, data) {
          playTypeSelect["CP"] = list;
          selectType.value = "CP";
          selectPlay.value = data;
        },
        key: ValueKey("JSK3_CP_${select.value.id ?? lotteryId}"))), // 长牌
    "DP": Obx(() => Quick3DicePlay(
        data: playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "DP"),
        details: playOdds.value,
        onChange: (list, data) {
          playTypeSelect["DP"] = list;
          selectType.value = "DP";
          selectPlay.value = data;
        },
        key: ValueKey("K3_DP_${select.value.id ?? lotteryId}"))),
    "RTH": Obx(() => Quick32SameNumPlay(
        type: "RTH",
        data: playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "RTH"),
        details: playOdds.value,
        onChange: (list, data) {
          playTypeSelect["RTH"] = list;
          selectType.value = "RTH";
          selectPlay.value = data;
        },
        key: ValueKey("K3_RTH_${select.value.id ?? lotteryId}"))), // 二同号
    "SBTH": Obx(() => Quick32SameNumPlay(
        type: "SBTH",
        data:
            playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "SBTH"),
        details: playOdds.value,
        onChange: (list, data) {
          playTypeSelect["SBTH"] = list;
          selectType.value = "SBTH";
          selectPlay.value = data;
        },
        key: ValueKey("K3_SBTHH_${select.value.id ?? lotteryId}"))), // 三不同号
    "CBC": Obx(() => Quick32SameNumPlay(
        data: playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "CBC"),
        details: playOdds.value,
        onChange: (list, data) {
          playTypeSelect["CBC"] = list;
          selectType.value = "CBC";
          selectPlay.value = data;
        },
        type: "CBC",
        key: ValueKey("K3_CBC_${select.value.id ?? lotteryId}"))), // 猜不出
    "CM": Obx(() => Quick32SameNumPlay(
        data: playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "CM"),
        details: playOdds.value,
        onChange: (list, data) {
          playTypeSelect["CM"] = list;
          selectType.value = "CM";
          selectPlay.value = data;
        },
        type: "CM",
        key: ValueKey("K3_CM_${select.value.id ?? lotteryId}"))), // 猜码
    "YS": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "YS",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "YS"),
            onChange: (list, data) {
              playTypeSelect["YS"] = list;
              selectType.value = "YS";
              selectPlay.value = data;
            }),
        key: ValueKey("YS_${select.value.id ?? lotteryId}")), // 颜色
    "DZHZ": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "DZHZ",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "DZHZ"),
            onChange: (list, data) {
              playTypeSelect["DZHZ"] = list;
              selectType.value = "DZHZ";
              selectPlay.value = data;
            }),
        key: ValueKey("DZHZ_${select.value.id ?? lotteryId}")), // 对子和值
    "SBTHZ": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "SBTHZ",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "SBTHZ"),
            onChange: (list, data) {
              playTypeSelect["SBTHZ"] = list;
              selectType.value = "SBTHZ";
              selectPlay.value = data;
            }),
        key: ValueKey("SBTHZ_${select.value.id ?? lotteryId}")), //三不同和值
    "XT": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "XT",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "XT"),
            onChange: (list, data) {
              playTypeSelect["XT"] = list;
              selectType.value = "XT";
              selectPlay.value = data;
            }),
        key: ValueKey("SBTHZ_${select.value.id ?? lotteryId}")), // 形态
    "1Z1": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "1Z1",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "1Z1"),
            onChange: (list, data) {
              playTypeSelect["1Z1"] = list;
              selectType.value = "1Z1";
              selectPlay.value = data;
            }),
        key: ValueKey("1Z1_${select.value.id ?? lotteryId}")), // 1中1
    "QW": Obx(
        () => TastePlay(
            details: playOdds.value,
            type: "QW",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "QW"),
            onChange: (list, data) {
              playTypeSelect["QW"] = list;
              selectType.value = "QW";
              selectPlay.value = data;
            }),
        key: ValueKey("QW_${select.value.id ?? lotteryId}")), //  趣味
    "RXDT": Obx(
        () => OptionalPlay(
            details: playOdds.value,
            type: "RXDT",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "RXDT"),
            onChange: (list, data) {
              playTypeSelect["RXDT"] = list;
              selectType.value = "RXDT";
              selectPlay.value = data;
            }), //
        key: ValueKey("RXDT_${select.value.id ?? lotteryId}")), //  任选
    "QIU1": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "QIU1"),
            onChange: (list, data) {
              playTypeSelect["QIU1"] = list;
              selectType.value = "QIU1";
              selectPlay.value = data;
            }),
        key: ValueKey("QIU1_${select.value.id ?? lotteryId}")), // 3d第一球
    "QIU2": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "QIU2"),
            onChange: (list, data) {
              playTypeSelect["QIU2"] = list;
              selectType.value = "QIU2";
              selectPlay.value = data;
            }),
        key: ValueKey("QIU2_${select.value.id ?? lotteryId}")), // 3d第2球
    "QIU3": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "QIU3"),
            onChange: (list, data) {
              playTypeSelect["QIU3"] = list;
              selectType.value = "QIU3";
              selectPlay.value = data;
            }),
        key: ValueKey("QIU3_${select.value.id ?? lotteryId}")), // 3d第3球
    "KD": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "KD"),
            onChange: (list, data) {
              playTypeSelect["KD"] = list;
              selectType.value = "KD";
              selectPlay.value = data;
            }),
        key: ValueKey("KD_${select.value.id ?? lotteryId}")), // 跨度
    "3L": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "3L"),
            onChange: (list, data) {
              playTypeSelect["3L"] = list;
              selectType.value = "3L";
              selectPlay.value = data;
            }),
        key: ValueKey("3L_${select.value.id ?? lotteryId}")), // 3连
    "DD": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "DD"),
            onChange: (list, data) {
              playTypeSelect["DD"] = list;
              selectType.value = "DD";
              selectPlay.value = data;
            }),
        key: ValueKey("DD_${select.value.id ?? lotteryId}")), // 独胆
    "ZHLH": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZHLH"),
            onChange: (list, data) {
              playTypeSelect["ZHLH"] = list;
              selectType.value = "ZHLH";
              selectPlay.value = data;
            }),
        key: ValueKey("ZHLH_${select.value.id ?? lotteryId}")), // 总和-龙虎
    "EZ": Obx(
        () => SecondFontPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "EZ"),
            onChange: (list, data) {
              playTypeSelect["EZ"] = list;
              selectType.value = "EZ";
              selectPlay.value = data;
            }),
        key: ValueKey("EZ_${select.value.id ?? lotteryId}")), // 二字
    "HS": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "HS"),
            onChange: (list, data) {
              playTypeSelect["HS"] = list;
              selectType.value = "HS";
              selectPlay.value = data;
            }),
        key: ValueKey("HS_${select.value.id ?? lotteryId}")), // 和数
    "HSWS": Obx(
        () => ChampionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "HSWS"),
            onChange: (list, data) {
              playTypeSelect["HSWS"] = list;
              selectType.value = "HSWS";
              selectPlay.value = data;
            }),
        key: ValueKey("HSWS_${select.value.id ?? lotteryId}")), // 和数尾和
    "QQ": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "QQ",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "QQ"),
            onChange: (list, data) {
              playTypeSelect["QQ"] = list;
              selectType.value = "QQ";
              selectPlay.value = data;
            }),
        key: ValueKey("QQ_${select.value.id ?? lotteryId}")), // 前区
    "HQ": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "HQ",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "HQ"),
            onChange: (list, data) {
              playTypeSelect["HQ"] = list;
              selectType.value = "HQ";
              selectPlay.value = data;
            }),
        key: ValueKey("HQ_${select.value.id ?? lotteryId}")), // 后区
    "FW": Obx(
        () => ChampionPlay(
            details: playOdds.value,
            type: "FW",
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "FW"),
            onChange: (list, data) {
              playTypeSelect["FW"] = list;
              selectType.value = "FW";
              selectPlay.value = data;
            }),
        key: ValueKey("FW_${select.value.id ?? lotteryId}")), // 方位
  }.cast<String, Widget>();

  handleShowRoom() {
    final list = chatData.value.chatAry
        ?.filter((e) => e.roomId != selectRoom.value.roomId)
        .toList();
    if ((chatData.value.chatAry??[]).isEmpty) {
      return;
    }
    ToastUtils.confirm(CustomConfirmParams(
        showTitle: false,
        showSubmit: false,
        contentChild: ListView.builder(
            shrinkWrap: true,
            itemCount: list?.length ?? 0,
            itemBuilder: (context, index) {
              final item = list?[index];
              return Container(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      margin: const EdgeInsets.only(bottom: 10),
                      decoration: BoxDecoration(
                          color: context.customTheme?.resetBg,
                          borderRadius: 10.radius),
                      alignment: Alignment.center,
                      child: Text(item?.roomName ?? "",
                          style: context.textTheme.bodyMedium?.copyWith(
                              color: context.customTheme?.reverseFontColor)))
                  .onTap(() {
                // 切换房间
                ToastUtils.closeAll();
                selectRoom.value = item!;
                tabs[1] = item.roomName ?? "";
              });
            })));
  }

  changeMoneyListener() {
    if (changeMoneyTextController.text.replaceAll(" ", "").isNotEmpty) {
      textControllerList.value = textControllerList.map((e) {
        e.text = changeMoneyTextController.text;
        return e;
      }).toList();
    }
  }

  handleSliderValue(double value, [bool isSlide = false]) {
    if (isSlide) {
      withdrawalProgress.value = value;
    } else {
      final res = withdrawalProgress.value + value;
      withdrawalProgress.value = value == -1 ? max(0, res) : min(res, 10);
    }
    // final current = playOdds.value.playOdds?.firstWhereOrNull((e) => e.code ==  playOdds.value.playOdds?[leftMenuActive.value].code);
    playOdds.value = defaultWithdrawalData.copyWith(
        playOdds: defaultWithdrawalData.playOdds?.map((e) {
      final isCurrent =
          e.code == defaultWithdrawalData.playOdds?[leftMenuActive.value].code;
      if (isCurrent) {
        return e.copyWith(
            playGroups: e.playGroups?.map((q) {
          return q.copyWith(
              plays: q.plays?.map((w) {
            // print("退水 ${(double.tryParse(w.odds ?? "0") ?? 0)} ${( withdrawalProgress.value / 100)}");
            return w.copyWith(
                odds: ((double.tryParse(w.odds ?? "0") ?? 0) -
                        (withdrawalProgress.value / 100)
                            .toStringAsFixed(2)
                            .toDouble())
                    .toStringAsFixed(2));
          }).toList());
        }).toList());
      }
      return e;
    }).toList());

    deleteAll();
  }

  // 六合彩特别的
  lhcSpecialWidget(PlayOdd value) {
    // value.
    if ((select.value.gameType?.contains("lhc") ?? false) &&
        value.code == "WX") {
      return Obx(() => FiveElementPlay(
          data:
              playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "WX"),
          details: playOdds.value,
          onChange: (list, data) {
            playTypeSelect["WX"] = list;
            selectType.value = "WX";
            selectPlay.value = data;
          },
          key: ValueKey("LHC_WX_${select.value.id ?? lotteryId}")));
    }

    /// 六合彩，连尾
    if ((select.value.gameType?.contains("lhc") ?? false) &&
        value.code == "LW") {
      return Obx(
          () => ConnectSimilarPlay(
              details: playOdds.value,
              type: "LW",
              data: playOdds.value.playOdds
                  ?.firstWhereOrNull((e) => e.code == "LW"),
              onChange: (list, data) {
                playTypeSelect["LW"] = list;
                selectType.value = "LW";
                selectPlay.value = data;
              }),
          key: ValueKey("LW_${select.value.id ?? lotteryId}"));
    }

    // 江苏快三 尾数
    if (select.value.gameType == "jsk3" && value.code == "WS") {
      return Obx(() => Quick3DicePlay(
          data:
              playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "WS"),
          details: playOdds.value,
          onChange: (list, data) {
            playTypeSelect["WS"] = list;
            selectType.value = "WS";
            selectPlay.value = data;
          },
          key: ValueKey("JSK3_WS_${select.value.id ?? lotteryId}")));
    }
    // 广东11选5 连码
    if (select.value.gameType == "gd11x5" && value.code == "LMA") {
      LogUtil.w(
          playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "LMA"));
      return Obx(
        () => ElevenOutOf5Play(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "LMA"),
            details: playOdds.value,
            onChange: (list, data) {
              playTypeSelect["LMA"] = list;
              selectType.value = "LMA";
              selectPlay.value = data;
            },
            type: "LMA",
            key: ValueKey("gd11x5_LMA_${select.value.id ?? lotteryId}")),
      );
    }
    // 广东11选5，猜不出
    if (select.value.gameType == "gd11x5" && value.code == "CBC") {
      return Obx(
        () => ElevenOutOf5Play(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "CBC"),
            details: playOdds.value,
            onChange: (list, data) {
              playTypeSelect["CBC"] = list;
              selectType.value = "CBC";
              selectPlay.value = data;
            },
            type: "CBC",
            key: ValueKey("gd11x5_CBC_${select.value.id ?? lotteryId}")),
      );
    }

    // 广东11选5 定位
    if (select.value.gameType == "gd11x5" && value.code == "DW") {
      return Obx(
        () => Gd11x5PositionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "DW"),
            details: playOdds.value,
            onChange: (list, data) {
              playTypeSelect["DW"] = list;
              selectType.value = "DW";
              selectPlay.value = data;
            },
            type: "DW",
            key: ValueKey("gd11x5_DW_${select.value.id ?? lotteryId}")),
      );
    }

    // 广东1选5，直选
    if (select.value.gameType == "gd11x5" && value.code == "ZX") {
      return Obx(
        () => DirectElectionPlay(
            data: playOdds.value.playOdds
                ?.firstWhereOrNull((e) => e.code == "ZX"),
            details: playOdds.value,
            onChange: (list, data) {
              playTypeSelect["ZX"] = list;
              selectType.value = "ZX";
              selectPlay.value = data;
            },
            type: "ZX",
            key: ValueKey("gd11x5_ZX_${select.value.id ?? lotteryId}")),
      );
    }

    // 福彩3D，定位胆
    if (select.value.gameType == "fc3d" && value.code == "DWD") {
      return Obx(() => Welfare3dDwdPlay(
          data:
              playOdds.value.playOdds?.firstWhereOrNull((e) => e.code == "DWD"),
          details: playOdds.value,
          onChange: (list, data) {
            playTypeSelect["DWD"] = list;
            if (["DWDZXSFS", "DWDZXLFS"]
                .contains(data.first.plays!.first.code)) {
              selectType.value = data.first.plays!.first.code ?? '';
              final name = data.first.plays!.first.name!
                  .split(",")
                  .map((e) => int.parse(e))
                  .toList();

              /// 组合数
              List<List<int>> result = LotteryConfig.generateCombinations(
                  name, data.first.plays!.first.code);
              // generatePermutations(arr, arr.length, result);

              // 注数
              if (data.first.plays!.first.code == "DWDZXSFS") {
                notes.value = LotteryConfig.combineArray(name, 2, true).length;
              } else {
                notes.value = LotteryConfig.combineArray(name, 3, false).length;
              }
              lmaResult.value = result;
            } else {
              selectType.value = "DWD";
              lmaResult.value = [];
            }

            selectPlay.value = data;
            // if ()
          },
          type: "DWD",
          key: ValueKey("fc3d_DWD_${select.value.id ?? lotteryId}")));
    }
    // 大乐透，特码
    if (select.value.gameType == "dlt" && value.code == "TM") {
      return Obx(
          () => ChampionPlay(
              type: "dlt_TW",
              data: playOdds.value.playOdds
                  ?.firstWhereOrNull((e) => e.code == "TM"),
              onChange: (list, data) {
                playTypeSelect["TM"] = list;
                selectType.value = "TM";
                selectPlay.value = data;
              }),
          key: ValueKey("TM_${select.value.id ?? lotteryId}"));
    }
    // 澳洲幸运20
    if (select.value.gameType == "bjkl8" && value.code == "WX") {
      return Obx(
          () => ChampionPlay(
              type: "bjkl8_WX",
              data: playOdds.value.playOdds
                  ?.firstWhereOrNull((e) => e.code == "WX"),
              onChange: (list, data) {
                playTypeSelect["WX"] = list;
                selectType.value = "WX";
                selectPlay.value = data;
              }),
          key: ValueKey("bjkl8_WX_${select.value.id ?? lotteryId}"));
    }
    return (leftMenuActiveDetail.value.code?.isEmpty ?? false)
        ? const SizedBox()
        : playTypeComponent[leftMenuActiveDetail.value.code] ??
            const SizedBox();
  }

  /// 左侧选中
  final leftMenuActive = (-1).obs;

  /// 左侧选中具体内容
  final leftMenuActiveDetail = (const PlayOdd()).obs;

  /// 赛事历史记录
  final history = (const GameLotteryHistoryModel()).obs;

  /// 展开历史记录状态
  final historyStatus = false.obs;

  /// 输入框控制器
  late final textController = TextEditingController()..addListener(listener);

  /// 金额
  final money = (0.0).obs;

  /// 投注总金额
  final betMoney = (0.0).obs;

  String? lotteryId = Get.parameters['id'];

  listener() {
    money.value = double.tryParse(textController.text) ?? 0;
  }

  /// 获取列表
  Future getData() async {
    final res = await GameHttp.gameLotteryGroupGames();
    if (res.models?.isNotEmpty ?? false) {
      // select.value = res.models?.first.lotteries?.first ?? const Lottery();
      select.value = res.models
              ?.firstOrNullWhere((e) {
                return (e.lotteries ?? [])
                        .firstWhereOrNull((q) => q.id == lotteryId) !=
                    null;
              })
              ?.lotteries
              ?.firstWhereOrNull((e) => e.id == lotteryId) ??
          res.models?.first.lotteries?.first ??
          const Lottery();

      openIndex.value = res.models?.indexWhere((e) {
            return (e.lotteries ?? [])
                    .firstWhereOrNull((q) => q.id == lotteryId) !=
                null;
          }) ??
          0;
      secondActive.value = select.value.id ?? "";
      GlobalService.to.currentLotteryId.value = lotteryId ?? "";
      getHistoryData();
      await getExpoertSwitch();
      await getNextIsuData();
      await getPlayOddsData(true);
      // LogUtil.w(res);
      data.value = res.models!;
      loading.value = false;
    }
  }

  // 展示长龙组件
  showLong() {
    Get.bottomSheet(LongComponent(
      id: select.value.id,
      selectType: select.value.gameType,
    ));
  }

  betMoneySum() {
    final isLma = [
      "LMA",
    ].contains(selectType.value);
    double result = 0;
    for (var e in textControllerList) {
      result += (double.tryParse(e.text) ?? 0);
    }

    betMoney.value = isLma
        ? result * lmaResult.length
        : ["DWDZXSFS", "DWDZXLFS"].contains(selectType.value)
            ? notes.value * result
            : result;
    LogUtil.w("betMoney___${betMoney.value}");
  }

  deleteAll() {
    if (deleteTimer?.isActive ?? false) {
      return;
    }
    notes.value = 0;

    deleteTimer = Timer.periodic(100.microseconds, (value) {
      if (selectPlay.isNotEmpty) {
        final item = selectPlay.last;
        GlobalService.to.changeDeleteGroup(item!);
        selectPlay.remove(item);
      } else {
        deleteTimer?.cancel();
      }
    });
  }

  /// 下注
  handleSubmit([bool isChasingNumber = false]) {
    LogUtil.w(selectPlay);
    if (!isChasingNumber) {
      if (selectPlay.isEmpty) {
        ToastUtils.show("请选择玩法");
        return;
      }
      if (textController.text.isEmpty) {
        ToastUtils.show("请输入投注金额");
        return;
      }
      textControllerList.value = selectPlay
          .map((e) => TextEditingController(text: textController.text))
          .toList();
    } else {
      if (chasingNumber.isEmpty) {
        return;
      }
      selectPlay.value = chasingNumber;
    }
    FocusScope.of(Get.context!).requestFocus(FocusNode());
    betMoneySum();
    ToastUtils.confirm(CustomConfirmParams(
        onDismiss: () {
          if (isChasingNumber) {
            selectPlay.value = [];
          }
        },
        cancel: () {
          ToastUtils.closeAll();
        },
        title: "第${getNextIsu.value.preIssue}期 ${select.value.title} 下注明细",
        submit: () async {
          List<PlayGroup?> betList = selectPlay;
          // 如果id重复，则合为一注
          bool synthesis = true;
          for (var i = 0; i < betList.length; i++) {
            final item = betList[i];
            if (item?.plays?.first.id != betList.first?.plays?.first.id) {
              synthesis = false;
            }
          }

          // 所有的id相同，合为一注, 目前只有猜不出需要特殊处理
          if (["CBC"].contains(selectType.value)) {
            betList = [
              betList.first?.copyWith(
                  plays: betList.first?.plays?.map((e) {
                return e.copyWith(
                    alias: betList
                        .map((q) => q?.plays?.first.alias)
                        .toList()
                        .join(","));
              }).toList())
            ];
          }
          // TODO: 合肖下注错误，无法排查到问题
          chasingNumber.value = [...betList];
          LogUtil.w("VETLIST___$betList");
          final dwList = ["YZDW", "EZDW", "SZDW", "SIZDW", "BDW"];
          List<BetBeanParams> betBean = betList
              .mapIndexed<BetBeanParams>((i, e) => BetBeanParams(
                    playId: e?.plays?.first.id,
                    betInfo: (e?.plays?.first.name?.contains(",") ?? false)
                        ? e?.plays?.first.name
                        : dwList.contains(leftMenuActiveDetail.value.code)
                            ? e?.plays?.first.name
                            : null,
                    money: ["LMA", "DWDZXSFS", "DWDZXLFS", "CBC"]
                            .contains(selectType.value)
                        ? betMoney.value.toStringAsFixed(2)
                        : "${double.tryParse(textControllerList[i].text)?.toStringAsFixed(2)}",
                    // betInfo: e?.alias,
                    playIds: "${e?.plays?.first.name}",
                    betNum: e?.plays?.first.name,
                    name: "${e?.alias},${e?.plays?.first.name}",
                    odds: e?.plays?.first.odds,
                  ))
              .toList();
          final res = await UserHttp.userGameBetWithParams(BetParams(
              gameId: select.value.id,
              betIssue: getNextIsu.value.curIssue,
              totalNum: "${[
                "LMA",
                "DWDZXSFS",
                "DWDZXLFS"
              ].contains(selectType.value) ? lmaResult.length : selectPlay.length}",
              totalMoney: ["LMA"].contains(selectType.value)
                  ? (betMoney.value * lmaResult.length).toStringAsFixed(2)
                  : betMoney.value.toStringAsFixed(2),
              betBean: betBean,
              endTime: (DateTime.tryParse(getNextIsu.value.curCloseTime ?? '')!
                          .millisecondsSinceEpoch ~/
                      1000)
                  .toString()));
          if (res.code == 0) {
            deleteAll();
            textController.text = "";
            getUserBalance();
            GlobalService.to.fetchUserBalance();
            // selectPlay.value = [];
            // GlobalService.to.handleDeleteAllRobotRndom();
          }
          ToastUtils.show(res.msg);
        },
        contentChild: ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: Get.context!.mediaQuerySize.height * .5,
          ),
          child: SingleChildScrollView(
            child: Obx(
              () => Column(
                children: [
                  Row(
                    children: [
                      Expanded(child: Text("号码")),
                      SizedBox(width: 80, child: Text("赔率")),
                      SizedBox(width: 50, child: Text("金额")),
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          // width: 50,
                          child: Text("操作")),
                    ],
                  ).paddingOnly(top: 20),
                  ...selectPlay.mapIndexed((i, e) => Column(
                        children: [
                          Row(
                            children: [
                              // -${e?.plays?.first.alias}
                              Expanded(
                                  child: Text(
                                      "[${e?.alias}-${e?.plays?.first.name}]"
                                      // "[${e?.name != leftMenuActiveDetail.value.name ? '${e?.name}-' : ""} ${(e?.plays?.first.alias?.isNotEmpty ?? false) ?  "${e?.plays?.first.alias}-":""} ${e?.plays?.first.name != null ? '${e?.plays?.first.name }' : ''}]"
                                      )),
                              SizedBox(
                                  width: 80,
                                  child: Text(
                                    "@${double.tryParse(e?.plays?.first.odds ?? "0")}x",
                                    softWrap: false,
                                  )),
                              SizedBox(
                                width: 50,
                                child: SizedBox(
                                  height: 40,
                                  // width: 50,
                                  child: TextField(
                                    controller: textControllerList[i],
                                    onChanged: (value) {
                                      betMoneySum();
                                    },
                                    keyboardType: TextInputType.number,
                                    decoration: const InputDecoration(
                                        isCollapsed: true,
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 10.0, horizontal: 10),
                                        border: OutlineInputBorder()),
                                  ),
                                ),
                              ),
                              Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      // width: 100,
                                      child: Icon(Icons.delete,
                                          color: Get
                                              .context!.customTheme?.lottery4))
                                  .onTap(() {
                                GlobalService.to.changeDeleteGroup(e!);
                                selectPlay.remove(e);
                                // betMoneySum();
                                LogUtil.w("selectPlay___${selectPlay}");
                                textControllerList.removeAt(i);
                                betMoneySum();

                                /// selectPlay 为长度为0 关闭下注弹窗
                                if (selectPlay.isEmpty) {
                                  ToastUtils.closeAll();
                                }
                              }),
                            ],
                          ).paddingSymmetric(vertical: 5),
                          Divider()
                        ],
                      )),
                  Row(
                    children: [
                      const Expanded(child: Text("批量修改金额")),
                      Expanded(
                        child: SizedBox(
                          // height: 40,
                          child: TextField(
                            controller: changeMoneyTextController,
                            keyboardType: TextInputType.number,
                            onChanged: (value) {
                              betMoneySum();
                            },
                            decoration: const InputDecoration(
                                isCollapsed: true,
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10),
                                border: OutlineInputBorder()),
                          ),
                        ),
                      )
                    ],
                  ).paddingSymmetric(vertical: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text.rich(
                        TextSpan(
                            text: "总注数：",
                            children: [
                              TextSpan(
                                  text: "${[
                                    "DWDZXSFS",
                                    "DWDZXLFS"
                                  ].contains(selectType.value) ? notes.value : [
                                      'LMA'
                                    ].contains(selectType.value) ? lmaResult.length : selectPlay.length}",
                                  style: Get.context!.textTheme.titleLarge
                                      ?.copyWith(
                                          fontSize: 24,
                                          color: Get
                                              .context!.customTheme?.lottery2)),
                            ],
                            style:
                                Get.context!.textTheme.titleMedium?.copyWith()),
                      ),
                      Text.rich(
                        TextSpan(
                            text: "总金额：",
                            children: [
                              TextSpan(
                                  text: "${betMoney.value}",
                                  style: Get.context!.textTheme.titleLarge
                                      ?.copyWith(
                                          fontSize: 24,
                                          color: Get
                                              .context!.customTheme?.lottery2)),
                            ],
                            style:
                                Get.context!.textTheme.titleMedium?.copyWith()),
                      ).paddingOnly(left: 30)
                    ],
                  ),
                  // 连码显示组合
                  ["LMA", "DWDZXSFS", "DWDZXLFS"].contains(selectType.value)
                      ? Container(
                          padding: EdgeInsets.only(top: 10),
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("组合数：${lmaResult.length}，组合明细："),
                              Wrap(
                                children: lmaResult
                                    .map((e) => Text(jsonEncode(e)))
                                    .toList(),
                              ).paddingSymmetric(vertical: 10)
                            ],
                          ),
                        )
                      : const SizedBox()
                ],
              ).onTap(() {
                FocusScope.of(Get.context!).requestFocus(FocusNode());
              }).paddingOnly(bottom: 10),
            ),
          ),
        )));
  }

  handleVideo() {
    videoUrl.value = "";
    final url = LotteryConfig.getVideoUrl(
        host: GlobalService.to.appInfo.value.siteUrl,
        id: select.value.id,
        siteId: KeyConfig().siteId);
    videoUrl.value = url;
    videoStatus.value = !videoStatus.value;
  }

  openHashDesc() async {
    final res = await GameHttp.gameLotteryRule(select.value.id ?? "");
    ToastUtils.confirm(CustomConfirmParams(
        showTitle: false,
        submit: () {
          ToastUtils.close();
        },
        contentChild: SizedBox(
          height: Get.context!.mediaQuerySize.height * .6,
          child: SingleChildScrollView(
            child: HtmlWidget("""
              ${res.data}
                  """),
          ),
        )));
  }

  handleOpenHashWebview() {
    Get.toNamed(AppRoutes.webview, parameters: {
      ...AppWebviewParams(
              // 替换为真实的路径
              url: jsonEncode(getNextIsu.value.verifUrl),
              title: select.value.title)
          .toJson()
    });
  }

  handleOpenWebview() {
    final url = LotteryConfig.getWebUrl(
        host: GlobalService.to.appInfo.value.siteUrl,
        lotteryId: select.value.id,
        gameType: select.value.gameType,
        token: GlobalService.to.token.value
        // id: select.value.id,
        // siteId:
        );
    Get.toNamed(AppRoutes.webview, parameters: {
      ...AppWebviewParams(
              // 替换为真实的路径
              url: jsonEncode(url),
              title: select.value.title)
          .toJson()
    });
    LogUtil.w("url____$url");
  }

  /// 重置
  handleReset() {
    resetSelect();
    FocusScope.of(Get.context!).requestFocus(FocusNode());
    textController.text = "";
  }

  /// 选择筹码
  /// *[index] -1 清除
  handleSelectChips(int index) {
    if (index == -1) {
      textController.text = "";
    } else {
      textController.text = "${chipsList[index]}";
    }
  }

  // 打开筹码分布
  handleOpenChip() {
    chipStatus.value = !chipStatus.value;
  }

  /// 机选
  handleRobotSelect() {
    GlobalService.to.robotRandom.value = UuidUtil.to.v4();
  }

  openDraw() {
    scffoldKey.value.currentState?.openDrawer();
  }

  closeDraw() {
    scffoldKey.value.currentState?.closeDrawer();
  }

  handleOpen(int index, LotteryGroupGamesModel item) {
    if (openIndex.value == index) {
      openIndex.value = -1;
      return;
    }
    openIndex.value = index;
    selectSub.value = item;
  }

  handleSecondActive(String value, Lottery obj) {
    secondActive.value = value;
    select.value = obj;
    GlobalService.to.currentLotteryId.value = obj.id ?? "";
    leftMenuActive.value = -1;
    leftMenuActiveDetail.value = const PlayOdd();
    playOdds.value = const GamePlayOddsModel();
    resetSelect();
    closeDraw();
    getNextIsuData();
    getHistoryData();
    getPlayOddsData(true);
  }

  handleRefresh() async {
    // refreshStatus.value = true;
    refreshController.forward();
    // await Future.delayed(3.seconds);
    await getUserBalance();
    refreshController.stop();
  }

  handleSelectTab(int value) {
    selectTab.value = value;
    tabController.animateTo(value);
  }

  handleBackHome() {
    // Get.offNamedUntil();
    // Get.back();
    // Get.back();
    Get.offNamedUntil(homePath, (route) => route.isFirst);
  }

  getNextIsuData([bool status = false]) async {
    if (!status) {
      loading.value = true;
    }
    try {
      final res = await GameHttp.gameNextIssue(select.value.id ?? "");
      if (res.data != null) {
        getNextIsu.value = res.data!;
        final closeTime = DateTime.tryParse(res.data?.curCloseTime ?? "")
                ?.millisecondsSinceEpoch ??
            0;
        final curTime = DateTime.tryParse(res.data?.serverTime ?? "")
                ?.millisecondsSinceEpoch ??
            0;
        final curOpenTime = DateTime.tryParse(res.data?.curOpenTime ?? "")
                ?.millisecondsSinceEpoch ??
            0;
        final closeDifference = closeTime - curTime;
        final openDifference = curOpenTime - curTime;
        closeingTimeNumber.value =
            int.parse((closeDifference ~/ 1000).toStringAsFixed(0));
        openTimeNumber.value =
            int.parse((openDifference ~/ 1000).toStringAsFixed(0));
        if (closingTimer?.isActive ?? false) {
          closingTimer?.cancel();
        }
        closingTimer = Timer.periodic(1000.milliseconds, (value) async {
          if (openTimeNumber < 0) {
            closingTimer?.cancel();
            return;
          }

          /// 封盘倒计时
          if (closeingTimeNumber.value > 0) {
            closeingTimeNumber.value -= 1;
          } else {
            if (tabController.index == 0) {
              FocusScope.of(Get.context!).requestFocus(FocusNode());
            }
          }

          /// 开奖倒计时
          if (openTimeNumber.value > 0) {
            openTimeNumber.value -= 1;
          } else {
            FocusScope.of(Get.context!).requestFocus(FocusNode());
          }
          if (closeingTimeNumber.value <= 0 && openTimeNumber.value <= 0) {
            await EventUtils.sleep(3.seconds);
            closingTimer?.cancel();
            getNextIsuData(true);
            getHistoryData();
            getPlayOddsData();
            // selectType.value = "";
            // selectPlay.value = [];
          }
        });
      }
    } catch (e) {
      LogUtil.e(e);
    }
    loading.value = false;
  }

  resetSelect() {
    selectType.value = "";
    LogUtil.w(selectPlay);
    deleteAll();
  }

  Future getHistoryData() async {
    final res = await GameHttp.gameLotteryHistory(GameLotteryHistoryParams(
      id: select.value.id,
    ));
    if (res.data != null) {
      history.value = res.data!;
    }
  }

  handleHistory() async {
    historyStatus.value = !historyStatus.value;
  }

  getPlayOddsData([bool status = false]) async {
    if (status) {
      oddStatus.value = true;
    }
    final res = await GameHttp.gamePlayOdds(select.value.id ?? "");
    // LogUtil.w("res____$res");
    if (expoertSwitch.value) {
      playOdds.value = (res.data ?? const GamePlayOddsModel()).copyWith(
          playOdds: [
            (const PlayOdd(name: "跟单计划", code: "-1")),
            ...(res.data?.playOdds ?? [])
          ]);
    } else {
      playOdds.value = (res.data ?? const GamePlayOddsModel());
    }
    defaultWithdrawalData = playOdds.value;

    if (res.data != null && res.data?.playOdds != null) {
      oddStatus.value = false;
      if (leftMenuActive.value == -1) {
        handleLeftMednuActive(
            expoertSwitch.value ? 1 : 0, res.data!.playOdds!.first);
      }
    }
  }

  handleIsuRefresh() async {
    isuRefreshController.forward();
    await getNextIsuData(true);
    isuRefreshController.stop();
  }

  handleCloseVideo() {
    videoUrl.value = "";
    videoStatus.value = false;
    progress.value = 0;
  }

  handleLeftMednuActive(int value, PlayOdd item) {
    if (item.code == "-1") {
      Get.toNamed(AppRoutes.expertPlan,
          parameters: {"data": jsonEncode(select.value.toJson())});
      return;
    }
    playOdds.value = defaultWithdrawalData;
    withdrawalProgress.value = 0;
    leftMenuActive.value = value;
    leftMenuActiveDetail.value = item;
    selectPlay.value = [];
    chasingNumber.value = [];
    handleCloseVideo();
  }

  handleProgress(double value) {
    LogUtil.w("value____$value");
    progress.value = value;
  }

  getGameChipList() async {
    GameHttp.gameChipList().then((res) {
      LogUtil.w(res);
      if (res.data != null) {
        chipsList.value = res.data!
            .toJson()
            .values
            .map((e) => int.tryParse(e ?? '0') ?? 0)
            .toList();
      }
    });
  }

  getUserBalance() async {
    if (GlobalService.to.token.isNotEmpty) {
      await UserHttp.userBalance().then((res) {
        if (res.data != null) {
          userBalanceData.value = res.data!;
        }
      });
    }
  }

  getChatData() async {
    final res = await ChatHttp.chatGetToken(ChatGetTokenParams(roomId: "0"));
    LogUtil.w("resData___${res.data}");
    if (res.data != null) {
      chatData.value = res.data!;
      final _tabs = tabs.value;
      _tabs[1] = res.data?.roomName ?? "";
      LogUtil.w("tabs______${_tabs}");
      tabs.value = _tabs;
      selectRoom.value = res.data!.chatAry?.first ??  ChatAry(
        roomName: res.data?.roomName
      );
    }
  }

  tabListener() {
    Future.delayed(500.milliseconds, () {
      selectTab.value = tabController.index;
    });
  }

  getExpoertSwitch() async {
    final res = await UserHttp.userExpertSwitch(select.value.id ?? "");
    LogUtil.w("专家跟单开关___${res.data}");
    // const isConfiged = gameArr?.some(c => String(c.gameId) === String(id));
    expoertSwitch.value = res.data?.gameArr
            ?.firstWhereOrNull((e) => e.gameId == select.value.id) !=
        null;
  }

  handleWithdrawalStatus() {
    withdrawalStatus.value = !withdrawalStatus.value;
  }

  @override
  void onReady() {
    getData();
    getUserBalance();
    getGameChipList();
    getChatData();
    // GlobalService.to.currentLotteryId.listen((value) {
    //   if (value != select.value.id && value != "") {
    //     getData();
    //     lotteryId = value;
    //   }
    // });
    debounce(selectPlay, (value) {
      selectLength.value = value.length;
    }, time: 0.milliseconds);
    debounce(closeingTimeNumber, (v) {
      if (v <= 0) {
        /// 封盘关闭所有下注弹窗
        if (tabController.index == 0) {
          ToastUtils.closeAll();
        }
      }
    });
    super.onReady();
  }

  @override
  void onClose() {
    print("lottery onClose");
    closingTimer?.cancel();
    deleteTimer?.cancel();
    tabController.removeListener(tabListener);
    super.onClose();
  }

  @override
  void onDetached() {
    print('onDetached called');
  }

  @override
  void onInactive() {
    print('onInative called');
  }

  @override
  void onPaused() {
    print('onPaused called');
  }

  @override
  void onResumed() {
    print('onResumed called');
    // getData(false);
    getNextIsuData(true);
  }

  @override
  void onHidden() {
    closingTimer?.cancel();
    print('onHidden called');
    // TODO: implement onHidden
  }
}
