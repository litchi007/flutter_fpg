import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

/// 连肖

class ConnectSimilarPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const ConnectSimilarPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<ConnectSimilarPlay> createState() => _ConnectSimilarPlayState();
}

class _ConnectSimilarPlayState extends State<ConnectSimilarPlay>
    with TickerProviderStateMixin {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  int selectIndex = 0;
  TabController? tabController;

  listener() {
    setState(() {
      selectIndex = tabController?.index ?? 0;
    });
  }

  @override
  void initState() {
    print("type___ ${widget.type} ${widget.details?.game?.gameType} __ ${[
      'HX',
      'ZX'
    ].contains(widget.type)}");

    if (!['HX', 'ZX'].contains(widget.type)) {
      tabController = TabController(
          length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);
    }

    super.initState();
  }

  @override
  void dispose() {
    tabController?.removeListener(listener);
    tabController?.dispose();
    super.dispose();
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  onChange(List<Play> plays, List<PlayGroup> data) {
    widget.onChange?.call(plays, data);
  }

  @override
  Widget build(BuildContext context) {
    return ['HX', 'ZX'].contains(widget.type)
        ? ConnectSimilarPlayItem(
            data: widget.data?.playGroups?.first,
            details: widget.details,
            onChange: onChange,
            type: widget.type,
          )
        : Column(
            children: [
              TabBar(
                controller: tabController,
                padding: EdgeInsets.zero,
                isScrollable: true,
                labelPadding: EdgeInsets.zero,
                indicatorColor: Colors.transparent,
                tabAlignment: TabAlignment.start,
                indicatorSize: TabBarIndicatorSize.label,
                splashFactory: NoSplash.splashFactory,
                dividerColor: Colors.transparent,
                indicator: BoxDecoration(
                    color: context.theme.cardColor.withOpacity(.2),
                    borderRadius: 4.radius),
                labelStyle: context.textTheme.bodyLarge?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
                unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
                tabs: (widget.data?.playGroups ?? [])
                    .mapIndexed((i, e) => Container(
                        decoration: BoxDecoration(
                            // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                            borderRadius: 4.radius),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 5),
                        child: Text(e.alias ?? "")))
                    .toList(),
                onTap: handleSelectTab,
              ),
              Expanded(
                child: TabBarView(
                    controller: tabController,
                    children: (widget.data?.playGroups ?? [])
                        .mapIndexed((i, e) => ConnectSimilarPlayItem(
                            data: e,
                            type: widget.type,
                            details: widget.details,
                            onChange: onChange))
                        .toList()),
              )
            ],
          );
  }
}

class ConnectSimilarPlayItem extends StatefulWidget {
  final PlayGroup? data;
  final GamePlayOddsModel? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  final String? type;
  const ConnectSimilarPlayItem(
      {super.key, this.data, this.details, this.onChange, this.type});

  @override
  State<ConnectSimilarPlayItem> createState() => _ConnectSimilarPlayItemState();
}

class _ConnectSimilarPlayItemState extends State<ConnectSimilarPlayItem> {
  // List<List<String>>
  List<List<String>> configs = [];
  // 六合彩连尾数据
  List<Play> selectList = [];

  List<PlayGroup> playGroups = [];
  late final isLhc =
      (widget.details?.game?.gameType?.contains("lhc") ?? false) &&
          widget.type == "LW";
  late final isLx =
      (widget.details?.game?.gameType?.contains("lhc") ?? false) &&
          widget.type == "LX";
  handleSelect(Play value) {
    final isHave = selectList.firstWhereOrNull((e) => e.id == value.id);
    if (isHave == null) {
      selectList.add(value);
    } else {
      selectList = selectList.where((e) => e.id != value.id).toList();
    }
    LogUtil.w(selectList);
    LogUtil.w(value);

    // widget.onChange?.call(selectList, );
    handleChange();

    setState(() {});
  }

  List<String>? list(Play item) {
    return ((widget.details?.setting?.zodiacNums ?? [])
        .firstWhereOrNull((e) => e.name == item.alias || e.name == item.name)
        ?.nums);
  }

  handleChange() {
    final genList =
        LotteryConfig.lhcLwGenList(selectList.mapIndexed((i, e) => i).toList());
    final groups = genList.map((e) {
      final alias = e.map((i) {
        return isLx ? selectList[i].alias : selectList[i].alias;
      }).join(",");
      return widget.data!.copyWith(plays: [
        widget.data!.plays!.first.copyWith(name: alias, alias: alias)
      ]);
    }).toList();
    LogUtil.w("genList $isLx $groups");
    playGroups = groups;
    if (widget.type == "ZX") {
      final _groups =
          selectList.map((e) => widget.data!.copyWith(plays: [e])).toList();
      widget.onChange?.call(selectList, _groups);
      playGroups = _groups;
    } else {
      widget.onChange?.call(selectList, groups);
    }
  }

  /// 处理六合彩-连尾
  init() {
    LogUtil.w(widget.details?.game?.gameType);
    if (widget.details?.game?.gameType?.contains("lhc") ?? false) {
      final key1 = LotteryConfig.nums.keys.toList();
      final key2 = LotteryConfig.nums.values
          .map<String>((e) => e['nums'] as String)
          .toList();
      LogUtil.w(key1);
      LogUtil.w(key2);
      configs = [key1, key2];
      setState(() {});
    }
  }

  @override
  void initState() {
    // print("type___ ${widget.type} ${widget.details?.game?.gameType}");
    init();
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      selectList.clear();
      final result = EventUtils.robotRandom(widget.data?.plays?.length ?? 0);
      selectList = result.map((e) => widget.data!.plays![e]).toList();
      handleChange();
      setState(() {});
    }, time: 0.microseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      LogUtil.w("values____$value");
      final alias = value.plays?.first.alias?.split(",");
      // selectList.where((e) {
      //   return e;
      // });
      playGroups = playGroups
          .where((e) => e.plays?.first.name != value.plays?.first.name)
          .toList();
      // 例如删除龙,虎
      // 判断余下的列表里有没有龙虎，没有就删除selectList的
      final allname = playGroups.map((e) => e.plays?.first.name).join(",");
      selectList = selectList.where((e) {
        return allname.contains((e.name ?? ""));
      }).toList();
      LogUtil.w("groups____ name_____ $selectList");
      setState(() {});
    }, time: 0.microseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data?.plays?.length ?? 0,
        itemBuilder: (context, index) {
          final item = isLhc
              ? widget.data?.plays![index].copyWith(name: configs.first[index])
              : widget.data?.plays?[index];
          final keys = configs.first;
          final values = configs.last;
          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: context.theme.dividerColor, width: 1)),
                    gradient:
                        selectList.firstWhereOrNull((e) => e.id == item?.id) !=
                                null
                            ? LinearGradient(colors: [
                                context.customTheme?.selectStart ??
                                    Colors.transparent,
                                context.customTheme?.selectEnd ??
                                    Colors.transparent,
                              ])
                            : null),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          isLhc
                              ? keys[index]
                              : "${(item?.alias?.isEmpty ?? false) ? item?.name : (item?.name?.isEmpty ?? false) ? item?.name : item?.alias}",
                          style: context.textTheme.titleMedium?.copyWith(
                              fontWeight: FontWeight.bold,
                              color: selectList.firstWhereOrNull(
                                          (e) => e.id == item?.id) !=
                                      null
                                  ? context.customTheme?.reverseFontColor
                                  : null),
                        ),
                        // Text("$item"),
                        Text(
                          double.tryParse(item?.odds ?? "0").toString(),
                          style: context.textTheme.titleMedium?.copyWith(
                              color: selectList.firstWhereOrNull(
                                          (e) => e.id == item?.id) !=
                                      null
                                  ? context.customTheme?.reverseFontColor
                                  : null),
                        ).paddingOnly(left: 10),
                      ],
                    ).paddingOnly(left: 5),
                    Row(
                      children: isLhc
                          ? values[index].split(",").map((e) {
                              return Container(
                                margin: const EdgeInsets.only(left: 2),
                                alignment: Alignment.center,
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: LotteryConfig.getBallBg(
                                            item!.copyWith(name: e),
                                            widget.details))),
                                child: Text(e,
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12
                                            // color: context.customTheme
                                            //     ?.reverseFontColor
                                            )),
                              );
                            }).toList()
                          : ((widget.details?.setting?.zodiacNums ?? [])
                                      .firstWhere((e) =>
                                          e.name == item?.alias ||
                                          e.name == item?.name)
                                      .nums ??
                                  [])
                              .map((e) {
                              return Container(
                                margin: const EdgeInsets.only(left: 5),
                                alignment: Alignment.center,
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: LotteryConfig.getBallBg(
                                            item!.copyWith(name: e),
                                            widget.details))),
                                child: Text(e,
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12
                                            // color: context.customTheme
                                            //     ?.reverseFontColor
                                            )),
                              );
                            }).toList(),
                    ).paddingOnly(right: 10)
                  ],
                ).paddingSymmetric(vertical: 10),
              ).onTap(() => handleSelect(item!)),
            ],
          );
        });
  }
}
