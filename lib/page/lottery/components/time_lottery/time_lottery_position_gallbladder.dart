import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

class TimeLotteryPositionGallbladder extends StatefulWidget {
  final PlayOdd? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const TimeLotteryPositionGallbladder({super.key, this.data, this.onChange});

  @override
  State<TimeLotteryPositionGallbladder> createState() =>
      _TimeLotteryPositionGallbladderState();
}

class _TimeLotteryPositionGallbladderState
    extends State<TimeLotteryPositionGallbladder> {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  List<ArrObj> selectArr = [].cast<ArrObj>();
  late final list = (widget.data?.playGroups ?? []).mapIndexed((index, e) {
    return List.generate(
        10,
        (e) =>
            ArrObj(id: "$e--${UuidUtil.to.v4()}", value: "$e", key: "$index"));
  }).toList();

  final tabOptions = [
    TabOptions(
      name: "全部",
      type: "all",
    ),
    TabOptions(
      name: "大",
      type: "big",
    ),
    TabOptions(
      name: "小",
      type: "small",
    ),
    TabOptions(
      name: "奇",
      type: "strange",
    ),
    TabOptions(
      name: "偶",
      type: "even",
    ),
    TabOptions(
      name: "移除",
      type: "remove",
    ),
  ].cast<TabOptions>();

  handleOptions(List<ArrObj> item, String? type) {
    final ids = item.map((e) => e.id).toList();
    switch (type) {
      case "all":
        selectArr.clear();
        selectId.clear();
        selectArr.addAll(item);
        selectId.addAll(item.map((e) => e.id!));
        break;
      case "big":
        final filterArr =
            item.where((e) => (int.tryParse(e.value ?? "0") ?? 0) > 4).toList();
        final filterSelectIds =
            selectId.where((e) => !ids.contains(e)).toList();
        final filterSelectArr =
            selectArr.where((e) => !ids.contains(e.id)).toList();
        selectId = filterArr.map((e) => e.id!).toList()
          ..addAll(filterSelectIds);
        selectArr = filterSelectArr..addAll(filterArr);
        break;
      case "small":
        final filterArr = item
            .where((e) => (int.tryParse(e.value ?? "0") ?? 0) <= 4)
            .toList();
        final filterSelectIds =
            selectId.where((e) => !ids.contains(e)).toList();
        final filterSelectArr =
            selectArr.where((e) => !ids.contains(e.id)).toList();
        selectId = filterArr.map((e) => e.id!).toList()
          ..addAll(filterSelectIds);
        selectArr = filterSelectArr..addAll(filterArr);
        break;
      case "strange":
        final filterArr = item
            .where((e) => (int.tryParse(e.value ?? "0") ?? 0) % 2 != 0)
            .toList();
        final filterSelectIds =
            selectId.where((e) => !ids.contains(e)).toList();
        final filterSelectArr =
            selectArr.where((e) => !ids.contains(e.id)).toList();
        selectId = filterArr.map((e) => e.id!).toList()
          ..addAll(filterSelectIds);
        selectArr = filterSelectArr..addAll(filterArr);
        break;
      case "even":
        final filterArr = item
            .where((e) => (int.tryParse(e.value ?? "0") ?? 0) % 2 == 0)
            .toList();
        final filterSelectIds =
            selectId.where((e) => !ids.contains(e)).toList();
        final filterSelectArr =
            selectArr.where((e) => !ids.contains(e.id)).toList();
        selectId = filterArr.map((e) => e.id!).toList()
          ..addAll(filterSelectIds);
        selectArr = filterSelectArr..addAll(filterArr);
        break;
      case "remove":
        selectId = selectId.where((e) => !ids.contains(e)).toList();
        selectArr = selectArr.where((e) => !ids.contains(e.id)).toList();
        break;
      default:
    }
    setState(() {});
  }

  handleSelectArr(ArrObj item) {
    final index = selectArr.indexWhere((e) => e.id == item.id);
    if (index == -1) {
      selectArr.add(item);
      selectId.add(item.id ?? "");
    } else {
      selectArr.removeAt(index);
      selectId.remove(item.id);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    LogUtil.w(selectArr);
    // 找到
    final groups = selectArr
        .mapIndexed((i, e) => widget.data!.playGroups![i].copyWith(plays: [
              widget.data!.playGroups![i].plays!.first.copyWith(name: e.value)
            ]))
        .toList();
    final plays = [].cast<Play>();
    for (var e in groups) {
      plays.addAll(e.plays ?? []);
    }
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final allArrObj = [].cast<ArrObj>();
      final arrObj = [].cast<ArrObj?>();
      for (var e in list) {
        allArrObj.addAll(e);
      }
      selectArr.clear();
      selectId.clear();
      final result = EventUtils.robotRandom(allArrObj.length);
      final selectArrObj = result.map((e) => allArrObj[e]);
      selectArr.addAll(selectArrObj);
      selectId.addAll(selectArrObj.map((e) => e.id!).toList());
      // LogUtil.w(widget.data?.playGroups);
      handleChange();

      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data?.playGroups?.length ?? 0,
        itemBuilder: (context, index) {
          final item = widget.data?.playGroups?[index];
          return Column(
            children: [
              Text(
                "${item?.alias}定位${double.tryParse(item?.plays?.first.odds ?? "0")}",
                style: context.textTheme.titleMedium?.copyWith(
                    color: context.customTheme?.error,
                    fontWeight: FontWeight.w600),
              ).paddingSymmetric(vertical: 10),
              // Text("${item?.plays}")
              Row(
                children: tabOptions
                    .mapIndexed((i, e1) => Expanded(
                        child: Text("${e1.name}",
                                style: context.textTheme.bodyMedium?.copyWith(
                                    color: context.customTheme?.error,
                                    fontSize: 15))
                            .onTap(() => handleOptions(list[index], e1.type))))
                    .toList(),
              ).paddingOnly(left: 10),
              GridView(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3, childAspectRatio: 1.5),
                children: (list[index])
                    .mapIndexed((i, e) => Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              gradient: selectId.contains(e.id)
                                  ? LinearGradient(colors: [
                                      context.customTheme?.selectStart ??
                                          Colors.transparent,
                                      context.customTheme?.selectEnd ??
                                          Colors.transparent,
                                    ])
                                  : null),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    borderRadius: 30.radius,
                                    color: context.customTheme?.lottery2),
                                child: Text("${e.value} ",
                                    style: context.textTheme.bodyLarge
                                        ?.copyWith(
                                            fontWeight: FontWeight.w600,
                                            color: context.customTheme
                                                ?.reverseFontColor)),
                              ),
                              // Text("--")
                            ],
                          ).onTap(() => handleSelectArr(e)),
                        ))
                    .toList(),
              ),
            ],
          );
        });
  }
}

class TabOptions {
  String? name;
  String? type;

  TabOptions({this.name, this.type});

  TabOptions.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['name'] = name;
    data['type'] = type;
    return data;
  }
}
