import 'dart:convert';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

/// 一字定位
class TimeLottery1PositionPlay extends StatefulWidget {
  final PlayOdd? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const TimeLottery1PositionPlay(
      {super.key, this.onChange, this.data, this.type});

  @override
  State<TimeLottery1PositionPlay> createState() =>
      _TimeLottery1PositionPlayState();
}

class _TimeLottery1PositionPlayState extends State<TimeLottery1PositionPlay>
    with TickerProviderStateMixin {
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);
  int selectIndex = 0;

  List<List<int>> resultList = [];

  late TimeLottery1PositionType? obj = <String, TimeLottery1PositionType>{
    "YZDW": TimeLottery1PositionType(len: 1, arr: [
      Arr(
          key: "万分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "万分位", id: UuidUtil.to.v4())))
    ]), // 一字定位
    "EZDW": TimeLottery1PositionType(len: 2, arr: [
      Arr(
          key: "万分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "万分位", id: UuidUtil.to.v4()))),
      Arr(
          key: "千分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "千分位", id: UuidUtil.to.v4()))),
    ]), // 二字定位
    "SZDW": TimeLottery1PositionType(len: 3, arr: [
      Arr(
          key: "万分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "万分位", id: UuidUtil.to.v4()))),
      Arr(
          key: "千分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "千分位", id: UuidUtil.to.v4()))),
      Arr(
          key: "百分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "百分位", id: UuidUtil.to.v4()))),
    ]), // 三字定位
    "SIZDW": TimeLottery1PositionType(len: 3, arr: [
      Arr(
          key: "万分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "万分位", id: UuidUtil.to.v4()))),
      Arr(
          key: "千分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "千分位", id: UuidUtil.to.v4()))),
      Arr(
          key: "百分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "百分位", id: UuidUtil.to.v4()))),
      Arr(
          key: "十分位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "十分位", id: UuidUtil.to.v4()))),
    ]), // 四字定位
    "BDW": TimeLottery1PositionType(len: 1, arr: [
      Arr(
          key: "不定位",
          select: [],
          value: List.generate(
              10,
              (index) =>
                  ArrObj(value: "$index", key: "不定位", id: UuidUtil.to.v4())))
    ]), // 不定位
  }[widget.type ?? "YZDW"];

  List<ArrObj> selectArr = [].cast<ArrObj>();
  List<PlayGroup> selectGroups = [].cast<PlayGroup>();
  listener() {
    //  LogUtil.w("selectIndex___${tabController.index}");
    if (selectIndex != tabController.index) {
      setState(() {
        selectArr.clear();
      });
      handleParentData();
    }

    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int? index) {
    LogUtil.w(widget.data?.playGroups);
  }

  handleParentData() {
    final play = widget.data!.playGroups![selectIndex].plays!.first;
    final playList =
        selectArr.map<Play>((e) => play.copyWith(alias: e.value)).toList();
    // 组合
    List<PlayGroup> groups = [];
    if (widget.type == "BDW") {
      groups = selectArr
          .map((e) => widget.data!.playGroups![selectIndex].copyWith(
                  name: e.value,
                  plays: [
                    widget.data!.playGroups![selectIndex].plays!.first
                        .copyWith(name: e.value)
                  ]))
          .toList();
      // [
      //   widget.data!.playGroups![selectIndex].copyWith(
      //     name: selectArr.map((e) => e.value).join(",")
      //   )
      // ];
      widget.onChange?.call(playList, groups);
      return;
    }
    final list = obj?.arr
        ?.map<List<int>>((e) =>
            (e.select
                ?.map((q) => int.tryParse(q.value ?? "0") ?? 0)
                .toList()) ??
            [])
        .toList();
    final result = LotteryConfig.generateCombinationsList(list!);
    LogUtil.w(result);
    final sendResult = result.where((e) => e.length == list.length).toList();
    resultList = sendResult;
    groups.addAll(sendResult.map((e) => widget.data!.playGroups![selectIndex]
        .copyWith(plays: [play.copyWith(alias: "", name: e.join(","))])));
    widget.onChange?.call(playList, groups);
  }

  handleChecked(ArrObj item, Arr arr) {
    final index = selectArr.indexWhere((e) => e.id == item.id);
    if (index == -1) {
      selectArr.add(item);
    } else {
      selectArr.removeAt(index);
    }
    obj!.arr ??= obj?.arr?.map((e) {
      if (e.key == arr.key) {
        final isHave = e.select?.firstWhereOrNull((q) => q.id == item.id);
        if (isHave == null && index == -1) {
          e.select?.add(item);
        } else {
          e.select?.remove(item);
        }
      }
      return e;
    }).toList();
    resultList = obj!.arr!
        .map((e) =>
            (e.select ?? []).map((q) => int.parse(q.value ?? '0')).toList())
        .toList();

    setState(() {});
    handleParentData();
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      selectArr.clear();
      obj!.arr = obj?.arr?.map((e) {
        final result = EventUtils.robotRandom(e.value?.length ?? 0, 1);
        e.select = result.map((q) => e.value![q]!).toList();
        selectArr.add(e.value![result.first]!);
        return e;
      }).toList();
      handleParentData();
      setState(() {});
    }, time: 0.microseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      final deleteArr = (value.plays?.first.alias?.split(",") ?? [])
          .map<int>((e) => int.tryParse(e) ?? 0)
          .toList();
      // [2,1,0]
      // LogUtil.w(result)
      resultList = resultList
          .where((e) => jsonEncode(e) != jsonEncode(deleteArr))
          .toList();
      LogUtil.w(resultList);
      for (int i = 0; i < deleteArr.length; i++) {
        bool isDelete = false;
        // 判断每一位是否要被删除
        final deleteItemValueList = resultList
            .where((e) => jsonEncode(e) != jsonEncode(deleteArr))
            .map((e) => e[i]);
        isDelete = !deleteItemValueList.contains(deleteArr[i]);

        // 如果isDelete为真，则删除数组中对应的索引的项
        if (isDelete) {
          obj!.arr = obj?.arr?.mapIndexed((index, e) {
            // e.select =
            if (i == index) {
              final deleteItem = e?.value
                  ?.firstWhereOrNull((w) => w?.value == "${deleteArr[i]}");
              e.select =
                  e.select?.where((q) => q.value != "${deleteArr[i]}").toList();
              selectArr.remove(deleteItem);
            }
            return e;
          }).toList();
        }
      }
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: TabBar(
            controller: tabController,
            padding: EdgeInsets.zero,
            isScrollable: tabController.length > 3 ? true : false,
            labelPadding: EdgeInsets.zero,
            indicatorColor: Colors.transparent,
            tabAlignment: tabController.length <= 3 ? null : TabAlignment.start,
            indicatorSize: TabBarIndicatorSize.tab,
            splashFactory: NoSplash.splashFactory,
            dividerColor: Colors.transparent,
            indicator: BoxDecoration(
                color: context.theme.cardColor.withOpacity(.2),
                borderRadius: 4.radius),
            labelStyle: context.textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
            unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
            tabs: (widget.data?.playGroups ?? [])
                .mapIndexed((i, e) => Container(
                    decoration: BoxDecoration(
                        // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                        borderRadius: 4.radius),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: Text(e.alias ?? "")))
                .toList(),
            onTap: handleSelectTab,
          ),
        ),
        Visibility(
          visible: widget.type != "BDW",
          child: Text(
                  "赔率${num.tryParse(widget.data?.playGroups?.first.plays?.first.odds ?? "0")}",
                  style: context.textTheme.titleLarge?.copyWith(
                      color: context.customTheme?.error,
                      fontWeight: FontWeight.w600))
              .paddingSymmetric(vertical: 10),
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children: List.generate(
                        widget.data?.playGroups?.length ?? 0, (e) => null)
                    .mapIndexed((index, e) => TimeLottery1PositionPlayTabView(
                        data: widget.data,
                        obj: obj,
                        callback: handleChecked,
                        select: selectArr,
                        type: widget.type,
                        name: widget.data?.playGroups?[selectIndex].alias,
                        selectTab: widget.data?.playGroups?[selectIndex]))
                    .toList()))
      ],
    );
  }
}

class TimeLottery1PositionPlayTabView extends StatelessWidget {
  final TimeLottery1PositionType? obj;
  final PlayOdd? data;
  final Function(ArrObj, Arr)? callback;
  final List<ArrObj>? select;
  final String? type;
  final String? name;
  final PlayGroup? selectTab;
  const TimeLottery1PositionPlayTabView(
      {super.key,
      this.obj,
      this.data,
      this.callback,
      this.select,
      this.type,
      this.name,
      this.selectTab});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: obj?.arr?.length,
        itemBuilder: (context, index) {
          final item = obj?.arr?[index];
          return Column(
            children: [
              // 不定位显示
              Visibility(
                visible: type == "BDW",
                child: Text(
                        "赔率${num.tryParse(selectTab?.plays?.first.odds ?? "0")}",
                        style: context.textTheme.titleLarge?.copyWith(
                            color: context.customTheme?.error,
                            fontWeight: FontWeight.w600))
                    .paddingSymmetric(vertical: 10),
              ),
              // Text("$item"),
              Text((item?.key ?? name) ?? "",
                      style: context.textTheme.titleMedium?.copyWith(
                          color: context.customTheme?.error,
                          fontWeight: FontWeight.w600))
                  .paddingSymmetric(vertical: 10),
              GridView(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3, childAspectRatio: 1.4),
                children: (item?.value ?? [])
                    .map((e) => Container(
                          decoration: BoxDecoration(
                              gradient: (select ?? []).firstWhereOrNull(
                                          (item) => e?.id == item.id) !=
                                      null
                                  ? LinearGradient(colors: [
                                      context.customTheme?.selectStart ??
                                          Colors.transparent,
                                      context.customTheme?.selectEnd ??
                                          Colors.transparent,
                                    ])
                                  : null),
                          child: UnconstrainedBox(
                            child: Container(
                                    alignment: Alignment.center,
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        color: context.customTheme?.lottery2,
                                        borderRadius: 30.radius),
                                    child: Text("${e?.value}",
                                        style: context.textTheme.titleMedium
                                            ?.copyWith(
                                                fontWeight: FontWeight.bold,
                                                color: context.customTheme
                                                    ?.reverseFontColor)))
                                .onTap(() => callback?.call(e!, item!)),
                          ),
                        ))
                    .toList(),
              )
            ],
          );
        });
  }
}

class TimeLottery1PositionType {
  int? len;
  List<Arr>? arr;

  TimeLottery1PositionType({this.len, this.arr});

  TimeLottery1PositionType.fromJson(Map<String, dynamic> json) {
    len = json['len'];
    if (json['arr'] != null) {
      arr = <Arr>[];
      json['arr'].forEach((v) {
        arr!.add(Arr.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['len'] = len;
    if (arr != null) {
      data['arr'] = arr!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Arr {
  String? key;
  List<ArrObj?>? value;
  List<ArrObj>? select;
  Arr({this.key, this.value, this.select});

  Arr.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    value = json['value'] != null
        ? (json['value'] as List<dynamic>)
            .map((e) => ArrObj.fromJson(e))
            .toList()
        : null;
    select = json['select'] != null
        ? (json['select'] as List<dynamic>)
            .map((e) => ArrObj.fromJson(e))
            .toList()
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['key'] = key;
    data['value'] = value;
    data['select'] = select;
    return data;
  }
}

class ArrObj {
  String? id;
  String? key;
  String? value;
  List<ArrObj>? ext;
  ArrObj({this.id, this.key, this.value, this.ext});

  @override
  @override
  String toString() => "ArrObj(id: $id, key: $key, value: $value, ext: $ext)";

  ArrObj.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    value = json['value'];
    ext = json['ext'] != null
        ? (json['ext'] as List<dynamic>).map((e) => ArrObj.fromJson(e)).toList()
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['key'] = key;
    data['value'] = value;
    data['ext'] = ext;
    return data;
  }

  ArrObj copyWith({String? id, String? key, String? value, List<ArrObj>? ext}) {
    return ArrObj(
        id: id ?? this.id,
        key: key ?? this.key,
        value: value ?? this.value,
        ext: ext ?? this.ext);
  }
}
