import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/fx.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

class Zx30 extends StatefulWidget {
  final Play? data;
  final PlayGroup? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  const Zx30({super.key, this.data, this.details, this.onChange});

  @override
  State<Zx30> createState() => _Zx30State();
}

class _Zx30State extends State<Zx30> {
  List<FxOptions> list = [
    FxOptions(
      title: "二重号",
      id: "0_${UuidUtil.to.v4()}",
      list: List.generate(10, (index) => index),
      select: [],
    ),
    FxOptions(
      title: "单号",
      id: "1_${UuidUtil.to.v4()}",
      list: List.generate(10, (index) => index),
      select: [],
    ),
  ];

  /// 选择操作
  handleSelect({String? id, int? value, int? type}) {
    final currentList = list;
    final index = list.indexWhere((e) => e.id == id);
    if (type == 1) {
      currentList[index].select = [value!];
    } else {
      if ((currentList[index].select?.length ?? 0) < 2) {
        currentList[index] = currentList[index]
            .copyWith(select: [...(currentList[index].select ?? []), value!]);
      } else {
        currentList[index] = currentList[index].copyWith(
            select: (currentList[index].select ?? [])
                .where((e) => e != value)
                .toList());
      }
    }
    LogUtil.w(currentList);
    setState(() {
      list = currentList;
    });
    handleChange();
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    final currentSelect = list.map((e) => e.select).toList();
    final first = currentSelect.first;
    final end = currentSelect.last;
    if (first?.length == 2 && end?.length == 1) {
      final play = widget.data!
          .copyWith(name: currentSelect.map((e) => e!.join(",")).join(","));
      groups = [
        widget.details!.copyWith(plays: [play])
      ];
    }
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }

      final result = list
          .map((e) => EventUtils.robotRandom(
              e.list?.length ?? 0, e.title == "二重号" ? 2 : 1))
          .toList();
      list = list
          .mapIndexed((i, e) =>
              e.copyWith(select: result[i].map((q) => e.list![q]).toList()))
          .toList();
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);
    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      setState(() {
        list = list.map((e) {
          return e.copyWith(select: []);
        }).toList();
      });
    }, time: 0.microseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: list.mapIndexed((index, item) {
        return Column(
          children: [
            Text(
              item.title ?? "",
              style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error,
              ),
            ).paddingSymmetric(vertical: 10),
            GridView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, childAspectRatio: 2.1),
              children: (item.list ?? [])
                  .mapIndexed((i, p) => Container(
                        decoration: BoxDecoration(
                            gradient: (item.select ?? []).contains(p)
                                ? LinearGradient(colors: [
                                    context.customTheme?.selectStart ??
                                        Colors.transparent,
                                    context.customTheme?.selectEnd ??
                                        Colors.transparent,
                                  ])
                                : null),
                        child: UnconstrainedBox(
                          child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: 30.radius,
                                      color: context.customTheme?.lottery2),
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.center,
                                  child: Text("$p",
                                      style: context.textTheme.bodyMedium
                                          ?.copyWith(
                                              color: context.customTheme
                                                  ?.reverseFontColor)))
                              .onTap(() => handleSelect(
                                  id: item.id, value: p, type: index)),
                        ),
                      ))
                  .toList(),
            )
          ],
        );
      }).toList(),
    );
  }
}
