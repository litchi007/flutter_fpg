import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

class FX extends StatefulWidget {
  final Play? data;
  final PlayGroup? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  const FX({super.key, this.data, this.details, this.onChange});

  @override
  State<FX> createState() => _FXState();
}

class _FXState extends State<FX> {
  List<FxOptions> list = [
    FxOptions(
      title: "第一球(万位)",
      id: "0_${UuidUtil.to.v4()}",
      list: List.generate(10, (index) => index),
      select: [],
    ),
    FxOptions(
      title: "第二球(千位)",
      id: "1_${UuidUtil.to.v4()}",
      list: List.generate(10, (index) => index),
      select: [],
    ),
    FxOptions(
      id: "2_${UuidUtil.to.v4()}",
      title: "第三球(百位)",
      list: List.generate(10, (index) => index),
      select: [],
    ),
    FxOptions(
      id: "3_${UuidUtil.to.v4()}",
      title: "第四球(十位)",
      list: List.generate(10, (index) => index),
      select: [],
    ),
    FxOptions(
      id: "4_${UuidUtil.to.v4()}",
      title: "第五球(个位)",
      list: List.generate(10, (index) => index),
      select: [],
    ),
  ];

  /// 选择操作
  handleSelect({String? id, int? value}) {
    final index = list.indexWhere((e) => e.id == id);
    // list[index].select = [value!];
    list = list.mapIndexed((i, e) {
      if (i == index) {
        final select = e.select;
        if (e.select?.contains(value) ?? false) {
          select?.remove(value);
        } else {
          select?.add(value!);
        }
        return e.copyWith(select: select);
      }
      return e;
    }).toList();
    LogUtil.w("index____${list[index].select}");
    setState(() {});
    handleChange();
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    final data = list.map((e) => e.select!).toList();
    final result = LotteryConfig.gen2Random(data);
    LogUtil.w("select____$result");

    for (var e in result) {
      final play = widget.data?.copyWith(name: e.join(","));
      plays.add(play!);
      groups.add(widget.details!.copyWith(plays: [play]));
    }

    widget.onChange?.call(plays, groups);
    // setState(() {});
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = list
          .map((e) => EventUtils.robotRandom(e.list!.length).first)
          .toList();
      LogUtil.w("result___$result");
      list = result
          .mapIndexed((index, e) =>
              list[index].copyWith(select: [list[index].list![e]]))
          .toList();
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      list = list.map((e) {
        return e.copyWith(select: []);
      }).toList();
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: list.mapIndexed((index, item) {
        return Column(
          children: [
            Text(
              item.title ?? "",
              style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error,
              ),
            ).paddingSymmetric(vertical: 10),
            GridView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, childAspectRatio: 2.1),
              children: (item.list ?? [])
                  .mapIndexed((i, p) => Container(
                        decoration: BoxDecoration(
                            gradient: (item.select ?? []).contains(p)
                                ? LinearGradient(colors: [
                                    context.customTheme?.selectStart ??
                                        Colors.transparent,
                                    context.customTheme?.selectEnd ??
                                        Colors.transparent,
                                  ])
                                : null),
                        child: UnconstrainedBox(
                          child: Column(
                            children: [
                              Container(
                                      decoration: BoxDecoration(
                                          borderRadius: 30.radius,
                                          color: context.customTheme?.lottery2),
                                      width: 30,
                                      height: 30,
                                      alignment: Alignment.center,
                                      child: Text("$p",
                                          style: context.textTheme.bodyMedium
                                              ?.copyWith(
                                                  color: context.customTheme
                                                      ?.reverseFontColor)))
                                  .onTap(() =>
                                      handleSelect(id: item.id, value: p)),
                            ],
                          ),
                        ),
                      ))
                  .toList(),
            )
          ],
        );
      }).toList(),
    );
  }
}

class FxOptions {
  String? title;
  int? pos;
  List<int>? list;
  List<int>? select;
  String? id;

  FxOptions({this.title, this.pos, this.list, this.select, this.id});

  FxOptions.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    pos = json['pos'];
    list = json['list'].cast<int>();
    select = json['select'].cast<int>();
    id = json['id'] as String?;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['title'] = title;
    data['pos'] = pos;
    data['list'] = list;
    data['select'] = select;
    data['id'] = id;
    return data;
  }

  FxOptions copyWith({
    String? title,
    int? pos,
    List<int>? list,
    List<int>? select,
    String? id,
  }) {
    return FxOptions(
        title: title ?? this.title,
        pos: pos ?? this.pos,
        list: list ?? this.list,
        select: select ?? this.select,
        id: id ?? this.id);
  }
}
