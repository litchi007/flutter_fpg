import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class ZX120 extends StatefulWidget {
  final Play? data;
  final PlayGroup? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  const ZX120({super.key, this.data, this.details, this.onChange});

  @override
  State<ZX120> createState() => _ZX120State();
}

class _ZX120State extends State<ZX120> {
  final list = List.generate(10, (index) => index);
  final select = [].cast<int>();
  handleSelect(int value) {
    final isHave = select.contains(value);

    if (isHave) {
      select.remove(value);
    } else {
      if (select.length < 5) {
        select.add(value);
      }
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    LogUtil.w(widget.data);
    LogUtil.w(widget.details);
    for (var e in select) {
      final play = widget.data!.copyWith(name: select.join(","), alias: "$e");

      plays.add(play);
    }
    groups.add(widget.details!
        .copyWith(plays: [widget.data!.copyWith(name: select.join(","))]));

    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = EventUtils.robotRandom(list.length, 5);
      select.clear();
      select.addAll(result.map((e) => list[e]));
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("选号",
            style: context.textTheme.titleMedium?.copyWith(
              color: context.customTheme?.error,
            )).paddingSymmetric(vertical: 10),
        GridView(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, childAspectRatio: 2.1),
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: list
              .map((e) => Container(
                    decoration: BoxDecoration(
                        gradient: select.contains(e)
                            ? LinearGradient(colors: [
                                context.customTheme?.selectStart ??
                                    Colors.transparent,
                                context.customTheme?.selectEnd ??
                                    Colors.transparent,
                              ])
                            : null),
                    child: UnconstrainedBox(
                            child: Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    borderRadius: 30.radius,
                                    color: context.customTheme?.lottery2),
                                alignment: Alignment.center,
                                child: Text("$e",
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            color: context.customTheme
                                                ?.reverseFontColor))))
                        .onTap(() => handleSelect(e)),
                  ))
              .toList(),
        )
      ],
    );
  }
}
