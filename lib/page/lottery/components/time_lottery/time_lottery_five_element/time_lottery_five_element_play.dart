import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/fx.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/zx10.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/zx120.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/zx20.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/zx30.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/zx5.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_five_element/components/zx60.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:get/get.dart';

/// 五行玩法

class TimerLotteryFiveElementPlay extends StatefulWidget {
  final PlayOdd? data;
  final Function(List<Play> list, List<PlayGroup>)? onChange;
  final String? type;
  const TimerLotteryFiveElementPlay(
      {super.key, this.data, this.onChange, this.type});

  @override
  State<TimerLotteryFiveElementPlay> createState() => _FiveElementPlayState();
}

class _FiveElementPlayState extends State<TimerLotteryFiveElementPlay>
    with TickerProviderStateMixin {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  int selectIndex = 0;
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);
  listener() {
    widget.onChange?.call([], []);
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          controller: tabController,
          padding: EdgeInsets.zero,
          isScrollable: true,
          labelPadding: EdgeInsets.zero,
          indicatorColor: Colors.transparent,
          tabAlignment: TabAlignment.start,
          indicatorSize: TabBarIndicatorSize.label,
          splashFactory: NoSplash.splashFactory,
          dividerColor: Colors.transparent,
          indicator: BoxDecoration(
              color: context.theme.cardColor.withOpacity(.2),
              borderRadius: 4.radius),
          labelStyle: context.textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
          tabs: (widget.data?.playGroups ?? [])
              .mapIndexed((i, e) => Container(
                  decoration: BoxDecoration(
                      // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                      borderRadius: 4.radius),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Text(e.alias ?? "")))
              .toList(),
          onTap: handleSelectTab,
        ),
        Expanded(
          child: TabBarView(
              controller: tabController,
              children: (widget.data?.playGroups ?? [])
                  .mapIndexed((i, e) => e.plays?.first.code == "DS"
                      ? FiveElementSingle(
                          data: e.plays?.first,
                          details: e,
                          onChange: widget.onChange,
                        )
                      : FiveElementPlayWidget(
                          data: e.plays?.first,
                          details: e,
                          onChange: widget.onChange,
                        ))
                  .toList()),
        )
      ],
    );
  }
}

class FiveElementSingle extends StatefulWidget {
  final Play? data;
  final PlayGroup? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  const FiveElementSingle({super.key, this.data, this.details, this.onChange});

  @override
  State<FiveElementSingle> createState() => _FiveElementSingleState();
}

class _FiveElementSingleState extends State<FiveElementSingle> {
  late final textController = TextEditingController()..addListener(listener);

  listener() {
    handleChange();
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    textController.text.split(",").forEach((e) {
      final play = widget.data!.copyWith(alias: e);
      plays.add(play);
      groups.add(widget.details!.copyWith(plays: [play]));
    });
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = EventUtils.robotRandom(10, 5);
      textController.text = result.join(",");
      handleChange();
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            "陪率：${double.tryParse(widget.data?.odds ?? '0')}",
            style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error, fontWeight: FontWeight.w600),
          ).paddingSymmetric(vertical: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("玩法提示：手动输入一个5位数的号码组成一注",
                  style: context.textTheme.bodySmall?.copyWith(fontSize: 12)),
              TextField(
                controller: textController,
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: context.theme.cardColor.withOpacity(.5),
                            width: 0.5)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: context.theme.cardColor.withOpacity(.5),
                            width: .5)),
                    border: const OutlineInputBorder()),
              ).marginSymmetric(vertical: 10),
              Text("每一注号码之间请用逗号、空格、换行进行展开",
                  style: context.textTheme.bodySmall?.copyWith(fontSize: 12)),
            ],
          ),
        ],
      ),
    );
  }
}

class FiveElementPlayWidget extends StatelessWidget {
  final Play? data;
  final PlayGroup? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  FiveElementPlayWidget({super.key, this.data, this.details, this.onChange});
  final textMap = {
    "FS": "从万千百十个个选一个号码组成一注",
    "ZX120": "从0～9中任选5个号码组成一注",
    "ZX60": "选1个二重号，3个单号组成一注",
    "ZX30": "选2个二重号,一个单号组成一注",
    "ZX20": "选1个三重号，2个单号组成一注",
    "ZX10": "选1个三重号，1个二重号组成一注",
    "ZX5": "选1个四重号，1个单号组成一注"
  };

  late final childMap = {
    "FS": FX(data: data, details: details, onChange: onChange),
    "ZX120": ZX120(data: data, details: details, onChange: onChange),
    "ZX60": Zx60(data: data, details: details, onChange: onChange),
    "ZX30": Zx30(data: data, details: details, onChange: onChange),
    "ZX20": Zx20(data: data, details: details, onChange: onChange),
    "ZX10": Zx10(data: data, details: details, onChange: onChange),
    "ZX5": Zx5(data: data, details: details, onChange: onChange),
  };
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            "陪率：${double.tryParse(data?.odds ?? "0")}",
            style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error, fontWeight: FontWeight.w600),
          ).paddingSymmetric(vertical: 10),
          Text(
            "玩法提示：${textMap[data?.code]}",
            style: context.textTheme.bodySmall?.copyWith(fontSize: 12),
          ),
          childMap[data?.code] ?? const SizedBox(),
        ],
      ),
    );
  }
}
