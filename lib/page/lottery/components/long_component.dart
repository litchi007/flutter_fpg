import 'dart:math';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/twoSideDragon/two_side_dragon_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LongComponent extends StatefulWidget {
  final String? id;
  final String? selectType;
  const LongComponent({super.key, this.id, this.selectType});

  @override
  State<LongComponent> createState() => _LongComponentState();
}

class _LongComponentState extends State<LongComponent>
    with TickerProviderStateMixin {
  late final parentTabs =
      LotteryConfig.tabConfig.entries.map((e) => {e.key: e.value}).toList();

  int selectIndex = 0;
  late final tabController =
      TabController(length: (parentTabs).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      color: context.customTheme?.navbarBg,
      child: Column(
        children: [
          TabBar(
            controller: tabController,
            padding: EdgeInsets.zero,
            // isScrollable: true,
            labelPadding: EdgeInsets.zero,
            indicatorColor: Colors.transparent,
            // tabAlignment: TabAlignment.start,
            indicatorSize: TabBarIndicatorSize.tab,
            splashFactory: NoSplash.splashFactory,
            dividerColor: Colors.transparent,
            indicator: BoxDecoration(
                color: context.theme.cardColor.withOpacity(.2),
                borderRadius: 4.radius),
            labelStyle: context.textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
            unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
            tabs: parentTabs.map((e) {
              return Text(e.keys.first).paddingSymmetric(vertical: 5);
            }).toList(),
            onTap: handleSelectTab,
          ),

          // Text("$tabs"),
          Expanded(
              child: TabBarView(controller: tabController, children: [
            Road(
              selectType: widget.selectType,
              id: widget.id,
            ),
            Long(
              id: widget.id,
            )
          ]))
        ],
      ),
    );
  }
}

class Road extends StatefulWidget {
  final String? selectType;
  final String? id;
  const Road({super.key, this.selectType, this.id});

  @override
  State<Road> createState() => _RoadState();
}

class _RoadState extends State<Road> with AutomaticKeepAliveClientMixin {
  String select = "";
  int count = 0;

  List resList = [];
  dynamic dataList = {};
  List<Map<String, String>> tabs = [];
  final scrollController = ScrollController();
  bool status = true;
  initTabs() {
    final gameType = widget.selectType;
    final current = gameType == 'pk10' || gameType == 'xyft'
        ? LotteryConfig.roadTypeConfigJssc
        : gameType == 'cqssc' || gameType == 'gd11x5'
            ? LotteryConfig.roadTypeConfigSsc
            : LotteryConfig.roadTypeConfigK3;
    final data = current.entries.map((e) {
      return {e.key: e.value};
    }).toList();
    getData();

    LogUtil.w("entries___${data.first.values.first}");
    setState(() {
      tabs = data;
      select = data.first.values.first;
    });
  }

  getData() async {
    final res = await GameHttp.gameRoad(widget.id ?? '');
    dataList = res.rawData;
    setState(() {
      status = false;
    });
    init();
  }

  /// 拐弯算法
  init() async {
    // final result = res.
    final list = (dataList[select] as List<dynamic>)
        .map<String>((e) => e.join(","))
        .toList();
    if (list.isEmpty) {
      return;
    }
    final maxLength =
        (list).map((e) => e.split(",").length).toList().reduce(max);
    // 先获取有几行几列
    final length = list.length + max<int>(maxLength - 6, 3);
    // 先创建对应的数组空位
    final spaceList = List.generate(length * 6, (index) => "");

    final current = [].cast<List<String>>();
    for (int i = 0; i < (list).length; i++) {
      final t = (list ?? [])[i];
      final tList = t.split(",");
      int findIndex = -1;
      for (int q = 0; q < tList.length; q++) {
        final current = tList[q];
        if (i == 0) {
          // 第一列不用做遍历判断
          if (q < 6) {
            spaceList[i * 6 + q] = current;
          } else {
            /// 11
            /// 17
            spaceList[(i + q - 6) * 6 + 5] = current;
          }
        } else {
          // 第二列开始做遍历判断
          if (findIndex == -1) {
            final rightIndex = spaceList
                .sublist(i * 6, (i + 1) * 6)
                .indexWhere((element) => element.isNotEmpty);
            if (rightIndex != -1) {
              findIndex = rightIndex;
            } else {
              // 避免第一次找不到，再次赋值导致的重复查找
              findIndex = 9999999;
            }
            if (spaceList[(i) * 6 + q].isEmpty) {
              spaceList[(i) * 6 + q] = current;
            }
          } else {
            if (q < findIndex) {
              if (q <= 5) {
                spaceList[(i) * 6 + q] = current;
              } else {
                spaceList[(i + q - 6 + 1) * 6 + 5] = current;
              }
            } else {
              spaceList[(i + q - findIndex + 1) * 6 + findIndex - 1] = current;
            }
          }
          if (q == tList.length - 1) {
            findIndex = -1;
          }
        }
      }
    }

    setState(() {
      count = length * 6;
      resList = spaceList;
    });
  }

  handleTabs(String value) {
    setState(() {
      select = value;
      scrollController.jumpTo(0);
      init();
    });
  }

  @override
  void initState() {
    initTabs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      children: [
        Row(
          children: tabs.map((e) {
            return Container(
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    decoration: BoxDecoration(
                        color: e.values.first == select
                            ? context.customTheme?.leftActive
                            : null,
                        border: e.values.first == select
                            ? null
                            : Border.all(
                                width: 1,
                                color: (context.textTheme.bodyLarge?.color ??
                                        Colors.transparent)
                                    .withOpacity(.3)),
                        borderRadius: 5.radius),
                    child: Text(e.keys.first,
                        style: context.textTheme.bodyMedium?.copyWith(
                            color: e.values.first == select
                                ? context.customTheme?.reverseFontColor
                                : null)))
                .onTap(() => handleTabs(e.values.first));
          }).toList(),
        ).marginSymmetric(vertical: 5),
        Expanded(
          child: CustomLoading(
            status: status,
            child: GridView.builder(
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                itemCount: resList.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 6, //每行三列
                  childAspectRatio: 1, //显示区域宽高相等
                ),
                itemBuilder: (c, i) => Container(
                      decoration: BoxDecoration(
                          border: Border(
                        top: BorderSide(
                            width: 1, color: context.theme.cardColor),
                        left: BorderSide(
                            width: 1, color: context.theme.cardColor),
                      )),
                      child: UnconstrainedBox(
                        child: Container(
                          alignment: Alignment.center,
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            color: resList[i] != ''
                                ? (i ~/ 6) % 2 != 0
                                    ? Colors.red
                                    : Colors.blue
                                : Colors.transparent,
                            borderRadius: 30.radius,
                            // border: Border.all(width: 1, color: context.customTheme?.leftActive ?? Colors.transparent)
                          ),
                          child: Text(
                            "${resList[i]} ",
                            style: TextStyle(
                                fontSize: 12,
                                color: context.customTheme?.reverseFontColor
                                // color:  Colors.red
                                ),
                          ),
                        ),
                      ),
                    )),
          ),
        ),
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class Long extends StatefulWidget {
  final String? selectType;
  final String? id;
  const Long({super.key, this.selectType, this.id});

  @override
  State<Long> createState() => _LongState();
}

class _LongState extends State<Long> with AutomaticKeepAliveClientMixin {
  List<TwoSideDragonModel> list = [];
  bool status = true;
  getData() async {
    setState(() {
      status = true;
    });
    final res = await GameHttp.gameLong(widget.id ?? '');
    LogUtil.w(res.models);
    setState(() {
      list = res.models ?? [];
      status = false;
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return CustomLoading(
      status: status,
      child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            final item = list[index];
            return Container(
              decoration: BoxDecoration(
                  border: Border(
                top: BorderSide(
                    width: .5,
                    color: (context.textTheme.bodyMedium?.color ??
                            Colors.transparent)
                        .withOpacity(.1)),
              )),
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Row(
                children: [
                  Expanded(flex: 2, child: Text(item.playCateName ?? '')),
                  Expanded(flex: 2, child: Text(item.playName ?? '')),
                  Expanded(
                      child: Text("${item.count ?? ''}期",
                          style: context.textTheme.bodyMedium?.copyWith(
                              color: context.customTheme?.leftActive))),
                ],
              ),
            );
          }),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
