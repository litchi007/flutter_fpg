// 两面
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:get/get.dart';

class SecondFacePlay extends StatefulWidget {
  final PlayOdd? data;
  final String? type;
  final Function(List<Play> list, List<PlayGroup?> playGroups)? onChange;
  const SecondFacePlay({super.key, this.data, this.onChange, this.type});

  @override
  State<SecondFacePlay> createState() => _SecondFacePlayState();
}

class _SecondFacePlayState extends State<SecondFacePlay> {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];

  /// 选择的所有列表
  List<PlayGroup?> selectGroupList = [];

  handleSelect(Play item, PlayGroup? data) {
    final isHave = select.firstWhereOrNull((e) => e.id == item.id);
    if (isHave == null) {
      select.add(item);
      selectId.add(item.id ?? "");
    } else {
      select = select.where((e) => e.id != item.id).toList();
      selectId.remove(item.id ?? "");
    }
    List<PlayGroup> groups = [];
    for (var e in select) {
      for (var q in (widget.data?.playGroups ?? [].cast<PlayGroup>())) {
        final isHave = q.plays?.firstWhereOrNull((w) => w.id == e.id);
        if (isHave != null) {
          groups.add(q.copyWith(alias: data?.name, plays: [e.copyWith()]));
          break;
        }
      }
    }
    widget.onChange?.call(select, groups);
    setState(() {});
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }

      /// 随机group，在随机plays
      final result =
          LotteryConfig.robotRandomSelect(widget.data?.playGroups ?? []);
      selectGroupList = [];
      select = result.currentPlay ?? [];
      selectId = (result.currentPlay ?? []).map((e) => e.id ?? '').toList();

      for (var i = 0; i < (result.currentPlay?.length ?? 0); i++) {
        // final group = result.currentGroup?[i];
        final play = result.currentPlay?[i];
        PlayGroup? groups;
        widget.data?.playGroups?.forEach((e) {
          if (e.plays?.contains(play) ?? false) {
            groups = e;
          }
        });
        selectGroupList.add(groups?.copyWith(plays: [play!]));
        // selectGroupList
        // handleSelect(play!, groups!);
      }
      widget.onChange?.call(select, selectGroupList);
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      select.remove(value.plays?.first);
      selectId.remove(value.plays?.first.id);
      // selectList = selectList.where((e) => e.id != value.plays?.first.id).toList();
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data?.playGroups?.length,
        itemBuilder: (context, index) {
          final item = widget.data?.playGroups?[index];
          return Column(
            children: [
              Text(item?.name ?? "",
                      style: context.textTheme.titleMedium
                          ?.copyWith(color: context.customTheme?.error))
                  .paddingSymmetric(vertical: 10),

              /// 分组
              GridView(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, childAspectRatio: 3),
                children: (item?.plays ?? []).mapIndexed((index, e) {
                  return Container(
                      decoration: BoxDecoration(
                          color: context.customTheme?.reverseFontColor,
                          boxShadow: [
                            BoxShadow(
                                color: context.customTheme?.boxShadowBorder ??
                                    Colors.transparent,
                                offset: const Offset(1.0, 1), //阴影y轴偏移量
                                blurRadius: 3, //阴影模糊程度
                                spreadRadius: 0, //阴影扩散程度
                                blurStyle: BlurStyle.outer)
                          ],
                          gradient: selectId.contains(e.id)
                              ? LinearGradient(colors: [
                                  context.customTheme?.selectStart ??
                                      Colors.transparent,
                                  context.customTheme?.selectEnd ??
                                      Colors.transparent,
                                ])
                              : null),
                      alignment: Alignment.center,
                      child: Text.rich(TextSpan(
                          text: e.name ?? "",
                          style: context.textTheme.titleMedium?.copyWith(
                              fontWeight: FontWeight.w600,
                              color: selectId.contains(e.id)
                                  ? context.customTheme?.reverseFontColor
                                  : null),
                          children: [
                            WidgetSpan(
                              child: Text("${double.tryParse(e.odds ?? "0")}",
                                      style: context.textTheme.bodyMedium
                                          ?.copyWith(
                                              color: selectId.contains(e.id)
                                                  ? context.customTheme
                                                      ?.reverseFontColor
                                                  : null))
                                  .paddingOnly(left: 10),
                            )
                          ]))).onTap(() => handleSelect(e, item));
                }).toList(),
              )
            ],
          );
        });
  }
}
