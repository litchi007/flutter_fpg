import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

/// 三军

class Quick3ThreeArmyPlay extends StatefulWidget {
  final GamePlayOddsModel? details;
  final PlayOdd? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const Quick3ThreeArmyPlay(
      {super.key, this.onChange, this.data, this.details});

  @override
  State<Quick3ThreeArmyPlay> createState() => _Quick3ThreeArmyPlayState();
}

class _Quick3ThreeArmyPlayState extends State<Quick3ThreeArmyPlay> {
  List<Play> select = [];
  List<PlayGroup> groups = [];
  handleSelect(Play value, PlayGroup data) {
    final isHave = select.firstWhereOrNull((e) => e.code == value.code);
    if (isHave == null) {
      select.add(value);
      groups.add(data);
    } else {
      select.remove(value);
      groups.remove(data);
    }
    handleChange();
    setState(() {});
  }

  String textTitle() {
    return (widget.data?.playGroups?.map((e) => e.name ?? e.alias ?? "") ?? [])
        .join("、");
  }

  List<Widget> _buildItem() {
    return (widget.data?.playGroups ?? []).map((e) {
      return e.code == "SJDX"
          ? GridView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 2.2,
                crossAxisSpacing: 3,
              ),
              children: (e.plays ?? []).map((ele) {
                return Container(
                  decoration: BoxDecoration(
                      gradient: select.firstWhereOrNull(
                                  (q) => (q.code == ele.code)) !=
                              null
                          ? LinearGradient(colors: [
                              context.customTheme?.selectStart ??
                                  Colors.transparent,
                              context.customTheme?.selectEnd ??
                                  Colors.transparent,
                            ])
                          : null,
                      borderRadius: 10.radius,
                      border: Border.all(color: context.theme.dividerColor)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        LotteryConfig.getQuick3Img(ele.name ?? ele.alias ?? ""),
                        width: 30,
                        height: 30,
                      ),
                      Text(double.tryParse(ele.odds ?? "").toString())
                          .paddingOnly(left: 10)
                    ],
                  ),
                ).onTap(() => handleSelect(ele, e));
              }).toList(),
            )
          : e.code == "DX"
              ? GridView(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 2.2,
                  ),
                  children: (e.plays ?? []).map((ele) {
                    return Container(
                      decoration: BoxDecoration(
                        // color: select.firstWhereOrNull(
                        //             (q) => (q.code == ele.code)) !=
                        //         null
                        //     ? context.customTheme?.error
                        //     : null,
                        gradient: select.firstWhereOrNull(
                                    (q) => (q.code == ele.code)) !=
                                null
                            ? LinearGradient(colors: [
                                context.customTheme?.selectStart ??
                                    Colors.transparent,
                                context.customTheme?.selectEnd ??
                                    Colors.transparent,
                              ])
                            : null,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            ele.name ?? ele.alias ?? "",
                            style: context.textTheme.bodyMedium
                                ?.copyWith(fontWeight: FontWeight.bold),
                          ),
                          Text(double.tryParse(ele.odds ?? "").toString())
                              .paddingOnly(left: 10)
                        ],
                      ),
                    ).onTap(() => handleSelect(ele, e));
                  }).toList(),
                )
              : const SizedBox();
    }).toList();
  }

  handleChange() {
    // widget
    final resultGroup = [].cast<PlayGroup>();
    for (var e in select) {
      for (var q in groups) {
        final isHave = q.plays?.contains(e);
        if (isHave != null) {
          resultGroup.add(q.copyWith(plays: [e]));
          break;
        }
      }
    }
    widget.onChange?.call(select, resultGroup);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result =
          LotteryConfig.robotRandomSelect(widget.data?.playGroups ?? []);
      LogUtil.w(result.currentGroup);
      select.clear();
      select.addAll(result.currentPlay ?? []);
      groups.clear();
      groups.addAll(result.currentGroup ?? []);
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }

      setState(() {
        select = select.where((e) => e.id != value.plays?.first.id).toList();
      });
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(textTitle(),
                  style: context.textTheme.titleMedium?.copyWith(
                      color: context.customTheme?.error,
                      fontWeight: FontWeight.w600))
              .paddingSymmetric(vertical: 10),
          ..._buildItem(),
        ],
      ),
    );
  }
}
