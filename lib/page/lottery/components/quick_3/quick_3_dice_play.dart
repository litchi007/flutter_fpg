import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

/// 围骰

class Quick3DicePlay extends StatefulWidget {
  final GamePlayOddsModel? details;
  final PlayOdd? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const Quick3DicePlay({super.key, this.onChange, this.data, this.details});

  @override
  State<Quick3DicePlay> createState() => _Quick3DicePlayState();
}

class _Quick3DicePlayState extends State<Quick3DicePlay> {
  List<Play> select = [];
  String textTitle() {
    return (widget.data?.playGroups?.map((e) => e.name ?? e.alias ?? "") ?? [])
        .join("、");
  }

  handleSelect(Play value, PlayGroup data) {
    final isHave = select.firstWhereOrNull((e) => e.id == value.id);
    if (isHave == null) {
      select.add(value);
    } else {
      select.remove(value);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<PlayGroup> groups = [];
    for (var e in select) {
      for (var q in (widget.data?.playGroups ?? [].cast<PlayGroup>())) {
        final isHave = q.plays?.firstWhereOrNull((w) => w.id == e.id);
        if (isHave != null) {
          groups.add(q.copyWith(plays: [e]));
          break;
        }
      }
    }

    widget.onChange?.call(select, groups);
  }

  List<Widget> _buildItem() {
    return (widget.data?.playGroups ?? []).map((e) {
      return e.code == "QX"
          ? GridView(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 2.5,
              ),
              children: (e.plays ?? []).map((ele) {
                return Container(
                  decoration: BoxDecoration(
                      gradient: select.firstWhereOrNull(
                                  (q) => (q.code == ele.code)) !=
                              null
                          ? LinearGradient(colors: [
                              context.customTheme?.selectStart ??
                                  Colors.transparent,
                              context.customTheme?.selectEnd ??
                                  Colors.transparent,
                            ])
                          : null),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        ele.name ?? ele.alias ?? "",
                        style: context.textTheme.bodyMedium
                            ?.copyWith(fontWeight: FontWeight.bold),
                      ),
                      Text(double.tryParse(ele.odds ?? "").toString())
                          .paddingOnly(left: 10)
                    ],
                  ),
                ).onTap(() => handleSelect(ele, e));
              }).toList(),
            )
          : GridView(
              padding: const EdgeInsets.all(0),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 2.1,
                crossAxisSpacing: 3,
              ),
              children: (e.plays ?? []).map((ele) {
                return Container(
                  decoration: BoxDecoration(
                      gradient: select.firstWhereOrNull(
                                  (q) => (q.code == ele.code)) !=
                              null
                          ? LinearGradient(colors: [
                              context.customTheme?.selectStart ??
                                  Colors.transparent,
                              context.customTheme?.selectEnd ??
                                  Colors.transparent,
                            ])
                          : null,
                      borderRadius: 10.radius,
                      border: Border.all(color: context.theme.dividerColor)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: (ele.name?.split("_") ?? []).map((e) {
                          return Image.asset(
                            LotteryConfig.getQuick3Img(e),
                            width: 30,
                            height: 30,
                          ).paddingOnly(right: 4, bottom: 5);
                        }).toList(),
                      ),
                      // Image.asset(LotteryConfig.getQuick3Img(ele.name ?? ele.alias ?? ""), width: 30, height: 30,),
                      Text(double.tryParse(ele.odds ?? "").toString())
                          .paddingOnly(left: 10)
                    ],
                  ),
                ).onTap(() => handleSelect(ele, e));
              }).toList(),
            );
    }).toList();
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      select.clear();
      final current =
          LotteryConfig.robotRandomSelect(widget.data?.playGroups ?? []);
      select.addAll(current.currentPlay ?? []);
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);
    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      LogUtil.w("投注清楚____$value");
      setState(() {
        select.remove(value.plays?.first);
      });
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          Text(textTitle(),
                  style: context.textTheme.titleMedium?.copyWith(
                      color: context.customTheme?.error,
                      fontWeight: FontWeight.w600))
              .paddingSymmetric(vertical: 10),
          // ..._buildItem()
          Expanded(
              child: SingleChildScrollView(
            child: Column(
              children: _buildItem(),
            ),
          ))
        ],
      ),
    );
  }
}
