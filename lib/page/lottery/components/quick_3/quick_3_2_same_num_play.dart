import 'dart:convert';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

/// 二同号

class Quick32SameNumPlay extends StatefulWidget {
  final GamePlayOddsModel? details;
  final PlayOdd? data;
  final String? type;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const Quick32SameNumPlay(
      {super.key, this.onChange, this.data, this.details, this.type});

  @override
  State<Quick32SameNumPlay> createState() => _Quick32SameNumPlayState();
}

class _Quick32SameNumPlayState extends State<Quick32SameNumPlay>
    with TickerProviderStateMixin {
  int selectIndex = 0;
  late final tabController = TabController(
      length: (widget.data?.playGroups?.first.plays ?? []).length, vsync: this)
    ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          controller: tabController,
          padding: EdgeInsets.zero,
          // isScrollable: true,
          labelPadding: EdgeInsets.zero,
          indicatorColor: Colors.transparent,
          // tabAlignment: TabAlignment.start,
          indicatorSize: TabBarIndicatorSize.tab,
          splashFactory: NoSplash.splashFactory,
          dividerColor: Colors.transparent,
          indicator: BoxDecoration(
              color: context.theme.cardColor.withOpacity(.2),
              borderRadius: 4.radius),
          labelStyle: context.textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
          tabs: (widget.data?.playGroups?.first.plays ?? [])
              .mapIndexed((i, e) => Container(
                  decoration: BoxDecoration(
                      // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                      borderRadius: 4.radius),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Text(e.name ?? "")))
              .toList(),
          onTap: handleSelectTab,
        ),
        Expanded(
          child: TabBarView(
              controller: tabController,
              children: (widget.data?.playGroups?.first.plays ?? [])
                  .mapIndexed((index, e) {
                return Quicj32SameNumPlayItem(
                  data: e,
                  type: widget.type,
                  index: selectIndex,
                  group: widget.data?.playGroups?.first,
                  onChange: widget.onChange,
                );
              }).toList()),
        )
        // Text(
        //   "赔率：${widget.data?.}",
        //   style: context.textTheme.titleMedium?.copyWith(
        //       color: context.customTheme?.error,
        //       fontWeight: FontWeight.w600),
        // ).paddingSymmetric(vertical: 10),
      ],
    );
  }
}

class Quicj32SameNumPlayItem extends StatefulWidget {
  final String? type;
  final Play? data;
  final int? index;
  final Function(List<Play> plays, List<PlayGroup> data)? onChange;
  final PlayGroup? group;
  const Quicj32SameNumPlayItem(
      {super.key, this.data, this.type, this.index, this.onChange, this.group});

  @override
  State<Quicj32SameNumPlayItem> createState() => _Quicj32SameNumPlayItemState();
}

class _Quicj32SameNumPlayItemState extends State<Quicj32SameNumPlayItem> {
  late final List<ArrObj> list = (LotteryConfig.rthTabs).map((e) {
    return ArrObj(
      id: UuidUtil.to.v4(),
      key: e,
      ext: List.generate(
          6,
          (index) => ArrObj(
              id: UuidUtil.to.v4(),
              key: "$index",
              value: "${index + 1}")).toList(),
    );
  }).toList();

  List<ArrObj> select = [];
  List<List<int>> resultList = [];
  handleSelect(ArrObj value) {
    final isHave = select.firstWhereOrNull((e) => e.value == value.value);
    if (isHave == null) {
      select.add(value);
    } else {
      select.remove(value);
    }
    handleChange();

    setState(() {});
  }

  handleChange() {
    final plays = select.map((e) => Play(name: "$e", alias: "$e")).toList();
    List<PlayGroup> groups = [];
    final genParams = list.map((e) {
      final resultList = [].cast<int>();
      e.ext?.forEach((e) {
        final isHave = select.firstWhereOrNull((q) => q.id == e.id);
        if (isHave != null) {
          resultList.add(int.parse(isHave.value ?? '0'));
        }
      });
      return resultList;
    }).toList();
    LogUtil.w("resultList___$genParams");
    List<List<int>> result = [];
    if (widget.group?.code == "SBTH") {
      result = LotteryConfig.threeBthGenRandom(genParams.first);
    } else if (widget.group?.code == "RTH") {
      result = LotteryConfig.generateCombinationsList(genParams);
    } else {
      // result =
      for (var e in select) {
        result.add([int.parse(e.value ?? "0")]);
      }
    }

    LogUtil.w("result___${widget.data}");
    resultList = result;
    // list.map(toElement)
    // final groups = plays.map((e) => widget.group!.copyWith(
    //   plays: [e]
    // )).toList();
    // TODO: 投注号码重复
    for (int i = 0; i < result.length; i++) {
      groups.add(widget.group!.copyWith(alias: widget.data?.name, plays: [
        widget.data!
            .copyWith(name: result[i].join(","), alias: result[i].join(","))
      ]));
    }
    // 猜码，特殊处理
    if (widget.type == "CM") {
      final cmList = select.map((e) => int.parse(e.value ?? "0")).toList();
      LogUtil.w("222222");
      switch (widget.index) {
        case 0:
          if (select.length < 4) {
            groups.clear();
            plays.clear();
          } else {
            final result = LotteryConfig.combineArray(cmList, 4);
            groups = result.map((e) {
              return widget.group!
                  .copyWith(plays: [widget.data!.copyWith(name: e.join(","))]);
            }).toList();
          }
          break;
        case 1:
          if (select.length < 4) {
            groups.clear();
            plays.clear();
          } else {
            final result = LotteryConfig.combineArray(cmList, 4);
            LogUtil.w("result______$result");
            groups = (result.map((e) {
              return widget.group!
                  .copyWith(plays: [widget.data!.copyWith(name: e.join(","))]);
            }).toList());
          }
          break;
        case 2:
          if (select.length < 5) {
            groups.clear();
            plays.clear();
          } else {
            final result = LotteryConfig.combineArray(cmList, 5);
            LogUtil.w("result______$result");
            groups = (result.map((e) {
              return widget.group!
                  .copyWith(plays: [widget.data!.copyWith(name: e.join(","))]);
            }).toList());
          }
          break;
        default:
          break;
      }
      // groups.add();
    }

    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final allList = [].cast<ArrObj>();
      if (!['CBC', "CM"].contains(widget.type)) {
        for (var e in list) {
          allList.addAll(e.ext ?? []);
        }
      } else {
        allList.addAll(list[0].ext ?? []);
      }

      /// 上下不能选择相同的号码
      final result1 = EventUtils.robotRandom(
          list[0].ext?.length ?? 0, widget.group?.code == "SBTH" ? 3 : 2);
      final resultList1 =
          list[1].ext?.whereIndexed((e, i) => !result1.contains(i)).toList();
      final result2 = widget.group?.code == "SBTH"
          ? []
          : EventUtils.robotRandom(resultList1?.length ?? 0, 1);
      final selectList2 = result2.map((e) => resultList1![e]).toList();
      select.clear();
      select.addAll(result1.map((e) => allList[e]).toList());
      LogUtil.w(list);

      /// 猜不出，猜码只有一个列表，不用两次添加
      if (!['CBC', "CM"].contains(widget.type)) {
        select.addAll(selectList2);
      }
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      final deleteArr = value.plays!.first.alias!
          .split(",")
          .map((e) => int.parse(e))
          .toList();
      resultList = resultList
          .where((e) => jsonEncode(e) != jsonEncode(deleteArr))
          .toList();
      for (int i = 0; i < deleteArr.length; i++) {
        bool isDelete = false;
        // 判断每一位是否要被删除
        final deleteItemValueList = resultList
            .where((e) => jsonEncode(e) != jsonEncode(deleteArr))
            .map((e) => e[i]);
        isDelete = !deleteItemValueList.contains(deleteArr[i]);

        // 如果isDelete为真，则删除数组中对应的索引的项
        if (isDelete) {
          final deleteItem = list[i]
              .ext
              ?.firstWhereOrNull((e) => e.value == '${deleteArr[i]}');
          select.remove(deleteItem);
        }
      }
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            "赔率：${double.tryParse(widget.data?.odds ?? "0")}",
            style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error, fontWeight: FontWeight.w600),
          ).paddingSymmetric(vertical: 10),
          // Text(widget.type ?? ""),
          // 单选
          Visibility(
              visible: widget.data?.code == "DX",
              child: Column(
                children: (widget.type == "RTH"
                        ? LotteryConfig.rthTabs
                        : LotteryConfig.rth3Tabs)
                    .mapIndexed((index, e) {
                  return Column(
                    children: [
                      Text(e,
                          style: context.textTheme.bodyMedium?.copyWith(
                            color: context.customTheme?.error,
                            fontWeight: FontWeight.w600,
                          )).paddingOnly(bottom: 10),
                      GridView(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3, childAspectRatio: 2.2),
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        children: (list[index].ext ?? [])
                            .map((ele) => Container(
                                  decoration: BoxDecoration(
                                      gradient: select.firstWhereOrNull(
                                                  (e) => e.id == ele.id) !=
                                              null
                                          ? LinearGradient(colors: [
                                              context.customTheme
                                                      ?.selectStart ??
                                                  Colors.transparent,
                                              context.customTheme?.selectEnd ??
                                                  Colors.transparent,
                                            ])
                                          : null),
                                  child: Image.asset(LotteryConfig.getQuick3Img(
                                          ele.value ?? ""))
                                      .onTap(() => handleSelect(ele)),
                                ))
                            .toList(),
                      )
                    ],
                  );
                }).toList(),
              )),
          Visibility(
              // 猜不出，猜码
              visible: ['CBC', "CM"].contains(widget.type),
              child: Column(
                children: [
                  Text(widget.data?.name ?? "",
                      style: context.textTheme.bodyMedium?.copyWith(
                        color: context.customTheme?.error,
                        fontWeight: FontWeight.w600,
                      )).paddingOnly(bottom: 10),
                  GridView(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3, childAspectRatio: 2.2),
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: (list[0].ext ?? [])
                        .map((ele) => Container(
                              decoration: BoxDecoration(
                                  // color: select.firstWhereOrNull(
                                  //             (e) => e.id == ele.id) !=
                                  //         null
                                  //     ? context.customTheme?.error
                                  //     : null
                                  gradient: select.firstWhereOrNull(
                                              (e) => e.id == ele.id) !=
                                          null
                                      ? LinearGradient(colors: [
                                          context.customTheme?.selectStart ??
                                              Colors.transparent,
                                          context.customTheme?.selectEnd ??
                                              Colors.transparent,
                                        ])
                                      : null),
                              child: Image.asset(LotteryConfig.getQuick3Img(
                                      ele.value ?? ""))
                                  .onTap(() => handleSelect(ele)),
                            ))
                        .toList(),
                  )
                ],
              )),
          Visibility(visible: widget.data?.code == "TX", child: Text("通选--"))
        ],
      ),
    );
  }
}
