import 'package:flutter/material.dart';

/// 不定位

class PcEggsNoPositionPlay extends StatefulWidget {
  const PcEggsNoPositionPlay({super.key});

  @override
  State<PcEggsNoPositionPlay> createState() => _PcEggsNoPositionPlayState();
}

class _PcEggsNoPositionPlayState extends State<PcEggsNoPositionPlay> {
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
