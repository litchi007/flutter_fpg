import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

class ElevenOutOf5Play extends StatefulWidget {
  final GamePlayOddsModel? details;
  final PlayOdd? data;
  final String? type;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const ElevenOutOf5Play(
      {super.key, this.onChange, this.data, this.details, this.type});

  @override
  State<ElevenOutOf5Play> createState() => _ElevenOutOf5PlayState();
}

class _ElevenOutOf5PlayState extends State<ElevenOutOf5Play>
    with TickerProviderStateMixin {
  int selectIndex = 0;
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void initState() {
    LogUtil.w(widget.data?.playGroups);

    super.initState();
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          controller: tabController,
          padding: EdgeInsets.zero,
          isScrollable: true,
          labelPadding: EdgeInsets.zero,
          indicatorColor: Colors.transparent,
          tabAlignment: TabAlignment.start,
          indicatorSize: TabBarIndicatorSize.tab,
          splashFactory: NoSplash.splashFactory,
          dividerColor: Colors.transparent,
          indicator: BoxDecoration(
              color: context.theme.cardColor.withOpacity(.2),
              borderRadius: 4.radius),
          labelStyle: context.textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
          tabs: (widget.data?.playGroups ?? [])
              .mapIndexed((i, e) => Container(
                  decoration: BoxDecoration(
                      // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                      borderRadius: 4.radius),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Text(e.alias ?? "")))
              .toList(),
          onTap: handleSelectTab,
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children: (widget.data?.playGroups ?? [])
                    .mapIndexed((index, ele) => ElevenOutOf5PlayItem(
                          data: ele,
                          type: widget.type,
                          onChange: widget.onChange,
                        ))
                    .toList()))
      ],
    );
  }
}

class ElevenOutOf5PlayItem extends StatefulWidget {
  final PlayGroup? data;
  final String? type;
  final Function(List<Play> plays, List<PlayGroup> data)? onChange;
  const ElevenOutOf5PlayItem({super.key, this.data, this.type, this.onChange});

  @override
  State<ElevenOutOf5PlayItem> createState() => _ElevenOutOf5PlayItemState();
}

class _ElevenOutOf5PlayItemState extends State<ElevenOutOf5PlayItem> {
  List<ArrObj> select = [];
  final List<ArrObj> list = List.generate(
      11,
      (index) =>
          ArrObj(id: UuidUtil.to.v4(), key: "$index", value: "${index + 1}"));

  handleSelect(ArrObj value) {
    final isHave = select.firstWhereOrNull((e) => e.id == value.id);
    if (isHave == null) {
      select.add(value);
    } else {
      select.remove(value);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    // final plays = select
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    for (var e in select) {
      final play = Play(name: "$e", alias: "$e");
      plays.add(play);
      groups.add(widget.data!.copyWith(plays: [play]));
    }

    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = EventUtils.robotRandom(list.length);
      select.clear();
      select.addAll(result.map((e) => list[e]).toList());
      handleChange();

      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          widget.data?.alias ?? "",
          style: context.textTheme.titleMedium?.copyWith(
              color: context.customTheme?.error, fontWeight: FontWeight.w600),
        ).paddingSymmetric(vertical: 10),
        Visibility(
            visible: widget.type == "CBC",
            child: Text("赔率：${widget.data?.plays?.first.odds ?? ""}",
                    style: context.textTheme.titleMedium?.copyWith(
                        color: context.customTheme?.error,
                        fontWeight: FontWeight.w600))
                .paddingSymmetric(vertical: 10)),
        GridView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, childAspectRatio: 1.2),
          children: list
              .mapIndexed((index, item) => Container(
                    color:
                        select.firstWhereOrNull((e) => e.id == item.id) != null
                            ? context.customTheme?.error
                            : null,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              color: context.customTheme?.lottery2,
                              borderRadius: 20.radius),
                          child: Text(item.value ?? ""),
                        ),
                        Text(double.tryParse(
                                    widget.data?.plays?.first.odds ?? "")
                                .toString())
                            .paddingOnly(top: 10)
                      ],
                    ),
                  ).onTap(() => handleSelect(item)))
              .toList(),
        )
      ],
    );
  }
}
