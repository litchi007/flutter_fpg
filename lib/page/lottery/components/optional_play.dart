import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';

// 任选
class OptionalPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const OptionalPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<OptionalPlay> createState() => _OptionalPlayState();
}

class _OptionalPlayState extends State<OptionalPlay>
    with TickerProviderStateMixin {
  int selectIndex = 0;
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: TabBar(
            controller: tabController,
            padding: EdgeInsets.zero,
            isScrollable: true,
            labelPadding: EdgeInsets.zero,
            indicatorColor: Colors.transparent,
            tabAlignment: TabAlignment.start,
            indicatorSize: TabBarIndicatorSize.tab,
            splashFactory: NoSplash.splashFactory,
            dividerColor: Colors.transparent,
            indicator: BoxDecoration(
                color: context.theme.cardColor.withOpacity(.2),
                borderRadius: 4.radius),
            labelStyle: context.textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
            unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
            tabs: (widget.data?.playGroups ?? [])
                .mapIndexed((i, e) => Container(
                    decoration: BoxDecoration(
                        // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                        borderRadius: 4.radius),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: Text(e.alias ?? '')))
                .toList(),
            onTap: handleSelectTab,
          ),
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children: (widget.data?.playGroups ?? [])
                    .map((e) =>
                        OptionalPlayItem(data: e, onChange: widget.onChange))
                    .toList()))
      ],
    );
  }
}

class OptionalPlayItem extends StatefulWidget {
  final PlayGroup? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;

  const OptionalPlayItem({super.key, this.data, this.onChange});

  @override
  State<OptionalPlayItem> createState() => _OptionalPlayItemState();
}

class _OptionalPlayItemState extends State<OptionalPlayItem> {
  late final List<ArrObj> list = LotteryConfig.optionalTabs
      .mapIndexed((index, e) => ArrObj(
          id: UuidUtil.to.v4(),
          key: "$index",
          value: e,
          ext: List.generate(
              11,
              (i) =>
                  ArrObj(id: UuidUtil.to.v4(), key: "$i", value: "${i + 1}"))))
      .toList();

  List<ArrObj> select = [];

  handleSelect(ArrObj value) {
    final isHave = select.firstOrNullWhere((e) => e.value == value.value);
    if (isHave == null) {
      select.add(value);
    } else {
      select.remove(value);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    LogUtil.w(widget.data);
    for (var e in select) {
      plays.add(Play(name: "${e.value}", alias: "${e.value}"));
      groups.add(widget.data!.copyWith(
          plays: widget.data?.plays
              ?.map((q) => q.copyWith(alias: e.value))
              .toList()));
    }
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      List<ArrObj> allList = [];
      for (var e in list) {
        allList.addAll(e.ext ?? []);
      }
      final result = EventUtils.robotRandom(allList.length);
      select.clear();
      select.addAll(result.map((e) => allList[e]));
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          final item = list[index];
          return Column(
            children: [
              Text(
                item.value ?? "",
                style: context.textTheme.titleMedium?.copyWith(
                    color: context.customTheme?.error,
                    fontWeight: FontWeight.w600),
              ).paddingSymmetric(vertical: 10),
              GridView(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3, childAspectRatio: 1.8),
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                children: (item.ext ?? [])
                    .map((q) => Container(
                          decoration: BoxDecoration(
                              gradient: select.firstOrNullWhere(
                                          (e) => e.id == q.id) !=
                                      null
                                  ? LinearGradient(colors: [
                                      context.customTheme?.selectStart ??
                                          Colors.transparent,
                                      context.customTheme?.selectEnd ??
                                          Colors.transparent,
                                    ])
                                  : null),
                          child: UnconstrainedBox(
                            child: Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                  color: context.customTheme?.lottery2,
                                  borderRadius: 20.radius),
                              alignment: Alignment.center,
                              child: Text("${q.value}"),
                            ),
                          ),
                        ).onTap(() => handleSelect(q)))
                    .toList(),
              )
            ],
          );
        });
  }
}
