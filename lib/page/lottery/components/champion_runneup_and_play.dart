import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

// 冠亚和
class ChampionRunneupAndPlay extends StatefulWidget {
  final PlayOdd? data;
  final Function(List<Play> list)? onChange;
  const ChampionRunneupAndPlay({super.key, this.data, this.onChange});

  @override
  State<ChampionRunneupAndPlay> createState() => _ChampionRunneupAndPlayState();
}

class _ChampionRunneupAndPlayState extends State<ChampionRunneupAndPlay> {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  handleSelect(Play item) {
    final isHave = select.firstWhereOrNull((e) => e.id == item.id);
    if (isHave == null) {
      select.add(item);
      selectId.add(item.id ?? "");
    } else {
      select = select.where((e) => e.id != item.id).toList();
      selectId.remove(item.id ?? "");
    }
    widget.onChange?.call(select);
    setState(() {});
  }

  @override
  void initState() {
    LogUtil.w(widget.data);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data?.playGroups?.length ?? 0,
        itemBuilder: (context, index) {
          final item = widget.data?.playGroups?[index];
          return Column(
            children: [
              Text(
                "${item?.alias}",
                style: context.textTheme.titleMedium?.copyWith(
                    color: context.customTheme?.error,
                    fontWeight: FontWeight.w600),
              ).paddingSymmetric(vertical: 10),
              // Text("${item?.plays}")
              item?.plays != null
                  ? GridView(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2, childAspectRatio: 3),
                      children: ((item?.plays) ?? [])
                          .map((e) => Container(
                                color: selectId.contains(e.id)
                                    ? context.customTheme?.error
                                    : null,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(e.name ?? "",
                                        style: context.textTheme.bodyLarge
                                            ?.copyWith(
                                                fontWeight: FontWeight.w600)),
                                    Text(double.tryParse(e.odds ?? "0")
                                            .toString())
                                        .paddingOnly(left: 10),
                                  ],
                                ).onTap(() => handleSelect(e)),
                              ))
                          .toList(),
                    )
                  : const SizedBox(),
            ],
          );
        });
  }
}
