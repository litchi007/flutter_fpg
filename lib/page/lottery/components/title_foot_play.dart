import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class TitleFootPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const TitleFootPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<TitleFootPlay> createState() => _TitleFootPlayState();
}

class _TitleFootPlayState extends State<TitleFootPlay> {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  List<PlayGroup> groupsList = [];
  final specialName = ["ZM"];
  handleSelect(Play item, PlayGroup data) {
    final isHave = select.firstWhereOrNull((e) => e.id == item.id);

    if (isHave == null) {
      select.add(item);
      selectId.add(item.id ?? "");
    } else {
      // select = select.where((e) => e.id != item.id).toList();
      selectId.remove(item.id ?? "");
      // 7125374 7125373
      select.remove(item);
    }
    handleChange();
    // widget.onChange?.call(select, groupsList);
    setState(() {});
  }

  List<String> list(Play item) {
    if (LotteryConfig.nums[item.name ?? ""] != null) {
      return (LotteryConfig.nums[item.name ?? ""]?['nums'] as String)
          .split(",");
    } else {
      return ((widget.details?.setting?.zodiacNums ?? [])
              .firstWhereOrNull((e) => e.name == item.name)
              ?.nums ??
          []);
    }
  }

  handleChange() {
    List<PlayGroup> groups = [];
    LogUtil.w(widget.data);
    for (var e in select) {
      for (var q in (widget.data?.playGroups ?? [].cast<PlayGroup>())) {
        final isHave = q.plays?.firstWhereOrNull((w) => w.id == e.id);
        if (isHave != null) {
          groups.add(q.copyWith(plays: [e]));
          break;
        }
      }
    }
    widget.onChange?.call(select, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      List<Play> playList = [];
      widget.data?.playGroups?.forEach((e) {
        playList.addAll(e.plays ?? []);
      });
      select.clear();
      selectId.clear();
      groupsList.clear();
      final result =
          LotteryConfig.robotRandomSelect(widget.data?.playGroups ?? []);
      // final result = EventUtils.robotRandom(playList.length);
      final plays = (result.currentPlay ?? []);
      select.addAll(plays);
      selectId.addAll(plays.map((e) => e.id!));
      handleChange();
      // groupsList.addAll(groups);
      // LogUtil.w(plays);
      // widget.onChange?.call(plays, groups);
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      final play = value.plays?.first.id;
      select.removeWhere((e) => e.id == play);
      selectId.remove(play);
      setState(() {});
    }, time: 0.microseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data?.playGroups?.length ?? 0,
        itemBuilder: (context, index) {
          final item = widget.data?.playGroups?[index];
          return Column(
            children: [
              Text(
                "${item?.alias}",
                style: context.textTheme.titleMedium?.copyWith(
                    color: context.customTheme?.error,
                    fontWeight: FontWeight.w600),
              ).paddingSymmetric(vertical: 10),
              // Text("${item?.plays}")
              item?.plays != null && item?.alias == "特码头数"
                  ? GridView(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2, childAspectRatio: 3),
                      children: ((item?.plays) ?? [])
                          .map((e) => Container(
                                decoration: BoxDecoration(
                                    gradient: selectId.contains(e.id)
                                        ? LinearGradient(colors: [
                                            context.customTheme?.selectStart ??
                                                Colors.transparent,
                                            context.customTheme?.selectEnd ??
                                                Colors.transparent,
                                          ])
                                        : null),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      width: double.tryParse(e.name ?? "")
                                                  .runtimeType ==
                                              double
                                          ? 40
                                          : null,
                                      height: 40,
                                      decoration: BoxDecoration(
                                          image: specialName
                                                  .contains(widget.type)
                                              ? DecorationImage(
                                                  image:
                                                      LotteryConfig.getBallBg(
                                                          e, widget.details),
                                                  fit: BoxFit.cover)
                                              : null,
                                          borderRadius: 40.radius,
                                          color: int.tryParse(e.code ?? "")
                                                      ?.runtimeType ==
                                                  int
                                              ? context.customTheme?.lottery2
                                              : null),
                                      child: Text(e.name ?? "",
                                          style: context.textTheme.bodyLarge
                                              ?.copyWith(
                                                  fontWeight: FontWeight.w600,
                                                  color: selectId.contains(e.id)
                                                      ? context.customTheme
                                                          ?.reverseFontColor
                                                      : null)),
                                    ),
                                    Text(
                                      double.tryParse(e.odds ?? "0").toString(),
                                      style: context.textTheme.bodyMedium
                                          ?.copyWith(
                                              color: selectId.contains(e.id)
                                                  ? context.customTheme
                                                      ?.reverseFontColor
                                                  : null),
                                    ).paddingOnly(left: 10),
                                  ],
                                ).onTap(() => handleSelect(e, item!)),
                              ))
                          .toList(),
                    )
                  : ListView(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      children: (item?.plays ?? []).mapIndexed((i, e) {
                        return Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      color: context.theme.dividerColor,
                                      width: 1)),
                              gradient: select.firstWhereOrNull(
                                          (e1) => e1.name == e.name) !=
                                      null
                                  ? LinearGradient(colors: [
                                      context.customTheme?.selectStart ??
                                          Colors.transparent,
                                      context.customTheme?.selectEnd ??
                                          Colors.transparent,
                                    ])
                                  : null),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    e.name ?? "",
                                    style: context.textTheme.titleMedium
                                        ?.copyWith(
                                            fontWeight: FontWeight.bold,
                                            color: selectId.contains(e.id)
                                                ? context.customTheme
                                                    ?.reverseFontColor
                                                : null),
                                  ),
                                  Text(
                                    double.tryParse(e.odds ?? "0").toString(),
                                    style: context.textTheme.bodyMedium
                                        ?.copyWith(
                                            color: selectId.contains(e.id)
                                                ? context.customTheme
                                                    ?.reverseFontColor
                                                : null),
                                  ).paddingOnly(left: 10),
                                ],
                              ).paddingOnly(left: 5),
                              Expanded(
                                child: Wrap(
                                  alignment: WrapAlignment.end,
                                  children: list(e).map((e1) {
                                    return Container(
                                      margin: const EdgeInsets.only(left: 5),
                                      alignment: Alignment.center,
                                      width: 30,
                                      height: 30,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: LotteryConfig.getBallBg(
                                                  Play(name: e1),
                                                  widget.details))),
                                      child: Text(e1,
                                          style: context.textTheme.bodyMedium
                                              ?.copyWith(
                                            fontWeight: FontWeight.bold,
                                            // color: context.customTheme
                                            //     ?.reverseFontColor
                                          )),
                                    ).onTap(() => handleSelect(e, item!));
                                  }).toList(),
                                ).paddingOnly(left: 10),
                              )
                            ],
                          ).paddingSymmetric(vertical: 10),
                        ).onTap(() => handleSelect(e, item!));
                      }).toList(),
                    ),
            ],
          );
        });
  }
}
