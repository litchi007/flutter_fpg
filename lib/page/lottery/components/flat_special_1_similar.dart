import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

/// 平特一肖

class FlatSpecial1Similar extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const FlatSpecial1Similar(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<FlatSpecial1Similar> createState() => _FlatSpecial1SimilarState();
}

class _FlatSpecial1SimilarState extends State<FlatSpecial1Similar> {
  List<Play> selectList = [];

  handleSelect(Play value) {
    final isHave = selectList.firstWhereOrNull((e) => e.id == value.id);
    if (isHave == null) {
      selectList.add(value);
    } else {
      selectList.remove(value);
    }
    widget.onChange?.call(
        selectList,
        selectList
            .map((e) => widget.data!.playGroups!.first.copyWith(plays: [e]))
            .toList());
    setState(() {});
  }

  List<String> list(Play item) {
    // LogUtil.w("itemname___${widget.data}");
    if (["WS", "LW"].contains(widget.type)) {
      return (LotteryConfig.nums[item.name ?? ""]?['nums'] as String)
          .split(",");
    }
    return ((widget.details?.setting?.zodiacNums ?? [])
            .firstWhere((e) => e.name == item.name)
            .nums ??
        []);
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    for (var e in selectList) {
      final play = e.copyWith(alias: list(e).join(","));
      plays.add(play);
      groups.add(widget.data!.playGroups!.first.copyWith(plays: [play]));
    }

    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = EventUtils.robotRandom(
          widget.data?.playGroups?.first.plays?.length ?? 0,
          min(3, widget.data?.playGroups?.first.plays?.length ?? 0));
      LogUtil.w(result);
      selectList.clear();
      final selects =
          result.map((e) => widget.data!.playGroups!.first.plays![e]);
      // .copyWith(
      //   alias: list(widget.data!.playGroups!.first.plays![e]).join(",")
      // )).toList();
      selectList.addAll(selects);
      handleChange();
      // widget.onChange?.call(selects, selects.map((e) => widget.data!.playGroups!.first.copyWith(
      //   plays: [e]
      // )).toList() );
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      final isHave =
          selectList.firstWhereOrNull((e) => e.id == value.plays?.first.id);
      selectList.remove(isHave);
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data?.playGroups?.first.plays?.length ?? 0,
        itemBuilder: (context, index) {
          final item = widget.data?.playGroups?.first.plays?[index];
          return Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: context.theme.dividerColor, width: 1)),
                    gradient:
                        selectList.firstWhereOrNull((e) => e.id == item?.id) !=
                                null
                            ? LinearGradient(colors: [
                                context.customTheme?.selectStart ??
                                    Colors.transparent,
                                context.customTheme?.selectEnd ??
                                    Colors.transparent,
                              ])
                            : null),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          item?.name ?? "",
                          style: context.textTheme.titleMedium?.copyWith(
                              fontWeight: FontWeight.bold,
                              color: selectList.firstWhereOrNull(
                                          (e) => e.id == item?.id) !=
                                      null
                                  ? context.customTheme?.reverseFontColor
                                  : null),
                        ),
                        Text(double.tryParse(item?.odds ?? "0").toString(),
                                style: context.textTheme.bodyMedium?.copyWith(
                                    color: selectList.firstWhereOrNull(
                                                (e) => e.id == item?.id) !=
                                            null
                                        ? context.customTheme?.reverseFontColor
                                        : null))
                            .paddingOnly(left: 10),
                      ],
                    ).paddingOnly(left: 5),
                    Row(
                      children: list(item!).map((e) {
                        return Container(
                          margin: const EdgeInsets.only(left: 5),
                          alignment: Alignment.center,
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: LotteryConfig.getBallBg(
                                      item.copyWith(name: e), widget.details))),
                          child: Text(e,
                              style: context.textTheme.bodyMedium?.copyWith(
                                  fontWeight: FontWeight.bold, fontSize: 12
                                  // color: context.customTheme?.reverseFontColor
                                  )),
                        );
                      }).toList(),
                    ).paddingOnly(right: 10)
                  ],
                ).paddingSymmetric(vertical: 10),
              ).onTap(() => handleSelect(item)),
            ],
          );
        });
  }
}
