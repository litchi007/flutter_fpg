import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/mode_list.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

class OfficePlay extends StatefulWidget {
  final PlayOdd? data;
  final Function(List<Play> list, List<PlayGroup?> playGroups)? onChange;
  const OfficePlay({super.key, this.data, this.onChange});

  @override
  State<OfficePlay> createState() => _OfficePlayState();
}

class _OfficePlayState extends State<OfficePlay> with TickerProviderStateMixin {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  int selectIndex = 0;

  /// 选择的所有列表
  List<PlayGroup?> selectGroupList = [];
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
      widget.onChange?.call([], []);
    });
  }

  handleSelect(Play item, PlayGroup? data) {
    final isHave = select.firstWhereOrNull((e) => e.id == item.id);
    if (isHave == null) {
      select.add(item);
      selectId.add(item.id ?? "");
    } else {
      select = select.where((e) => e.id != item.id).toList();
      selectId.remove(item.id ?? "");
    }

    /// TODO:回传到父组件数据
    /// 先找到group有没有，在找group - list里有没有
    final isSelectGrouphave =
        selectGroupList.firstOrNullWhere((e) => e?.id == data?.id);
    if (isSelectGrouphave != null) {
      // 找group - list里有没有
      final isListHave =
          isSelectGrouphave.plays?.firstOrNullWhere((e) => e.id == item.id);
      if (isListHave != null) {
        final current = isSelectGrouphave.copyWith(plays: select);

        /// 设置
        selectGroupList = selectGroupList
            .map((e) =>
                e?.copyWith(plays: e.id == data?.id ? current.plays : e.plays))
            .toList();
      }
    } else {
      selectGroupList.add(data?.copyWith(plays: select));
    }
    // widget.onChange?.call(select,);
    setState(() {});
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          controller: tabController,
          padding: EdgeInsets.zero,
          isScrollable: true,
          labelPadding: EdgeInsets.zero,
          indicatorColor: Colors.transparent,
          tabAlignment: TabAlignment.start,
          indicatorSize: TabBarIndicatorSize.label,
          splashFactory: NoSplash.splashFactory,
          dividerColor: Colors.transparent,
          indicator: BoxDecoration(
              color: context.theme.cardColor.withOpacity(.2),
              borderRadius: 4.radius),
          labelStyle: context.textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
          tabs: (widget.data?.playGroups ?? [])
              .mapIndexed((i, e) => Container(
                  decoration: BoxDecoration(
                      // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                      borderRadius: 4.radius),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Text(e.alias ?? "")))
              .toList(),
          onTap: handleSelectTab,
        ),
        Expanded(
          child: TabBarView(
              controller: tabController,
              children: (widget.data?.playGroups ?? [])
                  .mapIndexed((i, e) => GuessNo(
                        data: e,
                        onChange: widget.onChange,
                      ))
                  .toList()),
        )
      ],
    );
  }
}

// 猜第几名
class GuessNo extends StatefulWidget {
  final PlayGroup? data;
  final Function(List<Play> list, List<PlayGroup> playGroups)? onChange;
  const GuessNo({super.key, this.data, this.onChange});

  @override
  State<GuessNo> createState() => _GuessNoState();
}

class _GuessNoState extends State<GuessNo> {
  int select = 0;
  List<ArrObj> selectList = [];
  final list = List.generate(10, (e) => e);
  ArrObj typeList = ArrObj();
  handleSelect(int index) {
    setState(() {
      select = index;
      selectList = [];
      widget.onChange?.call([], []);
    });
  }

  handleSelectItem(ArrObj value, [int? i]) {
    final isHave = selectList.contains(value);
    switch (widget.data?.alias) {
      case "猜冠军":
        selectList.clear();
        selectList.add(value);
        break;
      case "猜前二":
      case "猜前三":
      case "猜前四":
      case "猜前五":
        int count = 2;
        final type = widget.data?.alias;
        if (type == "猜前三") {
          count = 3;
        }
        if (type == "猜前四") {
          count = 4;
        }
        if (type == "猜前五") {
          count = 5;
        }
        if (select == 0 && ["猜冠军", "猜前二", "猜前三"].contains(widget.data?.alias)) {
          // 最多只能两个
          if (selectList.length < count) {
            if (!isHave) {
              selectList.add(value);
            } else {
              selectList.remove(value);
            }
          } else {
            selectList.remove(value);
          }
        } else {
          LogUtil.w(typeList.ext?[select].ext);
          // 先找到当前选择的在那个ext里
          for (var e in (typeList.ext?[select].ext ?? [].cast<ArrObj>())) {
            final valueHave = e.ext?.firstWhereOrNull((r) => r.id == value.id);
            // 存在，跳出
            if (valueHave != null) {
              // 判断当前选中的selectList 里是否有当前了列表的项，有则清除
              final currentHave =
                  e.ext?.firstWhereOrNull((e) => selectList.contains(e));
              if (currentHave?.id != valueHave.id) {
                selectList.remove(currentHave);
                selectList.add(value);
              }
              break;
            }
          }
        }
        break;

      default:
    }

    handleChange();
    setState(() {});
  }

  handleChange() {
    List<ArrObj> list = selectList;
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    // if (!["猜冠军", "猜前二", "猜前三"].contains(widget.data?.alias)) {
    list.sort((a, b) => int.parse(a.key ?? '0') - int.parse(b.key ?? '0'));
    // }
    plays = list.map((e) => Play(alias: "$e")).toList();
    groups = [
      widget.data!.copyWith(plays: [
        widget.data!.plays!.first.copyWith(
          alias: list.map((e) => e.value).join(","),
          name: list.map((e) => e.value).join(","),
        )
      ])
    ];

    LogUtil.w(typeList.ext?[select].ext);

    final type = widget.data?.alias;
    if (type != '猜冠军') {
      int count = 2;
      if (type == "猜前三") {
        count = 3;
      }
      if (type == "猜前四") {
        count = 4;
      }
      if (type == "猜前五") {
        count = 5;
      }
      if (selectList.length < count) {
        // ToastUtils.show("选择$count个号码");
        widget.onChange?.call([], []);
        return;
      }
    }
    LogUtil.w(groups);
    widget.onChange?.call(plays, groups);
  }

  init() {
    LogUtil.w("${widget.data}");
    switch (widget.data?.alias) {
      case "猜冠军":
        typeList = ArrObj(id: UuidUtil.to.v4(), value: "猜冠军", key: "猜冠军", ext: [
          ArrObj(id: UuidUtil.to.v4(), value: "冠军", key: "冠军", ext: [
            ArrObj(
                value: "冠军",
                key: "冠军",
                ext: List.generate(
                    10,
                    (e) => ArrObj(
                        id: UuidUtil.to.v4(), value: "${e + 1}", key: "0")))
          ])
        ]);
        break;
      case "猜前二":
      case "猜前三":
      case "猜前四":
      case "猜前五":
      case "猜后二":
      case "猜后三":
      case "猜后四":
      case "猜后五":
        List<ModeList>? modeList = widget.data?.plays?.first.modeList;
        if (modeList?.isEmpty ?? false) {
          if (widget.data?.alias == "猜前四") {
            modeList = [
              ModeList(
                  name: "复式",
                  mode: "duplex",
                  modes: List.generate(4, (value) => "第${value + 1}名")),
            ];
          }
          if (widget.data?.alias == "猜前五") {
            modeList = [
              ModeList(
                  name: "复式",
                  mode: "duplex",
                  modes: List.generate(5, (value) => "第${value + 1}名")),
            ];
          }
          if (widget.data?.alias == "猜前五") {
            modeList = [
              ModeList(
                  name: "复式",
                  mode: "duplex",
                  modes: List.generate(5, (value) => "第${value + 1}名")),
            ];
          }
        }
        typeList = ArrObj(
            value: "${widget.data?.alias}",
            key: "${widget.data?.alias}",
            id: UuidUtil.to.v4(),
            ext: (modeList ?? []).map((e) {
              ModeList item = e;
              //   /// 单式。填充
              if (item.mode == "simplex") {
                item = item.copyWith(modes: ['${item.name}']);
              }
              return ArrObj(
                  key: e.name,
                  value: e.name,
                  id: UuidUtil.to.v4(),
                  ext: item.modes?.mapIndexed((i, e) {
                    return ArrObj(
                        key: e,
                        value: e,
                        ext: List.generate(
                            10,
                            (q) => ArrObj(
                                id: UuidUtil.to.v4(),
                                key: "$i",
                                value: "${q + 1}")));
                  }).toList());
            }).toList());

        break;

      default:
    }
    // 设置一下select
    // if (!["猜冠军", "猜前二", "猜前三"].contains(widget.data?.alias)) {
    //   select = 1;
    // }
  }

  @override
  void initState() {
    init();
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final type = widget.data?.alias;
      int count = 1;
      if (type != '猜冠军') {
        if (type == "猜前二") {
          count = 2;
        }
        if (type == "猜前三") {
          count = 3;
        }
        if (type == "猜前四") {
          count = 4;
        }
        if (type == "猜前五") {
          count = 5;
        }
      } else {
        count = 1;
      }
      List<List<int>> result = [];
      selectList.clear();

      /// 单式
      result = (typeList.ext?[select].ext ?? [])
          .map((e) => EventUtils.robotRandom(
              10,
              ["猜冠军", "猜前二", "猜前三"].contains(widget.data?.alias) && select == 0
                  ? count
                  : 1))
          .toList();
      LogUtil.w("result_____${result}");
      for (var i = 0; i < result.length; i++) {
        final item = result[i];
        for (var e in item) {
          LogUtil.w("e_______${typeList.ext![select].ext![i].ext![e]}");
          selectList.add(typeList.ext![select].ext![i].ext![e]);
        }
      }
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      final deleteArr = value.plays?.first.alias?.split(",") ?? [];
      selectList.removeWhere((e) => deleteArr.contains(e.value));
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  Color? getColor(String num) {
    switch (num) {
      case "1":
        return context.customTheme?.lottery1;
      case "2":
        return context.customTheme?.lottery2;
      case "3":
        return context.customTheme?.lottery3;
      case "4":
        return context.customTheme?.lottery4;
      case "5":
        return context.customTheme?.lottery5;
      case "6":
        return context.customTheme?.lottery6;
      case "7":
        return context.customTheme?.lottery7;
      case "8":
        return context.customTheme?.lottery8;
      case "9":
        return context.customTheme?.lottery9;
      case "10":
        return context.customTheme?.lottery10;
      default:
        return context.customTheme?.lottery1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: ((widget.data?.plays)?.first.modeList ?? [])
              .mapIndexed((i, e) => Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform.scale(
                          scale: 1.2,
                          child: Checkbox(
                              activeColor: context.customTheme?.leftActive,
                              shape: RoundedRectangleBorder(
                                  borderRadius: 30.radius),
                              value: select == i,
                              onChanged: (v) {
                                handleSelect(i);
                              }),
                        ),
                        Text(
                          e.name ?? "",
                          style: context.textTheme.titleMedium,
                        )
                      ],
                    ).paddingOnly(top: 10),
                  ))
              .toList(),
        ),
        const Divider(),
        Expanded(
            child: SingleChildScrollView(
          child: Column(
            children: typeList.ext?[select].ext?.map((e) {
                  return Column(
                    children: [
                      Text(e.value ?? '',
                          style: context.textTheme.titleMedium?.copyWith(
                              color: context.customTheme?.error,
                              fontWeight: FontWeight.w600)),
                      GridView(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.zero,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3, childAspectRatio: 1.2),
                        children: (e.ext ?? [])
                            .mapIndexed((o, e) => Container(
                                  decoration: BoxDecoration(
                                      gradient: selectList.contains(e)
                                          ? LinearGradient(colors: [
                                              context.customTheme
                                                      ?.selectStart ??
                                                  Colors.transparent,
                                              context.customTheme?.selectEnd ??
                                                  Colors.transparent,
                                            ])
                                          : null),
                                  // color: selectList.contains(e)
                                  //     ? context.customTheme?.error
                                  //     : null,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          width: 40,
                                          height: 40,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              color: getColor(e.value ?? "1"),
                                              borderRadius: 30.radius),
                                          child: Text(
                                            "${e.value}",
                                            style: context.textTheme.titleMedium
                                                ?.copyWith(
                                                    fontWeight: FontWeight.bold,
                                                    color: context.customTheme
                                                        ?.reverseFontColor),
                                          )),
                                      Text(
                                              double.parse(widget.data?.plays
                                                          ?.first.odds ??
                                                      "")
                                                  .toString(),
                                              style: context
                                                  .textTheme.bodyMedium
                                                  ?.copyWith(
                                                      color: selectList
                                                              .contains(e)
                                                          ? context.customTheme
                                                              ?.reverseFontColor
                                                          : null))
                                          .paddingOnly(top: 4)
                                    ],
                                  ).onTap(() => handleSelectItem(e, o)),
                                ))
                            .toList(),
                      ),
                    ],
                  );
                }).toList() ??
                [],
          ),
        )),
      ],
    );
  }
}
