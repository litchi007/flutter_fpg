import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

// 冠军玩法
class ChampionPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup?> playGroups)? onChange;
  final String? type;
  const ChampionPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<ChampionPlay> createState() => _ChampionPlayState();
}

class _ChampionPlayState extends State<ChampionPlay> {
  late List<PlayGroup> data = widget.data?.playGroups ?? [];
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  final specialName = ["ZM"];

  /// 是否是形态，需要特殊处理
  late final isXt = ['XT'].contains(widget.type);

  /// 三不同和值
  late final isSbthz = ['SBTHZ'].contains(widget.type);

  /// 对子和值
  late final isDzhz = ['DZHZ'].contains(widget.type);

  /// 选择的所有列表
  List<PlayGroup?> selectGroupList = [];
  // 前区，后区，方位
  final verticalArr = ["QQ", "HQ", "FW", "dlt_TW", "bjkl8_ZM"];

  handleSelect(Play item, PlayGroup? data) {
    final isHave = select.firstWhereOrNull((e) => e.id == item.id);
    if (isHave == null) {
      select.add(item);
      selectId.add(item.id ?? "");
    } else {
      select = select.where((e) => e.id != item.id).toList();
      selectId.remove(item.id ?? "");
    }
    LogUtil.w(widget.data?.playGroups);
    List<PlayGroup> groups = [];
    for (var e in select) {
      for (var q in (widget.data?.playGroups ?? [].cast<PlayGroup>())) {
        final isHave = q.plays?.firstWhereOrNull((w) => w.id == e.id);
        if (isHave != null) {
          groups.add(q.copyWith(plays: [e]));
          break;
        }
      }
      // final isHave = widget.data?.playGroups.;
    }
    widget.onChange?.call(select, groups);
    setState(() {});
  }

  Widget _rowWidget(Play e, PlayGroup? data) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          alignment: Alignment.center,
          width:
              double.tryParse(e.name ?? "").runtimeType == double ? 40 : null,
          height: 40,
          decoration: BoxDecoration(
              image: specialName.contains(widget.type) &&
                      !["bjkl8"].contains(widget.details?.game?.gameType)
                  ? DecorationImage(
                      image: LotteryConfig.getBallBg(e, widget.details),
                      fit: BoxFit.cover)
                  : null,
              borderRadius: 40.radius,
              color: ["DS"].contains(widget.type)
                  ? null
                  : int.tryParse(e.code ?? "")?.runtimeType == int
                      ? context.customTheme?.lottery2
                      : null),
          child: Text(e.name ?? "",
              style: context.textTheme.bodyLarge?.copyWith(
                  fontWeight: FontWeight.w600,
                  color: int.tryParse(e.name ?? "")?.runtimeType == int
                      ? null
                      : selectId.contains(e.id)
                          ? context.customTheme?.reverseFontColor
                          : null)),
        ),
        Text(
                double.tryParse(
                  e.odds ?? "0",
                ).toString(),
                style: context.textTheme.bodyMedium?.copyWith(
                    color: selectId.contains(e.id)
                        ? context.customTheme?.reverseFontColor
                        : null))
            .paddingOnly(
          left: 10,
        ),
      ],
    ).onTap(() => handleSelect(e, data));
  }

  Widget _columnWidget(Play e, PlayGroup? data) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          alignment: Alignment.center,
          width:
              double.tryParse(e.name ?? "").runtimeType == double ? 40 : null,
          height: 40,
          decoration: BoxDecoration(
              image: specialName.contains(widget.type)
                  ? DecorationImage(
                      image: LotteryConfig.getBallBg(e, widget.details),
                      fit: BoxFit.cover)
                  : null,
              borderRadius: 40.radius,
              color: ["DS"].contains(widget.type)
                  ? null
                  : int.tryParse(e.code ?? "")?.runtimeType == int
                      ? context.customTheme?.lottery2
                      : null),
          child: Text(e.name ?? "",
              style: context.textTheme.bodyLarge?.copyWith(
                  fontWeight: FontWeight.w600,
                  color: selectId.contains(e.id)
                      ? context.customTheme?.reverseFontColor
                      : null)),
        ),
        Text(double.tryParse(e.odds ?? "0").toString(),
                style: context.textTheme.bodyMedium?.copyWith(
                    color: selectId.contains(e.id)
                        ? context.customTheme?.reverseFontColor
                        : null))
            .paddingOnly(left: 10),
      ],
    ).onTap(() => handleSelect(e, data));
  }

  init() {
    // 是形态，重新组装数据
    if (isXt) {
      data = LotteryConfig.xtList.map<PlayGroup>((e) {
        final item = widget.data!.playGroups!.first;
        return item.copyWith(
            alias: e.name, plays: item.plays?.sublist(e.start!, e.end!));
      }).toList();
    } else if (isSbthz) {
      data = LotteryConfig.sbthzList.map<PlayGroup>((e) {
        final item = widget.data!.playGroups!.first;
        return item.copyWith(
            alias: e.name, plays: item.plays?.sublist(e.start!, e.end!));
      }).toList();
    } else if (isDzhz) {
      data = LotteryConfig.dzhzList.map<PlayGroup>((e) {
        final item = widget.data!.playGroups!.first;
        return item.copyWith(
            alias: e.name, plays: item.plays?.sublist(e.start!, e.end!));
      }).toList();
    } else {
      data = widget.data?.playGroups ?? [];
    }
    setState(() {});
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }

      /// 随机group，在随机plays
      final result =
          LotteryConfig.robotRandomSelect(widget.data?.playGroups ?? []);
      selectGroupList = [];
      select = result.currentPlay ?? [];
      selectId = (result.currentPlay ?? []).map((e) => e.id ?? '').toList();
      for (var i = 0; i < (result.currentPlay?.length ?? 0); i++) {
        // final group = result.currentGroup?[i];
        final play = result.currentPlay?[i];
        PlayGroup? groups;
        widget.data?.playGroups?.forEach((e) {
          if (e.plays?.contains(play) ?? false) {
            groups = e;
          }
        });
        selectGroupList.add(groups?.copyWith(plays: [play!]));
        // selectGroupList
        // handleSelect(play!, groups!);
      }
      widget.onChange?.call(select, selectGroupList);
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      select = select.where((e) => e.id == value.plays?.first.id).toList();
      selectId.remove(value.plays?.first.id);
      setState(() {});
    }, time: 0.milliseconds);
    init();
    super.initState();
  }

  @override
  void didUpdateWidget(ChampionPlay oldWidget) {
    init();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final item = data[index];
          return Column(
            children: [
              Visibility(
                // visible: !["DZHZ", "SBTHZ", "XT"].contains(widget.type),
                child: Text(
                  "${item.alias}",
                  style: context.textTheme.titleMedium?.copyWith(
                      color: context.customTheme?.error,
                      fontWeight: FontWeight.w600),
                ).paddingSymmetric(vertical: 10),
              ),

              // Text("${item?.plays}"),
              item.plays != null
                  ? GridView(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio:
                              verticalArr.contains(widget.type) ? 2.1 : 3),
                      children: ((item.plays) ?? [])
                          .map((e) => Container(
                                decoration: BoxDecoration(
                                    color:
                                        context.customTheme?.reverseFontColor,
                                    boxShadow: [
                                      BoxShadow(
                                          color: context.customTheme
                                                  ?.boxShadowBorder ??
                                              Colors.transparent,
                                          offset:
                                              const Offset(1.0, 1), //阴影y轴偏移量
                                          blurRadius: 3, //阴影模糊程度
                                          spreadRadius: 0, //阴影扩散程度
                                          blurStyle: BlurStyle.outer)
                                    ],
                                    gradient: selectId.contains(e.id)
                                        ? LinearGradient(colors: [
                                            context.customTheme?.selectStart ??
                                                Colors.transparent,
                                            context.customTheme?.selectEnd ??
                                                Colors.transparent,
                                          ])
                                        : null),
                                // color: selectId.contains(e.id)
                                //     ? context.customTheme?.error
                                //     : null,
                                child: verticalArr.contains(widget.type)
                                    ? _columnWidget(e, item)
                                    : _rowWidget(e, item),
                              ))
                          .toList(),
                    )
                  : const SizedBox(),
            ],
          );
        });
  }
}
