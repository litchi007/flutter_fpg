import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class OutOfChoicePlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const OutOfChoicePlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<OutOfChoicePlay> createState() => _OutOfChoicePlayState();
}

class _OutOfChoicePlayState extends State<OutOfChoicePlay> {
  List<ArrObj> list = List.generate(
      49,
      (index) => ArrObj(
          id: UuidUtil.to.v4(), key: "${index + 1}", value: "${index + 1}"));

  List<ArrObj> selectList = [];
  List<Play> plays = [];
  handleSelect(ArrObj item) {
    final isHave = selectList.firstWhereOrNull((e) => e.id == item.id);
    if (isHave == null) {
      selectList.add(item);
    } else {
      selectList.remove(item);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    final currentPlays =
        selectList.map((e) => Play(name: e.value, alias: e.value)).toList();
    // final groups = currentPlays
    //     .map((e) => widget.data!.playGroups!.first.copyWith(plays: [e]))
    //     .toList();
    if (selectList.length < 5) {
      return;
    }
    widget.onChange?.call(currentPlays, [
      widget.data!.playGroups!.first.copyWith(plays: [
        widget.data!.playGroups!.first.plays!.first.copyWith(
          alias: selectList.map((e) => e.value).join(","),
          name: selectList.map((e) => e.value).join(","),
        )
      ])
    ]);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = EventUtils.robotRandom(list.length);
      plays.clear();
      selectList.clear();
      plays.clear();
      for (var e in result) {
        selectList.add(list[e]);
      }
      handleChange();
      setState(() {});
      // final plays = result.map((e) => Play(name: "$e", alias: "$e")).toList();
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (v) {
      if (!mounted) {
        return;
      }
      selectList.clear();
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: list.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 5, childAspectRatio: .9),
        itemBuilder: (context, index) {
          final item = list[index];
          return Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
                gradient: selectList.firstWhereOrNull((e) => e.id == item.id) !=
                        null
                    ? LinearGradient(colors: [
                        context.customTheme?.selectStart ?? Colors.transparent,
                        context.customTheme?.selectEnd ?? Colors.transparent,
                      ])
                    : null,
                image: DecorationImage(
                    image: LotteryConfig.getBallBg(
                        Play(name: item.value), widget.details))),
            child: Text(
              "${item.value}",
              style: context.textTheme.bodyMedium?.copyWith(
                fontWeight: FontWeight.bold,
                // color: context.customTheme?.reverseFontColor
              ),
            ),
          ).onTap(() => handleSelect(item));
        });
  }
}
