// 五行
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class FiveElementPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const FiveElementPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<FiveElementPlay> createState() => _FiveElementPlayState();
}

class _FiveElementPlayState extends State<FiveElementPlay> {
  List<Play> selectList = [];
  List<PlayGroup> playGroups = [];

  handleSelect(Play value) {
    final isHave = selectList.firstWhereOrNull((e) => e.id == value.id);
    if (isHave == null) {
      selectList.add(value);
    } else {
      selectList.remove(value);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    final groups = selectList.map((e) {
      final item = ((widget.details?.setting?.fiveElements ?? [])
              .firstWhereOrNull((e1) => e1.name == e.name)
              ?.nums ??
          []);
      return widget.data!.playGroups!.first
          .copyWith(plays: [e.copyWith(alias: item.join(","))]);
    }).toList();
    playGroups.addAll(groups);

    widget.onChange?.call(selectList, groups);
  }

  @override
  void initState() {
    super.initState();
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = EventUtils.robotRandom(
          widget.data?.playGroups?.first.plays?.length ?? 0);
      selectList.clear();
      final plays = result.map((e) {
        final item = widget.data!.playGroups!.first.plays![e];
        final alias = ((widget.details?.setting?.fiveElements ?? [])
                .firstWhere((e1) => e1.name == item.name)
                .nums ??
            []);

        return item.copyWith(alias: alias.join(","));
      }).toList();
      selectList.addAll(plays);
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      LogUtil.w("values____$value");
      final alias = value.plays?.first.alias?.split(",");
      // selectList.where((e) {
      //   return e;
      // });
      playGroups = playGroups
          .where((e) => e.plays?.first.name != value.plays?.first.name)
          .toList();
      // 例如删除龙,虎
      // 判断余下的列表里有没有龙虎，没有就删除selectList的
      final allname = playGroups.map((e) => e.plays?.first.name).join(",");
      selectList = selectList.where((e) {
        return allname.contains((e.name ?? ""));
      }).toList();
      LogUtil.w("groups____ name_____ $selectList");
      setState(() {});
    }, time: 0.milliseconds);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.data?.playGroups?.first.plays?.length ?? 0,
        itemBuilder: (context, index) {
          final item = widget.data?.playGroups?.first.plays?[index];

          return Container(
            decoration: BoxDecoration(
              border: Border(
                  bottom:
                      BorderSide(color: context.theme.dividerColor, width: 1)),
              color:
                  selectList.firstWhereOrNull((e) => e.id == item?.id) != null
                      ? context.customTheme?.leftActive
                      : null,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      item?.name ?? "",
                      style: context.textTheme.titleMedium?.copyWith(
                          fontWeight: FontWeight.bold,
                          color: selectList.firstWhereOrNull(
                                      (e) => e.id == item?.id) !=
                                  null
                              ? context.customTheme?.reverseFontColor
                              : null),
                    ),
                    Text(double.tryParse(item?.odds ?? "0").toString(),
                            style: context.textTheme.titleMedium?.copyWith(
                                color: selectList.firstWhereOrNull(
                                            (e) => e.id == item?.id) !=
                                        null
                                    ? context.customTheme?.reverseFontColor
                                    : null))
                        .paddingOnly(left: 10),
                  ],
                ).paddingOnly(left: 5),
                Expanded(
                  child: Wrap(
                    runSpacing: 10,
                    children: ((widget.details?.setting?.fiveElements ?? [])
                                .firstWhere((e) => e.name == item?.name)
                                .nums ??
                            [])
                        .map((e) {
                      return Container(
                        margin: const EdgeInsets.only(left: 5),
                        alignment: Alignment.center,
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: LotteryConfig.getBallBg(
                                    item!.copyWith(name: e), widget.details))),
                        child: Text(e,
                            style: context.textTheme.bodyMedium?.copyWith(
                              fontWeight: FontWeight.bold,
                              // color: context.customTheme?.reverseFontColor
                            )),
                      );
                    }).toList(),
                  ).paddingOnly(right: 10, left: 20),
                )
              ],
            ).paddingSymmetric(vertical: 10),
          ).onTap(() => handleSelect(item!));
        });
  }
}
