import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

// 直选
class DirectElectionPlay extends StatefulWidget {
  final GamePlayOddsModel? details;
  final PlayOdd? data;
  final String? type;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const DirectElectionPlay(
      {super.key, this.onChange, this.data, this.details, this.type});

  @override
  State<DirectElectionPlay> createState() => _DirectElectionPlayState();
}

class _DirectElectionPlayState extends State<DirectElectionPlay>
    with TickerProviderStateMixin {
  int selectIndex = 0;
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: TabBar(
            controller: tabController,
            padding: EdgeInsets.zero,
            // isScrollable: true,
            labelPadding: EdgeInsets.zero,
            indicatorColor: Colors.transparent,
            // tabAlignment: TabAlignment.start,
            indicatorSize: TabBarIndicatorSize.tab,
            splashFactory: NoSplash.splashFactory,
            dividerColor: Colors.transparent,
            indicator: BoxDecoration(
                color: context.theme.cardColor.withOpacity(.2),
                borderRadius: 4.radius),
            labelStyle: context.textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
            unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
            tabs: (widget.data?.playGroups ?? [])
                .mapIndexed((i, e) => Container(
                    decoration: BoxDecoration(
                        // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                        borderRadius: 4.radius),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: Text(e.alias ?? "")))
                .toList(),
            onTap: handleSelectTab,
          ),
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children:
                    (widget.data?.playGroups ?? []).mapIndexed((index, e) {
                  return DirectElectionPlayItem(
                    data: e,
                    onChange: widget.onChange,
                  );
                }).toList()))
      ],
    );
  }
}

class DirectElectionPlayItem extends StatefulWidget {
  final PlayGroup? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const DirectElectionPlayItem({super.key, this.data, this.onChange});

  @override
  State<DirectElectionPlayItem> createState() => _DirectElectionPlayItemState();
}

class _DirectElectionPlayItemState extends State<DirectElectionPlayItem> {
  List<ArrObj> select = [];

  List<ArrObj> list = [
    ArrObj(
        id: UuidUtil.to.v4(),
        key: "第一球",
        ext: List.generate(
            11,
            (index) => ArrObj(
                id: UuidUtil.to.v4(), key: "第一球", value: "${index + 1}"))),
    ArrObj(
        id: UuidUtil.to.v4(),
        key: "第二球",
        ext: List.generate(
            11,
            (index) => ArrObj(
                id: UuidUtil.to.v4(), key: "第二球", value: "${index + 1}"))),
  ];

  List<ArrObj> list3 = [
    ArrObj(
        id: UuidUtil.to.v4(),
        key: "第一球",
        ext: List.generate(
            11,
            (index) => ArrObj(
                id: UuidUtil.to.v4(), key: "第一球", value: "${index + 1}"))),
    ArrObj(
        id: UuidUtil.to.v4(),
        key: "第二球",
        ext: List.generate(
            11,
            (index) => ArrObj(
                id: UuidUtil.to.v4(), key: "第二球", value: "${index + 1}"))),
    ArrObj(
        id: UuidUtil.to.v4(),
        key: "第三球",
        ext: List.generate(
            11,
            (index) => ArrObj(
                id: UuidUtil.to.v4(), key: "第三球", value: "${index + 1}"))),
  ];

  handleSelect(ArrObj value) {
    final isHave = select.firstWhereOrNull((e) => e.value == value.value);
    if (isHave == null) {
      select.add(value);
    } else {
      select.remove(value);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    for (var e in select) {
      final play = Play(name: e.value, alias: e.value);
      plays.add(play);
      groups.add(widget.data!.copyWith(plays: [play]));
    }
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      List<ArrObj> allList = [];
      if (widget.data?.plays?.first.code == "Q3ZHIXUAN") {
        for (var e in list3) {
          allList.addAll(e.ext ?? []);
        }
      } else {
        for (var e in list) {
          allList.addAll(e.ext ?? []);
        }
      }
      select.clear();
      final result = EventUtils.robotRandom(allList.length);
      select.addAll(result.map((e) => allList[e]));
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            "赔率：${double.tryParse(widget.data?.plays?.first.odds ?? "0")}",
            style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error, fontWeight: FontWeight.w600),
          ).paddingSymmetric(vertical: 10),
          ...(widget.data?.plays?.first.code == "Q3ZHIXUAN"
              ? list3
                  .map((e) => Column(
                        children: [
                          Text(e.key ?? "",
                              style: context.textTheme.titleMedium?.copyWith(
                                  color: context.customTheme?.error,
                                  fontWeight: FontWeight.w600)),
                          GridView(
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3, childAspectRatio: 1.8),
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              children: (e.ext ?? [])
                                  .map((q) => Container(
                                      color: select.firstWhereOrNull(
                                                  (e) => e.id == q.id) !=
                                              null
                                          ? context.customTheme?.error
                                          : null,
                                      alignment: Alignment.center,
                                      child: UnconstrainedBox(
                                          child: Container(
                                                  alignment: Alignment.center,
                                                  width: 40,
                                                  height: 40,
                                                  decoration: BoxDecoration(
                                                      color:
                                                          context.customTheme?.lottery2,
                                                      borderRadius: 20.radius),
                                                  child: Text(q.value ?? ""))
                                              .onTap(() => handleSelect(q)))))
                                  .toList()),
                        ],
                      ))
                  .toList()
              : list
                  .map((e) => Column(
                        children: [
                          Text(e.key ?? "",
                              style: context.textTheme.titleMedium?.copyWith(
                                  color: context.customTheme?.error,
                                  fontWeight: FontWeight.w600)),
                          GridView(
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3, childAspectRatio: 1.8),
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              children: (e.ext ?? [])
                                  .map((q) => Container(
                                      color: select.firstWhereOrNull(
                                                  (e) => e.id == q.id) !=
                                              null
                                          ? context.customTheme?.error
                                          : null,
                                      child: UnconstrainedBox(
                                        child: Container(
                                                alignment: Alignment.center,
                                                width: 40,
                                                height: 40,
                                                decoration: BoxDecoration(
                                                    color: context
                                                        .customTheme?.lottery2,
                                                    borderRadius: 20.radius),
                                                child: Text(q.value ?? ""))
                                            .onTap(() => handleSelect(q)),
                                      )))
                                  .toList()),
                        ],
                      ))
                  .toList()),
        ],
      ),
    );
  }
}
