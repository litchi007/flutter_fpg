import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

/// 正特

class JustSpecialPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup>? data)? onChange;
  final String? type;
  const JustSpecialPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<JustSpecialPlay> createState() => _JustSpecialPlayState();
}

class _JustSpecialPlayState extends State<JustSpecialPlay>
    with TickerProviderStateMixin {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  int selectIndex = 0;
  late List<PlayGroup> tabs = (widget.data?.playGroups ?? [])
      .where((e) => !(e.alias?.contains("两面") ?? false))
      .toList();
  late final tabController = TabController(length: (tabs).length, vsync: this)
    ..addListener(listener);
  late List<PlayGroup> second = (widget.data?.playGroups ?? [])
          .where((e) => (e.alias?.contains("两面") ?? false))
          .toList() ??
      [];
  listener() {
    widget.onChange?.call([], []);
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  handleSelect(List<Play>? plays, List<PlayGroup>? groups) {
    widget.onChange?.call(plays ?? [], groups ?? []);
  }

  @override
  void initState() {
    LogUtil.w(
        "widget____${(widget.data?.playGroups ?? []).where((e) => !(e.alias?.contains("两面") ?? false)).length}");
    super.initState();
  }

  @override
  void didUpdateWidget(JustSpecialPlay oldWidget) {
    setState(() {
      tabs = (widget.data?.playGroups ?? [])
          .where((e) => !(e.alias?.contains("两面") ?? false))
          .toList();
    });
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          controller: tabController,
          padding: EdgeInsets.zero,
          isScrollable: true,
          labelPadding: EdgeInsets.zero,
          indicatorColor: Colors.transparent,
          tabAlignment: TabAlignment.start,
          indicatorSize: TabBarIndicatorSize.label,
          splashFactory: NoSplash.splashFactory,
          dividerColor: Colors.transparent,
          indicator: BoxDecoration(
              color: context.theme.cardColor.withOpacity(.2),
              borderRadius: 4.radius),
          labelStyle: context.textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
          tabs: tabs
              .mapIndexed((i, e) => Container(
                  decoration: BoxDecoration(
                      // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                      borderRadius: 4.radius),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Text(e.alias ?? "")))
              .toList(),
          onTap: handleSelectTab,
        ),
        Expanded(
          child: TabBarView(
              controller: tabController,
              children: tabs
                  .mapIndexed((i, e) => JustSpecialPlayItem(
                      secondData: second[selectIndex],
                      details: widget.details,
                      data: e,
                      onChange: handleSelect,
                      select: select))
                  .toList()),
        )
      ],
    );
  }
}

class JustSpecialPlayItem extends StatefulWidget {
  final PlayGroup? data;
  final GamePlayOddsModel? details;
  final Function(List<Play>? list, List<PlayGroup>? data)? onChange;
  final PlayGroup? secondData;
  final List<Play>? select;
  const JustSpecialPlayItem(
      {super.key,
      this.data,
      this.details,
      this.onChange,
      this.select,
      this.secondData});

  @override
  State<JustSpecialPlayItem> createState() => _JustSpecialPlayItemState();
}

class _JustSpecialPlayItemState extends State<JustSpecialPlayItem> {
  int select = 0;
  List<Play> selectList = [];
  late final list = widget.data?.plays ?? [];

  /// 选择的所有列表
  List<PlayGroup> selectGroupList = [];
  handleSelect(int index) {
    setState(() {
      select = index;
    });
  }

  handleSelectItem(Play item) {
    final isHave = selectList.firstWhereOrNull((e) => e.name == item.name);
    if (isHave == null) {
      selectList.add(item);
    } else {
      selectList.remove(item);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<PlayGroup> groups = [];

    for (var i = 0; i < (selectList.length); i++) {
      final play = selectList[i];
      PlayGroup? group = widget.data;
      groups.add(group!.copyWith(plays: [play]));
    }
    widget.onChange?.call(selectList, groups);
  }

  init() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }

      /// 随机group，在随机plays
      final result =
          LotteryConfig.robotRandomSelect([widget.data ?? const PlayGroup()]);
      selectGroupList = [];
      selectList.clear();
      selectList = result.currentPlay ?? [];
      handleChange();
      setState(() {});
      // selectList.addAll(result.currentPlay);

      // selectGroupList =
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      handleSelectItem(value.plays!.first);
    }, time: 0.milliseconds);
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: ((widget.data?.plays)?.first.modeList ?? [])
              .mapIndexed((i, e) => Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform.scale(
                          scale: 1.2,
                          child: Checkbox(
                              activeColor: context.customTheme?.error,
                              shape: RoundedRectangleBorder(
                                  borderRadius: 30.radius),
                              value: select == i,
                              onChanged: (v) {
                                handleSelect(i);
                              }),
                        ),
                        Text(
                          e.name ?? "",
                          style: context.textTheme.titleMedium,
                        )
                      ],
                    ).paddingSymmetric(vertical: 10),
                  ))
              .toList(),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                GridView(
                  physics: const NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, childAspectRatio: 1.4),
                  children: (list)
                      .map((e) => Container(
                            decoration: BoxDecoration(
                                gradient: selectList.firstOrNullWhere(
                                            (item) => e.name == item.name) !=
                                        null
                                    ? LinearGradient(colors: [
                                        context.customTheme?.selectStart ??
                                            Colors.transparent,
                                        context.customTheme?.selectEnd ??
                                            Colors.transparent,
                                      ])
                                    : null),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    width: 35,
                                    height: 35,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        // color: context.customTheme?.lottery2,
                                        image: double.tryParse(
                                                        e.name ?? e.code ?? "0")
                                                    .runtimeType ==
                                                double
                                            ? DecorationImage(
                                                image: LotteryConfig.getBallBg(
                                                    e, widget.details))
                                            : null,
                                        borderRadius: 30.radius),
                                    child: Text(
                                      "${e.name}",
                                      style: context.textTheme.titleMedium?.copyWith(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12
                                          // color:
                                          //     double.tryParse(e.name ?? e.code ?? "0")
                                          //                 .runtimeType ==
                                          //             double
                                          //         ? context
                                          //             .customTheme?.reverseFontColor
                                          //         : null
                                          ),
                                    )),
                                Text(
                                        double.parse(widget
                                                    .data?.plays?.first.odds ??
                                                "")
                                            .toString(),
                                        style: context.textTheme.bodyMedium
                                            ?.copyWith(
                                                color:
                                                    selectList.firstOrNullWhere(
                                                                (item) =>
                                                                    e.name ==
                                                                    item
                                                                        .name) !=
                                                            null
                                                        ? context.customTheme
                                                            ?.reverseFontColor
                                                        : null))
                                    .paddingOnly(top: 4)
                              ],
                            ).onTap(() => handleSelectItem(e)),
                          ))
                      .toList(),
                ),
                Text("${widget.secondData?.alias}",
                    style: context.textTheme.titleMedium
                        ?.copyWith(color: context.customTheme?.error)),
                GridView(
                  physics: const NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, childAspectRatio: 1.4),
                  children: (widget.secondData?.plays ?? [])
                      .map((e) => Container(
                            decoration: BoxDecoration(
                                gradient: selectList.firstOrNullWhere(
                                            (item) => e.name == item.name) !=
                                        null
                                    ? LinearGradient(colors: [
                                        context.customTheme?.selectStart ??
                                            Colors.transparent,
                                        context.customTheme?.selectEnd ??
                                            Colors.transparent,
                                      ])
                                    : null),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    width: 35,
                                    height: 35,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        // color: context.customTheme?.lottery2,
                                        image: double.tryParse(
                                                        e.name ?? e.code ?? "0")
                                                    .runtimeType ==
                                                double
                                            ? DecorationImage(
                                                image: LotteryConfig.getBallBg(
                                                    e, widget.details))
                                            : null,
                                        borderRadius: 30.radius),
                                    child: Text(
                                      "${e.name}",
                                      style: context.textTheme.titleMedium?.copyWith(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                          color: selectList.firstOrNullWhere(
                                                      (item) =>
                                                          e.name ==
                                                          item.name) !=
                                                  null
                                              ? context
                                                  .customTheme?.reverseFontColor
                                              : null
                                          // color:
                                          //     double.tryParse(e.name ?? e.code ?? "0")
                                          //                 .runtimeType ==
                                          //             double
                                          //         ? context
                                          //             .customTheme?.reverseFontColor
                                          //         : null
                                          ),
                                    )),
                                Text(
                                        double.parse(widget
                                                    .data?.plays?.first.odds ??
                                                "")
                                            .toString(),
                                        style: context.textTheme.bodyMedium
                                            ?.copyWith(
                                                color:
                                                    selectList.firstOrNullWhere(
                                                                (item) =>
                                                                    e.name ==
                                                                    item
                                                                        .name) !=
                                                            null
                                                        ? context.customTheme
                                                            ?.reverseFontColor
                                                        : null))
                                    .paddingOnly(top: 4)
                              ],
                            ).onTap(() => handleSelectItem(e)),
                          ))
                      .toList(),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
