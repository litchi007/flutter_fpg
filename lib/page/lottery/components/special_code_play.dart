import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

/// 特码

class SpecialCodePlay extends StatefulWidget {
  final GamePlayOddsModel? details;
  final PlayOdd? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const SpecialCodePlay({super.key, this.onChange, this.data, this.details});

  @override
  State<SpecialCodePlay> createState() => _SpecialCodePlayState();
}

class _SpecialCodePlayState extends State<SpecialCodePlay> {
  // 选择的是特码A还是特码B
  int select = 0;
  final tabs = ["特码B", "特码A"];
  int selectZodiacs = -1;
  List<Play> list = [].cast<Play>();
  List<Play> selectList = [];
  List<String> selectZodiacsList = [];
  List<PlayGroup> secondData = [];
  handleSelect(int value) {
    setState(() {
      final current = widget.data?.playGroups
          ?.where((e) => e.alias == tabs[value])
          .toList();
      if (current?.isNotEmpty ?? false) {
        list = current?.first.plays ?? [];
        select = value;
      }
      secondData = widget.data?.playGroups
              ?.where((e) =>
                  (e.alias?.contains(tabs[value]) ?? false) &&
                  e.alias != tabs[value])
              .toList() ??
          [];
      // secondData = widget.data?.playGroups?.firstWhere((e) => e.alias == "${tabs[value]}两面")??const PlayGroup();
      LogUtil.w("secondData____$secondData");
    });
  }

  handleSelectZodiacs(int value, String name) {
    LogUtil.w("isHave_______________$name");
    final currentName = name;
    setState(() {
      final isHave =
          selectZodiacsList.firstOrNullWhere((e) => e == currentName);
      if (isHave == null) {
        selectZodiacsList.add(currentName);
      }

      // 选择的生肖详情
      final result = selectZodiacsList
          .map((e) => widget.details?.setting?.zodiacNums
              ?.firstWhereOrNull((e) => e.name == currentName))
          .toList();
      final current = result.map<List<Play>>((e) {
        final numList = e?.nums?.map((q) => int.tryParse(q) ?? 0).toList();
        final currentList = list
            .where(
                (q) => numList?.contains(int.tryParse(q.name ?? "")) ?? false)
            .toList();
        return currentList;
      });
      // 过滤，重复项
      final expandList = current.expand((list) => list).toList();
      LogUtil.w("isHave_______________$currentName $result");
      // Get.toNamed(page)
      for (var e in expandList) {
        if (isHave != null) {
          selectList.remove(e);
        } else {
          if (selectList.contains(e)) {
            continue;
          } else {
            selectList.add(e);
          }
        }
      }

      /// 处理完了在删除生肖
      if (isHave != null) {
        selectZodiacsList.remove(currentName);
      }

      handleChange();
    });
  }

  handleSelectList(Play value) {
    final isHave = selectList.firstWhereOrNull((e) => e.name == value.name);
    if (isHave == null) {
      selectList.add(value);
    } else {
      selectList.remove(value);
    }
    handleChange();

    setState(() {});
  }

  handleChange() {
    // LogUtil.w(widget.data?.playGroups?.first);
    final groups = selectList
        .map((e) => widget.data!.playGroups!.first
            .copyWith(plays: [e.copyWith(alias: '${e.alias}')]))
        .toList();
    widget.onChange?.call(selectList, groups);
  }

  @override
  void didUpdateWidget(SpecialCodePlay oldWidget) {
    handleSelect(select);
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    super.initState();
    handleSelect(0);
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      selectZodiacsList.clear();
      selectList.clear();
      final robotSelect = EventUtils.robotRandom(list.length);
      final zodiac = EventUtils.robotRandom(
          widget.details?.setting?.zodiacNums?.length ?? 0);
      selectZodiacs = zodiac.first;
      final tabSelect = EventUtils.robotRandom(2, 1);
      select = tabSelect.first;
      // LogUtil.w(robotSelect);
      final plays =
          list.whereIndexed((e, i) => robotSelect.contains(i)).toList();
      selectList.addAll(plays);
      LogUtil.w(selectList);
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      LogUtil.w("value_______$value");
      selectList =
          selectList.where((e) => e.id != value.plays?.first.id).toList();
      setState(() {});
    }, time: 0.milliseconds);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Container(
                        color: 0 == select
                            ? context.theme.cardColor.withOpacity(.5)
                            : null,
                        alignment: Alignment.center,
                        child: Text(tabs[0]).paddingSymmetric(vertical: 10))
                    .onTap(() => handleSelect(0))),
            Expanded(
                child: Container(
                        color: 1 == select
                            ? context.theme.cardColor.withOpacity(.5)
                            : null,
                        alignment: Alignment.center,
                        child: Text(tabs[1]).paddingSymmetric(vertical: 10))
                    .onTap(() => handleSelect(1))),
          ],
        ),

        // 生肖
        SizedBox(
          height: 40,
          child: ListView.builder(
              itemCount: widget.details?.setting?.zodiacNums?.length ?? 0,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                final item = widget.details?.setting?.zodiacs?[index];
                return Row(
                  children: [
                    Transform.scale(
                      scale: 1.2,
                      child: Checkbox(
                          side: BorderSide(
                              width: 1,
                              color: context.textTheme.bodyMedium?.color ??
                                  Colors.transparent),
                          activeColor: context.customTheme?.leftActive,
                          shape:
                              RoundedRectangleBorder(borderRadius: 30.radius),
                          value: selectZodiacsList.contains(item),
                          onChanged: (v) {
                            handleSelectZodiacs(index, item!);
                          }),
                    ),
                    Text("$item"),
                  ],
                );
              }),
        ),

        Text(
          // "${select == 0 ? '特码B' : '特码A'}",
          tabs[select],
          style: context.textTheme.titleMedium?.copyWith(
              color: context.customTheme?.error, fontWeight: FontWeight.w600),
        ).paddingSymmetric(vertical: 10),

        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                GridView(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, childAspectRatio: 2),
                  children: list.mapIndexed((index, item) {
                    final item = list[index];
                    return Container(
                      decoration: BoxDecoration(
                          gradient: selectList.firstWhereOrNull(
                                      (e) => e.name == item.name) !=
                                  null
                              ? LinearGradient(colors: [
                                  context.customTheme?.selectStart ??
                                      Colors.transparent,
                                  context.customTheme?.selectEnd ??
                                      Colors.transparent,
                                ])
                              : null),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: LotteryConfig.getBallBg(
                                      item, widget.details)),
                            ),
                            child: Text(item.name ?? "",
                                style: context.textTheme.bodyMedium?.copyWith(
                                    // color: context.customTheme?.reverseFontColor
                                    )),
                          ),
                          Text(double.tryParse(item.odds ?? "0").toString(),
                                  style: context.textTheme.bodyMedium?.copyWith(
                                      color: selectList.firstWhereOrNull(
                                                  (e) => e.name == item.name) !=
                                              null
                                          ? context
                                              .customTheme?.reverseFontColor
                                          : null))
                              .paddingOnly(left: 5)
                        ],
                      ).onTap(() => handleSelectList(item)),
                    );
                  }).toList(),
                ),
                Text("两面",
                    style: context.textTheme.titleMedium
                        ?.copyWith(color: context.customTheme?.error)),
                ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: secondData.length,
                    itemBuilder: (context, index) {
                      final e = secondData[index];
                      return GridView(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3, childAspectRatio: 2),
                        children: (e.plays ?? []).mapIndexed((index, item) {
                          final item = (e.plays ?? [])[index];
                          return Container(
                            decoration: BoxDecoration(
                                gradient: selectList.firstWhereOrNull(
                                            (e) => e.name == item.name) !=
                                        null
                                    ? LinearGradient(colors: [
                                        context.customTheme?.selectStart ??
                                            Colors.transparent,
                                        context.customTheme?.selectEnd ??
                                            Colors.transparent,
                                      ])
                                    : null),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: 40,
                                  height: 40,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: LotteryConfig.getBallBg(
                                            item, widget.details)),
                                  ),
                                  child: Text(item.name ?? "",
                                      style: context.textTheme.bodyMedium
                                          ?.copyWith(
                                              color:
                                                  selectList.firstWhereOrNull(
                                                              (e) =>
                                                                  e
                                                                      .name ==
                                                                  item.name) !=
                                                          null
                                                      ? context.customTheme
                                                          ?.reverseFontColor
                                                      : null)),
                                ),
                                Text(
                                        double.tryParse(item.odds ?? "0")
                                            .toString(),
                                        style: context.textTheme.bodyMedium
                                            ?.copyWith(
                                                color:
                                                    selectList.firstWhereOrNull(
                                                                (e) =>
                                                                    e.name ==
                                                                    item
                                                                        .name) !=
                                                            null
                                                        ? context.customTheme
                                                            ?.reverseFontColor
                                                        : null))
                                    .paddingOnly(left: 5)
                              ],
                            ).onTap(() => handleSelectList(item)),
                          );
                        }).toList(),
                      );
                    })
              ],
            ),
          ),
        )
      ],
    );
  }
}
