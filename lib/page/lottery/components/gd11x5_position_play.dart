import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

// 广东11选5 定位
class Gd11x5PositionPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const Gd11x5PositionPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<Gd11x5PositionPlay> createState() => _Gd11x5PositionPlayState();
}

class _Gd11x5PositionPlayState extends State<Gd11x5PositionPlay> {
  late final list = widget.data?.playGroups
      ?.map((e) => ArrObj(
          id: UuidUtil.to.v4(),
          key: e.plays?.first.odds,
          value: e.alias,
          ext: List.generate(
              11,
              (index) => ArrObj(
                  id: UuidUtil.to.v4(), key: "$index", value: "${index + 1}"))))
      .toList();

  List<ArrObj> select = [];

  handleSelect(ArrObj value) {
    final isHave = select.firstOrNullWhere((e) => e.id == value.id);
    if (isHave == null) {
      select.add(value);
    } else {
      select.remove(value);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    for (var e in select) {
      final play = Play(name: "$e", alias: "$e");
      plays.add(play);
      groups.add(widget.data!.playGroups!.first.copyWith(plays: [play]));
    }

    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      List<ArrObj> allList = [];
      list?.forEach((e) {
        allList.addAll(e.ext ?? []);
      });
      final result = EventUtils.robotRandom(allList.length);
      select.clear();
      select.addAll(result.map((e) => allList[e]));
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: list?.length ?? 0,
        itemBuilder: (context, index) {
          final item = list?[index];
          return Column(
            children: [
              Text(
                "${item?.value} ${double.tryParse(item?.key ?? '0')}",
                style: context.textTheme.bodyMedium?.copyWith(
                    // fontWeight: FontWeight.bold
                    fontSize: 16,
                    color: context.customTheme?.error),
              ).paddingSymmetric(vertical: 10),
              GridView(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3, childAspectRatio: 1.8),
                children: (item?.ext ?? [])
                    .mapIndexed((index, e) => Container(
                          color: select.firstOrNullWhere((q) => e.id == q.id) !=
                                  null
                              ? context.customTheme?.error
                              : null,
                          child: UnconstrainedBox(
                            child: Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                  color: context.customTheme?.lottery2,
                                  borderRadius: 20.radius),
                              alignment: Alignment.center,
                              child: Text("${e.value}"),
                            ).onTap(() => handleSelect(e)),
                          ),
                        ))
                    .toList(),
              )
            ],
          );
        });
  }
}
