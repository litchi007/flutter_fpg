import 'dart:convert';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

// 二字
class SecondFontPlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  final String? type;
  const SecondFontPlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<SecondFontPlay> createState() => _SecondFontPlayState();
}

class _SecondFontPlayState extends State<SecondFontPlay>
    with TickerProviderStateMixin {
  int selectIndex = 0;
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
      widget.onChange?.call([], []);
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: TabBar(
            controller: tabController,
            padding: EdgeInsets.zero,
            // isScrollable: true,
            labelPadding: EdgeInsets.zero,
            indicatorColor: Colors.transparent,
            // tabAlignment: TabAlignment.start,
            indicatorSize: TabBarIndicatorSize.tab,
            splashFactory: NoSplash.splashFactory,
            dividerColor: Colors.transparent,
            indicator: BoxDecoration(
                color: context.theme.cardColor.withOpacity(.2),
                borderRadius: 4.radius),
            labelStyle: context.textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
            unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
            tabs: (widget.data?.playGroups ?? [])
                .mapIndexed((i, e) => Container(
                    decoration: BoxDecoration(
                        // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                        borderRadius: 4.radius),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: Text(e.alias ?? "")))
                .toList(),
            onTap: handleSelectTab,
          ),
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children: (widget.data?.playGroups ?? [])
                    .mapIndexed((index, ele) => SecondFontPlayItem(
                          data: ele,
                          onChange: widget.onChange,
                        ))
                    .toList()))
      ],
    );
  }
}

class SecondFontPlayItem extends StatefulWidget {
  final PlayGroup? data;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  const SecondFontPlayItem({super.key, this.data, this.onChange});

  @override
  State<SecondFontPlayItem> createState() => _SecondFontPlayItemState();
}

class _SecondFontPlayItemState extends State<SecondFontPlayItem> {
  //二字
  // circleCount = 2;
  // const nameArr = play0Name?.split('');
  // if (play0Name == '佰拾') {
  //   titleArr = [`第一球（${nameArr[0]}位）`, `第二球（${nameArr[1]}位）`];
  // } else if (play0Name == '佰个') {
  //   titleArr = [`第一球（${nameArr[0]}位）`, `第三球（${nameArr[1]}位）`];
  // } else {
  //   titleArr = [`第二球（${nameArr[0]}位）`, `第三球（${nameArr[1]}位）`];
  // }
  List<ArrObj> list = [];
  List<ArrObj> select = [];
  List<List<int>> resultList = [];
  init() {
    final play = widget.data?.plays?.first;
    final arr = widget.data?.plays?.first.name?.split("");
    if (play?.code == "BS") {
      list = (arr ?? [])
          .mapIndexed((index, e) => ArrObj(
              id: UuidUtil.to.v4(),
              key: "$index",
              // ignore: unnecessary_string_interpolations
              value: "${index == 0 ? '第一球（${arr?[0]}位）' : '第二球（${arr?[1]}位）'}",
              ext: List.generate(
                  10,
                  (index) => ArrObj(
                      id: UuidUtil.to.v4(), key: "$index", value: "$index"))))
          .toList();
    } else if (play?.code == "BG") {
      list = (arr ?? [])
          .mapIndexed((index, e) => ArrObj(
              id: UuidUtil.to.v4(),
              key: "$index",
              // ignore: unnecessary_string_interpolations
              value: "${index == 0 ? '第一球（${arr?[0]}位）' : '第三球（${arr?[1]}位）'}",
              ext: List.generate(
                  10,
                  (index) => ArrObj(
                      id: UuidUtil.to.v4(), key: "$index", value: "$index"))))
          .toList();
    } else {
      list = (arr ?? [])
          .mapIndexed((index, e) => ArrObj(
              id: UuidUtil.to.v4(),
              key: "$index",
              // ignore: unnecessary_string_interpolations
              value: "${index == 0 ? '第一球（${arr?[0]}位）' : '第三球（${arr?[1]}位）'}",
              ext: List.generate(
                  10,
                  (index) => ArrObj(
                      id: UuidUtil.to.v4(), key: "$index", value: "$index"))))
          .toList();
    }
    setState(() {});
  }

  handleSelect(ArrObj value) {
    final isHave = select.firstOrNullWhere((e) => e.id == value.id);
    if (isHave != null) {
      select.remove(value);
    } else {
      select.add(value);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    LogUtil.w(select);
    final result = list.map<List<int>>((e) {
      final list = [].cast<int>();
      // e.ext?.firstWhereOrNull((q) => select.firstOrNullWhere((w) => w.id == q.id) != null);
      e.ext?.forEach((e) {
        final isHave = select.firstWhereOrNull((q) => q.id == e.id);
        if (isHave != null) {
          list.add(int.parse(isHave.value ?? '0'));
        }
      });
      return list;
    }).toList();
    final genList = LotteryConfig.generateCombinationsList(result);
    LogUtil.w(genList);
    for (var e in genList) {
      LogUtil.w(e);
      groups.add(widget.data!.copyWith(
          plays: [widget.data!.plays!.first.copyWith(name: e.join(","))]));
    }
    resultList = genList;
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    init();
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = list
          .map((e) => EventUtils.robotRandom(e.ext?.length ?? 0, 1))
          .toList();
      select.clear();
      for (int i = 0; i < list.length; i++) {
        final item = list[i];
        final nums = result[i].first;
        select.add(item.ext![nums]);
      }
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      // 删除的数组
      final deleteArr = (value.plays?.first.alias?.split(",") ?? [])
          .map<int>((e) => int.tryParse(e) ?? 0)
          .toList();
      resultList = resultList
          .where((e) => jsonEncode(e) != jsonEncode(deleteArr))
          .toList();
      for (int i = 0; i < deleteArr.length; i++) {
        bool isDelete = false;
        // 判断每一位是否要被删除
        final deleteItemValueList = resultList
            .where((e) => jsonEncode(e) != jsonEncode(deleteArr))
            .map((e) => e[i]);
        isDelete = !deleteItemValueList.contains(deleteArr[i]);

        // 如果isDelete为真，则删除数组中对应的索引的项
        if (isDelete) {
          LogUtil.w("deleteArr____${deleteArr[i]} $i");
          final item = list[i].ext![deleteArr[i]];
          select.remove(item);
        }
      }
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            "赔率：${double.tryParse(widget.data?.plays?.first.odds ?? "")}",
            style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error, fontWeight: FontWeight.w600),
          ).paddingSymmetric(vertical: 10),
          ListView(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: list
                .mapIndexed((index, e) => Column(
                      children: [
                        Text(
                          e.value ?? "",
                          style: context.textTheme.titleMedium?.copyWith(
                              color: context.customTheme?.error,
                              fontWeight: FontWeight.w600),
                        ).paddingSymmetric(vertical: 10),
                        GridView(
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3, childAspectRatio: 1.8),
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: (e.ext ?? [])
                              .map((q) => Container(
                                    decoration: BoxDecoration(
                                        gradient: select.firstOrNullWhere(
                                                    (e) => e.id == q.id) !=
                                                null
                                            ? LinearGradient(colors: [
                                                context.customTheme
                                                        ?.selectStart ??
                                                    Colors.transparent,
                                                context.customTheme
                                                        ?.selectEnd ??
                                                    Colors.transparent,
                                              ])
                                            : null),
                                    child: UnconstrainedBox(
                                      child: Container(
                                        width: 40,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color:
                                                context.customTheme?.lottery2,
                                            borderRadius: 40.radius),
                                        alignment: Alignment.center,
                                        child: Text(q.value ?? ""),
                                      ).onTap(() => handleSelect(q)),
                                    ),
                                  ))
                              .toList(),
                        )
                      ],
                    ))
                .toList(),
          )
        ],
      ),
    );
  }
}
