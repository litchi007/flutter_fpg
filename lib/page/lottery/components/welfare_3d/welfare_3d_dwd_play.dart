import 'dart:convert';
import 'dart:math';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';

// 定位胆
class Welfare3dDwdPlay extends StatefulWidget {
  final GamePlayOddsModel? details;
  final PlayOdd? data;
  final String? type;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const Welfare3dDwdPlay(
      {super.key, this.onChange, this.data, this.details, this.type});

  @override
  State<Welfare3dDwdPlay> createState() => _Welfare3dDwdPlayState();
}

class _Welfare3dDwdPlayState extends State<Welfare3dDwdPlay>
    with TickerProviderStateMixin {
  int selectIndex = 0;
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: TabBar(
            controller: tabController,
            padding: EdgeInsets.zero,
            isScrollable: true,
            labelPadding: EdgeInsets.zero,
            indicatorColor: Colors.transparent,
            tabAlignment: TabAlignment.start,
            indicatorSize: TabBarIndicatorSize.tab,
            splashFactory: NoSplash.splashFactory,
            dividerColor: Colors.transparent,
            indicator: BoxDecoration(
                color: context.theme.cardColor.withOpacity(.2),
                borderRadius: 4.radius),
            labelStyle: context.textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
            unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
            tabs: (widget.data?.playGroups ?? [])
                .mapIndexed((i, e) => Container(
                    decoration: BoxDecoration(
                        // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                        borderRadius: 4.radius),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: Text(e.alias ?? "")))
                .toList(),
            onTap: handleSelectTab,
          ),
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children: (widget.data?.playGroups ?? [])
                    .mapIndexed((index, e) => Welfare3dDwdPlayItem(
                          data: e,
                          onChange: widget.onChange,
                        ))
                    .toList()))
      ],
    );
  }
}

class Welfare3dDwdPlayItem extends StatefulWidget {
  final PlayGroup? data;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  const Welfare3dDwdPlayItem({super.key, this.data, this.onChange});

  @override
  State<Welfare3dDwdPlayItem> createState() => _Welfare3dDwdPlayItemState();
}

class _Welfare3dDwdPlayItemState extends State<Welfare3dDwdPlayItem> {
  List<ArrObj> select = [];
  late final list = {
    // 复试
    "DWDFS": ArrObj(
      id: UuidUtil.to.v4(),
      key: widget.data?.alias,
      ext: List.generate(
          3,
          (index) => ArrObj(
              id: UuidUtil.to.v4(),
              key: index == 0
                  ? '第一球（百位）'
                  : index == 1
                      ? '第二球（十位）'
                      : '第三球（个位）',
              value: "$index",
              ext: List.generate(
                  10,
                  (i) => ArrObj(
                      id: UuidUtil.to.v4(), key: "$i", value: "$i")))).toList(),
    ),
    // 组选3
    "DWDZXS": ArrObj(
        id: UuidUtil.to.v4(),
        key: widget.data?.alias,
        value: "玩法提示：选一个二重号，一个单号组成一注。(单号号码不得与二重号重复)",
        ext: ["二重号", "单号"]
            .mapIndexed((index, e) => ArrObj(
                id: UuidUtil.to.v4(),
                key: e,
                value: "$index",
                ext: List.generate(
                    10,
                    (i) => ArrObj(
                        id: UuidUtil.to.v4(),
                        key: "$i",
                        value: "$index----$i"))))
            .toList()),
    // 组选六
    "DWDZXL": ArrObj(
        id: UuidUtil.to.v4(),
        key: widget.data?.alias,
        value: "玩法提示:任选3个号码组成一注(号码不重复)",
        ext: ["选号"]
            .mapIndexed((index, e) => ArrObj(
                id: UuidUtil.to.v4(),
                key: e,
                value: "$index",
                ext: List.generate(
                    10,
                    (i) =>
                        ArrObj(id: UuidUtil.to.v4(), key: "$i", value: "$i"))))
            .toList()),
    // 组选3复式
    "DWDZXSFS": ArrObj(
        id: UuidUtil.to.v4(),
        key: widget.data?.alias,
        value:
            "玩法提示：从0~9选择两个号码(或以上)，系统会自动将所选号码的所有组三组合(即三个号中有两个号相同)进行购买，若当期开奖号码的形态为组三且包含了号码，即中奖",
        ext: ["选号"]
            .mapIndexed((index, e) => ArrObj(
                id: UuidUtil.to.v4(),
                key: e,
                value: "$index",
                ext: List.generate(
                    10,
                    (i) =>
                        ArrObj(id: UuidUtil.to.v4(), key: "$i", value: "$i"))))
            .toList()),
    // 组选6复式
    "DWDZXLFS": ArrObj(
        id: UuidUtil.to.v4(),
        key: widget.data?.alias,
        value: "玩法提示：从0~9选择三个号码或多个号码投注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖",
        ext: ["选号"]
            .mapIndexed((index, e) => ArrObj(
                id: UuidUtil.to.v4(),
                key: e,
                value: "$index",
                ext: List.generate(
                    10,
                    (i) =>
                        ArrObj(id: UuidUtil.to.v4(), key: "$i", value: "$i"))))
            .toList()),
  }[widget.data?.plays?.first.code ?? "DWDFS"];

  handleSelect(ArrObj value) {
    switch (widget.data?.plays?.first.code ?? "DWDFS") {
      case "DWDFS":
        // 复试
        final isHave = select.firstWhereOrNull((e) => e.id == value.id);

        if (isHave == null) {
          select.add(value);
        } else {
          select.remove(value);
        }
        break;
      case "DWDZXS":
        //  组选3
        final isHave = select.firstWhereOrNull((e) => e.value == value.value);
        // final idHave = select.firstWhereOrNull((e) => e.id == value.id);
        if (isHave != null) {
          select.remove(value);
        } else {
          // 判断一下是不是一个类别
          final arr = value.value?.split("----");
          // 获取一选择的
          final selectArr = select
              .where((e) =>
                  e.value?.split("----")[0] == arr?[0] ||
                  e.value?.split("----")[1] == arr?[1])
              .toList();
          if (selectArr.isEmpty) {
            select.add(value);
          }
        }
        break;
      case "DWDZXL":
        // 组选6
        final isHave = select.firstWhereOrNull((e) => e.id == value.id);
        if (isHave == null && select.length < 3) {
          select.add(value);
        } else {
          select.remove(value);
        }
        break;
      case "DWDZXSFS":
      case "DWDZXLFS":
        // 组选3复式
        // 组选6复式
        final isHave = select.firstWhereOrNull((e) => e.id == value.id);
        if (isHave == null) {
          select.add(value);
        } else {
          select.remove(value);
        }
        break;
      default:
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    for (var e in select) {
      final play = Play(name: "${e.value}", alias: "${e.value}");
      plays.add(play);
      // groups.add(widget.data!.copyWith(
      //   plays: [play]
      // ));
    }
    // 组合
    final result = list!.ext!.map((e) {
      final resultList = [].cast<int>();
      for (var q in select) {
        final isHave = e.ext?.firstWhereOrNull((w) => w.id == q.id);
        LogUtil.w(isHave?.value);
        if (isHave != null) {
          resultList.add(int.tryParse(isHave.value ?? '0') ?? 0);
        }
      }
      // 排列组合
      return resultList;
    }).toList();

    final genList = LotteryConfig.generateCombinationsList(result);
    if (["DWDZXSFS", "DWDZXLFS"]
        .contains(widget.data?.plays?.first.code ?? "DWDFS")) {
      // groups.add
      // TODO:复式没搞懂注数怎么计算
      final result = select.map((e) => int.parse(e.value!)).toList();
      result.sort((a, b) => a - b);
      groups.add(widget.data!.copyWith(plays: [
        widget.data!.plays!.first.copyWith(
          name: result.join(","),
          // alias: combinationsList.map((e) => jsonEncode(e)).toList().join(",")
        )
      ]));
    } else {
      for (var e in genList) {
        groups.add(widget.data!.copyWith(
          plays: [
            Play(name: widget.data?.plays?.first.name, alias: e.join(","))
          ],
        ));
      }
    }

    LogUtil.w(groups);
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      select.clear();
      switch (widget.data?.plays?.first.code ?? "DWDFS") {
        case "DWDFS":
          // 复试
          final result = list!.ext!
              .map((e) => EventUtils.robotRandom(e.ext?.length ?? 0, 1))
              .toList();
          for (int i = 0; i < list!.ext!.length; i++) {
            final tem = list!.ext![i];
            for (var e in result[i]) {
              select.add(tem.ext![e]);
            }
          }

          break;
        case "DWDZXS":

          /// 组选3
          // final result1 =
          final result = list!.ext!
              .map((e) => EventUtils.robotRandom(e.ext?.length ?? 0, 1))
              .toList();
          for (int i = 0; i < list!.ext!.length; i++) {
            final tem = list!.ext![i];
            for (var e in result[i]) {
              select.add(tem.ext![e]);
            }
          }
          break;
        case "DWDZXL":

          /// 组选6
          List<ArrObj> allList = [];
          for (var e in (list?.ext ?? [].cast<ArrObj>())) {
            allList.addAll(e.ext ?? []);
          }
          final result = EventUtils.robotRandom(allList.length);
          select.addAll(result.map((e) => allList[e]).toList());
          break;
        case "DWDZXSFS":
        case "DWDZXLFS":
          // 组选3复式
          final allList = list?.ext?.first.ext ?? [];
          LogUtil.w(allList.length);
          final _random = Random().nextInt(5);
          final result =
              EventUtils.robotRandom(allList.length, max(2, _random));
          select.addAll(result.map((e) => allList[e]).toList());
          break;
      }
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      // 删除的数组
      final deleteArr = value.plays?.first.alias?.split(",") ?? [];
      // 组合
      final result = list!.ext!.map((e) {
        final resultList = [].cast<ArrObj>();
        for (var q in select) {
          final isHave = e.ext?.firstWhereOrNull((w) => w.id == q.id);
          if (isHave != null) {
            resultList.add(isHave);
          }
        }
        // 排列组合
        return resultList;
      }).toList();
      LogUtil.w(result);
      // 遍历result 找出 没有的那一项
      for (int i = 0; i < deleteArr.length; i++) {
        final tem = deleteArr[i];
        final isHave = result[i].firstWhereOrNull((e) => e.value == tem);
        LogUtil.w("tem___$tem ");
        LogUtil.w(result[i]);

        /// 第某项已经没有这个数据了，找到ext里的 删除
        if (isHave == null) {
          final currentList = list?.ext?[i].ext;
          final current = currentList?.firstWhereOrNull((e) => e.value == tem);
          select.remove(current);
        }
      }
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Text(
            "赔率： ${double.tryParse(widget.data?.plays?.first.odds ?? '0')}",
            style: context.textTheme.titleMedium?.copyWith(
                color: context.customTheme?.error, fontWeight: FontWeight.w600),
          ).paddingSymmetric(vertical: 10),
          // 玩法
          Visibility(
              visible: (list?.value ?? "").isNotEmpty,
              child: Text(list?.value ?? "")),
          ListView.builder(
              padding: EdgeInsets.zero,
              itemCount: list?.ext?.length ?? 0,
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                final item = list?.ext?[index];
                return Column(
                  children: [
                    Text(
                      item?.key ?? '',
                      style: context.textTheme.titleMedium?.copyWith(
                          color: context.customTheme?.error,
                          fontWeight: FontWeight.w600),
                    ).paddingSymmetric(vertical: 10),
                    GridView(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3, childAspectRatio: 1.8),
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      children: (item?.ext ?? []).mapIndexed((i, e) {
                        return Container(
                          color: select.firstWhereOrNull(
                                      (value) => e.id == value.id) !=
                                  null
                              ? context.customTheme?.error
                              : null,
                          child: UnconstrainedBox(
                            child: Container(
                                alignment: Alignment.center,
                                width: 40,
                                height: 40,
                                decoration: BoxDecoration(
                                    borderRadius: 40.radius,
                                    color: context.customTheme?.lottery2),
                                child: Text("${e.key}")),
                          ),
                        ).onTap(() => handleSelect(e));
                      }).toList(),
                    )
                  ],
                );
              })
        ],
      ),
    );
  }
}
