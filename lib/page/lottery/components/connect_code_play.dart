// ignore: dangling_library_doc_comments
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/page/lottery/components/time_lottery/time_lottery_1_position_play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';
import 'package:get/get.dart';

/// 连码

class ConnectCodePlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const ConnectCodePlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<ConnectCodePlay> createState() => _ConnectCodePlayState();
}

class _ConnectCodePlayState extends State<ConnectCodePlay>
    with TickerProviderStateMixin {
  // 需要回传给父组件的数据
  List<Play> select = [];
  List<String> selectId = [];
  int selectIndex = 0;
  late final tabController =
      TabController(length: (widget.data?.playGroups ?? []).length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  onChange(List<Play> plays, List<PlayGroup> data) {
    widget.onChange?.call(plays, data);
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          controller: tabController,
          padding: EdgeInsets.zero,
          isScrollable: true,
          labelPadding: EdgeInsets.zero,
          indicatorColor: Colors.transparent,
          tabAlignment: TabAlignment.start,
          indicatorSize: TabBarIndicatorSize.label,
          splashFactory: NoSplash.splashFactory,
          dividerColor: Colors.transparent,
          indicator: BoxDecoration(
              color: context.theme.cardColor.withOpacity(.2),
              borderRadius: 4.radius),
          labelStyle: context.textTheme.bodyLarge?.copyWith(
            fontWeight: FontWeight.bold,
          ),
          unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
          tabs: (widget.data?.playGroups ?? [])
              .mapIndexed((i, e) => Container(
                  decoration: BoxDecoration(
                      // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                      borderRadius: 4.radius),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Text(e.alias ?? "")))
              .toList(),
          onTap: handleSelectTab,
        ),
        Expanded(
          child: TabBarView(
              controller: tabController,
              children: (widget.data?.playGroups ?? [])
                  .mapIndexed((i, e) => ConnectCodePlayItem(
                      details: widget.details, data: e, onChange: onChange))
                  .toList()),
        )
      ],
    );
  }
}

class ConnectCodePlayItem extends StatefulWidget {
  final PlayGroup? data;
  final GamePlayOddsModel? details;
  final Function(List<Play>, List<PlayGroup>)? onChange;
  const ConnectCodePlayItem(
      {super.key, this.data, this.details, this.onChange});
  @override
  State<ConnectCodePlayItem> createState() => _ConnectCodePlayItemState();
}

class _ConnectCodePlayItemState extends State<ConnectCodePlayItem> {
  List<PlayGroup> selectGroupList = [];

  late final list = List.generate(
      50 * (widget.data?.plays?.length ?? 1),
      (index) => ArrObj(
          value: "${index % 50}", key: "$index", id: UuidUtil.to.v4())); // 列表

  List<ArrObj> selectList = [];

  handleSelect(ArrObj value) {
    final isHave = selectList.firstWhereOrNull((e) => e.id == value.id);
    if (isHave != null) {
      selectList.remove(value);
    } else {
      selectList.add(value);
    }
    setState(() {});
    handleChange();
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    final result = LotteryConfig.threeBthGenRandom(
        selectList.map((e) => int.tryParse(e.value ?? '0') ?? 0).toList());
    LogUtil.w("result____$result");
    // widget.onChange?.call(plays, selectGroupList);
    if (selectList.length < 3) {
      widget.onChange?.call([], []);
      return;
    }
    for (var e in selectList) {
      final play =
          widget.data!.plays![(int.tryParse(e.value ?? "0") ?? 0) ~/ 50];
      plays.add(play);
    }

    groups.add(widget.data!.copyWith(plays: [
      widget.data!.plays!.first.copyWith(
        name: selectList.map((e) => e.value).join(","),
        alias: selectList.map((e) => e.value).join(","),
      )
    ]));
    widget.onChange?.call(plays, groups);
  }

  init() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }

      /// 随机group，在随机plays
      // final result = LotteryConfig.robotRandomSelect([widget.data ?? const PlayGroup()], 1);
      selectGroupList = [];
      final result = EventUtils.robotRandom(list.length);
      // final
      selectList = [];
      selectList.addAll(result.map((e) => list[e]).toList());
      LogUtil.w(widget.data?.plays);
      final plays = [].cast<Play>();
      for (var e in result) {
        plays.add(widget.data!.plays![e ~/ 50]);
        selectGroupList
            .add(widget.data!.copyWith(plays: [widget.data!.plays![e ~/ 50]]));
      }
      LogUtil.w(plays);
      handleChange();
      // final plays = widget.data?.plays
      // selectList = result.currentPlay ?? [];
      //   plays: selectList
      // ) ??const PlayGroup()]);
      setState(() {});
      // selectList.addAll(result.currentPlay);

      // selectGroupList =
    }, time: 0.milliseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      setState(() {
        selectList.clear();
      });
    }, time: 0.milliseconds);
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  String _buildOddds() {
    String result = "";
    widget.data?.plays?.forEach((e) {
      result += "${double.tryParse(e.odds ?? "0")}/";
    });
    return result.endsWith("/")
        ? result.substring(0, result.length - 1)
        : result;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.data?.plays?.length ?? 0,
      itemBuilder: (BuildContext context, int index) {
        final item = widget.data?.plays?[index];
        return Column(
          children: [
            Text(
              item?.name ?? '',
              style: context.textTheme.titleMedium?.copyWith(
                  color: context.customTheme?.error,
                  fontWeight: FontWeight.w600),
            ).paddingSymmetric(vertical: 10),
            GridView(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, childAspectRatio: 1.2),
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: list
                  .sublist((index * 50) + 1, (index + 1) * 50)
                  .mapIndexed((index, e) {
                return Container(
                  color:
                      selectList.firstWhereOrNull((q) => q.id == e.id) != null
                          ? context.customTheme?.leftActive
                          : null,
                  child: Column(
                    children: [
                      Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: LotteryConfig.getBallBg(
                                    Play(name: "${e.value}"), widget.details))),
                        alignment: Alignment.center,
                        child: Text("${e.value}",
                            style: context.textTheme.bodyMedium?.copyWith(
                                // color: context.customTheme?.reverseFontColor,
                                fontWeight: FontWeight.bold)),
                      ),
                      Text(_buildOddds(),
                              style: context.textTheme.bodyMedium?.copyWith(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  color: selectList.firstWhereOrNull(
                                              (q) => q.id == e.id) !=
                                          null
                                      ? context.customTheme?.reverseFontColor
                                      : null))
                          .paddingOnly(top: 5)
                    ],
                  ).onTap(() => handleSelect(e)),
                );
              }).toList(),
            )
          ],
        );
      },
    );
  }
}
