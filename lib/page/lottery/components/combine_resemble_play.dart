import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:get/get.dart';

// 合肖
class CombineResemblePlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const CombineResemblePlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<CombineResemblePlay> createState() => _CombineResemblePlayState();
}

class _CombineResemblePlayState extends State<CombineResemblePlay> {
  List<String> select = [];
  handleSelect(String value) {
    final isHave = select.firstWhereOrNull((e) => e == value);
    if (select.length < 11) {
      if (isHave == null) {
        select.add(value);
      } else {
        select.remove(value);
      }
      handleChange();
      setState(() {});
    }
  }

  handleChange() {
    List<Play> plays = [];
    List<PlayGroup> groups = [];
    if (select.length > 1) {
      for (var e in select) {
        final items = ((widget.details?.setting?.zodiacNums ?? [])
                .firstOrNullWhere((e1) => e1.name == e)
                ?.nums ??
            []);
        final play = Play(name: e, alias: items.join(","));
        plays.add(play);
      }
      final itemName = "合肖${select.length}";
      final currentPlay = widget.data?.playGroups?.first.plays
          ?.firstWhereOrNull((e) => e.name == itemName);

      groups.add(widget.data!.playGroups!.first.copyWith(plays: [
        currentPlay!.copyWith(
          // alias: plays.map((e) => e.name).join(","),
          name: plays.map((e) => e.name).join(","),
        )
      ]));
    }

    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result =
          EventUtils.robotRandom(widget.details?.setting?.zodiacs?.length ?? 0);
      select.clear();
      select.addAll(
          result.map((e) => widget.details?.setting?.zodiacs?[e] ?? ''));
      handleChange();
      setState(() {});
    }, time: 0.microseconds);

    debounce(GlobalService.to.deleteGroup, (value) {
      if (!mounted) {
        return;
      }
      setState(() {
        select = [];
      });
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.details?.setting?.zodiacs?.length ?? 0,
        itemBuilder: (context, index) {
          final item = widget.details?.setting?.zodiacs?[index];
          return Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        color: context.theme.dividerColor, width: 1)),
                gradient: select.firstWhereOrNull((e1) => e1 == item) != null
                    ? LinearGradient(colors: [
                        context.customTheme?.selectStart ?? Colors.transparent,
                        context.customTheme?.selectEnd ?? Colors.transparent,
                      ])
                    : null),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("$item",
                    style: context.textTheme.bodyMedium?.copyWith(
                        color:
                            select.firstWhereOrNull((e1) => e1 == item) != null
                                ? context.customTheme?.reverseFontColor
                                : null)),
                Wrap(
                  spacing: 5,
                  children: ((widget.details?.setting?.zodiacNums ?? [])
                              .firstOrNullWhere((e) => e.name == item)
                              ?.nums ??
                          [])
                      .mapIndexed((i, e) {
                    return Container(
                      width: 30,
                      height: 30,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: LotteryConfig.getBallBg(
                                  Play(name: e), widget.details))),
                      child: Text(e,
                          style: context.textTheme.bodyMedium?.copyWith(
                            fontWeight: FontWeight.bold, fontSize: 12,
                            // color:  select.firstWhereOrNull((e1) => e1 == item) != null ? context.customTheme?.reverseFontColor : null
                          )),
                    );
                  }).toList(),
                )
              ],
            ).paddingSymmetric(vertical: 10, horizontal: 10),
          ).onTap(() => handleSelect(item!));
        });
  }
}
