import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_odd.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

// 趣味
class TastePlay extends StatefulWidget {
  final PlayOdd? data;
  final GamePlayOddsModel? details;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;
  final String? type;
  const TastePlay(
      {super.key, this.onChange, this.data, this.type, this.details});

  @override
  State<TastePlay> createState() => _TastePlayState();
}

class _TastePlayState extends State<TastePlay> with TickerProviderStateMixin {
  int selectIndex = 0;
  late final tabController =
      TabController(length: LotteryConfig.qwTabs.length, vsync: this)
        ..addListener(listener);

  listener() {
    setState(() {
      selectIndex = tabController.index;
    });
  }

  handleSelectTab(int index) {
    setState(() {
      selectIndex = index;
    });
  }

  @override
  void dispose() {
    tabController.removeListener(listener);
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: TabBar(
            controller: tabController,
            padding: EdgeInsets.zero,
            // isScrollable: true,
            labelPadding: EdgeInsets.zero,
            indicatorColor: Colors.transparent,
            // tabAlignment: TabAlignment.start,
            indicatorSize: TabBarIndicatorSize.tab,
            splashFactory: NoSplash.splashFactory,
            dividerColor: Colors.transparent,
            indicator: BoxDecoration(
                color: context.theme.cardColor.withOpacity(.2),
                borderRadius: 4.radius),
            labelStyle: context.textTheme.bodyLarge?.copyWith(
              fontWeight: FontWeight.bold,
            ),
            unselectedLabelStyle: context.textTheme.bodyLarge?.copyWith(),
            tabs: LotteryConfig.qwTabs
                .mapIndexed((i, e) => Container(
                    decoration: BoxDecoration(
                        // color:selectIndex == i ? context.theme.cardColor.withOpacity(.2) : null,
                        borderRadius: 4.radius),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: Text(e)))
                .toList(),
            onTap: handleSelectTab,
          ),
        ),
        Expanded(
            child: TabBarView(
                controller: tabController,
                children: LotteryConfig.qwTabs
                    .mapIndexed((index, e) => TastePlayItem(
                          data: widget.data,
                          index: index,
                          onChange: widget.onChange,
                        ))
                    .toList()))
      ],
    );
  }
}

class TastePlayItem extends StatefulWidget {
  final PlayOdd? data;
  final int? index;
  final Function(List<Play> list, List<PlayGroup> data)? onChange;

  const TastePlayItem({super.key, this.data, this.index, this.onChange});

  @override
  State<TastePlayItem> createState() => _TastePlayItemState();
}

class _TastePlayItemState extends State<TastePlayItem> {
  List<Play> select = [];
  late final list = widget.index == 0
      ? (widget.data?.playGroups ?? [])
          .where((e) =>
              int.tryParse(e.plays?.first.name ?? '0').runtimeType != int)
          .toList()
      : (widget.data?.playGroups ?? [])
          .where((e) =>
              int.tryParse(e.plays?.first.name ?? '0').runtimeType == int)
          .toList();

  handleSelect(PlayGroup item) {
    final isHave = select.firstWhereOrNull((e) => e.id == item.plays?.first.id);
    if (isHave != null) {
      select.remove(item.plays!.first);
    } else {
      select.add(item.plays!.first);
    }
    handleChange();
    setState(() {});
  }

  handleChange() {
    final plays = select;
    List<PlayGroup> groups = [];
    for (var e in select) {
      final group = widget.data?.playGroups
          ?.firstWhereOrNull((q) => q.plays?.first.id == e.id);
      if (group != null) {
        groups.add(group);
      }
    }
    widget.onChange?.call(plays, groups);
  }

  @override
  void initState() {
    debounce(GlobalService.to.robotRandom, (value) {
      if (!mounted) {
        return;
      }
      final result = LotteryConfig.robotRandomSelect(list);
      LogUtil.w(result);
      select.clear();
      select.addAll(result.currentPlay ?? []);
      handleChange();
      setState(() {});
    }, time: 0.milliseconds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          LotteryConfig.secondQwtabs[widget.index ?? 0],
          style: context.textTheme.titleMedium?.copyWith(
              color: context.customTheme?.error, fontWeight: FontWeight.w600),
        ).paddingSymmetric(vertical: 10),
        Expanded(
          child: GridView.builder(
              itemCount: list.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 2.2),
              itemBuilder: (context, index) {
                final item = list[index];
                return Container(
                  decoration: BoxDecoration(
                      gradient: select.firstWhereOrNull(
                                  (e) => e.id == item.plays?.first.id) !=
                              null
                          ? LinearGradient(colors: [
                              context.customTheme?.selectStart ??
                                  Colors.transparent,
                              context.customTheme?.selectEnd ??
                                  Colors.transparent,
                            ])
                          : null),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(item.alias ?? '',
                          style: context.textTheme.bodyMedium?.copyWith(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                      Text(double.tryParse(item?.plays?.first.odds ?? '-')
                              .toString())
                          .paddingOnly(left: 10)
                    ],
                  ).onTap(() => handleSelect(item)),
                );
              }),
        )
      ],
    );
  }
}
