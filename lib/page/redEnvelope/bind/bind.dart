import '../controller/controller.dart';
import 'package:get/get.dart';

class RedEnvelopeBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["type"];
    Get.lazyPut<RedEnvelopeController>(() => RedEnvelopeController(), tag: tag);
  }
}
