import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/models/redEnvelope/red_envelope_model.dart';
import 'package:fpg_flutter/page/redEnvelope/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:fpg_flutter/extension/string.ext.dart';

class RedEnvelopePage extends GetView<RedEnvelopeController> {
  const RedEnvelopePage({super.key});

  @override
  String? get tag => Get.parameters['type'];

  @override
  Widget build(BuildContext context) {
    controller.type = tag;

    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: const Color(0xffb89b65),
            header: (tag == "1" ? "红包记录" : "扫雷记录")),
        body: SafeArea(
            child: Column(
          children: [
            _buildHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context))
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35,
            child: Text("时间", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("类型", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("输赢", style: context.textTheme.bodySmall),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(() => SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: CustomHeader(
            builder: (BuildContext context, RefreshStatus? mode) {
              Widget body;
              if (mode == RefreshStatus.refreshing) {
                body = Text("刷新中..."); // "Refreshing..."
              } else if (mode == RefreshStatus.canRefresh) {
                body = Text("释放刷新"); // "Release to refresh"
              } else if (mode == RefreshStatus.completed) {
                body = Text("刷新完成"); // "Refresh complete"
              } else {
                body = Text("刷新失败"); // "Refresh failed"
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus? mode) {
              Widget body;
              if (mode == LoadStatus.loading) {
                body = Text("加载中..."); // Change 'Loading' to Chinese here
              } else if (mode == LoadStatus.failed) {
                body = Text("加载失败，请重试");
              } else if (mode == LoadStatus.canLoading) {
                body = Text("释放立即加载更多");
              } else {
                body = Text("没有更多数据");
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          controller: controller.refreshController,
          onRefresh: controller.onRefresh,
          onLoading: controller.onLoading,
          child: ListView.builder(
            itemBuilder: (c, i) =>
                _buildBodyItemWidget(context, controller.list[i]),
            itemCount: controller.list.length,
          ),
        ));
  }

  Widget _buildBodyItemWidget(BuildContext context, RedEnvelopeModel model) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 51,
                child: Column(
                  children: [
                    const Spacer(),
                    Text(controller.getDateStr(model.createTime),
                        style: context.textTheme.bodySmall),
                    Text(controller.getTimeStr(model.createTime),
                        style: context.textTheme.bodySmall),
                    const Spacer(),
                  ],
                ),
              ),
            ),
            Container(
              width: 0.5,
              height: 51,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
              alignment: Alignment.center,
              height: 51,
              child: Text(
                '${model.operateText}',
                style: context.textTheme.bodySmall,
                maxLines: 10,
                textAlign: TextAlign.center,
              ),
            )),
            Container(
              width: 0.5,
              height: 51,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 51,
                child: Text(controller.getAmountStr(model),
                    style: TextStyle(
                        color: controller.getAmountColor(model).color(),
                        fontSize: 12)),
              ),
            ),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    );
  }
}
