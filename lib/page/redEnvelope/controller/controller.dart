import 'package:fpg_flutter/api/chat/chat.http.dart';
import 'package:fpg_flutter/models/redEnvelope/red_envelope_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class RedEnvelopeController extends GetxController {
  static RedEnvelopeController get to => Get.find<RedEnvelopeController>();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  /// 页码
  int page = 1;

  /// 类型
  String? type;

  /// 记录列表
  late var list = [].cast<RedEnvelopeModel>().obs;

  /// 获取注单列表
  Future getChatRedBagLogPage(bool isRefresh) async {
    if (isRefresh) {
      page = 1;
    } else {
      page++;
    }

    DateTime today = DateTime.now();
    // 设置时分秒为0，获取今日开始的时间戳
    today = DateTime(today.year, today.month, today.day, 0, 0, 0, 0);
    String startTime = (today.millisecondsSinceEpoch ~/ 1000).toString();
    // 设置时分秒为23:59:59，获取今日结束的时间戳
    today = DateTime(today.year, today.month, today.day, 23, 59, 59, 999);
    String endTime = (today.millisecondsSinceEpoch ~/ 1000).toString();

    final res = await ChatHttp.chatRedBagLogPage(
      type ?? '1',
      page: page,
      startTime: startTime,
      endTime: endTime,
    );

    LogUtil.w(res.data);

    var resultList = res.data!.list ?? [];
    List<RedEnvelopeModel> tempList = [];
    if (page > 1) {
      tempList.addAll(list);
    }

    if (page == 1) {
      refreshController.refreshCompleted();
    }

    if (resultList.length < 20) {
      refreshController.loadNoData();
    } else {
      refreshController.loadComplete();
    }

    tempList.addAll(resultList);
    list.value = tempList;
  }

  Future onRefresh() async {
    getChatRedBagLogPage(true);
  }

  Future onLoading() async {
    getChatRedBagLogPage(false);
  }

  String getAmountColor(RedEnvelopeModel model) {
    var colorStr = '#F15C5F';
    if (model.amount != null &&
        (model.operate == '1' || model.operate == '4')) {
      colorStr = '#16AD58';
    }
    return colorStr;
  }

  String getAmountStr(RedEnvelopeModel model) {
    var amount = '';
    if (model.amount != null) {
      if (model.operate == '1' || model.operate == '4') {
        amount = '-${model.amount}';
      } else {
        amount = '+${model.amount}';
      }
    }
    return amount;
  }

  String getDateStr(String? timestampString) {
    int timestamp = int.parse(timestampString ?? '0');
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    String formattedDate = DateFormat('yyyy-MM-dd').format(date);
    return formattedDate;
  }

  String getTimeStr(String? timestampString) {
    int timestamp = int.parse(timestampString ?? '0');
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    String formattedDate = DateFormat('HH:mm:ss').format(date);
    return formattedDate;
  }

  @override
  void onReady() {
    getChatRedBagLogPage(true);
    super.onReady();
  }
}
