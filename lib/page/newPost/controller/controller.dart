import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/lhcdoc/lhcdoc.http.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/components/custom.emoji.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class NewPostController extends GetxController {
  static NewPostController get to => Get.find<NewPostController>();

  final textController = TextEditingController();
  final contentTextController = TextEditingController();
  final ImagePicker picker = ImagePicker();
  final imageList = [].cast<String>().obs;
  final showImgList = [].cast<Uint8List>().obs;
  onChange(String emoji) {
    contentTextController.text = "${contentTextController.text}$emoji";
  }

  handlePic() async {
    if (showImgList.length >= 3) {
      ToastUtils.show("最多上传3张图片");
      return;
    }
    final XFile? response = await picker.pickImage(source: ImageSource.gallery);

    if (response != null) {
      final File file = File(response.path);
      final bytes = file.readAsBytesSync();
      //final bytes = (await image.readAsBytes()).lengthInBytes;
      final size = file.readAsBytesSync().lengthInBytes;
      final kb = size / 1024;
      final mb = kb / 1024;
      if (mb > 2) {
        ToastUtils.show("文件需小与2MB");
        return;
      }
      // if (bytes.)
      String base64Image = "data:image/png;base64, ${base64Encode(bytes)}";
      imageList.add(base64Image);
      showImgList.add(bytes);
    }
  }

  handleRemoveImg(int index) {
    showImgList.removeAt(index);
    imageList.removeAt(index);
  }

  handleEmjoi() {
    Get.bottomSheet(CustomEmoji(
      onChange: onChange,
    ));
  }

  Future handleSubmit() async {
    final res = await LhcdocHttp.lhcdocPostContent(LhcdocPostContentParams(
      alias: "forum",
      title: textController.text,
      content: contentTextController.text,
      // images: imageList
    ));
    if (res.code == 0) {
      ToastUtils.showTip(CustomConfirmParams(title: res.msg));
      textController.text = "";
      contentTextController.text = "";
      imageList.clear();
      showImgList.clear();
    } else {
      ToastUtils.show(res.msg);
    }
  }
}
