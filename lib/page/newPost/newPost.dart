import 'dart:convert';

import 'package:dartx/dartx.dart';
import 'package:extended_text_field/extended_text_field.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.btn.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/page/expertPlanBet/expertPlanBet.dart';
import 'package:fpg_flutter/page/homeMenu/homeMenu.dart';
import 'package:fpg_flutter/page/longDragonTool/components/long_dragon_item.dart';
import 'package:fpg_flutter/page/newPost/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/text_utils.dart';
import 'package:get/get.dart';

class NewPostPage extends GetView<NewPostController> {
  const NewPostPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        background: context.customTheme?.lotteryAppBar,
        iconColor: context.customTheme?.reverseFontColor,
        actionMaxWidth: 105,
        actions: Row(
          children: [
            const Text("历史帖子"),
            GestureDetector(
              onTap: () {
                Get.to(() => HomeMenuPage(),
                    opaque: false,
                    fullscreenDialog: true,
                    transition: Transition.noTransition);
                // _HomeKey.currentState!.openEndDrawer();
              },
              child: AppImage.asset('menu_btn.png',
                      width: 30,
                      height: 30,
                      color: context.customTheme?.reverseFontColor)
                  .marginOnly(left: 10),
            )
          ],
        ),
        header: "发帖",
        textColor: context.customTheme?.reverseFontColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
              textAlign: TextAlign.start,
              controller: controller.textController,
              // placeholder: "请输入标题（32字以內）",
              maxLines: 4,
              maxLength: 32,
              textAlignVertical: TextAlignVertical.center,
              decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: "请输入标题（32字以內）",
                  contentPadding: EdgeInsets.all(10)),
            ),
            Container(
              height: 20,
              color: context.theme.cardColor.withOpacity(.1),
            ),
            ExtendedTextField(
              controller: controller.contentTextController,
              textAlignVertical: TextAlignVertical.center,
              maxLines: 12,
              specialTextSpanBuilder: MySpecialTextSpanBuilder(),
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: "请输入内容...",
                contentPadding: EdgeInsets.all(10),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              margin: const EdgeInsets.only(right: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  AppImage.network("${LotteryConfig.emojiBaseUrl}/1.gif")
                      .onTap(controller.handleEmjoi),
                  Icon(
                    Icons.image,
                    color: context.customTheme?.error,
                  ).marginOnly(left: 10).onTap(controller.handlePic)
                ],
              ),
            ),
            Obx(() => Row(
                  children: controller.showImgList
                      .mapIndexed((i, e) => Container(
                          alignment: Alignment.bottomLeft,
                          width: 110,
                          height: 110,
                          child: Stack(
                            children: [
                              Container(
                                  alignment: Alignment.bottomLeft,
                                  child: Image.memory(
                                    e,
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.fill,
                                  )),
                              Positioned(
                                right: 0,
                                child: Container(
                                        width: 20,
                                        height: 20,
                                        decoration: BoxDecoration(
                                          color: context.theme.cardColor
                                              .withOpacity(.9),
                                          borderRadius: 20.radius,
                                        ),
                                        alignment: Alignment.center,
                                        child: Icon(
                                          Icons.close_sharp,
                                          color: context
                                              .customTheme?.reverseFontColor,
                                          size: 16,
                                        ))
                                    .onTap(() => controller.handleRemoveImg(i)),
                              )
                            ],
                          )))
                      .toList(),
                )),
            CustomButton(
              onTap: controller.handleSubmit,

              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
              // height: 20,
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: context.customTheme?.lotteryAppBar,
                  borderRadius: 10.radius),
              child: Text("发布",
                  style: context.textTheme.bodyMedium
                      ?.copyWith(color: context.customTheme?.reverseFontColor)),
            ),
            Text.rich(TextSpan(text: "提示：", children: [
              TextSpan(
                  text: "请勿发布联系方式和广告,一经发现直接封号！",
                  style: context.textTheme.bodyMedium
                      ?.copyWith(color: context.customTheme?.error))
            ]))
          ],
        ),
      ),
    );
  }
}
