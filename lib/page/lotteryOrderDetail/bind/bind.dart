import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryOrderDetailBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<LotteryOrderDetailController>(
        () => LotteryOrderDetailController());
  }
}
