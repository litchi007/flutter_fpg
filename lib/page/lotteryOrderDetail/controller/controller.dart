import 'package:fpg_flutter/api/report/report_http.dart';
import 'package:fpg_flutter/models/lottery_my_bet_model/lottery_my_bet_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class LotteryOrderDetailController extends GetxController {
  static LotteryOrderDetailController get to =>
      Get.find<LotteryOrderDetailController>();

  final loading = true.obs;
  final betModel = const LotteryMyBetModel().obs;
  late String betId = '';

  Future getReportGetUserRecentBet() async {
    final res = await ReportHttp.reportGetUserRecentBet(tag: 1, betId: betId);
    var firstModel = res.models?.first;
    if (firstModel != null) {
      betModel.value = firstModel;
      LogUtil.w(betModel);
    }
    loading.value = false;
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null) {
      betId = arguments.toString();
    }
    getReportGetUserRecentBet();
    super.onInit();
  }
}
