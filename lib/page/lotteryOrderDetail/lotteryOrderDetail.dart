import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/models/lottery_my_bet_model/lottery_my_bet_model.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/page/lotteryOrderDetail/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LotteryOrderDetailPage extends GetView<LotteryOrderDetailController> {
  LotteryOrderDetailPage({super.key});

  final tags = Get.parameters["null"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: '注单详情'),
        body: SafeArea(
            child: Obx(() => CustomLoading(
                  status: controller.loading.value,
                  child: Obx(() => Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                            height: 92,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                        width: 60,
                                        height: 60,
                                        child: AppImage.network(
                                            '${controller.betModel.value.logo}',
                                            fit: BoxFit.cover,
                                            errorWidget: AppImage.asset(
                                              'defaultIcon.png',
                                            )),
                                        decoration: ShapeDecoration(
                                            color: Colors.white,
                                            shadows: [
                                              BoxShadow(
                                                  color: Colors.grey,
                                                  blurRadius: 6)
                                            ],
                                            // image: DecorationImage(
                                            //     fit: BoxFit.cover,
                                            //     image: _getLogoImage(controller
                                            //         .betModel.value.logo)),
                                            shape: CircleBorder(
                                                side: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey)))),
                                    const SizedBox(width: 20),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${controller.betModel.value.title}',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16),
                                        ),
                                        const SizedBox(height: 5),
                                        Text(
                                          '第${controller.betModel.value.issue}期',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                Text(
                                  _getStatusStr(),
                                  style: TextStyle(color: _getStatusColor()),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 1,
                            color: context.customTheme?.lotteryPopupDivColor,
                          ),
                          Row(children: [
                            Text(
                              '投注时间',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                            const SizedBox(width: 20),
                            Text('${controller.betModel.value.openTime}'),
                          ]).paddingOnly(top: 10, bottom: 10),
                          Container(
                            height: 1,
                            color: context.customTheme?.lotteryPopupDivColor,
                          ),
                          Row(children: [
                            Text(
                              '投注单号',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                            const SizedBox(width: 20),
                            Text('${controller.betModel.value.orderNo}'),
                          ]).paddingOnly(top: 10, bottom: 10),
                          Container(
                            height: 1,
                            color: context.customTheme?.lotteryPopupDivColor,
                          ),
                          Row(children: [
                            Text(
                              '投注金额',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                            const SizedBox(width: 20),
                            Text('¥${controller.betModel.value.money}'),
                          ]).paddingOnly(top: 10, bottom: 10),
                          Container(
                            height: 1,
                            color: context.customTheme?.lotteryPopupDivColor,
                          ),
                          Row(children: [
                            Text(
                              '派送金额',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                            const SizedBox(width: 20),
                            Text('¥${controller.betModel.value.bonus}'),
                          ]).paddingOnly(top: 10, bottom: 10),
                          Container(
                            height: 1,
                            color: context.customTheme?.lotteryPopupDivColor,
                          ),
                          Row(children: [
                            Text(
                              '开奖号码',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                            const SizedBox(width: 20),
                            Text(_getLotteryNoStr(),
                                style: TextStyle(
                                    color: Colors.black, fontSize: 14)),
                          ]).paddingOnly(top: 10, bottom: 10),
                          Container(
                            height: 1,
                            color: context.customTheme?.lotteryPopupDivColor,
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.fromLTRB(20, 20, 0, 10),
                            child: Text(
                              '我的投注',
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                          ),
                          Stack(
                            children: [
                              AppImage.network(img_images('paper'),
                                  width: 335, height: 105, fit: BoxFit.fill),
                              Positioned(
                                  top: 30,
                                  left: 25,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${controller.betModel.value.groupName}- ${controller.betModel.value.playName}',
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 14),
                                      ),
                                      Text(
                                        '奖金: ¥${controller.betModel.value.bonus}',
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 14),
                                      )
                                    ],
                                  ))
                            ],
                          )
                        ],
                      ).paddingOnly(left: 20, right: 20)),
                ))));
  }

  /// 获取logo
  ImageProvider _getLogoImage(String? logo) {
    if (logo != null && logo.isNotEmpty) {
      return NetworkImage(logo);
    } else {
      return const AssetImage('assets/images/loading.gif');
    }
  }

  /// 获取状态
  String _getLotteryNoStr() {
    LotteryMyBetModel model = controller.betModel.value;
    String lotteryNo = model.lotteryNo ?? '';
    return lotteryNo.isNotEmpty ? lotteryNo : '等待开奖';
  }

  /// 获取状态
  String _getStatusStr() {
    LotteryMyBetModel model = controller.betModel.value;
    String isWin = model.isWin.toString();
    if (isWin == '1') {
      return '+¥${model.bonus}\n${model.msg}';
    } else {
      return model.msg ?? '';
    }
  }

  /// 获取状态颜色
  Color _getStatusColor() {
    LotteryMyBetModel model = controller.betModel.value;
    String status = model.status.toString();
    String isWin = model.isWin.toString();

    if (status == '0') {
      return '#008000'.color();
    } else if (status == '1' && isWin == '1') {
      return '#FF0000'.color();
    } else {
      return '#333333'.color();
    }
  }
}
