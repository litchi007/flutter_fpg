import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_rank_model.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_type.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/drop_down_utils.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class ExpertPlanController extends GetxController {
  static ExpertPlanController get to => Get.find<ExpertPlanController>();

  Timer? _timer;
  bool _isDisposed = false;
  late String gameId = '1';
  final isShowed = 1.obs;
  final autoFollowStatus = 0.obs;
  late List<String> codeArea;
  final ycLength = ''.obs;
  late List<Position> positionList;
  final position = Position().obs;
  late List<ExpertPlanGameModel> gameArr;
  final gameModel = ExpertPlanGameModel().obs;
  final getNextIsu = (const GameNextIssueModel()).obs;

  /// 玩法和赔率
  late Map<String, Play> odds = {};
  Timer? closingTimer;
  final closeingTimeNumber = (0).obs;
  final openTimeNumber = 0.obs;

  /// 专家排名列表
  final rankList = [].cast<ExpertPlanRankModel>().obs;

  /// 专家追号列表
  final dataList = [].cast<ExpertPlanDataModel>().obs;

  /// tabs
  final tabs = ['跟单计划', '专家排名'];

  /// 选中的tab
  final selectedTab = 0.obs;

  /// 加载状态
  final loading = true.obs;

  /// 选中tab
  handleSelectTab(int value) {
    selectedTab.value = value;
    if (selectedTab.value == 1) {
      // getUserExpertRank();
    } else {}
  }

  handleSelectedPosition(String value) {
    loading.value = true;
    position.value = positionList.firstWhere((e) => e.value == value);
    getUserExpertDataList();
    getUserExpertRank();
  }

  handleSelectedLength(String value) {
    loading.value = true;
    ycLength.value = value;
    getUserExpertDataList();
    getUserExpertRank();
  }

  getNextIsuData() async {
    try {
      final res = await GameHttp.gameNextIssue(gameModel.value.gameId);
      if (res.data != null) {
        getNextIsu.value = res.data!;

        /// 预测位置
        positionList =
            DropDownUtils.getLeftDropDownOptions(res.data!.gameType ?? '');
        if (position.value.value == null && position.value.name == null) {
          position.value = positionList.first;
        }

        getExpertPlanData();

        final closeTime = DateTime.tryParse(res.data?.curCloseTime ?? "")
                ?.millisecondsSinceEpoch ??
            0;
        final curTime = DateTime.tryParse(res.data?.serverTime ?? "")
                ?.millisecondsSinceEpoch ??
            0;
        final curOpenTime = DateTime.tryParse(res.data?.curOpenTime ?? "")
                ?.millisecondsSinceEpoch ??
            0;
        final closeDifference = closeTime - curTime;
        final openDifference = curOpenTime - curTime;
        closeingTimeNumber.value =
            int.parse((closeDifference ~/ 1000).toStringAsFixed(0));
        openTimeNumber.value =
            int.parse((openDifference ~/ 1000).toStringAsFixed(0));

        closingTimer?.cancel();
        closingTimer = Timer.periodic(1000.milliseconds, (value) async {
          if (openTimeNumber < 0) {
            closingTimer?.cancel();
            return;
          }

          /// 封盘倒计时
          if (closeingTimeNumber.value > 0) {
            closeingTimeNumber.value -= 1;
          }

          /// 开奖倒计时
          if (openTimeNumber.value > 0) {
            openTimeNumber.value -= 1;
          }
          if (closeingTimeNumber.value <= 0 && openTimeNumber.value <= 0) {
            await EventUtils.sleep(3.seconds);
            closingTimer?.cancel();
            getNextIsuData();
          }
        });
      }
    } catch (e) {
      LogUtil.e(e);
    }
  }

  /// 获取专家计划相关数据
  getExpertPlanData() {
    /// 获取专家追号数据
    getUserExpertDataList();
    // /// 获取专家排名数据
    getUserExpertRank();

    /// 赔率
    getPlayOddsData();
  }

  /// 获取专家追号开关数据
  getUserExpertSwitch() {
    UserHttp.userExpertSwitch(gameId).then((res) {
      gameArr = res.data?.gameArr ?? [];
      if (gameId == '1') {
        if (gameArr.isNotEmpty) {
          gameModel.value = res.data!.gameArr!.first;
          gameId = gameModel.value.gameId ?? '1';
          getUserExpertSwitch();
        }
      } else {
        isShowed.value = res.data?.enable ?? 0;
        autoFollowStatus.value = res.data?.autoFollowStatus ?? 0;

        /// 预测长度
        codeArea = res.data?.codeArea ?? [];
        ycLength.value = codeArea.first;

        if (isShowed.value == 1) {
          getNextIsuData();
        }
      }
    }).catchError((error) {
      LogUtil.w(error);
    });
  }

  /// 获取专家追号数据
  getUserExpertDataList() {
    UserHttp.userExpertDataList(gameModel.value.gameId,
            getNextIsu.value.curIssue, getNextIsu.value.preIssue,
            ycLength: ycLength.value, position: position.value.value)
        .then((res) {
      var list = res.models ?? [];
      if (list.isEmpty) {
        getUserExpertDataList();
      } else {
        dataList.value = list;
        loading.value = false;
        var first = list.first;
        if (!(first.winning != '0' && first.netAmount != '0')) {
          _isDisposed = true;
        } else {
          _isDisposed = false;
        }
      }
    });
  }

  /// 获取专家排行数据
  getUserExpertRank() {
    UserHttp.userExpertRank(gameModel.value.gameId, getNextIsu.value.preIssue,
            ycLength: ycLength.value, position: position.value.value)
        .then((res) {
      rankList.value = res.models ?? [];
    }).catchError((error) {
      LogUtil.w(error);
    });
  }

  /// 获取玩法赔率
  getPlayOddsData() {
    GameHttp.gamePlayOdds(gameModel.value.gameId ?? '').then((res) {
      Map<String, Play> oddsMap = {};
      var playOdds = res.data?.playOdds ?? [];
      for (var playOdd in playOdds) {
        var playGroups = playOdd.playGroups ?? [];
        var name = playOdd.name ?? '';
        for (var playGroup in playGroups) {
          var plays = playGroup.plays ?? [];
          for (var play in plays) {
            oddsMap[name + (play.name ?? '')] = play;
          }
        }
      }
      odds = oddsMap;
    }).catchError((error) {});
  }

  /// 选择游戏
  handleSelectedGameModel(ExpertPlanGameModel model) {
    if (model.gameId != gameModel.value.gameId) {
      gameModel.value = model.copyWith();
      gameId = gameModel.value.gameId ?? '1';
      position.value = Position();
      loading.value = true;
      getUserExpertSwitch();
    }
  }

  /// 获取开奖号码背景色
  Color? getNumBackColor(String value) {
    if (getNextIsu.value.gameType == GameType.luckyAirship ||
        getNextIsu.value.gameType == GameType.racing) {
      return value.getNumColor();
    } else if (getNextIsu.value.gameType == GameType.timeLottery ||
        getNextIsu.value.gameType == GameType.elevenSelectedFive ||
        getNextIsu.value.gameType == GameType.sortThree ||
        getNextIsu.value.gameType == GameType.gdkl10 ||
        getNextIsu.value.gameType == GameType.sevenStarLottery ||
        getNextIsu.value.gameType == GameType.pcEggs) {
      return '#63a0d9'.color();
    }
    return Colors.white;
  }

  /// 获取文字颜色
  String getTextColorStr() {
    var colorStr = '#333333';
    if (getNextIsu.value.gameType != GameType.markSixLottery) {
      colorStr = '#ffffff';
    }
    return colorStr;
  }

  @override
  void onInit() {
    var arguments = Get.arguments;
    if (arguments != null && arguments is Lottery) {
      final newGameName = arguments.shortName ?? '';
      final newGameId = arguments.id ?? '';
      gameId = newGameId;
      gameModel.value =
          ExpertPlanGameModel(gameId: newGameId, gameName: newGameName);
    }

    GlobalService.to.fetchUserBalance();
    getUserExpertSwitch();
    super.onInit();

    _timer = Timer.periodic(const Duration(seconds: 5), (Timer timer) {
      if (_isDisposed) {
        getUserExpertDataList();
      }
    });
  }

  @override
  void onClose() {
    super.onClose();
    _isDisposed = false;
    closingTimer?.cancel();
    _timer?.cancel();
    _timer = null;
  }
}
