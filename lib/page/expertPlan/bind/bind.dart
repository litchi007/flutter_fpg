import '../controller/controller.dart';
import 'package:get/get.dart';

class ExpertPlanBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ExpertPlanController>(() => ExpertPlanController());
  }
}
