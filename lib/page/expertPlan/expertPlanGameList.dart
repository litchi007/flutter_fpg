import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ExpertPlanGameListPage extends GetView {
  final List<ExpertPlanGameModel> gameArr;

  const ExpertPlanGameListPage(this.gameArr, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent, // 设置背景透明
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                color: Colors.transparent,
              ),
            ),
            Positioned(
                top: MediaQuery.of(context).padding.top + 56,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 20 + (gameArr.length / 3).ceil() * 50,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3), // 阴影颜色
                        spreadRadius: 0, // 阴影扩散半径
                        blurRadius: 10, // 模糊半径
                        offset: Offset(0, 5), // 阴影偏移量
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Expanded(
                          child: SingleChildScrollView(
                              child: GridView.builder(
                        shrinkWrap: true,
                        itemCount: gameArr.length,
                        physics: NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          mainAxisSpacing: 10,
                          crossAxisSpacing: 8,
                          childAspectRatio: 3,
                        ),
                        itemBuilder: (context, index) {
                          ExpertPlanGameModel model = gameArr[index];
                          return Container(
                            decoration: BoxDecoration(
                              // 设置渐变色背景
                              gradient: LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    '#0084bd'.color(),
                                    '#0084bd'.color()
                                  ]),
                              border: Border.all(
                                color: '#C9C9C9'.color(),
                                // 边框颜色
                                width: 1.0, // 边框宽度
                              ),
                              borderRadius: BorderRadius.circular(4), // 设置圆角
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              '${model.gameName}',
                              textAlign: TextAlign.center,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ).onTap(() {
                            Get.back(result: model);
                          });
                        },
                      ))),
                      SizedBox(height: 10)
                    ],
                  ),
                ))
          ],
        ));
  }
}
