import 'package:dartx/dartx.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_rank_model.dart';
import 'package:fpg_flutter/page/expertPlan/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/page/expertPlan/expertPlanGameList.dart';
import 'package:fpg_flutter/page/expertPlanAutoBet/expertPlanAutoBet.dart';
import 'package:fpg_flutter/page/expertPlanAutoBetDetail/expertPlanAutoBetDetail.dart';
import 'package:fpg_flutter/page/expertPlanBet/expertPlanBet.dart';
import 'package:fpg_flutter/page/expertPlanHistory/expertPlanHistory.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class ExpertPlanPage extends GetView<ExpertPlanController> {
  const ExpertPlanPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              onPressed: () => Get.back(),
            ),
            actions: [
              SizedBox(
                width: 44,
              )
            ],

            /// 是为了让标题居中
            title: Obx(() {
              return controller.gameModel.value.gameName != null
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(controller.gameModel.value.gameName ?? ''),
                        Icon(Icons.arrow_drop_down, color: '#FFBB00'.color())
                      ],
                    ).onTap(() {
                      showGameList();
                    })
                  : SizedBox();
            }),
            flexibleSpace: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    '#b89b65'.color(),
                    '#b89b65'.color(),
                  ],
                )))),
        body: SafeArea(child: Obx(() {
          return controller.isShowed.value == 0
              ? Center(
                  child: Text('跟单计划未开启'),
                )
              : _getCurrentPage();
        })));
  }

  /// 跟单计划页面
  Widget _getCurrentPage() {
    return Column(
      children: [
        Container(
            padding: EdgeInsets.all(10),
            height: 56,
            color: Colors.white,
            child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: '#d8d8d8'.color(),
                    width: 0.5,
                  ),
                  borderRadius: BorderRadius.circular(4), // 设置圆角
                ),
                child: Obx(() => Row(
                      children: controller.tabs
                          .mapIndexed((i, e) => Expanded(
                                child: Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: controller.selectedTab.value == i
                                        ? '#aeaeae'.color()
                                        : Colors.white,
                                    borderRadius: i == 0
                                        ? BorderRadius.only(
                                            topLeft:
                                                Radius.circular(4), // 左上角圆角
                                            bottomLeft:
                                                Radius.circular(4), // 左下角圆角
                                          )
                                        : BorderRadius.only(
                                            topRight:
                                                Radius.circular(4), // 右上角圆角
                                            bottomRight:
                                                Radius.circular(4), // 右下角圆角
                                          ),
                                  ),
                                  child: Text(
                                    e,
                                    style: TextStyle(
                                        color: controller.selectedTab.value == i
                                            ? Colors.white
                                            : '#333333'.color(),
                                        fontSize: 16),
                                  ),
                                ).onTap(() => controller.handleSelectTab(i)),
                              ))
                          .toList(),
                    )))),
        Expanded(child: Obx(() {
          return controller.selectedTab.value == 0
              ? _getExpertPlan()
              : _getExpertRank();
        }))
      ],
    );
  }

  /// 跟单计划
  Widget _getExpertPlan() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          color: '#f3f3f3'.color(),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PopupMenuButton<String>(
                    color: Colors.white,
                    child: Container(
                      padding: EdgeInsets.only(left: 2),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(width: 1, color: '#333333'.color()),
                      ),
                      child: Row(
                        children: [
                          Text(
                            '${controller.position.value.name ?? ''}',
                            style: TextStyle(
                                color: '#333333'.color(), fontSize: 14),
                          ),
                          Icon(
                            Icons.keyboard_arrow_down,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    itemBuilder: (c) =>
                        controller.positionList.mapIndexed((i, e) {
                      return CheckedPopupMenuItem(
                        value: e.value,
                        checked: controller.position.value.value == e.value,
                        child: Text('${e.name}'),
                      );
                    }).toList(),
                    onSelected: (value) {
                      controller.handleSelectedPosition(value);
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  PopupMenuButton<String>(
                    color: Colors.white,
                    child: Container(
                      padding: EdgeInsets.only(left: 2),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(width: 1, color: '#333333'.color()),
                      ),
                      child: Row(
                        children: [
                          Text(
                            controller.ycLength.value.isNotEmpty
                                ? '${controller.ycLength.value}码计划'
                                : '',
                            style: TextStyle(
                                color: '#333333'.color(), fontSize: 14),
                          ),
                          Icon(
                            Icons.keyboard_arrow_down,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    itemBuilder: (c) => controller.codeArea.mapIndexed((i, e) {
                      return CheckedPopupMenuItem(
                        value: e,
                        child: Text('${e}码计划'),
                        checked: controller.ycLength.value == e,
                      );
                    }).toList(),
                    onSelected: (value) {
                      controller.handleSelectedLength(value);
                    },
                  ),
                  Spacer(),
                  controller.autoFollowStatus.value == 1
                      ? Container(
                          padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                          decoration: BoxDecoration(
                            color: '#38c53f'.color(),
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.2), // 阴影颜色
                                spreadRadius: 0, // 阴影扩散半径
                                blurRadius: 5, // 模糊半径
                                offset: Offset(2, 2), // 阴影偏移量
                              ),
                            ],
                          ),
                          child: Text(
                            '自动跟单明细',
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ).onTap(() => showExpetAutoBetDetailPage())
                      : SizedBox()
                ],
              ),
              SizedBox(
                height: 10,
              ),
              controller.getNextIsu.value.curIssue != null
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text.rich(TextSpan(
                            text:
                                '${controller.getNextIsu.value.curIssue ?? ''}',
                            style: TextStyle(color: '#ff0000'.color()),
                            children: [
                              TextSpan(
                                  text: '期',
                                  style: TextStyle(color: '#333333'.color()))
                            ])),

                        /// 封盘
                        Text.rich(TextSpan(text: "封盘：", children: [
                          TextSpan(
                              text: controller.closeingTimeNumber.value <= 0
                                  ? '已封盘'
                                  : controller.closeingTimeNumber.value.toHMS(),
                              style: TextStyle(color: '#ff0000'.color()))
                        ])),

                        /// 开奖
                        Text.rich(TextSpan(text: "开奖：", children: [
                          TextSpan(
                              text: controller.openTimeNumber.value <= 0
                                  ? '获取下一期'
                                  : controller.openTimeNumber.value.toHMS(),
                              style: TextStyle(color: '#16a98e'.color()))
                        ])),
                      ],
                    )
                  : const SizedBox(),
            ],
          ),
        ),
        Expanded(
            child: CustomLoading(
          status: controller.loading.value,
          child: controller.dataList.isNotEmpty
              ? ListView.builder(
                  itemBuilder: (c, i) =>
                      _buildExpertDataItemWidget(controller.dataList[i]),
                  itemCount: controller.dataList.length,
                )
              : Text(
                  '暂无计划数据',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: '#333333'.color(), fontSize: 15),
                ).paddingOnly(top: 10, bottom: 10),
        ))
      ],
    );
  }

  /// 自动跟单明细
  showExpetAutoBetDetailPage() {
    Get.to(() => ExpertPlanAutoBetDetailPage(controller.gameModel.value),
        opaque: false,
        fullscreenDialog: true,
        transition: Transition.noTransition);
  }

  /// 专家追号列表item
  Widget _buildExpertDataItemWidget(ExpertPlanDataModel model) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(model.name ?? '',
                style: TextStyle(
                    color: '#333333'.color(),
                    fontSize: 16,
                    fontWeight: FontWeight.w600)),
            Spacer(),
            Text.rich(TextSpan(text: '胜率：', children: [
              TextSpan(
                  text: '${model.winning ?? '0'}%',
                  style: TextStyle(color: '#ff0000'.color()))
            ])),
            SizedBox(
              width: 5,
            ),
            Text.rich(TextSpan(text: '盈亏：', children: [
              TextSpan(
                  text: '￥${model.netAmount ?? '0.00'}',
                  style: TextStyle(color: '#ff0000'.color()))
            ])),
          ],
        ).paddingSymmetric(horizontal: 12, vertical: 5),
        Container(
          height: 1,
          color: '#e4e4e4'.color(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text('游戏：${controller.gameModel.value.gameName}',
                style: TextStyle(color: '#333333'.color(), fontSize: 15)),
            SizedBox(width: 40),
            Text('计划类型：${controller.ycLength.value}码',
                style: TextStyle(color: '#333333'.color(), fontSize: 15)),
          ],
        ).paddingSymmetric(horizontal: 12, vertical: 5),
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
          child: Text('球号：${controller.position.value.name}',
              textAlign: TextAlign.left,
              style: TextStyle(color: '#333333'.color(), fontSize: 15)),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text('预测：',
                style: TextStyle(color: '#333333'.color(), fontSize: 15)),
            model.ycData != null && model.ycData!.isNotEmpty
                ? Row(
                    children: model.ycData!.mapIndexed((i, e) {
                    return Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                      constraints: const BoxConstraints(
                        minWidth: 30,
                      ),
                      height: 30,
                      decoration: BoxDecoration(
                        color: controller.getNumBackColor(e),
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.15), // 阴影颜色
                              spreadRadius: 0, // 阴影扩散半径
                              blurRadius: 5, // 模糊半径
                              offset: Offset(3, 3),
                              blurStyle: BlurStyle.inner),
                        ],
                      ),
                      child: Text(
                        e,
                        style: TextStyle(
                            color: controller.getTextColorStr().color()),
                      ),
                    ).paddingOnly(right: 5);
                  }).toList())
                : SizedBox()
          ],
        ).paddingOnly(left: 12, right: 12, top: 5, bottom: 10),
        Container(
          height: 1,
          color: '#e4e4e4'.color(),
        ),
        Row(
          children: _getBetFunction(model),
        ).paddingOnly(top: 10)
      ],
    ).paddingAll(15).onTap(() => showExpertHistoryPage(model));
  }

  /// 返回投注功能
  List<Widget> _getBetFunction(ExpertPlanDataModel model) {
    List<Widget> children = [];
    children.add(Spacer());
    children.add(Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(16)),
        border: Border.all(
          color: '#dddddd'.color(),
          // 边框颜色
          width: 1, // 边框宽度
        ),
      ),
      child: Text('跟投'),
    ).onTap(() => showExpetBetPage(model)));
    children.add(SizedBox(width: 20));
    children.add(Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(16)),
              border: Border.all(
                color: '#dddddd'.color(), // 边框颜色
                width: 1, // 边框宽度
              ),
            ),
            child: Text('反投'))
        .onTap(() => showExpetBetPage(model, isFollowBet: false)));

    /// 开启了自动跟投
    if (controller.autoFollowStatus.value == 1) {
      children.add(SizedBox(width: 20));
      children.add(Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          border: Border.all(
            color: '#dddddd'.color(),
            // 边框颜色
            width: 1, // 边框宽度
          ),
        ),
        child: Text('自动跟投'),
      ).onTap(() => showExpertAutoBetPage(model)));
    }

    children.add(Spacer());

    return children;
  }

  /// 投注页面
  showExpetBetPage(ExpertPlanDataModel model, {bool isFollowBet = true}) async {
    var res = await Get.to(
        () => ExpertPlanBetPage(
            controller.gameModel.value,
            controller.getNextIsu.value.copyWith(),
            model.copyWith(),
            controller.position.value,
            isFollowBet,
            controller.odds),
        opaque: false,
        fullscreenDialog: true,
        transition: Transition.noTransition);
  }

  /// 自动跟投页面
  showExpertAutoBetPage(ExpertPlanDataModel model) {
    Get.to(
        () => ExpertPlanAutoBetPage(controller.gameModel.value,
            model.copyWith(), controller.position.value),
        opaque: false,
        fullscreenDialog: true,
        transition: Transition.noTransition);
  }

  /// 专家历史记录页面
  showExpertHistoryPage(ExpertPlanDataModel model) {
    Get.to(
        () => ExpertPlanHistoryPage(
            controller.gameModel.value,
            controller.getNextIsu.value.copyWith(),
            model.copyWith(),
            controller.position.value,
            controller.odds),
        opaque: false,
        fullscreenDialog: true);
  }

  /// 专家排名
  Widget _getExpertRank() {
    return Column(
      children: [
        Container(
          color: '#ececec'.color(),
          height: 48,
          child: Row(
            children: [
              Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        '专家',
                        style: TextStyle(
                            color: '#333333'.color(),
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ))),
              Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        '胜率',
                        style: TextStyle(
                            color: '#333333'.color(),
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ))),
            ],
          ),
        ),
        Expanded(
            child: CustomLoading(
                status: controller.loading.value,
                child: controller.rankList.isNotEmpty
                    ? ListView.builder(
                        itemBuilder: (c, i) =>
                            _buildExpertRankItemWidget(controller.rankList[i]),
                        itemCount: controller.rankList.length,
                      )
                    : Text(
                        '暂无专家排名',
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(color: '#333333'.color(), fontSize: 16),
                      ).paddingOnly(top: 10, bottom: 10)))
      ],
    );
  }

  /// 专家排名列表item
  Widget _buildExpertRankItemWidget(ExpertPlanRankModel model) {
    return Column(
      children: [
        SizedBox(
          height: 36,
          child: Row(
            children: [
              Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        model.name ?? '',
                        style:
                            TextStyle(color: '#333333'.color(), fontSize: 16),
                      ))),
              Expanded(
                  child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        '${model.winning}%',
                        style:
                            TextStyle(color: '#333333'.color(), fontSize: 16),
                      ))),
            ],
          ),
        ),
        Container(
          height: 1,
          color: '#e8e8e8'.color(),
        ),
      ],
    );
  }

  /// 游戏列表页面
  showGameList() async {
    var result = await Get.to(() => ExpertPlanGameListPage(controller.gameArr),
        opaque: false,
        fullscreenDialog: true,
        transition: Transition.noTransition);

    if (result != null && result is ExpertPlanGameModel) {
      controller.handleSelectedGameModel(result);
    }
  }
}
