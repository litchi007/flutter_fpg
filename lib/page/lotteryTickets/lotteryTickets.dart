import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/page/lotteryTickets/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LotteryTicketsPage extends GetView<LotteryTicketsController> {
  const LotteryTicketsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: "彩票注单"),
        body: SafeArea(
            child: Column(
          children: [
            _buildHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context)),
            _buildBottomWidget(context)
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35,
            child: Text("时间",
                style: TextStyle(
                    fontSize: 13, color: context.customTheme?.fontColor)),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("笔数",
                style: TextStyle(
                    fontSize: 13, color: context.customTheme?.fontColor)),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("下注金额",
                style: TextStyle(
                    fontSize: 13, color: context.customTheme?.fontColor)),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("输赢",
                style: TextStyle(
                    fontSize: 13, color: context.customTheme?.fontColor)),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return SmartRefresher(
        header: CustomHeader(
          builder: (BuildContext context, RefreshStatus? mode) {
            Widget body;
            if (mode == RefreshStatus.refreshing) {
              body = Text("刷新中..."); // "Refreshing..."
            } else if (mode == RefreshStatus.canRefresh) {
              body = Text("释放刷新"); // "Release to refresh"
            } else if (mode == RefreshStatus.completed) {
              body = Text("刷新完成"); // "Refresh complete"
            } else {
              body = Text("刷新失败"); // "Refresh failed"
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: controller.refreshController,
        onRefresh: controller.onRefresh,
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus? mode) {
            Widget body;
            if (mode == LoadStatus.loading) {
              body = Text("加载中..."); // Change 'Loading' to Chinese here
            } else if (mode == LoadStatus.failed) {
              body = Text("加载失败，请重试");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("释放立即加载更多");
            } else {
              body = Text("没有更多数据");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        child: CustomScrollView(
          slivers: <Widget>[
            // 当列表项高度固定时，使用 SliverFixedExtendList 比 SliverList 具有更高的性能
            Obx(() => SliverFixedExtentList(
                delegate: SliverChildBuilderDelegate(_buildBodyItemWidget,
                    childCount: controller.data.length),
                itemExtent: 53.0)),
            // 如果不是Sliver家族的Widget，需要使用SliverToBoxAdapter做层包裹
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(top: 20),
                alignment: Alignment.center,
                height: 40,
                child: Text(
                  "点击日期可查看下注详情",
                  style: TextStyle(
                      color: context.customTheme?.fontColor, fontSize: 14),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildBodyItemWidget(BuildContext context, int index) {
    double height = 52;
    final model = controller.data[index];
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: height,
                child: Text(
                  "${model.date}\n${model.dayOfWeek}",
                  style: TextStyle(
                      fontSize: 13, color: context.customTheme?.fontColor),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              width: 0.5,
              height: height,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: height, // 设置高度作为示例
                child: Text("${model.betCount ?? 0}",
                    style: const TextStyle(fontSize: 13, color: Colors.red)),
              ),
            ),
            Container(
              width: 0.5,
              height: height,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: height, // 设置高度作为示例
                child: Text("${model.betAmount}",
                    style: const TextStyle(fontSize: 13, color: Colors.red)),
              ),
            ),
            Container(
              width: 0.5,
              height: height,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: height, // 设置高度作为示例
                child: Text("${model.winLoseAmount}",
                    style: const TextStyle(fontSize: 13, color: Colors.red)),
              ),
            ),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    ).onTap(() {
      final betCount = int.parse(model.betCount!);
      if (betCount > 0) {
        /// 如果投注数大于0,则可以进入投注详情
        Get.toNamed(AppRoutes.lotteryBetDetail, arguments: model.date);
      }
    });
  }

  Widget _buildBottomWidget(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      height: 45,
      color: const Color(0xffb89b65),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              height: 34,
              padding: const EdgeInsets.only(left: 10),
              alignment: Alignment.centerRight,
              child: Obx(() => Text('总笔数:  ${controller.betCountTotal.value}',
                  style: TextStyle(color: Colors.white, fontSize: 13)))),
          Container(
              height: 34,
              padding: const EdgeInsets.only(right: 10),
              alignment: Alignment.centerRight,
              child: Obx(() => Text(
                  '总输赢:  ${controller.winLoseAmountTotal.value.toStringAsFixed(2)}',
                  style: TextStyle(color: Colors.white, fontSize: 13))))
        ],
      ),
    );
  }
}
