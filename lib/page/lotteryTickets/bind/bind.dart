import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryTicketsBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<LotteryTicketsController>(() => LotteryTicketsController());
  }
}
