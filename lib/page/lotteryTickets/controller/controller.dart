import 'package:fpg_flutter/api/ticket/ticket.http.dart';
import 'package:fpg_flutter/models/lotteryTickets/lottery_ticket_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';

class LotteryTicketsController extends GetxController {
  static LotteryTicketsController get to =>
      Get.find<LotteryTicketsController>();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  /// 投注记录信息
  late var data = [].cast<LotteryTicketModel>().obs;
  final betCountTotal = 0.obs;
  final winLoseAmountTotal = (0.0).obs;

  Future onRefresh() async {
    await getTicketLotteryStatistics();
    refreshController.refreshCompleted();
  }

  Future getTicketLotteryStatistics() async {
    // 初始化日期格式化工具
    DateTime currentDate = DateTime.now();
    DateFormat dateFormatter = DateFormat('yyyy-MM-dd');
    DateTime previousDate = currentDate.subtract(const Duration(days: 6));
    String endDate = dateFormatter.format(currentDate);
    String startDate = dateFormatter.format(previousDate);

    var res = await TicketHttp.ticketLotteryStatistics(
        startDate: startDate, endDate: endDate);
    data.value = res.data!.tickets ?? [];

    int totalBetCount = 0;
    double totalWinLoseAmount = 0.0;
    for (var model in data) {
      totalBetCount += int.tryParse(model.betCount ?? '0') ?? 0;
      totalWinLoseAmount +=
          double.tryParse(model.winLoseAmount ?? '0.0') ?? 0.0;
    }

    betCountTotal.value = totalBetCount;
    winLoseAmountTotal.value = totalWinLoseAmount;
  }

  @override
  void onReady() {
    getTicketLotteryStatistics();
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
