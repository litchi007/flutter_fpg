import '../controller/controller.dart';
import 'package:get/get.dart';

class ExpertPlanHistoryBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ExpertPlanHistoryController>(
        () => ExpertPlanHistoryController());
  }
}
