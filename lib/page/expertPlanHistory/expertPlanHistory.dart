import 'package:dartx/dartx.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/page/expertPlanBet/expertPlanBet.dart';
import 'package:fpg_flutter/page/expertPlanHistory/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/drop_down_utils.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class ExpertPlanHistoryPage extends GetView<ExpertPlanHistoryController> {
  /// 游戏数据
  final ExpertPlanGameModel gameModel;

  /// 当期数据
  final GameNextIssueModel issueModel;

  /// 专家数据
  final ExpertPlanDataModel dataModel;

  /// 预测位置
  final Position position;

  /// 玩法和赔率
  final Map<String, Play> oddsMap;

  const ExpertPlanHistoryPage(this.gameModel, this.issueModel, this.dataModel,
      this.position, this.oddsMap,
      {super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ExpertPlanHistoryController());
    controller.gameModel = gameModel;
    controller.issueModel = issueModel;
    controller.dataModel = dataModel;
    controller.position = position;
    return Scaffold(
      backgroundColor: Colors.transparent, // 设置背景透明
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(top: 56),
          child: Container(
            height: MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top -
                56 -
                MediaQuery.of(context).padding.bottom,
            color: Colors.white,
            child: Stack(
              children: [
                Column(
                  children: [
                    _buildExpertDataHeaderWidget(dataModel),
                    Container(
                      height: 5,
                      color: '#f5f5f5'.color(),
                    ),
                    Obx(() => Expanded(
                            child: CustomLoading(
                          status: controller.loading.value,
                          child: controller.dataList.isNotEmpty
                              ? ListView.builder(
                                  itemBuilder: (c, i) =>
                                      _buildExpertHistoryItemWidget(
                                          controller.dataList[i]),
                                  itemCount: controller.dataList.length,
                                )
                              : Text(
                                  textAlign: TextAlign.center,
                                  '暂无历史数据',
                                  style: TextStyle(
                                      color: '#333333'.color(), fontSize: 15),
                                ).paddingOnly(top: 10, bottom: 10),
                        ))),
                  ],
                ),
                Positioned(
                  top: 50,
                  right: 8,
                  width: 32,
                  height: 60,
                  child: Container(
                    alignment: Alignment.center,
                    // padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                    decoration: BoxDecoration(
                      color: '#ff0000'.color(),
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.15),
                            // 阴影颜色
                            spreadRadius: 4,
                            // 阴影扩散半径
                            blurRadius: 10,
                            // 模糊半径
                            offset: Offset(0, 0),
                            blurStyle: BlurStyle.inner),
                      ],
                    ),
                    child: Text(
                      '关\n闭',
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    ),
                  ).onTap(() => Get.back()),
                )
              ],
            ),
          ),
        ).onTap(() => Get.back()),
      ),
    );
  }

  /// 专家追号头部
  Widget _buildExpertDataHeaderWidget(ExpertPlanDataModel model) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(model.name ?? '',
                style: TextStyle(
                    color: '#333333'.color(),
                    fontSize: 16,
                    fontWeight: FontWeight.w600)),
            Spacer(),
            Text.rich(TextSpan(text: '胜率：', children: [
              TextSpan(
                  text: '${model.winning ?? '0'}%',
                  style: TextStyle(color: '#ff0000'.color()))
            ])),
            SizedBox(
              width: 5,
            ),
            Text.rich(TextSpan(text: '盈亏：', children: [
              TextSpan(
                  text: '￥${model.netAmount ?? '0.00'}',
                  style: TextStyle(color: '#ff0000'.color()))
            ])),
          ],
        ).paddingSymmetric(horizontal: 12, vertical: 10),
        Container(
          height: 1,
          color: '#e4e4e4'.color(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text('游戏：${controller.gameModel.gameName}',
                style: TextStyle(color: '#333333'.color(), fontSize: 15)),
            SizedBox(width: 40),
            Text('计划类型：${controller.dataModel.ycLength}码',
                style: TextStyle(color: '#333333'.color(), fontSize: 15)),
          ],
        ).paddingSymmetric(horizontal: 12, vertical: 5),
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
          child: Text('球号：${controller.position.name}',
              textAlign: TextAlign.left,
              style: TextStyle(color: '#333333'.color(), fontSize: 15)),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text('预测：',
                style: TextStyle(color: '#333333'.color(), fontSize: 15)),
            model.ycData != null && model.ycData!.isNotEmpty
                ? Row(
                    children: model.ycData!.mapIndexed((i, e) {
                    return Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                      constraints: const BoxConstraints(
                        minWidth: 30,
                      ),
                      height: 30,
                      decoration: BoxDecoration(
                        color: controller.getNumBackColor(e),
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.15),
                              // 阴影颜色
                              spreadRadius: 0,
                              // 阴影扩散半径
                              blurRadius: 5,
                              // 模糊半径
                              offset: Offset(3, 3),
                              blurStyle: BlurStyle.inner),
                        ],
                      ),
                      child: Text(
                        e,
                        style: TextStyle(
                            color: controller.getTextColorStr().color()),
                      ),
                    ).paddingOnly(right: 5);
                  }).toList())
                : SizedBox()
          ],
        ).paddingOnly(left: 12, right: 12, top: 5, bottom: 10),
        Container(
          height: 1,
          color: '#e4e4e4'.color(),
        ).paddingSymmetric(horizontal: 12),
        Row(
          children: [
            Spacer(),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                border: Border.all(
                  color: '#dddddd'.color(),
                  // 边框颜色
                  width: 1, // 边框宽度
                ),
              ),
              child: Text('跟投'),
            ).onTap(() => showExpetBetPage(model)),
            SizedBox(
              width: 20,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                border: Border.all(
                  color: '#dddddd'.color(), // 边框颜色
                  width: 1, // 边框宽度
                ),
              ),
              child: Text('反投'),
            ).onTap(() => showExpetBetPage(model, isFollowBet: false)),
            Spacer()
          ],
        ).paddingSymmetric(horizontal: 0, vertical: 10)
      ],
    );
  }

  Widget _buildExpertHistoryItemWidget(ExpertPlanDataModel model) {
    return Column(
      children: [
        Row(
          children: [
            Text.rich(TextSpan(
                text: model.number,
                style: TextStyle(color: '#ff0000'.color(), fontSize: 14),
                children: [
                  TextSpan(
                      text: '期',
                      style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14))
                ])),
            Container(
                width: 28,
                height: 28,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: controller.getResultTextColor(model.result).color(),
                  borderRadius: BorderRadius.all(Radius.circular(14)),
                ),
                child: Text(
                  controller.getResultText(model.result),
                  style: TextStyle(color: Colors.white, fontSize: 14),
                )).paddingSymmetric(horizontal: 10),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                model.ycData != null && model.ycData!.isNotEmpty
                    ? Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 5,
                            runSpacing: 5,
                            children: model.ycData!.mapIndexed((i, e) {
                              /// 开了 && 中了 && 哪个号码中了
                              if (model.status == '1' &&
                                  model.status == '1' &&
                                  controller.isWinning(e, model.kjData)) {
                                return Stack(children: [
                                  Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 0),
                                    width: controller.getNumberWidth(),
                                    height: 30,
                                    decoration: BoxDecoration(
                                      color: controller.getNumBackColor(e),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(4)),
                                      boxShadow: [
                                        BoxShadow(
                                            color:
                                                Colors.black.withOpacity(0.15),
                                            // 阴影颜色
                                            spreadRadius: 0,
                                            // 阴影扩散半径
                                            blurRadius: 5,
                                            // 模糊半径
                                            offset: Offset(3, 3),
                                            blurStyle: BlurStyle.inner),
                                      ],
                                    ),
                                    child: Text(
                                      e,
                                      style: TextStyle(
                                          color: controller
                                              .getTextColorStr()
                                              .color(),
                                          fontSize: 12),
                                    ),
                                  ),
                                  Positioned(
                                    left: 0,
                                    right: 0,
                                    bottom: 0,
                                    // width: 30,
                                    height: 3,
                                    child: Container(
                                        decoration: BoxDecoration(
                                      color: '#ff0000'.color(),
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(4),
                                          bottomRight: Radius.circular(4)),
                                      boxShadow: [
                                        BoxShadow(
                                            color:
                                                Colors.black.withOpacity(0.15),
                                            // 阴影颜色
                                            spreadRadius: 0,
                                            // 阴影扩散半径
                                            blurRadius: 5,
                                            // 模糊半径
                                            offset: Offset(3, 3),
                                            blurStyle: BlurStyle.inner),
                                      ],
                                    )),
                                  )
                                ]);
                              }
                              return Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 0),
                                width: controller.getNumberWidth(),
                                height: 30,
                                decoration: BoxDecoration(
                                  color: controller.getNumBackColor(e),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(0.15),
                                        // 阴影颜色
                                        spreadRadius: 0,
                                        // 阴影扩散半径
                                        blurRadius: 5,
                                        // 模糊半径
                                        offset: Offset(3, 3),
                                        blurStyle: BlurStyle.inner),
                                  ],
                                ),
                                child: Text(
                                  e,
                                  style: TextStyle(
                                      color:
                                          controller.getTextColorStr().color(),
                                      fontSize: 12),
                                ),
                              );
                            }).toList())
                        .paddingOnly(bottom: 5)
                    : SizedBox(),
                Text.rich(TextSpan(text: '胜率：', children: [
                  TextSpan(
                      text: '${model.winning ?? '0'}%',
                      style: TextStyle(color: '#ff0000'.color()))
                ])),
              ],
            ))
          ],
        ).paddingSymmetric(horizontal: 10, vertical: 10),
        Container(
          height: 5,
          color: '#f5f5f5'.color(),
        ),
      ],
    );
  }

  /// 投注页面
  showExpetBetPage(ExpertPlanDataModel model, {bool isFollowBet = true}) {
    Get.to(
        () => ExpertPlanBetPage(controller.gameModel, controller.issueModel,
            model, position, isFollowBet, oddsMap),
        opaque: false,
        fullscreenDialog: true,
        transition: Transition.noTransition);
  }
}
