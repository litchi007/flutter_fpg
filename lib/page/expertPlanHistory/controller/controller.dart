import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_type.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/utils/drop_down_utils.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class ExpertPlanHistoryController extends GetxController {
  static ExpertPlanHistoryController get to =>
      Get.find<ExpertPlanHistoryController>();

  /// 专家追号列表
  final dataList = [].cast<ExpertPlanDataModel>().obs;

  /// 游戏数据
  late ExpertPlanGameModel gameModel;

  /// 当期数据
  late GameNextIssueModel issueModel;

  /// 专家数据
  late ExpertPlanDataModel dataModel;

  /// 预测位置
  late Position position;

  /// 加载状态
  final loading = true.obs;

  getUserExpertHistoryList() async {
    var res = await UserHttp.userExpertHistoryList(
        gameModel.gameId ?? '', issueModel.curIssue, dataModel.eid ?? '',
        position: position.value, ycLength: dataModel.ycLength);
    LogUtil.w(res.models);
    dataList.value = res.models ?? [];
    loading.value = false;
  }

  @override
  void onReady() {
    getUserExpertHistoryList();
    super.onReady();
  }

  /// 获取开奖文本
  String getResultText(String? res) {
    if (res == '0') {
      return '待';
    } else if (res == '1') {
      return '中';
    } else if (res == '2') {
      return '挂';
    } else {
      return '...';
    }
  }

  /// 获取开奖文本颜色
  String getResultTextColor(String? res) {
    if (res == '0') {
      return '#808080';
    } else if (res == '1') {
      return '#ff0000';
    } else if (res == '2') {
      return '#008000';
    } else {
      return '#808080';
    }
  }

  /// 获取开奖号码宽度
  double getNumberWidth() {
    if (issueModel.gameType == GameType.markSixLottery &&
        (position.value == '3' || position.value == '4')) {
      return 34;
    }
    return 30;
  }

  /// 获取开奖号码背景色
  Color? getNumBackColor(String value) {
    if (issueModel.gameType == GameType.luckyAirship ||
        issueModel.gameType == GameType.racing) {
      return value.getNumColor();
    } else if (issueModel.gameType == GameType.timeLottery ||
        issueModel.gameType == GameType.elevenSelectedFive ||
        issueModel.gameType == GameType.sortThree ||
        issueModel.gameType == GameType.gdkl10 ||
        issueModel.gameType == GameType.sevenStarLottery ||
        issueModel.gameType == GameType.pcEggs) {
      return '#63a0d9'.color();
    }
    return Colors.white;
  }

  /// 号码是否中奖
  bool isWinning(String value, String? kjData) {
    if (issueModel.gameType == GameType.markSixLottery) {
      if (position.value == '3' || position.value == '4') {
        /// 获取前缀
        String prefix = value.substring(0, 1);

        /// 按逗号分割字符串
        List<String> numbers =
            (kjData ?? '').split(',').map((e) => e.trim()).toList();

        /// 提取每个数字的最后一位
        List<String> lastDigits = numbers.map((e) => e[e.length - 1]).toList();

        return lastDigits.contains(prefix);
      } else if (position.value == '5') {
        /// 按逗号分割字符串
        List<String> zodiacs =
            (kjData ?? '').split(',').map((e) => e.trim()).toList();
        return zodiacs.contains(value);
      }
    }

    return value == kjData;
  }

  /// 获取文本颜色
  String getTextColorStr() {
    var colorStr = '#333333';
    if (issueModel.gameType != GameType.markSixLottery) {
      colorStr = '#ffffff';
    }
    return colorStr;
  }
}
