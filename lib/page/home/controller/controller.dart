import 'package:fpg_flutter/api/app/app.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.find<HomeController>();
  final test = "22".obs;
  login() async {
    final res = await UserHttp.guestLogin();
    if (res.data != null) {
      GlobalService.to.changeUserDetail(res.data!);
      AppToast.showSuccess(msg: "游客登录成功");
    }
  }

  userLogin() async {
    final res = await UserHttp.userLogin('miya01', 'a123456');

    GlobalService.to.changeUserDetail(res.data!);
    AppToast.showSuccess(msg: "用户登录成功");
  }

  getAppInfo() async {
    final res = await AppHttp.appInfoAppId();
    LogUtil.w("app信息___$res");
    GlobalService.to.changeAppInfo(res.data!);
  }

  @override
  void onReady() {
    getAppInfo();
    // login();
    // userLogin();
    super.onReady();
  }
}
