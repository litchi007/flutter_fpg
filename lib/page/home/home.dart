import 'package:fpg_flutter/api/error_report/error_report.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/generated/locales.g.dart';
import 'package:fpg_flutter/models/error_report/error_report.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_type.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/models/twoSideDragon/two_side_dragon_model.dart';
import 'package:fpg_flutter/page/home/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/page/lotteryPopup/controller/controller.dart';
import 'package:fpg_flutter/page/lotteryPopup/lotteryPopup.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/services/lang_service.dart';
import 'package:fpg_flutter/services/theme_service.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: IconButton(
          icon: Icon(Icons.light),
          onPressed: () {
            // ThemeService.to.handleSetTheme(ThemeMode.dark);
            // LangService.to.handleSetLang(const Locale("en", "US"));
            Get.toNamed("${AppRoutes.lottery}/1");
          },
        ),
        body: SafeArea(
            child: Column(
          children: [
            Obx(
              () => Text(LocaleKeys.yearAgo.tr + controller.test.value)
                  .onTap(() async {
                GlobalService.to.handlePerformance();
                // await ErrorReportHttp.report(
                //     ErrorReport(err: "测试", text: "error", page: Get.currentRoute));
              }),
            ),
            const SizedBox(height: 10),
            const Text("测试弹框用").onTap(() {
              // Get.to(() => const LotteryPopupPage(Lottery(id: '27', shortName: '澳洲幸运10', gameType: GameType.luckyAirship)),
              Get.to(
                  () => const LotteryPopupPage(Lottery(
                      id: '171',
                      shortName: '快速鼓包',
                      gameType: GameType.quickThree)),
                  // Get.to(() => const LotteryPopupPage(Lottery(id: '70', shortName: '香港6合彩', gameType: GameType.markSixLottery)),
                  opaque: false,
                  fullscreenDialog: true,
                  transition: Transition.noTransition);
            }),
          ],
        )));
  }
}
