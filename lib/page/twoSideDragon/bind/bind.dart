import '../controller/controller.dart';
import 'package:get/get.dart';

class TwoSideDragonBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<TwoSideDragonController>(() => TwoSideDragonController());
  }
}
