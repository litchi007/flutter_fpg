import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/models/twoSideDragon/two_side_dragon_model.dart';
import 'package:fpg_flutter/page/twoSideDragon/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TwoSideDragonPage extends GetView<TwoSideDragonController> {
  const TwoSideDragonPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: "两面长龙"),
        body: SafeArea(
            child: Column(
          children: [
            _buildHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context)),
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 45, // 设置高度作为示例
            child: Text(
              '彩种',
              style: context.textTheme.bodySmall,
            ),
          ),
        ),
        Container(
          width: 0.5,
          height: 45,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 45, // 设置高度作为示例
            child: Text('期数', style: context.textTheme.bodySmall),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(
      () => CustomLoading(
          status: controller.loading.value,
          child: SmartRefresher(
              header: CustomHeader(
                builder: (BuildContext context, RefreshStatus? mode) {
                  Widget body;
                  if (mode == RefreshStatus.refreshing) {
                    body = Text("刷新中..."); // "Refreshing..."
                  } else if (mode == RefreshStatus.canRefresh) {
                    body = Text("释放刷新"); // "Release to refresh"
                  } else if (mode == RefreshStatus.completed) {
                    body = Text("刷新完成"); // "Refresh complete"
                  } else {
                    body = Text("刷新失败"); // "Refresh failed"
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: controller.refreshController,
              onRefresh: controller.onRefresh,
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus? mode) {
                  Widget body;
                  if (mode == LoadStatus.loading) {
                    body = Text("加载中..."); // Change 'Loading' to Chinese here
                  } else if (mode == LoadStatus.failed) {
                    body = Text("加载失败，请重试");
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("释放立即加载更多");
                  } else {
                    body = Text("没有更多数据");
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              child: ListView.builder(
                itemBuilder: (c, i) =>
                    _buildBodyItemWidget(context, controller.list[i]),
                itemCount: controller.list.length,
              ))),
    );
  }

  Widget _buildBodyItemWidget(BuildContext context, TwoSideDragonModel model) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Container(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    alignment: Alignment.center,
                    height: 45, // 设置高度作为示例
                    child: Text(
                      '${model.playCateName}-${model.playName}',
                      maxLines: 3,
                      textAlign: TextAlign.center,
                      style: context.textTheme.bodySmall,
                    ))),
            Container(
              width: 0.5,
              height: 45,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 45, // 设置高度作为示例
                child:
                    Text('${model.count}', style: context.textTheme.bodySmall),
              ),
            ),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    );
  }
}
