import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/models/twoSideDragon/two_side_dragon_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TwoSideDragonController extends GetxController {
  static TwoSideDragonController get to => Get.find<TwoSideDragonController>();
  RefreshController refreshController = RefreshController();

  /// 游戏Id
  String gameId = '';

  /// 两面长龙列表
  late var list = [].cast<TwoSideDragonModel>().obs;

  final loading = true.obs;

  /// 获取两面长龙数据
  Future getGameLong() async {
    var res = await GameHttp.gameLong(gameId);
    list.value = res.models!;
    loading.value = false;
  }

  Future onRefresh() async {
    await getGameLong();
    refreshController.refreshCompleted();
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null && arguments is Lottery) {
      LogUtil.w(arguments.toString());
      gameId = arguments.id ?? '';
    }
    super.onInit();
  }

  @override
  void onReady() {
    getGameLong();
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
