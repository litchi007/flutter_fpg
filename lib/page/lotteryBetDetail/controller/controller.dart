import 'dart:async';

import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/models/lotteryBetDetail/lottery_bet_detail.model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LotteryBetDetailController extends GetxController {
  static LotteryBetDetailController get to =>
      Get.find<LotteryBetDetailController>();

  RefreshController refreshController = RefreshController(initialRefresh: true);

  /// 日期
  late String date = '';

  /// 下注明细列表
  late var list = [].cast<LotteryBetDetailModel>().obs;

  Future getUserLotteryDayStat() async {
    final res = await UserHttp.userLotteryDayStat(date: date);
    list.value = res.models ?? [];
  }

  Future onRefresh() async {
    await getUserLotteryDayStat();
    refreshController.refreshCompleted();
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null) {
      date = arguments;
    }
    super.onInit();
  }

  @override
  void onReady() {
    getUserLotteryDayStat();
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
