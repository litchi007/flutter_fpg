import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lotteryBetDetail/lottery_bet_detail.model.dart';
import 'package:fpg_flutter/page/lotteryBetDetail/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LotteryBetDetailPage extends GetView<LotteryBetDetailController> {
  const LotteryBetDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: "下注明细"),
        body: SafeArea(
            child: Column(
          children: [
            _buildHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context)),
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          flex: 2,
          child: Container(
            alignment: Alignment.center,
            height: 35,
            child: Text(
              "彩种",
              style: context.textTheme.bodySmall,
            ),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text(
              "笔数",
              style: context.textTheme.bodySmall,
            ),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("下注金额", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("输赢", style: context.textTheme.bodySmall),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(() => SmartRefresher(
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus? mode) {
              Widget body;
              if (mode == LoadStatus.loading) {
                body = Text("加载中..."); // Change 'Loading' to Chinese here
              } else if (mode == LoadStatus.failed) {
                body = Text("加载失败，请重试");
              } else if (mode == LoadStatus.canLoading) {
                body = Text("释放立即加载更多");
              } else {
                body = Text("没有更多数据");
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          header: CustomHeader(
            builder: (BuildContext context, RefreshStatus? mode) {
              Widget body;
              if (mode == RefreshStatus.refreshing) {
                body = Text("刷新中..."); // "Refreshing..."
              } else if (mode == RefreshStatus.canRefresh) {
                body = Text("释放刷新"); // "Release to refresh"
              } else if (mode == RefreshStatus.completed) {
                body = Text("刷新完成"); // "Refresh complete"
              } else {
                body = Text("刷新失败"); // "Refresh failed"
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          controller: controller.refreshController,
          onRefresh: controller.onRefresh,
          child: ListView.builder(
            itemBuilder: (c, i) =>
                _buildBodyItemWidget(context, controller.list[i]),
            itemCount: controller.list.length,
          ),
        ));
  }

  Widget _buildBodyItemWidget(
      BuildContext context, LotteryBetDetailModel model) {
    double height = 45;
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Column(
                children: [
                  Text('${model.title}',
                      style: context.textTheme.bodySmall,
                      maxLines: 5,
                      textAlign: TextAlign.center),
                ],
              ),
            ),
            Container(
              width: 0.5,
              height: height,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    alignment: Alignment.center,
                    height: 45,
                    child: Text(
                      '${model.betCount}',
                      style: TextStyle(
                        fontSize: 12,
                        decoration: TextDecoration.underline,
                        decorationColor:
                            context.customTheme?.fontColor, // 可选：指定下划线颜色
                        decorationThickness: 2, // 可选：指定下划线粗细
                        decorationStyle:
                            TextDecorationStyle.solid, // 可选：指定下划线样式,
                      ),
                    ))),
            Container(
              width: 0.5,
              height: height,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: height,
                child: Text(model.betMoney!.toStringAsFixed(2),
                    style: context.textTheme.bodySmall),
              ),
            ),
            Container(
              width: 0.5,
              height: height,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      model.rewardRebate!.toStringAsFixed(2),
                      style: context.textTheme.bodySmall,
                    ))),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    ).onTap(() {
      Get.toNamed(AppRoutes.lotteryBetDetailSettled,
          arguments: {'date': model.statDate, 'gameId': model.gameId});
    });
  }
}
