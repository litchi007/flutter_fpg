import '../controller/controller.dart';
import 'package:get/get.dart';

class LongDragonToolBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<LongDragonToolController>(() => LongDragonToolController());
  }
}
