import 'dart:math';
import 'package:dartx/dartx.dart';
import 'package:flutter/services.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lottery_my_bet_model/lottery_my_bet_model.dart';
import 'package:fpg_flutter/page/longDragonTool/components/long_dragon_item.dart';
import 'package:fpg_flutter/page/longDragonTool/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class LongDragonToolPage extends GetView<LongDragonToolController> {
  LongDragonToolPage({super.key});

  final tags = Get.parameters['null'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          textColor: Colors.white,
          iconColor: Colors.white,
          background: Color(0xffb89b65),
          customChild: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '长龙助手',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
              Row(
                children: [
                  Obx(() => Text(
                        GlobalService.to.userBalance.balance ?? '',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      )),
                  AnimatedBuilder(
                      animation: controller.animation,
                      builder: (context, child) {
                        return Transform.rotate(
                                angle: pi * controller.animation.value,
                                child: const Icon(
                                  Icons.refresh,
                                  color: Colors.white,
                                ))
                            .paddingOnly(left: 10)
                            .onTap(controller.handleRefresh);
                      }),
                  const Text('必看说明',
                          style: TextStyle(color: Colors.white, fontSize: 16))
                      .paddingOnly(left: 10, right: 10)
                      .onTap(() => showDialog())
                ],
              ),
            ],
          ),
        ),
        body: SafeArea(
            child: Column(
          children: [
            Obx(() => Row(
                  children: controller.tabs
                      .mapIndexed((i, e) => Expanded(
                            child: Stack(
                              children: [
                                Container(
                                  height: 50,
                                  color: Colors.white,
                                  alignment: Alignment.center,
                                  child: Text(
                                    e,
                                    style: TextStyle(
                                        color: controller.selectedTab.value == i
                                            ? Colors.red
                                            : Colors.black,
                                        fontSize: 16),
                                  ),
                                ).onTap(() => controller.handleSelectTab(i)),
                                Positioned(
                                    left: 0,
                                    right: 0,
                                    bottom: 5,
                                    child: Container(
                                      height: controller.selectedTab.value == i
                                          ? 2
                                          : 0,
                                      color: Colors.red,
                                    ))
                              ],
                            ),
                          ))
                      .toList(),
                )),
            Obx(() => Expanded(
                child: CustomLoading(
                    status: controller.loading.value,
                    child: _getBodyWidget(context)))),
            Obx(() =>
                (controller.selectedTab.value == 0 && !controller.loading.value)
                    ? _getBetTool()
                    : const SizedBox(height: 0)),
          ],
        )));
  }

  /// 返回 最新长龙列表 或者 我的投注列表
  Widget _getBodyWidget(BuildContext context) {
    if (controller.selectedTab.value == 0) {
      return CustomScrollView(
        slivers: <Widget>[
          Obx(() => SliverFixedExtentList(
              delegate: SliverChildBuilderDelegate(
                  _buildBodyLongDragonItemWidget,
                  childCount: controller.list.length),
              itemExtent: 95.0)),
          const SliverToBoxAdapter(
            child: SizedBox(height: 20),
          ),
        ],
      );
    }

    if (controller.betList.isEmpty) {
      return Center(
        child: Text('暂无投注记录'),
      );
    }

    return CustomScrollView(
      slivers: <Widget>[
        // 当列表项高度固定时，使用 SliverFixedExtendList 比 SliverList 具有更高的性能
        Obx(() => SliverFixedExtentList(
            delegate: SliverChildBuilderDelegate(_buildBodyMyBetItemWidget,
                childCount: controller.betList.length),
            itemExtent: 90.0)),
        // 如果不是Sliver家族的Widget，需要使用SliverToBoxAdapter做层包裹
        SliverToBoxAdapter(
          child: Container(
            padding: const EdgeInsets.only(top: 20),
            alignment: Alignment.center,
            height: 40,
            child: Text(
              '查看更多注单记录',
              style: TextStyle(
                  decoration: TextDecoration.underline,
                  decorationColor: Colors.red,
                  // 下划线颜色
                  decorationStyle: TextDecorationStyle.solid,
                  color: context.customTheme?.error,
                  fontSize: 14),
            ).onTap(() => Get.toNamed(AppRoutes.lotteryTickets)),
          ),
        ),
      ],
    );
  }

  /// 最新长龙列表 item
  Widget _buildBodyLongDragonItemWidget(BuildContext context, int index) {
    return LongDragonItem(
        key: UniqueKey(),
        model: controller.list[index],
        onChange: (model, betModel) =>
            controller.handleSelectedBetModel(index, model, betModel));
  }

  /// 获取投注工具栏
  Widget _getBetTool() {
    return Container(
      color: Colors.black,
      height: 56,
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5),
            child: Text(
              '清空',
              style: TextStyle(color: '#FFAF36'.color(), fontSize: 20),
            ),
          ).onTap(() => controller.handleClearEvent()),
          const Spacer(),
          Obx(() => Text(
                '共${controller.betCount.value}注，${controller.betMoney.value.isNotEmpty ? controller.betMoney.value : '0'}',
                style: TextStyle(color: '#FFFFFF'.color(), fontSize: 14),
              )),
          const Spacer(),
          Container(
            width: 70,
            height: 30,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.grey[200], // 输入框背景颜色
            ),
            child: TextField(
              cursorWidth: 1,
              maxLength: 7,
              controller: controller.editingController,
              style: TextStyle(
                  fontSize: 12.0, // 设置字体大小
                  color: Colors.black),
              keyboardType:
                  TextInputType.numberWithOptions(decimal: true, signed: false),
              // 设置键盘类型为数字
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
                // 只允许输入数字
              ],
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
                hintText: '金额',
                hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
                filled: true,
                fillColor: Colors.white,
                counterText: '',
                floatingLabelBehavior: FloatingLabelBehavior.never,
                // 输入框背景颜色
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide.none, // 隐藏默认边框
                ),
              ),
              onChanged: (text) {
                controller.betMoney.value = text;
              },
              onSubmitted: (text) {},
            ),
          ),
          const SizedBox(width: 5),
          Container(
            width: 102,
            height: 55,
            alignment: Alignment.center,
            color: '#387EF5'.color(),
            child: Text(
              '马上投注',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
          ).onTap(() async {
            var res = await controller.startBet();
            ToastUtils.show(res.isNotEmpty ? res : '投注成功');
            if (res.isEmpty) {
              controller.handleClearEvent();
            }
          }),
        ],
      ),
    ).onTap(() => controller.focusNode.unfocus());
  }

  /// 我的投注列表 item
  Widget _buildBodyMyBetItemWidget(BuildContext context, int index) {
    LotteryMyBetModel model = controller.betList[index];
    return Container(
            height: 75,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6), // 圆角边框
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.8), // 阴影颜色
                  spreadRadius: 1, // 阴影扩散程度
                  blurRadius: 2, // 阴影模糊程度
                  offset: const Offset(0, 0), // 阴影偏移量
                ),
              ],
              border: Border.all(
                color: Colors.grey.shade400, // 边框颜色
                width: 1, // 边框宽度
              ),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(6),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${model.title} ¥${model.money}',
                          style: TextStyle(
                              color: context.customTheme?.fontColor,
                              fontSize: 14)),
                      Text('${model.issue} 期',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                    ],
                  ),
                  Text(_getStatusStr(model),
                      textAlign: TextAlign.right,
                      style: TextStyle(
                          color: _getStatusColor(model), fontSize: 14))
                ],
              ),
            ).paddingOnly(
                left: 15, right: 15, top: 10, bottom: 10) // 替换为您想要放置的子组件
            )
        .paddingOnly(left: 10, right: 10, top: 10)
        .onTap(() =>
            Get.toNamed(AppRoutes.lotteryOrderDetail, arguments: model.id));
  }

  /// 获取状态
  String _getStatusStr(LotteryMyBetModel model) {
    String isWin = model.isWin.toString();
    if (isWin == '1') {
      return '+¥${model.bonus}\n${model.msg}';
    } else {
      return model.msg ?? '';
    }
  }

  /// 获取状态颜色
  Color _getStatusColor(LotteryMyBetModel model) {
    String status = model.status.toString();
    String isWin = model.isWin.toString();

    LogUtil.w("status:");
    LogUtil.w(status);

    if (status == '0') {
      return '#008000'.color();
    } else if (status == '1' && isWin == '1') {
      return '#FF0000'.color();
    } else {
      return '#333333'.color();
    }
  }

  /// 必看说明弹框
  showDialog() {
    SmartDialog.show(
        clickMaskDismiss: false,
        builder: (context) {
          return Container(
            width: MediaQuery.of(context).size.width - 36,
            height: MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top -
                MediaQuery.of(context).padding.bottom -
                100,
            decoration: BoxDecoration(
              color: context.customTheme?.lotteryPopupItemBg,
              borderRadius: const BorderRadius.all(Radius.circular(4)),
            ),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 44,
                  color: Colors.red,
                  child: Text(
                    '必看说明',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Expanded(
                    child: SingleChildScrollView(
                        padding: const EdgeInsets.all(16.0),
                        child: HtmlWidget(controller.detailText))),
                Container(
                  height: 42,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    // 设置渐变色背景
                    border: Border.all(
                      color: const Color(0xffa9a9a9), // 边框颜色
                      width: 1.0, // 边框宽度
                    ),
                    borderRadius: BorderRadius.circular(4), // 设置圆角
                  ),
                  child: Text(
                    '确定',
                    style: TextStyle(
                        color: context.customTheme?.fontColor, fontSize: 16),
                  ),
                )
                    .onTap(() => SmartDialog.dismiss())
                    .paddingOnly(left: 10, right: 10),
                const SizedBox(height: 10),
              ],
            ),
          );
        });
  }
}
