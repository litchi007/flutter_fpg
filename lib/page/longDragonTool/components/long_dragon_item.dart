import 'dart:async';
import 'package:dartx/dartx.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/longDragon/long_dragon_bet_model.dart';
import 'package:fpg_flutter/models/longDragon/long_dragon_model.dart';
import 'package:get/get.dart';

class LongDragonItem extends StatefulWidget {
  final LongDragonModel model;
  final Function(LongDragonModel model, LongDragonBetModel betModel)? onChange;

  LongDragonItem({super.key, required this.model, this.onChange});

  @override
  State<LongDragonItem> createState() => _LongDragonItemState();
}

class _LongDragonItemState extends State<LongDragonItem> {
  late Timer _timer;
  int _countdown = 0;
  String _countdownStr = '';

  @override
  void initState() {
    _countdown = int.tryParse(widget.model.closeCountdown.toString()) ?? 0;
    _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      showTime();
    });
    showTime();
    super.initState();
  }

  /// 展示时间
  showTime() {
    String countdownStr = '';
    if (_countdown > 0) {
      _countdown--;
      int hours = _countdown ~/ (60 * 60);
      int minutes = (_countdown ~/ 60) % 60;
      int seconds = _countdown % 60;

      if (hours > 0) {
        countdownStr += '${hours.toString().padLeft(2, '0')}:';
      }

      countdownStr += '${minutes.toString().padLeft(2, '0')}:';
      countdownStr += seconds.toString().padLeft(2, '0');
    } else {
      _timer.cancel();
      countdownStr = '开奖中';
    }

    setState(() {
      _countdownStr = countdownStr;
    });
  }

  /// 获取logo
  ImageProvider _getLogoImage(String? logo) {
    if (logo != null && logo.isNotEmpty) {
      return NetworkImage(logo);
    } else {
      return const AssetImage('assets/images/defaultIcon.png');
      // return const AssetImage('assets/images/loading.gif');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 104,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(6),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.8), // 阴影颜色
              spreadRadius: 1, // 阴影扩散程度
              blurRadius: 2, // 阴影模糊程度
              offset: const Offset(0, 3), // 阴影偏移量
            ),
          ],
          border: Border.all(
            color: Colors.grey.shade400, // 边框颜色
            width: 1, // 边框宽度
          ),
        ),
        child: Container(
            padding: EdgeInsets.fromLTRB(5, 10, 0, 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        width: 60,
                        height: 60,
                        decoration: ShapeDecoration(
                            color: Colors.white,
                            shadows: [
                              BoxShadow(color: Colors.grey, blurRadius: 6)
                            ],
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: _getLogoImage(widget.model.logo)),
                            shape: CircleBorder(
                                side:
                                    BorderSide(width: 1, color: Colors.grey)))),
                    const SizedBox(width: 5),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('${widget.model.title}',
                            style: TextStyle(
                                color: context.customTheme?.fontColor,
                                fontSize: 16)),
                        Row(
                          children: [
                            Text('${widget.model.getLotteryIssue()}期 ',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 14)),
                            Text(_countdownStr,
                                style:
                                    TextStyle(color: Colors.red, fontSize: 14))
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                                padding:
                                    const EdgeInsets.only(left: 5, right: 5),
                                decoration: BoxDecoration(
                                    color: '#C9C9C9'.color(),
                                    borderRadius: 3.radius),
                                child: Text('${widget.model.playCateName}',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12))),
                            const SizedBox(width: 10),
                            Container(
                                padding:
                                    const EdgeInsets.only(left: 5, right: 5),
                                decoration: BoxDecoration(
                                    color:
                                        widget.model.getPlayNameColor().color(),
                                    borderRadius: 3.radius),
                                child: Text(
                                  '${widget.model.playName}',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12),
                                )),
                            const SizedBox(width: 10),
                            Container(
                                padding:
                                    const EdgeInsets.only(left: 5, right: 5),
                                decoration: BoxDecoration(
                                    color: '#DC143C'.color(),
                                    borderRadius: 3.radius),
                                child: Text(
                                  '${widget.model.count}',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12),
                                )),
                          ],
                        )
                      ],
                    )
                  ],
                ),
                Row(
                    children:
                        (widget.model.betList ?? []).mapIndexed((index, e) {
                  return Stack(
                    children: [
                      Container(
                        width: 55,
                        height: 55,
                        decoration: BoxDecoration(
                            color: (_countdown <= 0
                                ? Colors.white
                                : (e.selected == true
                                    ? Colors.red
                                    : Colors.white)),
                            borderRadius: 5.radius,
                            border: Border.all(
                                color: '#cccccc'.color(),
                                width: (_countdown <= 0
                                    ? 1
                                    : (e.selected == true ? 0 : 1)))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              '${e.playName}',
                              style: TextStyle(
                                  color: (_countdown <= 0
                                      ? '#666666'.color()
                                      : (e.selected == true
                                          ? Colors.white
                                          : '#dc3b40'.color())),
                                  fontSize: 16),
                            ),
                            Text(
                              '赔${e.odds}',
                              // '赔 ${double.tryParse(e.odds ?? '0.00') ?? 0.00.toStringAsFixed(2)}',
                              style: TextStyle(
                                  color: (_countdown <= 0
                                      ? '#666666'.color()
                                      : (e.selected == true
                                          ? Colors.white
                                          : '#666666'.color())),
                                  fontSize: 12),
                            ),
                          ],
                        ),
                      ).paddingOnly(right: 5).onTap(() {
                        if (widget.onChange != null) {
                          widget.onChange!(widget.model, e);
                        }
                      }),
                      Visibility(
                          visible: _countdown <= 0,
                          child: Container(
                            width: 55,
                            height: 55,
                            color: Colors.white.withOpacity(0.5),
                          ))
                    ],
                  );
                }).toList())
              ],
            ))).paddingOnly(left: 10, right: 10, top: 10);
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
