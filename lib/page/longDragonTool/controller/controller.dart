import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/api/report/report_http.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/models/longDragon/long_dragon_bet_model.dart';
import 'package:fpg_flutter/models/longDragon/long_dragon_model.dart';
import 'package:fpg_flutter/models/lottery_my_bet_model/lottery_my_bet_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class LongDragonToolController extends GetxController
    with GetTickerProviderStateMixin {
  static LongDragonToolController get to =>
      Get.find<LongDragonToolController>();

  late Timer _timer;

  final TextEditingController editingController = TextEditingController();
  FocusNode focusNode = FocusNode();

  late final refreshController = AnimationController(
    duration: const Duration(seconds: 1),
    vsync: this,
  );
  late final animation = Tween(begin: 0.0, end: 10).animate(refreshController);

  late final isuRefreshController = AnimationController(
    duration: const Duration(seconds: 10),
    vsync: this,
  );
  late final isuAnimation =
      Tween(begin: 0.0, end: 10).animate(isuRefreshController);

  final detailText = '''<div>
  <p>长龙助手是对快3、时时彩、PK10、六合彩、幸运飞艇、北京赛车等特定玩法的“大小单双” 开奖结果进行跟踪统计，并可进行快捷投注的助手工具；</p>
  <p>每期出现大、小、单、双的概率为50%，如果连续3期及以上的开奖结果相同，称之为“长龙”，通常会采用倍投的方式进行“砍龙”或“顺龙”。</p>
  <h3 style="color: #cb4946; font-size: 15px;">1、什么是砍龙？</h3> 
  <p>如连续开5期“单”，可以选择“双”进行投注，这种投注方案称之为“砍龙”；</p>
  <h3 style="color: #cb4946; font-size: 15px;">2、什么是顺龙？</h3>
   <p>如连续开5期“单”，继续选择“单”进行投注，这种投注方案称之为“顺龙”；</p>
  <h3 style="color: #cb4946; font-size: 15px;">3、什么是倍投？</h3>
   <p>倍投是一种翻倍投注方式，是为了保障能够在“砍龙”或“顺龙”的过程中持续盈利的一种投注方式。</p>
   </div>''';

  final loading = true.obs;

  /// tab
  final tabs = ["最新长龙", "我的投注"];

  /// 选择的tab
  final selectedTab = 0.obs;

  /// 长龙列表
  final list = [].cast<LongDragonModel>().obs;

  /// 选中的投注
  LongDragonModel? selectedDragonModel;

  /// 投注列表
  final betList = [].cast<LotteryMyBetModel>().obs;

  /// 投注数
  final betCount = 0.obs;

  /// 金额
  final betMoney = '0'.obs;

  /// 选中tab
  handleSelectTab(int value) {
    selectedTab.value = value;
    loading.value = true;
    if (value == 0) {
      getGameChangLong(false);
      _timer = Timer.periodic(const Duration(seconds: 10), (Timer timer) {
        getGameChangLong(true);
      });
    } else {
      handleClearEvent();
      // _timer.cancel();
      getReportGetUserRecentBet();
    }
  }

  /// 处理选中的投注项目
  handleSelectedBetModel(
      int index, LongDragonModel model, LongDragonBetModel betModel) {
    if (betModel.selected == true) {
      betModel.selected = false;
      selectedDragonModel = null;
      betCount.value = 0;
    } else {
      selectedDragonModel?.betList
          ?.forEach((betItem) => betItem.selected = false);
      selectedDragonModel = model;
      betModel.selected = true;
      betCount.value = 1;
    }

    list[index] = model;
  }

  /// 清空
  handleClearEvent() {
    if (selectedDragonModel != null) {
      selectedDragonModel?.betList
          ?.forEach((betItem) => betItem.selected = false);
      selectedDragonModel = null;
      betCount.value = 0;
      betMoney.value = '0';
      editingController.clear();

      var index = list.indexOf(selectedDragonModel);
      list[index] = selectedDragonModel!;
    }
  }

  /// 开始投注
  Future<String> startBet() async {
    if (selectedDragonModel == null) {
      return '请选择一注号码投注';
    }

    double betMoneyDouble = double.tryParse(betMoney.value) ?? 0.00;
    if (betMoneyDouble == 0) {
      return '投注金额不能为空';
    }

    ToastUtils.showLoading();

    /// 处理金额, 必须是带2位小数, 否则接口报错
    String amount = betMoneyDouble.toStringAsFixed(2);

    /// 找出选中的投注项
    LongDragonBetModel? selectedBetModel =
        selectedDragonModel?.betList?.firstWhere((bet) => bet.selected == true);
    BetBeanParams betBean = BetBeanParams(
      playId: selectedBetModel?.playId,
      money: amount,
    );

    DateTime dateTime = DateTime.parse(selectedDragonModel!.closeTime!);
    // 计算时间对象的 Unix 时间戳（秒）
    int unixTimestamp = dateTime.millisecondsSinceEpoch ~/ 1000;

    BetParams betParams = BetParams(
      gameId: selectedDragonModel?.gameId,
      betIssue: selectedDragonModel?.issue.toString(),
      totalNum: '1',
      endTime: unixTimestamp.toString(),
      totalMoney: amount,
      tag: '1',
      betBean: [betBean],
    );

    var res = await UserHttp.changLongUserBet(betParams);
    ToastUtils.closeAll();

    /// 清空
    if (res.code == 0) {
      await handleRefresh();
      return '';
    }
    return res.msg;
  }

  handleRefresh() async {
    refreshController.forward();
    await GlobalService.to.fetchUserBalance();
    Future.delayed(Duration(seconds: 2));
    refreshController.stop();
  }

  getGameChangLong(bool isRefresh) async {
    final res = await GameHttp.gameChangLong();
    var models = res.models ?? [];
    if (!isRefresh) {
      loading.value = false;
    } else {
      if (selectedDragonModel != null) {
        /// 如果存在 选中项需要同步
        for (var model in models) {
          if (model.gameId == selectedDragonModel?.gameId &&
              model.playCateId == selectedDragonModel?.playCateId) {
            for (int index = 0; index < model.betList!.length; index++) {
              var betModel = model.betList?[index];
              var selectedBetModel = selectedDragonModel?.betList?[index];
              if (betModel?.playId == selectedBetModel?.playId) {
                betModel?.selected = selectedBetModel?.selected;
              }
            }

            /// 重新记录选中的
            selectedDragonModel = model;
            break;
          }
        }

        /// 如果未找到选中项 则清空之前选中的
        if (selectedDragonModel == null) {
          handleClearEvent();
        }
      }
    }
    list.value = models;
  }

  Future getReportGetUserRecentBet() async {
    final res = await ReportHttp.reportGetUserRecentBet(tag: 1);
    betList.value = res.models ?? [];
    loading.value = false;
  }

  @override
  void onInit() {
    GlobalService.to.fetchUserBalance();
    getGameChangLong(false);
    _timer = Timer.periodic(const Duration(seconds: 5), (Timer timer) {
      handleRefresh();
      getGameChangLong(true);
      getReportGetUserRecentBet();
    });
    super.onInit();
  }

  @override
  void onClose() {
    focusNode.dispose();
    refreshController.dispose();
    isuRefreshController.dispose();
    _timer.cancel();
    super.onClose();
  }
}
