import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  static SplashController get to => Get.find<SplashController>();

  handleHome() {
    Get.offAllNamed(AppRoutes.home);
  }

  getNetwork() async {
    final List<ConnectivityResult> connectivityResult =
        await (Connectivity().checkConnectivity());
    Connectivity().onConnectivityChanged.listen((state) {
      print("网络状态___$state");
      if ((state.contains(ConnectivityResult.wifi) ||
              state.contains(ConnectivityResult.mobile) ||
              state.contains(ConnectivityResult.other)) &&
          connectivityResult.contains(ConnectivityResult.none)) {
        // Restart.restartApp();
        Get.offAllNamed("/home");
      }
    });
    if ((connectivityResult.contains(ConnectivityResult.wifi) ||
        connectivityResult.contains(ConnectivityResult.mobile) ||
        connectivityResult.contains(ConnectivityResult.other))) {
      Get.offAllNamed("/home");
    }
  }

  @override
  void onReady() {
    getNetwork();
    super.onReady();
  }
}
