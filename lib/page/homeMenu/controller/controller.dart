import 'dart:convert';

import 'package:flutter/animation.dart';
import 'package:fpg_flutter/api/system/system.dart';
import 'package:fpg_flutter/app/component/home_menu/enum/ug_link_position_type.dart';
import 'package:fpg_flutter/models/home_menu/home_menu_model.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class HomeMenuController extends GetxController
    with GetTickerProviderStateMixin {
  static HomeMenuController get to => Get.find<HomeMenuController>();

  late final animationController = AnimationController(
    vsync: this,
    duration: const Duration(milliseconds: 3000),
  );

  late final spinAnimation =
      Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
    parent: animationController,
    curve: Curves.linear,
  ));

  late var homeRightMenus = [
    HomeMenuModel(
        name: '未结算',
        subId: UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(4, 4, 'wjs_r'),
        show: true,
        link: AppRoutes.lotteryLiveBet), // 即时注单
    HomeMenuModel(
        name: '今日已结',
        subId: UGLinkPositionType.jinRiYiJie.value,
        icon: img_mobileTemplateByStaticServer(6, 4, 'yj_r'),
        show: true,
        link: AppRoutes.todaySettled), // 今日已结
    HomeMenuModel(
        name: '长龙助手',
        subId: UGLinkPositionType.changLongZhuShou.value,
        icon: img_mobileTemplateByStaticServer(9, 4, 'cl_r'),
        isHot: true,
        show: true,
        link: AppRoutes.longDragonTool), // 长龙助手
    HomeMenuModel(
        name: '下注记录',
        subId: 5, //UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(10, 4, 'xzjl_r'),
        show: true,
        link: AppRoutes.lotteryTickets), // 下注记录
    HomeMenuModel(
        name: '额度转换',
        subId: UGLinkPositionType.eDuZhuanHuan.value,
        icon: img_mobileTemplateByStaticServer(1, 4, 'edzh_r'),
        show: true,
        link: realtransPath), // 额度转换
    HomeMenuModel(
        name: '开奖结果',
        subId: 0, // UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(2, 4, 'kj_r'),
        show: true,
        link: AppRoutes.lotteryResult), // 开奖结果
    HomeMenuModel(
      name: '开奖走势',
      subId: 0, //UGLinkPositionType.jiShiZhuDan.value,
      icon: img_mobileTemplateByStaticServer(6, 4, 'kj_r'),
      show: false, //siteId === 'f022',
    ), // 开奖走势
    HomeMenuModel(
        name: '开奖网',
        subId: UGLinkPositionType.jiShiZhuDan.value,
        icon: img_mobileTemplateByStaticServer(2, 4, 'kj_r'),
        show: false, //siteId === 'f022',
        link: lotteryWebsitePath), // 开奖网
    HomeMenuModel(
        name: '站内信',
        subId: UGLinkPositionType.kaiJiangWang.value,
        icon: img_mobileTemplateByStaticServer(3, 4, 'icon-email'),
        show: true,
        link: userMessagePath), // 站内信
    HomeMenuModel(
        name: '在线客服',
        subId: UGLinkPositionType.zaiXianKeFu.value,
        icon: img_mobileTemplateByStaticServer(2, 4, 'kf_active'),
        show: true,
        link: onlineServicePath), // 在线客服
    HomeMenuModel(
        name: '个人中心',
        subId: 0, //UGLinkPositionType.会员中心,
        icon: img_mobileTemplateByStaticServer(3, 4, 'wd_active'),
        show: true,
        link: '/user'), // 会员中心
    HomeMenuModel(
        name: '退出登录',
        subId: UGLinkPositionType.tuiChuDengLu.value,
        icon: img_mobileTemplateByStaticServer(4, 4, 'tc_r'),
        show: true,
        link: 'logout'), // 退出登录
  ];

  @override
  void onInit() {
    final menus = GlobalService.to.homeRightMenus;
    if (menus.isNotEmpty) {
      if (menus is List<HomeMenuModel>) {
        homeRightMenus = menus;
      } else {
        homeRightMenus = menus.map((e) => HomeMenuModel.fromJson(e)).toList();
      }
    }
    super.onInit();
  }

  @override
  void onClose() {
    animationController.dispose();
    super.onClose();
  }
}
