import 'dart:math';

import 'package:fpg_flutter/app/controllers/auth_controller.dart';
import 'package:fpg_flutter/app/controllers/main_home_controller.dart';
import 'package:fpg_flutter/app/pages/account/pages/myliuhe/myliuhe_view_controller.dart';
import 'package:fpg_flutter/app/pages/home/widgets/game_transfer_widget.dart';
import 'package:fpg_flutter/configs/pushhelper.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';
import 'package:fpg_flutter/models/home_menu/home_menu_model.dart';
import 'package:fpg_flutter/page/homeMenu/controller/controller.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'dart:io';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_common_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/app_version.dart';
import 'package:fpg_flutter/configs/constants.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/theme/app_button_styles.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:gap/gap.dart';

class HomeMenuPage extends GetView<HomeMenuController> {
  const HomeMenuPage({super.key});

  @override
  Widget build(BuildContext context) {
    // 初始化
    Get.put(HomeMenuController());
    return Scaffold(
        backgroundColor: Colors.transparent, // 设置背景透明
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                color: Colors.black.withOpacity(0.5),
              ),
            ),
            Positioned(top: 0, right: 0, child: _buildList(context)),
          ],
        ));
  }

  Widget _buildList(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      height: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: Obx(() => ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                decoration: const BoxDecoration(
                  color: AppColors.drawMenu,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).padding.top,
                    ),
                    Text(AppDefine.systemConfig?.webName ?? '',
                        style: TextStyle(color: Colors.white)),
                    if (GlobalService.to.isAuthenticated.value)
                      Text(GlobalService.to.userInfo.value?.usr ?? '',
                          style: const TextStyle(color: Colors.white)),
                    const Divider(),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10.w),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              const Text('余额：',
                                  style: TextStyle(color: Colors.white)),
                              Text(
                                  '${currencyLogo[AppDefine.systemConfig?.currency ?? ''] ?? ''} ${GlobalService.to.userBalance.balance}',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.sp,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          AnimatedBuilder(
                            animation: controller.spinAnimation,
                            builder: (context, child) {
                              return Transform.rotate(
                                angle: controller.spinAnimation.value * 10 * pi,
                                child: GestureDetector(
                                  onTap: () async {
                                    controller.animationController.forward();
                                    await GlobalService.to.fetchUserBalance();
                                    controller.animationController.stop();
                                  },
                                  child: AppImage.network(
                                    img_images('kj_refresh'),
                                    width: 20.w,
                                    height: 20.w,
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                // padding: EdgeInsets.symmetric(vertical: 5.w),
                color: Color.fromARGB(213, 255, 255, 255),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        if (!GlobalService.isShowTestToast) {
                          AppNavigator.toNamed(depositPath);
                        }
                      },
                      style: AppButtonStyles.elevatedStyle(
                          backgroundColor: AppColors.drawMenu,
                          foregroundColor: Colors.white,
                          padding: EdgeInsets.only(top: 12.h, bottom: 12.h),
                          radius: 5.w,
                          fontSize: 18),
                      child: Row(children: [
                        AppImage.asset('chonngzhi.png',
                            width: 30.w, height: 30.w),
                        SizedBox(width: 5.w),
                        const Text('充值')
                      ]),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (!GlobalService.isShowTestToast) {
                          AppNavigator.toNamed(depositPath, arguments: [1]);
                        }
                      },
                      style: AppButtonStyles.elevatedStyle(
                          backgroundColor: AppColors.drawMenu,
                          foregroundColor: Colors.white,
                          radius: 5.w,
                          padding: EdgeInsets.only(top: 12.h, bottom: 12.h),
                          fontSize: 18),
                      child: Row(children: [
                        AppImage.asset('tixian.png', width: 30.w, height: 30.w),
                        SizedBox(width: 5.w),
                        const Text('提现')
                      ]),
                    ),
                  ],
                ),
              ),
              Gap(10.w),
              ...List.generate(controller.homeRightMenus.length, (index) {
                final model = controller.homeRightMenus[index];
                if (!(model.show ?? false)) return const SizedBox();
                return InkWell(
                    onTap: () {
                      if (model.link != null && model.link!.isNotEmpty) {
                        handleDefaultJump(context, model.link!);
                      } else {
                        handleJump(context, model);
                      }
                    },
                    child: Stack(
                      alignment: AlignmentDirectional.topEnd,
                      children: [
                        Column(
                          children: [
                            Container(
                              // color: Colors.amber,
                              padding: EdgeInsets.symmetric(horizontal: 10.w),
                              child: Row(
                                children: [
                                  _getImage(model.icon, model.subId.toString()),
                                  SizedBox(
                                    width: 10.w,
                                  ),
                                  Text(model.name ?? ''),
                                  const Spacer(),
                                  if (!(model.isHot ?? false))
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      size: 30.w,
                                      color: '#888888'.color(),
                                    )
                                ],
                              ),
                            ),
                            Divider(
                              indent: 50.w,
                            ),
                          ],
                        ),
                        if (model.isHot ?? false)
                          Padding(
                            padding: EdgeInsets.only(right: 10.w),
                            child: AppImage.network(img_images('hot2x'),
                                width: 40.w, height: 40.w),
                          )
                      ],
                    ));
              }),
              Text("""
         ${GlobalService.to.packageInfo.value.version}+${GlobalService.to.versionLast.value}
         ${Platform.isAndroid ? AppVersion.andridVersion : AppVersion.iosVersion}
        """),
            ],
          )),
    );
  }

  Widget _getImage(String? icon, String subId) {
    String iconUrl = icon ?? '';
    if (iconUrl.isEmpty) {
      iconUrl = img_home('menuLists/menu_nav_$subId');
    }

    return AppImage.network(iconUrl,
        width: 30.w,
        height: 30.w,
        errorWidget: SizedBox(
          width: 20.w,
          height: 20.w,
          child: Image.asset(
            'assets/images/home_menu_placeholder.png',
            fit: BoxFit.contain,
          ),
        ));
  }

  void handleJump(BuildContext context, HomeMenuModel model) {
    LogUtil.w(
        'id:${model.id}---subId:${model.subId}---seriesId:${model.seriesId}');
    LogUtil.w('HomeMenuModel:${model.toJson()}');

    UGUserCenterType centerType = UGUserCenterType.mySixHarmonies;
    final subId = model.subId.toString();
    final seriesId = model.seriesId.toString();

    /// 长龙助手
    /// 全民竞猜
    /// 开奖走势
    /// QQ客服
    /// 开奖网
    /// 未结注单
    /// 已结注单
    /// 优惠活动
    /// 聊天室
    /// 我的关注
    /// 我的动态
    /// 我的粉丝
    /// 抢单
    /// 挂机投注
    /// 真人大厅
    /// 棋牌大厅
    /// 游戏注单
    /// 电子大厅
    /// 体育大厅
    /// 电竞大厅
    /// 彩票大厅
    /// 捕鱼大厅
    /// 路珠
    /// 六合助手
    /// 历史帖子
    /// 游戏大厅
    /// 我的页
    /// 登录页(試玩帳號必須先登出才能進入)
    /// 注册页 (試玩帳號必須先登出才能進入)
    /// 开奖结果
    /// 即时注单
    /// 眯牌
    /// APP下载
    /// 我的六合
    /// 关于APP
    /// 排行榜
    /// 若是今日已结, 则可以点进去

    if (subId == '-1') {
      Get.back();
      Future.delayed(Duration(milliseconds: 300), () {
        if (seriesId == '12') {
          Get.find<MainHomeController>().selectedPath.value = '/gameHall';
        } else {
          Get.find<MainHomeController>().selectedPath.value = '/community';
        }
      });
      return;
    } else if (subId == '0') {
      centerType = UGUserCenterType.chatRoom;
    } else if (subId == '4') {
      centerType = UGUserCenterType.onlineCustomerService;
    } else if (subId == '5') {
      centerType = UGUserCenterType.longDragonAssistant;
    } else if (subId == '6') {
      centerType = UGUserCenterType.recommendedIncome;
    } else if (subId == '9') {
      centerType = UGUserCenterType.promotionalActivities;
    } else if (subId == '13') {
      centerType = UGUserCenterType.taskCenter;
    } else if (subId == '14') {
      centerType = UGUserCenterType.siteMessage;
    } else if (subId == '20') {
      centerType = UGUserCenterType.siteMessage;
      Get.back();
      Future.delayed(Duration(milliseconds: 300), () {
        Get.find<MainHomeController>().selectedPath.value = userPath;
      });
      return;
    } else if (subId == '23') {
      centerType = UGUserCenterType.balanceConversion;
    } else if (subId == '24') {
      centerType = UGUserCenterType.realTimeOrder;
    } else if (subId == '25') {
      centerType = UGUserCenterType.settledOrder;
    } else if (subId == '26') {
      centerType = UGUserCenterType.result;
    } else if (subId == '28') {
      centerType = UGUserCenterType.fundDetails;
    } else if (subId == '31') {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) => AppCommonDialog(
              onOk: Get.find<AuthController>().logout,
              okLabel: '确定',
              msg: '确认要退出该帐号？',
              cancelLabel: '取消'));
      return;
    } else if (subId == '32') {
      centerType = UGUserCenterType.lotteryOrderRecord;
    } else if (subId == '33') {
      ToastUtils.show('请到彩种里面查看');
      return;
    } else if (subId == '36') {
      centerType = UGUserCenterType.redEnvelopeRecord;
    } else if (subId == '37') {
      centerType = UGUserCenterType.redEnvelopeRecord2;
    } else if (subId == '54') {
      centerType = UGUserCenterType.resultTrend;
    } else if (subId == '56') {
      centerType = UGUserCenterType.myFavorites;
      Get.put(MyLiuheViewController());
    } else if (subId == '57') {
      centerType = UGUserCenterType.myDynamics;
    } else if (subId == '58') {
      centerType = UGUserCenterType.myFans;
    } else if (subId == '264') {
      centerType = UGUserCenterType.lottery;
    } else if (subId == '10769' ||
        subId == '10843' ||
        subId == '10388' ||
        subId == '10869' ||
        subId == '10088') {
      /// 若是试玩账号 不支持试玩,弹框
      if (GlobalService.isTest) {
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => AppCommonDialog(
                onOk: () => Get.toNamed(loginPath),
                okLabel: '马上登录',
                title: '该游戏暂未开启试玩',
                msg: '请登录正式账号',
                cancelLabel: '取消'));
        return;
      }

      final gameItem =
          GameModel(gameId: model.gameId, gameCode: '-1', name: model.name);
      final gameTransfer = GameTransfer(context: context, gameItem: gameItem);

      if (AppDefine.systemConfig!.switchAutoTransfer == false) {
        gameTransfer.showTransfer();
      } else {
        gameTransfer.gotoGame();
      }

      return;
    }

    if (centerType == UGUserCenterType.interestTreasure ||
        centerType == UGUserCenterType.recommendedIncome ||
        centerType == UGUserCenterType.siteMessage ||
        centerType == UGUserCenterType.onlineCustomerService ||
        centerType == UGUserCenterType.longDragonAssistant ||
        centerType == UGUserCenterType.allPeopleQuiz ||
        centerType == UGUserCenterType.resultTrend ||
        centerType == UGUserCenterType.lotteryWebsite ||
        centerType == UGUserCenterType.pendingOrder ||
        centerType == UGUserCenterType.settledOrder ||
        centerType == UGUserCenterType.promotionalActivities ||
        centerType == UGUserCenterType.chatRoom ||
        centerType == UGUserCenterType.myFavorites ||
        centerType == UGUserCenterType.myDynamics ||
        centerType == UGUserCenterType.myFans ||
        centerType == UGUserCenterType.grabOrder ||
        centerType == UGUserCenterType.automaticBet ||
        centerType == UGUserCenterType.liveHall ||
        centerType == UGUserCenterType.chessHall ||
        centerType == UGUserCenterType.gameOrder ||
        centerType == UGUserCenterType.electronicHall ||
        centerType == UGUserCenterType.sportsHall ||
        centerType == UGUserCenterType.lotteryHall ||
        centerType == UGUserCenterType.fishingHall ||
        centerType == UGUserCenterType.roadBeads ||
        centerType == UGUserCenterType.historicalPosts ||
        centerType == UGUserCenterType.gameLobby ||
        centerType == UGUserCenterType.myPage ||
        centerType == UGUserCenterType.loginPage ||
        centerType == UGUserCenterType.registerPage ||
        centerType == UGUserCenterType.result ||
        centerType == UGUserCenterType.realTimeOrder ||
        centerType == UGUserCenterType.sleepyCards ||
        centerType == UGUserCenterType.appDownload ||
        centerType == UGUserCenterType.aboutApp ||
        centerType == UGUserCenterType.leaderboard ||
        centerType == UGUserCenterType.mySixHarmonies ||
        centerType == UGUserCenterType.lottery) {
      PushHelper.pushUserCenterType(centerType);
    } else {
      if (!GlobalService.isShowTestToast) {
        PushHelper.pushUserCenterType(centerType);
      }
    }
  }

  void handleDefaultJump(BuildContext context, String link) {
    if (link == 'logout') {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) => AppCommonDialog(
              onOk: Get.find<AuthController>().logout,
              okLabel: '确定',
              msg: '确认要退出该帐号？',
              cancelLabel: '取消'));
      return;
    }
    if (link == userPath) {
      Get.back();
      Future.delayed(Duration(milliseconds: 300), () {
        Get.find<MainHomeController>().selectedPath.value = link;
      });
      return;
    }

    /// 额度转换,下注记录 试玩账号进入了;
    if (link == realtransPath || link == AppRoutes.lotteryTickets) {
      if (!GlobalService.isShowTestToast) {
        AppNavigator.toNamed(link);
      }
    } else {
      AppNavigator.toNamed(link);
    }
  }
}
