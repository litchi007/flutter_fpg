import '../controller/controller.dart';
import 'package:get/get.dart';

class HomeMenuBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeMenuController>(() => HomeMenuController());
  }
}
