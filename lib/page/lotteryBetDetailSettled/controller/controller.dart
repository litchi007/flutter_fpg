import 'package:fpg_flutter/api/ticket/ticket.http.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_model.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LotteryBetDetailSettledController extends GetxController {
  static LotteryBetDetailSettledController get to =>
      Get.find<LotteryBetDetailSettledController>();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  /// 页码
  int page = 1;

  /// 游戏Id
  String gameId = '';

  /// 日期
  String date = '';

  /// 下注金额
  Rx<double> totalBetAmount = 0.00.obs;

  /// 输赢金额
  Rx<double> totalWinAmount = 0.00.obs;

  /// 注单列表
  late var list = [].cast<LotteryHistoryModel>().obs;

  /// 获取注单列表
  Future getTickeHistory(bool isRefresh) async {
    if (isRefresh) {
      page = 1;
    } else {
      page++;
    }

    final res = await TicketHttp.ticketHistory(
      page: page,
      gameId: gameId,
      category: 'lottery',
      startDate: date,
      endDate: date,
      status: 5,
    );

    var resultList = res.data!.list ?? [];
    List<LotteryHistoryModel> tempList = [];
    if (page == 1) {
      totalBetAmount.value =
          double.tryParse(res.data!.totalBetAmount ?? '0.00') ?? 0.00;
      totalWinAmount.value =
          double.tryParse(res.data!.totalWinAmount ?? '0.00') ?? 0.00;
    } else {
      totalBetAmount.value +=
          double.tryParse(res.data!.totalBetAmount ?? '0.00') ?? 0.00;
      totalWinAmount.value +=
          double.tryParse(res.data!.totalWinAmount ?? '0.00') ?? 0.00;
      tempList.addAll(list);
    }

    if (resultList.length < 30) {
      refreshController.loadNoData();
    } else {
      refreshController.loadComplete();
    }

    tempList.addAll(resultList);
    list.value = tempList;
  }

  Future onRefresh() async {
    getTickeHistory(true);
  }

  Future onLoading() async {
    getTickeHistory(false);
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null && arguments is Map) {
      date = arguments['date'];
      gameId = arguments['gameId'];
    }
    super.onInit();
  }

  @override
  void onReady() {
    getTickeHistory(true);
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
