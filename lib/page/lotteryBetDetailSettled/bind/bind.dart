import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryBetDetailSettledBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<LotteryBetDetailSettledController>(
        () => LotteryBetDetailSettledController());
  }
}
