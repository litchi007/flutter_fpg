import '../controller/controller.dart';
import 'package:get/get.dart';

class ExpertPlanBetBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ExpertPlanBetController>(() => ExpertPlanBetController());
  }
}
