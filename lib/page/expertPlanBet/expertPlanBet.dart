import 'package:flutter/services.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/page/expertPlanBet/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/drop_down_utils.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class ExpertPlanBetPage extends GetView<ExpertPlanBetController> {
  /// 游戏数据
  final ExpertPlanGameModel gameModel;

  /// 当期数据
  final GameNextIssueModel issueModel;

  /// 专家数据
  final ExpertPlanDataModel dataModel;

  /// 预测位置
  final Position position;

  /// 是否跟投
  final bool isFollowBet;

  /// 玩法和赔率
  final Map<String, Play> oddsMap;

  const ExpertPlanBetPage(this.gameModel, this.issueModel, this.dataModel,
      this.position, this.isFollowBet, this.oddsMap,
      {super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ExpertPlanBetController());
    controller.gameModel = gameModel;
    controller.issueModel = issueModel;
    controller.dataModel = dataModel;
    controller.position = position;
    controller.isFollowBet = isFollowBet;
    controller.oddsMap = oddsMap;
    return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.5), // 设置背景透明
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.fromLTRB(18, 10, 18, 10),
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 44,
                        color: '#6a98ec'.color(),
                        alignment: Alignment.center,
                        child: Text(
                          gameModel.gameName ?? '',
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      Positioned(
                        top: 0,
                        right: 0,
                        width: 44,
                        height: 44,
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 30,
                        ).onTap(() {
                          controller.focusNode.unfocus();
                          Get.back();
                        }),
                      )
                    ],
                  ),
                  _buildHeaderWidget(context),
                  Container(
                    height: 1,
                    color: '#dddddd'.color(),
                  ).paddingSymmetric(horizontal: 12),
                  Expanded(
                    child: Obx(() {
                      return controller.betInfoList.isNotEmpty
                          ? ListView.builder(
                              padding: EdgeInsets.zero,
                              itemBuilder: (c, i) =>
                                  _buildExpertBetItemWidget(i),
                              itemCount: controller.betInfoList.length,
                            )
                          : Container();
                    }),
                  ),
                  _buildBottomTool()
                ],
              ),
            ),
          ).onTap(() => controller.focusNode.unfocus()),
        ));
  }

  /// 标题
  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Text('金额',
                textAlign: TextAlign.center,
                style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Text('玩法',
                style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Text('赔率',
                style: TextStyle(color: '#ff0000'.color(), fontSize: 14)),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Text('操作',
                style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
          ),
        ),
      ],
    ).paddingOnly(left: 12, top: 10, bottom: 10);
  }

  /// 专家预测投注列表item
  Widget _buildExpertBetItemWidget(int index) {
    ExpertPlanBetInfo betInfo = controller.betInfoList[index];
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                height: 30,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: '#dddddd'.color(), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                child: TextField(
                  cursorWidth: 1,
                  maxLength: 5,
                  controller: controller.textControllerList[index],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 12.0, // 设置字体大小
                      color: Colors.black),
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: true, signed: false),
                  // 设置键盘类型为数字
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
                    // 只允许输入数字
                  ],
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                    hintText: '0',
                    hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
                    filled: true,
                    fillColor: Colors.white,
                    counterText: '',
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    // 输入框背景颜色
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: BorderSide.none, // 隐藏默认边框
                    ),
                  ),
                  onChanged: (text) {},
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 5),
                alignment: Alignment.center,
                child: Text('${position.name}-${betInfo.playNum}',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text(double.parse(betInfo.odds).toStringAsFixed(2),
                    style: TextStyle(color: '#ff0000'.color(), fontSize: 14)),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Icon(
                  Icons.delete_forever_outlined,
                  color: Colors.redAccent,
                ),
              ).onTap(() {
                controller.betInfoList.removeAt(index);
                controller.textControllerList.removeAt(index);
                // if (controller.betInfoList.isEmpty) {
                //   controller.focusNode.unfocus();
                //   Future.delayed(Duration(milliseconds: 10), (){
                //     Get.back();
                //   });
                // }
              }),
            ),
          ],
        ).paddingSymmetric(vertical: 5),
        Container(
          height: 1,
          color: '#dddddd'.color(),
        )
      ],
    ).paddingOnly(left: 12);
  }

  /// 底部工具栏
  Widget _buildBottomTool() {
    return Container(
      height: 96,
      color: '#f5f5f5'.color(),
      child: Column(
        children: [
          Container(
            height: 1,
            color: '#dddddd'.color(),
          ),
          SizedBox(height: 11),
          Row(
            children: [
              Obx(() => Text('已选${controller.betInfoList.length}注',
                      style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)))
                  .paddingOnly(right: 12),
              Obx(() => Text('余额：¥${GlobalService.to.userBalance.balance}',
                  style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)))
            ],
          ).paddingOnly(left: 12, bottom: 10),
          Row(
            children: [
              Container(
                width: 72,
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      '#eefba9'.color(),
                      '#e2be39'.color(),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        // 阴影颜色
                        spreadRadius: 0,
                        // 阴影扩散半径
                        blurRadius: 5,
                        // 模糊半径
                        offset: Offset(2.5, 2.5),
                        blurStyle: BlurStyle.inner // 阴影偏移量
                        ),
                  ],
                ),
                child: Text(
                  '重置',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ).onTap(() => controller.handleReset()),
              SizedBox(width: 10),
              Container(
                width: 120,
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: '#dddddd'.color(), width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        // 阴影颜色
                        spreadRadius: 0,
                        // 阴影扩散半径
                        blurRadius: 5,
                        // 模糊半径
                        offset: Offset(2.5, 2.5),
                        blurStyle: BlurStyle.inner // 阴影偏移量
                        ),
                  ],
                ),
                child: TextField(
                  cursorWidth: 1,
                  maxLength: 5,
                  controller: controller.editingController,
                  style: TextStyle(
                      fontSize: 12.0, // 设置字体大小
                      color: Colors.black),
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: true, signed: false),
                  // 设置键盘类型为数字
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
                    // 只允许输入数字
                  ],
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
                    hintText: '输入金额',
                    hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
                    filled: true,
                    fillColor: Colors.white,
                    counterText: '',
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    // 输入框背景颜色
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      borderSide: BorderSide.none, // 隐藏默认边框
                    ),
                  ),
                  onChanged: (text) {},
                ),
              ),
              Spacer(),
              Container(
                width: 72,
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      '#a9cefb'.color(),
                      '#396de2'.color(),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        // 阴影颜色
                        spreadRadius: 0,
                        // 阴影扩散半径
                        blurRadius: 5,
                        // 模糊半径
                        offset: Offset(2.5, 2.5),
                        blurStyle: BlurStyle.inner // 阴影偏移量
                        ),
                  ],
                ),
                child: Text(
                  '投注',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ).onTap(() async {
                controller.focusNode.unfocus();
                var res = await controller.startBet();
                ToastUtils.show(res.isNotEmpty ? res : '投注成功');
                if (res.isEmpty) {
                  Get.back();
                }
              }),
            ],
          ).paddingSymmetric(horizontal: 12),
        ],
      ),
    );
  }
}
