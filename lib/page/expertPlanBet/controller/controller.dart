import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/models/game_next_issue_model/game_next_issue_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/drop_down_utils.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

// 定义 投注信息 数据模型
class ExpertPlanBetInfo {
  String amount;
  final String odds;
  final String id;
  final String playNum;

  ExpertPlanBetInfo({
    required this.amount,
    required this.odds,
    required this.id,
    required this.playNum,
  });
}

class ExpertPlanBetController extends GetxController {
  static ExpertPlanBetController get to => Get.find<ExpertPlanBetController>();

  /// 追号数据
  late ExpertPlanGameModel gameModel;

  /// 当期数据
  late GameNextIssueModel issueModel;

  /// 专家数据
  late ExpertPlanDataModel dataModel;

  /// 预测位置
  late Position position;

  /// 是否跟投
  late bool isFollowBet;

  /// 玩法和赔率
  late Map<String, Play> oddsMap;

  /// 投注列表
  final betInfoList = [].cast<ExpertPlanBetInfo>().obs;

  late TextEditingController editingController = TextEditingController()
    ..addListener(changeMoneyListener);
  FocusNode focusNode = FocusNode();

  /// 输入框控制器数组
  final textControllerList = [].cast<TextEditingController>().obs;

  /// 生肖
  List<String> zodiacs = [
    '鼠',
    '牛',
    '虎',
    '兔',
    '龙',
    '蛇',
    '马',
    '羊',
    '猴',
    '鸡',
    '狗',
    '猪',
  ];

  /// 尾数
  List<String> mantissa = [
    '0尾',
    '1尾',
    '2尾',
    '3尾',
    '4尾',
    '5尾',
    '6尾',
    '7尾',
    '8尾',
    '9尾',
  ];

  /// 默认
  List<String> defaultNum = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

  /// 幸运飞艇
  List<String> xyfts = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
  ];

  List<String> pk10s = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
  ];

  List<String> gd11x5s = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
  ];

  changeMoneyListener() {
    textControllerList.value = textControllerList.map((e) {
      e.text = editingController.text;
      return e;
    }).toList();
  }

  handleReset() {
    editingController.text = '';
    for (var c in textControllerList) {
      c.text = '';
    }
  }

  /// 获取投注信息数据
  getBetInfoData() {
    List<String> arr = defaultNum;
    String? gameType = issueModel.gameType;
    String positionName = position.name ?? '';
    if (gameType == 'lhc') {
      if (positionName.contains('尾')) {
        arr = mantissa;
      } else if (positionName.contains('肖')) {
        arr = zodiacs;
      }
    } else if (gameType == 'gd11x5') {
      if (positionName.contains('球') || positionName.contains('中')) {
        arr = gd11x5s;
      }
    } else if (gameType == 'xyft') {
      arr = xyfts;
    } else if (gameType == 'pk10') {
      arr = pk10s;
    }

    List<String> ycData = dataModel.ycData ?? [];
    if (!isFollowBet) {
      /// 反投
      ycData = arr.where((item) => !ycData.contains(item)).toList();
    }

    List<ExpertPlanBetInfo> dataList = [];
    if (positionName == '合肖') {
      String keyPositive = '$positionName$positionName${ycData.length}';
      Play? play = oddsMap[keyPositive];

      dataList.add(ExpertPlanBetInfo(
        amount: '0',
        odds: play?.odds ?? '0.00',
        id: play?.id ?? '',
        playNum: ycData.join(','),
      ));
    } else {
      for (String y in ycData) {
        if (gameType == 'gd11x5' &&
            (positionName.contains('球') || positionName.contains('中')) &&
            y.startsWith('0')) {
          y = y.replaceFirst('0', '');
        }

        if (['xyft', 'pk10'].contains(gameType) && y.startsWith('0')) {
          y = y.replaceFirst('0', '');
        }

        String keyPositive = (positionName + y)
            .replaceAll('特码尾数', '头/尾数')
            .replaceAll('平特一肖尾数', '平特尾数');

        Play? play = oddsMap[keyPositive];
        dataList.add(ExpertPlanBetInfo(
          amount: '0',
          odds: play?.odds ?? '0.00',
          id: play?.id ?? '',
          playNum: y,
        ));
      }
    }

    betInfoList.value = dataList;
    textControllerList.value =
        dataList.map((e) => TextEditingController()).toList();
  }

  /// 开始投注
  Future<String> startBet() async {
    if (betInfoList.isEmpty) {
      return '暂无投注信息';
    }

    /// 同步金额和计算总金额
    double totalMoney = 0;

    /// 统计投信息
    List<BetBeanParams> betBeanList = [];
    for (int index = 0; index < betInfoList.length; index++) {
      var betInfo = betInfoList[index];
      var controller = textControllerList[index];
      var amount = double.tryParse(controller.text) ?? 0.00;
      betInfo.amount = amount.toStringAsFixed(2);
      totalMoney += amount * 10000;
      if (amount > 0) {
        betBeanList.add(BetBeanParams(
            betInfo: ('合肖' == position.name ? betInfo.playNum : ''),
            playId: betInfo.id,
            money: betInfo.amount,
            odds: betInfo.odds,
            playIds: ''));
      }
    }

    if (betBeanList.isEmpty) {
      return '请输入金额';
    }

    ToastUtils.showLoading();

    DateTime dateTime = DateTime.parse(issueModel.curCloseTime ?? '0');
    // 计算时间对象的 Unix 时间戳（秒）
    int unixTimestamp = dateTime.millisecondsSinceEpoch ~/ 1000;

    BetParams betParams = BetParams(
        gameId: issueModel.id,
        betIssue: issueModel.curIssue,
        totalNum: betBeanList.length.toString(),
        endTime: unixTimestamp.toString(),
        totalMoney: (totalMoney / 10000).toStringAsFixed(2),
        tag: isFollowBet ? '4' : '5',
        betBean: betBeanList,
        position: position.value,
        ycLength: dataModel.ycLength,
        eid: dataModel.eid);

    final res = await UserHttp.userGameBetWithParams(betParams);
    ToastUtils.closeAll();
    if (res.code == 0) {
      GlobalService.to.fetchUserBalance();
      return '';
    }

    return res.msg;
  }

  @override
  void onReady() {
    getBetInfoData();
    super.onReady();
  }

  @override
  void onClose() {
    editingController.removeListener(changeMoneyListener);
    super.onClose();
  }
}
