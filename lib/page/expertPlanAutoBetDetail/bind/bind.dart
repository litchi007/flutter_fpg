import '../controller/controller.dart';
import 'package:get/get.dart';

class ExpertPlanAutoBetDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ExpertPlanAutoBetDetailController>(
        () => ExpertPlanAutoBetDetailController());
  }
}
