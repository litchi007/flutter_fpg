import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_record_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class ExpertPlanAutoBetDetailController extends GetxController {
  static ExpertPlanAutoBetDetailController get to =>
      Get.find<ExpertPlanAutoBetDetailController>();

  final gameModel = ExpertPlanGameModel().obs;

  final loading = true.obs;

  /// 跟单记录列表
  final recordList = [].cast<ExpertPlanFollowRecordModel>().obs;

  /// 获取跟单记录列表
  getFollowList() {
    UserHttp.getAutoFollowFollowList(gameModel.value.gameId ?? '').then((res) {
      LogUtil.w(res.models);
      recordList.value = res.models ?? [];
      loading.value = false;
    }).catchError((error) {
      loading.value = false;
      LogUtil.w(error);
    });
  }

  /// 删除跟单记录
  deleteRecord(String orderId) {
    ToastUtils.showLoading();
    UserHttp.autoFollowDeleteFollow(orderId).then((res) {
      ToastUtils.closeAll();
      ToastUtils.show(res.msg);
      if (res.code == 0) {
        getFollowList();
      }
    }).catchError((error) {
      ToastUtils.closeAll();
      ToastUtils.show('删除失败');
    });
  }

  @override
  void onReady() {
    getFollowList();
    super.onReady();
  }
}
