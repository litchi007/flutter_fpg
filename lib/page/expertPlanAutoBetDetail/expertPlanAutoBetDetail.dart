import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_record_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/page/expertPlanAutoBetDetail/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ExpertPlanAutoBetDetailPage
    extends GetView<ExpertPlanAutoBetDetailController> {
  final ExpertPlanGameModel model;

  const ExpertPlanAutoBetDetailPage(this.model, {super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ExpertPlanAutoBetDetailController());
    controller.gameModel.value = model;
    return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.5), // 设置背景透明
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.fromLTRB(18, 44, 18, 40),
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 44,
                        color: '#6a98ec'.color(),
                        alignment: Alignment.center,
                        child: Text(
                          model.gameName ?? '',
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      Positioned(
                        top: 0,
                        right: 0,
                        width: 44,
                        height: 44,
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 30,
                        ).onTap(() => Get.back()),
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: Text(
                      '自动跟单明细',
                      style: TextStyle(
                          color: '#3c3c3c'.color(),
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  _buildHeaderWidget(context),
                  Container(
                    height: 1,
                    color: '#dddddd'.color(),
                  ).paddingSymmetric(horizontal: 12),
                  Obx(() => Expanded(
                          child: CustomLoading(
                        status: controller.loading.value,
                        child: controller.recordList.isNotEmpty
                            ? ListView.builder(
                                itemBuilder: (c, i) =>
                                    _buildExpertBetDetailItemWidget(i),
                                itemCount: controller.recordList.length,
                              )
                            : Text(
                                textAlign: TextAlign.center,
                                '暂无自动跟单明细',
                                style: TextStyle(
                                    color: '#333333'.color(), fontSize: 15),
                              ).paddingOnly(top: 10, bottom: 10),
                      ))),
                  SizedBox(height: 5)
                ],
              ),
            ),
          ),
        ));
  }

  /// 标题
  Widget _buildHeaderWidget(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        color: '4a76c5'.color(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text('专家',
                    style: TextStyle(color: Colors.white, fontSize: 14)),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text('码数',
                    style: TextStyle(color: Colors.white, fontSize: 14)),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text('球号',
                    style: TextStyle(color: Colors.white, fontSize: 14)),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text('期数',
                    style: TextStyle(color: Colors.white, fontSize: 14)),
              ),
            ),
            Expanded(
              child: SizedBox(),
            ),
          ],
        ));
  }

  /// 专家预测投注列表item
  Widget _buildExpertBetDetailItemWidget(int index) {
    ExpertPlanFollowRecordModel model = controller.recordList[index];
    return Container(
      color: index % 2 != 0 ? '#dddddd'.color() : Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(model.name ?? '',
                  style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(model.lengthText ?? '',
                  style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(model.positionText ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text('${model.issueCount}期',
                  style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Icon(
                Icons.delete_forever_outlined,
                color: Colors.redAccent,
              ),
            ).onTap(() => showDeleteAutoBetRecordDialog(model.id ?? '')),
          ),
        ],
      ).paddingSymmetric(vertical: 5),
    );
  }

  void showDeleteAutoBetRecordDialog(String orderId) {
    SmartDialog.show(builder: (context) {
      return Container(
        width: MediaQuery.of(context).size.width - 36,
        height: 100,
        decoration: BoxDecoration(
          color: context.customTheme?.lotteryPopupItemBg,
          borderRadius: const BorderRadius.all(Radius.circular(4)),
        ),
        child: Column(
          children: [
            const SizedBox(height: 10),
            Text(
              '您确定要删除此条信息吗？',
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                const SizedBox(width: 10),
                Expanded(
                    child: Container(
                  height: 42,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    // 设置渐变色背景
                    border: Border.all(
                      color: const Color(0xffa9a9a9), // 边框颜色
                      width: 1.0, // 边框宽度
                    ),
                    borderRadius: BorderRadius.circular(4), // 设置圆角
                  ),
                  child: Text(
                    '取消',
                    style: TextStyle(
                        color: context.customTheme?.fontColor, fontSize: 16),
                  ),
                ).onTap(() => SmartDialog.dismiss())),
                const SizedBox(width: 5),
                Expanded(
                  child: Container(
                    height: 42,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          '#b89b65'.color(),
                          '#b89b65'.color(),
                        ],
                      ), // 设置渐变色背景
                      borderRadius: BorderRadius.circular(4), // 设置圆角
                    ),
                    child: const Text(
                      '确定',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ).onTap(() {
                    SmartDialog.dismiss();
                    controller.deleteRecord(orderId);
                  }),
                ),
                const SizedBox(width: 10),
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      );
    });
  }
}
