import '../controller/controller.dart';
import 'package:get/get.dart';

class ExpertPlanAutoBetBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ExpertPlanAutoBetController>(
        () => ExpertPlanAutoBetController());
  }
}
