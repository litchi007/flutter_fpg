import 'package:dartx/dartx.dart';
import 'package:flutter/services.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_issue_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_task_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/page/expertPlanAutoBet/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/utils/drop_down_utils.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class ExpertPlanAutoBetPage extends GetView<ExpertPlanAutoBetController> {
  /// 游戏
  final ExpertPlanGameModel gameModel;

  /// 专家数据
  final ExpertPlanDataModel dataModel;

  /// 预测位置
  final Position position;

  const ExpertPlanAutoBetPage(this.gameModel, this.dataModel, this.position,
      {super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ExpertPlanAutoBetController());
    controller.gameModel = gameModel;
    controller.dataModel = dataModel;
    controller.position = position;

    return Scaffold(
        backgroundColor: Colors.black.withOpacity(0.5), // 设置背景透明
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 44,
                        color: '#6a98ec'.color(),
                        alignment: Alignment.center,
                        child: Text(
                          gameModel.gameName ?? '',
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      ),
                      Positioned(
                        top: 0,
                        right: 0,
                        width: 44,
                        height: 44,
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 30,
                        ).onTap(() {
                          controller.focusNode.unfocus();
                          Get.back();
                        }),
                      )
                    ],
                  ),
                  _buildExpertDataHeaderWidget(),
                  Obx(() => Expanded(
                      child: CustomLoading(
                          status: controller.loading.value,
                          child: _getListWidget()))),
                  _buildBottomTool()
                ],
              ),
            ),
          ).onTap(() => controller.focusNode.unfocus()),
        ));
  }

  /// 专家追号头部
  Widget _buildExpertDataHeaderWidget() {
    return Container(
      color: '#f6f6f6'.color(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text.rich(TextSpan(
              text: '专',
              style: TextStyle(color: '#3c3c3c'.color(), fontSize: 16),
              children: [
                /// 这两个字是占位用的
                TextSpan(
                    text: '专家',
                    style: TextStyle(color: Colors.transparent, fontSize: 16)),
                TextSpan(text: '家：'),
                TextSpan(text: dataModel.name),
              ])).paddingOnly(left: 8, top: 5, bottom: 5),
          Row(
            children: [
              Text('计划类型：${dataModel.ycLength}码计划',
                  style: TextStyle(color: '#3c3c3c'.color(), fontSize: 16)),
              SizedBox(width: 30),
              Text('球号：${position.name}',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: '#3c3c3c'.color(), fontSize: 16)),
            ],
          ).paddingOnly(left: 8, bottom: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('快捷跟投：',
                  style: TextStyle(color: '#333333'.color(), fontSize: 16)),
              Obx(() => Row(
                      children: controller.tabs.mapIndexed((i, e) {
                    return Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                      constraints: const BoxConstraints(
                        minWidth: 30,
                      ),
                      height: 30,
                      decoration: BoxDecoration(
                          color: controller.fastBetNumber.value == e
                              ? '#6495ed'.color()
                              : Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          border: Border.all(
                              color: '#cccccc'.color(),
                              width:
                                  controller.fastBetNumber.value == e ? 0 : 1)),
                      child: Text(
                        '${e}期',
                        style: TextStyle(
                            color: controller.fastBetNumber.value == e
                                ? Colors.white
                                : Colors.black),
                      ),
                    )
                        .paddingOnly(right: 5)
                        .onTap(() => controller.handleSelectedIssue(e));
                  }).toList()))
            ],
          ).paddingOnly(left: 8, bottom: 5),
          Obx(() => Row(children: _getHeaderInputRowChildren()))
              .paddingOnly(left: 8, bottom: 5),
          Container(
            height: 1,
            color: '#dddddd'.color(),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            color: '#edf4fe'.color(),
            child: Obx(() => Row(
                  children: _getHeaderIssueRowChildren(),
                )),
          )
        ],
      ),
    );
  }

  List<Widget> _getHeaderInputRowChildren() {
    List<Widget> children = [];

    children.add(Text('注：已开盘期数将不被保存',
        style: TextStyle(color: '#ff0000'.color(), fontSize: 12)));
    children.add(SizedBox(width: 10));
    children.add(Container(
      width: 64,
      height: 24,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(4)),
        border: Border.all(color: '#dddddd'.color(), width: 1),
      ),
      child: TextField(
        cursorWidth: 1,
        maxLength: 7,
        controller: controller.editingControllerOne,
        enabled: controller.followBetIsEnabled(),
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 12.0, // 设置字体大小
            color: Colors.black),
        keyboardType:
            TextInputType.numberWithOptions(decimal: true, signed: false),
        // 设置键盘类型为数字
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
        ],
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
          filled: true,
          fillColor: controller.followBetIsEnabled()
              ? Colors.white
              : '#f8f8f8'.color(),
          counterText: '',
          floatingLabelBehavior: FloatingLabelBehavior.never,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4.0),
            borderSide: BorderSide.none, // 隐藏默认边框
          ),
        ),
        onChanged: (text) {},
      ),
    ));
    children.add(
        _getExchangeWidget().onTap(() => controller.handleHeaderExChange()));
    children.add(Container(
      width: 64,
      height: 24,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(4)),
        border: Border.all(color: '#dddddd'.color(), width: 1),
      ),
      child: TextField(
        cursorWidth: 1,
        maxLength: 7,
        controller: controller.editingControllerTwo,
        enabled: controller.reverseBetIsEnabled(),
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 12.0, // 设置字体大小
            color: Colors.black),
        keyboardType:
            TextInputType.numberWithOptions(decimal: true, signed: false),
        // 设置键盘类型为数字
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
        ],
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
          filled: true,
          fillColor: controller.reverseBetIsEnabled()
              ? Colors.white
              : '#f8f8f8'.color(),
          counterText: '',
          floatingLabelBehavior: FloatingLabelBehavior.never,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4.0),
            borderSide: BorderSide.none, // 隐藏默认边框
          ),
        ),
        onChanged: (text) {},
      ),
    ));
    return children;
  }

  /// 获取头部期数Row的子widget
  List<Widget> _getHeaderIssueRowChildren() {
    List<Widget> children = [];

    children.add(
        Text('跟投期', style: TextStyle(color: '#333333'.color(), fontSize: 14))
            .paddingOnly(left: 8));
    children.add(Container(
            width: 24,
            height: 24,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(12)),
                border: Border.all(color: '#dddddd'.color(), width: 1)),
            child: Text(
              '-',
              style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12),
            ))
        .paddingSymmetric(horizontal: 3.6)
        .onTap(() => controller.handleIssueChange()));

    children.add(Container(
      width: 56,
      height: 24,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: '#f8f8f8'.color(),
          borderRadius: BorderRadius.all(Radius.circular(4)),
          border: Border.all(color: '#dddddd'.color(), width: 1)),
      child: TextField(
        cursorWidth: 1,
        maxLength: 3,
        controller: controller.issueController,
        enabled: controller.issueInputIsEnabled(),
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 12.0, // 设置字体大小
            color: Colors.black),
        keyboardType:
            TextInputType.numberWithOptions(decimal: true, signed: false),
        // 设置键盘类型为数字
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'^\d+$'))
        ],
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
          filled: true,
          fillColor: controller.issueInputIsEnabled()
              ? Colors.white
              : '#f8f8f8'.color(),
          counterText: '',
          floatingLabelBehavior: FloatingLabelBehavior.never,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4.0),
            borderSide: BorderSide.none, // 隐藏默认边框
          ),
        ),
        onChanged: (text) {},
      ),
    ));

    children.add(Container(
            width: 24,
            height: 24,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(12)),
                border: Border.all(color: '#dddddd'.color(), width: 1)),
            child: Text(
              '+',
              style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12),
            ))
        .paddingSymmetric(horizontal: 3.6)
        .onTap(() => controller.handleIssueChange(isAdd: true)));

    children.add(Expanded(
        child: Text('正投',
            textAlign: TextAlign.left,
            style: TextStyle(color: '#333333'.color(), fontSize: 14))));

    children.add(Expanded(
        child: Text('反投',
            textAlign: TextAlign.center,
            style: TextStyle(color: '#333333'.color(), fontSize: 14))));

    children.add(Expanded(
        child: Text('状态',
            textAlign: TextAlign.center,
            style: TextStyle(color: '#333333'.color(), fontSize: 14))));

    return children;
  }

  /// 获取不同的listView
  Widget _getListWidget() {
    if (controller.taskList.isNotEmpty) {
      return ListView.builder(
        padding: EdgeInsets.zero,
        itemBuilder: (c, i) =>
            _buildExpertAutoBetTaskItemWidget(controller.taskList[i]),
        itemCount: controller.taskList.length,
      );
    }

    return controller.betInfoList.isNotEmpty
        ? ListView.builder(
            padding: EdgeInsets.zero,
            itemBuilder: (c, i) => _buildExpertAutoBetInfoItemWidget(i),
            itemCount: controller.betInfoList.length,
          )
        : Text(
            '暂无相关信息',
            textAlign: TextAlign.center,
            style: TextStyle(color: '#333333'.color(), fontSize: 15),
          ).paddingOnly(top: 10, bottom: 10);
  }

  /// 自动跟单已投列表item
  Widget _buildExpertAutoBetTaskItemWidget(
      ExpertPlanFollowTaskModel taskModel) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text('${taskModel.issue}',
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12)),
              ),
            ),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text(
                    taskModel.betType == '1' ? taskModel.money.toString() : '',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12)),
              ),
            ),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            _getExchangeWidget(),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text(
                    taskModel.betType == '2' ? taskModel.money.toString() : '',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12)),
              ),
            ),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text('已保存',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12)),
              ),
            ),
          ],
        ),
        Container(
          height: 1,
          color: '#dddddd'.color(),
        )
      ],
    );
  }

  /// 自动跟单投注信息列表item
  Widget _buildExpertAutoBetInfoItemWidget(int index) {
    ExpertPlanFollowIssueModel issueModel = controller.betInfoList[index];
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text('${issueModel.displayNumber ?? issueModel.issue}',
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12)),
              ),
            ),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            Expanded(
                child: Container(
              height: 24,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: '#f8f8f8'.color(),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  border: Border.all(color: '#dddddd'.color(), width: 1)),
              child: TextField(
                  cursorWidth: 1,
                  maxLength: 7,
                  controller: controller.textControllerListOne[index],
                  enabled: issueModel.betType != '2',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 12.0, // 设置字体大小
                      color: Colors.black),
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: true, signed: false),
                  // 设置键盘类型为数字
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
                  ],
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                    filled: true,
                    fillColor: issueModel.betType != '2'
                        ? Colors.white
                        : '#f8f8f8'.color(),
                    counterText: '',
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      borderSide: BorderSide.none, // 隐藏默认边框
                    ),
                  ),
                  onChanged: (text) => controller.handleAutoBetItemInputEvent(
                      index, issueModel, text)),
            ).paddingSymmetric(horizontal: 2.5)),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            _getExchangeWidget().onTap(() => controller
                .handleAutoBetItemInputExchangeEvent(issueModel, index)),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            Expanded(
                child: Container(
              // width: 56,
              height: 24,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: '#f8f8f8'.color(),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  border: Border.all(color: '#dddddd'.color(), width: 1)),
              child: TextField(
                  cursorWidth: 1,
                  maxLength: 7,
                  controller: controller.textControllerListTwo[index],
                  enabled: issueModel.betType != '1',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 12.0, // 设置字体大小
                      color: Colors.black),
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: true, signed: false),
                  // 设置键盘类型为数字
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
                  ],
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                    filled: true,
                    fillColor: issueModel.betType != '1'
                        ? Colors.white
                        : '#f8f8f8'.color(),
                    counterText: '',
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                      borderSide: BorderSide.none, // 隐藏默认边框
                    ),
                  ),
                  onChanged: (text) => controller.handleAutoBetItemInputEvent(
                      index, issueModel, text,
                      isFollowBet: false)),
            ).paddingSymmetric(horizontal: 2.5)),
            Container(
              width: 1,
              height: 30,
              color: '#dddddd'.color(),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Text('未保存',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 12)),
              ),
            ),
          ],
        ),
        Container(
          height: 1,
          color: '#dddddd'.color(),
        )
      ],
    );
  }

  /// 交换图标
  Widget _getExchangeWidget() {
    return Text('⇄',
            style: TextStyle(
                color: Colors.black, fontSize: 14, fontWeight: FontWeight.w600))
        .paddingSymmetric(horizontal: 10);
  }

  /// 底部工具栏
  Widget _buildBottomTool() {
    return Container(
      height: 76,
      decoration: BoxDecoration(
        color: '#f5f5f5'.color(),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3), // 阴影颜色
            spreadRadius: 0, // 阴影扩散半径
            blurRadius: 10, // 模糊半径
            offset: Offset(0, -2), // 阴影偏移量
          ),
        ],
      ),
      child: Column(
        children: [
          Container(
            height: 32,
            color: Colors.white,
            alignment: Alignment.center,
            child: Obx(() => Text('总金额：${controller.betTotalMoney.value}',
                    style: TextStyle(color: '#3c3c3c'.color(), fontSize: 14)))
                .paddingOnly(right: 12),
          ),
          Row(
            children: [
              Spacer(),
              Container(
                width: 72,
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      '#a9cefb'.color(),
                      '#396de2'.color(),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        // 阴影颜色
                        spreadRadius: 0,
                        // 阴影扩散半径
                        blurRadius: 5,
                        // 模糊半径
                        offset: Offset(2.5, 2.5),
                        blurStyle: BlurStyle.inner // 阴影偏移量
                        ),
                  ],
                ),
                child: Text(
                  '确定',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ).onTap(() async {
                if (controller.taskList.isNotEmpty) {
                  Get.back();
                } else {
                  var res = await controller.saveFollowBetList();
                  ToastUtils.show(res.isNotEmpty ? res : '保存跟单成功');
                  if (res.isEmpty) {
                    Get.back();
                  }
                }
              }),
              SizedBox(width: 20),
              Container(
                width: 72,
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      '#eefba9'.color(),
                      '#e2be39'.color(),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        // 阴影颜色
                        spreadRadius: 0,
                        // 阴影扩散半径
                        blurRadius: 5,
                        // 模糊半径
                        offset: Offset(2.5, 2.5),
                        blurStyle: BlurStyle.inner // 阴影偏移量
                        ),
                  ],
                ),
                child: Text(
                  '取消',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ).onTap(() => Get.back()),
              Spacer(),
            ],
          ).paddingOnly(top: 5),
        ],
      ),
    );
  }
}
