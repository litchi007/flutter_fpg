import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_data_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_issue_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_follow_task_model.dart';
import 'package:fpg_flutter/models/expert_plan/expert_plan_game_model.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/drop_down_utils.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class ExpertPlanAutoBetController extends GetxController {
  static ExpertPlanAutoBetController get to =>
      Get.find<ExpertPlanAutoBetController>();

  /// 游戏
  late ExpertPlanGameModel gameModel;

  /// 专家数据
  late ExpertPlanDataModel dataModel;

  /// 预测位置
  late Position position;

  /// tabs
  final tabs = ['10', '50', '100'];

  /// 选中的tab
  final fastBetNumber = '10'.obs;

  /// 防抖
  late Worker followBetWorker;

  /// 加载状态
  final loading = true.obs;

  /// 已自动跟投列表
  final taskList = [].cast<ExpertPlanFollowTaskModel>().obs;

  /// 投注列表
  final betInfoList = [].cast<ExpertPlanFollowIssueModel>().obs;

  /// 防抖
  late Worker debounceWorker;

  /// 期数最大数
  final issueMaxCount = '10'.obs;

  /// 总金额
  final betTotalMoney = '0'.obs;

  /// 跟投金额
  final followBetMoney = ''.obs;

  /// 反投金额
  final reverseBetMoney = ''.obs;

  FocusNode focusNode = FocusNode();

  /// 正投输入框
  late TextEditingController editingControllerOne = TextEditingController()
    ..addListener(changeMoneyListenerOne);

  /// 反投输入框
  late TextEditingController editingControllerTwo = TextEditingController()
    ..addListener(changeMoneyListenerTwo);

  /// 正投输入框控制器数组
  late List<TextEditingController> textControllerListOne;

  /// 反投输入框控制器数组
  late List<TextEditingController> textControllerListTwo;

  /// 期数输入框
  late TextEditingController issueController = TextEditingController()
    ..addListener(changeIssueListener);

  /// 监听跟投输入框改变
  changeMoneyListenerOne() {
    var betMoney = editingControllerOne.text;

    if (betMoney == followBetMoney.value) {
      return;
    }

    followBetMoney.value = betMoney;

    textControllerListOne = textControllerListOne.map((e) {
      e.text = betMoney;
      return e;
    }).toList();

    /// 重置投注类型
    for (var e in betInfoList) {
      e.betMoney = betMoney;
      e.betType = betMoney.isEmpty ? null : '1';
    }

    if (betMoney.isNotEmpty) {
      textControllerListTwo = textControllerListTwo.map((e) {
        e.text = '';
        return e;
      }).toList();

      /// 计算投注金额
      sumBetMoney();
    }

    List<ExpertPlanFollowIssueModel> tempList = [];
    tempList.addAll(betInfoList);
    betInfoList.value = tempList;
  }

  /// 监听反投输入框改变
  changeMoneyListenerTwo() {
    var betMoney = editingControllerTwo.text;

    if (betMoney == reverseBetMoney.value) {
      return;
    }

    reverseBetMoney.value = betMoney;

    textControllerListTwo = textControllerListTwo.map((e) {
      e.text = betMoney;
      return e;
    }).toList();

    for (var e in betInfoList) {
      e.betMoney = betMoney;
      e.betType = betMoney.isEmpty ? null : '2';
    }

    if (betMoney.isNotEmpty) {
      textControllerListOne = textControllerListOne.map((e) {
        e.text = '';
        return e;
      }).toList();

      /// 计算投注金额
      sumBetMoney();
    }

    List<ExpertPlanFollowIssueModel> tempList = [];
    tempList.addAll(betInfoList);
    betInfoList.value = tempList;
  }

  /// 监听期数最大值改变
  changeIssueListener() {
    issueMaxCount.value = issueController.text;
    if (issueController.text.isEmpty) {
      betInfoList.value = [];
    }
  }

  /// 选择快捷跟投期数
  handleSelectedIssue(String value) {
    if (issueInputIsEnabled() && value != fastBetNumber.value) {
      fastBetNumber.value = value;
      issueController.text = value;
      loading.value = true;
    }
  }

  /// 跟投输入框是否启用
  bool followBetIsEnabled() {
    /// 如果不是加载中 且 没有跟投信息了, 反投没有值,  则启用
    return issueInputIsEnabled() && reverseBetMoney.isEmpty;
  }

  /// 反投输入框是否启用
  bool reverseBetIsEnabled() {
    /// 如果不是加载中 且 没有跟投信息了, 跟投没有值,  则启用
    return issueInputIsEnabled() && followBetMoney.isEmpty;
  }

  /// 期数输入框是否启用
  bool issueInputIsEnabled() {
    /// 如果不是加载中 且 没有跟投信息了,  则启用
    return !loading.value && taskList.isEmpty;
  }

  /// 处理总金额交换
  handleHeaderExChange() {
    /// 如果是加载中 或者 已有跟投信息了, 则点不了
    if (!issueInputIsEnabled()) {
      return;
    }

    var pBetMoney = editingControllerOne.text;
    var aBetMoney = editingControllerTwo.text;

    if (pBetMoney.isEmpty && aBetMoney.isEmpty) {
      return;
    }

    if (pBetMoney.isNotEmpty) {
      textControllerListTwo = textControllerListTwo.map((e) {
        e.text = pBetMoney;
        return e;
      }).toList();

      textControllerListOne = textControllerListOne.map((e) {
        e.text = '';
        return e;
      }).toList();

      editingControllerTwo.text = pBetMoney;
      reverseBetMoney.value = pBetMoney;
      editingControllerOne.text = '';
      followBetMoney.value = '';

      for (var e in betInfoList) {
        e.betMoney = pBetMoney;
        e.betType = '2';
      }
    } else {
      textControllerListOne = textControllerListOne.map((e) {
        e.text = aBetMoney;
        return e;
      }).toList();

      textControllerListTwo = textControllerListTwo.map((e) {
        e.text = '';
        return e;
      }).toList();

      editingControllerOne.text = aBetMoney;
      followBetMoney.value = aBetMoney;
      editingControllerTwo.text = '';
      reverseBetMoney.value = '';

      for (var e in betInfoList) {
        e.betMoney = aBetMoney;
        e.betType = '1';
      }
    }

    /// 计算投注金额
    sumBetMoney();

    List<ExpertPlanFollowIssueModel> tempList = [];
    tempList.addAll(betInfoList);
    betInfoList.value = tempList;
  }

  /// 处理加减号期数
  handleIssueChange({bool isAdd = false}) {
    /// 如果是加载中 或者 已有跟投信息了, 则点不了
    if (!issueInputIsEnabled()) {
      return;
    }

    if (taskList.isEmpty) {
      var issue = issueController.text;

      /// 若是加 && 输入框无值, 则默认为0
      if (isAdd && issue.isEmpty) {
        issue = '0';
      } else {
        if (issue.isEmpty) {
          return;
        }
      }

      var issueInt = int.parse(issue);

      if (isAdd) {
        issueInt += 1;
      } else {
        issueInt -= 1;
        if (issueInt < 1) {
          return;
        }
      }
      var issueStr = issueInt.toString();
      issueController.text = issueStr;
      issueMaxCount.value = issueStr;
    }
  }

  /// 处理自动跟投列表输入框事件
  handleAutoBetItemInputEvent(
      int index, ExpertPlanFollowIssueModel issueModel, String value,
      {bool isFollowBet = true}) {
    issueModel.betMoney = value;

    /// 如果输入的金额为空, 则置空投注类型
    if (value.isEmpty) {
      issueModel.betType = null;
    } else {
      issueModel.betType = isFollowBet ? '1' : '2';
    }
    betInfoList[index] = issueModel;

    /// 计算投注金额
    sumBetMoney();
  }

  /// 计算投注金额
  sumBetMoney() {
    double totalMoney = 0.0;
    for (var e in betInfoList) {
      if (e.betMoney != null && e.betMoney!.isNotEmpty) {
        totalMoney += double.tryParse(e.betMoney!) ?? 0.0;
      }
    }
    betTotalMoney.value = totalMoney.toStringAsFixed(2);
  }

  /// 处理自动跟投列表交换事件
  handleAutoBetItemInputExchangeEvent(
      ExpertPlanFollowIssueModel issueModel, int index) {
    var betMoney = issueModel.betMoney ?? '';

    if (betMoney.isEmpty) {
      return;
    }

    if (issueModel.betType == '1') {
      issueModel.betType = '2';
      textControllerListOne[index].text = '';
      textControllerListTwo[index].text = issueModel.betMoney ?? '';
    } else {
      issueModel.betType = '1';
      textControllerListOne[index].text = issueModel.betMoney ?? '';
      textControllerListTwo[index].text = '';
    }

    betInfoList[index] = issueModel;
  }

  /// 获取已自动跟投信息
  getFollowTaskList() async {
    var res = await UserHttp.getAutoFollowFollowInfo(
        gameModel.gameId, dataModel.eid, dataModel.ycLength, position.value);

    taskList.value = res.data?.taskList ?? [];

    /// 存在已自动跟投信息,
    if (taskList.isNotEmpty) {
      var totalMoney = 0;
      for (var task in taskList) {
        totalMoney += int.tryParse(task.money.toString()) ?? 0;
      }
      betTotalMoney.value = totalMoney.toString();
      issueController.text = taskList.length.toString();
      loading.value = false;
    } else {
      /// 不存在 则获取跟投列表
      issueController.text = '10';
      getFollowIssueList(fastBetNumber.value);
    }
  }

  /// 获取跟投列表
  getFollowIssueList(String maxCount) async {
    var res = await UserHttp.getAutoFollowIssueList(gameModel.gameId, maxCount);
    betInfoList.value = res.models ?? [];

    textControllerListOne =
        betInfoList.map((e) => TextEditingController()).toList();
    textControllerListTwo =
        betInfoList.map((e) => TextEditingController()).toList();

    loading.value = false;
  }

  /// 保存跟单
  Future<String> saveFollowBetList() async {
    if (betTotalMoney.isEmpty) {
      return '请输入金额';
    }

    ToastUtils.showLoading();

    List<dynamic> betList = [];

    for (var betInfo in betInfoList) {
      if (betInfo.betMoney != null && betInfo.betMoney!.isNotEmpty) {
        Map<String, dynamic> map = {};
        map['issue'] = betInfo.displayNumber ?? betInfo.issue;
        map['betType'] = betInfo.betType;
        map['money'] = betInfo.betMoney;
        map['startTime'] = betInfo.startTime;
        map['endTime'] = betInfo.endTime;
        betList.add(map);
      }
    }

    final res = await UserHttp.autoFollowSaveFollow(gameModel.gameId,
        dataModel.eid, dataModel.ycLength, position.value, jsonEncode(betList));

    ToastUtils.closeAll();

    if (res.code == 0) {
      GlobalService.to.fetchUserBalance();
      return '';
    }

    return res.msg;
  }

  @override
  void onReady() {
    getFollowTaskList();
    super.onReady();
  }

  @override
  void onInit() {
    followBetWorker = debounce(fastBetNumber, (fastBetNumber) {
      getFollowIssueList(fastBetNumber);
    }, time: Duration(seconds: 1));

    debounceWorker = debounce(issueMaxCount, (maxCount) {
      if (maxCount.isNotEmpty) {
        getFollowIssueList(maxCount);
      }
    }, time: Duration(seconds: 1));
    super.onInit();
  }

  @override
  void onClose() {
    followBetWorker.dispose();
    debounceWorker.dispose();
    super.onClose();
  }
}
