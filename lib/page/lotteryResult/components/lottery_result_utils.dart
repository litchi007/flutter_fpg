import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_type.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:flutter/material.dart';

class LotteryResultUtils {
  static Color? getNumBackColor(String? gameType, String value) {
    if (gameType == GameType.luckyAirship || gameType == GameType.racing) {
      return value.getNumColor();
    } else if (gameType == GameType.timeLottery ||
        gameType == GameType.elevenSelectedFive ||
        gameType == GameType.sortThree ||
        gameType == GameType.gdkl10 ||
        gameType == GameType.sevenStarLottery ||
        gameType == GameType.pcEggs) {
      return '#d72035'.color();
    }
    return Colors.red;
  }

  static BorderRadius getNumBackRadius(String? gameType) {
    if (gameType == GameType.luckyAirship ||
        gameType == GameType.timeLottery ||
        gameType == GameType.elevenSelectedFive ||
        gameType == GameType.sortThree ||
        gameType == GameType.gdkl10 ||
        gameType == GameType.sevenStarLottery ||
        gameType == GameType.pcEggs) {
      return 12.radius;
    } else if (gameType == GameType.racing) {
      return 5.radius;
    }
    return 0.radius;
  }
}
