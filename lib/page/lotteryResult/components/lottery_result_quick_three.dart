import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LotteryResultQuickThree extends StatelessWidget {
  final ListModel.LotteryList model;

  const LotteryResultQuickThree({super.key, required this.model});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /// 开奖号码
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: (model.num ?? '').split(',').map((value) {
            return Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(right: 2),
              width: 22,
              height: 22,
              child: Image(
                  image: AssetImage('assets/images/dice-color/$value.png')),
            );
          }).toList(),
        ),

        /// 开奖文字
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: (model.result ?? '').split(',').map((value) {
            return Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 0.5,
                      color: context.textTheme.bodyMedium?.color ??
                          Colors.transparent),
                  borderRadius: 5.radius),
              margin: const EdgeInsets.only(right: 2),
              constraints: const BoxConstraints(
                minWidth: 20,
              ),
              height: 20,
              child: Text(
                value,
                style: context.textTheme.bodySmall,
              ),
            );
          }).toList(),
        ).paddingOnly(top: 5)
      ],
    );
  }
}
