import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LotteryResultMarkSixLottery extends StatelessWidget {
  final ListModel.LotteryList model;
  final List<String> redBalls;
  final List<String> greenBalls;
  final List<String> blueBalls;

  const LotteryResultMarkSixLottery(
      {super.key,
      required this.model,
      required this.redBalls,
      required this.greenBalls,
      required this.blueBalls});

  @override
  Widget build(BuildContext context) {
    List<String> numberList = (model.num ?? '').split(',');
    numberList.insert(numberList.length - 1, '+');
    List<String> animalList = (model.result ?? '').split(',');
    animalList.insert(animalList.length - 1, '+');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /// 开奖号码
        Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: numberList.map((value) {
            if (value == '+') {
              return Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(right: 2),
                width: 24,
                height: 24,
                child: Text(
                  value,
                  style: context.textTheme.bodySmall,
                ),
              );
            }

            return Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(right: 2),
              width: 24,
              height: 24,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    getBallIcon(value), // 替换为你的图片路径
                    width: 24.0,
                    height: 24.0,
                    fit: BoxFit.cover,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      value.padLeft(2, '0'),
                      style: const TextStyle(
                        fontSize: 12.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }).toList(),
        ),

        /// 开奖文字
        Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: animalList.map((value) {
            if (value == '+') {
              return Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(right: 2),
                width: 24,
                height: 24,
              );
            }
            return Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 0.5,
                      color: context.textTheme.bodyMedium?.color ??
                          Colors.transparent),
                  borderRadius: 5.radius),
              margin: const EdgeInsets.only(right: 2),
              constraints: const BoxConstraints(
                minWidth: 24,
              ),
              height: 24,
              child: Text(
                value,
                style: context.textTheme.bodySmall,
              ),
            );
          }).toList(),
        ).paddingOnly(top: 5)
      ],
    ).paddingOnly(left: 15);
  }

  String getBallIcon(String value) {
    if (redBalls.contains(value)) {
      return 'assets/images/ball/red.png';
    }

    if (greenBalls.contains(value)) {
      return 'assets/images/ball/green.png';
    }

    if (blueBalls.contains(value)) {
      return 'assets/images/ball/blue.png';
    }

    return 'assets/images/ball/blue.png';
  }
}
