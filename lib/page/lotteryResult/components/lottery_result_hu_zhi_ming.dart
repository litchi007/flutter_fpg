import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:flutter/material.dart';

class LotteryResultHuZhiMing extends StatelessWidget {
  final ListModel.LotteryList model;

  const LotteryResultHuZhiMing({super.key, required this.model});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: (model.num ?? '').split(',').map((value) {
        return Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.only(right: 2),
          width: 20,
          height: 20,
          decoration:
              BoxDecoration(color: '#d72035'.color(), borderRadius: 12.radius),
          child: Text(
            '${int.tryParse(value)}',
            style: const TextStyle(color: Colors.white),
          ),
        );
      }).toList(),
    );
  }
}
