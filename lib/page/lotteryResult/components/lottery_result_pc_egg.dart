import 'package:fpg_flutter/page/lotteryResult/components/lottery_result_utils.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LotteryResultPcEgg extends StatelessWidget {
  final ListModel.LotteryList model;

  const LotteryResultPcEgg({super.key, required this.model});

  @override
  Widget build(BuildContext context) {
    List<String> numberList = (model.num ?? "").split(',');
    int middleIndex = numberList.length ~/ 2;
    List<String> firstHalf = numberList.sublist(0, middleIndex);
    List<String> secondHalf = numberList.sublist(middleIndex);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /// 开奖号码
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: firstHalf.map((value) {
            return Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(right: 2),
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                  color:
                      LotteryResultUtils.getNumBackColor(model.gameType, value),
                  borderRadius:
                      LotteryResultUtils.getNumBackRadius(model.gameType)),
              child: Text(
                "${int.tryParse(value)}",
                style: const TextStyle(color: Colors.white),
              ),
            );
          }).toList(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: secondHalf.map((value) {
            return Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(right: 2),
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                  color:
                      LotteryResultUtils.getNumBackColor(model.gameType, value),
                  borderRadius:
                      LotteryResultUtils.getNumBackRadius(model.gameType)),
              child: Text(
                "${int.tryParse(value)}",
                style: const TextStyle(color: Colors.white),
              ),
            );
          }).toList(),
        ).paddingOnly(top: 5),

        /// 开奖文字
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: (model.result ?? "").split(",").map((value) {
            return Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 0.5,
                      color: context.textTheme.bodyMedium?.color ??
                          Colors.transparent),
                  borderRadius: 5.radius),
              margin: const EdgeInsets.only(right: 2),
              constraints: const BoxConstraints(
                minWidth: 20,
              ),
              height: 20,
              child: Text(
                value,
                style: context.textTheme.bodySmall,
              ),
            );
          }).toList(),
        ).paddingOnly(top: 5)
      ],
    );
  }
}
