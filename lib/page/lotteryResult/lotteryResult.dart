import 'dart:ui';

import 'package:dartx/dartx.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_type.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/page/lotteryResult/components/lottery_result_hu_zhi_ming.dart';
import 'package:fpg_flutter/page/lotteryResult/components/lottery_result_lucky_airship.dart';
import 'package:fpg_flutter/page/lotteryResult/components/lottery_result_mark_six_lottery.dart';
import 'package:fpg_flutter/page/lotteryResult/components/lottery_result_pc_egg.dart';
import 'package:fpg_flutter/page/lotteryResult/components/lottery_result_quick_three.dart';
import 'package:fpg_flutter/page/lotteryResult/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/page/webview/controller/controller.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class LotteryResultPage extends GetView<LotteryResultController> {
  LotteryResultPage({super.key});

  final tags = Get.parameters['null'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: '历史记录'),
        body: SafeArea(
            child: Column(
          children: [
            _buildHeaderWidget(context),
            _buildTitleHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context))
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5, right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _buildGameName(),
          const SizedBox(width: 52),
          Obx(() {
            if (controller.gameType.value == GameType.markSixLottery ||
                controller.gameType.value == GameType.huZhiMing ||
                controller.gameType.value == GameType.sevenStarLottery) {
              return SizedBox();
            }
            return _buildTime(context);
          })
        ],
      ),
    );
  }

  /// 游戏选择器
  Widget _buildGameName() {
    return PopupMenuButton<String>(
      color: Colors.white,
      child: Container(
        padding: EdgeInsets.only(left: 5, top: 5, bottom: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: '#a9a9a9'.color()),
          borderRadius: BorderRadius.circular(4), // 设置圆角
        ),
        child: Row(
          children: [
            Obx(() => Text(
                  controller.gameName.value,
                  style: TextStyle(color: '#333333'.color()),
                )),
            Icon(
              Icons.keyboard_arrow_down,
              size: 20,
            ),
          ],
        ),
      ),
      itemBuilder: (c) => controller.gameList.mapIndexed((i, e) {
        return CheckedPopupMenuItem(
          value: e.id,
          checked: controller.gameId.value == e.id,
          child: Text(e.shortName ?? ''),
        );
      }).toList(),
      onSelected: (value) {
        controller.selectedGame(value);
      },
    );
  }

  /// 时间选择器
  Widget _buildTime(BuildContext context) {
    return PopupMenuButton<int>(
      color: Colors.white,
      child: Container(
        padding: EdgeInsets.only(left: 5, top: 5, bottom: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1, color: '#a9a9a9'.color()),
          borderRadius: BorderRadius.circular(4), // 设置圆角
        ),
        child: Row(
          children: [
            Obx(() => Text(
                  '${controller.dateModel.value.dateStr}',
                  style: TextStyle(color: '#333333'.color()),
                )),
            Icon(
              Icons.keyboard_arrow_down,
              size: 20,
            ),
          ],
        ),
      ),
      itemBuilder: (c) => controller.dateList.mapIndexed((i, e) {
        return CheckedPopupMenuItem(
          value: e.dateId,
          checked: controller.dateModel.value.dateId == e.dateId,
          child: Text(e.dateStr ?? ''),
        );
      }).toList(),
      onSelected: (value) {
        controller.selectedDate(value);
      },
    );
  }

  Widget _buildTitleHeaderWidget(BuildContext context) {
    double height = 45;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          alignment: Alignment.center,
          width: 128,
          height: height,
          child: Text('期数',
              style: TextStyle(
                  color: context.customTheme?.fontColor, fontSize: 15)),
        ),
        Container(
          width: 0.5,
          height: height,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: height, // 设置高度作为示例
            child: Text('开奖号码',
                style: TextStyle(
                    color: context.customTheme?.fontColor, fontSize: 15)),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(() => CustomLoading(
          status: controller.loading.value,
          child: controller.historyList.isNotEmpty
              ? SmartRefresher(
                  enablePullDown: true,
                  enablePullUp: true,
                  header: CustomHeader(
                    builder: (BuildContext context, RefreshStatus? mode) {
                      Widget body;
                      if (mode == RefreshStatus.refreshing) {
                        body = Text("刷新中..."); // "Refreshing..."
                      } else if (mode == RefreshStatus.canRefresh) {
                        body = Text("释放刷新"); // "Release to refresh"
                      } else if (mode == RefreshStatus.completed) {
                        body = Text("刷新完成"); // "Refresh complete"
                      } else {
                        body = Text("刷新失败"); // "Refresh failed"
                      }
                      return Container(
                        height: 55.0,
                        child: Center(child: body),
                      );
                    },
                  ),
                  footer: CustomFooter(
                    builder: (BuildContext context, LoadStatus? mode) {
                      Widget body;
                      if (mode == LoadStatus.loading) {
                        body =
                            Text("加载中..."); // Change 'Loading' to Chinese here
                      } else if (mode == LoadStatus.failed) {
                        body = Text("加载失败，请重试");
                      } else if (mode == LoadStatus.canLoading) {
                        body = Text("释放立即加载更多");
                      } else {
                        body = Text("没有更多数据");
                      }
                      return Container(
                        height: 55.0,
                        child: Center(child: body),
                      );
                    },
                  ),
                  controller: controller.refreshController,
                  onRefresh: controller.onRefresh,
                  onLoading: controller.onLoading,
                  child: ListView.builder(
                    itemBuilder: (c, i) => _buildBodyItemWidget(
                        context, controller.historyList[i]),
                    itemCount: controller.historyList.length,
                  ),
                )
              : Text(
                  '暂无数据',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: '#333333'.color(), fontSize: 15),
                ).paddingOnly(top: 10, bottom: 10),
        ));
  }

  Widget _buildBodyItemWidget(
      BuildContext context, ListModel.LotteryList model) {
    double height = getResultItemHeight(model.gameType);
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              alignment: Alignment.center,
              width: 128,
              height: height,
              child: Column(
                children: [
                  const Spacer(),
                  Text('${model.issue}',
                      style: TextStyle(
                          color: context.customTheme?.fontColor, fontSize: 15)),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(controller.handleOpenTime('${model.openTime}'),
                      style: TextStyle(
                          color: context.customTheme?.fontColor, fontSize: 15)),
                  const Spacer(),
                ],
              ),
            ),
            Container(
              width: 0.5,
              height: height,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _getResultItemWithHash(model)),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    );
  }

  double getResultItemHeight(String? gameType) {
    if (gameType == GameType.pcEggs) {
      return 116;
    }
    return 76;
  }

  Widget _getResultItemWithHash(ListModel.LotteryList model) {
    if (model.hash != null &&
        model.hash!.isNotEmpty &&
        model.verifUrl != null &&
        model.verifUrl!.isNotEmpty) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _getResultItem(model),
          Column(
            children: [
              AppImage.network(img_lotteryResult('hx_icon'),
                  width: 22, height: 22),
              // Image.asset(
              //   'assets/images/hx_icon.png',
              //   width: 22,
              //   height: 22,
              // ),
              Text('验证', style: TextStyle(color: Colors.red, fontSize: 12))
            ],
          ).onTap(() {
            launchUrl(Uri.parse(model.verifUrl ?? ""));
          })
        ],
      );
    }
    return _getResultItem(model);
  }

  Widget _getResultItem(ListModel.LotteryList model) {
    /// 幸运飞艇 || 时时彩 || 赛车 || 11选5
    if (model.gameType == GameType.luckyAirship ||
        model.gameType == GameType.timeLottery ||
        model.gameType == GameType.racing ||
        model.gameType == GameType.elevenSelectedFive ||
        model.gameType == GameType.sortThree ||
        model.gameType == GameType.gdkl10 ||
        model.gameType == GameType.sevenStarLottery) {
      return LotteryResultLuckyAirship(model: model);
    }

    /// pc蛋蛋
    if (model.gameType == GameType.pcEggs) {
      return LotteryResultPcEgg(model: model);
    }

    /// 快3
    if (model.gameType == GameType.quickThree) {
      return LotteryResultQuickThree(model: model);
    }

    /// 六合彩
    if (model.gameType == GameType.markSixLottery) {
      return LotteryResultMarkSixLottery(
        model: model,
        redBalls: controller.redBalls,
        greenBalls: controller.greenBalls,
        blueBalls: controller.blueBalls,
      );
    }

    if (model.gameType == GameType.huZhiMing) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          LotteryResultHuZhiMing(model: model),
          Container(
            padding: EdgeInsets.all(5),
            color: Colors.red,
            child: Text(
              '开奖详情',
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ).onTap(() => showDialog(model))
        ],
      );
    }

    if (kReleaseMode) {
      return Container();
    }

    return Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Text(
          '没此类型:${model.gameType},请确认样式',
          style: const TextStyle(color: Colors.red),
        ));
  }

  showDialog(ListModel.LotteryList model) {
    SmartDialog.show(
        clickMaskDismiss: false,
        builder: (context) {
          return Container(
            width: MediaQuery.of(context).size.width - 36,
            height: 460,
            decoration: BoxDecoration(
              color: context.customTheme?.lotteryPopupItemBg,
              borderRadius: const BorderRadius.all(Radius.circular(4)),
            ),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 54,
                  color: Colors.white.withOpacity(0.2),
                  child: Text(
                    '开奖详情',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text(
                              '特等奖',
                              style: TextStyle(color: Colors.grey),
                            ),
                            SizedBox(width: 20),
                            Text('${model.d0}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('头', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('尾巴')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('一等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.d1}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('0', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.t0}')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('二等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.d2}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('1', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.t1}')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('三等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.d3}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('2', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.t2}')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(width: 20),
                            Text('四等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Expanded(
                                child: Text(
                              '${model.d4}',
                              maxLines: 10,
                              textAlign: TextAlign.left,
                            ))
                          ],
                        )),
                    Container(width: 0.5, height: 91, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: [
                            SizedBox(
                                height: 30,
                                child: Row(
                                  children: [
                                    SizedBox(width: 20),
                                    Text('3',
                                        style: TextStyle(color: Colors.grey)),
                                    SizedBox(width: 20),
                                    Text('${model.t3}')
                                  ],
                                )),
                            Container(height: 0.5, color: Colors.grey),
                            SizedBox(
                                height: 30,
                                child: Row(
                                  children: [
                                    SizedBox(width: 20),
                                    Text('4',
                                        style: TextStyle(color: Colors.grey)),
                                    SizedBox(width: 20),
                                    Text('${model.t4}')
                                  ],
                                )),
                            Container(height: 0.5, color: Colors.grey),
                            SizedBox(
                                height: 30,
                                child: Row(
                                  children: [
                                    SizedBox(width: 20),
                                    Text('5',
                                        style: TextStyle(color: Colors.grey)),
                                    SizedBox(width: 20),
                                    Text('${model.t5}')
                                  ],
                                ))
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('五等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.d5}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('6', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.t6}')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('六等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.d6}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('7', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.t7}')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('七等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.d7}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('8', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.t8}')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                Row(
                  children: [
                    Expanded(
                        flex: 2,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('八等奖', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.d8}')
                          ],
                        )),
                    Container(width: 0.5, height: 30, color: Colors.grey),
                    Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            SizedBox(width: 20),
                            Text('9', style: TextStyle(color: Colors.grey)),
                            SizedBox(width: 20),
                            Text('${model.t9}')
                          ],
                        )),
                  ],
                ),
                Container(height: 0.5, color: Colors.grey),
                const SizedBox(height: 10),
                Row(
                  children: [
                    const SizedBox(width: 10),
                    Expanded(
                        child: Container(
                      height: 42,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        // 设置渐变色背景
                        border: Border.all(
                          color: const Color(0xffa9a9a9), // 边框颜色
                          width: 1.0, // 边框宽度
                        ),
                        borderRadius: BorderRadius.circular(4), // 设置圆角
                      ),
                      child: Text(
                        '取消',
                        style: TextStyle(
                            color: context.customTheme?.fontColor,
                            fontSize: 16),
                      ),
                    ).onTap(() => SmartDialog.dismiss())),
                    const SizedBox(width: 5),
                    Expanded(
                      child: Container(
                        height: 42,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          gradient: const LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              Color(0xffe5caae),
                              Color(0xffd3a06f),
                            ],
                          ), // 设置渐变色背景
                          borderRadius: BorderRadius.circular(4), // 设置圆角
                        ),
                        child: const Text(
                          '确定',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ).onTap(() => SmartDialog.dismiss()),
                    ),
                    const SizedBox(width: 10),
                  ],
                ),
                const SizedBox(height: 10),
              ],
            ),
          );
        });
  }
}
