import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/game_lottery_type.dart';
import 'package:fpg_flutter/models/game_lottery_history_model/lottery_list.dart'
    as ListModel;
import 'package:fpg_flutter/models/lotteryTickets/lottery_date_model.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LotteryResultController extends GetxController {
  static LotteryResultController get to => Get.find<LotteryResultController>();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  /// 加载状态
  final loading = true.obs;

  /// 页码
  int page = 1;

  /// 游戏Id
  final gameId = ''.obs;

  /// 游戏名称
  final gameName = ''.obs;

  /// 游戏类型
  final gameType = ''.obs;

  /// 日期
  final dateModel = LotteryDateModel().obs;

  /// 赛事历史记录
  var historyList = [].cast<ListModel.LotteryList>().obs;

  var gameList = [].cast<Lottery>().obs;
  var dateList = [].cast<LotteryDateModel>().obs;
  late List<String> redBalls;
  late List<String> greenBalls;
  late List<String> blueBalls;

  /// 获取注单列表
  Future getTicketHistory(bool isRefresh) async {
    String? date;
    if (gameType.value != GameType.markSixLottery &&
        gameType.value != GameType.huZhiMing &&
        gameType.value != GameType.sevenStarLottery) {
      date = dateModel.value.dateStr;
    }

    if (isRefresh) {
      page = 1;
    } else {
      page++;
    }

    final res = await GameHttp.gameLotteryHistory(GameLotteryHistoryParams(
        id: gameId.value, date: date, rows: '50', page: page));

    /// 保存六合彩的号码位置
    redBalls = res.data?.redBalls ?? [];
    greenBalls = res.data?.greenBalls ?? [];
    blueBalls = res.data?.blueBalls ?? [];

    var resultList = res.data?.list ?? [];

    if (resultList.length < 50) {
      refreshController.loadNoData();
    } else {
      refreshController.loadComplete();
    }

    if (isRefresh) {
      historyList.value = resultList;
      refreshController.refreshCompleted();
      loading.value = false;
    } else {
      historyList.value = [...historyList, ...resultList];
    }
  }

  /// 获取列表
  Future getGameLotteryGroupGames() async {
    final res = await GameHttp.gameLotteryGroupGames();

    if (res.models?.isNotEmpty ?? false) {
      List<Lottery> lotteries = [];
      res.models?.forEach((m) {
        lotteries.addAll(m.lotteries?.where((l) => l.enable == '1') ?? []);
      });
      gameList.value = lotteries;

      if (gameId.value.isEmpty) {
        gameId.value = gameList.first.id ?? '';
        gameName.value = gameList.first.shortName ?? '';
      } else {
        for (var model in gameList) {
          if (gameId.value == model.id) {
            gameName.value = model.shortName ?? '';
            gameType.value = model.gameType ?? '';
          }
        }
      }
      getTicketHistory(true);
    }
  }

  Future onRefresh() async {
    refreshController.resetNoData();
    getTicketHistory(true);
  }

  Future onLoading() async {
    getTicketHistory(false);
  }

  selectedGame(String id) {
    loading.value = true;

    gameId.value = id;
    for (var lottery in gameList) {
      if (lottery.id == id) {
        gameType.value = lottery.gameType ?? '';
        gameName.value = lottery.shortName ?? '';
        break;
      }
    }

    /// 如果是六合彩重置日期
    if (gameType.value == GameType.markSixLottery ||
        gameType.value == GameType.huZhiMing ||
        gameType.value == GameType.sevenStarLottery) {
      dateModel.value = dateList.first;
    }

    getTicketHistory(true);
  }

  selectedDate(int index) {
    loading.value = true;
    dateModel.value = dateList[index];
    getTicketHistory(true);
  }

  configDateData() {
    DateTime currentDate = DateTime.now();
    // 初始化日期格式化工具
    DateFormat dateFormatter = DateFormat('yyyy-MM-dd');
    // 创建一个空的日期列表
    List<LotteryDateModel> list = [];

    // 向前推算7天，并将日期格式化为字符串添加到列表中
    for (int i = 0; i < 7; i++) {
      DateTime previousDate = currentDate.subtract(Duration(days: i));
      String dateStr = dateFormatter.format(previousDate);
      LotteryDateModel model = LotteryDateModel(dateId: i, dateStr: dateStr);
      list.add(model);
      if (i == 0) {
        dateModel.value = model;
      }
    }
    dateList.value = list;
  }

  String handleOpenTime(String openTime) {
    // 将原始日期字符串解析为DateTime对象
    DateTime originalDateTime = DateTime.parse(openTime);
    // 创建日期格式化对象，指定输出格式为 "MM-dd HH:mm:ss"
    DateFormat formatter = DateFormat('MM-dd HH:mm');
    // 格式化日期时间并输出结果
    return formatter.format(originalDateTime);
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null && arguments is String) {
      gameId.value = arguments;
    }

    configDateData();
    super.onInit();
  }

  @override
  void onReady() {
    getGameLotteryGroupGames();
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
