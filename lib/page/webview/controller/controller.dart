import 'package:flutter/foundation.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:get/get.dart';

class AppWebviewParams {
  String? url;
  String? title;
  AppWebviewParams({this.url, this.title});

  AppWebviewParams.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['url'] = url;
    data['title'] = title;
    return data;
  }
}

class WebviewController extends GetxController {
  static WebviewController get to => Get.find<WebviewController>();

  final params = AppWebviewParams.fromJson(Get.parameters);
  InAppWebViewController? webViewController;
  // InAppWebViewSettings settings = InAppWebViewSettings(
  //   isInspectable: kDebugMode,
  //   mediaPlaybackRequiresUserGesture: false,
  //   allowsInlineMediaPlayback: true,
  //   iframeAllow: "camera; microphone",
  //   iframeAllowFullscreen: true,
  //   allowsLinkPreview: true,
  //   allowsPictureInPictureMediaPlayback: true,
  //   cacheMode: CacheMode.LOAD_DEFAULT,
  // );
  // webview 路径
  String get webUri => (params.url ?? "").decode<String>();

  final title = "".obs;
  final progress = (0.0).obs;
  final canBack = true.obs;

  @override
  void onClose() {
    // webViewController?.dispose();
    super.onClose();
  }

  @override
  void onReady() {
    title.value = params.title ?? "";
    super.onReady();
  }
}
