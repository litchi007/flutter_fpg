import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.webview.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/page/webview/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WebviewPage extends GetView<WebviewController> {
  const WebviewPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => WillPopScope(
        onWillPop: () async {
          // return controller.canBack.value;
          return false;
        },
        child: Scaffold(
          appBar: CustomAppBar(
            header: controller.title.value,
            leading: Container(
                constraints: const BoxConstraints(
                  minWidth: 50,
                ),
                child: const Icon(Icons.close_outlined).onTap(() {
                  Get.back();
                })),
          ),
          body: Column(
            children: [
              Container(
                width: double.infinity,
                // color: context.customTheme?.card,
                alignment: Alignment.centerLeft,
                height: 5,
                child: UnconstrainedBox(
                  child: AnimatedSize(
                    duration: 50.milliseconds,
                    child: Container(
                        decoration: BoxDecoration(
                            color: context.customTheme?.leftActive,
                            borderRadius: 3.radius),
                        height: 5,
                        width: context.mediaQuerySize.width *
                            0.01 *
                            controller.progress.value),
                  ),
                ),
              ),

              // LinearProgressIndicator(
              //   value: controller.progress.value,
              //   color: context.customTheme?.active,
              // ),
              Expanded(
                child: CustomWebview(
                  webUri: controller.webUri,
                  onProgress: (p0) {
                    controller.progress.value = p0;
                  },
                ),
                // InAppWebView(
                //     keepAlive: InAppWebViewKeepAlive(),

                //     initialUrlRequest:
                //         URLRequest(url: WebUri(controller.webUri)),
                //     onProgressChanged: (webviewController, progress) async {
                //       final canBack = await webviewController.canGoBack();
                //       controller.canBack.value = canBack;
                //       controller.progress.value =
                //           double.parse(progress.toStringAsFixed(2));
                //     },
                //     onWebViewCreated:
                //         (InAppWebViewController webviewController) {

                //       // webviewController.addJavaScriptHandler(
                //       //     handlerName: WebviewMethods.close,
                //       //     callback: (arguments) {});
                //       // webviewController?.evaluateJavascript(source: '');

                //       /// [example] 示例
                //       /// window.flutter_inappwebview.callHandler("paySuccess", {
                //       ///  id: "xxx"
                //       /// })
                //     },
                //     onLoadStop: (webviewController, url) async {
                //       final ctl = Get.find<WebviewController?>();
                //       if (ctl != null) {
                //         final res = await webviewController.evaluateJavascript(
                //           source: "document.title");
                //           controller.title.value = res;
                //       }

                // }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
