import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_model.dart';
import 'package:fpg_flutter/page/todaySettled/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TodaySettledPage extends GetView<TodaySettledController> {
  const TodaySettledPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: "今日已结"),
        body: SafeArea(
            child: Column(
          children: [
            _buildHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context)),
            _buildBottomWidget(context)
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          width: 100,
          child: Container(
            alignment: Alignment.center,
            height: 35,
            child: Text.rich(TextSpan(
                text: "期号/",
                style: context.textTheme.bodySmall,
                children: const [
                  TextSpan(
                      text: "注单号",
                      style: TextStyle(color: Colors.red, fontSize: 12))
                ])),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text(
              "下注明细",
              style: context.textTheme.bodySmall,
            ),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("下注金额", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("开奖结果", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("输赢", style: context.textTheme.bodySmall),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(() => CustomLoading(
          status: controller.loading.value,
          child: SmartRefresher(
            enablePullDown: false,
            enablePullUp: true,
            header: CustomHeader(
              builder: (BuildContext context, RefreshStatus? mode) {
                Widget body;
                if (mode == RefreshStatus.refreshing) {
                  body = Text("刷新中..."); // "Refreshing..."
                } else if (mode == RefreshStatus.canRefresh) {
                  body = Text("释放刷新"); // "Release to refresh"
                } else if (mode == RefreshStatus.completed) {
                  body = Text("刷新完成"); // "Refresh complete"
                } else {
                  body = Text("刷新失败"); // "Refresh failed"
                }
                return Container(
                  height: 55.0,
                  child: Center(child: body),
                );
              },
            ),
            footer: CustomFooter(
              builder: (BuildContext context, LoadStatus? mode) {
                Widget body;
                if (mode == LoadStatus.loading) {
                  body = Text("加载中..."); // Change 'Loading' to Chinese here
                } else if (mode == LoadStatus.failed) {
                  body = Text("加载失败，请重试");
                } else if (mode == LoadStatus.canLoading) {
                  body = Text("释放立即加载更多");
                } else {
                  body = Text("没有更多数据");
                }
                return Container(
                  height: 55.0,
                  child: Center(child: body),
                );
              },
            ),
            controller: controller.refreshController,
            onRefresh: controller.onRefresh,
            onLoading: controller.onLoading,
            child: ListView.builder(
              itemBuilder: (c, i) =>
                  _buildBodyItemWidget(context, controller.list[i]),
              itemCount: controller.list.length,
            ),
          ),
        ));
  }

  Widget _buildBodyItemWidget(BuildContext context, LotteryHistoryModel model) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 5),
              width: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(model.gameName ?? '',
                      style: context.textTheme.bodySmall),
                  const SizedBox(height: 10),
                  Text(model.displayNumber ?? '',
                      style: context.textTheme.bodySmall),
                  const SizedBox(height: 10),
                  Text(model.id ?? '',
                      style: const TextStyle(fontSize: 12, color: Colors.red))
                ],
              ),
            ),
            Container(
              width: 0.5,
              height: 85,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    alignment: Alignment.center,
                    height: 85, // 设置高度作为示例
                    child: Text(
                      '${model.playGroupName} ${model.playName} @ ${double.parse(model.odds ?? "0.0").toStringAsFixed(2)}',
                      maxLines: 3,
                      textAlign: TextAlign.center,
                      style: context.textTheme.bodySmall,
                    ))),
            Container(
              width: 0.5,
              height: 85,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 85, // 设置高度作为示例
                child: Text(
                    double.parse(model.betAmount ?? "0.00").toStringAsFixed(2),
                    style: context.textTheme.bodySmall),
              ),
            ),
            Container(
              width: 0.5,
              height: 85, // 设置高度作为示例
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    alignment: Alignment.center,
                    child: Text(
                      model.lotteryNo ?? '',
                      maxLines: 3,
                      style: context.textTheme.bodySmall,
                    ))),
            Container(
              width: 0.5,
              height: 85, // 设置高度作为示例
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      double.parse(model.settleAmount ?? '0.00')
                          .toStringAsFixed(2),
                      maxLines: 20,
                      style: TextStyle(
                          color: double.parse(model.settleAmount ?? '0.00') < 0
                              ? const Color(0xff349044)
                              : const Color(0xffff1017),
                          fontSize: 12),
                    ))),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    );
  }

  Widget _buildBottomWidget(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 34,
              padding: const EdgeInsets.only(left: 20),
              alignment: Alignment.centerRight,
              child: Obx(() => Text(
                  "下注金额:  ${controller.totalBetAmount.toStringAsFixed(2)}",
                  style: TextStyle(
                      color: context.customTheme?.fontColor, fontSize: 13))),
            ),
            Container(
                height: 34,
                padding: const EdgeInsets.only(right: 20),
                alignment: Alignment.centerRight,
                child: Obx(() => Text.rich(TextSpan(
                        text: "输赢金额:  ",
                        style: TextStyle(
                            color: context.customTheme?.fontColor,
                            fontSize: 13),
                        children: [
                          TextSpan(
                              text:
                                  controller.totalWinAmount.toStringAsFixed(2),
                              style: TextStyle(
                                  color: controller.totalWinAmount.value < 0
                                      ? const Color(0xff349044)
                                      : const Color(0xffff1017),
                                  fontSize: 13))
                        ]))))
          ],
        )
      ],
    );
  }
}
