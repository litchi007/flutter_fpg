import '../controller/controller.dart';
import 'package:get/get.dart';

class TodaySettledBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<TodaySettledController>(() => TodaySettledController());
  }
}
