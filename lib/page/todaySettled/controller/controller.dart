import 'package:fpg_flutter/api/ticket/ticket.http.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_model.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:intl/intl.dart';

class TodaySettledController extends GetxController {
  static TodaySettledController get to => Get.find<TodaySettledController>();
  RefreshController refreshController = RefreshController();

  /// 页码
  int page = 1;

  /// 下注金额
  Rx<double> totalBetAmount = 0.00.obs;

  /// 输赢金额
  Rx<double> totalWinAmount = 0.00.obs;

  /// 注单列表
  late var list = [].cast<LotteryHistoryModel>().obs;

  final loading = true.obs;

  /// 获取注单列表
  Future getTickeHistory(bool isRefresh) async {
    if (isRefresh) {
      page = 1;
    } else {
      page++;
    }

    DateTime currentDate = DateTime.now();
    DateFormat dateFormatter = DateFormat('yyyy-MM-dd');
    String dateStr = dateFormatter.format(currentDate);

    final res = await TicketHttp.ticketHistory(
      page: page,
      category: 'lottery',
      startDate: dateStr,
      endDate: dateStr,
      status: 5,
    );

    var resultList = res.data!.list ?? [];
    List<LotteryHistoryModel> tempList = [];
    if (page == 1) {
      totalBetAmount.value = double.parse(res.data!.totalBetAmount ?? "0.00");
      totalWinAmount.value = double.parse(res.data!.totalWinAmount ?? "0.00");
      loading.value = false;
    } else {
      totalBetAmount.value += double.parse(res.data!.totalBetAmount ?? "0.00");
      totalWinAmount.value += double.parse(res.data!.totalWinAmount ?? "0.00");
      tempList.addAll(list);
    }

    if (resultList.length < 30) {
      refreshController.loadNoData();
    } else {
      refreshController.loadComplete();
    }

    tempList.addAll(resultList);
    list.value = tempList;
  }

  Future onRefresh() async {
    getTickeHistory(true);
  }

  Future onLoading() async {
    getTickeHistory(false);
  }

  @override
  void onReady() {
    getTickeHistory(true);
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
