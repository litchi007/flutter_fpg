import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryRoadDataBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<LotteryRoadDataController>(() => LotteryRoadDataController());
  }
}
