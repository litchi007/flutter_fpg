import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/models/lotteryRoadData/lottery_road_data_model.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:get/get.dart';

class LotteryRoadDataController extends GetxController {
  static LotteryRoadDataController get to =>
      Get.find<LotteryRoadDataController>();

  /// 游戏Id
  final gameId = ''.obs;

  /// 游戏名称
  late var gameName = "";

  /// 游戏类型
  final gameType = "".obs;

  final loading = true.obs;

  /// 游戏列表
  var gameList = [].cast<Lottery>().obs;

  /// 路珠数据
  late LotteryRoadDataModel lotteryRoad = const LotteryRoadDataModel();

  final resultOneList = [].cast<List<String>>().obs;
  final resultTwoList = [].cast<List<String>>().obs;

  /// tab
  final tabs = ["总和大小", "总和单双", "龙虎"];

  /// 选择的tab
  final selectedTab = 0.obs;

  /// tab
  final ballTabs = ["第一球", "第二球", "第三球", "第四球", "第五球"];

  /// 选择的tab
  final selectedBallTab = 0.obs;

  final playTabs = ["单双", "大小", "单双"];

  /// 选择的tab
  final selectedPlayTab = 0.obs;

  handleSelectedTab(int value) {
    if (value == 0) {
      resultOneList.value = lotteryRoad.totalSizeList ?? [];
    } else if (value == 1) {
      resultOneList.value = lotteryRoad.totalSingleDoubleList ?? [];
    } else {
      resultOneList.value = lotteryRoad.dragonTigerList ?? [];
    }

    selectedTab.value = value;
  }

  handleSelectedBallTab(int value) {
    selectedBallTab.value = value;
    if (value == 0) {
      if (selectedPlayTab.value == 0) {
        resultTwoList.value = lotteryRoad.numberList1 ?? [];
      } else if (selectedPlayTab.value == 1) {
        resultTwoList.value = lotteryRoad.sizeList1 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.singleDoubleList1 ?? [];
      }
    } else if (value == 1) {
      if (selectedPlayTab.value == 0) {
        resultTwoList.value = lotteryRoad.numberList2 ?? [];
      } else if (selectedPlayTab.value == 1) {
        resultTwoList.value = lotteryRoad.sizeList2 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.singleDoubleList2 ?? [];
      }
    } else if (value == 2) {
      if (selectedPlayTab.value == 0) {
        resultTwoList.value = lotteryRoad.numberList3 ?? [];
      } else if (selectedPlayTab.value == 1) {
        resultTwoList.value = lotteryRoad.sizeList3 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.singleDoubleList3 ?? [];
      }
    } else if (value == 3) {
      if (selectedPlayTab.value == 0) {
        resultTwoList.value = lotteryRoad.numberList4 ?? [];
      } else if (selectedPlayTab.value == 1) {
        resultTwoList.value = lotteryRoad.sizeList4 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.singleDoubleList4 ?? [];
      }
    } else {
      if (selectedPlayTab.value == 0) {
        resultTwoList.value = lotteryRoad.numberList5 ?? [];
      } else if (selectedPlayTab.value == 1) {
        resultTwoList.value = lotteryRoad.sizeList5 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.singleDoubleList5 ?? [];
      }
    }
  }

  handleSelectedPlayTab(int value) {
    selectedPlayTab.value = value;
    if (value == 0) {
      if (selectedBallTab.value == 0) {
        resultTwoList.value = lotteryRoad.numberList1 ?? [];
      } else if (selectedBallTab.value == 1) {
        resultTwoList.value = lotteryRoad.numberList2 ?? [];
      } else if (selectedBallTab.value == 2) {
        resultTwoList.value = lotteryRoad.numberList3 ?? [];
      } else if (selectedBallTab.value == 3) {
        resultTwoList.value = lotteryRoad.numberList4 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.numberList5 ?? [];
      }
    } else if (value == 1) {
      if (selectedBallTab.value == 0) {
        resultTwoList.value = lotteryRoad.sizeList1 ?? [];
      } else if (selectedBallTab.value == 1) {
        resultTwoList.value = lotteryRoad.sizeList2 ?? [];
      } else if (selectedBallTab.value == 2) {
        resultTwoList.value = lotteryRoad.sizeList3 ?? [];
      } else if (selectedBallTab.value == 3) {
        resultTwoList.value = lotteryRoad.sizeList4 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.sizeList5 ?? [];
      }
    } else {
      if (selectedBallTab.value == 0) {
        resultTwoList.value = lotteryRoad.singleDoubleList1 ?? [];
      } else if (selectedBallTab.value == 1) {
        resultTwoList.value = lotteryRoad.singleDoubleList2 ?? [];
      } else if (selectedBallTab.value == 2) {
        resultTwoList.value = lotteryRoad.singleDoubleList3 ?? [];
      } else if (selectedBallTab.value == 3) {
        resultTwoList.value = lotteryRoad.singleDoubleList4 ?? [];
      } else {
        resultTwoList.value = lotteryRoad.singleDoubleList5 ?? [];
      }
    }
  }

  /// 获取注单列表
  Future getRoadData() async {
    /// 刷新重置选中tab
    selectedTab.value = 0;
    selectedBallTab.value = 0;
    selectedPlayTab.value = 0;
    resultOneList.value = [];
    resultTwoList.value = [];
    lotteryRoad = const LotteryRoadDataModel();

    final res = await GameHttp.gameRoad(gameId.value);

    loading.value = false;

    if (res.data != null) {
      lotteryRoad = res.data!;
      resultOneList.value = lotteryRoad.totalSizeList ?? [];
      resultTwoList.value = lotteryRoad.numberList1 ?? [];
    }
  }

  /// 获取列表
  Future getGameLotteryGroupGames() async {
    final res = await GameHttp.gameLotteryGroupGames();

    if (res.models?.isNotEmpty ?? false) {
      List<Lottery> lotteries = [];
      res.models?.forEach((m) {
        lotteries.addAll(m.lotteries?.where((l) => l.enable == '1') ?? []);
      });
      gameList.value = lotteries;
    }
  }

  Future onRefresh() async {
    loading.value = true;
    getRoadData();
  }

  selectedGame(String id) {
    gameId.value = id;
    for (var lottery in gameList) {
      if (lottery.id == id) {
        gameType.value = lottery.gameType!;
        break;
      }
    }

    loading.value = true;
    getRoadData();
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null && arguments is Lottery) {
      gameName = arguments.shortName ?? '';
      gameId.value = arguments.id ?? '';
      gameType.value = arguments.gameType ?? '';
    }
    super.onInit();
  }

  @override
  void onReady() {
    // getGameLotteryGroupGames();
    // getRoadData();
    super.onReady();
  }
}
