import 'package:dartx/dartx.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/components/custom.webview.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/page/lotteryRoadData/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LotteryRoadDataPage extends GetView<LotteryRoadDataController> {
  LotteryRoadDataPage({super.key});

  final tags = Get.parameters["null"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: '路珠'),
        body: SafeArea(
            child: CustomWebview(
          webUri:
              '${AppDefine.host}/mobile/#/lottery/luZhu/${controller.gameId.value}',
        )));

    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: '路珠'),
        body: SafeArea(
            child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _buildHeaderWidget(context),
                Container(
                  width: 44,
                  height: 30,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: const Color(0xffa9a9a9), // 边框颜色
                      width: 1.0, // 边框宽度
                    ),
                    borderRadius: BorderRadius.circular(4), // 设置圆角
                  ),
                  child: Text('刷新'),
                ).paddingOnly(right: 20).onTap(() => controller.onRefresh())
              ],
            ),
            Expanded(child: _buildBodyWidget(context))
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.all(10),
      height: 54,
      child: Container(
          height: 34,
          padding: const EdgeInsets.only(left: 5),
          decoration: BoxDecoration(
            border: Border.all(
              color: const Color(0xffa9a9a9), // 边框颜色
              width: 1.0, // 边框宽度
            ),
            borderRadius: BorderRadius.circular(4), // 设置圆角
          ),
          child: _buildGameName(context)),
    );
  }

  /// 游戏选择器
  Widget _buildGameName(BuildContext context) {
    return Obx(() => DropdownButton(
          items: controller.gameList
              .map<DropdownMenuItem<String>>((Lottery lottery) {
            return DropdownMenuItem<String>(
              value: lottery.id,
              child:
                  Text(lottery.shortName ?? '', style: TextStyle(fontSize: 12)),
            );
          }).toList(),
          hint: Text(controller.gameName),
          // 当没有初始值时显示
          onChanged: (selectValue) {
            controller.selectedGame(selectValue!);
          },
          value: controller.gameId.value,
          // 设置初始值，要与列表中的value是相同的
          style: TextStyle(
              //设置文本框里面文字的样式
              color: context.customTheme?.fontColor,
              fontSize: 12),
          iconSize: 20,
          //设置三角标icon的大小
          underline: Container(),
        ));
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(() => CustomLoading(
        status: controller.loading.value,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _getListWidget(context),
          ),
        )));
  }

  List<Widget> _getListWidget(BuildContext context) {
    List<Widget> children = [];
    if (controller.lotteryRoad.dragonTigerList != null ||
        controller.lotteryRoad.totalSizeList != null ||
        controller.lotteryRoad.totalSingleDoubleList != null) {
      children.add(_getResultOneHeaderWidget(context));
      children.add(Container(
        color: "#d6d6da".color(),
        height: 1,
      ));
      children.add(_getResultOneWidget(context));
      children.add(SizedBox(height: 20));
    }

    if (controller.lotteryRoad.numberList1 != null ||
        controller.lotteryRoad.sizeList1 != null ||
        controller.lotteryRoad.singleDoubleList1 != null ||
        controller.lotteryRoad.numberList2 != null ||
        controller.lotteryRoad.sizeList2 != null ||
        controller.lotteryRoad.singleDoubleList2 != null ||
        controller.lotteryRoad.numberList3 != null ||
        controller.lotteryRoad.sizeList3 != null ||
        controller.lotteryRoad.singleDoubleList3 != null ||
        controller.lotteryRoad.numberList4 != null ||
        controller.lotteryRoad.sizeList4 != null ||
        controller.lotteryRoad.singleDoubleList4 != null ||
        controller.lotteryRoad.numberList5 != null ||
        controller.lotteryRoad.sizeList5 != null ||
        controller.lotteryRoad.singleDoubleList5 != null) {
      children.add(_getResultTwoHeaderWidget(context));
      children.add(Container(
        color: "#d6d6da".color(),
        height: 1,
      ));
      children.add(_getResultThreeHeaderWidget(context));
      children.add(_getResultTwoWidget(context));
    }

    if (children.isEmpty) {
      children.add(SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height -
            MediaQuery.of(context).padding.top -
            MediaQuery.of(context).padding.bottom -
            54,
        child: Align(
          child: Text('暂无数据'),
        ),
      ));
    }

    return children;
  }

  Widget _getResultOneHeaderWidget(BuildContext context) {
    return Row(
      children: controller.tabs
          .mapIndexed((i, e) => Expanded(
                child: Stack(
                  children: [
                    Container(
                        height: 44,
                        color: Colors.grey,
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.white,
                                blurRadius:
                                    controller.selectedTab.value == i ? 5 : 0,
                                spreadRadius: 10,
                              ),
                            ],
                          ),
                          child: Text(e),
                        )).onTap(() => controller.handleSelectedTab(i)),
                    Positioned(
                        child: Container(
                      color: "#d6d6da".color(),
                      width: 1,
                      height: 44,
                    ))
                  ],
                ),
              ))
          .toList(),
    );
  }

  Widget _getResultOneWidget(BuildContext context) {
    return Obx(() => SizedBox(
        width: context.mediaQuerySize.width,
        height: controller.resultOneList.isNotEmpty
            ? controller.resultOneList
                    .reduce((a, b) => a.length > b.length ? a : b)
                    .length *
                20
            : 40,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: controller.resultOneList.length,
          itemBuilder: (context, index) {
            return Column(
                children: controller.resultOneList[index]
                    .map((e) => Container(
                          height: 20,
                          padding: EdgeInsets.only(left: 10),
                          child: Text(e),
                        ))
                    .toList());
          },
        )));
  }

  Widget _getResultTwoHeaderWidget(BuildContext context) {
    return Row(
      children: controller.ballTabs
          .mapIndexed((i, e) => Expanded(
                child: Stack(
                  children: [
                    Container(
                        height: 44,
                        color: Colors.grey,
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.white,
                                blurRadius:
                                    controller.selectedBallTab.value == i
                                        ? 5
                                        : 0,
                                spreadRadius: 10,
                              ),
                            ],
                          ),
                          child: Text(e),
                        )).onTap(() => controller.handleSelectedBallTab(i)),
                    Positioned(
                        child: Container(
                      color: "#d6d6da".color(),
                      width: 1,
                      height: 44,
                    ))
                  ],
                ),
              ))
          .toList(),
    );
  }

  Widget _getResultThreeHeaderWidget(BuildContext context) {
    return Row(
      children: controller.playTabs
          .mapIndexed((i, e) => Expanded(
                child: SizedBox(
                  height: 44,
                  child: Row(
                    mainAxisAlignment: _getMainAxisAlignment(i),
                    children: [
                      controller.selectedPlayTab.value == i
                          ? const Icon(Icons.check_circle, color: Colors.red)
                          : const Icon(Icons.circle_outlined,
                              color: Colors.grey),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(e)
                    ],
                  ),
                ).onTap(() => controller.handleSelectedPlayTab(i)),
              ))
          .toList(),
    );
  }

  MainAxisAlignment _getMainAxisAlignment(int i) {
    if (i == 0) {
      return MainAxisAlignment.end;
    } else if (i == 2) {
      return MainAxisAlignment.start;
    } else {
      return MainAxisAlignment.center;
    }
  }

  Widget _getResultTwoWidget(BuildContext context) {
    return Obx(() => SizedBox(
        width: context.mediaQuerySize.width,
        height: controller.resultTwoList.isNotEmpty
            ? controller.resultTwoList
                    .reduce((a, b) => a.length > b.length ? a : b)
                    .length *
                20
            : 40,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: controller.resultTwoList.length,
          itemBuilder: (context, index) {
            return Column(
                children: controller.resultTwoList[index]
                    .map((e) => Container(
                          height: 20,
                          padding: EdgeInsets.only(left: 10),
                          child: Text(e),
                        ))
                    .toList());
          },
        )));
  }
}
