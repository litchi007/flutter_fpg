import 'package:fpg_flutter/api/game/game.http.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery_group_games_model.dart';
import 'package:get/get.dart';

class LotteryGameListController extends GetxController {
  static LotteryGameListController get to =>
      Get.find<LotteryGameListController>();

  final loading = true.obs;

  /// 彩票游戏列表
  var gameList = [].cast<Lottery>().obs;

  /// 选中的彩票游戏
  final selectedLottery = const Lottery().obs;

  /// 获取列表
  Future getGameLotteryGroupGames() async {
    final res = await GameHttp.gameLotteryGroupGames();
    loading.value = false;
    if (res.models?.isNotEmpty ?? false) {
      List<Lottery> lotteries = [];
      res.models?.forEach((m) {
        lotteries.addAll(m.lotteries?.where((l) => l.enable == '1') ?? []);
      });
      gameList.value = getGameList(lotteries);
    }
    loading.value = false;
  }

  List<Lottery> getGameList(List<Lottery>? data) {
    // 初始化一个空列表
    List<Lottery> games = [];

    // 检查 data 是否不为空，并将其赋值给 games
    if (data != null) {
      games = data;
    }

    // 初始化一个空列表用于存储过滤后的游戏
    List<Lottery> trendGameList = [];

    // 遍历 games 列表中的每个游戏
    for (var element in games) {
      // 检查游戏类型是否匹配指定的类型
      if ([
        'pcdd',
        'cqssc',
        'qxc',
        'xyft',
        'pk10nn',
        'pk10',
        'gd11x5',
        'jsk3',
        'gdkl10',
        'xync',
        'lhc'
      ].contains(element.gameType)) {
        // 检查名称中是否不包含 'mmc'
        if (!element.name!.contains('mmc')) {
          // 将符合条件的元素添加到 trendGameList 中
          trendGameList.add(element);
        }
      }
    }

    // 返回过滤后的列表
    return trendGameList;
  }

  @override
  void onInit() {
    getGameLotteryGroupGames();
    super.onInit();
  }
}
