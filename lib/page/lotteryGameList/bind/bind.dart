import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryGameListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LotteryGameListController>(() => LotteryGameListController());
  }
}
