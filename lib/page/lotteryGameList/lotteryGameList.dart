import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:fpg_flutter/components/custom.loading.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/page/lotteryGameList/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LotteryGameListPage extends GetView<LotteryGameListController> {
  const LotteryGameListPage({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(LotteryGameListController());
    return Scaffold(
        backgroundColor: Colors.transparent, // 设置背景透明
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                color: Colors.black.withOpacity(0.1),
              ),
            ),
            Positioned(
                top: MediaQuery.of(context).padding.top,
                left: 18,
                child: Container(
                  width: MediaQuery.of(context).size.width - 36,
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom -
                      40,
                  decoration: BoxDecoration(
                    color: context.customTheme?.lotteryPopupItemBg,
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                  ),
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      Text(
                        '选择彩种',
                        style: TextStyle(
                            color: context.customTheme?.fontColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(height: 10),
                      Container(
                        height: 1,
                        color: context.customTheme?.lotteryPopupDivColor,
                      ),
                      Obx(() => Expanded(
                          child: CustomLoading(
                              status: controller.loading.value,
                              child: SingleChildScrollView(
                                  child: GridView.builder(
                                shrinkWrap: true,
                                itemCount: controller.gameList.length,
                                physics: NeverScrollableScrollPhysics(),
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, top: 10),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  mainAxisSpacing: 10,
                                  crossAxisSpacing: 8,
                                  childAspectRatio: 3,
                                ),
                                itemBuilder: (context, index) {
                                  Lottery lottery = controller.gameList[index];
                                  return Obx(() => Container(
                                        decoration: BoxDecoration(
                                          // 设置渐变色背景
                                          gradient: LinearGradient(
                                            begin: Alignment.centerLeft,
                                            end: Alignment.centerRight,
                                            colors: controller.selectedLottery
                                                        .value ==
                                                    lottery
                                                ? [
                                                    Color(0xffe5caae),
                                                    Color(0xffd3a06f),
                                                  ]
                                                : [
                                                    Colors.white,
                                                    Colors.white,
                                                  ],
                                          ),
                                          border: Border.all(
                                            color: '#C9C9C9'.color(),
                                            // 边框颜色
                                            width: 1.0, // 边框宽度
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(4), // 设置圆角
                                        ),
                                        alignment: Alignment.center,
                                        child: Text(
                                          '${lottery.shortName}',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: controller.selectedLottery
                                                          .value ==
                                                      lottery
                                                  ? Colors.white
                                                  : Colors.black,
                                              fontSize: 12),
                                        ),
                                      ).onTap(() {
                                        controller.selectedLottery.value =
                                            lottery;
                                      }));
                                },
                              ))))),
                      const SizedBox(height: 10),
                      Row(
                        children: [
                          const SizedBox(width: 10),
                          Expanded(
                              child: Container(
                            height: 54,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              // 设置渐变色背景
                              border: Border.all(
                                color: const Color(0xffa9a9a9), // 边框颜色
                                width: 1.0, // 边框宽度
                              ),
                              borderRadius: BorderRadius.circular(4), // 设置圆角
                            ),
                            child: Text(
                              '取消',
                              style: TextStyle(
                                  color: context.customTheme?.fontColor,
                                  fontSize: 16),
                            ),
                          ).onTap(() => Get.back())),
                          const SizedBox(width: 15),
                          Expanded(
                            child: Container(
                              height: 54,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    Color(0xffe5caae),
                                    Color(0xffd3a06f),
                                  ],
                                ), // 设置渐变色背景
                                borderRadius: BorderRadius.circular(4), // 设置圆角
                              ),
                              child: const Text(
                                '确定',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ).onTap(() {
                              Get.back(
                                  result: controller.selectedLottery.value.id !=
                                          null
                                      ? controller.selectedLottery.value
                                      : null);
                            }),
                          ),
                          const SizedBox(width: 10),
                        ],
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                ))
          ],
        ));
  }
}
