import '../controller/controller.dart';
import 'package:get/get.dart';

class WaitingPrizeBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<WaitingPrizeController>(() => WaitingPrizeController());
  }
}
