import 'package:fpg_flutter/api/ticket/ticket.http.dart';
import 'package:fpg_flutter/api/user/user.http.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_model.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/get.dart';

class WaitingPrizeController extends GetxController {
  static WaitingPrizeController get to => Get.find<WaitingPrizeController>();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  /// 页码
  int page = 1;

  /// 游戏Id
  String gameId = '';

  /// 游戏名称
  final gameName = "".obs;

  /// 总下注金额
  Rx<double> totalBetAmount = 0.00.obs;

  /// 注单列表
  late var list = [].cast<LotteryHistoryModel>().obs;

  /// 获取注单列表
  Future getTickeHistory(bool isRefresh) async {
    if (isRefresh) {
      page = 1;
    } else {
      page++;
    }

    final res = await TicketHttp.ticketHistory(
      page: page,
      gameId: gameId,
      status: 1,
    );

    var resultList = res.data!.list ?? [];
    List<LotteryHistoryModel> tempList = [];
    if (isRefresh) {
      refreshController.refreshCompleted();
      totalBetAmount.value = double.parse(res.data!.totalBetAmount ?? "0.00");
    } else {
      totalBetAmount.value += double.parse(res.data!.totalBetAmount ?? "0.00");
      tempList.addAll(list);
    }

    if (resultList.length < 30) {
      refreshController.loadNoData();
    } else {
      refreshController.loadComplete();
    }

    tempList.addAll(resultList);
    list.value = tempList;
  }

  /// 取消订单
  Future cancelBet(int orderId) async {
    final res = await UserHttp.userCancelBet(orderId);
    ToastUtils.show(res.msg);
    getTickeHistory(true);
  }

  Future onRefresh() async {
    refreshController.resetNoData();
    getTickeHistory(true);
  }

  Future onLoading() async {
    getTickeHistory(false);
  }

  @override
  void onInit() {
    // 获取路由参数
    var arguments = Get.arguments;
    if (arguments != null && arguments is Map) {
      gameName.value = arguments["gameName"];
      gameId = arguments["gameId"];
    }
    super.onInit();
  }

  @override
  void onReady() {
    getTickeHistory(true);
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
