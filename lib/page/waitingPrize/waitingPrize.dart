import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/components/custom.appbar.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lotteryHistoryList/lottery_history_model.dart';
import 'package:fpg_flutter/page/waitingPrize/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class WaitingPrizePage extends GetView<WaitingPrizeController> {
  const WaitingPrizePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: context.customTheme?.lotteryPopupItemBg,
        appBar: const CustomAppBar(
            textColor: Colors.white,
            iconColor: Colors.white,
            background: Color(0xffb89b65),
            header: "等待开奖"),
        body: SafeArea(
            child: Column(
          children: [
            Container(
                padding: const EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                height: 40,
                child: Obx(
                  () => Text(
                    "${controller.gameName.value} > 下注明细",
                    style: context.textTheme.bodySmall,
                  ),
                )),
            _buildHeaderWidget(context),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(child: _buildBodyWidget(context)),
            _buildBottomWidget(context)
          ],
        )));
  }

  Widget _buildHeaderWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          width: 100,
          child: Container(
            alignment: Alignment.center,
            height: 35,
            child: Text.rich(TextSpan(
                text: "期号/",
                style: context.textTheme.bodySmall,
                children: const [
                  TextSpan(
                      text: "注单号",
                      style: TextStyle(color: Colors.red, fontSize: 12))
                ])),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text(
              "下注明细",
              style: context.textTheme.bodySmall,
            ),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("下注金额", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("可赢金额", style: context.textTheme.bodySmall),
          ),
        ),
        Container(
          width: 0.5,
          height: 35,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 35, // 设置高度作为示例
            child: Text("操作", style: context.textTheme.bodySmall),
          ),
        ),
      ],
    );
  }

  Widget _buildBodyWidget(BuildContext context) {
    return Obx(
      () => SmartRefresher(
          enablePullDown: false,
          enablePullUp: true,
          header: CustomHeader(
            builder: (BuildContext context, RefreshStatus? mode) {
              Widget body;
              if (mode == RefreshStatus.refreshing) {
                body = Text("刷新中..."); // "Refreshing..."
              } else if (mode == RefreshStatus.canRefresh) {
                body = Text("释放刷新"); // "Release to refresh"
              } else if (mode == RefreshStatus.completed) {
                body = Text("刷新完成"); // "Refresh complete"
              } else {
                body = Text("刷新失败"); // "Refresh failed"
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus? mode) {
              Widget body;
              if (mode == LoadStatus.loading) {
                body = Text("加载中..."); // Change 'Loading' to Chinese here
              } else if (mode == LoadStatus.failed) {
                body = Text("加载失败，请重试");
              } else if (mode == LoadStatus.canLoading) {
                body = Text("释放立即加载更多");
              } else {
                body = Text("没有更多数据");
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          controller: controller.refreshController,
          onRefresh: controller.onRefresh,
          onLoading: controller.onLoading,
          child: ListView.builder(
            itemBuilder: (c, i) =>
                _buildBodyItemWidget(context, controller.list[i]),
            itemCount: controller.list.length,
          )),
    );
  }

  Widget _buildBodyItemWidget(BuildContext context, LotteryHistoryModel model) {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text("${model.issue}", style: context.textTheme.bodySmall),
                  const SizedBox(height: 20),
                  Text("${model.id}",
                      style: const TextStyle(fontSize: 12, color: Colors.red))
                ],
              ).paddingOnly(left: 5),
            ),
            Container(
              width: 0.5,
              height: 85,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    alignment: Alignment.center,
                    height: 85, // 设置高度作为示例
                    child: Text(
                      '${model.playGroupName} ${model.playName} @ ${double.parse(model.odds ?? "0.0").toStringAsFixed(1)}',
                      maxLines: 10,
                      textAlign: TextAlign.center,
                      style: context.textTheme.bodySmall,
                    ))),
            Container(
              width: 0.5,
              height: 85,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                height: 85, // 设置高度作为示例
                child: Text(
                    double.parse(model.betAmount.toString()).toStringAsFixed(2),
                    style: context.textTheme.bodySmall),
              ),
            ),
            Container(
              width: 0.5,
              height: 85, // 设置高度作为示例
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      double.parse(model.expectAmount.toString())
                          .toStringAsFixed(2),
                      style: context.textTheme.bodySmall,
                    ))),
            Container(
              width: 0.5,
              height: 85, // 设置高度作为示例
              color: context.customTheme?.lotteryPopupDivColor,
            ),
            Expanded(
                child: Container(
                    alignment: Alignment.center,
                    child: const Text(
                      '撤单',
                      style: TextStyle(
                        fontSize: 12,
                        color: Color(0xff4b85f1),
                        decoration: TextDecoration.underline,
                        decorationColor: Color(0xff4b85f1), // 可选：指定下划线颜色
                        decorationThickness: 2, // 可选：指定下划线粗细
                        decorationStyle:
                            TextDecorationStyle.solid, // 可选：指定下划线样式,
                      ),
                    ).onTap(() {
                      showCancelBetDialog(model);
                    }))),
          ],
        ),
        Container(
          height: 1,
          color: context.customTheme?.lotteryPopupDivColor,
        ),
      ],
    );
  }

  void showCancelBetDialog(LotteryHistoryModel model) {
    SmartDialog.show(builder: (context) {
      return Container(
        width: MediaQuery.of(context).size.width - 36,
        height: 100,
        decoration: BoxDecoration(
          color: context.customTheme?.lotteryPopupItemBg,
          borderRadius: const BorderRadius.all(Radius.circular(4)),
        ),
        child: Column(
          children: [
            const SizedBox(height: 10),
            Text(
              "你确定要撤销注单${model.id}",
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                const SizedBox(width: 10),
                Expanded(
                    child: Container(
                  height: 42,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    // 设置渐变色背景
                    border: Border.all(
                      color: const Color(0xffa9a9a9), // 边框颜色
                      width: 1.0, // 边框宽度
                    ),
                    borderRadius: BorderRadius.circular(4), // 设置圆角
                  ),
                  child: Text(
                    '取消',
                    style: TextStyle(
                        color: context.customTheme?.fontColor, fontSize: 16),
                  ),
                ).onTap(() => SmartDialog.dismiss())),
                const SizedBox(width: 5),
                Expanded(
                  child: Container(
                    height: 42,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color(0xffe5caae),
                          Color(0xffd3a06f),
                        ],
                      ), // 设置渐变色背景
                      borderRadius: BorderRadius.circular(4), // 设置圆角
                    ),
                    child: const Text(
                      '确定',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ).onTap(() {
                    SmartDialog.dismiss();
                    controller.cancelBet(int.parse(model.id!));
                  }),
                ),
                const SizedBox(width: 10),
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      );
    });
  }

  Widget _buildBottomWidget(BuildContext context) {
    return SizedBox(
      height: 35,
      child: Column(
        children: [
          Container(
            height: 1,
            color: context.customTheme?.lotteryPopupDivColor,
          ),
          Container(
              height: 34,
              padding: const EdgeInsets.only(right: 10),
              alignment: Alignment.centerRight,
              child: Obx(() => Text.rich(TextSpan(
                      text: "下注总金额:  ",
                      style: context.textTheme.bodySmall,
                      children: [
                        TextSpan(
                            text: controller.totalBetAmount.value
                                .toStringAsFixed(2),
                            style: const TextStyle(
                                color: Color(0xff349044), fontSize: 12))
                      ]))))
        ],
      ),
    );
  }
}
