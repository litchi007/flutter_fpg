import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/models/lottery_group_games_model/lottery.dart';
import 'package:fpg_flutter/page/gameRule/gameRule.dart';
import 'package:fpg_flutter/page/lotteryPopup/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';

class LotteryPopupPage extends GetView<LotteryPopupController> {
  /// 游戏
  final Lottery lottery;
  const LotteryPopupPage(this.lottery, {super.key});

  @override
  Widget build(BuildContext context) {
    // 初始化
    Get.put(LotteryPopupController());
    return Scaffold(
        backgroundColor: Colors.transparent, // 设置背景透明
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                color: Colors.black.withOpacity(0.1),
              ),
            ),
            Positioned(
                top: MediaQuery.of(context).padding.top + kToolbarHeight - 15,
                right: 10,
                child: _buildList(context)),
          ],
        ));
  }

  Widget _buildList(BuildContext context) {
    List<Widget> children = [];

    children.add(_buildUnSettlementItem('即时注单\n未结注单', context, () {
      Get.back();
      Get.offAndToNamed(AppRoutes.lotteryLiveBet);
    }));

    children.add(_buildItem('今日已结', context, () {
      Get.offAndToNamed(AppRoutes.todaySettled);
    }));

    children.add(_buildItem('下注记录', context, () {
      if (!GlobalService.isShowTestToast) {
        Get.offAndToNamed(AppRoutes.lotteryTickets);
      }
    }));

    children.add(_buildItem('开奖结果', context, () {
      Get.offAndToNamed(AppRoutes.lotteryResult, arguments: lottery.id);
    }));

    children.add(_buildItem('游戏规则', context, () {
      Get.to(() => GameRulePage(lottery),
          opaque: false,
          fullscreenDialog: true,
          transition: Transition.noTransition);
    }));

    children.add(_buildItem('两面长龙', context, () {
      Get.offAndToNamed(AppRoutes.twoSideDragon, arguments: lottery);
    }));

    children.add(_buildLongDargonItem('长龙助手', context, () {
      Get.offAndToNamed(AppRoutes.longDragonTool);
    }));

    children.add(_buildItem('走势图', context, () {
      Get.back();
      Get.toNamed("${AppRoutes.lotteryTrend}/${lottery.id}",
          arguments: lottery);
    }));

    children.add(_buildItem('路珠', context, () {
      Get.offAndToNamed(AppRoutes.lotteryRoadData, arguments: lottery);
    }));

    children.add(_buildItem('红包记录', context, () {
      if (!GlobalService.isShowTestToast) {
        Get.offAndToNamed('${AppRoutes.redEnvelope}/1');
      }
    }));

    children.add(_buildItem('扫雷记录', context, () {
      if (!GlobalService.isShowTestToast) {
        Get.offAndToNamed('${AppRoutes.redEnvelope}/2');
      }
    }));

    if (!GlobalService.isTest) {
      // 如果不是游客 则添加这2个选项
      children.add(_buildItem('充值', context, () {
        Get.offAndToNamed(depositPath);
      }));

      children.add(_buildItem('提现', context, () {
        Get.offAndToNamed(depositPath, arguments: [1]);
      }));
    }

    children.add(_buildTodayWinLoseItem('今天输赢', context, () {
      Get.offAndToNamed(AppRoutes.todaySettled);
    }));

    return Container(
      width: 110,
      decoration: BoxDecoration(
        border: Border.all(
          color: context.customTheme?.lotteryPopupDivColor ??
              Colors.transparent, // 边框颜色
          width: 1, // 边框宽度
        ),
      ),
      child: Column(
        children: children,
      ),
    );
  }

  // 即时注单
  Widget _buildUnSettlementItem(
      String title, BuildContext context, GestureTapCallback? onTap) {
    return Container(
        color: context.customTheme?.lotteryPopupItemBg,
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            Obx(() {
              return controller.lotteryData.value.unbalancedMoney != null
                  ? Text("(${controller.lotteryData.value.unbalancedMoney})",
                      style: const TextStyle(
                          color: Color(0xfffe5337),
                          fontSize: 16,
                          fontWeight: FontWeight.w600))
                  : const SizedBox(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.red)));
            }),
            const SizedBox(height: 6),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
          ],
        )).onTap(onTap);
  }

  // 其他
  Widget _buildItem(
      String title, BuildContext context, GestureTapCallback? onTap) {
    return Container(
        color: context.customTheme?.lotteryPopupItemBg,
        // height: 35,
        child: Column(
          children: [
            const SizedBox(height: 6),
            Text(
              title,
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 6),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
          ],
        )).onTap(onTap);
  }

  // 长龙助手
  Widget _buildLongDargonItem(
      String title, BuildContext context, GestureTapCallback? onTap) {
    return Container(
      color: context.customTheme?.lotteryPopupItemBg,
      // height: 35,
      child: Stack(
        children: [
          Column(children: [
            const SizedBox(height: 6),
            Text(
              title,
              style: TextStyle(
                  color: context.customTheme?.fontColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 6),
            Container(
              height: 1,
              color: context.customTheme?.lotteryPopupDivColor,
            ),
          ]),
          Positioned(
            top: 2,
            right: 2,
            child: AppImage.network(img_images('hot0', type: 'gif')),
          ),
        ],
      ),
    ).onTap(onTap);
  }

  // 今天输赢
  Widget _buildTodayWinLoseItem(
      String title, BuildContext context, GestureTapCallback? onTap) {
    return Container(
            color: context.customTheme?.lotteryPopupItemBg,
            child: Container(
                width: 110,
                color: const Color(0xffe2e2e2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 6),
                    Text(
                      title,
                      style: const TextStyle(
                          color: Color(0xff477ddd),
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    Obx(() {
                      return controller.lotteryData.value.totalTotalMoney !=
                              null
                          ? Text(
                              "(${controller.lotteryData.value.totalTotalMoney})",
                              style: const TextStyle(
                                  color: Color(0xfffe5337),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600))
                          : const SizedBox(
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.red)));
                    }),
                    const SizedBox(height: 6),
                  ],
                )).paddingAll(1))
        .onTap(onTap);
  }
}
