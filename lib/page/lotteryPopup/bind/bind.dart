import '../controller/controller.dart';
import 'package:get/get.dart';

class LotteryPopupBinding implements Bindings {
  @override
  void dependencies() {
    final tag = Get.parameters["null"];
    Get.lazyPut<LotteryPopupController>(() => LotteryPopupController());
  }
}
