import 'package:fpg_flutter/api/ticket/ticket.http.dart';
import 'package:fpg_flutter/models/lottery_data/lottery_data_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart';
import 'dart:async';

import 'package:intl/intl.dart';

class LotteryPopupController extends GetxController {
  static LotteryPopupController get to => Get.find<LotteryPopupController>();

  /// 彩票数据
  final lotteryData = const LotteryDataModel().obs;

  /// 获取注单统计
  Future getTickeLotteryData() async {
    final res = await TicketHttp.ticketLotteryData();
    LogUtil.w(res.data);
    if (res.data != null) {
      lotteryData.value = res.data!;
    }
  }

  @override
  void onReady() {
    /// 获取注单统计
    getTickeLotteryData();
    super.onReady();
  }
}
