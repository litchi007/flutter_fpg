import 'package:fpg_flutter/configs/app_define.dart';

class Validator {
  static String? validateUserId(String value) {
    String min = AppDefine.systemConfig?.userLengthMin ?? "6";
    String max = AppDefine.systemConfig?.userLengthMax ?? "16";
    final RegExp regex = RegExp("^[a-zA-Z0-9]{$min,$max}\$");
    if (!regex.hasMatch(value)) {
      return '*请使用$min-$max位字母或数字的组合';
    } else {
      return null;
    }
  }

  static String? validateEmail(String value) {
    Pattern pattern = r'^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+';
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(value)) {
      return '電子郵件不符';
    } else {
      return null;
    }
  }

  static String? validatePassword(String value) {
    if (value.length < 6) {
      return '密码不能低于6位';
    }

    String min = AppDefine.systemConfig?.passLengthMin ?? "6";
    String max = AppDefine.systemConfig?.passLengthMax ?? "16";
    final RegExp regex = RegExp("^[a-zA-Z0-9]{$min,$max}\$");
    if (!regex.hasMatch(value)) {
      return '请使用$min-$max位字母或数字的组合';
    } else {
      return null;
    }
  }

  static String? validateFullName(String value) {
    if (value.length < 2) {
      return '请使用2个以上字母的组合。';
    } else {
      return null;
    }
  }

  static String? validateRequired(String value) {
    if (value.isEmpty) {
      return '此字段是必需的。';
    } else {
      return null;
    }
  }
}
