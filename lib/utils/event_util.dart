import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

class EventUtils {
  /// 防抖
  static debounce(Function? fn, Duration delay) {
    Timer? timer;

    return () {
      // ignore: unnecessary_null_comparison
      if (timer != null && (timer?.isActive ?? false)) {
        timer?.cancel();
      }
      timer = Timer(delay, fn?.call());
    };
  }

  /// 睡眠
  static Future<void> sleep(Duration delay) {
    return Future.delayed(delay);
  }

  /// 打开网页
  static lanuch(String url) async {
    await url_launcher.launch((url));
  }

  /// 生成规定数字内随机数
  /// *[length] 长度
  static List<int> robotRandom(int length, [int select = 3]) {
    final random = Random();
    List<int> numbers =
        List.generate(length, (index) => index); // 生成包含 0 到 10 的列表
    numbers.shuffle(random); // 打乱列表顺序
    return numbers.sublist(0, min(length, select));
  }

  // 获取字符串中的渐变色
  static LinearGradient getLinearGradient(String inputString) {
    LinearGradient result = LinearGradient(colors: [
      "#55c6ff".color(),
      "#91daff".color(),
    ]);
    // 定义正则表达式来匹配 background 样式
    RegExp regex = RegExp(r'background:(.*?)(;|\")');

    // 在输入字符串中查找匹配的样式
    Match? match = regex.firstMatch(inputString);

    // 如果找到匹配项，则输出 background 样式
    if (match != null) {
      String backgroundStyle = match.group(1)!;
      final reg = RegExp(r".\((.*?)\)");
      Match? match1 = reg.firstMatch(backgroundStyle);
      // result =
      final list = match1?.group(1)?.split(",");
      if ((list?.length ?? 0) > 3) {
        result = LinearGradient(colors: [
          list![1].color(),
          list![2].color(),
        ]);
      }
      // print("Background 样式为: ${match1?.group(1)?.split(",")}");
    } else {
      // print("未找到 Background 样式");
    }

    return result;
  }
}
