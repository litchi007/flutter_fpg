import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:fpg_flutter/api/error_report/error_report.dart';
import 'package:fpg_flutter/models/error_report/error_report.dart';
import 'package:get/get.dart';

class AppErrorUtils {
  static init() {
    /// 收集应用报错信息
    FlutterError.onError = (details) {
      /// 报错具体显示内容
      ErrorReportHttp.report(ErrorReport(
          err: details.exception, text: "布局构建错误", page: Get.currentRoute));
    };
  }

  /// 收集报错入口
  static run(void fn) {
    if (!kDebugMode) {
      init();
      runZonedGuarded(() {
        fn;
      }, (err, details) {
        final errorMsg = {
          "stackTrace": details,
        };

        /// 显示build的错误，build的错误可以在这里上报
        ErrorReportHttp.report(
            ErrorReport(err: errorMsg, text: "构建错误", page: Get.currentRoute));
      });
    } else {
      fn;
    }
  }
}
