import 'dart:async';

class TimerManager {
  // Singleton instance
  static final TimerManager _instance = TimerManager._internal();

  // Factory constructor
  factory TimerManager() {
    return _instance;
  }

  TimerManager._internal();

  // List to hold all active timers
  final List<Timer> _timers = [];

  // Method to add a timer to the list
  void addTimer(Timer timer) {
    _timers.add(timer);
  }

  // Method to stop all active timers
  void stopAllTimers() {
    for (final timer in _timers) {
      timer.cancel();
    }
    _timers.clear(); // Clear the list after stopping all timers
  }

  bool isAnyTimerRunning() {
    return _timers.isNotEmpty;
  }
}
