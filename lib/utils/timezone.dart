import 'package:intl/intl.dart';

class TimeZone {
  // Adjust the timezone offset as needed, -8 is for example
  static int getTimeZoneOffsetHours() {
    return -8; // Adjust according to your timezone
  }

  static String getTzTimeBySecond(int s) {
    // Check if the input is in seconds and has the correct length
    if (s.toString().length == 10) {
      // Calculate timezone offset in seconds
      int tzSeconds = getTimeZoneOffsetHours() * 60 * 60;

      // Convert seconds to DateTime
      DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
          (s + tzSeconds) * 1000,
          isUtc: true);

      // Format the DateTime to the desired string format
      String formattedDate =
          DateFormat('yyyy-MM-dd HH:mm:ss').format(dateTime.toLocal());
      return formattedDate;
    } else {
      print('传入的时间不是秒');
      return s.toString();
    }
  }
}
