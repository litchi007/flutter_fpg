import 'package:flutter/material.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/components/custom.toast.tip.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'package:fpg_flutter/utils/uuid_util.dart';

class ToastUtils {
  static show(String value, [bool isCloseAll = true]) async {
    if (isCloseAll) {
      ToastUtils.close();
    }
    await EventUtils.sleep(100.microseconds);
    SmartDialog.showToast(value,
        alignment: Alignment.center,
        usePenetrate: false,
        maskColor: Colors.transparent);
  }

  static showLoading({String? msg}) async {
    ToastUtils.close();
    await EventUtils.sleep(100.microseconds);

    SmartDialog.showLoading(msg: msg ?? '', backDismiss: false);
    Future.delayed(10.seconds, () {
      ToastUtils.close();
    });
  }

  static close([String? tag]) {
    if (tag != null) {
      SmartDialog.dismiss(tag: tag);
    } else {
      SmartDialog.dismiss(status: SmartStatus.allCustom);
    }
  }

  static confirm([CustomConfirmParams? params]) async {
    if (params?.isCloseAll ?? false) {
      ToastUtils.close();
    }
    await EventUtils.sleep(200.microseconds);
    final tag = UuidUtil.to.v4();
    SmartDialog.show(
        tag: tag,
        debounce: false,
        onDismiss: params?.onDismiss,
        clickMaskDismiss: params?.clickMaskDismiss ?? true,
        backDismiss: params?.backDismiss ?? true,
        builder: (BuildContext context) {
          return CustomConfirm(params: params, tag: tag);
        });
  }

  static showWidget([CustomConfirmParams? params]) async {
    await EventUtils.sleep(200.microseconds);
    if (params?.isCloseAll ?? false) {
      ToastUtils.close();
    }
    final tag = UuidUtil.to.v4();
    SmartDialog.show(
        bindPage: true,
        tag: tag,
        debounce: false,
        onDismiss: params?.onDismiss,
        clickMaskDismiss: params?.clickMaskDismiss ?? true,
        backDismiss: params?.backDismiss ?? true,
        builder: (BuildContext context) {
          return params?.child ?? const SizedBox();
        });
  }

  static showTip([CustomConfirmParams? params]) async {
    await EventUtils.sleep(200.microseconds);
    final tag = UuidUtil.to.v4();
    SmartDialog.show(
        bindPage: true,
        tag: tag,
        debounce: false,
        onDismiss: params?.onDismiss,
        clickMaskDismiss: params?.clickMaskDismiss ?? true,
        backDismiss: params?.backDismiss ?? true,
        builder: (BuildContext context) {
          return CustomToastTip(params: params);
        });
  }

  static closeAll() {
    SmartDialog.dismiss();
  }
}
