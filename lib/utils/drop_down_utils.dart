import 'package:flutter/material.dart';

// 定义 Position 数据模型
class Position {
  final String? name;
  final String? value;

  Position({this.name, this.value});
}

// 工具类 DropDownUtils
class DropDownUtils {
  // 获取左侧下拉选项
  static List<Position> getLeftDropDownOptions(String genre) {
    genre = genre.toLowerCase();
    switch (genre) {
      case 'lhc': // 六合彩
        final lhcName = [
          '特肖',
          '平特一肖',
          '平特一肖尾数',
          '特码尾数',
          '正肖',
          '合肖',
        ];
        return lhcName.asMap().entries.map((entry) {
          final index = entry.key;
          final name = entry.value;
          return Position(name: name, value: (index + 1).toString());
        }).toList();

      case 'ngysc': // 未知
        return [
          Position(name: 'ios048', value: '1'),
        ];

      case 'fc3d': // 福彩3D
        final fc3dName = [
          '第一球',
          '第二球',
          '第三球',
          '跨度',
          '独胆',
          '和数尾数',
        ];
        return fc3dName.asMap().entries.map((entry) {
          final index = entry.key;
          final name = entry.value;
          return Position(name: name, value: (index + 1).toString());
        }).toList();

      case 'gd11x5': // 广东11选5
        final gd11x5Name = [
          '第一球',
          '第二球',
          '第三球',
          '第四球',
          '第五球',
          '一中一',
        ];
        return gd11x5Name.asMap().entries.map((entry) {
          final index = entry.key;
          final name = entry.value;
          return Position(name: name, value: (index + 1).toString());
        }).toList();

      case 'cqssc': // 重庆时时彩
        final cqsscName = [
          '第一球',
          '第二球',
          '第三球',
          '第四球',
          '第五球',
        ];
        return cqsscName.asMap().entries.map((entry) {
          final index = entry.key;
          final name = entry.value;
          return Position(name: name, value: (index + 1).toString());
        }).toList();

      case 'xyft': // 幸运飞艇
        final xyftName = [
          '冠军',
          '亚军',
          '第三名',
          '第四名',
          '第五名',
          '第六名',
          '第七名',
          '第八名',
          '第九名',
          '第十名',
        ];
        return xyftName.asMap().entries.map((entry) {
          final index = entry.key;
          final name = entry.value;
          return Position(name: name, value: (index + 1).toString());
        }).toList();

      case 'pk10': // 北京赛车
        final pk10Name = [
          '冠军',
          '亚军',
          '第三名',
          '第四名',
          '第五名',
          '第六名',
          '第七名',
          '第八名',
          '第九名',
          '第十名',
        ];
        return pk10Name.asMap().entries.map((entry) {
          final index = entry.key;
          final name = entry.value;
          return Position(name: name, value: (index + 1).toString());
        }).toList();

      case 'ugysc': // UG颜色彩
        return [
          Position(name: '正码', value: '1'),
        ];

      default:
        final defaultName = [
          '特肖',
          '平特一肖',
          '平特一肖尾数',
          '特码尾数',
          '正肖',
          '合肖',
        ];
        return defaultName.asMap().entries.map((entry) {
          final index = entry.key;
          final name = entry.value;
          return Position(name: name, value: (index + 1).toString());
        }).toList();
    }
  }
}
