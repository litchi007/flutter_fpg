import 'package:connectivity_plus/connectivity_plus.dart';

class PermissionUtils {
  static Future<List<ConnectivityResult>> requestNetword() {
    return Connectivity().checkConnectivity();
  }
}
