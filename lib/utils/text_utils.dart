import 'package:extended_text/extended_text.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/lottery_config.dart';

class MySpecialTextSpanBuilder extends SpecialTextSpanBuilder {
  MySpecialTextSpanBuilder({this.showAtBackground = false});

  /// whether show background for @somebody
  final bool showAtBackground;

  @override
  SpecialText? createSpecialText(String flag,
      {TextStyle? textStyle,
      SpecialTextGestureTapCallback? onTap,
      int? index}) {
    if (flag == '') {
      return null;
    }

    ///index is end index of start flag, so text start index should be index-(flag.length-1)
    if (isStart(flag, EmojiText.flag)) {
      return EmojiText(textStyle!, start: index! - (EmojiText.flag.length - 1));
    }
    return null;
  }
}

class EmojiText extends SpecialText {
  static const String flag = "[";
  final int start;
  EmojiText(TextStyle textStyle, {required this.start})
      : super(EmojiText.flag, "]", textStyle);

  @override
  InlineSpan finishText() {
    var key = toString();
    if (EmojiUitl.instance.emojiMap.containsKey(key)) {
      //fontsize id define image height
      //size = 30.0/26.0 * fontSize
      final double size = 20.0;

      ///fontSize 26 and text height =30.0
      //final double fontSize = 26.0;
      return ImageSpan(NetworkImage(EmojiUitl.instance.emojiMap[key]!),
          actualText: key,
          imageWidth: size,
          imageHeight: size,
          start: start,
          fit: BoxFit.fill,
          margin: const EdgeInsets.only(left: 2.0, top: 2.0, right: 2.0));
    }

    return TextSpan(text: toString(), style: textStyle);
  }
}

class EmojiUitl {
  final Map<String, String> _emojiMap = <String, String>{};

  Map<String, String> get emojiMap => _emojiMap;

  // final String _emojiFilePath = "assets/images/emoji";
  final String _emojiFilePath = LotteryConfig.emojiBaseUrl;

  static EmojiUitl? _instance;
  static EmojiUitl get instance {
    _instance ??= EmojiUitl._();
    return _instance!;
  }

  EmojiUitl._() {
    final list = LotteryConfig.emojiList;
    for (int i = 0; i < list.length; i++) {
      final value = list[i];
      _emojiMap["[em_$value]"] = "${LotteryConfig.emojiBaseUrl}/$value.gif";
      // _emojiMap["[${list[i].text}]"] = "$_emojiFilePath/${list[i].src}.webp";
    }
    print("_emojiMap____$_emojiMap");
  }
}
