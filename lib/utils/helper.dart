import 'package:fpg_flutter/app/widgets/app_toast.dart';
import 'package:fpg_flutter/app_navigator.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:url_launcher/url_launcher.dart';

int? convertToInt(dynamic value) {
  try {
    if (value is int) {
      return value;
    }
    return int.parse(value.toString());
  } catch (e) {}
  return null;
}

double? convertToDouble(dynamic value) {
  if (value is int) {
    return value.toDouble();
  } else if (value is double) {
    return value;
  } else if (value is String) {
    try {
      return double.parse(value);
    } catch (e) {
      print('Error parsing string to double: $e');
      // return 0;
      // Alternatively, you can throw an error
      // throw ArgumentError('Value cannot be converted to double: $value');
    }
    // return double.parse(value);
  }

  return null;
}

String? getToken({bool isH5Token = false}) {
  // final user = UGStore.globalProps?.userInfo;
  // if (isH5Token) {
  // return user?.token ?? user?.apiToken;
  // }
  // return user?.sessid ?? user?.apiSID;
  return null;
}

bool checkIsTest() {
  if (GlobalService.to.userInfo.value?.isTest == true) {
    AppToast.showToast('请先登录正式账号');
    return true;
  } else {
    return false;
  }
}

String clearExHtml({String? text, String? color}) {
  return text ?? '';

  return '<span style="color: ${color ?? '#888888'}">'
      '${text?.replaceAll(RegExp('font'), 'span')}'
      // '${text?.replaceAll(RegExp(r'color="#'), 'style="color:#')}'
      // '${text?.replaceAll(RegExp(r'&nbsp;'), ' ')}'
      // '${text?.replaceAll(RegExp(r'<table'), '<table style="width:100%;"')}'
      '</span>';
}

// String parseMsg(String msg) {
//   List<InlineSpan> spans = [];
// final emojiReg = RegExp(r'\[em_.*?\]');
// final matches = emojiReg.allMatches(msg);

//   int lastMatchEnd = 0;
//   matches.forEach((match) {
//     // Add text before the emoji
//     if (match.start > lastMatchEnd) {
//       spans.add(TextSpan(text: msg.substring(lastMatchEnd, match.start)));
//     }

//     // Extract emoji number
//     String emojiTag = match.group(0)!; // Example: "[em_001]"
//     String emojiCode =
//         emojiTag.replaceAll(RegExp(r'\[em_|\]'), ''); // Extract "001"

//     // Add emoji image
//     String emojiUrl = img_chat('emoji/$emojiCode', type: 'gif');
//     spans.add(WidgetSpan(
//       child: Image.network(
//         emojiUrl,
//         width: 20, // You can adjust the size of the emoji
//         height: 20,
//       ),
//     ));

//     lastMatchEnd = match.end;
//   });

//   // Add any remaining text after the last emoji
//   if (lastMatchEnd < msg.length) {
//     spans.add(TextSpan(text: msg.substring(lastMatchEnd)));
//   }

//   print('${matches.length > 0 ? 'change html content' : ""}');

//   return msg;
// }

String parseEmojiMessage(String msg) {
  // If item.contentPic is empty, try to grep any image from <img> tag in item.fullContent
  // if (item['contentPic'] == null || item['contentPic'].isEmpty) {
  //   final RegExp regex = RegExp(r'<img[^>]+src="([^">]+\.jpe?g)"');
  //   List<String> contentPic = [];
  //   String fullContent = item['fullContent'] ?? '';
  //   Iterable<RegExpMatch> matches = regex.allMatches(fullContent);

  //   for (var match in matches) {
  //     contentPic.add(match.group(1)!);
  //   }
  //   item['contentPic'] = contentPic;
  // }

  const String separator = '<p style=';
  msg = msg.split(separator).take(4).join(separator);
  // }

  String resultMsg = '';
  final RegExp emojiReg = RegExp(r'\[em_.*?\]');
  List<String> emojiArr = [];
  List<String> strs = [];
  emojiArr = emojiReg.allMatches(msg).map((m) => m.group(0) ?? '').toList();

  if (emojiArr.isNotEmpty) {
    for (var s in emojiArr) {
      final int length = s.length;
      final int strIdx = msg.indexOf(s);

      if (strIdx != 0) {
        strs.add(msg.substring(0, strIdx));
      }
      strs.add(s);
      resultMsg += msg.substring(strIdx + length, msg.length);
    }

    return strs.map((s) {
      if (s.contains('[em_')) {
        s = s.replaceAll('[em_', '').replaceAll(']', '');
        String uri =
            'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz/images/arclist/${int.parse(s)}.gif';
        // String uri = imgChat('emoji/${int.parse(s)}', 'gif');
        return '<img width="25" src="$uri"/>';
      } else {
        return s;
      }
    }).join('');
  } else {
    return msg;
  }
}
