import 'package:uuid/uuid.dart';

class UuidUtil {
  static Uuid get to => const Uuid();
}
