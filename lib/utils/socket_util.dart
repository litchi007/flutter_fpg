import 'package:fpg_flutter/services/global_service.dart';

class SocketUtil {
  /// 获取搜索用户列表
  static SocketGetUserModel getUser({String? condition, String? value}) {
    return SocketGetUserModel(
        code: "0002",
        t: DateTime.now().millisecondsSinceEpoch,
        condition: condition,
        username: GlobalService.to.userInfo.value?.usr,
        token: GlobalService.to.token.value,
        level: GlobalService.to.userInfo.value?.curLevelInt,
        value: value);
  }
}

class SocketGetUserModel {
  String? code;
  int? t;
  String? username;
  String? condition;
  String? value;
  String? token;
  int? level;

  SocketGetUserModel(
      {this.code,
      this.t,
      this.username,
      this.condition,
      this.value,
      this.token,
      this.level});

  SocketGetUserModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    t = json['t'];
    username = json['username'];
    condition = json['condition'];
    value = json['value'];
    token = json['token'];
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['t'] = t;
    data['username'] = username;
    data['condition'] = condition;
    data['value'] = value;
    data['token'] = token;
    data['level'] = level;
    return data;
  }
}

class SocketGetUserResModel {
  String? code;
  String? uid;
  String? level;
  String? username;
  String? usernameBak;
  String? avator;
  String? levelImg;
  bool? isManager;
  String? chatIP;

  SocketGetUserResModel(
      {this.code,
      this.uid,
      this.level,
      this.username,
      this.usernameBak,
      this.avator,
      this.levelImg,
      this.isManager,
      this.chatIP});

  SocketGetUserResModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    uid = json['uid'];
    level = json['level'];
    username = json['username'];
    usernameBak = json['usernameBak'];
    avator = json['avator'];
    levelImg = json['levelImg'];
    isManager = json['isManager'];
    chatIP = json['chatIP'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['code'] = code;
    data['uid'] = uid;
    data['level'] = level;
    data['username'] = username;
    data['usernameBak'] = usernameBak;
    data['avator'] = avator;
    data['levelImg'] = levelImg;
    data['isManager'] = isManager;
    data['chatIP'] = chatIP;
    return data;
  }
}
