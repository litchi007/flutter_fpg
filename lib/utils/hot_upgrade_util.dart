import 'package:flutter/material.dart';
import 'package:fpg_flutter/components/custom.confirm.dart';
import 'package:fpg_flutter/components/custom.update.dart';
import 'package:fpg_flutter/main.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:restart_app/restart_app.dart';

@pragma('vm:entry-point')
void hotUpgradeIsolate(String arg) {
  HotUpgradeUtils.currentPatchNumber();
}

class HotUpgradeUtils {
  /// 检测当前版本
  static Future<int?> currentPatchNumber() async {
    final res = await shorebirdCodePush.currentPatchNumber();
    print("当前版本号 $res");

    /// 获取完版本号，存在热更就自动更新
    checkForUpdates();
    return res;
  }

  /// 热更
  static Future<void> checkForUpdates() async {
    // Check whether a patch is available to install.
    final isUpdateAvailable =
        await shorebirdCodePush.isNewPatchAvailableForDownload();
    print("是否可以升级$isUpdateAvailable");

    if (isUpdateAvailable) {
      await shorebirdCodePush.downloadUpdateIfAvailable();
      Restart.restartApp();
      // ToastUtils.showWidget(
      //   CustomConfirmParams(
      //     child: const CustomUpdateApp()
      //   ));
      // CustomConfirmParams(
      //   cancel: () {
      //     ToastUtils.close();
      //   },
      //   cancelText: "继续使用",
      //   submitText: "前往更新",
      //   showTitle: false,
      //   submit: () async{
      //     try {
      //       await shorebirdCodePush.downloadUpdateIfAvailable();
      //       LogUtil.w("message");
      //       ToastUtils.show("更新完成,请重启APP查看");
      //     } catch (e) {
      //       LogUtil.e(e);
      //     }
      //   },
      //   contentChild:
      // )
      // );
    }
  }
}
