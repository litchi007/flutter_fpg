import 'dart:async';

class Polling {
  Timer? _timer;
  late int _intervalS;
  late Function callbackFunc;
  bool now = true;

  Polling(int interval, Function callback, {this.now = true}) {
    this.interval = interval;
    callbackFunc = callback;
  }

  set interval(int interval) {
    if (interval <= 1) {
      interval = 1;
    }
    _intervalS = interval;
  }

  int get interval => _intervalS * 1000;

  // set callback(Function callback) {
  //   stop();
  //   callbackFunc = callback;
  // }

  // Function get callback => callbackFunc;

  // Start polling
  void start([List<dynamic>? args]) {
    if (now) {
      callbackFunc(args);
      now = false;
    }
    _timer = Timer(Duration(milliseconds: interval), () {
      callbackFunc(args);
      start(args);
    });
  }

  // Stop polling
  void stop() {
    _timer?.cancel();
  }
}

class CountdownTimer {
  Timer? _timer;
  late Function callbackFunc;

  CountdownTimer();

  set callback(Function callback) {
    stop();
    callbackFunc = callback;
  }

  Function get callback => callbackFunc;

  void start(int duration, {bool now = true}) {
    void countdown(int remainingTime) {
      if (remainingTime > 0) {
        if (now) callback(remainingTime);
        _timer = Timer(
            const Duration(seconds: 1), () => countdown(remainingTime - 1));
      } else {
        callback(0);
        stop();
      }
    }

    countdown(duration);
  }

  void stop() {
    _timer?.cancel();
  }
}

enum TimerKeys { diff, loop }

class CountdownTimerManager {
  static final Map<TimerKeys, dynamic> _timers = {};
  static final Map<TimerKeys, Function> _creates = {
    TimerKeys.diff: () => _timers[TimerKeys.diff] = CountdownTimer(),
    TimerKeys.loop: (Function callback) =>
        _timers[TimerKeys.loop] = Polling(5, callback),
  };

  // Add a timer
  static dynamic addTimer(TimerKeys key, [Function? callback]) {
    if (_has(key)) {
      return _getTimer(key);
    }
    if (_creates.containsKey(key)) {
      _creates[key]!(callback);
    }
    return _getTimer(key);
  }

  static bool _has(TimerKeys key) => _timers.containsKey(key);

  // Get a timer
  static dynamic _getTimer(TimerKeys key) {
    if (!_has(key)) {
      return null;
    }
    return _timers[key];
  }
}
