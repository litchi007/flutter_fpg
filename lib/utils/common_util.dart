import 'dart:convert';
import 'dart:ui';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:fpg_flutter/utils/device_util.dart';

class CommonUtil {
  factory CommonUtil() => _sharedInstance();

  CommonUtil._() {
    _init();
  }

  static CommonUtil? _instance;

  static CommonUtil _sharedInstance() {
    _instance ??= CommonUtil._();
    return _instance!;
  }

  // late AndroidId _androidId;
  late DeviceInfoPlugin _deviceInfo;

  Future<void> _init() async {
    // _androidId = const AndroidId();
    _deviceInfo = DeviceInfoPlugin();
  }

  Future<AndroidDeviceInfo> deviceInfo() async {
    final androidInfo = await _deviceInfo.androidInfo;
    return androidInfo;
  }

  Future<String> model() async {
    if (DeviceUtil.isIOS) {
      final IosDeviceInfo iosInfo = await _deviceInfo.iosInfo;
      return iosInfo.utsname.machine;
    } else if (DeviceUtil.isAndroid) {
      final androidInfo = await _deviceInfo.androidInfo;
      return '${androidInfo.brand} ${androidInfo.model}';
    }
    return '';
  }

  Future<String?> deviceId() async {
    if (DeviceUtil.isIOS) {
      final IosDeviceInfo iosInfo = await _deviceInfo.iosInfo;
      return iosInfo.identifierForVendor;
    } else if (DeviceUtil.isAndroid) {
      final androidDeviceId = await _deviceInfo.androidInfo;
      return androidDeviceId.id;
    } else if (DeviceUtil.isWeb) {
      final WebBrowserInfo webBrowserInfo = await _deviceInfo.webBrowserInfo;
      return webBrowserInfo.userAgent;
    }
    return '';
  }

  //检查字符串是否为有效的 JSON
  static bool isJson(String jsonStr) {
    try {
      json.decode(jsonStr);
      return true;
    } catch (e) {
      return false;
    }
  }

  static String removeLastSlash(String url) {
    return url.replaceAll(RegExp(r'\/$'), '');
  }

  static bool isValidUrl(String url) {
    // Regular expression pattern to match a valid URL
    final urlPattern =
        RegExp(r'^(ftp|http|https):\/\/[^ "]+$', caseSensitive: false);
    return urlPattern.hasMatch(url);
  }

  static String removeHTMLTag(String data) {
    final RegExp regex =
        RegExp(r'<[^>]*>', multiLine: true, caseSensitive: true);
    return data.replaceAll(regex, '');
  }

  static Color hexStringToColor(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return Color(int.parse(hexColor, radix: 16));
  }

  static bool isNumeric(String s) {
    return double.tryParse(s) != null;
  }
}
