import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';

class AppButtonStyles {
  static ButtonStyle elevatedStyle(
      {int fontSize = 14,
      fontWeight = FontWeight.normal,
      Color color = Colors.black,
      Color backgroundColor = AppColors.primaryVariant,
      Color foregroundColor = AppColors.onPrimary,
      double radius = 10.0,
      double width = 120,
      double height = 40,
      EdgeInsets padding =
          const EdgeInsets.symmetric(vertical: 10, horizontal: 20)}) {
    return ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius.w)),
        padding: padding,
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
        minimumSize: Size(width.w, height.w),
        textStyle: TextStyle(
            color: color, fontSize: fontSize.sp, fontWeight: fontWeight));
  }

  static ButtonStyle elevatedOnlyRightRadius(
      {int fontSize = 14,
      fontWeight = FontWeight.normal,
      Color color = Colors.black,
      Color backgroundColor = AppColors.primaryVariant,
      Color foregroundColor = AppColors.onPrimary,
      double radius = 10.0,
      double width = 120,
      double height = 40}) {
    return ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(radius.w),
              bottomRight: Radius.circular(radius.w)),
        ),
        padding: EdgeInsets.only(right: 20.w, left: 20.w),
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
        minimumSize: Size(width.w, height.w),
        textStyle: TextStyle(
            color: color, fontSize: fontSize.sp, fontWeight: fontWeight));
  }

  static ButtonStyle elevatedOnlyTopRadius(
      {int fontSize = 14,
      fontWeight = FontWeight.normal,
      Color color = Colors.black,
      Color backgroundColor = AppColors.primaryVariant,
      Color foregroundColor = AppColors.onPrimary,
      double radius = 10.0,
      double width = 120,
      double height = 40}) {
    return ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(radius.w),
              topRight: Radius.circular(radius.w)),
        ),
        padding: EdgeInsets.only(right: 20.w, left: 20.w),
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
        minimumSize: Size(width.w, height.w),
        textStyle: TextStyle(
            color: color, fontSize: fontSize.sp, fontWeight: fontWeight));
  }

  static ButtonStyle elevatedFullWidthStyle(
      {int fontSize = 14,
      fontWeight = FontWeight.normal,
      Color color = Colors.black,
      Color backgroundColor = AppColors.primaryVariant,
      Color foregroundColor = AppColors.onPrimary,
      double radius = 10.0,
      double width = 120,
      double height = 40,
      EdgeInsets padding =
          const EdgeInsets.symmetric(vertical: 10, horizontal: 20)}) {
    return ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius.w)),
        padding: padding,
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
        minimumSize: Size.fromHeight(height.h),
        textStyle: TextStyle(
            color: color, fontSize: fontSize.sp, fontWeight: fontWeight));
  }
}
