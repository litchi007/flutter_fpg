// import 'dart:convert';

// class UGSkinManagers {
//   static bool once = false;

//   /// 判断当前皮肤
//   static bool checkTemplateAndSkin(List<dynamic>? template) {
//     var result = template?.firstWhere(
//       (tp) => tp.toString() == UGStore.globalProps.sysConf?.mobileTemplateCategory,
//       orElse: () => null,
//     );
//     // Log statement can be added here if needed for debugging
//     return !anyEmpty(result);
//   }

//   // 更新皮肤
//   static Future<void> updateSkin(UGSysConfModel sysConf) async {
//     try {
//       // 這會把mobileTemplateCategory參數鎖死無法重新定義，先隱藏起來，如果沒問題可以把這行移除
//       // this.dynamicMobileTemplateCategory(sysConf);
//       String? skinType = sysConf != null ? getSkinTypeString(sysConf) : null;
//       // 单站写死模板
//       skinType = appConfig.skinKeys(AppDefine.siteId) ?? skinType;
//       //默认使用经典模板
//       skinType = skinType ?? UGSkinType1.经典1蓝;
//       print('ssssssssssssss $skinType');

//       // 本地调试模板
//       if (devConfig.isDebug) {
//         if (devConfig?.skinKey != null) {
//           skinType = devConfig?.skinKey!;
//         }
//       }

//       // 这里必须写两遍（因为第一遍时 skin1 还是空的，获取不到 getter 的 skin1.themeColor 值）
//       var skin1 = getSkinValue(skinType!);
//       skin1 = getSkinValue(skinType);
//       UGStore.dispatch({'type': 'reset', 'skinType': skinType ?? ''}, true);
//       print('当前为皮肤：${skin1['skitType']}, ${skin1['isPublished']}');
//       print('AppDefine languageCode：${AppDefine.languageCode}');
//       print('skin1 == ${jsonEncode(skin1)}');
//     } catch (error) {
//       print('↓↓↓↓↓↓↓↓ updateSkin ↓↓↓↓↓↓↓↓');
//       print('error ========> $error');
//       print('↑↑↑↑↑↑↑↑ updateSkin ↑↑↑↑↑↑↑↑');
//     }
//     // await updateOcSkin();
//   }

//   // 从sysConf获取对应模板名
//   static String? getSkinTypeString(UGSysConfModel sysConf, [String? mobileTemplateCategory1]) {
//     if (sysConf.mobileTemplateCategory == ThemeNumber.ChampagneGolden.toString()) {
//       //香槟金特殊处理
//       sysConf.mobileTemplateStyle = sysConf.mobileTemplateXbjStyle;
//     } else if (sysConf.mobileTemplateCategory == ThemeNumber.vb68.toString()) {
//       //vb特殊处理
//       sysConf.mobileTemplateStyle = sysConf.mobileTemplateVB68Style;
//     } else if (sysConf.mobileTemplateCategory == ThemeNumber.SixData.toString()) {
//       sysConf.mobileTemplateStyle = sysConf.mobileTemplateLhcStyle;
//     }

//     final mobileTemplateCategory = mobileTemplateCategory1 ?? sysConf.mobileTemplateCategory;

//     for (final k1 in skinConfig.mobileTemplateCategory.keys) {
//       final mtc = skinConfig.mobileTemplateCategory[k1];
//       final mtb = skinConfig.mobileTemplateBackground[k1];
//       final mts = skinConfig.mobileTemplateStyle[k1];

//       if (mtc != int.tryParse(mobileTemplateCategory!)) {
//         continue;
//       }
//       if ((mtb == null || mtb == sysConf.mobileTemplateBackground) && (mts == null || mts == sysConf.mobileTemplateStyle)) {
//         return k1;
//       }
//     }
//     return null;
//   }

//   // 根据模板名获取当前模板信息
//   static Map<String, dynamic> getSkinValue(String skin) {
//     void addParams(Map<String, dynamic> target, Map<String, dynamic> source) {
//       for (final k in source.keys) {
//         final obj = source[k];
//         if (obj is Map && obj.containsKey(UGSkinType1.默认)) {
//           target[k] = obj[skin] ?? obj[UGSkinType1.默认];
//         } else {
//           target[k] = {};
//           addParams(target[k], obj);
//         }
//       }
//     }

//     Map<String, dynamic> skinColor1 = {};
//     Map<String, dynamic> skinConf1 = {};
//     Map<String, dynamic> skinStyle1 = {};
//     Map<String, dynamic> skinImage1 = {};

//     addParams(skinColor1, skinColors);
//     skinColor1['themeColor'] = skinColor1['themeColor'] ??
//         chroma.scale(skinColor1['navBarBgColor'])(0.5).hex();
//     skinColor1['themeDarkColor'] = skinColor1['themeDarkColor'] ??
//         chroma(skinColor1['themeColor']).darken().hex();
//     skinColor1['themeLightColor'] = skinColor1['themeLightColor'] ??
//         chroma(skinColor1['themeColor']).brighten().hex();
//     skinColor1['themeSelectColor'] = skinColor1['themeSelectColor'] ??
//         chroma(skinColor1['themeColor']).brighten(0.1).hex();

//     addParams(skinConf1, skinConfig);
//     addParams(skinStyle1, skinStyles);
//     addParams(skinImage1, skinImages);

//     return {...skinColor1, ...skinConf1, ...skinStyle1, ...skinImage1};
//   }

//   // 找到所有UGSkinType对象 new 一遍（new操作会配置getter）
//   static void convertToSkinType(Map<String, dynamic> data) {
//     void convert(Map<String, dynamic> target) {
//       for (final k1 in target.keys) {
//         final obj = target[k1];
//         if (obj is Map && obj.containsKey(UGSkinType1.默认)) {
//           target[k1] = UGSkinType(obj);
//         } else {
//           convert(obj);
//         }
//       }
//     }
//     convert(data);
//   }

//   // mobileTemplateCategory 添加 get方法，支持动态判断模板环境
//   static void dynamicMobileTemplateCategory(UGSysConfModel sysConf) {
//     UGStore.globalProps['sysConf'] = sysConf;
//     sysConf.mobileTemplateCategory = sysConf.mobileTemplateCategory;
//     sysConf.mobileTemplateCategory = () {
//       // 单站写死模板
//       var skinType = appConfig.skinKeys(AppDefine.siteId) ?? '';
//       // 聊天APP
//       if (AppDefine.chatSites()) {
//         return skinConfig.mobileTemplateCategory['聊天APP'];
//       }
//       // 本地调试模板
//       if (devConfig.isDebug && devConfig?.skinKey != null) {
//         skinType = devConfig?.skinKey!;
//       }
//       if (skinType.isNotEmpty) {
//         return skinConfig.mobileTemplateCategory[skinType];
//       }
//       // 接口返回的模板
//       return sysConf.mobileTemplateCategory;
//     }();
//   }
// }

// // Helper functions
// bool anyEmpty(dynamic value) {
//   // Implement this function as per your need
//   return value == null || (value is String && value.isEmpty);
// }

// class ThemeNumber {
//   static const ChampagneGolden = 1;
//   static const vb68 = 2;
//   static const SixData = 3;
// }

// class UGSkinType1 {
//   static const 经典1蓝 = 'classic1blue';
//   static const 默认 = 'default';
// }

// Map<String, dynamic> skinColors = {};
// Map<String, dynamic> skinConfig = {};
// Map<String, dynamic> skinStyles = {};
// Map<String, dynamic> skinImages = {};

// class chroma {
//   static Function scale(dynamic color) {
//     return (double value) {
//       // Implement the scale function logic
//       return chroma(''); // Return a chroma object
//     };
//   }

//   chroma(String color);

//   String hex() {
//     // Implement hex conversion
//     return '';
//   }

//   chroma darken() {
//     // Implement darken logic
//     return this;
//   }

//   chroma brighten([double amount = 1.0]) {
//     // Implement brighten logic
//     return this;
//   }
// }
