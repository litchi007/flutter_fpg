import 'package:flutter/material.dart';
import 'package:fpg_flutter/constants/base.theme.dart';
import 'package:fpg_flutter/constants/theme.custom.dart';
import 'package:get/get.dart';
import 'app_theme_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppTheme extends ChangeNotifier {
  static ThemeData lightTheme = ThemeData(
    useMaterial3: false,
    brightness: Brightness.light,
    primarySwatch: Colors.blue,
    cardColor: BaseTheme.subTitleLight,
    extensions: <ThemeExtension<dynamic>>[
      AppThemeColors.light(),
      CustomTheme.light
    ],
  );

  static ThemeData darkTheme = ThemeData(
    useMaterial3: false,
    brightness: Brightness.dark,
    primarySwatch: Colors.blue,
    cardColor: BaseTheme.subTitleDark,
    extensions: <ThemeExtension<dynamic>>[
      AppThemeColors.dark(),
      CustomTheme.dark
    ],
  );
  ThemeMode _themeMode = ThemeMode.light;
  ThemeMode get themeMode => _themeMode;

  AppTheme() {
    final prefs = SharedPreferences.getInstance();
    prefs.then((storage) {
      final value = storage.get('themeMode');

      var themeMode = value ?? 'light';
      if (themeMode == 'light') {
        _themeMode = ThemeMode.light;
        notifyListeners();
      } else {
        _themeMode = ThemeMode.dark;
        notifyListeners();
      }
    });
  }

  void updateTheme(Map<String, CustomTheme> current) {
    AppTheme.lightTheme = AppTheme.lightTheme
        .copyWith(extensions: [AppThemeColors.light(), current['light']!]);
    AppTheme.darkTheme = AppTheme.darkTheme
        .copyWith(extensions: [AppThemeColors.dark(), current['dark']!]);
    Get.forceAppUpdate();
    notifyListeners();
  }

  void toggleTheme() async {
    _themeMode =
        _themeMode == ThemeMode.light ? ThemeMode.dark : ThemeMode.light;
    final prefs = await SharedPreferences.getInstance();
    if (_themeMode == ThemeMode.light) {
      prefs.setString('themeMode', 'light');
    } else {
      prefs.setString('themeMode', 'dark');
    }
    notifyListeners();
  }
}
