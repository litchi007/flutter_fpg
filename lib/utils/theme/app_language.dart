final Map<String, String> currencyLogo = {
  'CNY': '¥',
  'USD': '\$',
  'EUR': '€',
  'VND': '₫',
  'MYR': 'RM',
  'THB': '฿',
  'INR': '₹',
  'IDR': 'IDR',
  'JPY': '¥',
};
