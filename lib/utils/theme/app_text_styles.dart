import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fpg_flutter/utils/theme/app_colors.dart';
import 'app_theme_colors.dart';

class AppTextStyles {
  static TextStyle normalTextStyle(
      {int fontSize = 14,
      fontWeight = FontWeight.normal,
      color = Colors.black}) {
    return TextStyle(
      fontSize: fontSize * 1.sp,
      color: color,
      fontWeight: fontWeight,
    );
  }

  static TextStyle headline1(BuildContext context, {int fontSize = 32}) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return normalTextStyle(
        fontSize: fontSize,
        fontWeight: FontWeight.bold,
        color: appThemeColors?.onBackground);
  }

  static TextStyle headline2(BuildContext context, {int fontSize = 28}) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return normalTextStyle(
        fontSize: fontSize,
        fontWeight: FontWeight.bold,
        color: appThemeColors?.onBackground);
  }

  //ffDDA815_ = ARGB(225,255, 255, 255)
  static TextStyle ffffffff(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ffffffff);
  }

  static TextStyle ff000000(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ff000000);
  }

  static TextStyle ff666666(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ff666666);
  }

  //ffDDA815_ = ARGB(225,255, 255, 255)
  static TextStyle ffcf352e(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ffcf352e);
  }

  static TextStyle ffff9211_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ffff9211);
  }

  //ffDDA815_ = ARGB(255,92, 88, 105)
  static TextStyle ff5C5869(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ff5C5869);
  }

//
  static TextStyle fff7dB88(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.fff7dB88);
  }

  static TextStyle fffcc67e(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.fffcc67e);
  }

  static TextStyle ff4caf50(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    final appThemeColors = Theme.of(context).extension<AppColors>();
    return TextStyle(
      fontSize: 1.0.sp < 1.h ? fontSize * 1.sp : fontSize * 1.h,
      color: AppColors.ff4caf50,
      fontWeight: fontWeight,
    );
  }

  static TextStyle fff7b635_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    final appThemeColors = Theme.of(context).extension<AppColors>();
    return TextStyle(
      fontSize: 1.0.sp < 1.h ? fontSize * 1.sp : fontSize * 1.h,
      color: AppColors.fff7b635,
      fontWeight: fontWeight,
    );
  }

  static TextStyle bodyText1(BuildContext context, {int fontSize = 16}) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return normalTextStyle(
        fontSize: fontSize, color: appThemeColors?.onBackground);
  }

  static TextStyle bodyText2(BuildContext context, {int fontSize = 14}) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return normalTextStyle(
        fontSize: fontSize, color: appThemeColors?.onBackground);
  }

  static TextStyle bodyText3(BuildContext context, {int fontSize = 14}) {
    final appThemeColors = Theme.of(context).extension<AppThemeColors>();
    return normalTextStyle(
        fontSize: fontSize, color: appThemeColors?.onBackground);
  }

  // Add more text styles as needed

  // Add more text styles as needed

  // Add more text styles as needed

  //ffC5CBC6 = ARGB(255, 218, 164, 16)
  static TextStyle ffC5CBC6_(BuildContext context, {int fontSize = 14}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ffC5CBC6);
  }

  static TextStyle surface_(BuildContext context,
      {int fontSize = 14, FontWeight fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.surface);
  }

  static TextStyle error_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.error);
  }
  // Add more text styles as needed

  //ffbfa56d = ARGB(255, 191,165,109)
  static TextStyle ffbfa56d_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ffbfa56d);
  }

  static TextStyle ff000000_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
        fontSize: fontSize * 1.0.sp,
        color: AppColors.ff000000,
        fontWeight: fontWeight);
  }

  //ffbfa56d = ARGB(255, 117,117,117)
  static TextStyle ff757575(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ff757575);
  }

  //ff5C5869_bold = ARGB(255, 191,165,109)
  static TextStyle ff5C5869_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(
        fontSize: fontSize, fontWeight: fontWeight, color: AppColors.ff5C5869);
  }

  //ffDDA815_ = ARGB(255, 221, 168, 21)
  static TextStyle ffDDA815_(BuildContext context, {int fontSize = 14}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ffDDA815);
  }

  //ff8D8B8B = ARGB(255, 141, 139, 139)
  static TextStyle ff8D8B8B_(BuildContext context, {int fontSize = 14}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ff8D8B8B);
  }

  //ff252525 = ARGB(255, 37, 37, 37)
  static TextStyle ff252525_(BuildContext context, {int fontSize = 14}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ff252525);
  }

  //ff535B70 = ARGB(255, 83, 91, 112)
  static TextStyle ff535B70_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ff535B70);
  }

  //ff535B70 = ARGB(255, 91, 164, 109)
  static TextStyle ffbfa46d_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ffbfa46d);
  }

  //ff535B70 = ARGB(255, 190, 135, 22)
  static TextStyle ffbe8716_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ffbe8716);
  }

  //ff535B70 = ARGB(255, 209, 207, 208)
  static TextStyle ffd1cfd0_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ffd1cfd0);
  }

  //ff535B70 = ARGB(255, 255, 0, 0)
  static TextStyle ffff0000_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ffff0000);
  }

  static TextStyle ff969696(
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return normalTextStyle(fontSize: fontSize, color: AppColors.ff969696);
  }

  //ff535B70 = ARGB(255, 25 , 118, 210)
  static TextStyle ff1976D2_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff1976D2,
    );
  }

  static TextStyle ffBFA46D_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ffBFA46D,
    );
  }

  static TextStyle ff999999_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff999999,
    );
  }

  static TextStyle ffE44848_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ffE44848,
    );
  }

  static TextStyle ff858585_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff858585,
    );
  }

  static TextStyle ff666666_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff666666,
    );
  }

  static TextStyle ff387ef5_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff387ef5,
    );
  }

  static TextStyle textColor3_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.textColor3,
    );
  }

  static TextStyle ff17c492_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff17c492,
    );
  }

  static TextStyle ff4e57dd_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff4e57dd,
    );
  }

  static TextStyle ff3490D3_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ff3490D3,
    );
  }

  static TextStyle ffE68131_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ffE68131,
    );
  }

  static TextStyle ffcd9f4b_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ffcd9f4b,
    );
  }

  static TextStyle ffffff8c_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ffffff8c,
    );
  }

  static TextStyle ffFFF26F_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ffFFF26F,
    );
  }

  static TextStyle ffE27219_(BuildContext context,
      {int fontSize = 14, fontWeight = FontWeight.normal}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      color: AppColors.ffE27219,
    );
  }

  static TextStyle ffA50606_(BuildContext context,
      {int fontSize = 20, fontWeight = FontWeight.bold}) {
    return TextStyle(
      fontSize: fontSize * 1.0.sp,
      fontWeight: fontWeight,
      color: AppColors.ffA50606,
    );
  }
}
