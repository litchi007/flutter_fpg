import 'dart:async';
import 'dart:convert' as cover;
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:fconsole/fconsole.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/api/error_report/error_report.dart';
import 'package:fpg_flutter/configs/app_config.dart';
import 'package:fpg_flutter/constants/data_factories.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/http/UGEncryptParams.dart';
import 'package:fpg_flutter/models/error_report/error_report.dart';
import 'package:fpg_flutter/models/response_model.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/crypto_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:get/get.dart' hide Response;

// ignore: depend_on_referenced_packages
import 'package:path_provider/path_provider.dart';

// import 'log_util.dart';
// import 'package_util.dart';

enum FetchType { head, get, post, put, patch, delete }

enum CodeType {
  success,
  fail,
}

bool isTest = true;

final cache = <String, bool>{};

class HttpUtil {
  const HttpUtil._();

  static const String _tag = '🌐 HttpUtil';
  static bool isLogging = true;

  static final Dio dio = Dio()
    ..options = dioBaseOptions
    ..interceptors.addAll(dioInterceptors);

  static BaseOptions get dioBaseOptions {
    return BaseOptions(
      baseUrl: AppConfig.httpUrl,
      connectTimeout: AppConfig.connectTimeout.milliseconds,
      sendTimeout: AppConfig.connectTimeout.milliseconds,
      receiveTimeout: AppConfig.connectTimeout.milliseconds,
      receiveDataWhenStatusError: true,
    );
  }

  static List<Interceptor> get dioInterceptors => <Interceptor>[
        _interceptor,
        // PrettyDioLogger(
        //   requestHeader: true,
        //   requestBody: true,
        //   responseHeader: true,
        // ),
        // _retry
      ];

  static ResponseModel<T> _successModel<T extends DataModel>() =>
      ResponseModel<T>.succeed();

  static ResponseModel<T> _failModel<T extends DataModel>(String message) =>
      ResponseModel<T>.failed(
        msg: '${ResponseModel.errorInternalRequest}: $message',
      );

  // static ResponseModel<T> _cancelledModel<T extends DataModel>(
  //   String message,
  //   String url,
  // ) =>
  //     ResponseModel<T>.cancelled(msg: '$message, $url');

  static Future<ResponseModel<T>> fetch<T>(
    FetchType fetchType, {
    required String url,
    Map<String, String>? queryParameters,
    Object? body,
    Json? headers,
    ResponseType? responseType,
    CancelToken? cancelToken,
  }) async {
    final Response<Json> response = await getResponse(
      fetchType,
      url: url,
      queryParameters: queryParameters,
      body: body,
      headers: headers,
      responseType: responseType,
      cancelToken: cancelToken,
    );
    final res = ResponseModel<T>(
      code: response.data?['code'] ?? 1,
      msg: response.data?['msg'] ?? '_Http Error',
      data: dataModelFactories[T]!(response.data?['data']) as T,
      timestamp: response.data?['timestamp'] ?? 0,
    );

    return res;
  }

  static Future<ResponseModel<T>> fetchModel<T>(FetchType fetchType,
      {required String url,
      Map<String, dynamic>? queryParameters,
      Object? body,
      Json? headers,
      ResponseType responseType = ResponseType.json,
      CancelToken? cancelToken,
      String? baseUrl,
      bool Function(Json json)? modelFilter,
      bool toastError = true}) async {
    // _log(body);
    // return;

    Response<Json>? response;
    try {
      response = await getResponse(fetchType,
          url: url,
          queryParameters: queryParameters,
          body: body,
          headers: headers,
          responseType: responseType,
          cancelToken: cancelToken,
          baseUrl: baseUrl);
      // LogUtil.w("response_____1 $response");
    } catch (e) {
      ErrorReportHttp.report(
          ErrorReport(err: e, text: "http请求报错", page: Get.currentRoute));
      LogUtil.e("response_____2 $e");
    }

    final Json? resBody = response?.data;
    if (resBody?.isNotEmpty ?? false) {
      final ResponseModel<T> model = ResponseModel<T>.fromJson(resBody!,
          modelFilter: modelFilter, datas: response?.data);
      if (!model.isSucceed) {
        // ToastUtil.clear();
        _log('Response is not succeed: ${model.msg} ${model.code}');
        switch (model.code) {
          case 100:
            LogUtil.w('not login');
            // ToastUtils.show(model.msg);
            break;
          case 502:
            SmartDialog.showToast('服务器错误');
            break;
          default:
            // if (toastError && response.statusCode != ErrCode.CODE_KICK_OUT) {
            //   XDToast.text(text: model.msg, onlyOne: false);
            // }
            break;
        }
        // ToastUtil.text(model.msg);
      } else {}
      // _log('Response model: ${model.data}');
      return model;
    } else {
      return _handleStatusCode(response);
    }
  }

  static Future<String> getFilenameFromResponse(
    Response<dynamic> res,
    String url,
  ) async {
    String? filename = res.headers
        .value('content-disposition')
        ?.split('; ')
        .where((String element) => element.contains('filename'))
        .first;
    if (filename != null) {
      final RegExp filenameReg = RegExp(r'filename="(.+)"');
      filename = filenameReg.allMatches(filename).first.group(1);
      filename = Uri.decodeComponent(filename!);
    } else {
      filename = url.split('/').last.split('?').first;
    }
    return filename;
  }

  /// Return the saving path if succeed.
  static Future<String> download(
    String url, {
    String? filename,
    Map<String, dynamic>? data,
    Map<String, dynamic>? headers,
    ProgressCallback? progressCallback,
    CancelToken? cancelToken,
    Options? options,
    bool deleteIfExist = false,
    bool openAfterDownloaded = false,
  }) async {
    final Completer<String> completer = Completer<String>();
    try {
      final String tempPath = (await getTemporaryDirectory()).path;
      cancelToken ??= CancelToken();
      late final String filename;
      late final String filePath;
      late int totalContentLength;
      String progress = '';
      final Stopwatch stopwatch = Stopwatch()..start();
      final downloadDio = Dio(BaseOptions(baseUrl: ""));
      // final isHave = await db?.videoDao.findVideoByName(url.encode());
      filename = filenameFromUrl(Headers(), url);
      filePath = '$tempPath/$filename';
      final File file = File(filePath);
      LogUtil.w(
          "existsSync__ ${file.existsSync()} ${file.path} $deleteIfExist");
      if (file.existsSync()) {
        completer.complete(filePath);
        return completer.future;
      } else {
        // LogUtil.w("isHave____$isHave");
        await downloadDio.download(
          url,
          (Headers headers) {
            // if (file.existsSync() && deleteIfExist) {
            //   _log('Deleting existing download file: $filePath');
            //   file.deleteSync();
            // }
            if (file.existsSync()) {
              if (openAfterDownloaded) {
                _log('File exist in $filePath, opening...');
                // _openFile(filePath);
              } else {
                _log('File exist in $filePath.');
              }
              completer.complete(filePath);
              cancelToken!.cancel();
              stopwatch.stop();
              return filePath;
            }
            _log(
              'File start download:\n'
              '[URL ]: $url\n'
              '[PATH]: $filePath',
            );
            return filePath;
          },
          // data: data,
          // options: Options(headers: headers),
          cancelToken: cancelToken,
          onReceiveProgress: (int count, int total) {
            totalContentLength = total;
            final String newProgress;
            if (total == -1) {
              newProgress = count.fileSizeFromBytes;
            } else {
              newProgress = (count / total).toStringAsFixed(2);
            }
            if (newProgress != progress) {
              _log('File download progress: $newProgress ($filename)');
              progress = newProgress;
            }

            progressCallback?.call(count, total);
          },
        );
        stopwatch.stop();
        final StringBuffer sb = StringBuffer(
          'File downloaded:\n'
          '[URL  ]: $url\n'
          '[PATH ]: $filePath',
        );
        if (totalContentLength != -1) {
          final int speed = totalContentLength ~/ stopwatch.elapsed.inSeconds;
          sb.write('\n[SPEED]: ${speed.fileSizeFromBytes}/s');
        }
        _log(sb.toString());
        if (openAfterDownloaded) {
          // _openFile(filePath);
        }
        completer.complete(filePath);
        LogUtil.w("completer____$url $filePath");
      }

      // await db?.videoDao.insertVideo(VideoDb( url.encode(), filePath, null));
    } on DioException catch (e, s) {
      if (e.type != DioExceptionType.cancel) {
        completer.completeError(e, s);
      }
    } catch (e, s) {
      _log(
        'File download failed: $e\n'
        '[URL ]: $url\n',
        stackTrace: s,
        isError: true,
      );
      completer.completeError(e, s);
    }
    return completer.future;
  }

  static String filenameFromUrl(Headers headers, String url) {
    final RegExp filenameReg = RegExp(r'filename\*?=(.*)');
    final List<String>? validHeaders = headers
        .value('content-disposition')
        ?.split(';')
        .where((String e) => filenameReg.hasMatch(e.trim()))
        .map((String e) => e.trim())
        .toList();
    if (validHeaders == null || validHeaders.isEmpty) {
      return url.split('/').last.split('?').first;
    }
    if (validHeaders.any((String h) => h.startsWith('filename*='))) {
      final String header = validHeaders.singleWhere(
        (String h) => h.startsWith('filename*='),
      );
      return Uri.decodeComponent(
        header
            .removeFirst('filename*=')
            .removeFirst('utf-8')
            .removeFirst('UTF-8')
            .removeFirst("''"),
      );
    }
    return Uri.decodeComponent(validHeaders.first.removeAll('filename='));
  }

  // static Future<OpenResult?> _openFile(String path) async {
  //   try {
  //     _log('Opening file $path...');
  //     final OpenResult result = await OpenFile.open(path);
  //     return result;
  //   } catch (e, s) {
  //     _log('Error when opening file [$path]: $e', stackTrace: s, isError: true);
  //     return null;
  //   }
  // }

  static ResponseModel<T> _handleStatusCode<T extends DataModel>(
    Response<dynamic>? response,
  ) {
    final int statusCode = response?.statusCode ?? 0;
    _log('Response status code: ${response?.statusCode}');
    if (statusCode >= 200 && statusCode < 300) {
      _log('Response code success: $statusCode');
      return _successModel();
    } else if (statusCode >= 300 && statusCode < 400) {
      _log('Response code moved: $statusCode');
      return _successModel();
    } else if (statusCode >= 400 && statusCode < 500) {
      final String message = 'Response code client error: $statusCode';
      _log(message);
      return _failModel(message);
    } else if (statusCode >= 500) {
      final String message = 'Response code server error: $statusCode';
      _log(message);
      return _failModel(message);
    } else {
      final String message = 'Response code unknown status: $statusCode';
      _log(message);
      return _failModel(message);
    }
  }

  static Future<Response<T>> getResponse<T>(
    FetchType fetchType, {
    required String url,
    String? baseUrl,
    Map<String, dynamic>? queryParameters,
    Object? body,
    Json? headers,
    ResponseType? responseType = ResponseType.json,
    CancelToken? cancelToken,
    bool Function(Json json)? modelFilter,
  }) async {
    // if (!url.startsWith('http(s?)://')) {
    //   url = 'https://$url';
    // }
    // LogUtil.w(body?['fileds']);
    // 本地获取 token，塞入请求头
    // final _user = Storage.read<AccountLoginRes>(StoreName.userMsg);
    headers ??= <String, String?>{
      // "token": GlobalService.to.userDetail.value.apiToken
    };
    // final res = await CryptoUtils.addCryptKey();
    // res.forEach((key, value) {
    //   headers?[key] = "$value";
    // });
    // headers['debug'] = "true";
    // headers['key'] =
    //     "oPHCStT71O9Lfmom6zueOhK1nqb5Bxn+xwh6bkxOpqIJTD6Q9bjW2Gm3SUd2gq0bBrFx3IsTrtMcRICHe2aXggQQvF0x9Wdd4zodzNZ0+uDpekgoaEg96yeCJ6z2ZPI6tNY8VDK5fOE2BeTbKE2VP09aWNzVSThrNdKMFfx0U8c=";
    // headers['value'] =
    //     "5891CB8B203AF7ADB5E80970CE5D404C34C4A90B8FBB854630C92160E2FC963F04B54F96D39B8DF3FC157FB031D7B47648FB46262B1C494BF9C4FB5522762A1657A17F110A705D6370B7ACB6BD2DA134CDF42D3B5042DBD55114928BCF6AC28CFA0D07C42E6434B276FC4EA31A5232363181B3F6112AF3E1335A0276DE368CC18ED209FD744E2D58CA84E4BD76294D30664BB5BDDFA32FEC79D43F3797F9142EE37BD3AEE9DFB9EBB109D1F0BB0B95B4DC53621FE6A32A758B0689B93BD35F5BB81E45B38E116E8BA0B2394382229ADEADD6D075F61843663CA50E09C7479D716CF0DD03D4F76145340AA61B1639705D3D661C3C97A44A8FAA9BF792B9EF0374F41C321F400B7F3EE06C2983F094BF2F0274EC3AE87BF4F08503452FDD2FEE842F26578CEAF140F4171BD741120A45C6A9880AD38E89D62D0452A337E747C402EC2A48EA8F0D6A474D6CFD4E313870EE33E7EDE622AF0BC392A2E128A77A95B8944741A3DE26CC36CB714330AB124270342ADA49563B44FBB1FF579683DAC856DCFF966E1F600F24F2DD01783676BF3D";

    // Recreate <String, String> headers and queryParameters.
    // final Json effectiveHeaders = headers.map<String, dynamic>(
    //   (String key, dynamic value) =>
    //       MapEntry<String, dynamic>(key, value.toString()),
    // );
    // if (effectiveHeaders.isNotEmpty) {
    //   // _log('$fetchType headers: $effectiveHeaders');
    // }
    // queryParameters?['checkSign'] = 1;
    // final signParams = encryptParams(queryParameters ?? {});
    Map<String, dynamic>? signParams = queryParameters ?? {};
    if (queryParameters != null) {
      final cryptoData = CryptoUtil.encryptRsa(queryParameters);
      signParams = cryptoData;
      LogUtil.w("cryptoData__ $cryptoData");
    }
    signParams.forEach((key, value) {
      if (value != null) {
        url += "&$key=$value";
      }
    });

    // url += "&checkSign=1";
    if (body != null) {
      body = CryptoUtil.encryptRsa(body as Map<String, dynamic>);
    }
    // url += '&checkSign=1';
    // encryptParams

    Uri replacedUri = Uri.parse(url);
    LogUtil.w("replacedUri__ $url $queryParameters");
    Response<T>? response;
    // final _currentPath =
    //     "${AppConfig.httpUrl}${Uri.decodeComponent(replacedUri.path)}?${(queryParameters ?? {}).mapToString()}";
//     LogUtil.i("""
// cache___ url: ${AppConfig.httpUrl}${Uri.decodeComponent(replacedUri.path)}?${(queryParameters ?? {}).mapToString()}
// caches ${cache['${AppConfig.httpUrl}${Uri.decodeComponent(replacedUri.path)}?$queryParameters'] != null}
// $cache
// """);
    // if (cache[_currentPath] != null) {
    //   response?.data = null;
    //   return response!;
    // }
    // _log(
    //     "uri parse ===== ${AppConfig.httpUrl}${Uri.decodeComponent(replacedUri.path)}$queryParameters ======");
    // if (replacedUri.path.contains(UserOnlineUrl.getUsersOnlineStatus)) {
    //   replacedUri = Uri.parse("${Config.baseUrl}:10000$replacedUri") ;
    // }

    _log("assurers $replacedUri");
    final Options options = Options(
      followRedirects: true,
      headers: headers,
      receiveDataWhenStatusError: true,
      responseType: responseType,
    );
    try {
      Dio? _dio = Dio(HttpUtil.dioBaseOptions.copyWith(
          headers: headers,
          baseUrl: baseUrl ?? HttpUtil.dioBaseOptions.baseUrl))
        ..interceptors.addAll(HttpUtil.dioInterceptors);

      switch (fetchType) {
        case FetchType.head:
          response = await _dio.headUri(
            replacedUri,
            data: body,
            options: options,
            cancelToken: cancelToken,
          );
          break;
        case FetchType.get:
          response = await _dio.getUri(
            replacedUri,
            options: options,
            cancelToken: cancelToken,
          );
          break;
        case FetchType.post:
          response = await _dio.postUri(
            replacedUri,
            data: body,
            options: options,
            cancelToken: cancelToken,
          );
          // response = await dio.post("${replacedUri.path}?${replacedUri.query}", options: options, cancelToken: cancelToken);
          break;
        case FetchType.put:
          response = await _dio.putUri(
            replacedUri,
            data: body,
            options: options,
            cancelToken: cancelToken,
          );
          break;
        case FetchType.patch:
          response = await _dio.patchUri(
            replacedUri,
            data: body,
            options: options,
            cancelToken: cancelToken,
          );
          break;
        case FetchType.delete:
          response = await _dio.deleteUri(
            replacedUri,
            data: body,
            options: options,
            cancelToken: cancelToken,
          );
          break;
      }
      // _dio.close();
      // _dio = null;
    } catch (e) {
      LogUtil.e(e);
    }
    // print("response_____1 $response");

    // LogUtil.i("respone_____$response");
    if (response?.data == '') {
      response?.data = null;
    }
    return response!;
  }

  static void _log(
    Object? message, {
    StackTrace? stackTrace,
    bool isError = false,
  }) {
    if (isLogging) {
      if (isError) {
        LogUtil.e(message, stackTrace: stackTrace, tag: _tag);
      } else {
        LogUtil.d(message, stackTrace: stackTrace, tag: _tag);
      }
    }
  }

  static QueuedInterceptorsWrapper get _interceptor {
    return QueuedInterceptorsWrapper(
      onRequest: (RequestOptions options, RequestInterceptorHandler handler) {
        // 发出的请求回进入缓存，避免重复请求
        cache["${options.baseUrl}${Uri.decodeComponent(options.path)}"] = true;

        _log('Fetching(${options.method}) url: ${options.queryParameters}');
        // TODO:先检查 token 是否过期，如果过期了就先无感刷新 token
        if (options.path.contains(".txt")) {
          options = options.copyWith(baseUrl: "", contentType: "text/html");
        }
        // 下载baseUrl设置空
        if ([".mp4", ".m3u8"].contains(options.path)) {
          options = options.copyWith(baseUrl: "");
        }
        var _logger = FlowLog.ofNameAndId(
          options.path,
          id: '${options.hashCode}',
        );
        _logger.log('开始请求 ${options.method}');
        _logger.log('data:${options.data}');
        _logger.log('params:${options.queryParameters}');
        _logger.log('header:${options.headers.toString()}');
//         _log("""\n
// 请求路径 request：${options.baseUrl}${options.path}
// 请求方式：${options.method}
// 请求 body 参数 (加密前)：${options.queryParameters}
// 请求 params 参数 (加密前)：${options.data}
// 请求头：${options.headers}
// \n
//         """);
        handler.next(options);
      },
      onResponse:
          (Response<dynamic> res, ResponseInterceptorHandler handler) async {
        // 请求成功后删除缓存
        cache.remove(
            "${res.requestOptions.baseUrl}${Uri.decodeComponent(res.requestOptions.path)}");
        // _log(
        //   'Got response from: ${res.requestOptions.uri} ${res.statusCode}',
        // );
        // _log('请求结果：${res.data}');
        final _key = (res.headers['key']?[0] ?? "");
        var _logger = FlowLog.ofNameAndId(
          res.requestOptions.path,
          id: '${res.requestOptions.hashCode}',
        );
        _logger.log('请求结束');
        _logger.log('Http Status Code:${res.statusCode}');
        _logger.log('Response Data:\n${res.data}');
        _logger.end();
        if (res.requestOptions.path.contains(".txt")) {
          /// 解析图片
          // final __data = CryptoUtils.decryptKeyAndResp(keyEncrypt: _key, respData: res.data);
          // final String base64Encode = String.fromCharCodes(__data);
          // final Map<String, String> map =
          //     cover.jsonDecode(base64Encode) as Map<String, String>;
          // LogUtil.w("解析数据源：$__data");
          // res.data = await CryptoUtils.decodeImg(data: res.data);
          // handler.resolve(res);
          // return;
        }
        final encryptoData = res.data;
        dynamic resolvedData = encryptoData;
        LogUtil.w("""\n
请求路径 ${res.requestOptions.baseUrl}${Uri.decodeComponent(res.requestOptions.path)}
请求方式：${res.requestOptions.method}
请求 body 参数 (加密前)：${res.requestOptions.queryParameters}
请求 params 参数 (加密前)：${cover.jsonEncode(res.requestOptions.data)}
请求结果： ${cover.jsonEncode(resolvedData)}
\n
        """);
// 请求结果： ${cover.jsonEncode(resolvedData)}

        // data 转换 json 结构：
        // ${cover.jsonEncode(encryptoData)}
        // TODO:测试代码
        // if (res.requestOptions.path.contains("user/resources") && isTest) {
        //   isTest = false;
        //   return await refreshToken(response: res, handler: handler);
        // }
        // refreshToken(response: res, handler: handler);
        LogUtil.w("resolvedData___${[
          HttpStatus.lengthRequired,
          HttpStatus.preconditionFailed
        ].contains(resolvedData["code"])}");
        // 411，412 刷新 token
        if ([HttpStatus.lengthRequired, HttpStatus.preconditionFailed]
            .contains(resolvedData["code"])) {
          // await refreshToken(response: res, handler: handler);
          // handler.resolve(res);
          // ToastUtils.closeAll();
          return;
        }
        LogUtil.w("当前路由： ${Get.currentRoute}");
        if (resolvedData['code'] == 403 && Get.currentRoute != AppRoutes.home) {
          Get.toNamed(AppRoutes.login);
        }

        /// 413 refresh token 过期 清理用户数据
        if ([HttpStatus.forbidden].contains(resolvedData["code"])) {
          if (GlobalService.to.token.isNotEmpty) {
            // GlobalService.to.change(value)
            // ToastUtils.confirm(CustomConfirmParams(
            //     content: "登陆过期，请重新登陆",
            //     backDismiss: false,
            //     clickMaskDismiss: false,
            //     cancel: () {},
            //     submit: () {}));
          }
          // UserSaveAccountModel res = (const UserSaveAccountModel())
          //     .copyWith(user: const User(), token: const Token());

          // GlobalService.to.changeUserMsg(res);
        }
        // 报错了，显示错误提示
        if (resolvedData["code"] == HttpStatus.continue_) {
          // ToastUtils.show(resolvedData["msg"] ?? "");
        }
        if (res.statusCode == HttpStatus.noContent) {
          resolvedData = _successModel().toJson();
        } else {
          // final dynamic data = res.data;
          // if (data is String) {
          //   try {
          //     // If we do want a JSON all the time, DO try to decode the data.
          //     resolvedData = jsonDecode(data) as Json?;
          //   } catch (e) {
          //     resolvedData = data;
          //   }
          // } else {
          //   resolvedData = data;
          // }
        }
        res.data = resolvedData;

        handler.resolve(res);
      },
      onError: (DioException e, ErrorInterceptorHandler handler) async {
        print("请求失败 $e");
        // ToastUtils.show("${e.response}");
        // 请求失败，清除所有缓存
        cache.clear();
        LogUtil.e(e);
        if (e.type == DioExceptionType.cancel) {
          handler.resolve(e.response!);
          return;
        }
        if (e.type == DioExceptionType.unknown) {
          handler.resolve(Response(
              requestOptions: e.requestOptions,
              data: _failModel(e.message ?? '请求错误').toJson()));
          return;
        }
        switch (e.response?.statusCode) {
          case 401:
            LogUtil.w('未登陆');

            break;
        }
        if (e.type == DioExceptionType.badResponse) {
          // Alert.showConfirmDialog(I18n.accountKickOutTip, () {
          //   MeController.to.logout();
          // });
        }
        e.response!.data ??= _failModel(e.message ?? '请求错误').toJson();

        if (kDebugMode) {
          _log(e, stackTrace: e.stackTrace, isError: true);
        }
        final bool isConnectionTimeout =
            e.type == DioExceptionType.connectionTimeout;
        final bool isStatusError = e.response != null &&
            e.response!.statusCode != null &&
            e.response!.statusCode! >= HttpStatus.internalServerError;
        if (!isConnectionTimeout && isStatusError) {
          _log(
            'Error when requesting ${e.requestOptions.uri}\n$e\n'
            'Raw response data: ${e.response?.data}',
            isError: true,
          );
        }
        if (e.response?.data is String) {
          e.response!.data = _failModel(e.response!.data! as String).toJson();
        }
        e.response!.data ??= _failModel(e.message ?? '').toJson();
        handler.resolve(e.response!);
      },
    );
  }

  static addHeader() async {
    final headers = <String, String?>{};
    // final res = await CryptoUtils.addCryptKey();
    // res.forEach((key, value) {
    //   headers[key] = "$value";
    // });
    return headers;
  }
}

// refreshToken({
//   required Response<dynamic> response,
//   required ResponseInterceptorHandler handler,
// }) async {
//   final _refreshToken = GlobalService.to.userMsg.value.token?.refreshToken;
//   // GlobalService.to.userMsg.value.token!.accessToken! = null;
//   final newToken = Token(
//     tokenType: null,
//     accessToken: null,
//     refreshToken: _refreshToken,
//     accessTokenExpire: GlobalService.to.userMsg.value.token?.accessTokenExpire,
//     refreshTokenExpire:
//         GlobalService.to.userMsg.value.token?.refreshTokenExpire,
//   );
//   // final cleanToken = GlobalService.to.userMsg.value.
//   GlobalService.to.changeUserMsg(
//       GlobalService.to.userMsg.value.copyWith(token: newToken), false);
//   try {
//     // final headers = <String, String?>{};
//     // final cryptoKey = await CryptoUtils.addCryptKey();
//     // cryptoKey.forEach((key, value) {
//     //   headers[key] = "$value";
//     // });
//     // final headers = await HttpUtil.addHeader();
//     // final dio = Dio(HttpUtil.dioBaseOptions.copyWith(
//     //   headers: headers
//     // ))..interceptors.addAll(HttpUtil.dioInterceptors);
//     // final params = """params={"refresh_token": "$_refreshToken"}""";
//     // final res1 =
//     //     await dio.post("${ApiName.ApiV3ToolsAccess_token}?$params");
//     // LogUtil.w("refresh_token1111 $res1");
//     // final res = ToolsAccessTokenModel.fromJson(res1.data['data']) ;
//     final res = await GlobalHttp.toolsAccessToken(_refreshToken ?? "");
//     // LogUtil.w("刷新 token $res");
//     final token = GlobalService.to.userMsg.value.token?.copyWith(
//       tokenType: res.data?.tokenType,
//       accessToken: res.data?.accessToken,
//       accessTokenExpire: res.data?.accessTokenExpire,
//     );
//     final userMsg = GlobalService.to.userMsg.value.copyWith(token: token);
//     GlobalService.to.changeUserMsg(userMsg);
//     FetchType type = FetchType.post;
//     switch (response.requestOptions.method) {
//       case "GET":
//         type = FetchType.get;
//         break;
//       case "POST":
//         type = FetchType.post;
//         break;
//       default:
//     }
//     final options = response.requestOptions;
//     final retryResponse = await HttpUtil.getResponse(type,
//         url: "${options.baseUrl}${options.path}");
//     handler.resolve(retryResponse);
//   } catch (e) {
//     LogUtil.e("报错了____$e");
//   }

// }

/// 获取基础信息
Future<Response<String?>> fetchData(String url) {
  return Dio().get(url);
}
