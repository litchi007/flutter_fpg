import 'dart:convert';

import 'package:fpg_flutter/configs/favcor.dart';
import 'package:fpg_flutter/extension/num_ext.dart';
import 'package:fpg_flutter/http/UGEncryptParams.dart';
import 'package:fpg_flutter/page/longDragonTool/components/long_dragon_item.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:pointycastle/export.dart' as pointycastle;
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:crypto_dart/tripledes.dart' as tripledes;
import 'package:crypto_dart/crypto_dart.dart' as crypto_dart;

class CryptoUtil {
  /// aes
  static final encrypt.Key _keyAes =
      encrypt.Key.fromUtf8('4c43c361235a4ac05b91eb5fa95');
  static encrypt.Encrypter? _encryptAes;

  /// rsa
  static pointycastle.RSAPublicKey? _publicKey;
  static encrypt.Encrypter? _encryptRsa;
  static encrypt.Signer? _signer;
  static pointycastle.RSAEngine? pointycastleRsa;
  static String? lastDesKey;

  /// Rsa 加密最大长度
  static const int MAX_ENCRYPT_BLOCK = 117;

  /// Rsa 解密最大长度
  static const int MAX_DECRYPT_BLOCK = 128;

  ///初始化 AES 加密启动时调用
  static Future<void> initAes({
    encrypt.AESMode mode = encrypt.AESMode.ecb,
    String padding = 'PKCS7',
  }) async {
    _encryptAes =
        encrypt.Encrypter(encrypt.AES(_keyAes, mode: mode, padding: padding));
  }

  /// 初始化rsa
  static initRsa() {
    _publicKey =
        encrypt.RSAKeyParser().parse(pemString) as pointycastle.RSAPublicKey;
    // pointycastleRsa = encrypt.Encrypter(encrypt.RSA(publicKey: _publicKey));

    _encryptRsa = encrypt.Encrypter(
      encrypt.RSA(publicKey: _publicKey),
    );
    // _signer = encrypt.Signer(
    //     encrypt.RSASigner(encrypt.RSASignDigest.SHA256, publicKey: _publicKey));
  }

  static String encrypt3DES_ECB(String datas, String keys) {
    try {
      final res = tripledes.TripleDES();
      final key = encrypt.Key.fromUtf8(keys.substring(0, 24));
      print("key________$key");
      final data = res
          .encrypt(datas, key.bytes,
              options: const crypto_dart.CipherOptions(
                  mode: crypto_dart.Mode.ECB,
                  padding: crypto_dart.Padding.PKCS7))
          .toString();
      return data;
    } catch (e) {
      LogUtil.e(e);
      LogUtil.w("数据11 $datas $keys");
      return "";
    }
  }

  ///公钥 Rsa 加密
  static String encryptRsa1(String context) {
    // 原始字符串转成字节数组
    final List<int> sourceBytes = utf8.encode(context);
    // 数据长度
    final int inputLen = sourceBytes.length;
    // 缓存数组
    final List<int> cache = <int>[];
    // 分段加密 步长为 MAX_ENCRYPT_BLOCK
    for (int i = 0; i < inputLen; i += MAX_ENCRYPT_BLOCK) {
      // 剩余长度
      final int endLen = inputLen - i;
      List<int> item;
      if (endLen > MAX_ENCRYPT_BLOCK) {
        item = sourceBytes.sublist(i, i + MAX_ENCRYPT_BLOCK);
      } else {
        item = sourceBytes.sublist(i, i + endLen);
      }
      // 加密后对象转换成数组存放到缓存
      cache.addAll(_encryptRsa?.encryptBytes(item).bytes ?? []);
    }
    // 加密后数组转换成 base64 编码并返回
    final String en = base64.encode(cache);
    return en;
  }

  ///公钥 Rsa 加密
  /// **example**
  ///
  static Map<String, dynamic> encryptRsa(Map<String, dynamic> context) {
    if (context.isEmpty) {
      return context;
    }
    if (Favcor.currentName == "t002") {
      return context;
    }

    /// 1.生成随机密钥
    final randomKey = generateRandomKey();
    lastDesKey = generateRandomKey();
    /// 生成sign签名
    final sign = encryptRsa1("iOS_${lastDesKey ?? randomKey}");

    /// 2.rsa加密
    // _encryptRsa?.encrypt(context);
    final res = {}.cast<String, String>();
    
    context.forEach((key, v) {
      if (v != null ) {
        if (v.toString().isNotEmpty) {
          final data = encrypt3DES_ECB(v.toString(), lastDesKey ?? randomKey);
          if (v.toString() == "70") {
            print("加密11111$data");
          }
          res[key] = data;
        }
        
      }
    });
    res['checkSign'] = "1";
    // if (res['token'] != null) {
    //   res['token'] = context['token'];
    // }
    res['sign'] = sign;
    LogUtil.w("___sign: ${jsonEncode(res)} $randomKey ");

    return res;
    // // 原始字符串转成字节数组
    // final List<int> sourceBytes = utf8.encode(context);
    // // 数据长度
    // final int inputLen = sourceBytes.length;
    // // 缓存数组
    // final List<int> cache = <int>[];
    // // 分段加密 步长为 MAX_ENCRYPT_BLOCK
    // for (int i = 0; i < inputLen; i += MAX_ENCRYPT_BLOCK) {
    //   // 剩余长度
    //   final int endLen = inputLen - i;
    //   List<int> item;
    //   if (endLen > MAX_ENCRYPT_BLOCK) {
    //     item = sourceBytes.sublist(i, i + MAX_ENCRYPT_BLOCK);
    //   } else {
    //     item = sourceBytes.sublist(i, i + endLen);
    //   }
    //   // 加密后对象转换成数组存放到缓存
    //   cache.addAll(_encryptRsa?.encryptBytes(item).bytes ?? []);
    // }
    // // 加密后数组转换成 base64 编码并返回
    // final String en = base64.encode(cache);
    // return en;
  }
}
