import 'package:get_storage/get_storage.dart';

final class StorageKeys {
  static String lang = "lang";
  static String theme = "theme";
  static String token = "token";
  static String publicKey = "publicKey";
  static String user = "user";
  static String performanceStatus = "performanceStatus";

  static const String USERTOKEN = 'user_token';
  static const String USERINFO = 'user_info';
  static const String REMEMBER_DATA = 'remember_data';
  static const String USERNAME = 'user_name';
  static const String USERPWD = 'user_pwd';
  static const String ACCESSTOKEN = 'accessToken';
  static const String RIGHTMENU = 'right_menu';
}

///  [StorageUtils] 本地存储单例
class StorageUtils {
  static StorageUtils? _instance;

  factory StorageUtils() {
    _instance ??= StorageUtils._internal();
    return _instance!;
  }

  StorageUtils._internal();

  final GetStorage box = GetStorage();

  Future<bool> init() {
    return GetStorage.init();
  }

  String? read(String key) {
    return box.read<String>(key);
  }

  Future save(String key, dynamic value) {
    return box.write(key, value);
  }

  /// 移除本地存储
  void remove(String key) async {
    box.remove(key);
  }

  /// 清空所有
  void clean() {
    box.erase();
  }
}
