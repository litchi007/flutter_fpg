import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/extension/string.ext.dart';

class Template4Theme {
  static LinearGradient getMobileTemplate4Theme() {
    if (AppDefine.systemConfig?.mobileTemplateStyle == '1') {
      return LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          '#e5caae'.color(),
          '#d3a06f'.color(),
        ],
      );
    } else if (AppDefine.systemConfig?.mobileTemplateStyle == '2') {
      return LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          '#2f569e'.color(),
          '#2f569e'.color(),
        ],
      );
    } else if (AppDefine.systemConfig?.mobileTemplateStyle == '3') {
      return LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          '#13ab56'.color(),
          '#077e35'.color(),
        ],
      );
    }

    return LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: [
        '#ff4d63'.color(),
        '#ff4d63'.color(),
      ],
    );
  }
}
