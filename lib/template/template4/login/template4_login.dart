import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/template/template4/login/controller/controller.dart';
import 'package:fpg_flutter/template/template4_theme.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';

class Template4LoginPage extends GetView<Template4LoginController> {
  Template4LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            leading: IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              onPressed: () => Get.back(),
            ),
            title: const Text(
              '登录',
              style: TextStyle(color: Colors.white),
            ),
            actions: [
              Container(
                padding: const EdgeInsets.all(10),
                child: const Text(
                  '注册',
                  style: TextStyle(color: Colors.white),
                ),
              ).onTap(() => Get.toNamed(AppRoutes.template4Signup))
            ],
            flexibleSpace: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    gradient: Template4Theme.getMobileTemplate4Theme()))),
        body: SafeArea(
            child: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 36, right: 36, top: 30),
                child: Column(
                  children: _getChildrenWidget(),
                ))));
  }

  List<Widget> _getChildrenWidget() {
    List<Widget> children = [];

    /// 账号
    var account = Focus(
      onFocusChange: (isFocus) => controller.isFocus.value = isFocus,
      child: Obx(() => TextField(
            cursorColor: '#5c5c5c'.color(),
            cursorWidth: 1,
            controller: controller.accountEditingController,
            style: TextStyle(color: '#5d5d5d'.color()),
            decoration: InputDecoration(
              suffixIcon: (GestureDetector(
                  onTap: () => controller.accountEditingController.clear(),
                  child: controller.isFocus.value
                      ? Icon(size: 23, color: '#8D8B8B'.color(), Icons.close)
                      : const SizedBox())),
              contentPadding: const EdgeInsets.symmetric(horizontal: 5.0),
              hintText: '请输入会员账号',
              hintStyle: const TextStyle(fontSize: 14.0, color: Colors.grey),
              filled: true,
              fillColor: '#e5e5e5'.color(),
              counterText: '',
              floatingLabelBehavior: FloatingLabelBehavior.never,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                  color: '#8c8c8c'.color(), // 边框颜色
                  width: 1.0, // 边框宽度
                ), // 隐藏默认边框
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                  color: '#8c8c8c'.color(),
                  width: 1.0,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                  color: '#8c8c8c'.color(),
                  width: 1.0,
                ),
              ),
            ),
            onChanged: (text) {},
            onSubmitted: (text) {},
          )),
    );
    children.add(account);
    children.add(const SizedBox(height: 16));

    /// 密码
    var pwd = Obx(() => TextField(
          cursorColor: '#5c5c5c'.color(),
          cursorWidth: 1,
          controller: controller.pwdEditingController,
          obscureText: !controller.isPwdShowed.value,
          style: TextStyle(color: '#5d5d5d'.color()),
          decoration: InputDecoration(
            suffixIcon: IconButton(
              icon: controller.isPwdShowed.value
                  ? Icon(Icons.visibility, color: '#7cb4fb'.color())
                  : const Icon(Icons.visibility_off_outlined,
                      color: Colors.grey),
              onPressed: () =>
                  controller.isPwdShowed.value = !controller.isPwdShowed.value,
            ),
            contentPadding: const EdgeInsets.symmetric(horizontal: 5.0),
            hintText: '请输入密码',
            hintStyle: const TextStyle(fontSize: 14.0, color: Colors.grey),
            filled: true,
            fillColor: '#e5e5e5'.color(),
            counterText: '',
            floatingLabelBehavior: FloatingLabelBehavior.never,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: BorderSide(
                color: '#8c8c8c'.color(), // 边框颜色
                width: 1.0, // 边框宽度
              ), // 隐藏默认边框
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: BorderSide(
                color: '#8c8c8c'.color(),
                width: 1.0,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: BorderSide(
                color: '#8c8c8c'.color(),
                width: 1.0,
              ),
            ),
          ),
          onChanged: (text) {},
          onSubmitted: (text) {},
        ));
    children.add(pwd);
    children.add(const SizedBox(height: 16));

    /// 图形验证码
    if (AppDefine.isShowImageCode) {
      var code = Row(
        children: [
          Expanded(
              flex: 1,
              child: TextField(
                cursorColor: '#5c5c5c'.color(),
                cursorWidth: 1,
                maxLength: 10,
                controller: controller.codeEditingController,
                style: TextStyle(color: '#5d5d5d'.color()),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 5.0),
                  hintText: '验证码',
                  hintStyle:
                      const TextStyle(fontSize: 14.0, color: Colors.grey),
                  filled: true,
                  fillColor: '#e5e5e5'.color(),
                  counterText: '',
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4.0),
                    borderSide: BorderSide(
                      color: '#8c8c8c'.color(), // 边框颜色
                      width: 1.0, // 边框宽度
                    ), // 隐藏默认边框
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4.0),
                    borderSide: BorderSide(
                      color: '#8c8c8c'.color(),
                      width: 1.0,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4.0),
                    borderSide: BorderSide(
                      color: '#8c8c8c'.color(),
                      width: 1.0,
                    ),
                  ),
                ),
                onChanged: (text) {},
                onSubmitted: (text) {},
              )),
          const SizedBox(width: 20),
          Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () => controller.getImgCaptcha(),
                child: Obx(() => controller.imgCaptcha.value != ""
                    ? AppImage.memory(controller.imgCaptcha.value)
                    : const SizedBox()),
              ))
        ],
      );
      children.add(code);
      children.add(const SizedBox(height: 10));
    }

    /// 记住密码
    var rememberPwd = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          width: 20,
          child: Obx(() => Checkbox(
              value: controller.isChecked.value,
              activeColor: '#007aff'.color(),
              side: const BorderSide(
                color: Colors.black, // 边框颜色
                width: 1.0, // 边框宽度
              ),
              onChanged: (value) {
                controller.handleCheckBoxEvent();
              })),
        ),
        const SizedBox(
          width: 5,
        ),
        Text(
          '记住密码',
          style: TextStyle(color: '#3c3c3c'.color(), fontSize: 13),
        ),
      ],
    ).onTap(() => controller.handleCheckBoxEvent());
    children.add(rememberPwd);
    children.add(const SizedBox(height: 20));

    /// 登录
    var login = Container(
      height: 50,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: Template4Theme.getMobileTemplate4Theme(), // 设置渐变色背景
        borderRadius: BorderRadius.circular(4), // 设置圆角
      ),
      child: const Text(
        '登录',
        style: TextStyle(color: Colors.white),
      ),
    ).onTap(() => _login());
    children.add(login);
    children.add(const SizedBox(height: 22));

    /// 免费试玩
    var freePlay = Container(
      height: 50,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: Template4Theme.getMobileTemplate4Theme(), // 设置渐变色背景
        borderRadius: BorderRadius.circular(4), // 设置圆角
      ),
      child: const Text(
        '免费试玩',
        style: TextStyle(color: Colors.white),
      ),
    ).onTap(() => controller.guestLogin());
    children.add(freePlay);
    children.add(const SizedBox(height: 10));

    /// 返回首页
    var backHome = Container(
      height: 50,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: Template4Theme.getMobileTemplate4Theme(), // 设置渐变色背景
        borderRadius: BorderRadius.circular(4), // 设置圆角
      ),
      child: const Text(
        '返回首页',
        style: TextStyle(color: Colors.white),
      ),
    ).onTap(() => Get.offAllNamed(homePath));
    children.add(backHome);

    return children;
  }

  void _login({String? fullName}) {
    ToastUtils.showLoading();
    controller
        .userLogin(controller.accountEditingController.text,
            controller.pwdEditingController.text,
            fullName: fullName, imgCode: controller.codeEditingController.text)
        .then((data) {
      ToastUtils.closeAll();
      if (data == null) {
        ToastUtils.show('登录失败');
        controller.getImgCaptcha();
      } else {
        if (data.apiSid != null) {
          ToastUtils.show('登录成功');
          Get.offAllNamed(homePath);
        } else {
          if (data.needFullName ?? false) {
            showRealNameDialog();
          } else {
            ToastUtils.show('登录失败');
            controller.getImgCaptcha();
          }
        }
      }
    }).catchError((error) {
      ToastUtils.closeAll();
    });
  }

  void showRealNameDialog() {
    SmartDialog.show(
        permanent: true,
        clickMaskDismiss: false,
        usePenetrate: false,
        builder: (context) {
          return Container(
            padding: const EdgeInsets.only(left: 10, right: 10),
            width: MediaQuery.of(context).size.width - 36,
            height: 156,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(4)),
            ),
            child: Column(
              children: [
                const SizedBox(height: 10),
                const Text(
                  '请输入绑定的真实姓名',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  height: 34,
                  child: TextField(
                    cursorColor: '#111111'.color(),
                    cursorWidth: 1,
                    controller: controller.nameEditingController,
                    style: TextStyle(
                        fontSize: 14.0, // 设置字体大小
                        color: '#111111'.color()),
                    decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.symmetric(horizontal: 5.0),
                      hintText: '请输入真实姓名',
                      hintStyle:
                          TextStyle(fontSize: 14.0, color: '#111111'.color()),
                      filled: true,
                      fillColor: Colors.white,
                      counterText: '',
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        borderSide: BorderSide(
                          color: '#a9a9a9'.color(), // 边框颜色
                          width: 1.0, // 边框宽度
                        ), // 隐藏默认边框
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        borderSide: BorderSide(
                          color: '#a9a9a9'.color(),
                          width: 1.0,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        borderSide: BorderSide(
                          color: '#a9a9a9'.color(),
                          width: 1.0,
                        ),
                      ),
                    ),
                    onChanged: (text) {},
                    onSubmitted: (text) {},
                  ),
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 42,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        // 设置渐变色背景
                        border: Border.all(
                          color: const Color(0xffa9a9a9), // 边框颜色
                          width: 1.0, // 边框宽度
                        ),
                        borderRadius: BorderRadius.circular(4), // 设置圆角
                      ),
                      child: Text(
                        '取消',
                        style:
                            TextStyle(color: '#444444'.color(), fontSize: 16),
                      ),
                    ).onTap(() => SmartDialog.dismiss(force: true))),
                    const SizedBox(width: 5),
                    Expanded(
                      child: Container(
                        height: 42,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          gradient: Template4Theme.getMobileTemplate4Theme(),
                          // 设置渐变色背景
                          borderRadius: BorderRadius.circular(4), // 设置圆角
                        ),
                        child: const Text(
                          '确定',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ).onTap(() {
                        var realName = controller.nameEditingController.text;
                        if (realName.isNotEmpty) {
                          controller.nameEditingController.clear();
                          SmartDialog.dismiss(force: true);
                          _login(fullName: realName);
                        }
                      }),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
              ],
            ),
          );
        });
  }
}
