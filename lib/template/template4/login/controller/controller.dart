import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/UGLoginModel.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/v4.dart';

class Template4LoginController extends GetxController {
  static Template4LoginController get to =>
      Get.find<Template4LoginController>();

  final TextEditingController accountEditingController =
      TextEditingController();
  final TextEditingController pwdEditingController = TextEditingController();
  final TextEditingController codeEditingController = TextEditingController();
  final TextEditingController nameEditingController = TextEditingController();
  final AppSystemRepository _appSystemRepository = AppSystemRepository();
  final AppUserRepository _appUserRepository = AppUserRepository();
  final SharedPreferencesAsync asyncPrefs = SharedPreferencesAsync();

  final isFocus = false.obs;
  final imgCaptcha = ''.obs;
  final isChecked = true.obs;
  final isPwdShowed = false.obs;

  /// 获取验证码
  getImgCaptcha() {
    if (AppDefine.isShowImageCode) {
      _appSystemRepository.getImgCaptcha().then((data) {
        if (data != null) {
          imgCaptcha.value = data.base64Img!;
        }
      });
    }
  }

  /// 记住密码
  handleCheckBoxEvent() {
    isChecked.value = !isChecked.value;
  }

  /// 获取用户信息
  getUserInfo() {
    GlobalService.to.getUserInfo();
  }

  /// 用户登录
  Future<UGLoginModel?> userLogin(String userName, String password,
      {String? fullName, String? imgCode}) async {
    // UGLoginModel? state;

    // need to update the below code
    //////////////////////////////////////////////////

    // var res = await _appUserRepository.login(userName, password,
    //     fullName: fullName, imgCode: imgCode);
    // if (res?.data != null) {
    //   state = res?.data;
    //   if (res?.data?.apiSid != null) {
    //     AppDefine.userToken =
    //         UserToken(apiSid: res?.data?.apiSid, apiToken: res?.data?.apiToken);
    //     LogUtil.w({'tempToken', AppDefine.userToken?.toJson()});
    //     GlobalService.to.changeToken(res?.data?.apiSid ?? "");
    //     // storage.write(StorageKeys.USERTOKEN, AppDefine.userToken);
    //     // storage.write(StorageKeys.USERName, userName);
    //     // storage.write(StorageKeys.USERPwd, isChecked.value ? password : '');
    //   }
    // }

    // LogUtil.w('login data: ${res?.data?.toJson()}');

    // return state;
  }

  /// 游客登录
  guestLogin() {
    ToastUtils.showLoading();
    _appUserRepository.guestLogin().then((data) async {
      ToastUtils.closeAll();
      if (data != null && data.apiSid != null) {
        Get.offAllNamed(homePath);
        ToastUtils.show('登录成功');
        AppDefine.userToken =
            UserToken(apiSid: data.apiSid, apiToken: data.apiToken);
        GlobalService.to.changeToken(data.apiSid ?? '', data.apiToken ?? '');
        await asyncPrefs.setString(
            StorageKeys.USERTOKEN, jsonEncode(AppDefine.userToken));
      } else {
        ToastUtils.show('登录失败');
      }
    }).catchError((error) {
      ToastUtils.show('登录失败');
      ToastUtils.closeAll();
    });
  }

  @override
  void onInit() async {
    getImgCaptcha();
    var userName = await asyncPrefs.getString(StorageKeys.USERNAME);
    accountEditingController.text = userName ?? '';

    var pwd = await asyncPrefs.getString(StorageKeys.USERPWD);
    pwdEditingController.text = pwd ?? '';

    super.onInit();
  }
}
