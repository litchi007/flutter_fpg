import '../controller/controller.dart';
import 'package:get/get.dart';

class Template4SignupBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<Template4SignupController>(() => Template4SignupController());
  }
}
