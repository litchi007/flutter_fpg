import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/template/template4/signup/controller/controller.dart';
import 'package:fpg_flutter/template/template4_theme.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:fpg_flutter/utils/validator.dart';
import 'package:get/get.dart';

class Template4SignupPage extends GetView<Template4SignupController> {
  Template4SignupPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              onPressed: () => Get.back(),
            ),
            title: Text(
              '注册',
              style: TextStyle(color: Colors.white),
            ),
            flexibleSpace: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    gradient: Template4Theme.getMobileTemplate4Theme()))),
        body: SafeArea(
            child: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 36, right: 36, top: 20),
                child: Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: Column(
                    children: _getChildrenWidget(),
                  ),
                ))));
  }

  List<Widget> _getChildrenWidget() {
    List<Widget> children = [];

    /// 账号
    var account = Focus(
      onFocusChange: (isFocus) => controller.isAccountFocus.value = isFocus,
      child: Obx(() => TextFormField(
            cursorColor: '#5c5c5c'.color(),
            cursorWidth: 1,
            maxLength: 16,
            controller: controller.accountEditingController,
            style: TextStyle(color: '#5d5d5d'.color()),
            decoration: InputDecoration(
              suffixIcon: (GestureDetector(
                  onTap: () => controller.accountEditingController.clear(),
                  child: controller.isAccountFocus.value
                      ? Icon(size: 23, color: '#8D8B8B'.color(), Icons.close)
                      : const SizedBox())),
              contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
              hintText: '*请输入会员账号(6-16位字母或数字)',
              hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
              filled: true,
              fillColor: '#e5e5e5'.color(),
              counterText: '',
              floatingLabelBehavior: FloatingLabelBehavior.never,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                  color: '#8c8c8c'.color(), // 边框颜色
                  width: 1.0, // 边框宽度
                ), // 隐藏默认边框
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                  color: '#8c8c8c'.color(),
                  width: 1.0,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(
                  color: '#8c8c8c'.color(),
                  width: 1.0,
                ),
              ),
              // errorBorder: OutlineInputBorder(
              //   borderRadius: BorderRadius.circular(4.0),
              //   borderSide: BorderSide(
              //     color: '#8c8c8c'.color(),
              //     width: 1.0,
              //   ),
              // ),
              // focusedErrorBorder: OutlineInputBorder(
              //   borderRadius: BorderRadius.circular(4.0),
              //   borderSide: BorderSide(
              //     color: '#8c8c8c'.color(),
              //     width: 1.0,
              //   ),
              // ),
            ),
            onChanged: (text) {},
            validator: (text) {
              return Validator.validateUserId(text ?? '');
            },
          )),
    );
    children.add(account);
    children.add(SizedBox(height: 10));

    /// 密码
    var pwd = Obx(() => TextFormField(
        cursorColor: '#5c5c5c'.color(),
        cursorWidth: 1,
        controller: controller.pwdEditingController,
        obscureText: !controller.isPwdShowed.value,
        style: TextStyle(color: '#5d5d5d'.color()),
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: controller.isPwdShowed.value
                ? Icon(Icons.visibility, color: '#7cb4fb'.color())
                : Icon(Icons.visibility_off_outlined, color: Colors.grey),
            onPressed: () =>
                controller.isPwdShowed.value = !controller.isPwdShowed.value,
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
          hintText: '请输入密码(长度不能低于6位)',
          hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
          filled: true,
          fillColor: '#e5e5e5'.color(),
          counterText: '',
          floatingLabelBehavior: FloatingLabelBehavior.never,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4.0),
            borderSide: BorderSide(
              color: '#8c8c8c'.color(), // 边框颜色
              width: 1.0, // 边框宽度
            ), // 隐藏默认边框
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4.0),
            borderSide: BorderSide(
              color: '#8c8c8c'.color(),
              width: 1.0,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4.0),
            borderSide: BorderSide(
              color: '#8c8c8c'.color(),
              width: 1.0,
            ),
          ),
          // errorBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(4.0),
          //   borderSide: BorderSide(
          //     color: '#8c8c8c'.color(),
          //     width: 1.0,
          //   ),
          // ),
          // focusedErrorBorder: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(4.0),
          //   borderSide: BorderSide(
          //     color: '#8c8c8c'.color(),
          //     width: 1.0,
          //   ),
          // ),
        ),
        onChanged: (text) {},
        validator: (text) {
          return Validator.validatePassword(text ?? '');
        }));
    children.add(pwd);
    children.add(SizedBox(height: 10));

    /// 确认密码
    var confirmPwd = Obx(() => TextFormField(
          cursorColor: '#5c5c5c'.color(),
          cursorWidth: 1,
          controller: controller.confirmPwdEditingController,
          obscureText: !controller.isConfirmPwdShowed.value,
          style: TextStyle(color: '#5d5d5d'.color()),
          decoration: InputDecoration(
            suffixIcon: IconButton(
              icon: controller.isConfirmPwdShowed.value
                  ? Icon(Icons.visibility, color: '#7cb4fb'.color())
                  : Icon(Icons.visibility_off_outlined, color: Colors.grey),
              onPressed: () => controller.isConfirmPwdShowed.value =
                  !controller.isConfirmPwdShowed.value,
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
            hintText: '请确认密码',
            hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
            filled: true,
            fillColor: '#e5e5e5'.color(),
            counterText: '',
            floatingLabelBehavior: FloatingLabelBehavior.never,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: BorderSide(
                color: '#8c8c8c'.color(), // 边框颜色
                width: 1.0, // 边框宽度
              ), // 隐藏默认边框
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: BorderSide(
                color: '#8c8c8c'.color(),
                width: 1.0,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: BorderSide(
                color: '#8c8c8c'.color(),
                width: 1.0,
              ),
            ),
          ),
          onChanged: (text) {},
          validator: (text) {
            return text == controller.pwdEditingController.text
                ? null
                : '密码不一致';
          },
        ));
    children.add(confirmPwd);
    children.add(SizedBox(height: 10));

    /// 真实姓名
    var realName = Focus(
        onFocusChange: (isFocus) => controller.isRealNameFocus.value = isFocus,
        child: Obx(() => TextField(
              cursorColor: '#5c5c5c'.color(),
              cursorWidth: 1,
              controller: controller.nameEditingController,
              style: TextStyle(color: '#5d5d5d'.color()),
              decoration: InputDecoration(
                suffixIcon: (GestureDetector(
                    onTap: () => controller.nameEditingController.clear(),
                    child: controller.isRealNameFocus.value
                        ? Icon(size: 23, color: '#8D8B8B'.color(), Icons.close)
                        : const SizedBox())),
                contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
                hintText: '请输入真实姓名',
                hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
                filled: true,
                fillColor: '#e5e5e5'.color(),
                counterText: '',
                floatingLabelBehavior: FloatingLabelBehavior.never,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: '#8c8c8c'.color(), // 边框颜色
                    width: 1.0, // 边框宽度
                  ), // 隐藏默认边框
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: '#8c8c8c'.color(),
                    width: 1.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: '#8c8c8c'.color(),
                    width: 1.0,
                  ),
                ),
              ),
              onChanged: (text) {},
              onSubmitted: (text) {},
            )));
    children.add(realName);
    children.add(SizedBox(height: 10));

    /// 手机号码
    var phone = TextField(
      cursorColor: '#5c5c5c'.color(),
      cursorWidth: 1,
      maxLength: 11,
      controller: controller.phoneEditingController,
      style: TextStyle(color: '#5d5d5d'.color()),
      keyboardType:
          TextInputType.numberWithOptions(decimal: true, signed: false),
      // 设置键盘类型为数字
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))
        // 只允许输入数字
      ],
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
        hintText: '请输入手机号',
        hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
        filled: true,
        fillColor: '#e5e5e5'.color(),
        counterText: '',
        floatingLabelBehavior: FloatingLabelBehavior.never,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(
            color: '#8c8c8c'.color(), // 边框颜色
            width: 1.0, // 边框宽度
          ), // 隐藏默认边框
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(
            color: '#8c8c8c'.color(),
            width: 1.0,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(
            color: '#8c8c8c'.color(),
            width: 1.0,
          ),
        ),
      ),
      onChanged: (text) {},
      onSubmitted: (text) {},
    );
    children.add(phone);
    children.add(SizedBox(height: 10));

    /// 图形验证码
    if (AppDefine.systemConfig?.loginVCodeType == '1' &&
        AppDefine.systemConfig?.loginVCode == true) {
      var code = Row(
        children: [
          Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () => controller.getImgCaptcha(),
                child: Obx(() => controller.imgCaptcha.value != ""
                    ? AppImage.memory(controller.imgCaptcha.value)
                    : Container()),
              )),
          SizedBox(width: 10),
          Expanded(
            flex: 1,
            child: TextField(
              cursorColor: '#5c5c5c'.color(),
              cursorWidth: 1,
              maxLength: 10,
              controller: controller.codeEditingController,
              style: TextStyle(color: '#5d5d5d'.color()),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(horizontal: 5.0),
                hintText: '验证码',
                hintStyle: TextStyle(fontSize: 14.0, color: Colors.grey),
                filled: true,
                fillColor: '#e5e5e5'.color(),
                counterText: '',
                floatingLabelBehavior: FloatingLabelBehavior.never,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: '#8c8c8c'.color(), // 边框颜色
                    width: 1.0, // 边框宽度
                  ), // 隐藏默认边框
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: '#8c8c8c'.color(),
                    width: 1.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide(
                    color: '#8c8c8c'.color(),
                    width: 1.0,
                  ),
                ),
              ),
              onChanged: (text) {},
              onSubmitted: (text) {},
            ),
          ),
        ],
      );
      children.add(code);
      children.add(SizedBox(height: 20));
    }

    /// 注册
    var login = Builder(builder: (context) {
      return Container(
        height: 50,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: Template4Theme.getMobileTemplate4Theme(), // 设置渐变色背景
          borderRadius: BorderRadius.circular(4), // 设置圆角
        ),
        child: Text(
          '注册',
          style: TextStyle(color: Colors.white),
        ),
      ).onTap(() {
        if (Form.of(context).validate()) {
          ToastUtils.showLoading();
          controller
              .userRegister(
            controller.accountEditingController.text,
            controller.pwdEditingController.text,
            fullName: controller.nameEditingController.text,
            imgCode: controller.codeEditingController.text,
            phone: controller.phoneEditingController.text,
          )
              .then((msg) {
            ToastUtils.closeAll();
            if (msg == "") {
              ToastUtils.show('注册成功');
              Get.offAllNamed(homePath);
            } else {
              controller.getImgCaptcha();
              ToastUtils.show(msg);
            }
          });
        }
      });
    });
    children.add(login);
    children.add(SizedBox(height: 22));

    /// 免费试玩
    var backLogin = Container(
      height: 50,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: Template4Theme.getMobileTemplate4Theme(), // 设置渐变色背景
        borderRadius: BorderRadius.circular(4), // 设置圆角
      ),
      child: Text(
        '返回登录',
        style: TextStyle(color: Colors.white),
      ),
    ).onTap(() => Get.back());
    children.add(backLogin);
    children.add(SizedBox(height: 10));

    /// 返回首页
    var backHome = Container(
      height: 50,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        gradient: Template4Theme.getMobileTemplate4Theme(), // 设置渐变色背景
        borderRadius: BorderRadius.circular(4), // 设置圆角
      ),
      child: Text(
        '返回首页',
        style: TextStyle(color: Colors.white),
      ),
    ).onTap(() => Get.offAllNamed(homePath));
    children.add(backHome);

    return children;
  }
}
