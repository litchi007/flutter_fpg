import 'package:flutter/cupertino.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/UGRegisterModel.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/data/repositories/app_user_repository.dart';
import 'package:fpg_flutter/http/api_interface/response_model.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:uuid/v4.dart';

class Template4SignupController extends GetxController {
  static Template4SignupController get to =>
      Get.find<Template4SignupController>();
  final TextEditingController accountEditingController =
      TextEditingController();
  final TextEditingController pwdEditingController = TextEditingController();
  final TextEditingController confirmPwdEditingController =
      TextEditingController();
  final TextEditingController nameEditingController = TextEditingController();
  final TextEditingController phoneEditingController = TextEditingController();
  final TextEditingController codeEditingController = TextEditingController();
  final AppSystemRepository _appSystemRepository = AppSystemRepository();
  final AppUserRepository _appUserRepository = AppUserRepository();
  final storage = GetStorage();

  final imgCaptcha = ''.obs;
  final isPwdShowed = false.obs;
  final isConfirmPwdShowed = false.obs;
  final isAccountFocus = false.obs;
  final isRealNameFocus = false.obs;

  /// 获取验证码
  getImgCaptcha() {
    _appSystemRepository.getImgCaptcha().then((data) {
      if (data != null) {
        imgCaptcha.value = data.base64Img!;
      }
    }).catchError((data) {
      print({'getImgCaptcha : failed'});
    });
  }

  //注册
  Future<String> userRegister(String userName, String password,
      {String? fullName,
      String? inviter,
      String? inviteCode,
      String? fundPwd,
      String? qq,
      String? wx,
      String? email,
      String? imgCode,
      String? smsCode,
      String? phone,
      String? fb,
      String? line}) async {
    String state = "注册失败";
    NetBaseEntity<UGRegisterModel> res = await _appUserRepository.register(
        userName, password,
        fullName: fullName,
        inviter: inviter,
        inviteCode: inviteCode,
        fundPwd: fundPwd,
        qq: qq,
        wx: wx,
        email: email,
        imgCode: imgCode,
        smsCode: smsCode,
        phone: phone,
        fb: fb,
        line: line); //.then((data) {
    try {
      if (res.data == null) {
        return res.msg;
      }
      UGRegisterModel? data = res.data;
      AppDefine.userToken =
          UserToken(apiSid: data?.apiSid, apiToken: data?.apiToken);
      LogUtil.w({'tempToken', AppDefine.userToken?.toJson()});
      storage.write(StorageKeys.USERTOKEN, AppDefine.userToken);
      LogUtil.w({'login data': data});
      state = "";
    } catch (e) {
      state = e.toString();
      LogUtil.w({'register error': state});
    }
    return state;
  }

  @override
  void onInit() {
    if (AppDefine.systemConfig?.loginVCodeType == '1' ||
        AppDefine.systemConfig?.loginVCode == true) {
      getImgCaptcha();
    } // TODO: implement onInit
    super.onInit();
  }
}
