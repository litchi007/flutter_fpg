import '../controller/controller.dart';
import 'package:get/get.dart';

class Template4MineBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<Template4MineController>(() => Template4MineController());
  }
}
