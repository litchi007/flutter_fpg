import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/app/widgets/app_image.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/extension/string.ext.dart';
import 'package:fpg_flutter/extension/widget_ext.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/template/template4/mine/controller/controller.dart';
import 'package:fpg_flutter/template/template4_theme.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/toast_utils.dart';
import 'package:fpg_flutter/utils/validator.dart';
import 'package:get/get.dart';

class Template4MinePage extends GetView<Template4MineController> {
  Template4MinePage({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(Template4MineController());
    return Scaffold(
        backgroundColor: Colors.white,
        // appBar: AppBar(
        //     title: Text(
        //       '我的',
        //       style: TextStyle(color: Colors.white),
        //     ),
        //     flexibleSpace: Container(
        //         alignment: Alignment.center,
        //         decoration: BoxDecoration(
        //             gradient: Template4Theme.getMobileTemplate4Theme()))),
        body: SafeArea(
            child: SingleChildScrollView(
                child: Column(
          children: [
            _getUserInfoWidget(),
            _getMyFunctionWidget(),
          ],
          // children: <Widget>[
          //   UserdataWidget(),
          //   Gap(18.h),
          //   const MoneyWidget(),
          //   TabWidget(),
          //   SizedBox(
          //     width: 1.sw,
          //     height: 8.h,
          //     child: ColoredBox(color: AppColors.ffF5F5F5),
          //   ),
          //   ListWidgets(customAlertDialog: customAlertDialog),
          // ],
        ))));
  }

  Widget _getUserInfoWidget() {
    return Container(
      height: 160,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10), // 设置圆角
        image: DecorationImage(
          image: AssetImage('assets/images/img_banner.png'), // 使用本地图片
          fit: BoxFit.fill, // 图片填充容器
        ),
      ),
      child: Column(
        children: [
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(width: 10),
              Container(
                width: 66,
                height: 66,
                decoration: BoxDecoration(
                  shape: BoxShape.circle, // 使 Container 成为圆形
                  image: DecorationImage(
                    image: AssetImage('assets/images/money-2.png'), // 使用本地图片
                    fit: BoxFit.cover, // 图片填充 Container
                  ),
                  border: Border.all(
                    color: Colors.white, // 边框颜色
                    width: 1, // 边框宽度
                  ),
                ),
              ),
              SizedBox(width: 10),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 5),
                  Row(
                    children: [
                      Text(
                        'miya01',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(width: 10),
                      Container(
                          width: 50,
                          height: 20,
                          alignment: Alignment.center,
                          child: Text('VIP0',
                              style: TextStyle(color: Colors.white)),
                          decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(4.0))),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text.rich(TextSpan(
                          text: '余额：',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          children: const [
                            TextSpan(
                                text: '1234.566 RMB',
                                style: TextStyle(color: Colors.yellowAccent))
                          ])),
                      Icon(
                        Icons.refresh,
                        color: Colors.yellow,
                        size: 20,
                      ).onTap(() {})
                    ],
                  )
                ],
              ),
              Spacer(),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 100,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.purple,
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                  Container(
                    height: 5,
                    color: Colors.white,
                  ),
                  Container(
                    width: 100,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: [
              SizedBox(width: 20),
              Text('成长值：', style: TextStyle(color: Colors.white)),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    '距离下一级还差10.00',
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ).paddingOnly(right: 10),
                  Container(
                    height: 16,
                    decoration: BoxDecoration(
                        color: '#cbdbeb'.color(),
                        borderRadius: BorderRadius.circular(8.0)),
                  ),
                  SizedBox(height: 6),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Image.asset(
                            'assets/images/money-2.png', // 替换成你自己的图片路径
                            width: 20,
                            height: 20,
                          ),
                          Text('VIP0', style: TextStyle(color: Colors.yellow)),
                        ],
                      ),
                      Row(
                        children: [
                          Image.asset(
                            'assets/images/money-2.png', // 替换成你自己的图片路径
                            width: 20,
                            height: 20,
                          ),
                          Text('VIP1', style: TextStyle(color: Colors.yellow)),
                        ],
                      )
                    ],
                  )
                ],
              )),
              SizedBox(width: 30),
            ],
          )
        ],
      ),
    ).paddingAll(10);
  }

  Widget _getMyFunctionWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          width: 60,
          height: 100,
          color: Colors.yellow,
        ),
        Container(
          width: 60,
          height: 100,
          color: Colors.yellow,
        ),
        Container(
          width: 60,
          height: 100,
          color: Colors.yellow,
        ),
      ],
    );

    return Container(
      height: 160,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10), // 设置圆角
        image: DecorationImage(
          image: AssetImage('assets/images/img_banner.png'), // 使用本地图片
          fit: BoxFit.fill, // 图片填充容器
        ),
      ),
      child: Column(
        children: [
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(width: 10),
              Container(
                width: 66,
                height: 66,
                decoration: BoxDecoration(
                  shape: BoxShape.circle, // 使 Container 成为圆形
                  image: DecorationImage(
                    image: AssetImage('assets/images/csonline.png'), // 使用本地图片
                    fit: BoxFit.cover, // 图片填充 Container
                  ),
                  border: Border.all(
                    color: Colors.white, // 边框颜色
                    width: 1, // 边框宽度
                  ),
                ),
              ),
              SizedBox(width: 10),
              Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 5),
                  Container(
                    width: 100,
                    height: 20,
                    color: Colors.white,
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: 100,
                    height: 20,
                    color: Colors.white,
                  ),
                ],
              ),
              Spacer(),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 100,
                    height: 20,
                    color: Colors.white,
                  ),
                  Container(
                    height: 5,
                    color: Colors.white,
                  ),
                  Container(
                    width: 100,
                    height: 20,
                    color: Colors.black,
                  ),
                ],
              ),
              SizedBox(width: 10),
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: [
              SizedBox(width: 20),
              Text('成长值: '),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text('距离下一级还差10.00').paddingOnly(right: 10),
                  Container(
                    height: 20,
                    color: Colors.white,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 50,
                        height: 20,
                        color: Colors.purple,
                      ),
                      Container(
                        width: 50,
                        height: 20,
                        color: Colors.purple,
                      ),
                    ],
                  )
                ],
              )),
              SizedBox(width: 30),
            ],
          )
        ],
      ),
    ).paddingAll(10);
  }
}
