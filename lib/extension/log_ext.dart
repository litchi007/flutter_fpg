import 'package:dio/dio.dart';

extension GetLogID on RequestOptions {
  String get logId {
    return '[$method]${uri.path}';
  }
}
