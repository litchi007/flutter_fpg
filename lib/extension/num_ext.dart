import 'dart:convert';
import 'dart:math';
import 'dart:ui';
import 'package:fpg_flutter/generated/locales.g.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

extension IntExt on int {
  Duration get days => Duration(days: this);

  Duration get hours => Duration(hours: this);

  Duration get minutes => Duration(minutes: this);

  Duration get seconds => Duration(seconds: this);

  Duration get milliseconds => Duration(milliseconds: this);

  Duration get microseconds => Duration(microseconds: this);

  DateTime get toDateTimeInMicroseconds =>
      DateTime.fromMicrosecondsSinceEpoch(this);

  DateTime get toDateTimeInMilliseconds =>
      DateTime.fromMillisecondsSinceEpoch(this);

  BorderRadius get radius => BorderRadius.all(Radius.circular(toDouble()));

  String priceFix() {
    return "${this == 0 ? this : "--"}";
  }

  /// 时间戳转换年月日
  /// *[timestamp] 13 位时间戳
  String toTime() {
    int timestamp = this;
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp);
    return '${dateTime.year}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.day.toString().padLeft(2, '0')}';
  }

  /// 几天前/几年前/几月前/几小时前
  List<String> timeFormat() {
    final end = DateTime.now().millisecondsSinceEpoch;
    // 年
    if (end - this > (365 * 24 * 60 * 60 * 1000)) {
      return [
        ((end - this) / (365 * 24 * 60 * 60 * 1000)).truncate().toString(),
        LocaleKeys.yearAgo
      ];
    }
    // 月
    if (end - this > (30 * 24 * 60 * 60 * 1000)) {
      return [
        ((end - this) / (30 * 24 * 60 * 60 * 1000)).truncate().toString(),
        LocaleKeys.monthAgo
      ];
    }
    // 日
    if (end - this > (24 * 60 * 60 * 1000)) {
      return [
        ((end - this) / (24 * 60 * 60 * 1000)).truncate().toString(),
        LocaleKeys.dayAgo
      ];
    }
    // 小时
    if (end - this > (60 * 60 * 1000)) {
      return [
        ((end - this) / (60 * 60 * 1000)).truncate().toString(),
        LocaleKeys.hoursAgo
      ];
    }
    // 分钟
    return [
      ((end - this) / (60 * 1000)).truncate().toString(),
      LocaleKeys.minuteAgo
    ];
  }

  String toHMS() {
    final totalSeconds = this;
    int hours = totalSeconds ~/ 3600;
    int minutes = (totalSeconds % 3600) ~/ 60;
    int seconds = totalSeconds % 60;

    String formattedTime =
        '${hours > 0 ? '${hours.toString().padLeft(2, '0')}:' : ''}${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';

    return formattedTime;
  }

  /// 数字转换千/w/十万//百万
  String formatNum() {
    if (this >= 1000 && this < 10000) {
      final size = this / 1000;
      final arr = size.toString().split(".");
      if (arr[1] == "0") {
        return "${arr[0]}K";
      }
      return "${(this / 1000)}K";
    }
    NumberFormat formatM = NumberFormat.compactLong(locale: "zh_CN");
    return formatM.format(this);
  }

  String generateRandomString() {
    final Random random = Random();
    const String availableChars = 'ABCDEFGHIGKLMNOPQRSTUVWXYZ';
    final String randomString = List.generate(
        this,
        (int index) =>
            availableChars[random.nextInt(availableChars.length)]).join();

    return randomString;
  }
}

extension DoubleExt on double? {
  String priceFix() {
    return "${(this ?? 0) > 0 ? this : "--"}";
  }
}

extension Numext on num {
  String get fileSizeFromBytes {
    const int kb = 1024;
    const int mb = 1024 * kb;
    const int gb = 1024 * mb;
    if (this >= gb) {
      return '${(this / gb).toStringAsFixed(2)} GB';
    }
    if (this >= mb) {
      return '${(this / mb).toStringAsFixed(2)} MB';
    }
    if (this >= kb) {
      return '${(this / kb).toStringAsFixed(2)} KB';
    }
    return '$this B';
  }
}
