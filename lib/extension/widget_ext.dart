import 'package:flutter/material.dart';
import 'package:get/get.dart';

extension WidgetExtension on Widget {
  Widget center() => Center(child: this);
  Widget padding(EdgeInsetsGeometry p) => Padding(padding: p, child: this);
  Widget margin(EdgeInsetsGeometry m) => Container(margin: m, child: this);
  Widget infinity() =>
      SizedBox(width: double.infinity, height: double.infinity, child: this);

  /// 点击
  /// *[fn] 回调
  /// *[type] 类型 [PostType]
  /// *[value] json 转 string
  /// *[id] 唯一 id
  GestureDetector onTap(Function()? fn,
          [String? type, String? value, String? id]) =>
      GestureDetector(
        onTap: () {
          fn?.call();
          // FocusScope.of(Get.context!).requestFocus(FocusNode());
          // db?.history.deleteHistory(historyDb)
        },
        behavior: HitTestBehavior.opaque,
        child: this,
      );
}
