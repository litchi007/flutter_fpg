import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fpg_flutter/extension/context_ext.dart';
import 'package:get/get.dart';

extension StringExt on String {
  Color color() {
    String _ = this;
    _ = _.toUpperCase().replaceAll('#', '');
    if (_.length == 6) {
      _ = 'FF$_';
    }
    return Color(int.parse(_, radix: 16));
  }

  Map<String, dynamic> toJson() => jsonDecode(this);

  String removeFirst(Pattern pattern, [int startIndex = 0]) =>
      replaceFirst(pattern, '', startIndex);
  String removeAll(Pattern pattern) => replaceAll(pattern, '');

  String encode() {
    return jsonEncode(this);
  }

  T decode<T>() {
    return jsonDecode(this);
  }

  String nowrap() {
    return (Characters(this).join('\u{200B}'));
  }

  int getTime() {
    DateTime dateTime = DateTime.parse(this); // 将字符串解析为DateTime对象
    int timestamp = dateTime.millisecondsSinceEpoch; // 获取时间戳（毫秒级）
    return timestamp;
  }

  /// 获取开奖数字背景颜色
  Color? getNumColor() {
    final ballColor = {
      1: Get.context?.customTheme?.lottery1,
      2: Get.context?.customTheme?.lottery2,
      3: Get.context?.customTheme?.lottery3,
      4: Get.context?.customTheme?.lottery4,
      5: Get.context?.customTheme?.lottery5,
      6: Get.context?.customTheme?.lottery6,
      7: Get.context?.customTheme?.lottery7,
      8: Get.context?.customTheme?.lottery8,
      9: Get.context?.customTheme?.lottery9,
      10: Get.context?.customTheme?.lottery10,
    };
    return ballColor[int.tryParse(this)];
  }
}
