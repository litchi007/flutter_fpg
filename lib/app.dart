import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/generated/locales.g.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/router/router_observer.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/services/lang_service.dart';
import 'package:fpg_flutter/services/theme_service.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  Widget _buildAnnotatedRegion(BuildContext context, Widget child) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Get.isDarkMode
          ? SystemUiOverlayStyle.light
          : SystemUiOverlayStyle.dark,
      child: child,
    );
  }

  Widget _buildBottomPaddingVerticalShield(BuildContext context) {
    return PositionedDirectional(
      start: 0,
      end: 0,
      bottom: 0,
      height: MediaQuery.of(context).padding.bottom,
      child: GestureDetector(
        onTap: () {},
        behavior: HitTestBehavior.translucent,
        onVerticalDragStart: (_) {},
      ),
    );
  }

  MediaQuery _buildFontSize(BuildContext context, Widget child) {
    return MediaQuery(
        data: MediaQuery.of(context)
            .copyWith(textScaler: const TextScaler.linear(1)),
        child: child);
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      showPerformanceOverlay: GlobalService.to.performanceStatus.value,
      title: 'FPG-t002',
      enableLog: true,
      debugShowCheckedModeBanner: false,
      // initialRoute: AppRoutes.home,
      initialRoute: AppRoutes.splash,
      getPages: AppRoutes.routes,
      theme: ThemeService.to.light,
      darkTheme: ThemeService.to.dark,
      themeMode: ThemeService.to.mode,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        RefreshLocalizations.delegate,
      ],
      locale: LangService.to.lang,
      translationsKeys: AppTranslation.translations,
      fallbackLocale: const Locale('en', 'US'),
      supportedLocales: const [Locale('zh', 'CN'), Locale('en', 'US')],
      navigatorObservers: [
        GetXRouterObserver(),
        routeObserver,
        FlutterSmartDialog.observer
      ],
      builder: FlutterSmartDialog.init(
          // TODO: 如果需要自定义 loading 样式
          // loadingBuilder: (msg) => CustomLoading(msg: msg),
          builder: (context, child) {
        _buildAnnotatedRegion(context, child!);
        _buildBottomPaddingVerticalShield(context);
        return Listener(
            onPointerMove: (event) {
              // FocusScope.of(context).requestFocus(FocusNode());
            },
            onPointerDown: (detail) {
              // FocusScope.of(context).requestFocus(FocusNode());
            },
            child: _buildFontSize(context, child));
      }),
    );
  }
}
