import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:fpg_flutter/app/widgets/app_custom_toast_animation.dart';
import 'package:fpg_flutter/data/repositories/app_system_repository.dart';
import 'package:fpg_flutter/generated/locales.g.dart';
import 'package:fpg_flutter/main_bindings.dart';
import 'package:fpg_flutter/router/router_observer.dart';
import 'package:fpg_flutter/services/app_service.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/services/lang_service.dart';
import 'package:fpg_flutter/services/theme_service.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:fpg_flutter/utils/theme/app_theme.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shorebird_code_push/shorebird_code_push.dart';
import 'routes.dart';
import 'package:provider/provider.dart';
import 'package:fpg_flutter/utils/timermanager_util.dart';
import 'package:fconsole/fconsole.dart';

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
final shorebirdCodePush = ShorebirdCodePush();

void main() async {
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();

    await AppServices.init();
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    await GetStorage.init();
    await StorageUtils().init();

    await Get.putAsync(() => GlobalService().init(), permanent: true);
    Get.lazyPut(() => LangService(), fenix: true);
    Get.put(ThemeService(), permanent: true);

    // configLoading();
    final AppSystemRepository appSystemRepository = AppSystemRepository();
    await appSystemRepository.fetchConfig();
    // if (kDebugMode) {
    runAppWithFConsole(
        ChangeNotifierProvider(
          create: (_) => AppTheme(),
          child: const MyApp(),
        ), beforeRun: () async {
      WidgetsFlutterBinding.ensureInitialized();
      // Do some init before runApp
    });
    // }else {
    // runApp(
    //   ChangeNotifierProvider(
    //     create: (_) => AppTheme(),
    //     child: const MyApp(),
    //   ),
    // );
    // }
  }, (Object obj, StackTrace stack) {
    if (kDebugMode) {
      print(obj);
      print(stack);
    }
  });
}

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.custom
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = true
    ..dismissOnTap = false
    ..customAnimation = AppCustomToastAnimation();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  Widget _buildAnnotatedRegion(BuildContext context, Widget child) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Get.isDarkMode
          ? SystemUiOverlayStyle.light
          : SystemUiOverlayStyle.dark,
      child: child,
    );
  }

  Widget _buildBottomPaddingVerticalShield(BuildContext context) {
    return PositionedDirectional(
      start: 0,
      end: 0,
      bottom: 0,
      height: MediaQuery.of(context).padding.bottom,
      child: GestureDetector(
        onTap: () {},
        behavior: HitTestBehavior.translucent,
        onVerticalDragStart: (_) {},
      ),
    );
  }

  MediaQuery _buildFontSize(BuildContext context, Widget child) {
    return MediaQuery(
        data: MediaQuery.of(context)
            .copyWith(textScaler: const TextScaler.linear(1)),
        child: child);
  }

  @override
  Widget build(BuildContext context) {
    configLoading();
    return Consumer<AppTheme>(builder: (context, themeNotifier, child) {
      return ScreenUtilInit(
        designSize: const Size(540, 960),
        fontSizeResolver: FontSizeResolvers.width,
        builder: (context, child) {
          return GestureDetector(
            onTap: () {
              // FocusScope.of(context).requestFocus(FocusNode());
            },
            child: GetMaterialApp(
              // builder:
              // FlutterSmartDialog.init(
              //     // TODO: 如果需要自定义 loading 样式
              //     // loadingBuilder: (msg) => CustomLoading(msg: msg),
              //     builder: (context, child) {
              //   _buildAnnotatedRegion(context, child!);
              //   _buildBottomPaddingVerticalShield(context);
              //   EasyLoading.init();
              //   return Listener(
              //       onPointerMove: (event) {
              //         // FocusScope.of(context).requestFocus(FocusNode());
              //       },
              //       onPointerDown: (detail) {
              //         // FocusScope.of(context).requestFocus(FocusNode());
              //       },
              //       child: _buildFontSize(context, child));
              // }),
              // EasyLoading.init(),

              builder: EasyLoading.init(builder: FlutterSmartDialog.init(
                  // TODO: 如果需要自定义 loading 样式
                  // loadingBuilder: (msg) => CustomLoading(msg: msg),
                  builder: (context, child) {
                _buildAnnotatedRegion(context, child!);
                _buildBottomPaddingVerticalShield(context);

                return Listener(
                    onPointerMove: (event) {
                      // FocusScope.of(context).requestFocus(FocusNode());
                    },
                    onPointerDown: (detail) {
                      // FocusScope.of(context).requestFocus(FocusNode());
                    },
                    child: _buildFontSize(context, child));
              })),
              // EasyLoading.init(),

              title: 'FPG-t002',
              debugShowCheckedModeBanner: false,
              // initialRoute: '/home',
              initialRoute: '/home',
              // initialRoute: '/splash',
              // initialRoute: depositPath,
              getPages: routes,
              //            theme: ThemeService.light,
              //            darkTheme: ThemeService.dark,
              //            themeMode: ThemeService.to.mode,
              theme: AppTheme.lightTheme, // Use light theme
              darkTheme: AppTheme.darkTheme, // Use dark them
              themeMode: ThemeMode.system,
              localizationsDelegates: const [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              locale: LangService.to.lang,
              translationsKeys: AppTranslation.translations,
              fallbackLocale: const Locale('en', 'US'),
              supportedLocales: const [Locale('zh', 'CN'), Locale('en', 'US')],
              initialBinding: MainBindings(),
              navigatorObservers: [
                GetXRouterObserver(),
                routeObserver,
                FlutterSmartDialog.observer
              ],
              routingCallback: (routing) {
                if (routing?.current != null) {
                  print('Route changed to: ${routing?.current}');
                  // Call your function here
                  _onRouteChanged(routing?.current);
                }
              },
            ),
          );
        },
      );
    });
  }

  void _onRouteChanged(String? routeName) {
    if (TimerManager().isAnyTimerRunning()) {
      TimerManager().stopAllTimers();
    }
  }
}
