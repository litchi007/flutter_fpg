class UGImageHost {
  static String test5 = '';
  static String git10 = 'https://static10.fhpro.cc';
  static String cdn01 = 'https://cdn01.fhptstatic07.com/';

  static String staticServer01 =
      'https://wwwstatic01.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer02 =
      'https://wwwstatic02.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer03 =
      'https://wwwstatic03.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer04 =
      'https://wwwstatic04.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer05 =
      'https://wwwstatic05.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer06 =
      'https://wwwstatic06.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer07 =
      'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer08 =
      'https://wwwstatic08.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer09 =
      'https://wwwstatic09.fdgdggduydaa008aadsdf008.xyz';
  static String staticServer10 =
      'https://wwwstatic10.fdgdggduydaa008aadsdf008.xyz';
}

List BlueNum = [3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48];
List RedNum = [
  1,
  2,
  7,
  8,
  12,
  13,
  18,
  19,
  23,
  24,
  29,
  30,
  34,
  35,
  40,
  45,
  46,
];
List GreenNum = [
  5,
  6,
  11,
  16,
  17,
  21,
  22,
  27,
  28,
  32,
  33,
  38,
  39,
  43,
  44,
  49,
];

List<int> SquareColors = [
  0xFF2BC610,
  0xFFe1d463,
  0xFF008bf9,
  0xFF4c4d51,
  0xFFf47a00,
  0xFF63d2d2,
  0xFF420aff,
  0xFFaea6a6,
  0xFFff0400,
  0xFF770100,
  0xFF2bc610
];

// 货币类型
final currencyLogo = {
  'CNY': '¥',
  'USD': '\$',
  'EUR': '€',
  'VND': '₫',
  'MYR': 'RM',
  'THB': '฿',
  'INR': '₹',
  'IDR': 'IDR',
  'JPY': '¥',
};

class GameTypeItem {
  final String name;
  final String key;
  GameTypeItem({required this.name, required this.key});
}

final List<GameTypeItem> realGameTypes = [
  GameTypeItem(name: '视讯', key: 'real'),
  GameTypeItem(name: '棋牌', key: 'card'),
  GameTypeItem(name: '电子', key: 'game'),
  GameTypeItem(name: '电竞', key: 'esport'),
  GameTypeItem(name: '捕鱼', key: 'fish'),
  GameTypeItem(name: '体育', key: 'sport'),
  // GameTypeItem(name: '区块链', key: 'blockchain'),
];

class BankConst {
  static const String BANK = '1'; // 银行卡
  static const String ALI = '2'; // 支付宝
  static const String WX = '3'; // 微信
  static const String BTC = '4'; // 虚拟币
  static const String Mtzc = '-1'; // 免提直充
}
