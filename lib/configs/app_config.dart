import 'package:flutter/foundation.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/configs/favcor.dart';
import 'package:fpg_flutter/configs/pushhelper.dart';
import 'package:fpg_flutter/app/resource/icon_resource.dart';
import 'package:fpg_flutter/data/models/UGUserCenterItem.dart';
import 'package:fpg_flutter/services/global_service.dart';
import 'package:fpg_flutter/utils/app_logger.dart';

// https://gsyahcot002uqmhd.aaasfjyt.com/
enum GetAppConfigEnum {
  // test("https://gsyahcot002uqmhd.aaasfjyt.com/wjapp/api.php?", "test"),
  test("https://gsyahcot002uqmhd.aaasfjyt.com/wjapp/api.php?", "test"),
  // test("http://jpapp.fhcdn.cc/wjapp/api.php?", "test"),
  //test("http://pnpbgwt0244arlvvcv.nnnshijy.com", "test"),
  pre("", "pre"),
  release("", "release");

  final String url;

  /// test 测试环境 pre - 预生产 release - 正式环境
  final String mode;
  const GetAppConfigEnum(this.url, this.mode);

  /// 获取基础路径
  /// *[type] test 测试环境 pre - 预生产 release - 正式环境
  /// 没有对应环境默认测试环境
  static String getUrl(String type) {
    return GetAppConfigEnum.values
        .firstWhere((element) => element.mode == type,
            orElse: () => GetAppConfigEnum.test)
        .url;
  }

  // static String getValue(String type) {
  //   return ListEnum.values.firstWhere((element) => element.type == type).type;
  // }

  // static String getBg(String type) {
  //   return ListEnum.values.firstWhere((element) => element.type == type).bg;
  // }
}

class UGSkinType1 {
  // static const String 经典1蓝 = '经典1蓝';
  // static const String DOY钱包 = 'DOY钱包';
  // Define more skin types as needed
}

class AppConfig {
  static bool isLanguagesEnable() {
    return true; // Replace with your specific logic for isLanguagesEnable
  }

  static bool isWNZBottomTabHot() {
    return false; // Replace with your specific logic for isWNZBottomTabHot
  }

  static bool isTM_AB() {
    List<String> sites = ['f013a', 'f089', 'f122', 'f122b']; // Example list
    return AppDefine.inSites(sites);
  }

  static bool hideNewsBlock() {
    return AppDefine.inSites(
      'f017b,f028,f018,f036c,f051,f053b,f056b,f056c,f058b,f061,f062,f062c,f063,f065,f067,f068,f071,f072,f073,f075,f076,f078,f079,f080,f085,f123',
    );
  }

  static bool hideTopBanner() {
    return AppDefine.inSites(
      'f036c',
    );
  }

  static int displayNums() {
    return AppDefine.inSites('f036c') ? 4 : 3;
  }

  // 腰部广告图显示位置判断
  static bool showHomeAdAboveGameList() {
    return !AppDefine.inSites('f022,f036c,f036d,f053b,f056b,f058b,f070,f070b');
  }

  static List<UGUserCenterItem> userCenterItems = [
    UGUserCenterItem(
      code: getUserCenterTypeFromValue(8),
      name: '额度转换',
      logo: img_mobileTemplate(
        '38',
        AppDefine.inSites('f022') ? 'wdqb_f022' : 'wdqb',
      ),
      show: !AppDefine.inSites('f065,f028,f036c,f053b,f058b,f056b,f056c'),
    ),
    UGUserCenterItem(
      code: getUserCenterTypeFromValue(1),
      name: AppDefine.inSites('f061,f116') ? '存取款' : '存款',
      logo: img_mobileTemplate(
        '38',
        AppDefine.inSites('f022') ? 'tzjl_f022' : 'tzjl',
      ),
      show: !AppDefine.inSites('f065'),
    ),
    UGUserCenterItem(
      code: getUserCenterTypeFromValue(2),
      name: AppDefine.inSites('f036c') ? '提款' : '取款',
      logo: img_mobileTemplate('38', 'wdqb'),
      show: AppDefine.inSites('f028,f036c,f053b,f058b,f056b,f056c'),
    ),
    UGUserCenterItem(
      code: getUserCenterTypeFromValue(6),
      name: '未结注单',
      logo: img_mobileTemplate(
        '38',
        AppDefine.inSites('f022') ? 'wjzd_f022' : 'wjzd',
      ),
      show: !AppDefine.inSites('f065'),
    ),
    UGUserCenterItem(
      code: getUserCenterTypeFromValue(32),
      name: '交易记录',
      logo: img_mobileTemplate(
        '38',
        AppDefine.inSites('f022') ? 'jyjl_f022' : 'jyjl',
      ),
      show: !AppDefine.inSites('f065'),
    ),
    UGUserCenterItem(
      code: getUserCenterTypeFromValue(10),
      name: AppDefine.inSites('f022') ? '安全中心' : '设置中心',
      logo: img_mobileTemplate(
        '38',
        AppDefine.inSites('f022') ? 'szzx_f022' : 'szzx',
      ),
      show: true,
    )
  ].where((v) => v.show == true).toList();

  static List<UGUserCenterItem> getDefaultUserCenterItems({autoBet, agent}) {
    final int qdSwitch = GlobalService.to.userInfo.value?.qdSwitch ?? 0;
    final bool hideSuggest =
        GlobalService.to.userInfo.value?.hideSuggest ?? false;
    final String webName = AppDefine.systemConfig?.webName ?? '';
    final String mobileTemplateCategory =
        AppDefine.systemConfig?.mobileTemplateCategory ?? '';

    if (mobileTemplateCategory != '38' ||
        (mobileTemplateCategory == '38' &&
            AppDefine.inSites(
              'f078,f080,f075,f076,f061,f062c,f065,f067,f071,f022,f028,f029,f036c,f053b,f056c,f058b,f116',
            ))) {
      return [];
    }

    return [
      UGUserCenterItem(
        code: UGUserCenterType.activityBonus,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-activity_f022' : 'dlsq1',
        ),
        name: '优惠申请大厅',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.taskCenter,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-activity_f022' : 'menu-activity',
        ),
        name: '任务大厅',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.fundConversion,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'zjzh_f022' : 'zjzh',
        ),
        name: '资金转换',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.dailySignIn,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-betting_f022' : 'menu-betting',
        ),
        name: '每日签到',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.chatRoom,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'liaot_f022' : 'liaot',
        ),
        name: '聊天室',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.grabOrder,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'user_grab_f022' : 'user_grab',
        ),
        name: '抢单',
        show: qdSwitch == 1,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.securityCenter,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-password_f022' : 'menu-password',
        ),
        name: '安全中心',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.automaticBet,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'autoLottery_f022' : 'autoLottery',
        ),
        name: '挂机投注',
        show: autoBet,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.deposit,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'money_f022' : 'money',
        ),
        name: '资金管理',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.bankCardManagement,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'bank_f022' : 'bank',
        ),
        name: '提款账户',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.settledOrder,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-account_f022' : 'menu-account',
        ),
        name: '今日已结',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.lotteryOrderRecord,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-rule-1_f022' : 'menu-rule-1',
        ),
        name: '彩票注单',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.liveOrder,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-rule_f022' : 'menu-rule',
        ),
        name: AppDefine.siteId == 'f022' ? '真人注单' : '其它注单',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.recommendedIncome,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'dlsq1_f022' : 'dlsq1',
        ),
        name: '代理申请',
        show: !agent && AppDefine.siteId != 'f025',
      ),
      UGUserCenterItem(
        code: UGUserCenterType.siteMessage,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-notice_f022' : 'menu-notice',
        ),
        name: '站内信',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.feedback,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'menu-transfer_f022' : 'menu-transfer',
        ),
        name: '建议反馈',
        show: !hideSuggest,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.interestTreasure,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'lxb_f022' : 'lxb',
        ),
        name: '利息宝',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.recommendedIncome,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'dlsq1_f022' : 'dlsq1',
        ),
        name: '推荐收益',
        show: agent,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.aboutApp,
        logo: img_mobileTemplate(
          '38',
          AppDefine.siteId == 'f022' ? 'gyzla_f022' : 'gyzla',
        ),
        name: '关于${AppDefine.systemConfig?.webName ?? ''}',
        show: true,
      ),
      UGUserCenterItem(
        code: UGUserCenterType.redEnvelope,
        logo:
            'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz/views/mobileTemplate/38/images/envelope.png',
        name: '红包中心',
        show: true,
      ),
    ].where((v) => v.show == true).toList();
  }

  static final defaultUserCenterLogos = {
    1: img_mobileTemplate('38', 'tzjl'), // 存款
    2: img_mobileTemplate('38', 'tzjl'), // 取款
    3: img_mobileTemplate('38', 'bank'), // 银行卡管理
    4: img_mobileTemplate('38', 'lxb'), // 利息宝
    5: img_mobileTemplate('38', 'dlsq1'), // 推荐收益
    6: img_mobileTemplate('38', 'menu-rule-1'), // 彩票注单记录
    7: img_mobileTemplate('38', 'menu-rule'), // 其他注单记录
    8: img_mobileTemplate('38', 'wdqb'), // 额度转换
    9: img_mobileTemplate('38', 'menu-notice'), // 站内信
    10: img_mobileTemplate('38', 'menu-password'), // 安全中心
    11: img_mobileTemplate('38', 'menu-activity'), // 任务中心
    12: img_mobileTemplate('38', 'menu-notice'), // 个人信息
    13: img_mobileTemplate('38', 'menu-transfer'), // 建议反馈
    14: img_mobileTemplate('38', 'liaot'), // 在线客服 X
    15: img_mobileTemplate('38', 'dlsq1'), // 活动彩金
    16: img_mobileTemplate('38', 'wdlha'), // 长龙助手
    17: img_mobileTemplate('38', 'wdlha'), // 全民竞猜
    18: img_mobileTemplate('38', 'wdlha'), // 开奖走势
    19: img_mobileTemplate('38', 'wdlha'), // QQ客服
    20: img_mobileTemplate('38', 'wdlha'), // 開獎網
    21: img_mobileTemplate('38', 'wjzd'), // UCI_投注记录
    22: img_mobileTemplate('38', 'wjzd'), // UCI_电子注单
    23: img_mobileTemplate('38', 'wjzd'), // UCI_真人注单
    24: img_mobileTemplate('38', 'wjzd'), // UCI_棋牌注单
    25: img_mobileTemplate('38', 'wjzd'), // UCI_捕鱼注单
    26: img_mobileTemplate('38', 'wjzd'), // UCI_电竞注单
    27: img_mobileTemplate('38', 'wjzd'), // UCI_体育注单
    28: img_mobileTemplate('38', 'wjzd'), // UCI_UG注单
    29: img_mobileTemplate('38', 'wjzd'), // UCI_已结注单
    30: img_mobileTemplate('38', 'jyjl'), // UCI_充值纪录
    31: img_mobileTemplate('38', 'jyjl'), // UCI_提现纪录
    32: img_mobileTemplate('38', 'jyjl'), // UCI_资金明细
    33: img_mobileTemplate('38', 'wjzd'), // UCI_活动大厅
    34: img_mobileTemplate('38', 'liaot'), // 聊天室
    35: img_mobileTemplate('38', 'menu-notice'), // 我的关注
    36: img_mobileTemplate('38', 'menu-notice'), // 我的动态
    37: img_mobileTemplate('38', 'menu-notice'), // 我的粉丝
    38: img_mobileTemplate('38', 'user_grab'), // 抢单
    39: img_mobileTemplate('38', 'gyzla'), // 红包活动
    40: 'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz/views/mobileTemplate/38/images/envelope.png', // 红包活动
    102: img_mobileTemplate('38', 'menu-betting'), // 每日签到
  };

  /// http 请求基础路径 debug 模式链接测试环境
  static String get httpUrl =>
      "${Favcor.current.lotteryRequestUrl ?? GetAppConfigEnum.getUrl(mode)}wjapp/api.php?";

  /// 特殊情况设置域名
  static set httpUrl(String value) {
    httpUrl = value;
  }

  /// http 请求超时时间
  static int get connectTimeout => 100000;

  /// 当前环境
  // static String mode = "test";
  static String mode = "test";

  static bool get isDebug => kDebugMode;

  /// 设置模式
  /// *[value] test 测试环境 pre - 预生产 release - 正式环境
  static void setMode(String value) {
    AppConfig.mode = value;
  }

  /// 上报错误路径
  static String reportUrl =
      "https://api.telegram.org/bot7428137916:AAEr2twz-5C0m0IyzDqE41qESBqZtZY7Js8/sendMessage";

  /// 上报chat_id
  static String reportChatId = "7264041568";

  static String visitorsName = "46da83e1773338540e1e1c973f6c8a68";
  static String visitorsPwd = "46da83e1773338540e1e1c973f6c8a68";

  static pendingOrderNewWindow() =>
      ['f036', 'f053', 't002'].contains(AppDefine.siteId?.substring(0, 4));

  static isHideSQSM() => false;

  static bool isHidePaging() => ['t002'].contains(AppDefine.keyConfig?.siteId);
}
