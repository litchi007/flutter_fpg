enum SeriesId {
  lottery, // 彩票
  live, // 真人
  fishing, // 捕鱼
  electronic, // 电子
  chess, // 棋牌
  sports, // 体育
  esports, // 电竞
  navigation, // 导航链接
  chat, // 聊天室
  data, // 资料
  lotteryHall, // 彩票大厅
  lotteryData, // 六合彩资料
  blockchain, // 区块链
}

extension SeriesIdExtension on SeriesId {
  int get value {
    switch (this) {
      case SeriesId.live:
        return 2;
      case SeriesId.fishing:
        return 3;
      case SeriesId.electronic:
        return 4;
      case SeriesId.chess:
        return 5;
      case SeriesId.sports:
        return 6;
      case SeriesId.esports:
        return 8;
      case SeriesId.navigation:
        return 7;
      case SeriesId.chat:
        return 9;
      case SeriesId.data:
        return 10;
      case SeriesId.lotteryHall:
        return 12;
      case SeriesId.lotteryData:
        return 14;
      case SeriesId.blockchain:
        return 15;
      default:
        return -1; // Handle unknown cases or default value
    }
  }
}
