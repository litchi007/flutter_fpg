import 'package:fpg_flutter/configs/key_config.dart';

class FavcorModel {
  String? name;
  String? requestUrl;
  String? appId;
  String? lotteryRequestUrl;
  FavcorModel({this.name, this.requestUrl, this.appId, this.lotteryRequestUrl});

  FavcorModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    requestUrl = json['requestUrl'];
    appId = json['appId'];
    lotteryRequestUrl = json['lotteryRequestUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['name'] = name;
    data['requestUrl'] = requestUrl;
    data['appId'] = appId;
    data['lotteryRequestUrl'] = lotteryRequestUrl;
    return data;
  }
}

class Favcor {
  static Map<String, FavcorModel> data = {
    "t002": FavcorModel(
        name: "t002",
        requestUrl: "http://jpapp.fhcdn.cc/",
        appId: '7',
        lotteryRequestUrl: 'https://gsyahcot002uqmhd.aaasfjyt.com/'),
    "f036c": FavcorModel(
      name: "f036c",
      requestUrl: "https://1ultxs38.com/",
      appId: '54',
      lotteryRequestUrl: "https://1ultxs38.com/",
    )
  };

  static String currentName = "f036c";

  static FavcorModel current = data[currentName] ??
      FavcorModel(
        name: "t002",
        requestUrl: "http://jpapp.fhcdn.cc/",
        appId: '7',
      );
}
