import 'package:flutter/material.dart';
import 'package:fpg_flutter/configs/app_define.dart';
import 'package:fpg_flutter/constants/assets.dart';
import 'package:fpg_flutter/models/game_play_odds_model/game_play_odds_model.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play.dart';
import 'package:fpg_flutter/models/game_play_odds_model/play_group.dart';
import 'package:fpg_flutter/utils/event_util.dart';
import 'package:fpg_flutter/utils/log_util.dart';

class LotteryConfig {
  /// 六合彩获取背景
  static AssetImage getBallBg(Play value, GamePlayOddsModel? details) {
    if (details?.setting?.blueBalls
            ?.contains(int.tryParse(value.name ?? "0").toString()) ??
        false) {
      return const AssetImage(Assets.assetsImagesBallBlue3);
    }
    if (details?.setting?.greenBalls
            ?.contains(int.tryParse(value.name ?? "0").toString()) ??
        false) {
      return const AssetImage(Assets.assetsImagesBallGreen3);
    }
    final isDouble = double.tryParse(value.name ?? "");
    if (isDouble != null) {
      return const AssetImage(Assets.assetsImagesBallRed3);
    }
    return const AssetImage("");
  }

  /// 快三骰子
  static String getQuick3Img(String value) {
    return {
          "1": Assets.assetsImagesDiceColor1,
          "2": Assets.assetsImagesDiceColor2,
          "3": Assets.assetsImagesDiceColor3,
          "4": Assets.assetsImagesDiceColor4,
          "5": Assets.assetsImagesDiceColor5,
          "6": Assets.assetsImagesDiceColor6,
        }[value] ??
        Assets.assetsImagesDiceColor1;
  }

  static const OPENDATA_GAMEIDS = [
    '1',
    '10',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '50',
    '70',
  ];
  static const OPENDATA_FROMTYPES = ['1', '10', '50'];
  static const OPENDATA_GAMETYPES = [
    'europelhc',
    'ugamlhc',
    'ugamlhc1',
    'ugamlhc2'
  ];

  /// 显示video
  /// *[id] nextIssueData id
  static showVideo({required String id, required String gameType}) {
    final siteId = AppDefine.siteId ?? "";
    final fromType = {
      "cqssc": '1',
      "qxc": '2',
      "kxcs": '5',
      "fc3d": '6',
      "jsk3": '10',
      "dlt": '12',
      "gd11x5": '21',
      "uspowerball": '32',
      "pk10": '50',
      "xyft": '55',
      "gdkl10": '60',
      "xync": '61',
      "bjkl8": '65',
      "pcdd": '66',
      "ugysc": '67',
      "lhc": '70',
    }[gameType];
    return ((id == '285' && siteId.contains('f090')) ||
        (id == '276' && siteId.contains('f070')) ||
        (id == '275' && siteId.contains('f058')) ||
        (id == '193' && siteId.contains('f056')) ||
        (id == '193' && siteId.contains('f053')) ||
        (id == '270' && siteId.contains('f036')) ||
        OPENDATA_GAMEIDS.contains(id + '') ||
        OPENDATA_FROMTYPES.contains(fromType) ||
        OPENDATA_GAMETYPES.contains(gameType));
  }

  /// 二同号单选tab
  static List<String> rthTabs = ["二同号", "不同号"];

  /// 散步同号
  static List<String> rth3Tabs = ["三不同号"];

  /// 趣味tab
  static List<String> qwTabs = ["定单双", "猜中位"];

  /// 趣味二级tab
  static List<String> secondQwtabs = ["定单双", "猜中位"];

  /// 任选胆拖
  static List<String> optionalTabs = ["胆码", "拖码"];
  static final tabConfig = {
    "路珠": "0",
    "长龙": "1",
  };
  static final roadTypeConfigJssc = {
    "冠亚和": 'gy_nums',
    "冠亚和大小": 'gy_size',
    "冠亚和单双": 'gy_firstsd',
  };
  static final roadTypeConfigSsc = {
    "总和大小": 'total_size',
    "总和单双": 'total_firstsd',
    "龙虎": 'lhh',
  };
  static final roadTypeConfigK3 = {
    "大小": 'total_size',
    "单双": 'total_firstsd',
  };

  /// 形态
  static List<XtList> xtList = [
    XtList(name: "跨度", start: 0, end: 6),
    XtList(name: "顺子通选", start: 6, end: 7),
    XtList(name: "半顺通选", start: 7, end: 8),
    XtList(name: "杂", start: 8, end: 9),
  ];

  /// 三不同和值
  static List<XtList> sbthzList = [
    XtList(name: "三不同大小", start: 0, end: 2),
    XtList(name: "三不同单双", start: 2, end: 4),
  ];

  /// 对子和值
  static List<XtList> dzhzList = [
    XtList(name: "对子大小", start: 0, end: 2),
    XtList(name: "对子单双", start: 2, end: 4),
  ];

  /// 大乐透号码颜色
  /// *[index] 索引
  static dltBalls(int index) {
    return index < 5
        ? Assets.assetsImagesBallDltred
        : Assets.assetsImagesBallDltblue;
  }

  // 尾数参数
  static final nums = {
    "0尾": {"group": 10, "nums": "10,20,30,40"},
    "1尾": {"group": 10, "nums": "01,11,21,31,41"},
    "2尾": {"group": 10, "nums": "02,12,22,32,42"},
    "3尾": {"group": 10, "nums": "03,13,23,33,43"},
    "4尾": {"group": 10, "nums": "04,14,24,34,44"},
    "5尾": {"group": 10, "nums": "05,15,25,35,45"},
    "6尾": {"group": 10, "nums": "06,16,26,36,46"},
    "7尾": {"group": 10, "nums": "07,17,27,37,47"},
    "8尾": {"group": 10, "nums": "08,18,28,38,48"},
    "9尾": {"group": 10, "nums": "09,19,29,39,49"},
  };

  /// 一般-机选结果
  /// 特殊机选未处理
  static RobotRandomSelectResult robotRandomSelect(List<PlayGroup> list,
      [int select = 3]) {
    final current = [].cast<Play>();
    final groups = [].cast<PlayGroup>();
    final plays = [].cast<Play>();
    for (var e in list) {
      current.addAll(e.plays ?? []);
    }
    final indexList = EventUtils.robotRandom((current.length));
    final _list = List.generate(indexList.length, (i) {
      final item = current[indexList[i]];
      return item;
    }).toList();
    plays.addAll(_list);
    for (var e in list) {
      for (var v in plays) {
        if (e.plays?.contains(v) ?? false) {
          groups.add(e);
          break;
        }
      }
    }
    // final currentPlay = List.generate(indexList.length, (i) => current[indexList[i]]);
    return RobotRandomSelectResult(currentGroup: groups, currentPlay: plays);
  }

  /// 二字定位，三字定位，四字定位生成算法
  /// *[input] [[1,2], [3,4]] => [[1,3], [1,4], [2,3], [2,4]]
  static List<List<int>> generateCombinationsList(List<List<int>> input) {
    List<List<int>> combinations = [];
    // input.sort((a, b) => a.length - b.length);
    void generateCombinations(
        List<List<int>> lists, List<int> current, int index) {
      if (index == lists.length) {
        combinations.add(List.from(current));
        return;
      }
      for (int i = 0; i < lists[index].length; i++) {
        current.add(lists[index][i]);
        generateCombinations(lists, current, index + 1);
        current.removeLast();
      }
    }

    generateCombinations(input, [], 0);
    return combinations;
  }

  /// 连码生成算法
  /// *[input]
  static List<List<int>> lmGenerateCombinationsList(List<int> input) {
    List<List<int>> combinations = [];

    void generateCombinations(List<int> current, int index) {
      if (current.length == 3) {
        combinations.add(List.from(current));
        return;
      }

      for (int i = index; i < input.length; i++) {
        if (!current.contains(input[i])) {
          current.add(input[i]);
          generateCombinations(current, i + 1);
          current.removeLast();
        }
      }
    }

    generateCombinations([], 0);
    return combinations;
  }

  /// [组选6复式] [组选3复式] [五码黑] 生成算法
  static List<List<int>> gen2Random(List<List<int>> numbers) {
    List<List<int>> combinations = [];
    if (numbers.length < 5) {
      return [];
    }
    for (var num1 in numbers[0]) {
      for (var num2 in numbers[1]) {
        for (var num3 in numbers[2]) {
          for (var num4 in numbers[3]) {
            for (var num5 in numbers[4]) {
              combinations.add([num1, num2, num3, num4, num5]);
            }
          }
        }
      }
    }
    return combinations;
  }

  /// [四码黑] [四码红] 算法
  /// * [numbers] -> [1,2,3,4,5]
  /// * [count] ->  [四码黑] [四码红] 4 [五码黑] 5
  static List<List<int>> gen4Black(List<int> numbers, [int count = 4]) {
    List<List<int>> combinations = [];

    void generateCombinations(List<int> currentCombination, int start) {
      if (currentCombination.length == count) {
        combinations.add(List.from(currentCombination));
        return;
      }

      for (int i = start; i < numbers.length; i++) {
        currentCombination.add(numbers[i]);
        generateCombinations(currentCombination, i + 1);
        currentCombination.removeLast();
      }
    }

    generateCombinations([], 0);
    return combinations;
  }

  /// 三不同号 算法
  /// *[numbers] [1,2,3,4] => [[1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4]]
  static List<List<int>> threeBthGenRandom(List<int> numbers) {
    List<List<int>> combinations = [];

    for (int i = 0; i < numbers.length; i++) {
      for (int j = i + 1; j < numbers.length; j++) {
        for (int k = j + 1; k < numbers.length; k++) {
          combinations.add([numbers[i], numbers[j], numbers[k]]);
        }
      }
    }
    return combinations;
  }

  /// 六合彩 连尾生成算法
  /// *[numbers] [1,2,3] => [[1,2], [1,3], [2,3]]
  static List<List<int>> lhcLwGenList(List<int> numbers) {
    List<List<int>> combinations = [];

    for (int i = 0; i < numbers.length; i++) {
      for (int j = i + 1; j < numbers.length; j++) {
        combinations.add([numbers[i], numbers[j]]);
      }
    }
    return combinations;
  }

  /// 复式算法
  static List<List<int>> combineArray(List<int> source, int count,
      [bool isPermutation = false]) {
    List<List<int>> currentList = source.map((item) => [item]).toList();
    if (count == 1) {
      return currentList;
    }
    List<List<int>> result = [];
    for (int i = 0; i < currentList.length; i++) {
      List<int> current = List.from(currentList[i]);
      List<List<int>> children = [];
      if (isPermutation) {
        children = combineArray(
          source.where((item) => item != current[0]).toList(),
          count - 1,
          isPermutation,
        );
      } else {
        children = combineArray(
          source.sublist(i + 1),
          count - 1,
          isPermutation,
        );
      }
      for (var child in children) {
        result.add([...current, ...child]);
      }
    }
    return result;
  }

  /// 组选3，组选6 组合数算法
  /// *[arr] -> [1,2,3]
  /// *[code] -> "ZXSFS"|"ZXLFS"
  static List<List<int>> generateCombinations(List<int> arr,
      [String? code = "ZXSFS"]) {
    // final list = List.generate(3, (i) => [arr[i], -1, arr[i]]);
    // 每个数字都有三个组合
    List<List<int>> result = [];

    if (code == "DWDZXSFS") {
      final list = [
        [0, 0, -1],
        [0, -1, 0],
        [-1, 0, 0]
      ];

      for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr.length; j++) {
          if (j == i) {
            continue;
          }
          // 遍历list
          final item = list.map((e) {
            final tem = e.map((q) => q == 0 ? arr[i] : arr[j]).toList();
            return tem;
          }).toList();
          result.addAll(item);
        }
      }
    }
    if (code == "DWDZXLFS") {
      for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr.length; j++) {
          if (i == j) {
            continue;
          }
          for (var q = 0; q < arr.length; q++) {
            if (q == i || q == j) {
              continue;
            }
            result.add([arr[i], arr[j], arr[q]]);
          }
        }
      }
    }
    LogUtil.w("result____$result $code");
    return result;
  }

  static getWebUrl(
      {String? host,
      String? lotteryId,
      String? gameType,
      String? id,
      String? siteId,
      String? token}) {
    return "${host}Open_prize/index.php?$lotteryId";
  }

  static getVideoUrl({
    String? host,
    String? id,
    String? siteId,
    String? gameType,
  }) {
    return "$host/open_prize/video/${gameType ?? 'cqssc'}/index.html?gameId=$id&siteCode=$siteId&difftime=0&navhidden=1";
  }

  // 判断站点（多个站点用英文逗号隔开），安卓 siteId 是模拟的
  static bool inSites(dynamic sites, String? siteId) {
    if (sites is List<String>) {
      sites = sites.join(',');
    }

    return (sites
                ?.toString()
                .toLowerCase()
                .split(',')
                .where((v) => v == siteId?.toLowerCase())
                .length ??
            0) >
        0;
  }

  static String emojiBaseUrl =
      "https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz/images/arclist";

  static List<String> emojiList = List.generate(103, (e) => "${e + 1}");
}

class RobotRandomSelectResult {
  List<PlayGroup>? currentGroup;
  List<Play>? currentPlay;

  RobotRandomSelectResult({this.currentGroup, this.currentPlay});

  RobotRandomSelectResult.fromJson(Map<String, dynamic> json) {
    currentGroup = json['currentGroup'] != null
        ? (json['currentGroup'] as List<dynamic>)
            .map((e) => PlayGroup.fromJson(e))
            .toList()
            .toList()
        : null;
    currentPlay = json['currentPlay'] != null
        ? (json['currentPlay'] as List<dynamic>)
            .map((e) => Play.fromJson(e))
            .toList()
        : null;
  }
}

class XtList {
  String? name;
  int? start;
  int? end;

  XtList({this.name, this.start, this.end});

  XtList.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    start = json['start'];
    end = json['end'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['start'] = start;
    data['end'] = end;
    return data;
  }
}
