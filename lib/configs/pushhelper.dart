import 'package:get/get.dart';
import 'package:fpg_flutter/routes.dart';
import 'package:fpg_flutter/router/router.dart';
import 'package:fpg_flutter/app/component/home_menu/enum/ug_link_position_type.dart';
import 'package:fpg_flutter/configs/app_enum.dart';

enum UGUserCenterType {
  deposit, // 存款
  withdrawal, // 取款
  bankCardManagement, // 银行卡管理
  interestTreasure, // 利息宝
  recommendedIncome, // 推荐收益
  lotteryOrderRecord, // 彩票注单记录
  otherOrderRecord, // 其他注单记录
  balanceConversion, // 额度转换
  siteMessage, // 站内信
  securityCenter, // 安全中心
  taskCenter, // 任务中心
  personalInfo, // 个人信息
  feedback, // 建议反馈
  onlineCustomerService, // 在线客服
  activityBonus, // 活动彩金
  longDragonAssistant, // 长龙助手
  allPeopleQuiz, // 全民竞猜
  resultTrend, // 开奖走势
  qqCustomerService, // QQ客服
  lotteryWebsite, // 开奖网
  pendingOrder, // 未结注单
  electronicOrder, // 电子注单
  liveOrder, // 真人注单
  chessOrder, // 棋牌注单
  fishingOrder, // 捕鱼注单
  esportsOrder, // 电竞注单
  sportsOrder, // 体育注单
  ugOrder, // UG注单
  settledOrder, // 已结注单
  depositRecord, // 存款纪录
  withdrawalRecord, // 取款纪录
  fundDetails, // 资金明细
  promotionalActivities, // 优惠活动
  chatRoom, // 聊天室
  myFavorites, // 我的关注
  myDynamics, // 我的动态
  myFans, // 我的粉丝
  grabOrder, // 抢单
  redEnvelope, // 红包
  automaticBet, // 挂机投注
  fundConversion, // 资金转换
  liveHall, // 真人大厅
  chessHall, // 棋牌大厅
  gameOrder, // 游戏注单
  electronicHall, // 电子大厅
  sportsHall, // 体育大厅
  esportsHall, // 电竞大厅
  lotteryHall, // 彩票大厅
  fishingHall, // 捕鱼大厅
  roadBeads, // 路珠
  sixHarmoniesAssistant, // 六合助手
  historicalPosts, // 历史帖子
  dailySignIn, // 每日签到
  logout, // 登出
  gameLobby, // 游戏大厅
  myPage, // 我的页
  loginPage, // 登录页
  registerPage, // 注册页
  result, // 开奖结果
  goldenEgg, // 砸金蛋
  scratchCard, // 刮刮乐
  taskPopup, // 任务弹窗
  realTimeOrder, // 即时注单
  sleepyCards, // 眯牌
  vipLevel, // VIP等级
  myOrders, // 我的注单
  changeLoginPassword, // 修改登录密码
  changeTransactionPassword, // 修改交易密码
  secondaryVerification, // 二次验证
  commonLoginLocations, // 常用登录地
  bettingReport, // 投注报表
  roulette, // 转盘
  appDownload, // APP下载
  withdrawalAccount, // 提款账号
  receiveSalary, // 领取俸禄
  mySixHarmonies, // 我的六合
  aboutApp, // 关于APP
  blockchainHall, // 区块链大厅
  leaderboard, // 排行榜
  lottery, // 彩票游戏
  redEnvelopeRecord, // 红包记录
  redEnvelopeRecord2, // 扫雷记录
}

// Function to get enum from value
UGUserCenterType? getUserCenterTypeFromValue(int value) {
  switch (value) {
    case 1:
      return UGUserCenterType.deposit;
    case 2:
      return UGUserCenterType.withdrawal;
    case 3:
      return UGUserCenterType.bankCardManagement;
    case 4:
      return UGUserCenterType.interestTreasure;
    case 5:
      return UGUserCenterType.recommendedIncome;
    case 6:
      return UGUserCenterType.lotteryOrderRecord;
    case 7:
      return UGUserCenterType.otherOrderRecord;
    case 8:
      return UGUserCenterType.balanceConversion;
    case 9:
      return UGUserCenterType.siteMessage;
    case 10:
      return UGUserCenterType.securityCenter;
    case 11:
      return UGUserCenterType.taskCenter;
    case 12:
      return UGUserCenterType.personalInfo;
    case 13:
      return UGUserCenterType.feedback;
    case 14:
      return UGUserCenterType.onlineCustomerService;
    case 15:
      return UGUserCenterType.activityBonus;
    case 16:
      return UGUserCenterType.longDragonAssistant;
    case 17:
      return UGUserCenterType.allPeopleQuiz;
    case 18:
      return UGUserCenterType.resultTrend;
    case 19:
      return UGUserCenterType.qqCustomerService;
    case 20:
      return UGUserCenterType.lotteryWebsite;
    case 21:
      return UGUserCenterType.pendingOrder;
    case 22:
      return UGUserCenterType.electronicOrder;
    case 23:
      return UGUserCenterType.liveOrder;
    case 24:
      return UGUserCenterType.chessOrder;
    case 25:
      return UGUserCenterType.fishingOrder;
    case 26:
      return UGUserCenterType.esportsOrder;
    case 27:
      return UGUserCenterType.sportsOrder;
    case 28:
      return UGUserCenterType.ugOrder;
    case 29:
      return UGUserCenterType.settledOrder;
    case 30:
      return UGUserCenterType.depositRecord;
    case 31:
      return UGUserCenterType.withdrawalRecord;
    case 32:
      return UGUserCenterType.fundDetails;
    case 33:
      return UGUserCenterType.promotionalActivities;
    case 34:
      return UGUserCenterType.chatRoom;
    case 35:
      return UGUserCenterType.myFavorites;
    case 36:
      return UGUserCenterType.myDynamics;
    case 37:
      return UGUserCenterType.myFans;
    case 38:
      return UGUserCenterType.grabOrder;
    case 39:
      return UGUserCenterType.redEnvelope;
    case 40:
      return UGUserCenterType.automaticBet;
    case 41:
      return UGUserCenterType.fundConversion;
    case 42:
      return UGUserCenterType.liveHall;
    case 43:
      return UGUserCenterType.chessHall;
    case 44:
      return UGUserCenterType.electronicHall;
    case 45:
      return UGUserCenterType.sportsHall;
    case 46:
      return UGUserCenterType.esportsHall;
    case 47:
      return UGUserCenterType.lotteryHall;
    case 48:
      return UGUserCenterType.fishingHall;
    case 55:
      return UGUserCenterType.roadBeads;
    case 60:
      return UGUserCenterType.sixHarmoniesAssistant;
    case 61:
      return UGUserCenterType.historicalPosts;
    case 102:
      return UGUserCenterType.dailySignIn;
    case 103:
      return UGUserCenterType.logout;
    case 104:
      return UGUserCenterType.gameLobby;
    case 105:
      return UGUserCenterType.myPage;
    case 106:
      return UGUserCenterType.loginPage;
    case 107:
      return UGUserCenterType.registerPage;
    case 109:
      return UGUserCenterType.result;
    case 110:
      return UGUserCenterType.goldenEgg;
    case 111:
      return UGUserCenterType.scratchCard;
    case 112:
      return UGUserCenterType.taskPopup;
    case 113:
      return UGUserCenterType.realTimeOrder;
    case 114:
      return UGUserCenterType.sleepyCards;
    case 115:
      return UGUserCenterType.vipLevel;
    case 116:
      return UGUserCenterType.myOrders;
    case 117:
      return UGUserCenterType.changeLoginPassword;
    case 118:
      return UGUserCenterType.changeTransactionPassword;
    case 119:
      return UGUserCenterType.secondaryVerification;
    case 120:
      return UGUserCenterType.commonLoginLocations;
    case 121:
      return UGUserCenterType.bettingReport;
    case 122:
      return UGUserCenterType.roulette;
    case 123:
      return UGUserCenterType.appDownload;
    case 124:
      return UGUserCenterType.withdrawalAccount;
    case 125:
      return UGUserCenterType.receiveSalary;
    case 126:
      return UGUserCenterType.mySixHarmonies;
    case 127:
      return UGUserCenterType.aboutApp;
    case 128:
      return UGUserCenterType.blockchainHall;
    case 129:
      return UGUserCenterType.leaderboard;
    case 264:
      return UGUserCenterType.lottery;
    default:
      return null;
  }
}

class PushHelper {
  static void pushUserCenterType(UGUserCenterType code) {
    switch (code) {
      case UGUserCenterType.mySixHarmonies:
        Get.toNamed(myLhePath);
      case UGUserCenterType.activityBonus:
        Get.toNamed(promotePath);
      case UGUserCenterType.taskCenter:
        Get.toNamed(taskPath);
      case UGUserCenterType.fundConversion:
        Get.toNamed(newalipayTransInPath);
      case UGUserCenterType.dailySignIn:
        Get.toNamed(mobilePath);
      case UGUserCenterType.chatRoom:
        Get.toNamed(chatRoomListPath);
      case UGUserCenterType.grabOrder:
        Get.toNamed(grabPath);
      case UGUserCenterType.securityCenter:
        Get.toNamed(safeCenterPath);
      case UGUserCenterType.deposit:
        Get.toNamed(depositPath);
      case UGUserCenterType.bankCardManagement:
        Get.toNamed(manageBankAccountsPath);
      case UGUserCenterType.settledOrder:
        Get.toNamed(AppRoutes.todaySettled);
      case UGUserCenterType.lotteryOrderRecord:
        Get.toNamed(AppRoutes.lotteryTickets);
      case UGUserCenterType.liveOrder:
        Get.toNamed(realBetFishPath);
      case UGUserCenterType.siteMessage:
        Get.toNamed(userMessagePath);
      case UGUserCenterType.interestTreasure:
        Get.toNamed(interestTreasurePath);
      case UGUserCenterType.recommendedIncome:
        Get.toNamed(myrecoPath);
      case UGUserCenterType.personalInfo:
        Get.toNamed(myInfoPath);
      case UGUserCenterType.balanceConversion:
        Get.toNamed(realtransPath);
      case UGUserCenterType.onlineCustomerService:
        Get.toNamed(onlineServicePath);
      case UGUserCenterType.withdrawal:
        Get.toNamed(depositPath, arguments: [1]);
      case UGUserCenterType.feedback:
        Get.toNamed(feedbackPath);
      case UGUserCenterType.longDragonAssistant:
        Get.toNamed(AppRoutes.longDragonTool);
      case UGUserCenterType.allPeopleQuiz:
        Get.toNamed(allPeopleQuizPath);
      case UGUserCenterType.resultTrend:
        Get.toNamed("${AppRoutes.lotteryTrend}/-1");
      case UGUserCenterType.pendingOrder:
        Get.toNamed(weekPath);
      case UGUserCenterType.aboutApp:
        Get.toNamed(aboutAppPath);
      case UGUserCenterType.redEnvelope:
        Get.toNamed(redEnvelopPath);
      case UGUserCenterType.lotteryWebsite:
        Get.toNamed(lotteryWebsitePath);
      case UGUserCenterType.promotionalActivities:
        Get.toNamed(promotionListPath);
      case UGUserCenterType.lottery:
        Get.toNamed("${AppRoutes.lottery}/-1");
      case UGUserCenterType.realTimeOrder:
        Get.toNamed(AppRoutes.lotteryLiveBet);
      case UGUserCenterType.result:
        Get.toNamed(AppRoutes.lotteryResult);
      case UGUserCenterType.redEnvelopeRecord:
        Get.toNamed('${AppRoutes.redEnvelope}/1');
      case UGUserCenterType.redEnvelopeRecord2:
        Get.toNamed('${AppRoutes.redEnvelope}/2');
      case UGUserCenterType.myFavorites:
        Get.toNamed(followPath);
      case UGUserCenterType.myDynamics:
        Get.toNamed(focusPath);
      case UGUserCenterType.myFans:
        Get.toNamed(fansPath);
      case UGUserCenterType.fundDetails:
        Get.toNamed(capitalDetailPath);
      default:
        Get.toNamed(accountPath);
    }
  }
  // 跳转到彩票下注页，或内部功能页

  static pushCategory(int linkCategory) {
    if (linkCategory == SeriesId.lottery.value) {
      // this.gotoLottery(linkPosition?.toString());
    }
  }

  /*
   * 跳转 彩票 Jump to lottery
   * @param gameId 香港六合彩 等 彩票 ID param gameId Hong Kong Mark Six Lottery etc. Lottery ID
   */
  static gotoLottery(int gameId) {
    Get.toNamed(realtransPath, arguments: gameId);

    // push(PageName.BetLotteryPage, { lotteryId: gameId } as IBetLotteryPage);
  }
}
