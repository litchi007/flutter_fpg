import 'package:fpg_flutter/app/pages/account/account_view.dart';
import 'package:fpg_flutter/configs/favcor.dart';
import 'package:fpg_flutter/configs/key_config.dart';
import 'package:fpg_flutter/data/models/AppInfoModel.dart';
import 'package:fpg_flutter/data/models/UGSysConfModel.dart';
import 'package:fpg_flutter/data/models/InterfaceModel.dart';
import 'package:fpg_flutter/data/models/UGUserModel.dart';
import 'package:fpg_flutter/utils/app_logger.dart';
import 'package:fpg_flutter/utils/log_util.dart';
import 'package:fpg_flutter/utils/storage_util.dart';
import 'package:get_storage/get_storage.dart';
import 'package:uuid/v4.dart';
import 'package:fpg_flutter/models/app_info_app_id_model/app_info_app_id_model.dart';
import 'package:fpg_flutter/data/models/HomeGamesModel.dart';

class AppDefine {
  // Singleton pattern
  static final AppDefine _instance = AppDefine._internal();

  factory AppDefine() {
    return _instance;
  }

  AppDefine._internal();

  // Configuration parameters
  static String appInfoDomain = "http://jpapp.fhcdn.cc/";
  // static String appInfoDomain = Favcor.current.requestUrl ?? "";
  // static String appInfoDomain = "https://1ultxs38.com/";

  static KeyConfig? keyConfig = KeyConfig();
  static String? siteId = '';
  static String? realSiteId = '';
  static String host = '';

  static String lastDeskey = '';
  static String lastSign = '';

  static List<String> domains = [];
  static String deviceUUID = ''; // Initialized with a default value
  static int versionName = 1; // Provide a default value
  static int versionCode = 1; // Provide a default value
  static String currentVersionInfo = ''; // Initialized with a default value
  static String newestVersionInfo = ''; // Initialized with a default value
  static String defaultAvatar =
      'https://wwwstatic07.fdgdggduydaa008aadsdf008.xyz/views/mobileTemplate/2/images/money-2.png'; // Placeholder for img_assets
  static String languageCode = ''; // Initialized with a default value
  static bool baseScreenSafeTop = false;

  static bool emailEnable = true;
  static String userAgent = ''; // Initialized with a default value
  final int lauchCloseTime = 3000;

  final int buildCheckTime = 3000;
  final int codePushVersion = 0;
  final String hardCodingDomain = '';
  static List<String> staticServers = [];
  static UGSysConfModel? systemConfig;
  static List<IconModel>? homeGamesIcon;
  static UserToken? userToken;

  static String get accessToken {
    String accessToken = StorageUtils().read(StorageKeys.ACCESSTOKEN) ?? '';
    if (accessToken.isEmpty) {
      accessToken = const UuidV4().generate();
      StorageUtils().save(StorageKeys.ACCESSTOKEN, accessToken);
    }
    return accessToken;
  }

  static bool inSites(dynamic sites) {
    List<String> siteList = [];

    if (sites is List<String>) {
      siteList.addAll(sites);
    } else if (sites is String) {
      siteList.addAll(sites.split(','));
    }

    return siteList.any((site) =>
        site.toLowerCase() == (siteId?.toLowerCase() ?? '').toLowerCase());
  }

  static bool lhltMineSet() {
    return AppDefine.inSites(['f090']);
  }

  // static bool isLoggedin() {
  //   return userInfo != null;
  // }

  static String defaultLotteryId() {
    String id;
    switch (siteId) {
      case 'c084':
        id = '164';
        break;
      case 'c208':
        id = '78';
      default:
        id = '70';
    }
    return id;
  }

  /// 是否显示登录图形验证码
  static bool get isShowImageCode {
    if (AppDefine.systemConfig?.loginVCode == true) {
      if (AppDefine.systemConfig?.loginVCodeType == '0') {
        return false;
      } else {
        return true;
      }
    }
    return false;
  }
}
