#!/bin/bash
# CHAT_ID="7264041568"
# CHAT_ID="-1002099812710"
CHAT_ID="-4558947798"
TOKEN="6666500320:AAEgtRgodeX0oBsxCzR_JAZkP29ERM6v6oc"
TEXT="$1"
# TOKEN="6412020153:AAEvBVvuxC4f0VGFtHAOiPgnqL9F0bE8Leg"

SITE="t002"
# grep -oP 'version:\s+\K\d+(\.\d+){2}\+\d+' "./pubspec.yaml"
ANDROID_VERSION="1.0.0+1"
IOS_VERSION="1.0.0+1.0.0"
VERSION="1.0.0"

while IFS= read -r line; do
    # echo "$line"
    # echo grep -oP 'version:\s+\K\d+(\.\d+){2}\+\d+'
    # if  $line
    if [[ "${line}" =~ "version" ]];then
      version=$(echo "$line" | awk -F': ' '{print $2}')
      if [[ -n $version ]]; then
        TEXT="$version"
        echo "Version number: $version"
      fi
    fi
done < './pubspec.yaml'
# 使用date命令获取当前时间
current_time=$(date +"%Y-%m-%d %H:%M:%S")
# 写入的文本内容
text="class AppVersion {static String andridVersion=\""$current_time"\"; static String iosVersion=\""$current_time"\";}"

# 指定文件路径
file_path="./lib/configs/app_version.dart"
rm $file_path
echo "文件 $file_path 已成功删除"
touch $file_path
echo "文件 $file_path 已成功创建"
# 将文本内容写入文件
echo "$text" > "$file_path"
dart format $file_path

perform_curl_request() {
    response=$(curl -F "chat_id=$CHAT_ID"  -F "document=@$1" "https://api.telegram.org/bot$TOKEN/sendDocument")
    if [ $? -eq 0 ]; then
      echo "Request successful"
    else
      echo "Request failed, retrying in seconds..."
      perform_curl_request "$1"
      fi
}

curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n站点 '$SITE'\n[flutter_打包]封装app：打包 【Android】:  \n😋 版本: '$TEXT' \n⌛️  开始打包 \n---"}'  "https://api.telegram.org/bot$TOKEN/sendMessage"
# 更新Android
yes | shorebird release android --artifact=apk

if [ $? -eq 0 ]; then
  curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n站点 '$SITE' \n✅[flutter_打包]封装app：打包完成 【Android】: \n😋  版本: '$TEXT'版本更新完成 \n📦  "}'  "https://api.telegram.org/bot$TOKEN/sendMessage"
  perform_curl_request "./build/app/outputs/flutter-apk/app-release.apk"
else
  echo "\n命令被终止or命令错误,请检查"
fi 

curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n站点 '$SITE' \n[flutter_打包]封装app：打包 【iOS】  \n😋 版本: '$TEXT' \n⌛️   开始打包 \n---"}' "https://api.telegram.org/bot$TOKEN/sendMessage"

# iOS发布到hoc
yes | shorebird release ios --export-method ad-hoc
if [ $? -eq 0 ]; then
  curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n站点 '$SITE' \n✅[flutter_打包]封装app：打包完成 【iOS】 \n😋  版本: '$TEXT'更新完成 \n📦  下载地址: \n---"}'  "https://api.telegram.org/bot$TOKEN/sendMessage"
  perform_curl_request "./build/ios/ipa/fpg_flutter.ipa"
else
  echo "\n命令被终止or命令错误,请检查"
fi
