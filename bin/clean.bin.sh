#!/bin/bash
bin_path=$(cd `dirname $0` || exit; pwd)
cd "$bin_path" || exit
cd  ../
rootPath=$(pwd)

flutter clean 
rm -Rf "$rootPath/ios/Pods"
rm -Rf "$rootPath/ios/.symlinks"
rm -Rf "$rootPath/ios/Flutter/Flutter.framework"
rm -Rf "$rootPath/ios/Flutter/Flutter.podspec"
rm -rf "$rootPath/pubspec.lock"
rm -rf "$rootPath/ios/Podfile.lock"