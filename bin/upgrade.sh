#!/bin/bash
# CHAT_ID="-1002099812710"
# TOKEN="6412020153:AAEvBVvuxC4f0VGFtHAOiPgnqL9F0bE8Leg"

CHAT_ID="-4558947798"
TOKEN="6666500320:AAEgtRgodeX0oBsxCzR_JAZkP29ERM6v6oc"
TEXT="$1"
ANDROID_VERSION="1.0.0+1"
IOS_VERSION="1.0.0+1.0.0"
VERSION="1.0.0"
SITE="t002"
while IFS= read -r line; do
    # echo "$line"
    # echo grep -oP 'version:\s+\K\d+(\.\d+){2}\+\d+'
    # if  $line
    if [[ "${line}" =~ "version" ]];then
      version=$(echo "$line" | awk -F': ' '{print $2}')
      if [[ -n $version ]]; then
        echo "Version number: $version"
        VERSION="$version"
      fi
    fi
done < './pubspec.yaml'

echo "$VERSION"

if [ -z "$VERSION"];then
    echo "版本号为空，请检查pubspec.yaml输入版本号"
    exit
fi

# 使用date命令获取当前时间
current_time=$(date +"%Y-%m-%d %H:%M:%S")
# 写入的文本内容
text="class AppVersion {static String andridVersion=\""$current_time"\"; static String iosVersion=\""_$current_time"\";}"

# 指定文件路径
file_path="./lib/configs/app_version.dart"
rm $file_path
echo "文件 $file_path 已成功删除"
touch $file_path
echo "文件 $file_path 已成功创建"
# 将文本内容写入文件
echo "$text" > "$file_path"
dart format $file_path

curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n⚠️[flutter_热更新]Android:热更新提示  \n😋 版本: '$VERSION' \n📦  开始热更 \n---"}'  "https://api.telegram.org/bot$TOKEN/sendMessage"
# 热更新Android
shorebird patch --platforms=android --release-version=$ANDROID_VERSION 

if [ $? -eq 0 ]; then
  curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n✅[flutter_热更新]Android:热更新成功 \n😋  版本: '$VERSION'更新完成 \n📦  多次重启app查看最新内容 \n---"}'  "https://api.telegram.org/bot$TOKEN/sendMessage"
else
  echo "\n命令被终止or命令错误,请检查"
fi

curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n⚠️[flutter_热更新]iOS:热更新提示  \n😋 版本: '$VERSION' \n📦  开始热更 \n---"}'  "https://api.telegram.org/bot$TOKEN/sendMessage"

# 热更新iOS 
shorebird patch --platforms=ios --release-version=$IOS_VERSION

if [ $? -eq 0 ]; then
  curl -H "Content-Type: application/json" -X POST -d '{"chat_id":"'$CHAT_ID'","text":"--- \n✅[flutter_热更新]iOS:热更新成功 \n😋  版本: '$VERSION'更新完成 \n📦  多次重启app查看最新内容 \n---"}'  "https://api.telegram.org/bot$TOKEN/sendMessage"
else
  echo "\n命令被终止or命令错误,请检查"
fi