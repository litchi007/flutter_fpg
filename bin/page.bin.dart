import 'dart:io';
/// 生成命令
/// ```shell
///   // 无 tag
///   dart ./bin/page.bin.dart test
///   // 有 tag
///   dart ./bin/page.bin.dart test id 
/// ```
void main(List<String> value) {
  // 文件名称
  final String name = value.first.toUpperCase().substring(0,1) + value.first.substring(1);
  // tag
  final String? tag = value.length >= 2 ? value.last : null;

  // 是否有 tag - 一般使用 id
  final directory = Directory("./lib/page/${value[0]}"); 
  if (directory.existsSync()) {
    print("🙅 文件夹已存在，请勿重复创建");
    return;
  }else {
    directory.createSync();
    // 创建页面需要的文件
    createPage("./lib/page/${value[0]}", value[0], name, tag);
    createController("./lib/page/${value[0]}", value[0], name, tag);
    createBind("./lib/page/${value[0]}", value[0], name, tag);
  }
  print(directory);
}

createPage(String dirPath, String fileName,String name, [String? tag]) {
  final file = File("$dirPath/$fileName.dart");

  final str = """
import 'package:fpg_flutter/page/$fileName/controller/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ${name}Page extends GetView<${name}Controller> {
  ${tag == null ? 'const ': ''}${name}Page({super.key});
  ${tag != null ? 'final tags = Get.parameters["$tag"];' : ''}
  ${
    tag != null ? '''
      @override
      String? get tag => tags;
    ''':''
  }
  
  @override
  Widget build(BuildContext context) {

    return Scaffold(

    body: SafeArea(
      child: Text('$name'))
    );
  }
}
  """;
  file.writeAsString(str);
}

createController(String dirPath, String fileName,String name, [String? tag]) {
  final dir = Directory("$dirPath/controller");
  dir.createSync();
  final file = File("$dirPath/controller/controller.dart");
  final str = """ 
import 'package:get/get.dart';

class ${name}Controller extends GetxController {
  ${tag == null ? 'static ${name}Controller get to => Get.find<${name}Controller>();' : ''}
}
  """;
  file.writeAsString(str);
}

createBind(String dirPath, String fileName,String name, [String? tag]) {
final dir = Directory("$dirPath/bind");
  dir.createSync();
  final file = File("$dirPath/bind/bind.dart");
  final str = """ 
import '../controller/controller.dart';
import 'package:get/get.dart';

class ${name}Binding implements Bindings {
@override
void dependencies() {
  ${tag == null ? '' : 'final tag = Get.parameters["$tag"];'}
  ${tag != null ? 'Get.lazyPut<${name}Controller>(() => ${name}Controller(), tag: tag)': 'Get.lazyPut<${name}Controller>(() => ${name}Controller())'};
  }
}
  """;
  file.writeAsString(str);
}