import 'dart:convert';

void main() {
  // final str = "😀😃😄😁😆😅🤣😂🙂🙃";
  // print(str);
  final map = {
    "a": 1
  };

  map.forEach((key, v) {
    print("$key $v");
  });
}

List<List<int>> gen(List<int> arr) {
  // final list = List.generate(3, (i) => [arr[i], -1, arr[i]]);
  // 每个数字都有三个组合
  final list = [
    [0, 0, -1],
    [0, -1, 0],
    [-1, 0, 0]
  ];
  List<List<int>> result = [];

  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr.length; j++) {
      if (j == i) {
        continue;
      }
      // 遍历list
      final item = list.map((e) {
        final tem = e.map((q) => q == 0 ? arr[i] : arr[j]).toList();
        return tem;
      }).toList();
      result.addAll(item);
    }
  }
  return result;
}

gen1(List<int> arr) {
  final result = [];
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr.length; j++) {
      if (i == j) {
        continue;
      }
      for (var q = 0; q < arr.length; q++) {
        if (q == i || q == j) {
          continue;
        }
        result.add([arr[i], arr[j], arr[q]]);
      }
    }
  }
  return result;
}
