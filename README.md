
## build_runner

```
flutter pub run build_runner build --delete-conflicting-outputs
```

# FPG Flutter(请勿修改[脚本文件](./bin/)、请勿修改已经规定的文件格式)

```
    # 打包
    ./bin/build.bin.sh
```

```
    # 热更新
    ./bin/upgrade.sh
```

```
    # 生成页面 会自动生成[页面需要的控制器/页面/自动绑定，销毁控制器] 生成后需要在[路由文件](./lib/router/router.dart)里自己添加GetPage
    dart ./bin/page.bin.dart xxx
    # example
    # 生成无路径参数页面 /test
    # dart ./bin/page.bin.dart test
    # 生成带有id的路径 /test1/:id
    # dart ./bin/page.bin.dart test id
```

```
    # 生成主题
    dart ./bin/theme.bin.dart
```

```
    # 生成国际化，主题， 格式化生成文件
    ./bin/start.sh
```

# 项目结构
- [静态资源存储](./assets/)
    - [图片资源](./assets/images/) 在[资源文件](./lib/constants/assets.dart)中
    - [json文件](./assets/json/) 
        - [api文件](./assets/json/api.json) 所有的api需要在这个文件里写，会自动生成对应的名称
        - [主题文件](./assets/json/theme.json) 换肤主题需要在这里配置，按照规定格式配置即可
    - [语言文件](./assets/language/) 国际化配置需要写在这个文件夹

- [接口文件](./lib/api)
- [公共组件](./lib/components)
- [app配置](./lib/configs)
- [静态资源路径](./lib/constants)
- [正则等工具路径](./lib/core)
- [组件等拓展](./lib/extension)
- [国际化生成文件](./lib/generated)
- [路由中间件](./lib/middleware/)
- [结构体，工具生成的结构体](./lib/models/)
- [页面](./lib/page/)
- [路由](./lib/router/)
- [全局控制器](./lib/services/)
    - [控制器入口](./lib/services/app_service.dart)
    - [全局状态控制器](./lib/services/global_service.dart)
    - [语言控制器](./lib/services/lang_service.dart)
    - [主题控制器](./lib/services/theme_service.dart)
- [工具类](./lib/utils/)
- [app主页面](./lib/app.dart)
- [app启动文件](./lib/main.dart)


# 使用技巧

```dart
/// 事件点击
Text("xxx").onTap(() {});
```

```dart
/// 16进制颜色转换
Text("xxxx", style: TextStyle(color: "#ffffff".color()))
```

```dart
/// 使用主题配色
Text("xxxx", style: TextStyle(color: context.customTheme?.error))
```

```dart
/// 使用国际化
Text(LocaleKeys.xx.tr)
/// 使用带参数的国际化 , 对应国际化文件 设置为{"xx": "test@price"}
Text(LocaleKeys.xx.trParams({
    "price": "11"
}))
```

# 生成结构体插件[Json to Dart Model](https://marketplace.visualstudio.com/items?itemName=hirantha.json-to-dart)

1. 粘贴json到剪贴板
```dart
/// 例如
{
    "price": 10,
    "name": "xx",
    "status": false
}
```
2. 使用command+p打开选择 ** Json To Dart: From Clipboard to Code Generation Clases** , 固定格式为 ***  xxxModel *** 
```dart
/// 例如接口路径为https://gsyahcot002uqmhd.aaasfjyt.com/wjapp/api.php?c=system&a=config
/// 则model格式为SystemConfigModel
```
3. 根据提示在 ./assets/progress 中找到对应的截图，生成文件
   (除了 Implement equality operator? 选择No， 其他Yes/No的选项全部选择Yes)
   (Select code generator 选择 JSON Serializable Generator)
    *** 必须这么选择，否则会造成生成失败 ***
4. 生成文件存放在[结构体文件里](./lib/models/)
5. 在[结构体文件](./lib/constants/data_factories.dart 中按照格式添加结构体，然后刷新app，热更新不会载入)
